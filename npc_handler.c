/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file npc_handler.c
 * @brief Contains function for handling clicking on NPC's.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "dialogue_handler.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
#include "skill_fishing.h"
#include "skill_thieving.h"

/**
 * Handles when a player does a click actions on an NPC.
 * @param *c Client instance.
 */
void handle_npc_interactions(struct client *c)
{
	if (is_player_frozen(c, 1))
	{
		c->plr->is_pursuing_target = 0;
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Turn player to face NPC position before starting actions.  */
	player_turn_to_world_coord_big(c, 
			NPC_SPAWNS[c->plr->action_index]->world_x,
			NPC_SPAWNS[c->plr->action_index]->world_y,
			NPCS[c->plr->action_id]->size,
			NPCS[c->plr->action_id]->size);

	/* Checks that the player is within a good distance to do the action,
	   and isn't moving anymore.  */
	if (check_within_object_range(c->plr->world_x, c->plr->world_y, 
							NPC_SPAWNS[c->plr->action_index]->world_x,
							NPC_SPAWNS[c->plr->action_index]->world_y, 
							NPCS[c->plr->action_id]->size,
							NPCS[c->plr->action_id]->size, -1, -1)
		&& c->plr->walking_dir == -1 && c->plr->running_dir == -1)
	{
		/* Stop following target when reached.  */
		c->plr->is_pursuing_target = 0;

		/* First npc action, typically talk.  */
		if (c->plr->action_click_num == 1)
		{
			switch (c->plr->action_id)
			{
				/* Small net fishing.  */
				case 316:
					c->plr->action_id_type = FISHING_SMALL_NET;
					init_fishing(c);
					printf("Net fishing detected.\n");
					break;
				
				/* Large net fishing.  */
				case 313:
					c->plr->action_id_type = FISHING_LARGE_NET;
					init_fishing(c);
					printf("Big net fishing detected.\n");
					break;
				
				/* Lure fishing.  */
				case 309:
				case 310:
				case 311:
				case 328:
					c->plr->action_id_type = FISHING_LURE;
					init_fishing(c);
					printf("Lure fishing detected.\n");
					break;
				
				/* Cage fishing.  */
				case 312:
					c->plr->action_id_type = FISHING_LOBSTER_POT;
					init_fishing(c);
					printf("Cage fishing detected.\n"); 
					break;

				default:
					set_npc_ai_state(NPC_SPAWNS[c->plr->action_index], 
									 AI_TALK);
					npc_face_player(NPC_SPAWNS[c->plr->action_index], 
									(c->plr->index + 1));
					c->plr->chat_type = NPC_CHAT;
					c->plr->next_chat_id = 0;

					set_player_state(c, STATE_IDLE);
					c->plr->targeted_npc = -1;
					c->plr->is_pursuing_target = 0;

					printf("NPC 1st click, not handled ID: %d\n", 
						   c->plr->action_id);
					handle_dialogue(c);
					break;
			}
		}
		/* Second npc action, used for fishing spots, thieving, and stores.  */
		else if (c->plr->action_click_num == 2)
		{
			switch (c->plr->action_id)
			{
			/* Fishing.  */
				/* Bait fishing.  */
				case 316:
					c->plr->action_id_type = FISHING_BAIT;
					init_fishing(c);
					printf("Bait fishing detected.\n");
					break;
				
				/* River bait fishing.  */
				case 309:
				case 310:
				case 311:
				case 328:
					c->plr->action_id_type = FISHING_RIVER_BAIT;
					init_fishing(c);
					printf("River bait fishing detected.\n");
					break;

				/* Harpoon fishing.  */
				case 312:
				case 313:
					c->plr->action_id_type = FISHING_HARPOON;
					init_fishing(c);
					printf("Harpoon fishing detected.\n");
					break;

			/* Thieving.  */
				/* Women and men.  */
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 24:
				case 25:
				case 170:
				case 351:
				case 352:
				case 353:
				case 354:
				case 359:
				case 360:
				case 361:
				case 362:
				case 363:
				case 726:
				case 727:
				case 728:
				case 729:
				case 730:
				case 1086:
					c->plr->action_id_type = THIEVING_PP_MAN;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Farmers.  */
				case 7:
				case 1377:
				case 1701:
				case 1757:
				case 1758:
				case 1759:
				case 1760:
				case 1761:
				/* Check this one.  */
				case 758:
					c->plr->action_id_type = THIEVING_PP_FARMER;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Female H.A.M.  */
				case 1715:
					c->plr->action_id_type = THIEVING_PP_FEMALE_HAM;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Male H.A.M.  */
				case 1714:
					c->plr->action_id_type = THIEVING_PP_MALE_HAM;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Warrior women, Al-Kharid Warriors.  */
				case 15:
				case 18:
					c->plr->action_id_type = THIEVING_PP_WARRIOR;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Rogue.  */
				case 187:
					c->plr->action_id_type = THIEVING_PP_ROGUE;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Master farmers.  */
				case 2234:
				case 2235:
					c->plr->action_id_type = THIEVING_PP_MASTER_FARMER;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Guard.  */
				case 9:
				case 10:
				case 32:
					c->plr->action_id_type = THIEVING_PP_GUARD;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Fremennik Citizens.  */
				case 1305:
				case 1306:
				case 1307:
				case 1308:
				case 1309:
				case 1310:
				case 1311:
				case 1312:
				case 1313:
				case 1314:
					c->plr->action_id_type = THIEVING_PP_FREMENNIK;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Bearded Pollnivnian Bandits.  */
				case 1882:	
				case 1883:
				case 1884:
					c->plr->action_id_type = THIEVING_PP_BEARDED_P_BANDIT;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Desert bandits.  */
				case 1926:
				case 1931:
					c->plr->action_id_type = THIEVING_PP_DESERT_BANDIT;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Knights of Ardougne.  */
				case 23:
				case 26:
					c->plr->action_id_type = THIEVING_PP_ARDOUGNE_KNIGHT;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Pollnivnian Bandits.  */
				case 1880:
				case 1881:
					c->plr->action_id_type = THIEVING_PP_P_BANDIT;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Yannile watchmen.  */
				case 34:
					c->plr->action_id_type = THIEVING_PP_WATCHMEN;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Menaphite Thug.  */
				case 1904:
				case 1905:
					c->plr->action_id_type = THIEVING_PP_MENAPHITE;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Paladin.  */
				case 20:
					c->plr->action_id_type = THIEVING_PP_PALADIN;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Gnome.  */
				case 66:
				case 67:
				case 68:
				case 159:
				case 160:
				case 161:
				case 168:
				case 169:
					c->plr->action_id_type = THIEVING_PP_GNOME;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Hero.  */
				case 21:
					c->plr->action_id_type = THIEVING_PP_HERO;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;
				/* Elves.  */
				case 2363:
				case 2364:
				case 2365:
				case 2366:
				case 2367:
					c->plr->action_id_type = THIEVING_PP_ELF;
					c->plr->action_skill_type = THIEVING_ACTION_PICKPOCKET;
					init_thieving(c);
					break;

				default:
					/* FIXME: Change this to be somewhere other than here.  */
					/* Should explicitly list the NPCs above.  */
					if (NPCS[c->plr->action_id]->shop_id != -1)
					{
						send_shop_menu(c, NPCS[c->plr->action_id]->shop_id);
						set_npc_ai_state(NPC_SPAWNS[c->plr->action_index], 
										 AI_TALK);
						set_player_state(c, STATE_IDLE);
						c->plr->targeted_npc = -1;
						c->plr->is_pursuing_target = 0;
					}
					else
					{
						printf("NPC 2nd click, not handled ID: %d\n", 
							   c->plr->action_id);
					}
					break;
			}
		}
		/* Third npc action. Used for runecrafting teleport.  */
		else if (c->plr->action_click_num == 3)
		{
			switch (c->plr->action_id)
			{
				/* Aubury, teleport to rune essence mines.  */
				case 553:
					printf("NOT IMPLEMENTED, FIXME.\n");
					break;

				default:
					printf("NPC 3rd click, not handled ID: %d\n", 
						   c->plr->action_id);
					break;
			}
		}
		//set_player_state(c, STATE_IDLE);
		
	}
}

/**
 * Handles when a player tries to use an item on an npc.
 * @param *c Client instance.
 */
void handle_item_on_npc(struct client *c)
{
	if (player_has_item_slot(c, c->plr->action_item_id, 1, 
							 c->plr->action_item_slot) == -1)
	{
		printf("Player %s tried to use a nonexistant item on an"
			   "npc!\n", c->plr->name);
		player_stop_facing_npc(c);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Ensure the player is within range of the npc before preforming the
	 * action.  */
	if (check_within_object_range(c->plr->world_x, c->plr->world_y, 
							NPC_SPAWNS[c->plr->action_index]->world_x,
							NPC_SPAWNS[c->plr->action_index]->world_y, 
							NPCS[c->plr->action_id]->size,
							NPCS[c->plr->action_id]->size, -1, -1)
		&& c->plr->walking_dir == -1 && c->plr->running_dir == -1)
	{
		/* Stop following target when reached.  */
		c->plr->is_pursuing_target = 0;

		switch (c->plr->action_id)
		{
			/* Unsheared sheep.  */
			case 43:
			case 1763:
			case 1765:
				switch (c->plr->action_item_id)
				{
					/* Shears.  */
					case 1735:
						player_play_animation(c, 893);

						/* Add wool and shear the sheep after a delay.  */
						c->plr->frozen_type = EFFECT_FREEZE;
						c->plr->frozen_ticks = 1;
						c->plr->item_add_timer = 3;
						c->plr->item_add_id = 1737;
						c->plr->item_add_num = 1;
						c->plr->item_add_charges = -1;
						c->plr->item_add_message = 1;
						c->plr->item_add_type = SHEEP_SHEARING;
						c->plr->item_add_idx = c->plr->action_index;
						c->plr->item_add_uninterruptable = 0;
						break;
				}
				break;

			default:
				packet_send_chatbox_message(c, "Nothing interesting happens.");
				/*
				printf("Unhandled item on npc!\n");
				printf("NPC id: %d item id: %d\n", c->plr->action_id, 
						c->plr->action_item_id);
				printf("NPC idx: %d\n", c->plr->action_index);
				*/
				break;
		}

		player_stop_facing_npc(c);
		set_player_state(c, STATE_IDLE);
	}
}
