/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file object.c
 * @brief Contains all functions for manipulation of objects.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "constants.h"

#include "client.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "regions.h"

/**
 * Finds the first free slot in a player's instanced objects array, and returns
 * the index.
 * @param *c Client instance.
 * @return -1 on non found, -2 on instanced objects array not created, and
 * anything else is the first empty index.
 */
int find_free_instanced_object_slot(struct client *c)
{
	int x;

	/* Sanity checking.  */
	if (c->plr->inst_objects == NULL || c->plr->inst_objects_size == 0)
	{
		return -2;
	}

	for (x = 0; x < c->plr->inst_objects_size; x++)
	{
		if (c->plr->inst_objects[x] == NULL)
		{
			return x;
		}
	}
	
	return -1;
}

/**
 * Searches through a player's instanced object list to see if there is one
 * that exists at the passed location.
 * @param *c Client instance.
 * @param pos_x X coord of the object.
 * @param pos_y Y coord of the object.
 * @param pos_z Z coord of the object.
 * @return Returns -1 on not found, the object's index in the array if found.
 */
int get_inst_object_index_at_position(struct client *c, int pos_x, int pos_y, 
		int pos_z)
{
	int x = 0;

	if (c->plr->inst_objects == NULL)
	{
		return -1;
	}

	for (x = 0; x < c->plr->inst_objects_size; x++)
	{
		if (c->plr->inst_objects[x] != NULL)
		{
			if (c->plr->inst_objects[x]->world_x == pos_x
				&& c->plr->inst_objects[x]->world_y == pos_y
				&& c->plr->inst_objects[x]->world_z == pos_z)
			{
				return x;
			}
		}
	}

	return -1;
}

static int create_inst_object_array(struct client *c)
{
	/* Check if the inst_objects array exists. If not, create it and 
	 * continue.  */
	if (c->plr->inst_objects == NULL)
	{
		printf("Instanced objects array not created, calloc'ing...\n");

		c->plr->inst_objects = calloc(INST_OBJ_INIT_SIZE, 
								   sizeof(struct obj_spawn*));
		c->plr->inst_objects_on_ground = calloc(INST_OBJ_INIT_SIZE, 
												sizeof(int));
		c->plr->inst_objects_size = INST_OBJ_INIT_SIZE;
		c->plr->inst_objects_max_index = 0;

		/* Check for proper malloc.  */
		if (c->plr->inst_objects == NULL 
			|| c->plr->inst_objects_on_ground == NULL)
		{
			printf("Unable to calloc for player instanced obj list!\n");
			return 0;
		}
	}

	return 1;
}

/**
 * Creates an object in the player's instanced object array.
 * @param *c Client instance.
 * @param obj_id ID of the object to create.
 * @param obj_type Type of object, used by the client.
 * @param pos_x, pos_y, pos_z World coordinates.
 * @param face Which direction the object faces.
 * @param despawn_ticks How many ticks the object lasts until removal, -1 if
 * object stays forever.
 * @return -1 on failure, otherwise returns object's index in the instanced
 * array.
 */
int create_inst_object(struct client *c, int obj_id, int obj_type, 
					   int pos_x, int pos_y, int pos_z, int face, 
					   int despawn_ticks)
{
	int x;

	if (c->plr->inst_objects == NULL)
	{
		create_inst_object_array(c);
	}
	
	x = find_free_instanced_object_slot(c);

	if (x == -1)
	{
		printf("Failed to find a free slot, expand array.\n");
		return -1;
	}
	else if (x == -2)
	{
		printf("Player didn't have arrays created, malloc failure.\n");
		return -1;
	}

	c->plr->inst_objects[x] = calloc(1, sizeof(struct obj_spawn));

	/* Setup object data.  */
	c->plr->inst_objects[x]->id = c->plr->inst_objects[x]->spawn_id = obj_id;
	c->plr->inst_objects[x]->type = obj_type;
	c->plr->inst_objects[x]->face = face;
	c->plr->inst_objects[x]->world_x = pos_x;
	c->plr->inst_objects[x]->world_y = pos_y;
	c->plr->inst_objects[x]->world_z = pos_z;
	c->plr->inst_objects[x]->despawn_ticks = despawn_ticks;
	c->plr->inst_objects[x]->despawn_globally = -1;

	return x;
}

/**
 * Spawns a player's instanced object in their world.
 * @param *c Client instance.
 * @param obj_index Index of the object to spawn in the inst_objects array.
 */
void spawn_inst_object_on_ground(struct client *c, int obj_index)
{
	/* Check if an object is even there.  */
	if (c->plr->inst_objects[obj_index] == NULL
		|| c->plr->inst_objects_on_ground == NULL)
	{
		return;
	}

	if ((get_distance_from_point(c->plr->world_x,
		c->plr->world_y, 
		c->plr->inst_objects[obj_index]->world_x, 
		c->plr->inst_objects[obj_index]->world_y) <= 60
		&& (c->plr->world_z == c->plr->inst_objects[obj_index]->world_z)))
	{
		if (c->plr->inst_objects_on_ground[obj_index] == 0)
		{
			/* Object has no timer going, spawn it.  */
			packet_draw_object_in_world_custom(c, 
				c->plr->inst_objects[obj_index]->world_x,
				c->plr->inst_objects[obj_index]->world_y,
				c->plr->inst_objects[obj_index]->type,
				c->plr->inst_objects[obj_index]->face,
				c->plr->inst_objects[obj_index]->id);

			c->plr->inst_objects_on_ground[obj_index] = 1;
		}
	}
}

/**
 * Despawns a player's instanced object in their world.
 * @param *c Client instance.
 * @param obj_index Index of the object to despawn in the inst_objects array.
 */
void despawn_inst_object_on_ground(struct client *c, int obj_index)
{
	/* Check if an object is even there.  */
	if (c->plr->inst_objects[obj_index] == NULL
		|| c->plr->inst_objects_on_ground == NULL)
	{
		return;
	}

	/* Check if object has been spawned for the client, then remove if so.  */
	if (c->plr->objects_on_ground[obj_index] == 1)
	{
		packet_erase_object_from_world_custom(c, 
			c->plr->inst_objects[obj_index]->world_x,
			c->plr->inst_objects[obj_index]->world_y,
			c->plr->inst_objects[obj_index]->type,
			c->plr->inst_objects[obj_index]->face);
		c->plr->inst_objects_on_ground[obj_index] = 0;
	}
}

/**
 * Delete a specific player's instanced object.
 * @param *c Client instance.
 * @param obj_index Index of the object to despawn in the inst_objects array.
 * @param clear_on_client If object should be cleared from the player's screen,
 * or not.
 */
void remove_inst_object(struct client *c, int obj_index, int clear_on_client)
{
	/* Check if an object is even there.  */
	if (c->plr->inst_objects[obj_index] == NULL
		|| c->plr->inst_objects_on_ground == NULL
		|| obj_index == -1)
	{
		return;
	}

	/* Remove object from screen if set to.  */
	if (clear_on_client == 1)
	{
		packet_erase_object_from_world_custom(c, 
			c->plr->inst_objects[obj_index]->world_x,
			c->plr->inst_objects[obj_index]->world_y,
			c->plr->inst_objects[obj_index]->type,
			c->plr->inst_objects[obj_index]->face);
	}

	c->plr->inst_objects_on_ground[obj_index] = 0;
	free(c->plr->inst_objects[obj_index]);
	c->plr->inst_objects[obj_index] = NULL;
}

/**
 * Setup a specific instanced object to transform to something for x amount
 * of server ticks.
 * @param *c Client instance.
 * @param obj_index Location of object in instanced object array.
 * @param new_id Object id to transform to.
 * @param duration How many server ticks to transform for.
 */
void handle_transforming_inst_object(struct client *c, int obj_index, 
									 int new_id, int duration)
{
	if (obj_index == -1)
	{
		return;
	}

	if (c->plr->inst_objects[obj_index] == NULL
		|| c->plr->inst_objects_on_ground == NULL)
	{
		printf("Tried to set transform timer for null instanced object, %d\n", 
			   obj_index);	
	}

	c->plr->inst_objects[obj_index]->id = new_id;
	c->plr->inst_objects[obj_index]->transform_ticks = duration;
}

/**
 * Applies default settings to a obj_spawn struct.
 * @param *obj Object spawn struct to apply defaults to.
 */
void apply_obj_spawn_defaults(struct obj_spawn *obj)
{
	obj->despawn_ticks = -1;
	obj->transform_ticks = -1;
	obj->spawn_ticks = -1;
	obj->paired_object = -1;
	obj->timer = 0;
	obj->internal_counter = 0;
	obj->cooldown_ticks = -1;
	obj->spawn_id = obj->id;
	obj->spawn_x = obj->world_x;
	obj->spawn_y = obj->world_y;
	obj->spawn_face = obj->face;
	obj->default_open_state = obj->open;
}

/**
 * Spawns an object in the world of a specific player.
 * @param *c Client instance.
 * @param obj_index Location of object in OBJECT_SPAWNS array.
 */
void spawn_object_on_ground(struct client *c, int obj_index)
{
	if ((get_distance_from_point(c->plr->world_x,
		c->plr->world_y,
		OBJECT_SPAWNS[obj_index]->world_x, 
		OBJECT_SPAWNS[obj_index]->world_y) <= 60
		&& (c->plr->world_z == OBJECT_SPAWNS[obj_index]->world_z)))
		/* TODO: Implement objects that only spawn when they should.  */
		/*&& OBJECT_SPAWNS[obj_index]->should_spawn == 1)*/
	{
		if (c->plr->objects_on_ground[obj_index] == 0)
		{
			/* Object has no timer going, spawn it.  */
			packet_draw_object_in_world(c, obj_index);
			c->plr->objects_on_ground[obj_index] = 1;
		}
	}
}

/**
 * Despawns an object in the world of a specific player.
 * @param *c Client instance.
 * @param obj_index Location of object in OBJECT_SPAWNS array.
 */
void despawn_object_on_ground(struct client *c, int obj_index)
{
	/* Check if object has been spawned for the client, then remove if so.  */
	if (c->plr->objects_on_ground[obj_index] == 1)
	{
		packet_erase_object_from_world(c, obj_index);
		c->plr->objects_on_ground[obj_index] = 0;
	}
}

/**
 * Clear player's array that keeps track of what objects have been spawned in
 * their game world, but don't remove them from the world.
 * @param *c Client instance.
 */
void reset_player_objects(struct client *c)
{
	int x;

	for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
	{
		c->plr->objects_on_ground[x] = 0;
	}
}

/**
 * Spawn an object to everyone on the server.
 * @param obj_index Location of object in the OBJECT_SPAWNS array.
 */
void spawn_object_globally(int obj_index)
{
	int x;
	if (OBJECT_SPAWNS[obj_index] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			spawn_object_on_ground(CONNECTED_CLIENTS[x], obj_index);
		}
	}
}

/**
 * Spawns the default object in the game world for a certain location. Used
 * when an object spawn overwrote a default object, and the default object
 * needs to be brought back.
 * @param world_x, world_y, world_z Location of object in the game world.
 */
void spawn_default_object_globally(int world_x, int world_y, int world_z)
{
	int x = 0, obj_index = -1;
	int reg_id = get_region_id(world_x, world_y);
	int reg_index = get_region_index(reg_id);
	int obj_type = -1;
	int face = -1;
	int id = -1;

	/* Check if a valid region could be found.  */
	if (reg_id == -1 || reg_index == -1)
	{
		return;
	}
	
	/**
	 * Check if there is an object with the given coordinates at this position.
	 */
	for (x = 0; x < REGION_LIST[reg_index]->obj_spawns_size; x++)
	{
		if (REGION_LIST[reg_index]->OBJECTS[x]->world_x == world_x
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_y == world_y
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_z == world_z)
		{
			obj_index = x;
			break;
		}
	}

	if (obj_index == -1)
	{
		return;
	}
	
	/* Read data from the object info.  */
	obj_type = REGION_LIST[reg_index]->OBJECTS[obj_index]->type;
	face = REGION_LIST[reg_index]->OBJECTS[obj_index]->face;
	id = REGION_LIST[reg_index]->OBJECTS[obj_index]->id;

	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			packet_draw_object_in_world_custom(CONNECTED_CLIENTS[x], world_x, 
										  world_y, obj_type, face, id);
				
		}
	}
}

/**
 * Sets an object so that it has never been spawned to any client, used to 
 * reset an object index specifically.
 * @param obj_index Object index in the OBJECT_SPAWNS array.
 */
void reset_object_globally(int obj_index)
{
	int x;

	if (OBJECT_SPAWNS[obj_index] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			CONNECTED_CLIENTS[x]->plr->objects_on_ground[obj_index] = 0;
		}
	}
}

/**
 * Creates a new object spawn in the first available slot.
 * @param obj_id ID of the new object.
 * @param obj_type Object type, used by the client.
 * @param pos_x, pos_y, pos_z World coordinates of the object.
 * @param face Direction the object is facing.
 * @param despawn_ticks Ticks until the object is removed from the world.
 * @param despawn_globally Should the object be removed for everyone, or not.
 * @return -1 on failure, anything else is the index of the new object in the
 * OBJECT_SPAWNS array.
 */
int add_to_object_list(int obj_id, int obj_type, int pos_x, int pos_y, 
					   int pos_z, int face, int despawn_ticks, 
					   int despawn_globally)
{
	int x;

	for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
	{
		/* If world_x is set to zero, there is nothing there.  */
		if (OBJECT_SPAWNS[x]->world_x == 0)
		{
			apply_obj_spawn_defaults(OBJECT_SPAWNS[x]);
			OBJECT_SPAWNS[x]->id = OBJECT_SPAWNS[x]->spawn_id = obj_id;
			OBJECT_SPAWNS[x]->face = OBJECT_SPAWNS[x]->spawn_face = face;
			OBJECT_SPAWNS[x]->world_x = OBJECT_SPAWNS[x]->spawn_x = pos_x;
			OBJECT_SPAWNS[x]->world_y = OBJECT_SPAWNS[x]->spawn_y = pos_y;
			OBJECT_SPAWNS[x]->world_z = pos_z;
			OBJECT_SPAWNS[x]->type = obj_type;
			OBJECT_SPAWNS[x]->despawn_ticks = despawn_ticks;
			OBJECT_SPAWNS[x]->despawn_globally = despawn_globally;
			return x;
		}
	}

	return -1;
}

/**
 * Removes an object from the OBJECT_SPAWNS array, optionally removes object
 * from player's game world as well.
 * @param obj_index Object location in the OBJECT_SPAWNS array.
 * @param remove Should the object be removed from everyone's game world, or
 * just deleted internally.
 */
void delete_from_object_list(int obj_index, int remove)
{
	int x;

	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			if (CONNECTED_CLIENTS[x]->plr->objects_on_ground[obj_index] == 1)
			{
				if (remove == 1)
				{
					packet_erase_object_from_world(CONNECTED_CLIENTS[x], obj_index);
				}
				CONNECTED_CLIENTS[x]->plr->objects_on_ground[obj_index] = 0;
			}
		}
	}
	OBJECT_SPAWNS[obj_index]->world_x = OBJECT_SPAWNS[obj_index]->world_y = 0; 
	OBJECT_SPAWNS[obj_index]->id = OBJECT_SPAWNS[obj_index]->face = 
	OBJECT_SPAWNS[obj_index]->world_z = OBJECT_SPAWNS[obj_index]->timer = 
	OBJECT_SPAWNS[obj_index]->despawn_ticks = -1;
}

/**
 * Tries to find an object spawn matching the passed coordinates, and returns
 * the index if found.
 * @param obj_x, obj_y, obj_z Coordinates to search.
 * @return -1 on failure, anything else is the found object's index.
 */
int get_object_spawn_index(int obj_x, int obj_y, int obj_z)
{
	int x;
	for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
	{
		if (OBJECT_SPAWNS[x]->world_x == obj_x 
			&& OBJECT_SPAWNS[x]->world_y == obj_y 
			&& OBJECT_SPAWNS[x]->world_z == obj_z)
		{
			return x;
		}
	}
	return -1;
}

/**
 * Tries to find an object in a specific world map region, with a specific id
 * and coordinates.
 * @param reg_index Index of the region in the REGION_LIST array.
 * @param world_x, world_y, world_z Coordinates to search.
 * @param id Object id to search for.
 * @return -1 on failure, anything else is the found object's index.
 */
int get_region_object_index(int reg_index, int world_x, int world_y, 
							   int world_z, int id)
{
	int x = 0;

	if (reg_index == -1)
	{
		return -1;
	}

	/* Get what region struct in the REGION_LIST array contains data
	   for the region we are getting data for.  */
	for (x = 0; x < REGION_LIST[reg_index]->obj_spawns_size; x++)
	{
		if (REGION_LIST[reg_index]->OBJECTS[x]->world_x == world_x
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_y == world_y
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_z == world_z
			&& REGION_LIST[reg_index]->OBJECTS[x]->id == id)
		{
			return x;
		}
	}
	
	return -1;
}

/**
 * Searches a specific region for an object matching the parameters, and
 * returns what direction it is facing.
 * @param reg_index Index of the region in the REGION_LIST array.
 * @param world_x, world_y, world_z Coordinates to search.
 * @param id Object id to search for.
 * @return -1 on failure, anything else is the found object's facing direction.
 */
int get_region_object_face(int reg_index, int world_x, int world_y, 
							  int world_z, int id)
{
	int x = 0;

	if (reg_index == -1)
	{
		return -1;
	}

	/* Get what region struct in the REGION_LIST array contains data
	   for the region we are getting data for.  */
	for (x = 0; x < REGION_LIST[reg_index]->obj_spawns_size; x++)
	{
		if (REGION_LIST[reg_index]->OBJECTS[x]->world_x == world_x
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_y == world_y
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_z == world_z
			&& REGION_LIST[reg_index]->OBJECTS[x]->id == id)
		{
			return REGION_LIST[reg_index]->OBJECTS[x]->face;
		}
	}
	
	return -1;
}

/**
 * Searches a specific region for an object matching the parameters, and
 * returns what object type it is.
 * @param reg_index Index of the region in the REGION_LIST array.
 * @param world_x, world_y, world_z Coordinates to search.
 * @param id Object id to search for.
 * @return -1 on failure, anything else is the found object's type.
 */
int get_region_object_type(int reg_index, int world_x, int world_y, 
							  int world_z, int id)
{
	int x = 0;

	if (reg_index == -1)
	{
		return -1;
	}

	/* Get what region struct in the REGION_LIST array contains data
	   for the region we are getting data for.  */
	for (x = 0; x < REGION_LIST[reg_index]->obj_spawns_size; x++)
	{
		if (REGION_LIST[reg_index]->OBJECTS[x]->world_x == world_x
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_y == world_y
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_z == world_z
			&& REGION_LIST[reg_index]->OBJECTS[x]->id == id)
		{
			return REGION_LIST[reg_index]->OBJECTS[x]->type;
		}
	}
	
	return -1;
}

/* Returns the index, and sets the obj_face and obj_type ints.  */
/**
 * Searches a specific region for an object matching the parameters, and
 * returns its index, and sets the passed pointers to the object's facing
 * direction and object type.
 * @param reg_index Index of the region in the REGION_LIST array.
 * @param world_x, world_y, world_z Coordinates to search.
 * @param id Object id to search for.
 * @param *obj_face Variable to set to the found object's facing direction.
 * @param *obj_type Variable to set to the found object's type.
 * @return -1 on failure, anything else is the found object's index.
 */
int get_region_object_data(int reg_index, int world_x, int world_y, 
							  int world_z, int *id, int *obj_face, 
							  int *obj_type)
{
	int x = 0;

	if (reg_index == -1)
	{
		return -1;
	}

	/* Get what region struct in the REGION_LIST array contains data
	   for the region we are getting data for.  */
	for (x = 0; x < REGION_LIST[reg_index]->obj_spawns_size; x++)
	{
		if (REGION_LIST[reg_index]->OBJECTS[x]->world_x == world_x
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_y == world_y
			&& REGION_LIST[reg_index]->OBJECTS[x]->world_z == world_z)
		{
			if (id != NULL)
			{
				*id = REGION_LIST[reg_index]->OBJECTS[x]->id;
			}

			if (obj_face != NULL)
			{
				*obj_face = REGION_LIST[reg_index]->OBJECTS[x]->face;
			}

			if (obj_type != NULL)
			{
				*obj_type = REGION_LIST[reg_index]->OBJECTS[x]->type;
			}
			return x;
		}
	}
	
	return -1;
}

/**
 * Change object so that it transforms into a different one for a certain 
 * duration.
 * @param obj_index Index of the object to change in the OBJECT_SPAWNS array.
 * @param new_id Id to change the object to.
 * @param duration How many server ticks the object should stay transformed.
 */
void set_object_transforms(int obj_index, int new_id, int duration)
{
	if (obj_index != -1)
	{
		OBJECT_SPAWNS[obj_index]->id = new_id;
		OBJECT_SPAWNS[obj_index]->transform_ticks = duration;
	}
	else
	{
		printf("Tried to set transform timer for null object in pos: %d!\n", 
				obj_index);
	}
}

/**
 * Sets and object to spawn after a specified number of ticks.
 * @param obj_index Index of the object to change in the OBJECT_SPAWNS array.
 * @param ticks Server ticks until object is spawned.
 */
void set_object_spawning_time(int obj_index, int ticks)
{
	if (obj_index != -1)
	{
		OBJECT_SPAWNS[obj_index]->spawn_ticks = ticks;
	}
	else
	{
		printf("Tried to set spawn timer for null object in pos: %d!\n", 
				obj_index);
	}
}


/**
 * Searches to see if there is an object at this position in the player's
 * instanced objects, object spawns, then game cache objects, in that order.
 * @param *c Client instance.
 * @param pos_x X coord of the object.
 * @param pos_y Y coord of the object.
 * @param pos_z Z coord of the object.
 * @param *instance_type Is set based on in which object area the object
 * was found in.
 * @param *obj_face Facing direction of the object.
 * @param *obj_type Object type.
 * @return Returns -1 on not found, the object's index in the array if found.
 */
int find_object_at_position(struct client *c, int pos_x, int pos_y, int pos_z, 
		int *instance_type, int *obj_face, int *obj_type)
{
	int rv = get_inst_object_index_at_position(c, pos_x, pos_y, pos_z);

	if (rv != -1)
	{
		*instance_type = 1;

		if (obj_face != NULL)
		{
			*obj_face = c->plr->inst_objects[rv]->face;
		}

		if (obj_type != NULL)
		{
			*obj_type = c->plr->inst_objects[rv]->type;
		}

		return rv;
	}
	
	rv = get_object_spawn_index(pos_x, pos_y, pos_z);	

	if (rv != -1)
	{
		*instance_type = 2;

		if (obj_face != NULL)
		{
			*obj_face = OBJECT_SPAWNS[rv]->face;
		}

		if (obj_type != NULL)
		{
			*obj_type = OBJECT_SPAWNS[rv]->type;
		}

		return rv;
	}

	rv = get_region_object_data(get_region_index(get_region_id(pos_x, pos_y)), 
				pos_x, pos_y, pos_z, NULL, obj_face, obj_type);

	if (rv != -1)
	{
		*instance_type = 3;
		return rv;
	}

	*instance_type = 0;
	return -1;
}
