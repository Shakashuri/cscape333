#ifndef _AUDIO_H_
#define _AUDIO_H_

enum SFX_IDS
{
	BONE_BURY_SFX = 308
};

/**
 * Stores information for each song in the game.
 */
struct music_info
{
	/** ID number of the song.  */
	int id;
	/** Name of the song.  */
	char name[200];
	/** If the song is unlocked by default for new players or not.  */
	int unlocked_by_default;
	/** ID of the button in the client song list that corresponds to this song
	 */
	int interface_id;
	/** List of region ids that this song plays at.  */
	int regions[MUSIC_MAX_REGIONS];
	
	/** Client config id that is used for setting this song to locked or 
	 * unlocked.  */
	int config_id;
	/** The specific bit that needs to be set to have this song unlocked. Uses
	 * the config_id variable that is part of this struct.  */
	unsigned int bit;
};

int get_region_music_id(int region_id);
int get_music_index(int music_id);
char *get_music_name(int music_id);
void update_unlocked_music_list(struct client *c);
void handle_clicked_song(struct client *c, int interface_id);
int get_npc_attack_sound(int npc_id);
int get_npc_block_sound(int npc_id);
int get_npc_death_sound(int npc_id);
int get_player_attack_sounds(struct client *c);

#endif
