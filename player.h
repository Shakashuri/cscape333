/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PLAYER_H_
#define _PLAYER_H_

struct action_queue_item
{
	int ticks;
	int action_type;
	int action_data_1;
	int action_data_2;
	int action_data_3;
	int action_data_4;
	int action_data_5;
	int action_data_6;
};

/**
 * @brief Holds all of the information that is needed for each player.
 */
struct player
{
/* GENERIC PLAYER BASE INFO */
	/** Player name.  */
	char name[13];
	/** Lowercase name used for savefile.  */
	char file_name[13];
	/** Player password.  */
	char password[21];
	/** Player id.  */
	int id;	
	/** Location in the CONNECTED_CLIENTS array.  */
	int index;
	/** Is the player logged in or not.  */
	int logged_in;
	/** What sex the player is.  */
	int sex;
	/** Player combat level, visible to other players.  */
	int combat_level;

	/** Player appearance data.  */
	int player_look[12];
	/** If player is set to look like a npc, this is set to the npc's id.  */
	int player_npc_id;

	/** Player levels in each skill.  */
	int level[PROTOCOL_NUM_SKILLS];
	/** Original level each skill is at, used for stat restoration 
	 * tracking.  */
	int base_level[PROTOCOL_NUM_SKILLS];
	/** Player xp values for each skill.  */
	float xp[PROTOCOL_NUM_SKILLS];
	/** Stores temporary bonuses for each skill.  */
	int temp_bonuses[PROTOCOL_NUM_SKILLS];

	/** Stores if the player needs to see the level up dialogue for a skill,
	 * used for quest level ups and the like.  */
	int needs_level_up_dialogue[PROTOCOL_NUM_SKILLS];

	/** Weight of all items carried.  */
	float weight;
	
	/** IDs of items in player's inventory.  */
	int items[INV_SIZE];
	/** Number of each item in inventory.  */
	int items_n[INV_SIZE];
	/** Number of charges each item in inventory has.  */
	int items_charges[INV_SIZE];
	
	/** Bank items.  */
	int bank_items[PLAYER_BANK_SIZE];
	/** Number of item in each bank slot.  */
	int bank_items_n[PLAYER_BANK_SIZE];
	/** Number of charges the item has.  */
	int bank_items_charges[PLAYER_BANK_SIZE];
	
	/** If player is swapping, or inserting items in their bank.  */
	int bank_is_insert_mode;
	/** If player is withdrawing items from their bank as notes.  */
	int bank_is_note_mode;
	
	/** Equipment items.  */
	int equipment[EQUIP_SIZE];
	/** Number of item in each equipment slot.  */
	int equipment_n[EQUIP_SIZE];
	/** Number of charges for each equipment item.  */
	int equipment_charges[EQUIP_SIZE];

	/** Bonuses added by equipment, other effects.  */
	int player_bonus[12];
	
	/**@{*/
	/** Friends and ignore lists. Contains names of players converted to
	 * longs.  */
	long friends[FRIEND_LIST_SIZE];
	long ignores[IGNORE_LIST_SIZE];
	/**@}*/

	/** Contains progress status for each quest.  */
	int quests[PROTOCOL_NUM_QUESTS];
	/** Holds data for each quest, used to track needed variables.  */
	int quest_data[PROTOCOL_NUM_QUESTS][5];
	
	/** Where the player data is saved.  */
	char save_location[40];
	/** Ticks until server saves this player's data again.  */
	int save_data_ticks;

	/** If this character was just created, needs save file to be made.  */
	int new_player;
	/** If player has the ability to change their look.  */
	int can_packet_player_change_appearance;

	/** If player is moderator, admin, regular player.  */
	int player_rights;

	/**@{*/
	/** Maps of where npc's and players are relative to current player.  */
	char player_in_list_bitmap[(MAX_PLAYERS + 7) >> 3];
	char npc_in_list_bitmap[(MAX_NPC_SPAWNS + 7) >> 3];
	/**@}*/

	/**@{*/
	/** Correspond to the lists of items and objects, contains if that
	 * specific item/object has been spawned in the player's view or not.  */
	/* FIXME: Replace these with a more memory efficient bitmap variant.  */
	int items_on_ground[MAX_ITEM_DROPS];
	int objects_on_ground[MAX_OBJECT_SPAWNS];
	/**@}*/

	/**@{*/
	/** List of players, npcs in the nearby area.
	 * Used for updating the area.  */
	struct client *local_players[MAX_LOCAL_PLAYERS];
	struct npc_spawn *local_npcs[MAX_LOCAL_NPCS];
	/**@}*/
	
	/** Number of players in the same region.  */
	int local_players_num;
	/** Number of NPCs in the same region.  */
	int local_npcs_num;

	/**@{*/
	/** Player's respawn point coordinates.  */
	int home_x;
	int home_y;
	int home_z;
	/**@}*/

	/** If player is in the wilderness, and what level.  */
	int wilderness_level;
	/** If player will drop items on death or not.  */	
	int in_unsafe_zone;

/* State machine related variables.  */
	/** What type of state is the player in? @see PLAYER_AI_STATES  */
	int state;
	int state_type;
	int state_subtype;

/* COMBAT RELATED VARIABLES  */
	/** Index of npc the player is targeting.  */
	int targeted_npc;
	/** Index of player the player is targeting.  */
	int targeted_player;

	/** If player is attacking an npc.  */
	int attacking_npc;
	/** If player is attacking an player.  */
	int attacking_player;

	/** If player is following an npc.  */
	int following_npc;
	/** If player is following an player.  */
	int following_player;

	/**@{*/
	/** Percentage values for how much a player's stat is reduced when
	 * cursed, and how long it will last.  */
	float atk_reduced;
	int atk_reduced_ticks;
	float str_reduced;
	int str_reduced_ticks;
	float def_reduced;
	int def_reduced_ticks;
	/**@}*/

	/** Ticks until player can move.  */
	int frozen_ticks;
	/** If player cannot move because of magical means, or just can't move
	 * until an animation is done. Type may also affect other interactions.
	 * 0 - Can't move due to animation, skilling, etc.
	 * 1 - Magical restraint
	 * 2 - Stun  */
	int frozen_type;

	/** Ticks until player can teleport again, due to teleblock spell.  */
	int tele_block_ticks;

	/**
	 * What kind of spellbook they have open:
	 * 0 - Modern
	 * 1 - Ancient
	 * 2 - Lunar (For later revisions)
	 */
	int spellbook;

	/** How many points they have from completing quests.  */
	int quest_points;

	/** Holds which prayers are currently active.  */
	int prayers_active[PROTOCOL_NUM_PRAYERS];

	/** Increased by prayer_total_drain each tick, when the drain_counter
	 * exceeds the drain_resistance, remove a prayer point.  */
	int prayer_drain_counter;
	/** Total amount draining from the prayer_drain_counter, from all prayers
	 * used.  */
	int prayer_total_drain;
	/** Limit for the prayer drain buildup until prayer points are removed.  */
	int prayer_drain_resistance;
	
	/**@{*/
	/** Holds what each skill is multiplied by, used in prayer handling.  */
	float prayer_attack_bonus;
	float prayer_strength_bonus;
	float prayer_defence_bonus;
	float prayer_ranged_bonus;
	float prayer_magic_bonus;
	/**@}*/

	/** For auto retaliation handling.  */
	int is_attacking;
	/** Is player pursuing a target, or just standing there.  */
	int is_pursuing_target;

	/** Controls if the player retaliates automatically when attacked.  */
	int will_retaliate;
	/** Keeps track of if the retaliation target is an npc or not.  */
	int retaliation_is_npc;
	/** Tracks the index of the entity to retaliate against.  */
	int retaliation_index;
	/** Retaliation happens one tick after the damage is done, this is used for
	 * that delay.  */
	int retaliation_delay;
	
	/** Used if the player needs to get a bit closer if they are out of range
	 * with spells, or bows.  */
	int is_moving_closer;
	
	/**@{*/
	/** Used to detect if player needs to recalculate path again. Keeps track
	 * of target's last position.  */
	int target_x;
	int target_y;
	int target_is_npc;
	/**@}*/

	/** Kind of fighting style used.
	 * @see COMBAT_STYLES
	 */
	int fighting_style;
	
	/** Type of damage dealt when attacking. 
	 * @see COMBAT_DAMAGE_TYPES
	 */
	int damage_type;

	/** Elemental damage done when attacking. 
	 * @see ELEMENTAL_DAMAGE_TYPES
	 */
	int elemental_damage_type;

	/** What percent bonus the player's special attack applies.  */
	float special_attack_bonus;

	/** If player accepts teleother spells, etc.  */
	int accepts_aid;
	/** If player is autocasting with a staff or not.  */
	int autocasting;
	/**@{*/
	/**
	 * These are used for when autocasting a spell is selected, to keep track
	 * of the spell used, or to record what spell was cast through the menu,
	 * not through the autocasting process.
	 * @todo Check if all of these are needed once magic is overhauled.
	 */
	/** Internal id, used for spell casting in combat.  */
	int cast_spell_id;
	/** ID of spell being autocast, if any.  */
	int autocast_spell_id;
	/** ID of spell being manually cast, if any.  */
	int cast_spell_index;
	/**@}*/

	/** If the player is currently casting a spell or not, mainly used for non
	 * combat one. Ensures that players cannot cast spells too quickly. Tracks
	 * how many ticks until the player can cast another spell.  */
	int is_casting_ticks;

	/**@{*/
	/** Holds damage done, its effect, poison damage.  */
	int hit_diff;
	int hit_diff2;
	int hit_diff_effect;
	int hit_diff2_effect;
	int poison_hit;
	int poison_hit2;
	/**@}*/
	
	/** What damage is the maximum the player can hit in combat.  */
	int max_hit;
	
	/** Queue of attacks dealt to the player.  */
	int hit_queued[HIT_QUEUE_SIZE];
	/** Used with hit_queued, ticks before the attack is applied.  */
	int hit_delay[HIT_QUEUE_SIZE];
	/** Effect dealt to the player when the hit is applied.  */
	int hit_effect[HIT_QUEUE_SIZE];
	/** If the hit is from a player or an npc. 0 - npc, 1 - player.  */ 
	int hit_from[HIT_QUEUE_SIZE];
	/** Index of the npc/player struct the attack is from.  */ 
	int hit_index[HIT_QUEUE_SIZE];

	/** Used to track the farthest a search needs to go when looking for hit
	 * updates.  */
	int highest_hit_offset;

	/** Hit graphic to play on player from enemy's attack/spell.  */
	int hit_gfx;
	/** Height of hit graphic to play at.  */
	int hit_gfx_height;
	/** How many ticks until the hit graphic plays.  */
	int hit_gfx_delay;

	/** Damage poison is currently dealing to the player.
	 * @see player::poison_timer for more info on poison mechanics.
	 */
	int poison_damage;
	/**
	 * When poisoned, starts at 400, reduced by one each tick. When it hits
	 * 300, 200, 100, 0, deals poison_damage. When it hits 0, reduce 
	 * poison_damage by one and reset this to 400 if poison_damage is not at 0
	 * now.
	 */
	int poison_timer;
	
	/** Keeps track of how many ticks until the player is vulnerable to being
	 * poisoned again.  */
	int poison_immunity_timer;

	/** Type of projectile shot from player, ranged or magic.  */
	int projectile_type;
	/** What time a projectile will be shot from the player.  */
	int projectile_delay;
	/** Id of the projectile.  */
	int projectile_id;

	/** Isn't used at this point, for eating food and such.  */
	int heal_update_required;

	/** Are private chat messages shown on the 3D game screen?  */
	int split_chat;

	/* Player states */
	/** If player is 'skulled', has attacked a player first and will drop
	 * all their items on death.  */
	int is_skulled;
	/** How many ticks left until the skull is removed.  */
	int skulled_ticks;

	/** Tracks what part of dying the player is in, used for handling
	 * when to play the death animation, drop loot, etc.  */
	int death_stage;

	/** Tracks number of items the player will keep on death.  */
	int death_items_kept;

	/** When player is in combat, this is set to a value decided in the config,
	 * counts down to 0 when out of combat, and when it hits 0 the player is
	 * not in combat anymore.  */
	int last_combat_time;

	/** Ticks from teleport time activation, to teleport.  */
	int teleport_timer;
	/** What spellbook is the teleport from, used to determine
	 * arrival animation (if any).  */
	int teleport_type;
	/**@{*/
	/** Where the player will teleport when doing a delayed teleport.  */
	int stored_tele_x;
	int stored_tele_y;
	int stored_tele_z;
	/**@}*/
	/** Holds the difference of the last teleport that happened, used for 
	 * stairs.  */
	int last_tele_z_diff;

	/**@{*/
	/** Setup for adding a item to the player's inventory after a delay.  */
	int item_add_timer;
	int item_add_id;
	int item_add_num;
	int item_add_charges;
	int item_add_message;
	/** Type of action to do after the item is added.  
	 * @see ITEM_ADD_TYPES  */
	int item_add_type;
	/** Index of an npc, object needed for the action.  */
	int item_add_idx;
	/** If the adding item to the inventory can be interrupted or not.  */
	int item_add_uninterruptable;
	/**@}*/

	/** Unknown what this is used for, may not be needed.
	 * @todo investigate what this is for.
	 */
	int new_hp_type;

	/** Player standing animation.  */
	int standing_emote;
	/** Player turning animation.  */
	int turn_emote;
	/** Player turning around animation.  */
	int turn_about_emote;
	/** Player turning clockwise animation.  */
	int turn_cw_emote;
	/** Player turning counter-clockwise animation.  */
	int turn_ccw_emote;
	/** Player walking animation.  */
	int walking_emote;
	/** Player running animation.  */
	int running_emote;
	
	/** Player attacking animation.  */
	int attacking_emote;
	/** Player blocking animation.  */
	int blocking_emote;

	/** Number of ticks before the player can attack again.  */
	int attack_speed;
	
	/** Used to keep track of attack speed related things. Once this hits zero,
	 * the player can attack again.  */
	int last_attack;

	/** Used to keep track of non-combat timers, eating food at the moment.  */
	int last_action;
	
	/** ID of graphic to play at the player.  */
	int gfx;
	/** Height to play the graphic at.  */
	int gfx_height;
	
	/** Did the player log out.  */
	int logged_out;
	/** Did the player disconnect uncleanly, through closing the client or
	 * pulling the ethernet cord, etc.  */
	int unclean_logout;
	/** Ticks until the player is disconnected from the server.  */
	int disconnect_timer;

	/** Amount of running energy the player has left.  */
	int running_energy;

	/** Tracks energy restored based on Agility level.  */
	int running_restore;
	/** Tracks energy drained based on weight carried.  */
	float running_drain;

	/** Timer that keeps track of when hp should be restored.  */
	int health_regen;
	/** Timer that keeps track of when stats should be restored.  */
	int stat_regen;
	/** Timer that keeps track of when stat buffs decay.  */
	int stat_decay;
	
	/** Handles player ranged damage increases.  */
	int ranged_strength;

	/** Active overhead prayer icon.  */
	int prayer_icon;
	/** Player killing related overhead icon.  */
	int pk_icon;
	/** May not be used/needed, I think 333 using a different method for
	 * showing hint icons. Investigate.  */
	int hint_icon;

	/** Used in the PLR_TEXT mask, may not really be used at all. 
	 * Investigate.  */
	char player_string[PLAYER_STRING_LENGTH];
	
	/** Stores what is said by the player.  */
	char chat_text[CHAT_TEXT_SIZE];
	/** What effect is applied to the message.  */
	char chat_text_effect;
	/** What color the message is.  */
	char chat_text_color;
	/** How big the message is.  */
	char chat_msg_size;
	/** What type of dialogue is being used.  */
	int chat_type;
	/** Id for the next chat message to send to the player.  */
	int next_chat_id;
	/** What choice did the player pick in a dialogue.  */
	int chat_choice;

	/** 
	 * Masks used for the player update procedure to tell what
	 * needs to be resent. UPDATE_MASKS above corresponds with 
	 * each in this array.
	 */
	int update_masks[11]; 

/* OBJECT HANDLING */
	/**
	 * Masks used for the main loop to help determine what the player
	 * is walking to do (red X, left clicked on object) and how to handle
	 * that/check when they have walked to that position.
	 */
	int action_mask;
	/** Tracks what interface is shown to the player at the moment.  */
	int interface_open;
	/** Additional tracking for interface.  */
	int interface_open_sub;
	/** Tracks what interface is shown in the player's sidebar at the 
	 * moment.  */
	int interface_open_sidebar;
	/** Additional tracking for interface.  */
	int interface_open_sidebar_sub;

	/** Tracks if the player can use interfaces from the sidebar, or something
	 * isn't allowing it.  */
	int sidebar_hidden;

	/* Corresponds to the array above, stores data relating to the 
	   action requested.  */
	/**@{*/
	/** Coordinates where the action the player is doing is located.  */
	int action_x;
	int action_y;
	int action_z;
	/**@}*/

	/**@{*/
	/** Holds any extra information that is needed for an action.  */
	int action_store_one;
	int action_store_two;
	int action_store_three;
	int action_store_four;
	/**@}*/

	/** ID of the npc/object the player is doing an action on.  */
	int action_id;
	/** Index of the action target in the global arrays.  */
	int action_index;
	/** Index of the region that the action target is located.  */
	int action_region_index;

	/**@{*/
	/** ID numbers of items needed for the action.  */
	int action_item_id;
	int action_item_id_2;
	/**@}*/

	/** ID of the spell used. Only used for charge orb spells so far.  */
	int action_spell_id;

	/**@{*/
	/** Slot locations of items needed for the action.  */
	int action_item_slot;
	int action_item_slot_2;
	/**@}*/

	/** How many free inventory slots are needed for the action.  */
	int action_item_space_needed;
	/**@{*/
	/** Used for certain skill handlers.  */
	int action_id_type;
	int action_id_type2;
	/**@}*/
	/** Used for handling skills. Ticks for how long an action lasts,
	 * skill id for what skill is being used, skill type for what action
	 * from that skill is being done.  */
	int action_ticks;
	/** Total number of ticks that the player has been doing some action. Can
	 * be incremented once per tick usually, but can also used to track what
	 * stage in a process the player is.  */
	int action_total_time_acted;
	/** Used to check if the player has been trying too long and hasn't
	 * mined/chopped anything.  
	 * @deprecated Will be removed soon.  */
	int action_time_wo_resource;

	/** If the action is tied to a skill, what id of skill is being used.
	 * @see PLAYER_SKILLS.  */
	int action_skill_id;
	/** Subcategory of skill, if any. For example, this is used to split
	 * regular firemaking with pyre log firemaking.  */
	int action_skill_type;
	/** Used when the player has to put something in a furnace, and wait for
	 * it to come back.  */
	int action_skill_waiting_for_resource;
	/** ID of the item that is received at the end of the action.  */
	int action_skill_result_item;
	/** Level of the skill required to do the action.  */
	int action_skill_lvl_req;
	/** How many times left to do the requested action.  */
	int action_skill_remaining_actions;
	/**@{*/
	/** If a skill menu is used, these are used to store what the player
	 * picked from it, what menu it was.  */
	int action_menu_id;
	int action_menu_skill_id;
	int action_menu_choice;
	/**@}*/
	/** Tracks what option on an actionable thing the player chose.  */
	int action_click_num;

	/**@{*/
	/** Distance requirements to activate object, handled on a per 
	   object basis.  */
	int action_range_x;
	int action_range_y;
	/**@}*/

/* SKILL RELATED INFO.  */

	/* Variables to hold the id of the skill item in use,
	   the bonus to the skill it provides, if any,
	   and what emote plays when using it.  */
	/* Skill tool id, which slot it is in, and whether or not it is in inv or
	   equipment.  */
	/** ID of an item needed for a skill.  */
	int skill_tool_id;
	/** Slot that the tool is equipped in, if any.  */
	int skill_tool_slot;
	/** Is the tool equipped or not.  */
	int skill_tool_equip;
	/**@{*/
	/** Holds the ids for needed ingredients for skills.  */
	int skill_req_one_id;
	int skill_req_two_id;
	int skill_req_three_id;
	int skill_req_four_id;
	/**@}*/

	/** Skill bonus the tool gives, if any.  */
	int skill_tool_bonus;
	/** Skill time the tool requires.  */
	int skill_tool_time;
	/** Emote played when tool is used.  */
	int skill_tool_emote;

/* PLAYER LOCATION/MOVEMENT INFO.  */

	/** For FACE_MOB update mask.
	 * If value is under 32768, it is an NPC's ID to face. If it is above
	 * 32768, then 32768 is subtracted from it, and the result is the player
	 * ID to face.  */
	int face_mob;

	/* Things specific to handling player movement  */
	
	/**@{*/
	/** Direction moving while walking/running.  */
	int walking_dir;
	int running_dir;
	/**@{*/

	/** Direction that the player moved last, current facing direction.
	 * @bug Need to add things that invalidate this, such as being attacked
	 * by some sort of monster, no way currently to track changes like that.
	 * @todo Maybe rename this? Also used internally for detecting which way
	 * the player is facing.
	 */
	int last_walked_direction;

	/** If player is running or not.  */
	int is_running;
	/** If player is running for this waypoint, or not. Used for when a 
	 * movement command is sent by the player, while holding ctrl.  */
	int is_running_this_path;

	/** Player's X coordinate, position in world.  */
	int world_x;
	/** Player's Y coordinate, position in world.  */
	int world_y;
	/** Player's Z coordinate, goes from 0 to 3.  */
	int world_z;
	
	/**@{*/
	/** Position of the player relative to their current area. Where they are
	 * in the world region.  */
	int in_region_x;
	int in_region_y;
	/**@}*/
	
	/**@{*/
	/** Position of the area the player is in relative to the world. Where the
	 * region they are in is, relative to the other regions.  */
	int world_region_x;
	int world_region_y;
	/**@}*/

	/** Track what the last region the player was in, used to determine when to
	 * change music tracks.  */
	int old_region_id;
	/** Current region id that the player is in right now.  */
	int region_id;

	/** Track what music has been unlocked by the player.  */
	int unlocked_music[PROTOCOL_NUM_SONGS];

	/**@{*/
	/** Position of where they will teleport/travel. Different from delayed
	 * teleport, these are used for instant teleports.
	 */
	int tele_x;
	int tele_y;
	int tele_z;
	/**@}*/
	
	/** If the player has moved to a different map region and needs update.  */
	int map_region_changed;
	/** If npcs in area have changed, resends npc list.  */
	int rebuild_npc_list;
	/** If player just teleported, set to 1.  */
	int teleported;
	/** Has the player used an emote, changed equipment, etc. Need to update
	 * their appearance.  */
	int update_required;

	/**
	 * If the player is currently moving or not.
	 * @note Don't think this is needed, could just use walking_dir and 
	 * running_dir.
	 */
	int moving;

	/**@{*/
	/** At what point in the game world is the player facing.  */
	int focus_point_x;
	int focus_point_y;
	/**@}*/

	/**@{*/
	/** Walking queue, holds the squares the player will walk to when 
	 * moving.  */
	int walking_queue_x[WALK_QUEUE_SIZE];
	int walking_queue_y[WALK_QUEUE_SIZE];
	/**@}*/
	
	/** Offset used for tracking what position we are reading from in the 
	 * queue.  */
	int walking_queue_reading_offset;
	/** Offset used for tracking what position we are writing to in the 
	 * queue.  */
	int walking_queue_writing_offset;

	int destination_movement_needs_updating;
	int destination_x;
	int destination_y;
	int destination_size_x;
	int destination_size_y;

	/** ID of the animation to play.  */
	int animation_request;
	/** ID of the last animation that played.  */
	int animation_request_last;
	/** Ticks until the player does the animation in animation_request.  */
	int animation_wait_cycles;

	/** Animation play tupe, if it repeats, normal, or stops overwriting.  */
	int animation_play_type;
	/** Holds what animation ID to repeat.  */
	int animation_stored_emote;
	/** Ticks until the animation action is done.  */
	int animation_stored_ticks;
	/** Ticks the animation action starts with.  */
	int animation_stored_ticks_start;

	/**@{*/
	/** Starting and end position coordinates used for the agility update mask.
	 * Is in the world coodinate format, conversion is done by the mask handler
	 * in player.c.  */
	int phase_movement_start_x;
	int phase_movement_start_y;
	int phase_movement_end_x;
	int phase_movement_end_y;
	/**@}*/
	/**@{*/
	/** Start and end time of the movement. Both are times relative to the tick
	 * the client gets this update packet, thus end_time must be greater than
	 * start time. A client crash will occur if they are the same value.  */
	int phase_movement_start_time;
	int phase_movement_end_time;
	/**@}*/
	/** Direction the player faces when doing the agility movement. Client 
	 * format.  */
	int phase_movement_direction;

	/** Timed action type.  */
	int timed_action_type;
	/** Time until action repeats.  */
	int timed_action_ticks;
	/** Delay between each action.  */
	int timed_action_ticks_start;
	/** Stores a variable if needed.  */
	int timed_action_store;

	/** Instanced object spawning. Array is created and resized when needed.
	 */
	struct obj_spawn **inst_objects;
	/** Keeps track of inst_objects that have been spawned for the player.  */
	int *inst_objects_on_ground;
	/** How big the dynamic array for inst_objects is.  */
	int inst_objects_size;
	/** What the last used index in the array is.  
	 * @note may not be used currently. Fix this later.
	 */
	int inst_objects_max_index;

	/** Are flour bins spawned for the player?  */
	int flour_bins_spawned;
	/** Instanced object indexes for the bins.  */
	int flour_bins_indexes[PROTOCOL_MAX_FLOUR_BINS];
	/** Need to use instanced object tracking for grain bins.  */
	int grain_bins_flour_count;
	/** How many things of wheat is in the hopper, before it gets ground.  */
	int grain_hopper_count;
	/** Track what course is being done.  */
	int agility_course_idx;
	/** How many agility obstacles there are total in the current course.  */
	int agility_obstacles_number;
	/** Used to track the progress for agility courses.  */
	int agility_course_progress[AGILITY_MAX_OBSTACLES];
	
	struct action_queue_item action_queue[ACTION_QUEUE_SIZE];
	int action_queue_current_index;
	int action_queue_last_index;
	int action_queue_ticks;
	int action_queue_next_action_ticks;
};

char *get_player_state_text(int state);
void set_player_initial_state(struct client *c);
void set_player_state(struct client *c, int new_state);
void set_player_state_type(struct client *c, int new_state_type);
void set_player_state_subtype(struct client *c, int new_state_subtype);
int is_player_dead(struct client *c);
int is_player_in_combat(struct client *c);
int is_player_targeted(struct client *c);
int is_player_poisoned(struct client *c);
int is_player_poison_immune(struct client *c);
int is_player_poisonable(struct client *c);
void set_player_poison_damage(struct client *c, int damage, int timer);

void player_update_viewport(struct client *c); 

int get_free_inv_slots(struct client *c);

void clear_player_update_flags(struct client *c);

int is_player_within_viewpoint_distance(struct client *c, struct client *other);
int get_player_next_walking_direction(struct client *c);
void player_update_world_location(struct client *c);
void player_clear_walking_queue(struct client *c); 

void player_move_one_square(struct client *c, int x, int y); 
void player_add_straight_line_path_to_walking_queue(struct client *c, 
	int dest_x, int dest_y, int ignore_collision, int include_last);
void player_turn_to_world_coord(struct client *c, int x, int y);
void player_turn_to_direction(struct client *c, int direction);
void player_turn_to_world_coord_big(struct client *c, int x, int y, 
						int size_x, int size_y);
void player_turn_to_object(struct client *c, int x, int y, int size_x, 
						   int size_y, int face);
void player_face_npc(struct client *c, int npc_index);
void player_face_player(struct client *c, int player_index);
void player_stop_facing_npc(struct client *c);
void player_draw_gfx(struct client *c, int gfx_id);
void player_draw_gfx_at_height(struct client *c, int gfx_id, int height);

void player_stop_all_movement(struct client *c);
void player_check_if_movement_needs_updating(struct client *c);
void player_move_to_destination_point(struct client *c);
void player_process_damage(struct client *c);
void player_process_death(struct client *c);
void player_process_health_regen(struct client *c);
void player_process_stat_regen(struct client *c);
void player_process_stat_decay(struct client *c);
void player_process_poison_damage(struct client *c);
void player_process_prayer_drain(struct client *c);
void player_process_skills(struct client *c);
void player_process_timed_teleport(struct client *c);
void player_process_run_energy(struct client *c);
void player_process_following_target(struct client *c);
void player_process_save(struct client *c);
void player_process_music(struct client *c);
void player_process_stats(struct client *c);
void player_process_timed_actions(struct client *c);
#endif
