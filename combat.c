/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file combat.c
 * @brief Combat related functions, targeting.
 */
#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "audio.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "packet.h"
#include "pathfinding.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "skill_magic.h"

/**
 * Resets all player's combat variables.
 *
 * @param *c Client instance.
 */
void reset_player_combat(struct client *c)
{
	c->plr->targeted_npc = -1;
	c->plr->targeted_player = -1;
	c->plr->cast_spell_id = 0;
	
	c->plr->is_attacking = 0;
	c->plr->target_x = c->plr->target_y = -1;
	c->plr->is_pursuing_target = 0;
	c->plr->is_moving_closer = 0;
	player_replay_animation(c);
	player_stop_facing_npc(c);

	set_player_state(c, STATE_IDLE);
}

/**
 * Resets player's combat target, but not in combat state.
 *
 * @param *c Client instance.
 */
void reset_player_combat_target(struct client *c)
{
	c->plr->targeted_npc = -1;
	c->plr->targeted_player = -1;
	c->plr->cast_spell_id = 0;
	
	c->plr->is_attacking = 0;
	c->plr->is_pursuing_target = 0;
	c->plr->is_moving_closer = 0;
	player_replay_animation(c);
	player_stop_facing_npc(c);

	set_player_state(c, STATE_IDLE);
}

/**
 * Ends combat for npc, resets all needed variables.
 * @param *npc NPC instance.
 */
void reset_npc_combat(struct npc_spawn *npc)
{
	npc->walking_to_point = 1;
	npc->walking_to_home = 1;
	npc->targeted_player = -1;
	npc->face_mob = -1;
	npc->update_masks[FACE_MOB] = 1;
	npc->update_required = 1;
	npc->in_combat = 0;

	npc->target_x = -1;
	npc->target_y = -1;

	set_npc_path(npc, npc->world_x, npc->world_y, npc->spawn_x, 
				 npc->spawn_y, npc->world_z, 1);
}

/**
 * Ends combat for npc. Resets all needed variables and moves them back
 * to their spawn location.
 * @param *npc NPC instance.
 */
void reset_npc_combat_move_to_home(struct npc_spawn *npc)
{
	npc->walking_to_point = 1;
	npc->walking_to_home = 1;
	npc->targeted_player = -1;
	npc->face_mob = -1;
	npc->update_masks[FACE_MOB] = 1;
	npc->update_required = 1;
	npc->in_combat = 0;

	npc->target_x = -1;
	npc->target_y = -1;

	set_npc_path(npc, npc->world_x, npc->world_y, npc->spawn_x, 
				 npc->spawn_y, npc->world_z, 1);
}

/**
 * Sets player's combat target to either an npc or a player.
 *
 * @param *c Client instance.
 * @param npc_index NPC's index in spawns that the player is targeting.
 * @param player_index Player's index in CONNECTED_CLIENTS that the player is 
 * targeting.
 */
void set_player_combat_target(struct client *c, int npc_index, 
							  int player_index)
{
	if (npc_index != -1 && player_index != -1)
	{
		printf("INVALID PLAYER COMBAT TARGETS!\n");
		return;
	}

	if (npc_index != -1)
	{
		c->plr->target_is_npc = 1;
		c->plr->targeted_npc = npc_index;
		c->plr->target_x = NPC_SPAWNS[npc_index]->world_x;
		c->plr->target_y = NPC_SPAWNS[npc_index]->world_y;
		player_face_npc(c, npc_index);
	}
	else if (player_index != -1)
	{
		c->plr->target_is_npc = 0;
		c->plr->targeted_player = player_index;
		c->plr->target_x = CONNECTED_CLIENTS[player_index]->plr->world_x;
		c->plr->target_y = CONNECTED_CLIENTS[player_index]->plr->world_y;
		player_face_player(c, player_index);
	}

	c->plr->is_attacking = 1;
	set_player_state(c, STATE_ATTACK);
}

/**
 * Sets NPC's combat target to either an npc or a player.
 *
 * @param *n NPC spawn instance.
 * @param npc_index NPC's index in spawns that the NPC is targeting.
 * @param player_index Player's index in CONNECTED_CLIENTS that the NPC is 
 * targeting.
 */
void set_npc_combat_target(struct npc_spawn *n, int npc_index, 
						   int player_index)
{
	if (n->ai_state == AI_RESPAWN || is_npc_dead(n))
	{
		printf("NPC cannot attack now!\n");
		return;
	}

	if (npc_index != -1 && player_index != -1)
	{
		printf("INVALID NPC COMBAT TARGETS!\n");
		return;
	}
	if (n->health == 0 || n->attackable == 0)
	{
		printf("NPC cannot attack!\n");
		return;
	}
	if (npc_index != -1)
	{
		n->targeted_npc = npc_index;
		n->target_x = NPC_SPAWNS[npc_index]->world_x;
		n->target_y = NPC_SPAWNS[npc_index]->world_y;
	}
	if (player_index != -1)
	{
		n->targeted_player = player_index;
		n->target_x = CONNECTED_CLIENTS[player_index]->plr->world_x;
		n->target_y = CONNECTED_CLIENTS[player_index]->plr->world_y;
	}

	set_npc_ai_state(n, AI_ATTACK);
}

/**
 * Checks if an NPC has a valid player target to attack.
 * @param *npc NPC instance.
 * @return Returns 0 if invalid target, 1 if valid.
 */
int is_npc_combat_target_player_valid(struct npc_spawn *npc)
{
	if (npc->targeted_player == -1)
	{
		printf("No target, stopping.\n");
		reset_npc_combat_move_to_home(npc);
		set_npc_ai_state(npc, AI_IDLE);
		return 0;
	}
	else if (CONNECTED_CLIENTS[npc->targeted_player] == NULL)
	{
		printf("Client left, stopping combat.\n");
		reset_npc_combat_move_to_home(npc);
		set_npc_ai_state(npc, AI_IDLE);
		return 0;
	}

	return 1;
}

/**
 * Sets what entity the player should retaliate against when they can.
 * @param *c Client instance.
 * @param is_player If the attacker was a player, or if it is an NPC.
 * @param index Array index for the attacking entity.
 */
void set_player_retaliation_target(struct client *c, int is_player, int index)
{
	/* Make sure nothing is overwritten here.  */
	if (c->plr->retaliation_delay != -1 || index == -1)
	{
		return;
	}

	/* Retaliate if set to.  */
	if (c->plr->will_retaliate == 1)
	{
		/* If 0, hit is from an NPC.  */
		if (is_player == 0 && c->plr->targeted_npc == -1)
		{
			/* Check if the attacking entity has died, ignore retaliation if
			 * so.  */
			if (NPC_SPAWNS[index]->dead != 0)
			{
				return;
			}

			if (c->plr->targeted_npc == -1)
			{
				c->plr->retaliation_is_npc = 1;
				c->plr->retaliation_index = index;
				set_player_combat_target(c, index, -1);
			}
		}
		else if (is_player == 1 && c->plr->targeted_player == -1)
		{
			/* Check if the attacking entity has died, ignore retaliation if
			 * so.  */
			if (CONNECTED_CLIENTS[index]->plr->death_stage != 0)
			{
				return;
			}

			if (c->plr->targeted_player == -1)
			{
				c->plr->retaliation_is_npc = 0;
				c->plr->retaliation_index = index;
				set_player_combat_target(c, -1, index);
			}
		}
		
		c->plr->retaliation_delay = 1;
	}
}

/**
 * Check all npcs around a player for aggression, send them to attack the 
 * player if all criteria are met.
 * @param *c Client instance.
 * @todo Add multicombat zone support when implemented.
 * @todo Optimize this so it won't keep checking for combat when there are no
 * combat ready NPC's until a new NPC is added.
 * @todo Change the aggro type to an enum for clarity.
 */
void process_aggressive_npcs(struct client *c)
{
	int x = 0;

	/* Loop through all the npcs in the players area, checking for aggressive
	 * ones to attack this player.  */
	for (x = 0; x < c->plr->local_npcs_num; x++)
	{
		if (c->plr->local_npcs[x] == NULL)
		{
			continue;
		}
		else
		{
			if (c->plr->local_npcs[x]->dead != 0)
			{
				continue;
			}

			int aggro_type = c->plr->local_npcs[x]->aggressive;
			int npc_level = NPCS[c->plr->local_npcs[x]->npc_id]->level;

			if (aggro_type > 0 
				&& c->plr->local_npcs[x]->ai_state != AI_ATTACK)
			{
				/* If aggro type is 1, only attack players that are less than
				 * double the NPC's combat level. If aggro type is 2, attack
				 * anyone.
				 */
				if ((aggro_type == 1
					&& c->plr->combat_level <= (npc_level * 2))
					|| aggro_type == 2)
				{
					int distance 
						= get_distance_from_point(c->plr->world_x, 
									c->plr->world_y, 
									c->plr->local_npcs[x]->world_x,
									c->plr->local_npcs[x]->world_y);
					if (distance 
						<= NPCS[c->plr->local_npcs[x]->npc_id]
								->aggressive_distance
						&& is_player_in_combat(c) == 0)
						/* NOTE: This is here for non-multicombat zones. This
						 * needs to be removed for multicombat zones to work.
						 */
					{
						set_npc_combat_target(c->plr->local_npcs[x], -1, 
											  c->plr->index);
						break;
					}
					else
					{
						/* Out of range.  */
						continue;
					}
				}
				else
				{
					/* Player level too high, won't attack.  */
					continue;
				}
			}
		}
	}
}

/**
 * @brief Check everything for a player starting combat with something, and 
 * start it if everything is valid.
 *
 * @param *c Client instance.
 * @param npc_index NPC's index in spawns that the player is targeting, -1 if 
 * none.
 * @param player_index Player's index in CONNECTED_CLIENTS that the player is 
 * targeting, -1 if none.
 * @param spell_id Id of a spell that the player is using, -1 if none.
 *
 * @return 0 if combat was started, anything else for error.
 */
int player_init_combat(struct client *c, int npc_index, int player_index, 
					   int spell_id)
{
	printf("init combat Nidx %d Pidx %d\n", npc_index, player_index);
	/* Check for player death, don't allow attacking if so.  */
	if (is_player_dead(c))
	{
		printf("can't attack while dead!\n");
		return 1;
	}

	/* If player is wanting to attack an npc.  */
	if (npc_index != -1)
	{
		/* Ensure the npc isn't dead.  */
		if (is_npc_dead(NPC_SPAWNS[npc_index]) == 1)
		{
			printf("npc is dead!\n");
			set_player_state(c, STATE_IDLE);
			return 3;
		}

		/* Check if npc is even able to be attacked.  */
		if (NPCS[NPC_SPAWNS[npc_index]->npc_id]->attackable == 0)
		{
			packet_send_chatbox_message(c, "You can't attack that!");
			reset_player_combat_target(c);
			set_player_state(c, STATE_IDLE);
			return 3;
		}
		/* Check for a spell, and see if it is valid for use on an npc.  */
		if (spell_id != -1)
		{
			/* If spell is a teleother, cancel it.  */
			/* TODO: Replace this with a value in the spells struct?  */
			if (spell_id == 12425 || spell_id == 12435 || spell_id == 12455)
			{
				packet_send_chatbox_message(c, "You can't cast that spell on that!");
				reset_player_combat_target(c);
				set_player_state(c, STATE_IDLE);
				return 3;
			}
		}

		/* TODO: Check for multicombat too.  */
		if (is_player_in_combat(c)
			&& c->plr->index != NPC_SPAWNS[npc_index]->targeted_player)
		{
			if (is_player_targeted(c))
			{
				player_clear_walking_queue(c);
				player_stop_all_movement(c);
				packet_send_chatbox_message(c, "You are already in combat!");
				printf("already in combat.\n");
				set_player_state(c, STATE_IDLE);
				return 1;
			}
		}

		printf("set target: %d\n", npc_index);
		set_player_combat_target(c, npc_index, -1);
	}
	else
	{
		printf("invalid npc index\n");
	}

	if (player_index != -1)
	{
		printf("PVP TO DO!\n");
	}

	return 0;
}

/**
 * @brief Applies poison damage to a player.
 *
 * @param *c Client instance.
 * @param damage Poison damage to apply.
 */
void poison_player(struct client *c, int damage)
{
	/* As far as I know, you can't poison someone twice or overwrite
	   someone's poison.  */
	if (is_player_poisoned(c))
	{
		return;
	}

	/* Prevent player poisoning if they have an immunity running.  */
	if (is_player_poison_immune(c))
	{
		/* TODO I think there may be a message here that alerts the player
		 * if their anti-poison potion blocked a poison attempt.  */
		packet_send_chatbox_message(c, "Your antipoison blocked the poison!");
		return;
	}

	packet_send_chatbox_message(c, "You have been poisoned!");
	set_player_poison_damage(c, damage, 400);
}

/**
 * @brief Applies poison damage to an NPC.
 *
 * @param *npc NPC instance.
 * @param damage Poison damage to apply.
 */
void poison_npc(struct npc_spawn *npc, int damage)
{
	/* Don't repoison enemies.  */
	if (npc->poison_damage > 0)
	{
		return;
	}
	npc->poison_damage = damage;
	npc->poison_timer = 400;
}

/**
 * Cures a player of poison, allows setting of time until player can be
 * poisoned again.
 * @param *c Client instance.
 * @param immunity_time Ticks until the player can be poisoned again.
 */
void cure_poison_player(struct client *c, int immunity_time)
{
	if (c->plr->poison_damage != -1)
	{
		set_player_poison_damage(c, -1, -1);
		packet_send_chatbox_message(c, "The potion cures you of your poison.");
	}

	if (immunity_time != -1)
	{
		c->plr->poison_immunity_timer = immunity_time;
		packet_send_chatbox_message(c, "The potion temporarily protects you from poison.");
	}
}

/**
 * Applies a frozen debuff to a player.
 * @param *c Client instance.
 * @param type Type of freeze to apply. @see COMBAT_EFFECTS.
 * @param duration Ticks to be frozen for.
 */
void freeze_player(struct client *c, int type, int duration)
{
	c->plr->frozen_type = type;
	c->plr->frozen_ticks = duration;

	switch (type)
	{
		case EFFECT_FREEZE_STUN:
			packet_send_chatbox_message(c, "You've been stunned!");
			player_draw_gfx(c, 80);
			break;
		case EFFECT_FREEZE_MAGICAL:
			packet_send_chatbox_message(c, "A magical force stops you from moving!");
			break;
	}
}

/**
 * @brief Deal damage to player, can set delay, effect.
 *
 * @param *c Client instance to deal damage to.
 * @param amount Amount of hitpoints damage to deal.
 * @param damage_type What kind of damage is being dealt, melee, ranged, magic,
 * or other. Used in protection prayer handling.
 * @param delay Delay in server ticks until damage is dealt.
 * @param effect Effect id of the damage. 
 * @param is_player If the attacker is a player or an npc.
 * @param index Index of the attacker in the npc_spawns or players arrays.
 *
 * @todo Replace the effect argument with an array and add another argument for
 * the size of the array. Multiple effects may need to be handled at one time.
 *
 * @return -1 on failure, anything else on success.
 */
int deal_damage_to_player(struct client *c, int amount, int damage_type, 
						  int delay, int effect, int is_player, int index) 
{
	/* Ensure that damage done cannot be more than the player's current health.
	 */
	if (c->plr->level[HITPOINTS_SKILL] < amount)
	{
		amount = c->plr->level[HITPOINTS_SKILL];
	}

	/* Check for damage types and protection prayer handling.  */
	if (damage_type == DMG_TYPE_SLASH || damage_type == DMG_TYPE_CRUSH 
		|| damage_type == DMG_TYPE_STAB)
	{
		if (c->plr->prayers_active[PRAYER_PROTECT_FROM_MELEE] == 1)
		{
			if (is_player == 0)
			{
				amount = 0;
			}
			else
			{
				amount = (int) ((float) amount * 0.60);
			}
		}
	}
	else if (damage_type == DMG_TYPE_RANGE)
	{
		if (c->plr->prayers_active[PRAYER_PROTECT_FROM_MISSILES] == 1)
		{
			if (is_player == 0)
			{
				amount = 0;
			}
			else
			{
				amount = (int) ((float) amount * 0.60);
			}
		}
	}
	else if (damage_type == DMG_TYPE_MAGIC)
	{
		if (c->plr->prayers_active[PRAYER_PROTECT_FROM_MAGIC] == 1)
		{
			if (is_player == 0)
			{
				amount = 0;
			}
			else
			{
				amount = (int) ((float) amount * 0.60);
			}
		}
	}

	/* If nothing in the queue, add to first slot.  */
	if (c->plr->highest_hit_offset == -1)
	{
		c->plr->hit_queued[0] = amount;
		c->plr->hit_delay[0] = delay;
		c->plr->hit_effect[0] = effect;
		c->plr->hit_from[0] = is_player;
		c->plr->hit_index[0] = index;
		c->plr->highest_hit_offset = 0;
	}
	else
	{
		/* Check if there is space left for another hit.  */
		if ((c->plr->highest_hit_offset + 1) >= HIT_QUEUE_SIZE)
		{
			printf("Too many in queue, can't add!\n");
			printf("	Increase hit_delay queue size!\n");
			return -1;
		}
		else
		{
			c->plr->highest_hit_offset++;
			c->plr->hit_queued[c->plr->highest_hit_offset] = amount;
			c->plr->hit_delay[c->plr->highest_hit_offset] = delay;
			c->plr->hit_effect[c->plr->highest_hit_offset] = effect;
			c->plr->hit_from[c->plr->highest_hit_offset] = is_player;
			c->plr->hit_index[c->plr->highest_hit_offset] = index;
		}
	}

	/* Make sure player is treated as in combat.  */
	c->plr->last_combat_time = COMBAT_LOGOUT_TICKS;

	return c->plr->highest_hit_offset;
}

/**
 * @brief Deal damage to npc, can set delay, effect.
 *
 * @param *npc NPC instance to deal damage to.
 * @param amount Amount of hitpoints damage to deal.
 * @param delay Delay in server ticks until damage is dealt.
 * @param effect Effect id of the damage.
 * @param is_player If the attacker is a player or an npc.
 * @param index Index of the attacker in the npc_spawns or players arrays.
 *
 * @return -1 on failure, anything else on success.
 */
int deal_damage_to_npc(struct npc_spawn *npc, int amount, int delay, 
					   int effect, int is_player, int index)
{
	if (npc->health < amount)
	{
		amount = npc->health;
	}

	/* If nothing in the queue, add to first slot.  */
	if (npc->highest_hit_offset == -1)
	{
		npc->hit_queued[0] = amount;
		npc->hit_delay[0] = delay;
		npc->hit_effect[0] = effect;
		npc->hit_from[0] = is_player;
		npc->hit_index[0] = index;
		npc->highest_hit_offset = 0;
	}
	else
	{
		/* Check if there is space left for another hit.  */
		if ((npc->highest_hit_offset + 1) >= HIT_QUEUE_SIZE)
		{
			printf("Too many in queue, can't add!\n");
			printf("	Increase hit_delay queue size!\n");
			return -1;
		}
		else
		{
			npc->highest_hit_offset++;
			npc->hit_queued[npc->highest_hit_offset] = amount;
			npc->hit_delay[npc->highest_hit_offset] = delay;
			npc->hit_effect[npc->highest_hit_offset] = effect;
			npc->hit_from[npc->highest_hit_offset] = is_player;
			npc->hit_index[npc->highest_hit_offset] = index;
		}
	}

	return npc->highest_hit_offset;
}

/**
 * Heal player for specified amount of hitpoints. Ensure that it doesn't
 * overheal.  
 * @param *c Client instance.
 * @param heal_amount Number of hitpoints to heal player for.
 */
void heal_player(struct client *c, int heal_amount)
{
	int current_health = c->plr->level[HITPOINTS_SKILL];
	int max_health = get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]);

	if ((heal_amount + current_health) > max_health)
	{
		heal_amount = (max_health - current_health); 
	}

	c->plr->level[HITPOINTS_SKILL] += heal_amount;
	packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
}

/**
 * Restores a specified amount of run energy. Ensures that it doesn't overheal.
 * @param *c Client instance.
 * @param restore_amount Energy points to restore.
 */
void restore_run_energy(struct client *c, int restore_amount)
{
	if ((c->plr->running_energy + restore_amount) > 100)
	{
		restore_amount = (100 - c->plr->running_energy);
	}

	c->plr->running_energy += restore_amount;
	
	packet_send_run_energy(c);
}

/**
 * Restores the specified amount of points to a specific skill. Prevents
 * over-restore.
 * @param *c Client instance.
 * @param skill_id ID of the skill to restore points for.
 * @param restore_amount Number of skill points to restore.
 */
void restore_skill_points(struct client *c, int skill_id, int restore_amount)
{
	int current_level = c->plr->level[skill_id];
	int max_level = get_level_for_xp(c->plr->xp[skill_id]);

	if ((restore_amount + current_level)
		> max_level)
	{
		restore_amount = (max_level - current_level); 
	}

	c->plr->level[skill_id] += restore_amount;
	packet_send_updated_skill_stats(c, skill_id);
}

/**
 * @brief Adds the correct amount of xp for melee/ranged combat based on 
 * attack style, damage done.
 *
 * @param *c Client instance.
 * @param damage_done Damage that the client did to something, xp gain is based
 * on this.
 */
void send_combat_xp(struct client *c, int damage_done)
{
	switch (c->plr->fighting_style)
	{
		/* Accurate.  */
		case ACCURATE_STYLE:
			add_skill_xp(c, ATTACK_SKILL, (damage_done * 4.0));
			break;
		/* Aggressive.  */
		case AGGRESSIVE_STYLE:
			add_skill_xp(c, STRENGTH_SKILL, (damage_done * 4.0));
			break;
		/* Defensive.  */
		case DEFENSIVE_STYLE:
			add_skill_xp(c, DEFENCE_SKILL, (damage_done * 4.0));
			break;
		/* Controlled.  */
		case CONTROLLED_STYLE:
			add_skill_xp(c, ATTACK_SKILL, (damage_done * 1.33));
			add_skill_xp(c, STRENGTH_SKILL, (damage_done * 1.33));
			add_skill_xp(c, DEFENCE_SKILL, (damage_done * 1.33));
			break;
		/* Accurate (Ranged).  */
		case ACCURATE_STYLE_RANGE:
			add_skill_xp(c, RANGED_SKILL, (damage_done * 4.0));
			break;
		/* Rapid (Ranged).  */
		case RAPID_STYLE_RANGE:
			add_skill_xp(c, RANGED_SKILL, (damage_done * 4.0));
			break;
		/* Longrange.  */
		case LONGRANGED_STYLE_RANGE:
			add_skill_xp(c, RANGED_SKILL, (damage_done * 2.0));
			add_skill_xp(c, DEFENCE_SKILL, (damage_done * 2.0));
			break;
	}

	add_skill_xp(c, HITPOINTS_SKILL, (damage_done * 1.33));
}

/**
 * @brief Sends the correct xp gain for using spells, damage done from spell.
 *
 * @param *c Client instance.
 * @param damage_done Damage that the client's spell did.
 * @param spell_id Spell the client used.
 *
 * @note Xp gain is based off of a spell's base xp value, plus damage done * 2.
 */
void send_magic_xp(struct client *c, int damage_done, int spell_id)
{
	add_skill_xp(c, MAGIC_SKILL, (SPELLS[spell_id]->xp + (damage_done * 2)));
	add_skill_xp(c, HITPOINTS_SKILL, (damage_done * 1.33));
}

int is_player_using_magic_attacks(struct client *c)
{
	if (c->plr->cast_spell_id != 0 
		|| (c->plr->autocast_spell_id != 0 && c->plr->autocasting == 1))
	{
		return 1;
	}

	return 0;
}

int is_player_using_ranged_attacks(struct client *c)
{
	if (c->plr->fighting_style > 3 && is_player_using_magic_attacks(c) == 0)
	{
		return 1;
	}

	return 0;
}

int is_player_using_melee_attacks(struct client *c)
{
	if (c->plr->fighting_style <= 3 && is_player_using_magic_attacks(c) == 0)
	{
		return 1;
	}

	return 0;
}

/**
 * Draws the ranged weapons such as bows, crossbows, throwing axes, etc and 
 * loads it with ammo to fire.
 * @param *c Client instance.
 */
void draw_ranged_weapon(struct client *c)
{
	int ammo_type = ITEMS[c->plr->equipment[WEAPON_SLOT]]->ammo_type;

	/* Throwing weapons, ammo is in weapon slot.  */
	if (ammo_type == 3)
	{
		player_draw_gfx(c, ITEMS[c->plr->equipment[WEAPON_SLOT]]->draw_gfx);
		
		c->plr->projectile_id = c->plr->equipment[WEAPON_SLOT];
		delete_item_from_equipment_slot(c, WEAPON_SLOT, 1);
	}
	
	/* Weapons where ammo is in the ammo slot.  */
	else if (ammo_type > 3 && ammo_type < 40)
	{
		/* If using ogre arrows, use adjusted height for draw animation.  */
		if (c->plr->equipment[AMMO_SLOT] == 2866)
		{
			player_draw_gfx_at_height(c, 
							ITEMS[c->plr->equipment[AMMO_SLOT]]->draw_gfx, 50);
		}
		else
		{
			player_draw_gfx(c, ITEMS[c->plr->equipment[AMMO_SLOT]]->draw_gfx);
		}
		
		c->plr->projectile_id = c->plr->equipment[AMMO_SLOT];
		delete_item_from_equipment_slot(c, AMMO_SLOT, 1);
	}
	
	else
	{
		printf("UNHANDLED AMMO TYPE!\n");
	}
}

/**
 * @brief Player attacks an npc, changes all needed variables, deals damage,
 * uses ammo, etc.
 *
 * @param *c Client instance, attacker.
 * @param *npc NPC instance, target.
 *
 * @todo Add pursuit of npcs if retreating.
 */
void player_attack_npc(struct client *c, struct npc_spawn *npc)
{
	float npc_guard = 0, player_hit = 0;
	int ammo_result = 0, spell_id = 0;

	int damage_done = 0, damage_delay = 0, damage_effect = -1;
	int distance = get_distance_from_point(c->plr->world_x, c->plr->world_y, 
										   npc->world_x, npc->world_y);
	int player_attack_range = get_player_weapon_reach(c);

	int npc_size = NPCS[npc->npc_id]->size;
	int within_range = 0, clear_shot = 0;

	/* Don't attack until the attack cooldown has expired.  */
	if (c->plr->last_attack != 0)
	{
		return;
	}

	within_range = is_within_combat_range(npc->world_x, npc->world_y, 
										   npc_size, c->plr->world_x, 
										   c->plr->world_y, 
										   player_attack_range);
	/* Make sure the player only attacks when there is a clear shot.  */
	clear_shot = can_range_in_direction(c->plr->world_x, c->plr->world_y, 
										npc->world_x, npc->world_y, 
										c->plr->world_z);

	/* If player is within the npc's spaces.  */
	if (within_range == 2)
	{
		c->plr->destination_movement_needs_updating = 1;

		printf("Moving to non overlapping spot.\n");
		return;
	}

	/* Add checking for multicombat zones, if another player is already
	   attacking the npc, etc.  */
	ammo_result = get_player_weapon_has_ammo(c);

	if (within_range == 1 && ammo_result == 1 && clear_shot == 1)
	{
		printf("start attack\n");
		/* Check if using a magic attack, if so handle that.  */
		if (is_player_using_magic_attacks(c))
		{
			printf("magic attack\n");
			if (c->plr->cast_spell_id != 0)
			{
				spell_id = c->plr->cast_spell_id;
			}
			else
			{
				spell_id = c->plr->autocast_spell_id;
			}

			c->plr->cast_spell_index = get_spell_index(spell_id);
			player_play_animation(c, SPELLS[c->plr->cast_spell_index]->cast_emote);

			player_hit = rand_int(1, get_player_attacking_roll(c));
			npc_guard = rand_int(1, calculate_npc_guard_chance(npc, c->plr->damage_type));
		}
		else
		{
			player_play_animation(c, c->plr->attacking_emote);
			player_hit = rand_int(1, get_player_attacking_roll(c));
			npc_guard = rand_int(1, calculate_npc_guard_chance(npc, c->plr->damage_type));
		}

		/* Checks if the player hits, then apply damage.  */
		if (player_hit > npc_guard)
		{
			/* Play player hit sound effect if damage is dealt.  */
			packet_play_sfx(c, get_player_attack_sounds(c), 50, -1);

			calculate_max_hit(c);
			
			/* If not using a spell.  */
			if (spell_id == 0)
			{
				damage_done = rand_int(1, c->plr->max_hit);
				send_combat_xp(c, damage_done);
			}
			else
			{
				if (SPELLS[c->plr->cast_spell_index]->max_dmg != 0)
				{
					damage_done = rand_int(1, 
						SPELLS[c->plr->cast_spell_index]->max_dmg);
				}
				
				npc->hit_gfx = SPELLS[c->plr->cast_spell_index]->impact_gfx;
				npc->hit_gfx_height 
					= SPELLS[c->plr->cast_spell_index]->impact_gfx_height;

				damage_effect = SPELLS[c->plr->cast_spell_index]->spell_effect;
				send_magic_xp(c, damage_done, c->plr->cast_spell_index);
			}
			npc->need_hp_update = 1;
		}
		/* If attack misses.  */
		else
		{
			/* If spell didn't hit, play miss gfx and add base spell xp.  */
			if (spell_id != 0)
			{
				send_magic_xp(c, 0, c->plr->cast_spell_index);
				npc->hit_gfx = 339;
				npc->hit_gfx_height = 100;
			}

			/* Play NPC blocking sound if attack misses.  */
			packet_play_sfx(c, get_npc_block_sound(npc->npc_id), 50, -1);
		}
		
		/* Move this if block to its own function?  */
		if (is_player_using_magic_attacks(c))
		{
			player_draw_gfx_at_height(c, SPELLS[c->plr->cast_spell_index]->cast_gfx,
							SPELLS[c->plr->cast_spell_index]->cast_gfx_height);
			
			c->plr->projectile_type
				= SPELLS[c->plr->cast_spell_index]->projectile_gfx;

			c->plr->projectile_delay = 0;
			damage_delay = get_magic_hit_delay(distance);

			/* Sets last action time so that damage by spells are not
			   overridden when also attacking with a ranged weapon.  */
			c->plr->last_attack = 5;
			npc->hit_gfx_delay = damage_delay;

			/* This will create the proper projectile for the weapon used.  */
			player_shoot_npc(c, distance, NPCS[npc->npc_id]->size);
		}
		/* If using a bow.  */
		else if (is_player_using_ranged_attacks(c))
		{
			/* Does not handle crystal bow yet.  */
			draw_ranged_weapon(c);
			c->plr->last_attack = get_player_weapon_speed(c);

			c->plr->projectile_delay = 0;
			c->plr->projectile_type = 0;
			damage_delay = get_ranged_hit_delay(distance);
			
			/* This will create the proper projectile for the weapon used.  */
			player_shoot_npc(c, distance, NPCS[npc->npc_id]->size);
		}
		else
		{
			damage_delay = 0;
			c->plr->last_attack = get_player_weapon_speed(c);
		}

		if (npc->in_combat == 0)
		{
			npc->targeted_player = c->plr->index;
			npc->walks = 0;
		}
		
		c->plr->update_required = 1;
		c->plr->update_masks[APPEARANCE_UPDATE] = 1;

		/* FIXME: If a magic spell misses, does it show a hitsplat?  */
		deal_damage_to_npc(npc, damage_done, damage_delay, damage_effect, 1, 
						   c->plr->index);
		player_face_npc(c, npc->index);

		c->plr->last_combat_time = COMBAT_LOGOUT_TICKS; 

		c->plr->cast_spell_id = 0;
	}
	else if (ammo_result == 0)
	{
		set_player_state(c, STATE_IDLE);

		if (is_player_using_ranged_attacks(c))
		{
			packet_send_chatbox_message(c, "I don't have any ammo left!");
		}
		return;
	}
	else if (ammo_result == 2)
	{
		packet_send_chatbox_message(c, "I can't use that type of ammo!");
		set_player_state(c, STATE_IDLE);
		return;
	}
	else
	{
		if (is_player_weapon_ranged(c) == 1)
		{
			packet_send_chatbox_message(c, "I'm too far away.");
			set_player_state(c, STATE_IDLE);
			return;
		}
	}
}

/**
 * Checks if the NPC's target has moved out of range and should be pursued.
 * @param *c Player that is targeted.
 * @param *npc NPC that is attacking.
 * @return Returns 1 if within range, 0 if not.
 *
 * @todo May need to apply adjustments for larger NPC sizes.
 */
int is_npc_is_within_combat_range_with_player(struct client *c, 
										   struct npc_spawn *npc)
{
	int npc_size = NPCS[npc->npc_id]->size;
	int within_range = 0;
	int distance = 0;
	int diagonal = 0;

	/* Skip diagonal checking for large npcs.  */
	if (npc_size > 1)
	{
		diagonal = 0;
	}
	else
	{
		diagonal = is_diagonal_from_target(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);
	}

	if (npc->attack_style == NPC_USE_MELEE)
	{
		within_range = is_within_combat_range(npc->world_x, npc->world_y, 
										   npc_size,
										   c->plr->world_x, c->plr->world_y, 
										   npc->melee_range);

		npc->target_x = c->plr->world_x;
		npc->target_y = c->plr->world_y;

		if (within_range == 0 || diagonal != 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else if (npc->attack_style == NPC_USE_RANGE)
	{
		distance = get_distance_from_point(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);

		npc->target_x = c->plr->world_x;
		npc->target_y = c->plr->world_y;

		if (distance > npc->max_ranged
			|| can_range_in_direction(npc->world_x, npc->world_y, 
								 c->plr->world_x, c->plr->world_y, 
								 npc->world_z) == 0)
		{
			/* TODO: Double check these, do they need size adjustments like
			 * melee movement?  */
			printf("Out of range! Get closer!\n");
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else if (npc->attack_style == NPC_USE_MAGIC)
	{
		distance = get_distance_from_point(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);

		npc->target_x = c->plr->world_x;
		npc->target_y = c->plr->world_y;

		if (distance > npc->magic_range
			|| can_range_in_direction(npc->world_x, npc->world_y, 
								 c->plr->world_x, c->plr->world_y, 
								 npc->world_z) == 0)
		{
			/* TODO: Double check these, do they need size adjustments like
			 * melee movement?  */
			printf("Out of range! Get closer!\n");
			return 0;
		}
		else
		{
			return 1;
		}
	}

	return 1;
}

/**
 * Handles moving an NPC to a player target when needed.
 * @param *c Client instance, player targeted.
 * @param *npc NPC to move.
 */
void npc_move_to_player_target(struct client *c, struct npc_spawn *npc)
{
	int npc_size = NPCS[npc->npc_id]->size;
	int within_range = 0;
	int found = 0;
	int distance = 0;
	int diagonal = 0;
	int e_x = 0;
	int e_y = 0;

	/* Skip diagonal checking for large npcs.  */
	if (npc_size > 1)
	{
		diagonal = 0;
	}
	else
	{
		diagonal = is_diagonal_from_target(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);
	}

	if (npc->attack_style == NPC_USE_MELEE)
	{
		within_range = is_within_combat_range(npc->world_x, npc->world_y, 
										   npc_size,
										   c->plr->world_x, c->plr->world_y, 
										   npc->melee_range);

		if (within_range == 0 || diagonal != 0)
		{
			/* e_x = enemy_x.  */
			e_x = npc->world_x;
			e_y = npc->world_y;

			found = 
				get_closest_perpendicular_square_around_entity(c->plr->world_x, 
												 c->plr->world_y,
												 1,
												 1,
												 &e_x, &e_y, 
												 npc->world_z);

			/* If a spot has been found, move to it.  */
			if (found == 0)
			{
				/* Adjust travel for npc size.  */
				if (npc_size > 1)
				{
					adjust_large_npc_path(npc->world_x, npc->world_y, 
										  npc_size, &e_x, &e_y);
				}

				set_npc_path(npc, npc->world_x, npc->world_y, e_x, e_y, 
							 npc->world_z, 1);
			}
		}
	}
	else if (npc->attack_style == NPC_USE_RANGE)
	{
		distance = get_distance_from_point(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);

		if (distance > npc->max_ranged
			|| can_range_in_direction(npc->world_x, npc->world_y, 
								 c->plr->world_x, c->plr->world_y, 
								 npc->world_z) == 0)
		{
			/* TODO: Double check these, do they need size adjustments like
			 * melee movement?  */
			printf("Out of range! Get closer!\n");
			set_npc_path(npc, npc->world_x, npc->world_y, c->plr->world_x,
						 c->plr->world_y, npc->world_z, 0);
		}
	}
	else if (npc->attack_style == NPC_USE_MAGIC)
	{
		distance = get_distance_from_point(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);

		if (distance > npc->magic_range
			|| can_range_in_direction(npc->world_x, npc->world_y, 
								 c->plr->world_x, c->plr->world_y, 
								 npc->world_z) == 0)
		{
			/* TODO: Double check these, do they need size adjustments like
			 * melee movement?  */
			printf("Out of range! Get closer!\n");
			set_npc_path(npc, npc->world_x, npc->world_y, c->plr->world_x,
						 c->plr->world_y, npc->world_z, 0);
		}
	}

	npc->walking_to_point = 1;
	npc->pursue_target = 0;
}

static void npc_pick_attack_style(struct npc_spawn *npc)
{
	/* Figure out what attack style out of the ones available to the npc that
	   he will use.  */
	if (npc->melee_stats[ATK_ANIM] != 0)
	{
		/* NPC has melee attack, but also has spells. Use the 
		 * chance_to_use_magic variable to see if the NPC uses a spell or
		 * chooses to use their melee attack. Many NPCs only use their spells,
		 * and some NPCs like dragons use either melee or magic at times.  */
		if (npc->num_spells > 0)
		{
			if (rand_int(0, 100) <= npc->chance_to_use_magic)
			{
				printf("NPC WILL USE A SPELL\n");
				npc->attack_style = NPC_USE_MAGIC;
				npc->damage_type = DMG_TYPE_MAGIC;
			}
			else
			{
				printf("NPC WILL USE MELEE\n");
				npc->attack_style = NPC_USE_MELEE;
				npc->damage_type = npc->melee_stats[DMG_TYPE];
			}
		}
		else
		{
			npc->attack_style = NPC_USE_MELEE;
			npc->damage_type = npc->melee_stats[DMG_TYPE];
		}
	}
	else if (npc->ranged_stats[ATK_ANIM] != 0)
	{
		printf("NPC WILL USE RANGED\n");
		npc->attack_style = NPC_USE_RANGE;
		npc->damage_type = npc->ranged_stats[DMG_TYPE];
	}
	else
	{
		printf("NPC WILL USE MAGIC\n");
		npc->attack_style = NPC_USE_MAGIC;
		npc->damage_type = DMG_TYPE_MAGIC;
	}

}

/**
 * @brief NPC attacks player, handles all variables, attacks, damage, etc.
 *
 * @param *c Client instance, target.
 * @param *npc NPC instance, attacker.
 *
 * @todo Clean this up a lot.
 * @todo Change NPC attack style choice to be based on the distance from the
 * player. If an NPC has magic and melee, use magic only when player is out
 * of melee range. Add a flag to spells to allow some to be used in close 
 * range, like dragonfire.
 */
void npc_attack_player(struct client *c, struct npc_spawn *npc)
{
	/* Sanity check.  */
	if (c == NULL)
	{
		printf("PLAYER LOGGED OUT! EXIT COMBAT!\n");
		reset_npc_combat_move_to_home(npc);
		return;	
	}

	/* TODO: Add npc damage effect handling.  */
	float npc_hit = 0, player_guard = 0;
	float npc_c = 0, player_c = 0;

	int damage_done = -1, damage_delay = -1, damage_effect = -1;
	int distance = get_distance_from_point(npc->world_x, npc->world_y, 
											c->plr->world_x, c->plr->world_y);
	int within_range = 0;
	int npc_size = NPCS[npc->npc_id]->size;
	int diagonal = is_diagonal_from_target(npc->world_x, npc->world_y, 
										   c->plr->world_x, c->plr->world_y);
	
	/* NPC has killed player, return to normal functions.  */
	if (c->plr->level[HITPOINTS_SKILL] < 1)
	{
		reset_npc_combat_move_to_home(npc);
		return;
	}

	/* NPC select what kind of attack they will do, based on what attacks they
	 * are able to do, and range.  */
	npc_pick_attack_style(npc);

	/** @todo Add checking for multicombat zones, if another player is already
	 * attacking the npc, etc.  */
	if (is_npc_dead(npc) == 0 && npc->retaliates == 1)
	{
		/* Melee.  */
		if (npc->attack_style == NPC_USE_MELEE)
		{
			/* Check if within range to attack.  */
			within_range = is_within_combat_range(npc->world_x, npc->world_y, 
											   npc_size,
											   c->plr->world_x,
											   c->plr->world_y, 
											   npc->melee_range);

			/* Don't bother with diagonal checking if npc is large, don't think
			   it is needed here.  */
			if (npc_size > 1)
			{
				diagonal = 0;
			}

			/* XXX: > 0 allows attacking even if player is "within" their
			   space, == 1 doesn't.  */
			/* Also make sure that NPCs can't melee through walls as well.  */
			if (within_range > 0 && diagonal == 0
				&& can_range_in_direction(npc->world_x, npc->world_y,
								 c->plr->world_x, c->plr->world_y, 
								 npc->world_z) == 1)
			{
				set_npc_animation(npc, npc->melee_stats[ATK_ANIM]);
				packet_play_sfx(c, get_npc_attack_sound(npc->npc_id), 50, -1);
				npc->last_attack = npc->melee_stats[ATK_SPEED];
				
				npc->damage_type = npc->melee_stats[DMG_TYPE];
				player_guard = calculate_pve_guard_chance(c, npc);
				npc_hit = calculate_npc_hit_chance(npc);
				player_c = rand_int(0, player_guard);
				npc_c =  rand_int(0, npc_hit);

				/* If damage is done, set it.  */
				if (npc_c > player_c)
				{
					damage_done = rand_int(1, npc->melee_stats[MAX_HIT]);
				}
				else
				{
					damage_done = 0;
				}

				damage_delay = 0;
			}
			else
			{
				if (npc->pursue_target == 0)
				{
					npc->pursue_target = 1;
				}
			}
		}
		/* Ranged.  */
		else if (npc->attack_style == NPC_USE_RANGE)
		{
			/* Make sure player is not out of the max range to attack.  */
			if (distance <= npc->max_ranged
				&& can_range_in_direction(npc->world_x, npc->world_y, 
									 c->plr->world_x, c->plr->world_y, 
									 npc->world_z) == 1)
			{
				/* Draw arrow/bolt.  */
				set_npc_animation(npc, npc->ranged_stats[ATK_ANIM]);
				if (npc->ranged_stats[ATK_GFX] != 0)
				{
					set_npc_gfx(npc, npc->ranged_stats[ATK_GFX]);
				}
				/*
				npc->projectile_delay = 1;
				*/
				npc->projectile_type = npc->ranged_projectile_id;
				npc_shoot_player(npc, distance);
				
				damage_delay = get_ranged_hit_delay(distance);
				npc->last_attack = npc->ranged_stats[ATK_SPEED];
				npc->damage_type = DMG_TYPE_RANGE;
				
				player_guard = calculate_pve_guard_chance(c, npc);
				npc_hit = calculate_npc_hit_chance(npc);
				player_c = rand_int(0, player_guard);
				npc_c =  rand_int(0, npc_hit);

				if (npc_c > player_c)
				{
					damage_done = rand_int(1, npc->ranged_stats[MAX_HIT]);
				}
				else
				{
					damage_done = 0;
				}
			}
			else
			{
				if (npc->pursue_target == 0)
				{
					npc->pursue_target = 1;
				}
			}
		}
		/* Magic.  */
		else if (npc->attack_style == NPC_USE_MAGIC)
		{
			/* Get a random spell to use.  */
			int magic_index = get_npc_random_spell_index(npc);

			printf("magic idx: %d\n", magic_index);

			/* If spell exists...  */
			if (magic_index != -1)
			{
				if (distance <= npc->magic_range
					&& can_range_in_direction(npc->world_x, npc->world_y, 
										 c->plr->world_x, c->plr->world_y, 
										 npc->world_z) == 1)
				{
					/* Set npc animations and gfx.  */
					set_npc_animation(npc, SPELLS[magic_index]->cast_emote);

					/* If we need to play a cast gfx too, do so.  */
					if (SPELLS[magic_index]->cast_gfx != 0)
					{
						set_npc_gfx_height(npc, SPELLS[magic_index]->cast_gfx,
									   SPELLS[magic_index]->cast_gfx_height);
					}

					/* Set time when projectile is created, what gfx it 
					   uses.  */
					npc->projectile_type = SPELLS[magic_index]->projectile_gfx;
					npc_shoot_player(npc, distance);
					
					/* Setup time to hit, time to display gfx.  */
					damage_delay = get_magic_hit_delay(distance);

					c->plr->hit_gfx = SPELLS[magic_index]->impact_gfx;
					c->plr->hit_gfx_delay = (damage_delay - 1);
					c->plr->hit_gfx_height 
						= SPELLS[magic_index]->impact_gfx_height;

					npc->last_attack = get_higher(damage_delay, 5);

					npc->damage_type = DMG_TYPE_MAGIC;

					/* Calc guard and hit chance.  */
					player_guard = calculate_pve_guard_chance(c, npc);
					npc_hit = calculate_npc_hit_chance(npc);

					player_c = rand_int(0, player_guard);
					npc_c =  rand_int(0, npc_hit);
					
					/* These two damage ids are used for dragonfire and strong
					   dragonfire, respectively.  */
					if (SPELLS[magic_index]->dmg_type == DMG_ELEMENT_DFIRE
						|| SPELLS[magic_index]->dmg_type == DMG_ELEMENT_SDFIRE)
					{
						printf("Hit by dragonfire!\n");
						/* TODO: Place dragonfire damage handling here.  */
					}

					/* If npc rolls a hit on the player, do damage. Else,
					   it is a miss.  */
					if (npc_c > player_c)
					{
						if (SPELLS[magic_index]->max_dmg != -1)
						{
							/* TODO: Kalphite queen hits from 0 - 31, need to
							 * specially handle that later.  */
							damage_done 
								= rand_int(1, SPELLS[magic_index]->max_dmg);
						}
						else
						{
							damage_done = 0;
						}
					}
					else
					{
						damage_done = 0;
					}
				}
				else
				{
					if (npc->pursue_target == 0)
					{
						npc->pursue_target = 1;
					}
				}
			}
			else
			{
				printf("ERROR! SPELL DOESN'T EXIST! -1 WAS RETURNED!\n");
			}
		}

		if (is_player_in_combat(c) == 0)
		{
			c->plr->targeted_npc = npc->index;
		}

		c->plr->last_combat_time = COMBAT_LOGOUT_TICKS; 
	}

	if (damage_done != -1)
	{
		deal_damage_to_player(c, damage_done, npc->damage_type, damage_delay, 
							  damage_effect, 0, npc->index);
	}

	/* Face the player if we aren't already.  */
	if (npc->face_mob == -1)
	{
		npc_face_player(npc, (c->plr->index + 1));
	}
}
