/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file interface_handler.c
 * @brief Contains handle_interface_clicked, processes all interface actions.
 */
#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "audio.h"
#include "dialogue_handler.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
#include "skill_magic.h"
#include "skill_prayer.h"

/**
 * Handles actions when a player clicks on a interface.
 * @param *c Client instance.
 * @param interface_id Id of the interface clicked.
 */
void handle_interface_clicked(struct client *c, int interface_id)
{
	int not_sidebar = 0;

	/* If player's sidebar isn't hidden by something, allow handling of the
	 * regular interfaces.  */
	if (c->plr->sidebar_hidden == 0)
	{
		switch (interface_id)
		{
		/* Player interfaces.  */
			/* Auto retaliate toggle.  */
			case 150:
				c->plr->will_retaliate = 1;
				printf("Will retaliate.\n");
				break;
			case 151:
				c->plr->will_retaliate = 0;
				printf("No retaliation.\n");
				break;
			/* Accepts aid toggle.  */
			case 12590:
				c->plr->accepts_aid = 0;
				break;
			case 12591:
				c->plr->accepts_aid = 1;
				break;
			/* Walk button.  */
			case 152:
				c->plr->is_running = 0;
				break;
			/* Run button.  */
			case 153:
				if (c->plr->running_energy > 0)
				{
					c->plr->is_running = 1;
					c->plr->running_restore = 0;
				}
				else
				{
					packet_set_client_config_value(c, WALK_OR_RUN_MODE, 0);
				}
				break;
			/* Logout button is pressed.  */
			case 2458:
				if (c->plr->last_combat_time <= 0)
				{
					packet_player_disconnect(c);
					c->plr->logged_out = 1;
				}
				else
				{
					packet_send_chatbox_message(c, "You can't logout so soon after combat!");
				}
				break;
			/* Attack with autocasting button clicked.  */
			case 349:
				if (c->plr->autocast_spell_id == 0)
				{
					packet_send_chatbox_message(c, "You have no spell set to autocast!");
				}
				else if (c->plr->autocasting == 1)
				{
					packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 2);
					c->plr->autocasting = 0;
				}
				else
				{
					packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 3);
					c->plr->autocasting = 1;
				}
				break;
			/* Choose autocast spell is pressed.  */
			case 353:
				/* Modern magic menu.  */
				if (c->plr->spellbook == 0 
					&& c->plr->equipment[WEAPON_SLOT] != 4675)
				{
					packet_show_sidebar_interface(c, 0, 1829);
				}
				/* Ancient magic menu.  */
				else
				{
					if (c->plr->spellbook == 1 
						&& c->plr->equipment[WEAPON_SLOT] == 4675)
					{
						packet_show_sidebar_interface(c, 0, 1689);
					}
					else if (c->plr->equipment[WEAPON_SLOT] != 4675)
					{
						packet_send_chatbox_message(c, "This staff can't autocast ancient magicks!");
					}
					else
					{
						packet_send_chatbox_message(c, "You don't have the correct spellbook!");
					}
				}
				break;
		/* Emote buttons.  */
			/* Yes.  */
			case 168:
				player_play_animation(c, 855);
				break;
			/* No.  */
			case 169:
				player_play_animation(c, 856);
				break;
			/* Think.  */
			case 162:
				player_play_animation(c, 857);
				break;
			/* Bow.  */
			case 164:
				player_play_animation(c, 858);
				break;
			/* Angry.  */
			case 165:
				player_play_animation(c, 859);
				break;
			/* Cry.  */
			case 161:
				player_play_animation(c, 860);
				break;
			/* Laugh.  */
			case 170:
				player_play_animation(c, 861);
				break;
			/* Cheer.  */
			case 171:
				player_play_animation(c, 862);
				break;
			/* Wave.  */
			case 163:
				player_play_animation(c, 863);
				break;
			/* Beckon.  */
			case 167:
				player_play_animation(c, 864);
				break;
			/* Clap.  */
			case 172:
				player_play_animation(c, 865);
				break;
			/* Dance.  */
			case 166:
				player_play_animation(c, 866);
				break;
			/* Panic.  */
			case 13362:
				player_play_animation(c, 2105);
				break;
			/* Jig.  */
			case 13363:
				player_play_animation(c, 2106);
				break;
			/* Spin.  */
			case 13364:
				player_play_animation(c, 2107);
				break;
			/* Head bang.  */
			case 13365:
				player_play_animation(c, 2108);
				break;
			/* Joy jump.  */
			case 13366:
				player_play_animation(c, 2109);
				break;
			/* Raspberry.  */
			case 13367:
				player_play_animation(c, 2110);
				break;
			/* Yawn.  */
			case 13368:
				player_play_animation(c, 2111);
				break;
			/* Salute.  */
			case 13369:
				player_play_animation(c, 2112);
				break;
			/* Shrug.  */
			case 13370:
				player_play_animation(c, 2113);
				break;
			/* Blow kiss.  */
			case 11100:
				player_play_animation(c, 1368);
				break;
			/* XXX: Mime emotes are only unlocked after the random event.
			 * FIXME when random events are implemented.  */
			/* Glass Box.  */
			case 667:
				player_play_animation(c, 1131);
				break;
			/* Climb rope.  */
			case 6503:
				player_play_animation(c, 1130);
				break;
			/* Lean.  */
			case 6506:
				player_play_animation(c, 1129);
				break;
			/* Glass wall.  */
			case 666:
				player_play_animation(c, 1128);
				break;
			/* XXX: These are only unlocked during "The Lost Tribe" quest.
			 * FIXME when that quest is added.  */
			/* Goblin bow.  */
			case 13383:
				player_play_animation(c, 2127);
				break;
			/* Goblin dance.  */
			case 13384:
				player_play_animation(c, 2128);
				break;

		/* Picking autocast spells.  */
			/* MODERN SPELLS.  */
			/* Wind strike.  */
			case 1830:
				set_autocast_spell(c, 1152);
				break;
			/* Water strike.  */
			case 1831:
				set_autocast_spell(c, 1154);
				break;
			/* Earth strike.  */
			case 1832:
				set_autocast_spell(c, 1156);
				break;
			/* Fire strike.  */
			case 1833:
				set_autocast_spell(c, 1158);
				break;
			/* Wind bolt.  */
			case 1834:
				set_autocast_spell(c, 1160);
				break;
			/* Water bolt.  */
			case 1835:
				set_autocast_spell(c, 1163);
				break;
			/* Earth bolt.  */
			case 1836:
				set_autocast_spell(c, 1166);
				break;
			/* Fire bolt.  */
			case 1837:
				set_autocast_spell(c, 1169);
				break;
			/* Wind blast.  */
			case 1838:
				set_autocast_spell(c, 1172);
				break;
			/* Water blast.  */
			case 1839:
				set_autocast_spell(c, 1175);
				break;
			/* Earth blast.  */
			case 1840:
				set_autocast_spell(c, 1177);
				break;
			/* Fire blast.  */
			case 1841:
				set_autocast_spell(c, 1181);
				break;
			/* Wind wave.  */
			case 1842:
				set_autocast_spell(c, 1183);
				break;
			/* Water wave.  */
			case 1843:
				set_autocast_spell(c, 1185);
				break;
			/* Earth wave.  */
			case 1844:
				set_autocast_spell(c, 1188);
				break;
			/* Fire wave.  */
			case 1845:
				set_autocast_spell(c, 1189);
				break;
			/* ANCIENT SPELLS.  */
			/* Smoke rush.  */
			case 13189:
				set_autocast_spell(c, 12939);
				break;
			/* Shadow rush.  */
			case 13241:
				set_autocast_spell(c, 12987);
				break;
			/* Blood rush.  */
			case 13147:
				set_autocast_spell(c, 12901);
				break;
			/* Ice rush.  */
			case 6162:
				set_autocast_spell(c, 12861);
				break;
			/* Smoke burst.  */
			case 13215:
				set_autocast_spell(c, 12963);
				break;
			/* Shadow burst.  */
			case 13267:
				set_autocast_spell(c, 12987);
				break;
			/* Blood burst.  */
			case 13167:
				set_autocast_spell(c, 12919);
				break;
			/* Ice burst.  */
			case 13125:
				set_autocast_spell(c, 12881);
				break;
			/* Smoke blitz.  */
			case 13202:
				set_autocast_spell(c, 12951);
				break;
			/* Shadow blitz.  */
			case 13254:
				set_autocast_spell(c, 12999);
				break;
			/* Blood blitz.  */
			case 13158:
				set_autocast_spell(c, 12911);
				break;
			/* Ice blitz.  */
			case 13114:
				set_autocast_spell(c, 12871);
				break;
			/* Smoke barrage.  */
			case 13228:
				set_autocast_spell(c, 12975);
				break;
			/* Shadow barrage.  */
			case 13280:
				set_autocast_spell(c, 13028);
				break;
			/* Blood barrage.  */
			case 13178:
				set_autocast_spell(c, 12929);
				break;
			/* Ice barrage.  */
			case 13136:
				set_autocast_spell(c, 12891);
				break;
			/* IDS for teleport self to location.  */
			case 1164:
			case 1167:
			case 1170:
			case 1174:
			case 1540:
			case 1541:
			case 7455:
			case 13035:
			case 13045:
			case 13053:
			case 13061:
			case 13069:
			case 13079:
			case 13087:
			case 13095:
				cast_teleport_spell(c, interface_id);
				break;
			case 1159:
				cast_bones_to_bananas(c, interface_id);
				break;
			/* Close spell selection menu for ancient and modern magics.  */
			case 2004:
				/* FALL THROUGH.  */
			case 6161:
				apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
				break;
			/* Skill menus. TODO. */
			case 8654:
				send_skill_guide(c, ATTACK_SKILL, 0);
				break;
			case 8660:
				send_skill_guide(c, DEFENCE_SKILL, 0);
				break;
			case 8656:
				send_skill_guide(c, MINING_SKILL, 0);
				break;

			/* Skill guide section tabs.  */
			case 8846:
				send_skill_guide(c, c->plr->interface_open_sub, 0);
				break;
			case 8823:
				send_skill_guide(c, c->plr->interface_open_sub, 1);
				break;
			case 8824:
				send_skill_guide(c, c->plr->interface_open_sub, 2);
				break;
			case 8827:
				send_skill_guide(c, c->plr->interface_open_sub, 3);
				break;
			case 8837:
				send_skill_guide(c, c->plr->interface_open_sub, 4);
				break;
			case 8840:
				send_skill_guide(c, c->plr->interface_open_sub, 5);
				break;
			case 8843:
				send_skill_guide(c, c->plr->interface_open_sub, 6);
				break;
			case 8859:
				send_skill_guide(c, c->plr->interface_open_sub, 7);
				break;
			case 8862:
				send_skill_guide(c, c->plr->interface_open_sub, 8);
				break;
			case 8865:
				send_skill_guide(c, c->plr->interface_open_sub, 9);
				break;
		

		/* WEAPON RELATED IDS  */
			/* Slashing Attack IDS.  */
			case 782:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 783:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 1704:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 1705:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 1707:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 2284:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 2429:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 2430:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 2432:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 4688:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 4711:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 4712:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 4714:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 7768:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 7769:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 7771:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 8468:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 12296:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 12297:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 12298:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_SLASH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			/* Crushing Attack IDS.  */
			case 334:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 335:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 336:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 431:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 432:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 433:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 784:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 1706:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 3802:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 3803:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 3805:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 4687:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 4713:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 5578:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 5860:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote = 422;
				break;
			case 5861:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote = 422;
				break;
			case 5862:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_CRUSH;
				c->plr->attacking_emote = 423;
				break;

			/* Stabbing Attack IDS.  */
			case 785:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 2282:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 2283:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 2285:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 2431:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 3804:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 4685:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 4686:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 5576:
				c->plr->fighting_style = ACCURATE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 5577:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[3];
				break;
			case 5579:
				c->plr->fighting_style = AGGRESSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[1];
				break;
			case 7770:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
			case 8466:
				c->plr->fighting_style = CONTROLLED_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				break;
			case 8467:
				c->plr->fighting_style = DEFENSIVE_STYLE;
				c->plr->damage_type = DMG_TYPE_STAB;
				c->plr->attacking_emote 
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[2];
				break;
		/* Ranged weapons have attack speed changes based on their fighting style,
		 * note the attack_speed settings.  */
			/* Accurate ranged weapon anims.  */
			case 1772:
			case 1757:
			case 4454:
				c->plr->fighting_style = ACCURATE_STYLE_RANGE;
				c->plr->damage_type = DMG_TYPE_RANGE;
				c->plr->attacking_emote
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				c->plr->attack_speed = get_player_weapon_speed(c);
				break;
			/* Rapid ranged weapon anims.  */
			case 1771:
			case 1756:
			case 4453:
				c->plr->fighting_style = RAPID_STYLE_RANGE;
				c->plr->damage_type = DMG_TYPE_RANGE;
				c->plr->attacking_emote
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				c->plr->attack_speed = get_player_weapon_speed(c);
				break;
			/* Longranged weapon anims.  */
			case 1770:
			case 1755:
			case 4452:
				c->plr->fighting_style = LONGRANGED_STYLE_RANGE;
				c->plr->damage_type = DMG_TYPE_RANGE;
				c->plr->attacking_emote
				= ITEMS[c->plr->equipment[WEAPON_SLOT]]->attacking_emotes[0];
				c->plr->attack_speed = get_player_weapon_speed(c);
				break;
			/* Prayer icons.  */
			case 5609:
				handle_prayer(c, PRAYER_THICK_SKIN);
				break;
			case 5610:
				handle_prayer(c, PRAYER_BURST_OF_STRENGTH);
				break;
			case 5611:
				handle_prayer(c, PRAYER_CLARITY_OF_THOUGHT);
				break;
			case 5612:
				handle_prayer(c, PRAYER_ROCK_SKIN);
				break;
			case 5613:
				handle_prayer(c, PRAYER_SUPERHUMAN_STRENGTH);
				break;
			case 5614:
				handle_prayer(c, PRAYER_IMPROVED_REFLEXES);
				break;
			case 5615:
				handle_prayer(c, PRAYER_RAPID_RESTORE);
				break;
			case 5616:
				handle_prayer(c, PRAYER_RAPID_HEAL);
				break;
			case 5617:
				handle_prayer(c, PRAYER_PROTECT_ITEMS);
				break;
			case 5618:
				handle_prayer(c, PRAYER_STEEL_SKIN);
				break;
			case 5619:
				handle_prayer(c, PRAYER_ULTIMATE_STRENGTH);
				break;
			case 5620:
				handle_prayer(c, PRAYER_INCREDIBLE_REFLEXES);
				break;
			case 5621:
				handle_prayer(c, PRAYER_PROTECT_FROM_MAGIC);
				break;
			case 5622:
				handle_prayer(c, PRAYER_PROTECT_FROM_MISSILES);
				break;
			case 5623:
				handle_prayer(c, PRAYER_PROTECT_FROM_MELEE);
				break;
			case 683:
				handle_prayer(c, PRAYER_RETRIBUTION);
				break;
			case 684:
				handle_prayer(c, PRAYER_REDEMPTION);
				break;
			case 685:
				handle_prayer(c, PRAYER_SMITE);
				break;
			/* Buttons that affect clientside only.  */
			/* Screen Brightness.  */
			case 5452:
			case 6273:
			case 6275:
			case 6277:
			case 5451:
			case 6157:
			case 6274:
			case 6276:
			/* Mouse button setting.  */
			case 6278:
			case 6279:
			/* Chat effects.  */
			case 6280:
			case 6281:
			/* Split private chat.  */
			/* FIXME: May need to be actually handled later.  */
			case 952:
			case 953:
				break;
			/* Quest info.  */
			case 7332:
			case 7333:
			case 7334:
			case 7335:
			case 7336:
			case 7337:
			case 7338:
			case 7339:
			case 7340:
			case 7341:
			case 7342:
			case 7343:
			case 7344:
			case 7345:
			case 7346:
			case 7347:
			case 7348:
			case 7349:
			case 7350:
			case 7351:
			case 7352:
			case 7353:
			case 7354:
			case 673:
			case 7356:
			case 7357:
			case 7358:
			case 7359:
			case 7360:
			case 7361:
			case 7362:
			case 7363:
			case 11907:
			case 7365:
			case 7366:
			case 7367:
			case 7368:
			case 7369:
			case 7370:
			case 7371:
			case 7372:
			case 7373:
			case 7374:
			case 7375:
			case 7376:
			case 7378:
			case 7379:
			case 7380:
			case 7381:
			case 9927:
			case 7383:
			case 7459:
			case 7355:
			case 8115:
			case 8137:
			case 8438:
			case 191:
			case 8679:
			case 1740:
			case 11857:
			case 8576:
			case 8969:
			case 6518:
			case 10115:
			case 11132:
			case 12139:
			case 7364:
			case 10135:
			case 11858:
			case 12129:
			case 12345:
			case 12389:
			case 12282:
			case 12772:
			case 12836:
			case 12839:
			case 12852:
			case 4508:
			case 3278:
			case 13356:
			case 13577:
			case 13389:
			case 668:
			case 13974:
			case 14169:
			case 14604:
				printf("quest ID: %d\n", interface_id);
				send_quest_log(c, interface_id);
				break;
			/* Song Names.  */
			case 4389:
			case 4347:
			case 4304:
			case 4418:
			case 11881:
			case 4387:
			case 4351:
			case 4378:
			case 6983:
			case 8435:
			case 4373:
			case 11482:
			case 4362:
			case 4365:
			case 4430:
			case 4433:
			case 6943:
			case 4346:
			case 4294:
			case 4364:
			case 4372:
			case 4337:
			case 4297:
			case 4298:
			case 4299:
			case 4300:
			case 4301:
			case 4302:
			case 4423:
			case 4424:
			case 4339:
			case 4431:
			case 4393:
			case 4291:
			case 4319:
			case 4407:
			case 4427:
			case 4428:
			case 14168:
			case 6867:
			case 6843:
			case 6298:
			case 8117:
			case 4425:
			case 4288:
			case 4408:
			case 4395:
			case 4385:
			case 5988:
			case 4345:
			case 4321:
			case 4386:
			case 1893:
			case 677:
			case 4357:
			case 8118:
			case 4375:
			case 4314:
			case 4310:
			case 4371:
			case 4358:
			case 6944:
			case 4313:
			case 4324:
			case 4420:
			case 674:
			case 4329:
			case 4360:
			case 4421:
			case 4344:
			case 4399:
			case 4382:
			case 4404:
			case 4397:
			case 4432:
			case 4426:
			case 4309:
			case 8142:
			case 4422:
			case 961:
			case 7461:
			case 4383:
			case 4290:
			case 4384:
			case 4392:
			case 4379:
			case 4412:
			case 4350:
			case 4349:
			case 8434:
			case 4334:
			case 4307:
			case 4308:
			case 4340:
			case 4289:
			case 4377:
			case 4311:
			case 6297:
			case 4326:
			case 4370:
			case 4402:
			case 4436:
			case 4394:
			case 4403:
			case 4342:
			case 4359:
			case 4353:
			case 4354:
			case 4343:
			case 4355:
			case 4328:
			case 4332:
			case 5990:
			case 676:
			case 4396:
			case 4292:
			case 4293:
			case 4336:
			case 4376:
			case 675:
			case 4401:
			case 4409:
			case 678:
			case 8565:
			case 4406:
			case 4441:
			case 4435:
			case 4864:
			case 4870:
			case 4879:
			case 4388:
			case 5989:
			case 1898:
			case 4325:
			case 8433:
			case 4434:
			case 4306:
			case 4327:
			case 4437:
			case 8968:
			case 4374:
			case 8116:
			case 4368:
			case 4380:
			case 4335:
			case 4295:
			case 4361:
			case 4410:
			case 4333:
			case 1883:
			case 4411:
			case 4330:
			case 4331:
			case 6185:
			case 4316:
			case 4405:
			case 4352:
			case 4323:
			case 4320:
			case 4398:
			case 1890:
			case 4287:
			case 4880:
			case 4415:
			case 4303:
			case 4315:
			case 4317:
			case 4414:
			case 4367:
			case 4366:
			case 4296:
			case 8437:
			case 4348:
			case 6945:
			case 4356:
			case 4413:
			case 4369:
			case 8141:
			case 8432:
			case 7460:
			case 8140:
			case 8139:
			case 8138:
			case 8935:
			case 664:
			case 8574:
			case 8575:
			case 8569:
			case 8570:
			case 8571:
			case 8573:
			case 188:
			case 8436:
			case 8567:
			case 1739:
			case 190:
			case 192:
			case 7454:
			case 189:
			case 4881:
			case 12844:
			case 7453:
			case 8572:
			case 10983:
			case 8568:
			case 8566:
			case 11481:
			case 11483:
			case 11484:
			case 11109:
			case 11108:
			case 8970:
			case 8967:
			case 8936:
			case 10112:
			case 7451:
			case 10111:
			case 11883:
			case 7452:
			case 12847:
			case 10113:
			case 11135:
			case 11136:
			case 11137:
			case 11134:
			case 11138:
			case 11139:
			case 11140:
			case 4338:
			case 11141:
			case 11142:
			case 4341:
			case 11476:
			case 10114:
			case 11477:
			case 11478:
			case 11906:
			case 10131:
			case 10126:
			case 10132:
			case 7382:
			case 8971:
			case 8972:
			case 12390:
			case 8973:
			case 8974:
			case 8975:
			case 8976:
			case 8980:
			case 8977:
			case 8978:
			case 8979:
			case 10127:
			case 11882:
			case 10128:
			case 10129:
			case 10133:
			case 12048:
			case 12047:
			case 12049:
			case 12127:
			case 12336:
			case 12126:
			case 12128:
			case 11106:
			case 11107:
			case 11133:
			case 12835:
			case 12850:
			case 12286:
			case 12287:
			case 12288:
			case 12391:
			case 12289:
			case 12846:
			case 12848:
			case 11939:
			case 11941:
			case 11940:
			case 12808:
			case 12809:
			case 7030:
			case 12810:
			case 7044:
			case 12841:
			case 12842:
			case 12843:
			case 13352:
			case 13353:
			case 12849:
			case 4506:
			case 12851:
			case 4507:
			case 12584:
			case 13359:
			case 13355:
			case 12845:
			case 13354:
			case 13575:
			case 13576:
			case 3276:
			case 3277:
			case 12840:
			case 13779:
			case 13780:
			case 13360:
			case 14189:
			case 14243:
			case 13712:
			case 14190:
			case 14188:
			case 13361:
			case 13900:
			case 14602:
			case 13713:
			case 14167:
			case 14186:
			case 6842:
			case 11095:
			case 2802:
			case 13972:
			case 2803:
			case 14242:
			case 2801:
			case 14454:
			case 14453:
			case 14455:
			case 14603:
			case 11880:
				handle_clicked_song(c, interface_id);
				break;
			default:
				/* If the interface id was not found in here, it must be in
				 * the non-sidebar list. Set not_sidebar to 1 so we know to
				 * do the anticheat.  */
				not_sidebar = 1;
				break;
		}
	}
	
	if (not_sidebar == 1)
	{
		/* Anti-cheat checking for interface handling.  */
		if (anticheat_check_interface(c, interface_id) == 1)
		{
			printf("INTERFACE ID may not be implemented: %d\n", interface_id);
		}
	}
	else
	{
		return;
	}

	switch (interface_id)
	{
		/* DIALOGUE BUTTONS.  */
		case 2461:
		case 2471:
		case 2482:
		case 2494:
			printf("Picked choice one.\n");
			c->plr->chat_choice = 0;
			handle_dialogue(c);
			break;

		case 2462:
		case 2472:
		case 2483:
		case 2495:
			printf("Picked choice two.\n");
			c->plr->chat_choice = 1;
			handle_dialogue(c);
			break;

		case 2473:
		case 2484:
		case 2496:
			printf("Picked choice three.\n");
			c->plr->chat_choice = 2;
			handle_dialogue(c);
			break;

		case 2485:
		case 2497:
			printf("Picked choice four.\n");
			c->plr->chat_choice = 3;
			handle_dialogue(c);
			break;

		case 2498:
			printf("Picked choice five.\n");
			c->plr->chat_choice = 4;
			handle_dialogue(c);
			break;

	/* What would you like to make?  */
		/* Choice one.  */
		case 8874:
		case 8889:
		case 8909:
		case 8949:
		case 11474:
			printf("Choice one, one batch.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8873:
		case 8888:
		case 8908:
		case 8948:
		case 11473:
			printf("Choice one, five batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8872:
		case 8887:
		case 8907:
		case 8947:
		case 11472:
			printf("choice one, ten batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 8871:
		case 8886:
		case 8906:
		case 8946:
		case 11471:
			printf("choice one, x batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		
		/* Choice two.  */
		case 8893:
		case 8878:
		case 8913:
		case 8953:
		case 12396:
			printf("Choice two, one batch.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8892:
		case 8877:
		case 8912:
		case 8952:
		case 12395:
			printf("Choice two, five batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8891:
		case 8876:
		case 8911:
		case 8951:
		case 12394:
			printf("choice two, ten batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 8890:
		case 8875:
		case 8910:
		case 8950:
		case 11475:
			printf("choice two, x batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;

		/* Choice three.  */
		case 8897:
		case 8917:
		case 8957:
		case 12400:
			printf("Choice three, one batch.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8896:
		case 8916:
		case 8956:
		case 12399:
			printf("Choice three, five batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8895:
		case 8915:
		case 8955:
		case 12398:
			printf("choice three, ten batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 8894:
		case 8914:
		case 8954:
		case 12397:
			printf("choice three, x batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		
		/* Choice four.  */
		case 8921:
		case 8961:
		case 12404:
			printf("Choice four, one batch.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8920:
		case 8960:
		case 12403:
			printf("Choice four, five batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8919:
		case 8959:
		case 12402:
			printf("choice four, ten batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 8918:
		case 8958:
		case 12401:
			printf("choice four, x batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		
		/* Choice five.  */
		case 8965:
		case 12408:
			printf("Choice five, one batch.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8964:
		case 12407:
			printf("Choice five, five batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8963:
		case 12406:
			printf("choice five, ten batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 8962:
		case 12405:
			printf("choice five, x batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		
		/* Choice six.  */
		case 12412:
			printf("Choice six, one batch.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 12411:
			printf("Choice six, five batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 12410:
			printf("choice six, ten batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 12409:
			printf("choice six, x batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;

	/* How many would you like to make?  */
		case 2799:
			printf("Choice one, one batch.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 2798:
			printf("Choice one, five batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 1748:
			printf("choice one, x batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 1747:
			printf("choice one, all batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
	
	/* What hides would you like tanning?  */
		/* Soft leather.  */
		case 14817:
			printf("Choice one, one batch.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14809:
			printf("Choice one, five batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14801:
			printf("choice one, x batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14793:
			printf("choice one, all batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Hard leather.  */
		case 14818:
			printf("Choice two, one batch.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14810:
			printf("Choice two, five batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14802:
			printf("choice two, x batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14794:
			printf("choice two, all batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Snakeskin.  */
		case 14819:
			printf("Choice three, one batch.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14811:
			printf("Choice three, five batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14803:
			printf("choice three, x batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14795:
			printf("choice three, all batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Snakeskin (Swamp, later rev). XXX: Make sure chat_choice is adjusted
		 * before implementing.  */
		/*
		case 14820:
			printf("Choice four, one batch.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14812:
			printf("Choice four, five batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14804:
			printf("choice four, x batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14796:
			printf("choice four, all batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		*/
		/* Green d'hide.  */
		case 14821:
			printf("Choice four, one batch.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14813:
			printf("Choice four, four batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14805:
			printf("choice four, x batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14797:
			printf("choice four, all batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Blue d'hide.  */
		case 14822:
			printf("Choice five, one batch.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14814:
			printf("Choice five, five batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14806:
			printf("choice five, x batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14798:
			printf("choice five, all batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Red d'hide.  */
		case 14823:
			printf("Choice six, one batch.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14815:
			printf("Choice six, five batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14807:
			printf("choice six, x batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14799:
			printf("choice six, all batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
		/* Black d'hide.  */
		case 14824:
			printf("Choice seven, one batch.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 14816:
			printf("Choice seven, five batches.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 14808:
			printf("choice seven, x batches.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 14800:
			printf("choice seven, all batches.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 100000;
			handle_dialogue(c);
			break;
	/* Soft leather crafting menu.  */
		/* Soft leather armor.  */
		case 8635:
			printf("choice one, one batch.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8634:
			printf("choice one, five batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8633:
			printf("choice one, ten batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Gloves.  */
		case 8638:
			printf("choice two, one batch.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8637:
			printf("choice two, five batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8636:
			printf("choice two, ten batches.\n");
			c->plr->chat_choice = 1;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Boots.  */
		case 8641:
			printf("choice three, one batch.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8640:
			printf("choice three, five batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8639:
			printf("choice three, ten batches.\n");
			c->plr->chat_choice = 2;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Vambraces.  */
		case 8644:
			printf("choice four, one batch.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8643:
			printf("choice four, five batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8642:
			printf("choice four, ten batches.\n");
			c->plr->chat_choice = 3;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Chaps.  */
		case 8647:
			printf("choice five, one batch.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8646:
			printf("choice five, five batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8645:
			printf("choice five, ten batches.\n");
			c->plr->chat_choice = 4;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Coif.  */
		case 8650:
			printf("choice six, one batch.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8649:
			printf("choice six, five batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8648:
			printf("choice six, ten batches.\n");
			c->plr->chat_choice = 5;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		/* Cowl.  */
		case 8653:
			printf("choice seven, one batch.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 8652:
			printf("choice seven, five batches.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 8651:
			printf("choice seven, ten batches.\n");
			c->plr->chat_choice = 6;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
	/* Cooking menu.  */
		/* What would you like to cook?  */
		case 13720:
			printf("Choice one, one batch.\n");
			c->plr->chat_choice = 0;
			c->plr->action_menu_skill_id = COOKING_SKILL;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 13719:
			printf("Choice one, five batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_menu_skill_id = COOKING_SKILL;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 13718:
			printf("choice one, x batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_menu_skill_id = COOKING_SKILL;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		case 13717:
			printf("choice one, all batches.\n");
			c->plr->chat_choice = 0;
			c->plr->action_menu_skill_id = COOKING_SKILL;
			c->plr->action_skill_remaining_actions = 10000;
			handle_dialogue(c);
			break;
	/* Smelting menu.  */
		/* What would you like to smelt?  */
		/* Bronze.  */
		case 3987:
			printf("Bronze, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_BRONZE;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 3986:
			printf("Bronze, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_BRONZE;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 2807:
			printf("Bronze, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_BRONZE;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 2414:
			printf("Bronze, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_BRONZE;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Iron.  */
		case 3991:
			printf("Iron, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_IRON;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 3990:
			printf("Iron, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_IRON;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 3989:
			printf("Iron, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_IRON;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 3988:
			printf("Iron, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_IRON;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Silver.  */
		case 3995:
			printf("Silver, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_SILVER;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 3994:
			printf("Silver, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_SILVER;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 3993:
			printf("Silver, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_SILVER;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 3992:
			printf("Silver, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_SILVER;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Steel.  */
		case 3999:
			printf("Steel, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_STEEL;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 3998:
			printf("Steel, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_STEEL;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 3997:
			printf("Steel, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_STEEL;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 3996:
			printf("Steel, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_STEEL;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Gold.  */
		case 4003:
			printf("Gold, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_GOLD;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 4002:
			printf("Gold, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_GOLD;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 4001:
			printf("Gold, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_GOLD;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 4000:
			printf("Gold, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_GOLD;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Mithril.  */
		case 7441:
			printf("Mithril, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_MITHRIL;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 7440:
			printf("Mithril, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_MITHRIL;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 6397:
			printf("Mithril, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_MITHRIL;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 4158:
			printf("Mithril, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_MITHRIL;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Adamant.  */
		case 7446:
			printf("Adamant, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_ADAMANT;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 7444:
			printf("Adamant, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_ADAMANT;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 7443:
			printf("Adamant, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_ADAMANT;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 7442:
			printf("Adamant, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_ADAMANT;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Rune.  */
		case 7450:
			printf("Rune, one batch.\n");
			c->plr->chat_choice = SMITHING_BAR_RUNE;
			c->plr->action_skill_remaining_actions = 1;
			handle_dialogue(c);
			break;
		case 7449:
			printf("Rune, five batches.\n");
			c->plr->chat_choice = SMITHING_BAR_RUNE;
			c->plr->action_skill_remaining_actions = 5;
			handle_dialogue(c);
			break;
		case 7448:
			printf("Rune, ten batches.\n");
			c->plr->chat_choice = SMITHING_BAR_RUNE;
			c->plr->action_skill_remaining_actions = 10;
			handle_dialogue(c);
			break;
		case 7447:
			printf("Rune, x batches.\n");
			c->plr->chat_choice = SMITHING_BAR_RUNE;
			c->plr->action_skill_remaining_actions = -1;
			packet_send_choose_amount_chatbox_interface(c, 1);
			break;
		/* Bank item mode menu.  */
		/* Swap mode.  */
		case 8130:
			c->plr->bank_is_insert_mode = 0;
			packet_set_client_config_value(c, BANK_REARRANGE_MODE, 0);
			break;
		/* Insert mode.  */
		case 8131:
			c->plr->bank_is_insert_mode = 1;
			packet_set_client_config_value(c, BANK_REARRANGE_MODE, 1);
			break;
		/* Bank item withdraw mode.  */
		/* Item mode.  */
		case 5387:
			c->plr->bank_is_note_mode = 0;
			packet_set_client_config_value(c, BANK_WITHDRAW_MODE, 0);
			break;
		/* Note mode.  */
		case 5386:
			c->plr->bank_is_note_mode = 1;
			packet_set_client_config_value(c, BANK_WITHDRAW_MODE, 1);
			break;
		/* Close buttons.  */
		case 1084:
		case 2422:
		/* Player appearance close.  */
		case 3651:
		case 14922:
			packet_close_all_open_interfaces(c);
			break;

		default:
			printf("Unhandled interface id: %d\n", interface_id);
			break;
	}
}
