/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STREAM_H_
#define _STREAM_H_

/**
 * @brief Holds data for update_blocks, used for player updating.
 */
struct update_stream
{
	/** Update blocks buffer.  */
	char update_blocks[UPDATE_BLOCK_SIZE];
	/** For writing bits, offset for buffer.  */
	int bit_update_blocks;
	/** Offset for reading/writing the buffer.  */
	int update_blocks_offset;
};

int fill_instream(struct client *c, int force_read);
int fill_instream_music(struct music_client *c, int force_read);
void send_out_stream(struct client *c);
void send_out_music_stream(struct music_client *c);
void write_byte_to_socket(struct client *c, int byte);

void write_range_to_socket(struct client *c, int start);
void write_range_to_music_socket(struct music_client *c, int start);

int read_opcode(struct client *c);

void create_frame(struct client *c, int id);
void create_frame_vsize(struct client *c, int id);
void create_frame_vsize_word(struct client *c, int id);
void end_frame_vsize(struct client *c);
void end_frame_vsize_word(struct client *c);
void init_bit_access(struct client *c);
void finish_bit_access(struct client *c);
void read_bytes(struct client *c, char string[], int num_to_read, int start);
void read_bytes_a(struct client *c, char string[], int num_to_read, int start);
void read_bytes_reverse(struct client *c, char string[], int num_to_read, 
						int start);
void read_bytes_reverse_a(struct client *c, char string[], int num_to_read, 
						  int start);
uint32_t read_dword(struct client *c);
uint32_t read_dword_v1(struct client *c);
uint32_t read_dword_v2(struct client *c);
uint64_t read_qword(struct client *c); 
char read_signed_byte(struct client *c);
char read_signed_byte_a(struct client *c);
char read_signed_byte_c(struct client *c);
char read_signed_byte_s(struct client *c);
int read_signed_word(struct client *c);
int read_signed_word_a(struct client *c);
int read_signed_word_bigendian(struct client *c);
int read_signed_word_bigendian_a(struct client *c);
int read_login_string(struct client *c, char string[], int size);
int read_login_music_string(struct music_client *c, char string[], int size);

void read_string(struct client *c, char string[], int string_size);
char read_unsigned_byte(struct client *c);
char read_unsigned_music_byte(struct music_client *c);
char read_unsigned_byte_a(struct client *c);
char read_unsigned_byte_c(struct client *c);
char read_unsigned_byte_s(struct client *c);
int read_unsigned_word(struct client *c);
int read_unsigned_word_a(struct client *c);
int read_unsigned_word_bigendian(struct client *c);
int read_unsigned_word_bigendian_a(struct client *c);

void write_3byte(struct client *c, int i);
void write_bits(struct client *c, int num_bits, int value);
void write_bits_update(int num_bits, int value); 

void write_byte(struct client *c, int i);
void write_music_byte(struct music_client *c, int i);
void write_byte_update(int i); 
void write_byte_pprops(struct client *c, int i); 

void write_byte_a(struct client *c, int i);
void write_byte_a_update(int i); 

void write_byte_c(struct client *c, int i);
void write_byte_c_update(int i);

void write_bytes(struct client *c, char string[], int offset, int start);
void write_bytes_update(char string[], int i, int j); 

void write_bytes_a(struct client *c, char string[], int i, int j);
void write_bytes_a_update(char string[], int i, int j);
 
void write_byte_s(struct client *c, int i);
void write_byte_s_update(int i); 

void write_bytes_reverse(struct client *c, char string[], int i, int j);
void write_bytes_reverse_update(char string[], int i, int j);

void write_bytes_reverse_a(struct client *c, char string[], int i, int j);

void write_char_to_socket(struct client *c, char byte);
void write_char_to_music_socket(struct music_client *c, char byte);


void write_dword(struct client *c, int i);
void write_dword_update(int i); 

void write_dword_v1(struct client *c, int i);

void write_dword_v2(struct client *c, int i);
void write_dword_v2_update(int i) ;

void write_dword_bigendian(struct client *c, int i);
void write_dword_bigendian_update(int i);

void write_frame_size(struct client *c, int i);
void write_frame_size_word(struct client *c, int i);
void write_qword(struct client *c, uint64_t l);
void write_qword_pprops(struct client *c, uint64_t l); 

void write_string(struct client *c, char string[] );
void write_string_update(char string[]); 


void write_word(struct client *c, int i); 
void write_music_word(struct music_client *c, int i); 
void write_word_pprops(struct client *c, int i);
void write_word_update(int i); 

void write_word_a(struct client *c, int i);
void write_word_a_update(int i); 

void write_word_bigendian(struct client *c, int i);
void write_word_bigendian_update(int i); 

void write_word_bigendian_a(struct client *c, int i);
void write_word_bigendian_a_update(int i); 

void write_smart_b(struct client *c, int i);

long convert_player_name_to_int64(char name[]); 


#endif
