/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "item.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "misc.h"

int get_woodcutting_level_needed(int woodcutting_id)
{
	switch (woodcutting_id)
	{
		case WOODCUTTING_REGULAR:
		case WOODCUTTING_ACHEY:
			return 1;
		case WOODCUTTING_OAK:
			return 15;
		case WOODCUTTING_WILLOW:
			return 30;
		case WOODCUTTING_TEAK:
			return 35;
		case WOODCUTTING_MAPLE:
			return 45;
		case WOODCUTTING_HOLLOW:
			return 45;
		case WOODCUTTING_MAHOGANY:
			return 50;
		case WOODCUTTING_YEW:
			return 60;
		case WOODCUTTING_MAGIC:
			return 75;
		default:
			printf("Unknown woodcutting id: %d, level!\n", woodcutting_id);
			return -1;
	}
}

float get_woodcutting_chance(int woodcutting_id, int woodcutting_level)
{
	switch (woodcutting_id)
	{
		case WOODCUTTING_REGULAR:
		case WOODCUTTING_ACHEY:
			return (woodcutting_level * 0.0085) + 0.45;
		case WOODCUTTING_OAK:
			return (woodcutting_level * 0.0085);
		case WOODCUTTING_WILLOW:
			return (woodcutting_level * 0.004);
		case WOODCUTTING_TEAK:
			return (woodcutting_level * 0.004);
		case WOODCUTTING_MAPLE:
			return (woodcutting_level * 0.003);
		case WOODCUTTING_HOLLOW:
			return (woodcutting_level * 0.003);
		case WOODCUTTING_MAHOGANY:
			return (woodcutting_level * 0.002);
		case WOODCUTTING_YEW:
			return (woodcutting_level * 0.001);
		case WOODCUTTING_MAGIC:
			return (woodcutting_level * 0.0008);
		default:
			printf("Unknown woodcutting id: %d, level!\n", woodcutting_id);
			return -1;
	}
}

float get_woodcutting_xp(int woodcutting_id)
{
	switch (woodcutting_id)
	{
		case WOODCUTTING_REGULAR:
		case WOODCUTTING_ACHEY:
			return 25.0;
		case WOODCUTTING_OAK:
			return 37.5;
		case WOODCUTTING_WILLOW:
			return 67.5;
		case WOODCUTTING_TEAK:
			return 85.0;
		case WOODCUTTING_MAPLE:
			return 100.0;
		case WOODCUTTING_HOLLOW:
			return 82.5;
		case WOODCUTTING_MAHOGANY:
			return 125.0;
		case WOODCUTTING_YEW:
			return 175.0;
		case WOODCUTTING_MAGIC:
			return 250.0;
		default:
			printf("Unknown woodcutting id: %d, xp!\n", woodcutting_id);
			return -1;
	}
}

int get_woodcutting_log_id(int woodcutting_id)
{
	switch (woodcutting_id)
	{
		case WOODCUTTING_REGULAR:
			return 1511;
		case WOODCUTTING_ACHEY:
			return 2862;
		case WOODCUTTING_OAK:
			return 1521;
		case WOODCUTTING_WILLOW:
			return 1519;
		case WOODCUTTING_TEAK:
			return 6333;
		case WOODCUTTING_MAPLE:
			return 1517;
		case WOODCUTTING_HOLLOW:
			return 3239;
		case WOODCUTTING_MAHOGANY:
			return 6332;
		case WOODCUTTING_YEW:
			return 1515;
		case WOODCUTTING_MAGIC:
			return 1513;
		default:
			printf("Unknown woodcutting id: %d, log id!\n", woodcutting_id);
			return -1;
	}
}

/**
 * @todo Respawn ticks might vary some, check the wiki.
 */
int get_woodcutting_respawn_ticks(int woodcutting_id)
{
	switch (woodcutting_id)
	{
		case WOODCUTTING_REGULAR:
		case WOODCUTTING_ACHEY:
			return 50;
		case WOODCUTTING_OAK:
			return 16;
		case WOODCUTTING_WILLOW:
			return 16;
		case WOODCUTTING_TEAK:
			return 16;
		case WOODCUTTING_MAPLE:
			return 75;
		case WOODCUTTING_HOLLOW:
			return 75;
		case WOODCUTTING_MAHOGANY:
			return 100;
		case WOODCUTTING_YEW:
			return 125;
		case WOODCUTTING_MAGIC:
			return 247;
		default:
			printf("Unknown woodcutting id: %d, respawn ticks!\n", 
				   woodcutting_id);
			return -1;
	}
}


/**
 * Returns the tree stump object id for the passed tree id.
 * @todo Needs to be expanded a whole lot, very barren.
 */
int get_correct_stump_id(int tree_id)
{
	switch (tree_id)
	{
	/* TODO: More trees need to be handled.  */
		/* Dead trees.  */
		case 1282:
			return 1347;
		case 1286:
			return 1351;
		case 1383:
			return 1358;
		default:
			return 1342;
	}
}


/**
 * Sets up all the needed variables for woodcutting, bonuses for player's
 * axe used, checking skill requirements, setting animation.
 * @param *c Client instance.
 */
void init_woodcutting(struct client *c)
{
	int woodcutting_level = get_player_skill_level(c, WOODCUTTING_SKILL);
	/* Set action related ids.  */
	c->plr->action_skill_id = WOODCUTTING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_WOODCUTTING);
	
	if (get_free_inv_slots(c) == 0)
	{
		packet_send_chatbox_message(c, "You don't have any room for logs!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Store what level is needed to chop down the clicked tree.  */
	c->plr->action_skill_lvl_req 
		= get_woodcutting_level_needed(c->plr->action_id_type);

	/* Check if level requirements are met.  */
	if (woodcutting_level < c->plr->action_skill_lvl_req)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "woodcutting to chop that tree!\n", 
				 c->plr->action_skill_lvl_req);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	/* Checking player's equipment for needed items.  */
	if (c->plr->equipment[WEAPON_SLOT] <= 1362 
		&& c->plr->equipment[WEAPON_SLOT] >= 1349)
	{
		/* Player has a woodcutting axe equiped, use that.  */
		c->plr->skill_tool_id = c->plr->equipment[WEAPON_SLOT];
		c->plr->skill_tool_slot = WEAPON_SLOT;
		c->plr->skill_tool_equip = 1;
	}
	else
	{
		int x;
		/* Loops through the player's inventory to see if they have an axe to
		   use.  */
		for (x = 0; x < INV_SIZE; x++)
		{
			if (c->plr->items[x] <= 1362 
				&& c->plr->items[x] >= 1349
				&& ITEMS[c->plr->items[x]]->noted != 1)
			{
				c->plr->skill_tool_id = c->plr->items[x];
				c->plr->skill_tool_slot = x;
				c->plr->skill_tool_equip = 0;
				break;
			}
		}
	}

	if (c->plr->skill_tool_id != -1)
	{
		/* Unlike mining, woodcutting operates on a 4 tick cycle regardless
		 * of tool material quality.  */
		c->plr->skill_tool_time = 4;

		switch (c->plr->skill_tool_id)
		{
			/* Bronze.  */
			case 1351:
				c->plr->skill_tool_emote = 879;
				break;
			/* Iron.  */
			case 1349:
				c->plr->skill_tool_emote = 877;
				break;
			/* Steel.  */
			case 1353:
				c->plr->skill_tool_emote = 875;
				break;
			/* Black.  */
			case 1361:
				c->plr->skill_tool_emote = 873;
				break;
			/* Mithril.  */
			case 1355:
				c->plr->skill_tool_emote = 871;
				break;
			/* Adamant.  */
			case 1357:
				c->plr->skill_tool_emote = 869;
				break;
			/* Rune.  */
			case 1359:
				c->plr->skill_tool_emote = 867;
				break;
		}

		if (woodcutting_level 
			< ITEMS[c->plr->skill_tool_id]->skill_use_req
			&& c->plr->skill_tool_emote != -1)
		{
			char error_msg[150];
			snprintf(error_msg, 150, "You must be at least level %d in "
					 "woodcutting to use that axe!\n", 
					 ITEMS[c->plr->skill_tool_id]->skill_use_req);
			set_player_state(c, STATE_IDLE);
			packet_send_chatbox_message(c, error_msg);
			return;
		}

		/* Axe detection was without errors, check tree level next.  */
		c->plr->action_ticks = c->plr->skill_tool_time;
		player_play_animation_repeats(c, c->plr->skill_tool_emote, 4);
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have an axe to use!");
		set_player_state(c, STATE_IDLE);
		return;
	}
}

/**
 * Handles chance for chopping down trees, stumps, item handling, etc.
 * @param *c Client instance.
 * @todo Needs to be re-written to use the char based chance. These values
 * are probably accurate enough, but not quite.
 */
void handle_chopping_tree(struct client *c)
{
	int effective_wc_level = get_player_skill_level(c, WOODCUTTING_SKILL);
	int tree_respawn_ticks 
		= get_woodcutting_respawn_ticks(c->plr->action_id_type);

	double roll = rand_double(0.0, 1.0);
	double cut_chance = get_woodcutting_chance(c->plr->action_id_type, 
											   effective_wc_level);

	printf("roll: %f chance: %f %d\n", roll, cut_chance, 
		   (roll < cut_chance) ? 1 : 0);

	/* If woodcutting has suceeded.  */
	if (roll < cut_chance)
	{
		int obj_index, stump_obj_id;
		
		obj_index = get_object_spawn_index(c->plr->action_x, 
					c->plr->action_y, c->plr->world_z);

		/* Check if a stump has already been spawned at the position. If so,
		 * stop trying to chop down the tree.  */
		if (obj_index != -1)
		{
			set_player_state(c, STATE_IDLE);
			player_reset_animation(c);
			return;
		}

		if (add_item_to_inv(c, 
							get_woodcutting_log_id(c->plr->action_id_type), 1, 
							-1) 
							== 1)
		{
			packet_send_chatbox_message(c, "You get some logs.");
			/* Gives the player xp proper to the tree.  */
			add_skill_xp(c, WOODCUTTING_SKILL, 
						 get_woodcutting_xp(c->plr->action_id_type));
		}
		else
		{
			/* Otherwise player has no inventory space left for logs, stop
			 * woodcutting.  */
			set_player_state(c, STATE_IDLE);
			return;
		}

		/* If tree is one that gets chopped down instantly create the stump,
		   otherwise roll to see if the tree gets chopped if it is one that
		   gives multiple logs.  */
		if ((rand_int(1, 8) == 8)
			|| (c->plr->action_id_type == WOODCUTTING_REGULAR 
				|| c->plr->action_id_type == WOODCUTTING_ACHEY))
		{
			stump_obj_id = add_to_object_list(c->plr->action_id, 
							   10, 
							   c->plr->action_x,
							   c->plr->action_y, 
							   c->plr->world_z, 0, 
							   (tree_respawn_ticks + 1),
							   0);
			set_object_transforms(stump_obj_id, 
									   get_correct_stump_id(c->plr->action_id), 
									   tree_respawn_ticks);
			spawn_object_globally(stump_obj_id);
			
			set_player_state(c, STATE_IDLE);
			player_reset_animation(c);
			return;
		}
	}
	c->plr->action_ticks = c->plr->skill_tool_time;
	c->plr->action_total_time_acted++;
}
