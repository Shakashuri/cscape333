/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file stream.c
 * @brief Functions for the low-level network sockets handling.
 */

#include <string.h>
#include <arpa/inet.h>

#include "constants.h"
#include "client.h"
#include "isaac.h"
#include "rng.h"
#include "stream.h"

/**
 * Read force_read number of bytes from a client.
 * @param *c Client to read from.
 * @param force_read Number of bytes to read.
 * @return Number of bytes read.
 */
int fill_instream(struct client *c, int force_read)
{
	c->instream_offset = 0;
	return recv(c->socket, c->instream, force_read, 0);
}

/**
 * Read force_read number of bytes from a music client.
 * @param *c Music client to read from.
 * @param force_read Number of bytes to read.
 * @return Number of bytes read.
 */
int fill_instream_music(struct music_client *c, int force_read)
{
	c->instream_offset = 0;
	return recv(c->socket, c->instream, force_read, 0);
}

/**
 * Write all data in the client's outstream buffer to the socket.
 * @param *c Client instance.
 */
void send_out_stream(struct client *c)
{
	write_range_to_socket(c, 0);
	c->outstream_offset = 0;
}

/**
 * Write all data in the music client's outstream buffer to the socket.
 * @param *c Music client instance.
 */
void send_out_music_stream(struct music_client *c)
{
	write_range_to_music_socket(c, 0);
	c->outstream_offset = 0;
}

/**
 * Send a char to a specified client.
 * @param *c Client instance.
 * @param byte Char to send.
 */
void write_char_to_socket(struct client *c, char byte)
{
	send(c->socket, &byte, sizeof(byte), MSG_NOSIGNAL);
}

/**
 * Send a char to a specified music client.
 * @param *c Music client instance.
 * @param byte Char to send.
 */
void write_char_to_music_socket(struct music_client *c, char byte)
{
	send(c->socket, &byte, sizeof(byte), MSG_NOSIGNAL);
}

/**
 * Write a specified range from the outstream buffer to the client.
 * @param *c Client instance.
 * @param start Starting position in the buffer.
 */
void write_range_to_socket(struct client *c, int start)
{
	int x;

	for (x = start; x < c->outstream_offset; x++)
	{
		send(c->socket, &c->outstream[x], sizeof(c->outstream[x]), 
			 MSG_NOSIGNAL);
	}
}

/**
 * Write a specified range from the outstream buffer to the music client.
 * @param *c Music client instance.
 * @param start Starting position in the buffer.
 */
void write_range_to_music_socket(struct music_client *c, int start)
{
	int x;

	for (x = start; x < c->outstream_offset; x++)
	{
		send(c->socket, &c->outstream[x], sizeof(c->outstream[x]), 
			 MSG_NOSIGNAL);
	}
}

/**
 * Decrypts the opcode from the client.
 * @param *c Client instance.
 * @return decrypted opcode id.
 * @return -1 if there is no data left to read.
 */
int read_opcode(struct client *c) 
{
	if (c->instream_offset < c->bytes_read)
	{
		if (ENABLE_ISAAC == 1)
		{
			int x, y;
			/* Change this to a char.  */
			int opcode = c->instream[c->instream_offset++];
			int dec = get_isaac_value(c->decrypt);
		
			opcode &= 0xff;
			opcode -= dec;
			opcode &= 0xff;

			/* Stores the last n packets to aid with debugging.  */
			y = (LAST_PACKETS_SIZE - 2);
			for (x = (LAST_PACKETS_SIZE - 1); (x >= 0 && y != -1); x--)
			{
				c->last_packets[x] = c->last_packets[y];	
				y--;
			}
			c->last_packets[0] = c->current_packet = opcode;

			return opcode;
		}
		else
		{
			return read_unsigned_byte(c);
		}
	}
	else
	{
		return -1;
	}
}

/**
 * Writes a packet id to outstream.
 * @param *c Client instance.
 * @param id Opcode id to write.
 */
void create_frame(struct client *c, int id) 
{
	int enc = get_isaac_value(c->encrypt);

	if (ENABLE_ISAAC)
	{
		c->outstream[c->outstream_offset++] = (char) (enc + id);
	}
	else
	{
		c->outstream[c->outstream_offset++] = (char) id;
	}
}

/**
 * Writes a packet id to outstream, variable sized packet.
 * @param *c Client instance.
 * @param id Opcode id to write.
 */
void create_frame_vsize(struct client *c, int id) 
{
	int enc = get_isaac_value(c->encrypt);

	if (ENABLE_ISAAC)
	{
		c->outstream[c->outstream_offset++] = (char) (enc + id);
	}
	else
	{
		c->outstream[c->outstream_offset++] = (char) id;
	}

	c->outstream[c->outstream_offset++] = 0;
	c->frame_stack[++c->frame_stack_ptr] = c->outstream_offset;
}

/**
 * Writes a packet id to outstream, variable sized packet, word data type.
 * @param *c Client instance.
 * @param id Opcode id to write.
 */
void create_frame_vsize_word(struct client *c, int id)
{
	if (ENABLE_ISAAC)
	{
		unsigned int enc = get_isaac_value(c->encrypt);
		c->outstream[c->outstream_offset++] = (char) (enc + 
											  (unsigned int ) id);
	}
	else
	{
		c->outstream[c->outstream_offset++] = (char) id;
	}

	write_word(c, 0);
	c->frame_stack[++c->frame_stack_ptr] = c->outstream_offset;
}

/**
 * End a variable sized packet.
 * @param *c Client instance.
 */
void end_frame_vsize(struct client *c)
{
	int id = 0;

	id = c->outstream_offset - c->frame_stack[c->frame_stack_ptr--];
	write_frame_size(c, id); 
}

/**
 * End a variable sized packet started with a word datatype.
 * @param *c Client instance.
 */
void end_frame_vsize_word(struct client *c) 
{
	int id = 0;

	id = c->outstream_offset - c->frame_stack[c->frame_stack_ptr--];
	write_frame_size_word(c, id); 
}

/**
 * Start writing bits to the output stream.
 * @param *c Client instance.
 */
void init_bit_access(struct client *c)
{
	c->bit_outstream = c->outstream_offset * 8;
}

/**
 * Finish writing bits to the output stream.
 * @param *c Client instance.
 */
void finish_bit_access(struct client *c) 
{
	c->outstream_offset = (c->bit_outstream + 7) / 8;
}

/**
 * Read a number of bytes from the instream buffer to a char buffer.
 * @param *c Client instance.
 * @param string[] Array to write to.
 * @param num_to_read Number of bytes to read.
 * @param start Starting point to read from.
 */
void read_bytes(struct client *c, char string[], int num_to_read, int start)
{
	int k;	

	for (k = start; k < start + num_to_read; k++)
	{
		string[k] = c->instream[c->instream_offset++];
	}
}

/**
 * Read a number of bytes from the instream buffer to a char buffer, char type
 * a.
 * @param *c Client instance.
 * @param string[] Array to write to.
 * @param num_to_read Number of bytes to read.
 * @param start Starting point to read from.
 */
void read_bytes_a(struct client *c, char string[], int num_to_read, int start)
{
	int k;	

	for (k = start; k < start + num_to_read; k++)
	{
		string[k] = (char) (c->instream[c->instream_offset++] - 128);
	}
}

/**
 * Read a number of bytes from the instream buffer in reverse order to a char 
 * buffer.
 * @param *c Client instance.
 * @param string[] Array to write to.
 * @param num_to_read Number of bytes to read.
 * @param start Starting point to read from.
 */
void read_bytes_reverse(struct client *c, char string[], int num_to_read, 
						int start)
{
	int k;

	for (k = (start + num_to_read) - 1; k >= start; k--) 
	{
		string[k] = c->instream[c->instream_offset++];
	}
}

/**
 * Read a number of bytes from the instream buffer in reverse order to a char 
 * buffer, char type a.
 * @param *c Client instance.
 * @param string[] Array to write to.
 * @param num_to_read Number of bytes to read.
 * @param start Starting point to read from.
 */
void read_bytes_reverse_a(struct client *c, char string[], int num_to_read, 
						  int start)
{
	int k;

	for (k = (start + num_to_read) - 1; k >= start; k--)
	{
		string[k] = (char) (c->instream[c->instream_offset++] - 128);
	}
}

/**
 * Read 32 bit int from the client stream.
 * @param *c Client instance.
 * @note Order: 1 2 3 4
 * @return uint32_t int read from the client.
 */
uint32_t read_dword(struct client *c) 
{
	c->instream_offset += 4;
	return (((uint32_t) (c->instream[c->instream_offset - 4] & 0xff) << 24)
			+ ((uint32_t) (c->instream[c->instream_offset - 3] & 0xff) << 16)
			+ ((uint32_t) (c->instream[c->instream_offset - 2] & 0xff) << 8)
			+ (uint32_t) (c->instream[c->instream_offset - 1] & 0xff));
}

/**
 * Read 32 bit int from the client stream.
 * @param *c Client instance.
 * @note Order: 3 4 1 2
 * @return uint32_t int read from the client.
 */
uint32_t read_dword_v1(struct client *c) 
{
	c->instream_offset += 4;
	return ((uint32_t) (c->instream[c->instream_offset - 2] & 0xff) << 24)
			+ ((uint32_t) (c->instream[c->instream_offset - 1] & 0xff) << 16)
			+ ((uint32_t) (c->instream[c->instream_offset - 4] & 0xff) << 8)
			+ (uint32_t) (c->instream[c->instream_offset - 3] & 0xff);
}

/**
 * Read 32 bit int from the client stream.
 * @param *c Client instance.
 * @note Order: 2 1 4 3
 * @return uint32_t int read from the client.
 */
uint32_t read_dword_v2(struct client *c) 
{
	c->instream_offset += 4;

	return ((uint32_t) (c->instream[c->instream_offset - 3] & 0xff) << 24)
			+ ((uint32_t) (c->instream[c->instream_offset - 4] & 0xff) << 16)
			+ ((uint32_t) (c->instream[c->instream_offset - 1] & 0xff) << 8)
			+ (uint32_t) (c->instream[c->instream_offset - 2] & 0xff);
}

/**
 * Read 64 bit int from the client stream.
 * @param *c Client instance.
 * @return uint64_t int read from the client.
 */
uint64_t read_qword(struct client *c) 
{
	uint64_t l = read_dword(c) & 0xffffffffL;
	uint64_t l1 = read_dword(c) & 0xffffffffL;

	return (l << 32) + l1;
}

/**
 * Read a signed char from the client stream.
 * @param *c Client instance.
 * @return Signed char read from the client.
 */
char read_signed_byte(struct client *c) 
{
	return c->instream[c->instream_offset++];
}

/**
 * Read a signed char from the client stream.
 * @note Char is subtracted from 128.
 * @param *c Client instance.
 * @return Signed char read from the client.
 */
char read_signed_byte_a(struct client *c) 
{
	return (char) (c->instream[c->instream_offset++] - 128);
}

/**
 * Read a signed char from the client stream.
 * @note Char is multipled by -1.
 * @param *c Client instance.
 * @return Signed char read from the client.
 */
char read_signed_byte_c(struct client *c) 
{
	return (char) (-1 * c->instream[c->instream_offset++]);
}

/**
 * Read a signed char from the client stream.
 * @note 128 is subtracted from char.
 * @param *c Client instance.
 * @return Signed char read from the client.
 */
char read_signed_byte_s(struct client *c) 
{
	return (char) (128 - c->instream[c->instream_offset++]);
}

/**
 * Read a signed, 2 byte int from the client stream.
 * @param *c Client instance.
 * @note Order: 1 2
 * @return Signed 2 byte int read from the client.
 */
int read_signed_word(struct client *c) 
{
	int i;

	c->instream_offset += 2;
	i = ((c->instream[c->instream_offset - 2] & 0xff) << 8)
		  + (c->instream[c->instream_offset - 1] & 0xff);

	if (i > 32767) 
	{
		i -= 0x10000;
	}
	return i;
}

/**
 * Read a signed, 2 byte int from the client stream.
 * @param *c Client instance.
 * @note Order: 1 2, second byte has 128 subtracted from it.
 * @return Signed 2 byte int read from the client.
 */
int read_signed_word_a(struct client *c)
{
	int i;

	c->instream_offset += 2;
	i = ((c->instream[c->instream_offset - 2] & 0xff) << 8)
		  + ((c->instream[c->instream_offset - 1] - 128) & 0xff);
	if (i > 32767)
	{
		i -= 0x10000;
	}
	return i;
}

/**
 * Read a signed, 2 byte int from the client stream.
 * @param *c Client instance.
 * @note Order: 2 1
 * @return Signed 2 byte int read from the client.
 */
int read_signed_word_bigendian(struct client *c)
{
	int i;
	c->instream_offset += 2;
	i = ((c->instream[c->instream_offset - 1] & 0xff) << 8)
		  + (c->instream[c->instream_offset - 2] & 0xff);
	if (i > 32767)
	{
		i -= 0x10000;
	}
	return i;
}

/**
 * Read a signed, 2 byte int from the client stream.
 * @param *c Client instance.
 * @note Order: 2 1, second byte has 128 subtracted from it.
 * @return Signed 2 byte int read from the client.
 */
int read_signed_word_bigendian_a(struct client *c) 
{
	int i;
	c->instream_offset += 2;
	i = ((c->instream[c->instream_offset - 1] & 0xff) << 8)
		  + ((c->instream[c->instream_offset - 2] - 128) & 0xff);
	if (i > 32767)
	{
		i -= 0x10000;
	}
	return i;
}

/**
 * Read a login string from the client, places it into the passed string.
 * @param *c Client instance.
 * @param string[] String to put the result into.
 * @param size Size of the passed string.
 * @return Returns char array with the data in it.
 */
int read_login_string(struct client *c, char string[], int size) 
{
	int z, string_pos = 0;
	
	z = c->instream_offset;

	while (c->instream[c->instream_offset++] != '\n' &&
		   c->instream[c->instream_offset] != 0)
	{
		if (c->instream_offset > 100)
		{
			break;
		}
	}

	string_pos = 0;
	
	/* Enter the chars into the new string from where it is needed.  */
	for (; z < (c->instream_offset - 1); z++)
	{
		string[string_pos] = c->instream[z];
		string_pos++;

		if (string_pos >= size)
		{
			break;
		}
	}
	
	return string_pos;
}

/**
 * Read a login string from a music client, and place it into the string that
 * was passed as an argument to this function.
 * @param *c Music client instance.
 * @param string[] String to put the result into.
 * @param size Size of the passed string.
 * @return Returns char array with the data in it.
 */
int read_login_music_string(struct music_client *c, char string[], int size) 
{
	int z, string_pos = 0;
	
	z = c->instream_offset;

	while (c->instream[c->instream_offset++] != '\n' &&
		   c->instream[c->instream_offset] != 0)
	{
		if (c->instream_offset > 100)
		{
			break;
		}
	}

	string_pos = 0;
	
	/* Enter the chars into the new string from where it is needed.  */
	for (; z < (c->instream_offset - 1); z++)
	{
		string[string_pos] = c->instream[z];
		string_pos++;

		if (string_pos >= size)
		{
			break;
		}
	}
	
	return string_pos;
}

/**
 * Read a login string from the client, places it into the passed string.
 * @param *c Client instance.
 * @param string[] String to put the result into.
 * @param string_size Size of the passed string.
 */
void read_string(struct client *c, char string[], int string_size) 
{
	int z, size, string_pos;
	/* Size is set to one less than sent so that the newline character
	   that ends the special Runescape string doesn't get read.  */
	size = read_unsigned_byte(c);
	size--;
	z = c->instream_offset;
	
	/* Blank string before written to ensure no garbage chars.  */
	memset(string, '\0', (string_size / sizeof(char)));
	string_pos = 0;
	
	/* Enter the chars into the new string from where it is needed.  */
	for (; z < (c->instream_offset + size) && string_pos < size; z++)
	{
		if (c->instream[z] != '\n')
		{
			string[string_pos] = read_unsigned_byte(c);
			string_pos++;
		}
		else
		{
			break;
		}
	}
	/* Removes the newline.  */
	read_unsigned_byte(c);
}

/**
 * Read an unsigned byte from the client.
 * @param *c Client instance.
 * @return Unsigned byte read from the client.
 */
char read_unsigned_byte(struct client *c) 
{
	return c->instream[c->instream_offset++];
}

/**
 * Read an unsigned byte from a music client.
 * @param *c Music client instance.
 * @return Unsigned byte read from the music client.
 */
char read_unsigned_music_byte(struct music_client *c) 
{
	return c->instream[c->instream_offset++];
}

/**
 * Read an unsigned byte from the client.
 * @param *c Client instance.
 * @note 128 is subtracted from the byte read.
 * @return Unsigned byte read from the client.
 */
char read_unsigned_byte_a(struct client *c) 
{
	return (c->instream[c->instream_offset++] - 128);
}

/**
 * Read an unsigned byte from the client.
 * @param *c Client instance.
 * @note Byte sign is flipped, negative positive.
 * @return Unsigned byte read from the client.
 */
char read_unsigned_byte_c(struct client *c) 
{
	return -c->instream[c->instream_offset++];
}

/**
 * Read an unsigned byte from the client.
 * @param *c Client instance.
 * @note 128 - read byte.
 * @return Unsigned byte read from the client.
 */
char read_unsigned_byte_s(struct client *c)
{
	return (128 - c->instream[c->instream_offset++]);
}

/**
 * Read 2 byte int from the client.
 * @param *c Client instance.
 * @note Order: 1 2
 * @return Int read from the client.
 */
int read_unsigned_word(struct client *c) 
{
	c->instream_offset += 2;
	return ((c->instream[c->instream_offset - 2] & 0xff) << 8)
			+ (c->instream[c->instream_offset - 1] & 0xff);
}

/**
 * Read 2 byte int from the client.
 * @param *c Client instance.
 * @note Order: 1 2, 2nd byte has 128 subtracted from it.
 * @return Int read from the client.
 */
int read_unsigned_word_a(struct client *c) 
{
	c->instream_offset += 2;
	return ((c->instream[c->instream_offset - 2] & 0xff) << 8)
			+ ((c->instream[c->instream_offset - 1] - 128) & 0xff);
}

/**
 * Read 2 byte int from the client.
 * @param *c Client instance.
 * @note Order: 2 1
 * @return Int read from the client.
 */
int read_unsigned_word_bigendian(struct client *c)
{
	c->instream_offset += 2;
	return ((c->instream[c->instream_offset - 1] & 0xff) << 8)
			+ (c->instream[c->instream_offset - 2] & 0xff);
}

/**
 * Read 2 byte int from the client.
 * @param *c Client instance.
 * @note Order: 2 1, 2nd byte has 128 subtracted from it.
 * @return Int read from the client.
 */
int read_unsigned_word_bigendian_a(struct client *c)
{
	c->instream_offset += 2;
	return ((c->instream[c->instream_offset - 1] & 0xff) << 8)
			+ ((c->instream[c->instream_offset - 2] - 128) & 0xff);
}

/**
 * Write a 24 bit int to the outstream.
 * @param *c Client instance.
 * @param i Value to send.
 */
void write_3byte(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 16);
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write a set number of bits to a client's outstream buffer.
 * @param *c Client instance.
 * @param num_bits Bits to write.
 * @param value Number to write.
 */
void write_bits(struct client *c, int num_bits, int value) 
{
	int byte_pos, bit_offset;

	byte_pos = c->bit_outstream >> 3;
	bit_offset = 8 - (c->bit_outstream & 7);
	c->bit_outstream += num_bits;

	for (; num_bits > bit_offset; bit_offset = 8) 
	{
		c->outstream[byte_pos] &= ~BIT_MASKS[bit_offset];
		c->outstream[byte_pos++] |= (value >> (num_bits - bit_offset))
				& BIT_MASKS[bit_offset];

		num_bits -= bit_offset;
	}

	if (num_bits == bit_offset) 
	{
		c->outstream[byte_pos] &= ~BIT_MASKS[bit_offset];
		c->outstream[byte_pos] |= value & BIT_MASKS[bit_offset];
	} 
	else 
	{
		c->outstream[byte_pos] &= ~(BIT_MASKS[num_bits] 
								  << (bit_offset - num_bits));
		c->outstream[byte_pos] |= (value & BIT_MASKS[num_bits]) 
								  << (bit_offset - num_bits);
	}
}

/**
 * Write a set number of bits to the update block.
 * @param num_bits Bits to write.
 * @param value Number to write.
 */
void write_bits_update(int num_bits, int value) 
{
	int byte_pos, bit_offset;

	byte_pos = UPDATE_BLOCK->bit_update_blocks >> 3;
	bit_offset = 8 - (UPDATE_BLOCK->bit_update_blocks & 7);
	UPDATE_BLOCK->bit_update_blocks += num_bits;

	for (; num_bits > bit_offset; bit_offset = 8) 
	{
		UPDATE_BLOCK->update_blocks[byte_pos] &= ~BIT_MASKS[bit_offset];
		UPDATE_BLOCK->update_blocks[byte_pos++] 
			|= (value >> (num_bits - bit_offset)) & BIT_MASKS[bit_offset];

		num_bits -= bit_offset;
	}

	if (num_bits == bit_offset) 
	{
		UPDATE_BLOCK->update_blocks[byte_pos] &= ~BIT_MASKS[bit_offset];
		UPDATE_BLOCK->update_blocks[byte_pos] |= value & BIT_MASKS[bit_offset];
	} 
	else 
	{
		UPDATE_BLOCK->update_blocks[byte_pos] 
			&= ~(BIT_MASKS[num_bits] << (bit_offset - num_bits));
		UPDATE_BLOCK->update_blocks[byte_pos] 
			|= (value & BIT_MASKS[num_bits]) << (bit_offset - num_bits);
	}
}


/**
 * Write a byte to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_byte(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write a byte to the music client outstream.
 * @param *c Music client instance.
 * @param i Value to write.
 */
void write_music_byte(struct music_client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write a byte to the client player_props buffer.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_byte_pprops(struct client *c, int i) 
{
	c->player_props[c->player_props_offset++] = (char) i;
}

/**
 * Write a byte to the update_block array.
 * @param i Value to write.
 */
void write_byte_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
}

/**
 * Write a byte to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note 128 is added to the value before sent.
 */
void write_byte_a(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i + 128);
}

/**
 * Write a byte to the update block.
 * @param i Value to write.
 * @note 128 is added to the value before sent.
 */
void write_byte_a_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i + 128);
}

/**
 * Write a byte to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Byte sign is inverted before sent.
 */
void write_byte_c(struct client *c, int i)
{
	c->outstream[c->outstream_offset++] = (char) (i * -1);
}

/**
 * Write a byte to the update block.
 * @param i Value to write.
 * @note Byte sign is inverted before sent.
 */
void write_byte_c_update(int i)
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
	= (char) (i * -1);
}

/**
 * Write a byte to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Byte value is subtracted from 128 before sent.
 */
void write_byte_s(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (128 - i);
}

/**
 * Write a byte to the update block.
 * @param i Value to write.
 * @note Byte value is subtracted from 128 before sent.
 */
void write_byte_s_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
	= (char) (128 -i);
}

/**
 * Write a string of bytes to the client outstream.
 * @param *c Client instance.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param start Start position of string.
 */
void write_bytes(struct client *c, char string[], int offset, int start) 
{
	int k;

	for (k = start; k < (start + offset); k++)
		c->outstream[c->outstream_offset++] = string[k];
}

/**
 * Write a string of bytes to the update block.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param start Start position of string.
 */
void write_bytes_update(char string[], int offset, int start) 
{
	int k;

	for (k = start; k < (start + offset); k++)
	{
		UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= string[k];
	}
}

/**
 * Write a string of bytes to the client outstream.
 * @param *c Client instance.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param start Start position of string.
 * @note 128 is added to byte before sent.
 */
void write_bytes_a(struct client *c, char string[], int offset, int start)
{
	int k;

	for (k = start; k < (start + offset); k++)
	{
		c->outstream[c->outstream_offset++] = (char) (string[k] + 128);
	}
}

/**
 * Write a string of bytes to the update block.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param start Start position of string.
 * @note 128 is added to byte before sent.
 */
void write_bytes_a_update(char string[], int offset, int start)
{
	int k;
	for (k = start; k < (start + offset); k++)
	{
		UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (string[k] + 128);
	}
}

/**
 * Write a string of bytes in reverse order to the outstream.
 * @param *c Client instance.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param end Ending position of string.
 */
void write_bytes_reverse(struct client *c, char string[], int offset, int end)
{
	int k;

	for (k = (end + offset) - 1; k >= end; k--)
	{
		c->outstream[c->outstream_offset++] = string[k];
	}
}

/**
 * Write a string of bytes in reverse order to the update block.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param end Ending position of string.
 */
void write_bytes_reverse_update(char string[], int offset, int end)
{
	int k;
	for (k = (end + offset) - 1; k >= end; k--)
	{
		UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
			= string[k];
	}
}


/**
 * Write a string of bytes in reverse order to the outstream.
 * @param *c Client instance.
 * @param string[] String to write to buffer.
 * @param offset Offset of string to write.
 * @param end Ending position of string.
 * @note 128 is added to the byte before being sent.
 */
void write_bytes_reverse_a(struct client *c, char string[], int offset, 
						   int end)
{
	int k;

	for (k = (end + offset) - 1; k >= end; k--)
	{
		c->outstream[c->outstream_offset++] = (char) (string[k] + 128);
	}
}

/**
 * Write a 4 byte int to the client's outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Writes MSB to LSB, 1 2 3 4
 */
void write_dword(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 24);
	c->outstream[c->outstream_offset++] = (char) (i >> 16);
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write a 4 byte int to the update block.
 * @param i Value to write.
 */
void write_dword_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 24);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 16);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
}

/**
 * Write a 4 byte int to the client's outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Order: 3 4 1 2
 */
void write_dword_v1(struct client *c, int i)
{
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) i;
	c->outstream[c->outstream_offset++] = (char) (i >> 24);
	c->outstream[c->outstream_offset++] = (char) (i >> 16);
}

/**
 * Write a 4 byte int to the client's outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Order: 2 1 4 3
 */
void write_dword_v2(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 16);
	c->outstream[c->outstream_offset++] = (char) (i >> 24);
	c->outstream[c->outstream_offset++] = (char) i;
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
}

/**
 * Write a 4 byte int to the update block.
 * @param i Value to write.
 * @note Order: 2 1 4 3
 */
void write_dword_v2_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 16);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 24);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
}

/**
 * Write a 4 byte int to the client's outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Writes LSB to MSB, 4 3 2 1
 */
void write_dword_bigendian(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) i;
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) (i >> 16);
	c->outstream[c->outstream_offset++] = (char) (i >> 24);
}

/**
 * Write a 4 byte int to the update block.
 * @param i Value to write.
 * @note Writes LSB to MSB, 4 3 2 1
 */
void write_dword_bigendian_update(int i)
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 16);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 24);
}

/**
 * Write the size of the packet.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_frame_size(struct client *c, int i) 
{
	c->outstream[c->outstream_offset - i - 1] = (char) i;
}

/**
 * Write the size of the packet, 2 byte int.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_frame_size_word(struct client *c, int i) 
{
	c->outstream[c->outstream_offset - i - 2] = (char) (i >> 8);
	c->outstream[c->outstream_offset - i - 1] = (char) i;
}

/**
 * Write a 64 bit int to the client outstream.
 * @param *c Client instance.
 * @param l uint64_t value to write.
 */
void write_qword(struct client *c, uint64_t l) 
{
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 56);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 48);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 40);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 32);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 24);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 16);
	c->outstream[c->outstream_offset++] = (char) (int) (l >> 8);
	c->outstream[c->outstream_offset++] = (char) (int) l;
}

/**
 * Write a 64 bit int to the client player props buffer.
 * @param *c Client instance.
 * @param l uint64_t value to write.
 */
void write_qword_pprops(struct client *c, uint64_t l) 
{
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 56);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 48);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 40);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 32);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 24);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 16);
	c->player_props[c->player_props_offset++] = (char) (int) (l >> 8);
	c->player_props[c->player_props_offset++] = (char) (int) l;
}

/**
 * Write a string to the client outstream.
 * @param *c Client instance.
 * @param string[] String to write to the buffer.
 */
void write_string(struct client *c, char string[]) 
{
	int z, string_pos, string_max;

	string_pos = 0;
	string_max = ((int) strlen(string)) + c->outstream_offset;
	
	/* Enter the chars into the new string from where it is needed.  */
	for (z = c->outstream_offset; z < string_max; z++)
	{
		c->outstream[z] = string[string_pos];
		string_pos++;
	}
		
	c->outstream_offset += ((int) strlen(string));
	c->outstream[c->outstream_offset++] = '\n';
}

/**
 * Write a string to the update block.
 * @param string[] String to write to the buffer.
 */
void write_string_update(char string[]) 
{
	int z, string_pos, string_max;

	string_pos = 0;
	string_max = ((int) strlen(string)) + UPDATE_BLOCK->update_blocks_offset;
	
	/* Enter the chars into the new string from where it is needed.  */
	for (z = UPDATE_BLOCK->update_blocks_offset; z < string_max; z++)
	{
		UPDATE_BLOCK->update_blocks[z] = string[string_pos];
		string_pos++;
	}
		
	UPDATE_BLOCK->update_blocks_offset += ((int) strlen(string));
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] = '\n';
}

/**
 * Write 2 byte int to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_word(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write a 2 byte int to a music client.
 * @param *c Music client instance.
 * @param i Value to write.
 */
void write_music_word(struct music_client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) i;
}

/**
 * Write 2 byte int to the client player props.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_word_pprops(struct client *c, int i) 
{
	c->player_props[c->player_props_offset++] = (char) (i >> 8);
	c->player_props[c->player_props_offset++] = (char) i;
}


/**
 * Write 2 byte int to the update block.
 * @param i Value to write.
 */
void write_word_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
}

/**
 * Write 2 byte int to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note 2nd byte has 128 added to it.
 */
void write_word_a(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
	c->outstream[c->outstream_offset++] = (char) (i + 128);
}

/**
 * Write 2 byte int to the update block.
 * @param i Value to write.
 * @note 2nd byte has 128 added to it.
 */
void write_word_a_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i + 128);
}

/**
 * Write 2 byte int to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Order: 1 2
 */
void write_word_bigendian(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) i;
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
}

/**
 * Write 2 byte int to the update block.
 * @param i Value to write.
 * @note Order: 1 2
 */
void write_word_bigendian_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) i;
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
}

/**
 * Write 2 byte int to the client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 * @note Order: 1 2, 1st byte has 128 added to it.
 */
void write_word_bigendian_a(struct client *c, int i) 
{
	c->outstream[c->outstream_offset++] = (char) (i + 128);
	c->outstream[c->outstream_offset++] = (char) (i >> 8);
}

/**
 * Write 2 byte int to the update block.
 * @param i Value to write.
 * @note Order: 1 2, 1st byte has 128 added to it.
 */
void write_word_bigendian_a_update(int i) 
{
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i + 128);
	UPDATE_BLOCK->update_blocks[UPDATE_BLOCK->update_blocks_offset++] 
		= (char) (i >> 8);
}

/**
 * Write a smart byte, type b client outstream.
 * @param *c Client instance.
 * @param i Value to write.
 */
void write_smart_b(struct client *c, int i)
{
	if (i >= 128)
	{
		write_word(c, (i + 32768));
	}
	else
	{
		write_byte(c, i);
	}
}

/**
 * Convert a player's name to a 64 bit int.
 * @param name[] Player's name string.
 * @return 64 bit int conversion of the passed name.
 */
long convert_player_name_to_int64(char name[]) 
{
	long l = 0L;
	int i;
	char c;

	for (i = 0; (name[i] != '\0') && (i < 12); i++) 
	{
		c = name[i];
		l *= 37L;

		if ((c >= 'A') && (c <= 'Z'))
		{
			l += (1 + c) - 65;
		}
		else if ((c >= 'a') && (c <= 'z'))
		{
			l += (1 + c) - 97;
		}
		else if ((c >= '0') && (c <= '9'))
		{
			l += (27 + c) - 48;
		}
	}

	while ((l % 37L == 0L) && (l != 0L))
	{
		l /= 37L;
	}

	return l;
}
