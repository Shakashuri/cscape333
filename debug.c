/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file debug.c
 * @brief Contains functions related to debugging errors, printing debug 
 * output.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "object.h"
#include "player.h"
#include "regions.h"
#include "shops.h"
#include "skill_agility.h"
#include "stream.h"

/**
 * @brief Prints out the contents of a player's outstream buffer from the
 * start to a certain point.
 *
 * @param *c Client instance.
 * @param limit To what index should be printed out.
 */
void print_outstream(struct client *c, int limit)
{
	int z;

	for (z = 0; z < limit; z++)
	{
		printf("%d,", (unsigned char) c->outstream[z]);
	}

	printf("\n");
}

/**
 * @brief Prints out the contents of a player's instream buffer from the
 * start to a certain point.
 *
 * @param *c Client instance.
 * @param limit To what index should be printed out.
 */
void print_instream(struct client *c, int limit)
{
	int z;

	for (z = 0; z < limit; z++)
	{
		if (c->instream_offset == z)
		{
			printf("{%d} ", (int) c->instream[z]);
		}
		else
		{
			printf("%d ", (int) c->instream[z]);
		}
	}
	printf("\nUNSIGNED:\n");
	for (z = 0; z < limit; z++)
	{
		if (c->instream_offset == z)
		{
			printf("{%d} ", (unsigned char) c->instream[z]);
		}
		else
		{
			printf("%d ", (unsigned char) c->instream[z]);
		}
	}
	printf("\n");
}

/**
 * @brief Prints out the contents of the update blocks buffer from the
 * start to a certain point.
 *
 * @param limit To what index should be printed out.
 */
void print_update_blocks(int limit)
{
	int z;

	for (z = 0; z < limit; z++)
	{
		printf("%d ", (int) UPDATE_BLOCK->update_blocks[z]);
	}

	printf("\n");
}

/**
 * @brief Prints out the contents of a player's prop's buffer from the
 * start to a certain point.
 *
 * @param *c Client instance.
 * @param limit To what index should be printed out.
 */
void print_player_props(struct client *c, int limit)
{
	int z;

	for (z = 0; z < limit; z++)
	{
		printf("%d ", (int) c->player_props[z]);
	}

	printf("\n");
}

/**
 * Prints out the last packets that the player was sent from the server.
 * @param *c Client instance.
 */
void print_player_last_packets(struct client *c)
{
	int z;

	for (z = 0; z < LAST_PACKETS_SIZE; z++)
	{
		printf("%d ", (int) c->last_packets[z]);
	}

	printf("\n");
}

/**
 * Prints out what damage the player has taken, and from where.
 * @param *c Client instance.
 * @param idx Index of the hit applied.
 */
void print_player_damage_log(struct client *c, int idx)
{
	/* Print out npc hit debug info.  */
	switch (c->plr->hit_from[idx])
	{
		case 0:
			printf("Player %s hit from NPC idx: %d for %d dmg!\n",
				   c->plr->name, c->plr->hit_index[idx],
				   c->plr->hit_diff);
			break;
		case 1:
			printf("Player %s hit from plr idx: %d for %d dmg!\n",
				   c->plr->name, c->plr->hit_index[idx],
				   c->plr->hit_diff);
			break;
		default:
			printf("Player %s hit from unknown for %d dmg!\n",
				   c->plr->name, c->plr->hit_diff);
			break;
	}
}

/**
 * Prints out basic info about a npc located at a current spawn index.
 * @param *idx Index in the NPC_SPAWNS array to check.
 */
void print_npc_index_info(int idx)
{
	if (idx < 0 || idx > NUM_NPC_SPAWNS)
	{
		printf("Index out of bounds for npc spawns check.\n");
		return;
	}

	if (NPC_SPAWNS[idx] == NULL)
	{
		printf("Spawn slot at index %d empty!\n", idx);
		return;
	}

	printf("NPC Index info:\n");
	printf("Name: %s ID: %d idx: %d\n", NPCS[NPC_SPAWNS[idx]->npc_id]->name, 
		   NPC_SPAWNS[idx]->npc_id, idx);
	printf("Location x: %d y: %d\n", NPC_SPAWNS[idx]->world_x, NPC_SPAWNS[idx]->world_y);
}

/**
 * Print out an NPC's AI state, and their movement AI state.
 * @param idx Index of the NPC.
 */
void print_npc_ai_state(int idx)
{
	printf("NPC Index info:\n");
	printf("Name: %s ID: %d idx: %d\n", NPCS[NPC_SPAWNS[idx]->npc_id]->name, 
		   NPC_SPAWNS[idx]->npc_id, idx);
	printf("Location x: %d y: %d\n", NPC_SPAWNS[idx]->world_x, NPC_SPAWNS[idx]->world_y);

	printf("AI State: ");
	switch (NPC_SPAWNS[idx]->ai_state)
	{
		case AI_IDLE:
			printf("AI_IDLE\n");
			break;
		case AI_ATTACK:
			printf("AI_ATTACK\n");
			break;
		case AI_DEAD:
			printf("AI_DEAD\n");
			break;
		case AI_RESPAWN:
			printf("AI_RESPAWN\n");
			break;
		case AI_TALK:
			printf("AI_TALK\n");
			break;
	}
}

/**
 * Checks the state of the outstream buffer, ensures that the user is warned
 * if there is less than 100 slots left in the buffer.
 * @param *c Client instance.
 */
void check_outstream_buffer_limit(struct client *c)
{
	if ((BUFFER_SIZE - c->outstream_offset) < 100)
	{
		printf("### WARNING ###\n");
		printf("Increase BUFFER_SIZE in constants.h!\n");
		printf("Too close to the limit!\n");
		printf("Errors may occur if not increased!\n");
		printf("Size: %d offset: %d\n", BUFFER_SIZE, 
			   c->outstream_offset);
		return;
	}
}

/**
 * Checks the state of the player properties buffer, ensures that the user is
 * warned if there is less than 100 slots left in the buffer.
 * @param *c Client instance.
 */
void check_pprops_buffer_limit(struct client *c)
{
	if ((PLAYER_PROPS_SIZE - c->player_props_offset) < 20)
	{
		printf("### WARNING ###\n");
		printf("Increase PLAYER_PROPS_SIZE in constants.h!\n");
		printf("Too close to the limit!\n");
		printf("Errors may occur if not increased!\n");
		printf("Size: %d offset: %d\n", PLAYER_PROPS_SIZE, 
			   c->player_props_offset);
		return;
	}
}

/**
 * Checks the state of the update block buffer, ensures that the user is
 * warned if there is less than 100 slots left in the buffer.
 */
void check_update_block_buffer_limit(void)
{
	if ((UPDATE_BLOCK_SIZE - UPDATE_BLOCK->update_blocks_offset) < 100)
	{
		printf("### WARNING ###\n");
		printf("Increase UPDATE_BLOCK_SIZE in constants.h!\n");
		printf("Too close to the limit!\n");
		printf("Errors may occur if not increased!\n");
		return;
	}
}

/**
 * @brief Does some checking on object data to ensure no issues will be caused
 * by wrong entries.
 *
 * @param *o Object definition instance.
 * @param count Number that the object instance is, used for identification.
 */
void check_object_data_for_errors(struct object *o, int count)
{
	if (o->is_double_door == 0 && (o->door_type == 2 || o->door_type == 3))
	{
		printf("ERROR: Object double door pair id not set %d\n", count);
	}
}

/**
 * @brief Does some checking on item data to ensure no issues will be caused by
 * wrong entries.
 *
 * @param *i Item definition instance.
 * @param count Number that the item instance is, used for identification.
 */
void check_item_data_for_errors(struct item *i, int count)
{
	if (i->slot == WEAPON_SLOT)
	{
		if (i->atk_speed == 0)
		{
			printf("ERROR: Atk speed not set %d\n", count);
		}
		if (i->reach == 0)
		{
			printf("ERROR: Reach not set %d\n", count);
		}
		if (i->stand_anim == 0)
		{
			printf("ERROR: Stand animation not set %d\n", count);
		}
		if (i->walk_anim == 0)
		{
			printf("ERROR: Walk animation not set %d\n", count);
		}
		if (i->run_anim == 0)
		{
			printf("ERROR: Run animation not set %d\n", count);
		}

		if (i->attacking_emotes[0] == 0 || i->attacking_emotes[1] == 0
			|| i->attacking_emotes[2] == 0 || i->attacking_emotes[3] == 0)
		{
			printf("ERROR: Attack animation not set %d\n", count);
		}
		if (i->block_anim == 0)
		{
			printf("ERROR: Weapon block animation not set %d\n", count);
		}
		if (i->atk_type == 0)
		{
			printf("ERROR: Attack menu not set %d\n", count);
		}
		if (i->ranged == 1)
		{
			if (i->ammo_type == 0)
			{
				printf("ERROR: Ammo type not set %d\n", count);
			}
		}
	}
	else if (i->slot == SHIELD_SLOT)
	{
		if (i->block_anim == 0)
		{
			printf("ERROR: Block animation not set %d\n", count);
		}
	}
	else if (i->slot == AMMO_SLOT)
	{
		if (i->draw_gfx == 0)
		{
			printf("ERROR: Draw gfx not set %d\n", count);
		}
		if (i->shoot_gfx == 0)
		{
			printf("ERROR: Shoot gfx not set %d\n", count);
		}
		if (i->ranged_strength == 0)
		{
			printf("ERROR: Ranged strength not set %d\n", count);
		}
	}
	else if (i->slot != -1 && i->noted == 1)
	{
		printf("ERROR: Noted item has slot %d\n", count);
	}
	else if (i->noted == 1 && i->unnoted_id == -1)
	{
		printf("ERROR: Noted item has no unnoted id %d\n", count);
	}
	else if (i->stacking == 1 
			 && !is_double_about_equal(i->weight, 0.0, EPSILON))
	{
		printf("ERROR: Stackable item has weight %d\n", count);
	}
	else if (i->noted == 1 && !is_double_about_equal(i->weight, 0.0, EPSILON))
	{
		printf("ERROR: Noted item has weight %d\n", count);
	}
	else if (is_double_about_equal(i->weight, 0.0, EPSILON) && i->slot != -1)
	{
		printf("ERROR: Equipt item has no weight %d\n", count);
	}
	else if (i->tradable == 1 && i->spec_price == 0)
	{
		printf("ERROR: Tradable item has no value %d\n", count);
	}
}

/**
 * Check item spawn data for any sort of issues.
 *
 * @param *i Item spawn instance.
 * @param count Number that the item spawn instance is, used for identification.
 */
void check_item_spawn_data_for_errors(struct item_drop *i, int count)
{
	int world_region = get_region_id(i->world_x, i->world_y);

	count = count;
	if (get_region_index(world_region) == -1)
	{
		printf("ERROR: Item Spawn in invalid region %d\n", count);
		printf("\tX: %d Y: %d Z: %d\n", i->world_x, i->world_y, i->world_z);
	}
}

/**
 * @brief Does some checking on NPC data to ensure no issues will be caused by
 * wrong entries.
 *
 * @param *n NPC definition instance.
 * @param count Number that the NPC instance is, used for identification.
 */
void check_npc_data_for_errors(struct npc *n, int count)
{
	if (n->attackable == 1 && n->atk_skill == 0)
	{
		printf("ERROR: NPC atk skill set to 0 while it can be attacked %d\n", 
			   count);
	}
	if (n->health == 0 && n->attackable == 1)
	{
		printf("ERROR: NPC wrongly set to attackable %d\n", count);
	}
	if (n->size == 0)
	{
		printf("ERROR: NPC size not set %d\n", count);
	}
	if (n->melee_range == 0)
	{
		printf("ERROR: NPC melee range not set %d\n", count);
	}
	if (n->attackable == 1 && n->health == 0)
	{
		printf("ERROR: NPC hp not set %d\n", count);
	}
	if (n->stand_anim == 0)
	{
		printf("ERROR: NPC stand animation not set %d\n", count);
	}
	if (n->retaliates == 1 && n->melee_stats[ATK_ANIM] == 0 
		&& n->ranged_stats[ATK_ANIM] == 0 && n->num_spells == 0)
	{
		printf("ERROR: NPC has no attack animations set %d\n", count);
	}
	if (n->attackable == 1 && n->melee_stats[ATK_ANIM] > PROTOCOL_MAX_EMOTES)
	{
		printf("ERROR: NPC melee attack animation set too high %d\n", count);
	}
	if (n->health > 0 && n->attackable == 1 && n->level != 0)
	{
		int calc = get_npc_combat_level(n);
		if (n->level != calc)
		{
			printf("ERROR: NPC combat level differs %30s %5d Current level: %3d "
				   "calc level: %4d\n", n->name, count, n->level, calc);
		}
	}
	if (n->magic_spells[0] != 0 && n->chance_to_use_magic == 0)
	{
		printf("ERROR: NPC has no chance to use magic, but has spells %d\n", 
			   count);
	}
}

/**
 * Check npc spawn data for any sort of issues.
 *
 * @param *n NPC spawn instance.
 * @param count Number that the NPC spawn instance is, used for identification.
 */
void check_npc_spawn_data_for_errors(struct npc_spawn *n, int count)
{
	int spawn_region = get_region_id(n->spawn_x, n->spawn_y);

	if (get_region_index(spawn_region) == -1)
	{
		printf("ERROR: NPC Spawn in invalid region %d\n", count);
		printf("\tX: %d Y: %d Z: %d\n", n->spawn_x, n->spawn_y, n->spawn_z);
	}
}

/**
 * Check loaded shop data for any issues.
 * @param *s Shop info struct to check.
 * @param count ID of the shop, used for identification.
 */
void check_shop_data_for_errors(struct shop_info *s, int count)
{
	int x = 0;

	for (x = 0; x < SHOP_SIZE; x++)
	{
		/* Check if there are any items with invalid ID's.  */
		if (s->items[x] != -1)
		{
			if (is_item_id_invalid(s->items[x]) == 1)
			{
				printf("ERROR: Shop %d: %s at slot %d has an invalid item\n", 
					   count, s->name, x);
			}
			else
			{
				if (ITEMS[s->items[x]]->spec_price <= 0)
				{
					printf("ERROR: Shop %d is selling item %d with no spec price\n",
						count, s->items[x]);
				}
			}
		}
	}
}

/**
 * Check loaded agility data for any issues.
 * @param *a Obstacle info struct to check.
 * @param count ID of the obstacle, used for identification.
 */
void check_agility_data_for_errors(struct agility_obstacle_data *a, int count)
{
	int x = 0;

	if (a->is_course == 1)
	{
		/* Gnome stronghold.  */
		if (a->course_id == 0)
		{
			/* De-hardcode this. TODO.  */
			if (a->course_obstacle_id != 7)
			{
				printf("Agility course Gnome Stronghold has the wrong\n");
				printf("number of obstacles! Has: %d Needs: %d\n", 
					   a->course_obstacle_id, 7);
			}
		}
	}
	else
	{
		if (a->num_stages < 1)
		{
			printf("ERROR: Obstacle %d has 0 or less stages!\n", count);
		}

		for (x = 0; x < a->num_stages; x++)
		{
			if (a->stages[x].is_walk <= 0 || a->stages[x].is_walk > 4)
			{
				printf("ERROR: Obstacle %d stage %d has bad walk type!\n",
					count, x);
			}
		}
	}
}
