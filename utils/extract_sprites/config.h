#ifndef _CONFIG_H_
#define _CONFIG_H_

#define TEXT_SIZE 300

struct sprite
{
	int *pixels;
	int width;
	int height;
	int max_width;
	int max_height;
	int offset_x;
	int offset_y;
};

struct interface
{
	int id;
	int parent_id;
	int type;
	int action_type;
	int content_type;
	/* Dimensions.  */
	int width;
	int height;
	/* Transparency.  */
	int alpha;
	int hovered_popup;
	int condition_count;
	int *condition_type;
	int *condition_value;

	/* Part of type 2, action_type 2.  */
	char *selected_action_name;
	char *spell_name;
	int spell_usable_on;
	
	int scroll_max;
	int hover_only;

	/* Children of the interface, how many, sizes?  */
	int num_children;
	int *children;
	int *child_x;
	int *child_y;
	
	int *inventory_item_id;
	int *inventory_stack_size;
	int item_swapable;
	int inventory;
	int usable_item_interface;
	int item_deletes_dragged;
	int item_swappable;
	int inventory_sprite_padding_vert;
	int inventory_sprite_padding_hort;

	struct sprite *sprites;
	int *sprites_x;
	int *sprites_y;

	int num_actions;
	char **actions;

	int filled;
	int *fonts;
	int text_centered;
	int text_drawing_areas;
	int text_shadowed;
	/* Default size of these set by TEXT_SIZE.  */
	char *tooltip;
	char *text_default;
	char *text_active;

	int color_default;
	int color_active;
	int color_default_hover;
	int color_active_hover;

	int opcode_count;
	int sub_opcode_count;
	int **opcodes;

	/* Type 7 - 3D model holding interface.  */
	int model_id_default;
	int model_type_default;
	int model_id_active;
	int model_type_active;
	int animation_id_default;
	int animation_id_active;
	int model_zoom;
	int model_rotation_x;
	int model_rotation_y;
};

#endif
