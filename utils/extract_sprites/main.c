#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

/* Filenames need to be changed for each version.  */
/* 333 version filenames.  */
char file_name[100] = {"2/8297314.dat"};

struct interface *INTERFACES;

struct sprite sprites[5000];
int sprite_offset = 0;

void set_defaults(struct interface *i)
{
	i->id = -1;
	i->parent_id = -1;
	i->type = -1;
	i->action_type = -1;
	i->content_type = -1;

	i->num_children = -1;

	i->condition_type = NULL;
	i->condition_value = NULL;
	i->opcodes = NULL;
	i->sprites = NULL;
	i->tooltip = NULL;
	i->fonts = NULL;
	i->text_active = NULL;
	i->text_default = NULL;
	i->spell_name = NULL;
	i->selected_action_name = NULL;
}

/** 
 * Converts a char to its uppercase version.
 * @param letter Char to change case of.
 * @return Returns the uppercase version of a letter.
 */
char to_upper(char letter)
{
	if (letter < 123 && letter > 96)
	{
		letter -= 32;
	}
	return letter;
}

long sprite_name_to_hash(char *name)
{
	int x = 0;
	char hash_upper[200] = {'\0'};
	char test = '\0';
	long name_hash = 0L;

	for (x = 0; x < 9999; x++)
	{
		test = name[x];

		if (test == '\0')
		{
			break;
		}

		hash_upper[x] = to_upper(test);
	}

	for (x = 0; x < strlen(name); x++)
	{
		name_hash = (name_hash * 61L + hash_upper[x]) - 32L;
		name_hash = name_hash + (name_hash >> 56) & 0xffffffffffffffL;
	}

	return name_hash;
}

void get_sprite(int sprite_id, char *name)
{
	long sprite_hash = (sprite_name_to_hash(name) << 8) + sprite_id;

	printf("sprite_hash: %ld\n", sprite_hash);
}

/* Loops through the data file, reading the info from each interface and 
   placing them in the INTERFACES array.  */
void read_interfaces(char buffer[], int *position, int buffer_size)
{
	int x = 0, y = 0;
	int id = -1;
	int parent_id = -1;
	
	int intr_offset = 0;

	struct interface *intr;
	
	while (*position < buffer_size)
	{
		intr = &INTERFACES[intr_offset];
		set_defaults(intr);

		id = read_unsigned_le_short(buffer, position);

		if (id == 65535)
		{
			parent_id = read_unsigned_le_short(buffer, position);
			id = read_unsigned_le_short(buffer, position);
		}

		intr->id = id;
		intr->parent_id = parent_id;
		intr->type = read_unsigned_byte(buffer, position);
		intr->action_type = read_unsigned_byte(buffer, position);
		intr->content_type = read_unsigned_le_short(buffer, position);
		intr->width = read_unsigned_le_short(buffer, position);
		intr->height = read_unsigned_le_short(buffer, position);
		intr->alpha = read_unsigned_byte(buffer, position);
		intr->hovered_popup = read_unsigned_byte(buffer, position);
		
		if (intr->hovered_popup != 0)
		{
			intr->hovered_popup = (((intr->hovered_popup - 1) << 8) 
								   + read_unsigned_byte(buffer, position));
		}
		else
		{
			intr->hovered_popup = -1;
		}

		intr->condition_count = read_unsigned_byte(buffer, position);

		if (intr->condition_count > 0)
		{
			intr->condition_type = calloc(intr->condition_count, sizeof(int));
			intr->condition_value = calloc(intr->condition_count, sizeof(int));

			for (x = 0; x < intr->condition_count; x++)
			{
				intr->condition_type[x] = read_unsigned_byte(buffer, position);
				intr->condition_value[x] 
					= read_unsigned_le_short(buffer, position);
			}
		}

		/* Read all of the opcode information about the interface.  */
		intr->opcode_count = read_unsigned_byte(buffer, position);
		
		/* If there are more than one opcode, create arrays to hold all the needed
		   data about them.  */
		if (intr->opcode_count > 0)
		{
			intr->opcodes = calloc(intr->opcode_count, sizeof(int*));
			
			for (x = 0; x < intr->opcode_count; x++)
			{
				intr->sub_opcode_count = read_unsigned_le_short(buffer, position); 
				intr->opcodes[x] = calloc(intr->sub_opcode_count, sizeof(int));
				
				for (y = 0; y < intr->sub_opcode_count; y++)
				{
					intr->opcodes[x][y] = read_unsigned_le_short(buffer, position);
				}
			}
		}
		
		if (intr->type == 0)
		{
			intr->scroll_max = read_unsigned_le_short(buffer, position);
			intr->hover_only = read_unsigned_byte(buffer, position) == 1;
			intr->num_children = read_unsigned_le_short(buffer, position);
			
			intr->children = calloc(intr->num_children, sizeof(int));
			intr->child_x = calloc(intr->num_children, sizeof(int));
			intr->child_y = calloc(intr->num_children, sizeof(int));
			
			for (x = 0; x < intr->num_children; x++)
			{
				intr->children[x] = read_unsigned_le_short(buffer, position);
				intr->child_x[x] = read_short(buffer, position);
				intr->child_y[x] = read_short(buffer, position);
			}
		}
		if (intr->type == 1)
		{
			/* Just junk data?  */
			read_unsigned_le_short(buffer, position);
			read_unsigned_byte(buffer, position);
		}
		if (intr->type == 2)
		{
			intr->inventory_item_id = calloc((intr->width * intr->height), 
											 sizeof(int));
			intr->inventory_stack_size = calloc((intr->width * intr->height), 
												sizeof(int));
			intr->item_swappable = read_unsigned_byte(buffer, position) == 1;
			intr->inventory = read_unsigned_byte(buffer, position) == 1;
			intr->usable_item_interface 
				= read_unsigned_byte(buffer, position) == 1;
			intr->item_deletes_dragged = read_unsigned_byte(buffer, position) == 1;
			
			intr->inventory_sprite_padding_vert 
				= read_unsigned_byte(buffer, position);
			intr->inventory_sprite_padding_hort 
				= read_unsigned_byte(buffer, position);
			
			intr->sprites = calloc(20, sizeof(struct sprite));
			intr->sprites_x = calloc(20, sizeof(int));
			intr->sprites_y = calloc(20, sizeof(int));
			
			/* Sprites are not handled 100% here, data is just read, not used.  */
			
			for (x = 0; x < 20; x++)
			{
				/* If sprite exists...  */
				if (read_unsigned_byte(buffer, position) == 1)
				{
					char string[100] = {0};
					int string_size = 0;

					intr->sprites_x[x] = read_short(buffer, position);	
					intr->sprites_y[x] = read_short(buffer, position);	
					
					string_size = read_string(buffer, position, string, 100);

					if (string_size > 1)
					{
						printf("\nString: %s\n", string);
					
						char *comma_idx = strrchr(string, ',');
						char *string_idx = "";
						printf("string_idx %s\n", string_idx);
						*comma_idx = '\0';
						printf("\nString trim: %s\n", string);
						comma_idx++;

						int sprite_id = 0;
						sscanf(comma_idx, "%d", &sprite_id);

						printf("sprite_id %d\n", sprite_id);
						/* get image.  */
						//get_image(sprite_id + 1, archive, string);
						get_sprite(sprite_id + 1, string);
					}
				}
			}

			intr->num_actions = 5;
			intr->actions = calloc(5, sizeof(char*));
			
			for (x = 0; x < 5; x++)
			{
				intr->actions[x] = calloc(100, sizeof(char));
				int ssize = read_string(buffer, position, 
										intr->actions[x], 100);
				
				if (ssize == 0)
				{
					intr->actions[x][0] = '\0';
				}
			}
		}
		if (intr->type == 3)
		{
			intr->filled = read_unsigned_byte(buffer, position) == 1;
		}
		if (intr->type == 4 || intr->type == 1)
		{
			intr->text_centered = read_unsigned_byte(buffer, position) == 1;
			
			/*int font = read_unsigned_byte(buffer, position);*/
			read_unsigned_byte(buffer, position);
			
			if (intr->fonts != NULL)
			{
				/* intr->text_drawing_areas = fonts[font];  */
			}

			intr->text_shadowed = read_unsigned_byte(buffer, position) == 1;
		}
		if (intr->type == 4)
		{
			if (intr->text_default == NULL)
			{
				intr->text_default = calloc(TEXT_SIZE, sizeof(char));
			}

			if (intr->text_active == NULL)
			{
				intr->text_active = calloc(TEXT_SIZE, sizeof(char));
			}

			read_string(buffer, position, intr->text_default, TEXT_SIZE);
			read_string(buffer, position, intr->text_active, TEXT_SIZE);
		}
		if (intr->type == 1 || intr->type == 3 || intr->type == 4)
		{
			intr->color_default = read_int(buffer, position);	
		}
		if (intr->type == 3 || intr->type == 4)
		{
			intr->color_active = read_int(buffer, position);
			intr->color_default_hover = read_int(buffer, position);
			intr->color_active_hover = read_int(buffer, position);
		}
		if (intr->type == 5)
		{
			char sprite_name[100] = {0};
			char sprite2_name[100] = {0};
			int sprite_len = 0;
			int sprite2_len = 0;

			sprite_len = read_string(buffer, position, sprite_name, 100);
			
			if (sprite_len > 0)
			{
				/* gets sprite image.  */
			}

			sprite2_len = read_string(buffer, position, sprite2_name, 100);

			if (sprite2_len > 0)
			{
				/* gets sprite image, same as above.  */
			}
		}
		if (intr->type == 6)
		{
			int interface_id = read_unsigned_byte(buffer, position);

			if (interface_id != 0)
			{
				intr->model_type_default = 1;	
				intr->model_id_default = ((interface_id - 1) << 8) 
										 + read_unsigned_byte(buffer, position);
			}
			interface_id = read_unsigned_byte(buffer, position);
			if (interface_id != 0)
			{
				intr->model_type_active = 1;	
				intr->model_id_active = ((interface_id - 1) << 8) 
										 + read_unsigned_byte(buffer, position);
			}

			interface_id = read_unsigned_byte(buffer, position);
			if (interface_id != 0)
			{
				intr->animation_id_default = ((interface_id - 1) << 8) 
											 + read_unsigned_byte(buffer, position);
			}
			else
			{
				intr->animation_id_default = -1;
			}
			interface_id = read_unsigned_byte(buffer, position);
			if (interface_id != 0)
			{
				intr->animation_id_active = ((interface_id - 1) << 8) 
											 + read_unsigned_byte(buffer, position);
			}
			else
			{
				intr->animation_id_active = -1;
			}
			
			intr->model_zoom = read_unsigned_le_short(buffer, position);
			intr->model_rotation_x = read_unsigned_le_short(buffer, position);
			intr->model_rotation_y = read_unsigned_le_short(buffer, position);
		}
		if (intr->type == 7)
		{
			if (intr->inventory_item_id == NULL)
			{
				intr->inventory_item_id 
					= calloc((intr->width * intr->height), sizeof(int));
			}
			if (intr->inventory_stack_size == NULL)
			{
				intr->inventory_stack_size 
					= calloc((intr->width * intr->height), sizeof(int));
			}
			intr->text_centered = read_unsigned_byte(buffer, position) == 1;
			
			/* int font = read_unsigned_byte(buffer, position); */
			read_unsigned_byte(buffer, position);
			if (intr->fonts != NULL)
			{
				/* intr->text_drawing_areas = fonts[font];  */
			}
			intr->text_shadowed = read_unsigned_byte(buffer, position) == 1;
			intr->color_default = read_int(buffer, position);
			intr->inventory_sprite_padding_vert = read_short(buffer, position);
			intr->inventory_sprite_padding_hort = read_short(buffer, position);
			intr->inventory = read_unsigned_byte(buffer, position) == 1;
			
			if (intr->actions == NULL)
			{
				intr->num_actions = 5;
				intr->actions = calloc(5, sizeof(char*));
				
				for (x = 0; x < 5; x++)
				{
					intr->actions[x] = calloc(100, sizeof(char));
					int ssize = read_string(buffer, position, intr->actions[x], 
											100);
					if (ssize == 0)
					{
						intr->actions[x][0] = '\0';
					}
				}
			}
		}
		if (intr->action_type == 2 || intr->type == 2)
		{
			if (intr->selected_action_name == NULL)
			{
				intr->selected_action_name = calloc(100, sizeof(char));
			}
			if (intr->spell_name == NULL)
			{
				intr->spell_name = calloc(100, sizeof(char));
			}

			read_string(buffer, position, intr->selected_action_name, 100);
			read_string(buffer, position, intr->spell_name, 100);
			intr->spell_usable_on = read_unsigned_le_short(buffer, position);
		}
		if (intr->type == 8)
		{
			if (intr->text_default == NULL)
			{
				intr->text_default = calloc(100, sizeof(char));
			}

			read_string(buffer, position, intr->text_default, 100);
		}
		if (intr->action_type == 1 || intr->action_type == 4 
			|| intr->action_type == 5 || intr->action_type == 6)
		{
			if (intr->tooltip == NULL)
			{
				intr->tooltip = calloc(100, sizeof(char));
			}

			int ssize = read_string(buffer, position, intr->tooltip, 100);
			
			if (ssize == 0)
			{
				switch (intr->action_type)
				{
					case 1:
						strncpy(intr->tooltip, "Ok", 100);
						break;
					case 4:
					case 5:
						strncpy(intr->tooltip, "Select", 100);
						break;
					case 6:
						strncpy(intr->tooltip, "Continue", 100);
				}
			}
		}
		intr_offset++;
	}
	printf("Reached end of buffer, closing.\n");
}

void fprint_intr_struct(struct interface *i, FILE *fp)
{
	int x = 0;
	
	if (i->children != NULL)
	{
		if (i->id == i->parent_id)
		{
			fprintf(fp, "%d\t%d\n", i->id, i->num_children);
		
			for (x = 0; x < i->num_children; x++)
			{
				fprintf(fp, "\t%d\n", i->children[x]);
			}
		}
	}
}

int main(void)
{
	FILE *fp = fopen(file_name, "r");
	FILE *fp_obj = fopen("intr_data.dat", "w");

	int num_interfaces = 0;
	int buffer_size = 0;
	int position = 0;
	int x = 0, y = 0; 

	char *buffer;
	
	if (fp == NULL)
	{
		printf("Interface data file %s failed to open.\n", file_name);
		return 1;
	}

	if (fp_obj == NULL)
	{
		printf("Could not create interface file: intr_data.dat.\n");
		return 1;
	}

	/* Create the buffers, read data from files, then close them.  */
	buffer_size = file_len(fp);
	
	printf("Buffer sizes: %d\n", buffer_size);
	
	buffer = calloc(buffer_size, sizeof(char));

	for (x = 0; x < buffer_size; x++)
	{
		buffer[x] = fgetc(fp);
	}
	printf("Data read into buffer.\n");

	fclose(fp);
	
	/* Get number of objects.  */
	num_interfaces = read_unsigned_le_short(buffer, &position);
	printf("Number of interfaces: %d\n", num_interfaces);

	INTERFACES = calloc(num_interfaces, sizeof(struct interface));
	
	/* Read data.  */
	read_interfaces(buffer, &position, buffer_size);

	/* Output interface data to file.  */
	for (x = 0; x < num_interfaces; x++)
	{
		fprint_intr_struct(&INTERFACES[x], fp_obj);
	}

	fclose(fp_obj);
	
	printf("Finished reading interface data, cleaning up.\n");

	for (x = 0; x < num_interfaces; x++)
	{
		if (INTERFACES[x].selected_action_name != NULL)
		{
			free(INTERFACES[x].selected_action_name);
		}
		if (INTERFACES[x].spell_name != NULL)
		{
			free(INTERFACES[x].spell_name);
		}
		if (INTERFACES[x].condition_type != NULL)
		{
			free(INTERFACES[x].condition_type);
		}
		if (INTERFACES[x].condition_value != NULL)
		{
			free(INTERFACES[x].condition_value);
		}
		if (INTERFACES[x].inventory_item_id != NULL)
		{
			free(INTERFACES[x].inventory_item_id);
		}
		if (INTERFACES[x].inventory_stack_size != NULL)
		{
			free(INTERFACES[x].inventory_stack_size);
		}
		if (INTERFACES[x].sprites != NULL)
		{
			free(INTERFACES[x].sprites);
			free(INTERFACES[x].sprites_x);
			free(INTERFACES[x].sprites_y);
		}
		if (INTERFACES[x].actions != NULL)
		{
			for (y = 0; y < INTERFACES[x].num_actions; y++)
			{
				free(INTERFACES[x].actions[y]);
			}
			free(INTERFACES[x].actions);
		}
		if (INTERFACES[x].children != NULL)
		{
			free(INTERFACES[x].children);
			free(INTERFACES[x].child_x);
			free(INTERFACES[x].child_y);
		}
		if (INTERFACES[x].fonts != NULL)
		{
			free(INTERFACES[x].fonts);
		}
		if (INTERFACES[x].tooltip != NULL)
		{
			free(INTERFACES[x].tooltip);
		}
		if (INTERFACES[x].text_default != NULL)
		{
			free(INTERFACES[x].text_default);
		}
		if (INTERFACES[x].text_active != NULL)
		{
			free(INTERFACES[x].text_active);
		}
		if (INTERFACES[x].opcodes != NULL)
		{
			for (y = 0; y < INTERFACES[x].opcode_count; y++)
			{
				free(INTERFACES[x].opcodes[y]);
			}
			free(INTERFACES[x].opcodes);
		}
	}

	free(buffer);
	free(INTERFACES);
	printf("Done.\n");
	return 0;
}
