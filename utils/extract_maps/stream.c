
int get_unsigned_le_short(char buffer[], int *position) 
{
	*position += 2;
	return ((buffer[(*position) - 2] & 0xff) << 8)
			+ (buffer[(*position) - 1] & 0xff);
}

int read_unsigned_byte(char buffer[], int *position) 
{
	return buffer[(*position)++] & 0xff;
}
