#ifndef _STREAM_H_
#define _STREAM_H_

int get_unsigned_le_short(char *buffer, int *position);
int read_unsigned_byte(char buffer[], int *position);

#endif
