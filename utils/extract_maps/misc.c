#include <stdio.h>
#include <stdlib.h>

#include <bzlib.h>

#include "config.h"

int file_len(FILE *fp)
{
	int len;

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);

	return len;
}

void bzip2_error(int error_code)
{
	switch (error_code)
	{
		case BZ_PARAM_ERROR:
			printf("Error: param error\n");
			break;
		case BZ_MEM_ERROR:
			printf("Error: insufficient memory error\n");
			break;
		case BZ_OUTBUFF_FULL:
			printf("Error: compressed data size exceeds dest_len.\n");
			break;
		case BZ_DATA_ERROR:
			printf("Error: data integrity error found in compressed data\n");
			break;
		case BZ_DATA_ERROR_MAGIC:
			printf("Error: compressed data doesn't have magic bytes.\n");
			break;
		case BZ_UNEXPECTED_EOF:
			printf("Error: compressed data ends unexpectedly.\n");
			break;
		case BZ_OK:
			break;
	}
}

/* Constructs a file_cache struct with all the needed data and returns the 
   pointer to it.  */
struct file_cache *return_file_cache(FILE *main_cache, FILE *index_file, int id)
{
	struct file_cache *fc = malloc(sizeof(struct file_cache));
	
	fc->data_file = main_cache;
	fc->index_file = index_file;
	fc->store_id = id;

	return fc;
}

/* Decodes an archive from the cache.  */
int decode_archive(struct file_cache *fc, int index, char *decoded_data)
{
	int in = 0, r = 0, part = 0, read = 0, unread = 0, in2 = 0;
	int size = 0, sector = 0;
	int i = 0;
	
	fseek(fc->index_file, index * 6, SEEK_SET);

	for (r = 0; r < 6; r += in)
	{
		in = fread(fc->buffer+r, 1, 6 - r, 
				   fc->index_file);
		if (in == 0)
		{
			return -1;
		}
	}

	size = ((fc->buffer[0] & 0xff) << 16) 
			+ ((fc->buffer[1] & 0xff) << 8)
			+ (fc->buffer[2] & 0xff);
	sector = ((fc->buffer[3] & 0xff) << 16) 
			 + ((fc->buffer[4] & 0xff) << 8)
			 + (fc->buffer[5] & 0xff);
	
	if (size < 0 || size > 0x7a120)
		return -1;
	
	/* Matches.  */
	if (sector <= 0 || sector > file_len(fc->data_file) / 520L)
		return -1;

	read = 0;
	for (part = 0; read < size; part++)
	{
		if (sector == 0)
		{
			return -1;
		}
		
		fseek(fc->data_file, sector * 520, SEEK_SET);
		unread = (size - read);

		if (unread > 512)
		{
			unread = 512;
		}
	
		for (r = 0; r < (unread + 8); r += in2)
		{
			in2 = fread((fc->buffer)+r, 1, (unread + 8) - r, 
					   fc->data_file);
			if (in2 == -1)
			{
				return -1;
			}
		}
	
		int decomp_index = ((fc->buffer[0] & 0xff) << 8) 
						   + (fc->buffer[1] & 0xff);
		int decomp_part = ((fc->buffer[2] & 0xff) << 8) 
						  + (fc->buffer[3] & 0xff);
		int decomp_sector = ((fc->buffer[4] & 0xff) << 16) 
							+ ((fc->buffer[5] & 0xff) << 8)
							+ (fc->buffer[6] & 0xff);
		int decomp_store_id = fc->buffer[7] & 0xff;
		
		if (decomp_index != index 
			|| decomp_part != part 
			|| decomp_store_id != fc->store_id)
		{
			printf("Decomp mismatch!\n");
			return -1;
		}

		if (decomp_sector < 0 
			|| decomp_sector > file_len(fc->data_file) / 520L)
		{
			printf("Decomp sector mismatch!\n");
			return -1;
		}
		
		for (i = 0; i < unread; i++)
		{
			decoded_data[read++] = fc->buffer[i + 8];
		}

		sector = decomp_sector;
	}
	/* Return number of characters read into the decoded_data buffer.  */
	return read;
}
