/* Extract map data from the client files.  */
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

FILE *main_cache;
FILE *cache_idx[5];

struct file_cache *caches[5];

int *map_index_1;
int *map_index_2;
int *map_index_3;
int *map_index_4;

char map_folder[50] = {"map_data"};
char obj_folder[50] = {"obj_data"};

int main(void)
{
	FILE *map_index;
	char *map_index_stream;
	int map_index_pos = 0;
	int map_index_len = 0;
	int x, z;

	/* Open main cache.  */
	main_cache = fopen("main_file_cache.dat", "r+");

	/* Open secondary caches.  */
	cache_idx[0] = fopen("main_file_cache.idx0", "r+");
	cache_idx[1] = fopen("main_file_cache.idx1", "r+");
	cache_idx[2] = fopen("main_file_cache.idx2", "r+");
	cache_idx[3] = fopen("main_file_cache.idx3", "r+");
	cache_idx[4] = fopen("main_file_cache.idx4", "r+");
	
	if (main_cache == NULL)
	{
		printf("ERROR! MAIN CHACHE ERROR!\n");
		printf("Trying to open the main cache file returned a NULL.\n");
		printf("Make sure the cache files are in the same directory as the ");
		printf("program.\n");
		exit(1);
	}

	for (x = 0; x < 5; x++)
	{
		if (cache_idx[x] == NULL)
		{
			printf("ERROR! IDX CACHE %d ERROR!\n", x);
			printf("Trying to open the index cache file number %d "
				   "returned a NULL.\n", x);
			printf("Make sure the cache files are in the same directory"
				   " as the ");
			printf("program.\n");
			exit(1);
		}
	}

	/* Setup the file cache structs.  */
	for (x = 0; x < 5; x++)
	{
		caches[x] = return_file_cache(main_cache, cache_idx[x], (x + 1));
	}

	/* Open 333 Map index file.  */
	map_index = fopen("4/1987120305.dat", "r");

	if (map_index == NULL)
	{
		printf("Map index file not found. Make sure you decode all client\n"
			   "files with decode first.\n");
		return 1;
	}
	printf("Map index file opened, reading data.\n");
	
	map_index_len = file_len(map_index);
	map_index_stream = malloc(map_index_len * sizeof(char));
	fread(map_index_stream, 1, map_index_len, map_index);

	map_index_1 = malloc((map_index_len / 7) * sizeof(int));
	map_index_2 = malloc((map_index_len / 7) * sizeof(int));
	map_index_3 = malloc((map_index_len / 7) * sizeof(int));
	map_index_4 = malloc((map_index_len / 7) * sizeof(int));
	map_index_pos = 0;

	mkdir(map_folder, 0755);

	for (x = 0; x < (map_index_len / 7); x++)
	{
		map_index_1[x] = get_unsigned_le_short(map_index_stream, 
											   &map_index_pos);
		map_index_2[x] = get_unsigned_le_short(map_index_stream, 
											   &map_index_pos);
		map_index_3[x] = get_unsigned_le_short(map_index_stream, 
											   &map_index_pos);
		map_index_4[x] = read_unsigned_byte(map_index_stream, &map_index_pos);
	}
	free(map_index_stream);

	FILE *region_list = fopen("region_list.tsv", "w+");

	if (region_list == NULL)
	{
		printf("Unable to open region_list.tsv for creation!\n");
	}
	else
	{
		for (x = 0; x < (map_index_len / 7 ); x++)
		{
			/* Writes the region id, along with its absolute x and 
			   y coords.  */
			fprintf(region_list, "%d\t%d\t%d\n", map_index_1[x],
					((map_index_1[x] >> 8) * 64),		
					((map_index_1[x] & 0xff) * 64));		
		}
	}

	printf("Created region_list.tsv file.\n");
	printf("Number of value sets read from map_index: %d\n", x);
	printf("Extracting map data to files in the map_folder...\n");

	/* Extract all the map data files.  */
	for (z = 0; z < (map_index_len / 7); z++)
	{
		char *map_data = malloc(DECOMP_SIZE * sizeof(char));
		char *obj_data = malloc(DECOMP_SIZE * sizeof(char));
		int map_data_len = 0;
		int obj_data_len = 0;
		char output_name[100] = {0};
		char output_name2[100] = {0};
		FILE *map_file;
		FILE *obj_file;

		snprintf(output_name, 100, "%s/%d_map.gz", map_folder, map_index_1[z]);
		snprintf(output_name2, 100, "%s/%d_obj.gz", map_folder, 
				 map_index_1[z]);

		map_file = fopen(output_name, "w+");
		obj_file = fopen(output_name2, "w+");

		map_data_len = decode_archive(caches[4], map_index_2[z], map_data);
		obj_data_len = decode_archive(caches[4], map_index_3[z], obj_data);

		if (map_file == NULL)
		{
			printf("Map file %s failed to be created!\n", output_name);
		}

		if (obj_file == NULL)
		{
			printf("Object file %s failed to be created!\n", output_name2);
		}

		for (x = 0; x < map_data_len; x++)
		{
			fputc(map_data[x], map_file);
		}

		for (x = 0; x < obj_data_len; x++)
		{
			fputc(obj_data[x], obj_file);
		}

		free(map_data);
		free(obj_data);
		fclose(map_file);
		fclose(obj_file);
	}
	printf("Done!\n");
	printf("Cleaning up...\n");

	/* Free all map index arrays.  */
	free(map_index_1);
	free(map_index_2);
	free(map_index_3);
	free(map_index_4);

	/* Close files.  */
	fclose(main_cache);
	fclose(map_index);
	fclose(region_list);

	/* Close all cache files, free malloc'd structs relating to them.  */
	for (x = 0; x < 5; x++)
	{
		fclose(cache_idx[x]);
		free(caches[x]);
	}

	printf("All finished, closing.\n");
	return 0;
}
