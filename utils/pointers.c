#include <stdio.h>

int main(void)
{
	/* First we initialize the variables we are going to use. Note that int p
	   has an asterisk when being defined. This shows that it is a pointer,
	   not just a regular variable.  */
	int x = 5;
	int *p;

	/* Here we assign the pointer to the memory address of int x (note the
	   ampersand). The pointer, instead of holding a number like x does, it
	   holds a memory address. We do not use the asterisk here because the
	   asterisk gets the value at the pointer's memory address. Here we
	   just assign the memory address p will use, and later get the value at
	   it by adding the asterisk.  */
	p = &x;

	/* We print out the values of both variables here. Note that both are the
	   same. Because a pointer holds a memory address instead of a normal
	   value, we place an asterisk right before it to tell the computer to get
	   the data stored at the address, instead of just printing the memory
	   address itself. The asterisk is called a "dereferencing operator" 
	   because it gets data based on the "reference" that is its address in
	   memory.  */
	printf("X: %d P: %d\n", x, *p);

	/* Note the parenthesis around the variable. For doing simple increments
	   or decrements with the ++/-- operators, these are needed because of the
	   fact that the * and ++/-- have equal precedence in C's order of 
	   operations. Leaving out the parenthesis at this point will increment
	   the memory address instead of the value. Due to C reading from right
	   to left for its order of operations, you could alternatively use:
	   ++*p;
	   instead, or just
	   *p += 1;  */
	(*p)++;

	/* ALSO, if you need to increment a pointer's memory address, it will
	   increment it by how many bytes its type takes in memory. For ints,
	   it is usually 4. This is how incrementing arrays to get the next
	   value in it works.  
	   
	   Example:
	   int *test points at memory address 4
	   *p++;
	   now test points at memory address 8
	   */

	/* Now that we incremented the value pointed at by *p, note the output
	   by print. This is because p points at x's memory address. When p is
	   incremented, x gets changed as well.  */
	printf("X: %d P: %d\n", x, *p);

	/* Nothing left to do, close program and return all good.  */
	return 0;
}
