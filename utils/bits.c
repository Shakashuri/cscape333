/* file.c - Prints out the binary for an int value, MSB first.  */
#include <stdio.h>
#include <math.h>

void int_bits(int test)
{
	int size_test = (int) sizeof(test);
	int bits = (sizeof(test) * 8);
	unsigned int start = pow(2.0, (double) (bits - 1));
	int x, z;

	printf("Most significant bit first, newlines seperate bytes.\n");
	z = bits;

	for (; z > 0; start >>= 1)
	{
		printf("%d ", (test & start)?1:0);
		z--;

		/* If we have printed out a whole byte, send a newline.  */
		if ((z % 8) == 0)
		{
			putc('\n', stdout);
		}
	}
	printf("\n");
}

int main(void)
{
	char buffer[100];
	int value = -1;
	int rv = 0;

	while (1)
	{
		fgets(buffer, 100, stdin);
		sscanf(buffer, "%d", &value);
		printf("Entry: %d 0x%x\n", value, value);
		int_bits(value);
	}
}
