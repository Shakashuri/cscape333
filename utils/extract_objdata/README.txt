This program extracts all needed info from the cache data about objects in the
game, and outputs it to the file "obj_data". The obj_info.tsv file contains
information that is loaded into the program about special properties of
objects that are not located in the client data, but must be known about
anyways.
