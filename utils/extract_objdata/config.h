#ifndef _CONFIG_H_
#define _CONFIG_H_

struct obj
{
	int id;
	char name[300];
	char description[300];

	int size_x;
	int size_y;
	int face;
	int scale_x;
	int scale_y;
	int scale_z;
	int translate_x;
	int translate_y;
	int translate_z;
	int solid;
	int wall;
	int walkable;
	int unwalkable_solid;
	int adjust_to_terrain;
	int rotated;
	int casts_shadow;
	int map_scene;
	int ambient;
	int has_actions;
	int action_loc;
	char actions[10][300];

	int model_count;
	int *model_types;
	int *model_ids;
	int color_count;
	int *modified_model_colors;
	int *original_model_colors;
	int icon;
	int animation_id;
	
	int delay_shading;
	int diffuse;

	int offset_amplifier;

	int unknown_attribute;
	int var_bit_id;
	int config_ids;
	int child_count;
	int *child_ids;

/* Below is special object data that needs to be read from obj_info.  */

	/* Show if extra info was loaded from the obj_info file.  */ 
	int has_extra_info;
	/* 0 - normal door, 1 - fence gate.  */
	int door_type;
	/* Is it most likely part of a double door setup?  */
	int is_probably_ddoor;
	/* Extra data that is read from the file obj_info.  */
	int is_left_door;
	/* 0 - closed, 1 - open.  */
	int is_open;
	/* How much the id changes to get to its next version. Mostly used for
	   swapping between the open and closed versions.  */
	int variant_id_diff;
	int actionable;
};

struct obj_info
{
	int id;
	/* 0 - normal door, 1 - fence gate.  */
	int door_type;
	/* Is it most likely part of a double door setup?  */
	int is_probably_ddoor;
	/* Extra data that is read from the file obj_info.  */
	int is_left_door;
	/* 0 - closed, 1 - open.  */
	int is_open;
	/* How much the id changes to get to its next version. Mostly used for
	   swapping between the open and closed versions.  */
	int variant_id_diff;
	/* Used for special objects that are used for actions, but do not have
	   any build it. For example, using buckets on wells to get water.  */
	int actionable;
};

#endif
