#include <stdio.h>


int file_len(FILE *fp)
{
	int len;

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);

	return len;
}

int file_lines(FILE *fp)
{
	int num_newlines = 0;
	int read_ch;

	while ((read_ch = fgetc(fp)) != EOF)
	{
		if (read_ch == '\n')
		{
			num_newlines++;
		}
	}
	rewind(fp);

	return num_newlines;
}

/* Returns 1 if a char is an alphanumeric character.  */
int is_alpha(char letter)
{
	if ((letter < 123 && letter > 96) || 
	    (letter < 91 && letter > 64))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/** 
 * Converts a char to its lowercase version.
 * @param letter Char to change case of.
 * @return Returns the lowercase version of a letter.
 */
char to_lower(char letter)
{
	if (letter < 91 && letter > 64)
	{
		letter += 32;
	}
	return letter;
}

/**
 * Takes a string and converts all characters in it to their lowercase version.
 * @param *string Pointer to the string.
 * @return Returns the string pointer.
 */
char *string_to_lower(char *string)
{
	int x;

	for (x = 0; string[x] != '\0'; x++)
	{
		string[x] = to_lower(string[x]);
	}
	return string;
}

