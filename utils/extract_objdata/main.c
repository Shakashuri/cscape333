#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

/* Filenames need to be changed for each version.  */
/* 333 version filenames.  */
char file_name[100] = {"1/682978269.dat"};
char index_file_name[100] = {"1/682997061.dat"};

/* Will hold created arrays for each object read.  */
struct obj *OBJECTS;

/* Holds data read from the obj_info.tsv file.  */
struct obj_info *OBJECT_INFO;
int obj_info_size;

/* Holds all the offsets in the regular file, read from the index file.  */
int *obj_offsets;

/* Searches through the OBJECT_INFO array to find a match.  */
int get_obj_info_index(int id)
{
	int x = 0;

	for (x = 0; x < obj_info_size; x++)
	{
		if (OBJECT_INFO[x].id == id)
		{
			return x;
		}
	}
	return -1;
}

/* Need to make sure some defaults are set before we read in data.  */
void set_defaults(struct obj *o)
{
	strncpy(o->name, "null", 300);
	strncpy(o->description, "null", 300);
	o->color_count = -1;
	o->model_ids = NULL;
	o->model_types = NULL;
	o->modified_model_colors = NULL;
	o->original_model_colors = NULL;
	o->child_ids = NULL;
	/* Clear action arrays.  */
	strncpy(o->actions[0], "null", 300);
	strncpy(o->actions[1], "null", 300);
	strncpy(o->actions[2], "null", 300);
	strncpy(o->actions[3], "null", 300);
	strncpy(o->actions[4], "null", 300);
	strncpy(o->actions[5], "null", 300);
	strncpy(o->actions[6], "null", 300);
	strncpy(o->actions[7], "null", 300);
	strncpy(o->actions[8], "null", 300);
	strncpy(o->actions[9], "null", 300);
	/* Index of the action array, last action.  */
	o->action_loc = -1;
	o->size_x = 1;
	o->size_y = 1;
	o->solid = 1;
	o->walkable = 1;
	o->animation_id = -1;
	o->offset_amplifier = 16;
	o->icon = -1;
	o->map_scene = -1;
	o->casts_shadow = 1;
	o->scale_x = o->scale_y = o->scale_z = 128;
	o->var_bit_id = -1;
	o->config_ids = -1;

	/* Data that is read from the obj_info file goes here, if applicable.  */
	o->has_extra_info = 0;
	o->is_left_door = -1;
	o->is_probably_ddoor = -1;
	o->door_type = -1;
	o->is_open = -1;
	o->variant_id_diff = 0;
	o->actionable = 0;
}

struct obj *read_obj(char buffer[], int *position, int objects_offset)
{
	int x;
	int stored_solid = -1;
	int stored_actions = -1;
	char temp_action[300];

	struct obj *obj = &OBJECTS[objects_offset];

	set_defaults(&OBJECTS[objects_offset]);

start:

	do
	{
		int att;
		do 
		{
			att = read_unsigned_byte(buffer, position);	

			if (att == 0)
			{
				goto end;
			}
			else if (att == 1)
			{
				obj->model_count = read_unsigned_byte(buffer, position);	
				if (obj->model_count > 0)
				{
					if (obj->model_ids == NULL)
					{
						obj->model_types = malloc(obj->model_count * sizeof(int));
						obj->model_ids = malloc(obj->model_count * sizeof(int));
						
						for (x = 0; x < obj->model_count; x++)
						{
							obj->model_ids[x] 
							= read_unsigned_le_short(buffer, position);
							obj->model_types[x] 
								= read_unsigned_byte(buffer, position);
						}
					}
					else
					{
						(*position) += (obj->model_count * 3);
					}
				}
			}
			else if (att == 2)
			{
				read_string(buffer, position, obj->name, 300);
			}
			else if (att == 3)
			{
				read_string(buffer, position, obj->description, 300);
			}
			else if (att == 5)
			{
				obj->model_count = read_unsigned_byte(buffer, position);	
				if (obj->model_count > 0)
				{
					if (obj->model_ids == NULL)
					{
						obj->model_ids = malloc(obj->model_count * sizeof(int));
						
						for (x = 0; x < obj->model_count; x++)
						{
							obj->model_ids[x] 
							= read_unsigned_le_short(buffer, position);
						}
					}
					else
					{
						(*position) += (obj->model_count * 2);
					}
				}
			}
			else if (att == 14)
			{
				obj->size_x = read_unsigned_byte(buffer, position);
			}
			else if (att == 15)
			{
				obj->size_y = read_unsigned_byte(buffer, position);
			}
			else if (att == 17)
			{
				obj->solid = 0;
			}
			else if (att == 18)
			{
				obj->walkable = 0;
			}
			else if (att == 19)
			{
				stored_actions = read_unsigned_byte(buffer, position);
				if (stored_actions == 1)
				{
					obj->has_actions = 1;
				}
			}
			else if (att == 21)
			{
				obj->adjust_to_terrain = 1;
			}
			else if (att == 22)
			{
				obj->delay_shading = 1;
			}
			else if (att == 23)
			{
				obj->wall = 1;
			}
			else if (att == 24)
			{
				obj->animation_id = read_unsigned_le_short(buffer, position);
				if (obj->animation_id == 65535)
				{
					obj->animation_id = -1;
				}
			}
			else if (att == 28)
			{
				obj->offset_amplifier = read_value(buffer, position);
			}
			else if (att == 29)
			{
				obj->ambient = read_value(buffer, position);
			}
			else if (att == 39)
			{
				obj->diffuse = read_value(buffer, position);
			}
			else if (att >= 30 && att < 39)
			{
				int diff = (att - 30);

				temp_action[0] = '\0';
				read_string(buffer, position, temp_action, 300); 

				/* Make it all lowercase for parsing.  */
				strncpy(obj->actions[diff], string_to_lower(temp_action),
					300);

				/* Ignore the action if they are "hidden", or it is gibberish.  */
				if (strcmp(obj->actions[diff], "hidden") == 0)
				{
					strncpy(obj->actions[diff], "null", 300);
				}
				else
				{
					/* Replace all spaces with hypens, will make it easier to
					 * parse later.  */
					for (int z = 0; obj->actions[diff][z] != '\0' || z < 300; z++)
					{
						if (obj->actions[diff][z] == ' ')
						{
							obj->actions[diff][z] = '-';
						}
					}
					obj->has_actions = 1;
				}
			}
			else if (att == 40)
			{
				/* Check if color_count was already set, if so, skip.  */
				if (obj->color_count == -1)
				{
					obj->color_count = read_unsigned_byte(buffer, position);
					obj->modified_model_colors 
						= calloc(obj->color_count, sizeof(int));

					obj->original_model_colors 
						= calloc(obj->color_count, sizeof(int));
					
					for (x = 0; x < obj->color_count; x++)
					{
						obj->modified_model_colors[x] 
							= read_unsigned_le_short(buffer, position);
						obj->original_model_colors[x]
							= read_unsigned_le_short(buffer, position);
					}
				}
				else
				{
					int count = read_unsigned_byte(buffer, position);

					for (x = 0; x < count; x++)
					{
						read_unsigned_le_short(buffer, position);
						read_unsigned_le_short(buffer, position);
					}
				}
			}
			else if (att == 60)
			{
				obj->icon = read_unsigned_le_short(buffer, position);
			}
			else if (att == 62)
			{
				obj->rotated = 1;
			}
			else if (att == 64)
			{
				obj->casts_shadow = 0;
			}
			else if (att == 65)
			{
				obj->scale_x = read_unsigned_le_short(buffer, position);
			}
			else if (att == 66)
			{
				obj->scale_y = read_unsigned_le_short(buffer, position);
			}
			else if (att == 67)
			{
				obj->scale_z = read_unsigned_le_short(buffer, position);
			}
			else if (att == 68)
			{
				obj->map_scene = read_unsigned_le_short(buffer, position);
			}
			else if (att == 69)
			{
				obj->face = read_unsigned_byte(buffer, position);
			}
			else if (att == 70)
			{
				obj->translate_x = read_short(buffer, position);
			}
			else if (att == 71)
			{
				obj->translate_y = read_short(buffer, position);
			}
			else if (att == 72)
			{
				obj->translate_z = read_short(buffer, position);
			}
			else if (att == 73)
			{
				obj->unknown_attribute = 1;
			}
			else if (att == 74)
			{
				obj->unwalkable_solid = 1;
			}
			else 
			{
				if (att != 75)
				{
					continue;
				}
				stored_solid = read_unsigned_byte(buffer, position);
			}
			goto start;
		} while (att != 77);

		obj->var_bit_id = read_unsigned_le_short(buffer, position);
		if (obj->var_bit_id == 65535)
		{
			obj->var_bit_id = -1;
		}

		obj->config_ids = read_unsigned_le_short(buffer, position);
		if (obj->config_ids == 65535)
		{
			obj->config_ids = -1;
		}
		
		obj->child_count = read_unsigned_byte(buffer, position);
		obj->child_ids = malloc((obj->child_count + 1) * sizeof(int));
		for (x = 0; x <= obj->child_count; x++)
		{
			obj->child_ids[x] = read_unsigned_le_short(buffer, position);
			if (obj->child_ids[x] == 65535)
			{
				obj->child_ids[x] = -1;
			}
		}
	} while (1);

	end:

	for (x = 0; x < 10; x++)
	{
		//if (obj->actions[x][0], != '\0'
		if (strcmp(obj->actions[x], "null") != 0)
		{
			obj->action_loc = x;
			break;
		}
	}

	if (obj->unwalkable_solid == 1)
	{
		obj->solid = 0;
		obj->walkable = 0;
	}

	if (stored_solid == -1)
	{
		stored_solid = obj->solid ? 1 : 0;
	}

	return obj;
}

void fprint_obj_struct(struct obj *o, FILE *fp)
{
	fprintf(fp, "%d\t", o->id);

	if (o->name[0] == '\0')
	{
		fprintf(fp, "Null\t");
	}
	else
	{
		fprintf(fp, "%s\t", o->name);
	}

	if (o->description[0] == '\0')
	{
		fprintf(fp, "Null\t");
	}
	else
	{
		fprintf(fp, "%s\t", o->description);
	}

	fprintf(fp, "%d\t%d\t", o->size_x, o->size_y);
	/* Face is not used as the default position for the object spawn. Can
	   be ignored.  */
	/*
	printf("%d\t", o->face % 4);
	*/

	fprintf(fp, "%d\t%d\t", o->solid, o->walkable);

	/* Print out all of the extra info that the object has.  */
	//fprintf(fp, "%d\t", o->actionable);
	//fprintf(fp, "%d\t", o->has_extra_info);
	fprintf(fp, "%d\t", o->door_type);
	fprintf(fp, "%d\t", o->is_probably_ddoor);
	//fprintf(fp, "%d\t", o->is_left_door);
	fprintf(fp, "%d\t", o->is_open);
	fprintf(fp, "%d\t", o->variant_id_diff);


	fprintf(fp, "%d\t", o->has_actions);
	//fprintf(fp, "%d\t", o->action_loc + 1);

	for (int x = 0; x < 5; x++)
	{
		fprintf(fp, "%s\t", o->actions[x]);
	}

	fprintf(fp, "\n");
}

void read_obj_info(FILE *fp)
{
	int x = 0;
	char buffer[200] = {0};

	/* Remove the comment line.  */
	fgets(buffer, sizeof(buffer), fp);
	
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct obj_info *obj = &OBJECT_INFO[x];

		if (sscanf(buffer, "%d\t%d\t%d\t%d\t%d\t%d\t%d", &obj->id, &obj->door_type, 
				   &obj->is_probably_ddoor, &obj->is_left_door, &obj->is_open, 
				   &obj->variant_id_diff, &obj->actionable) != EOF)
		{
			x++;
			if (x >= obj_info_size)
			{
				printf("HIT MAX!\n");
				return;
			}
		}
		else
		{
			printf("ERROR: Reading obj_info on line: %d!\n", x);
		}
	}
	printf("Loaded %d obj_info definitions.\n", (x + 1));
}

int main(void)
{
	FILE *fp = fopen(file_name, "r");
	FILE *fp_index = fopen(index_file_name, "r");
	FILE *fp_objinfo = fopen("obj_info.tsv", "r");
	FILE *fp_obj = fopen("obj_data.tsv", "w");

	int num_objects = 0;
	int buffer_size;
	int index_buffer_size;
	int index_position = 0;
	int obj_info_index;
	int offset;
	int x; 

	char *buffer;
	char *index_buffer;
	
	if (fp == NULL)
	{
		printf("Object data file %s failed to open.\n", file_name);
		return 1;
	}

	if (fp_index == NULL)
	{
		printf("Object index file %s failed to open.\n", index_file_name);
		return 1;
	}

	if (fp_obj == NULL)
	{
		printf("Could not create object file: obj_data.\n");
		return 1;
	}

	if (fp_objinfo == NULL)
	{
		printf("Failed to open obj_info.tsv. Ensure you copied it over to\n");
		printf("the cache directory from the extract_objdata folder.\n");
		return 1;
	}

	/* Create the buffers, read data from files, then close them.  */
	buffer_size = file_len(fp);
	index_buffer_size = file_len(fp_index);
	/* Minus one to account for the comment line at the top for spreadsheet
	   software.  */
	obj_info_size = file_lines(fp_objinfo);
	printf("obj_info size: %d\n", obj_info_size);
	
	printf("Buffer sizes: %d %d\n", buffer_size, index_buffer_size);
	
	buffer = malloc(buffer_size * sizeof(char));
	index_buffer = malloc(index_buffer_size * sizeof(char));
	OBJECT_INFO = calloc(obj_info_size, sizeof(struct obj_info));

	for (x = 0; x < buffer_size; x++)
	{
		buffer[x] = fgetc(fp);
	}

	for (x = 0; x < index_buffer_size; x++)
	{
		index_buffer[x] = fgetc(fp_index);
	}
	printf("Data read into buffer.\n");

	fclose(fp);
	fclose(fp_index);

	read_obj_info(fp_objinfo);
	fclose(fp_objinfo);
	
	/* Get number of objects from the index buffer, and create the OBJECTS 
	   array from that data. Then read the offsets for each object.  */
	num_objects = read_unsigned_le_short(index_buffer, &index_position);
	printf("Number of objects: %d\n", num_objects);

	OBJECTS = calloc(num_objects, sizeof(struct obj));

	/* Setup reading the offsets where each object data block starts.  */
	obj_offsets = malloc(num_objects * sizeof(int));
	
	printf("Reading offsets...\n");

	offset = 2;
	for (x = 0; x < num_objects; x++)
	{
		obj_offsets[x] = offset;
		offset += read_unsigned_le_short(index_buffer, &index_position);
	}

	printf("Offsets read.\n");

	/* Read data from the buffer we setup earlier and place it into the OBJECTS
	   array. After that, output all needed data from each object to a file
	   defined at the start of main.  */
	printf("Reading object data...\n");
	for (x = 0; x < num_objects; x++)
	{
		read_obj(buffer, &obj_offsets[x], x);
		OBJECTS[x].id = x;

		obj_info_index = get_obj_info_index(OBJECTS[x].id);

		/* This sets any extra values needed for the object.  */
		if (obj_info_index != -1)
		{
			printf("Found a match! Setting values...\n");
			printf("INDEX: %d ID: %d\n", obj_info_index, OBJECTS[x].id);

			/*
			if (OBJECT_INFO[obj_info_index].actionable == 1
				&& OBJECTS[x].actions[OBJECTS[x].action_loc][0] != '\0')
				*/
			if (OBJECT_INFO[obj_info_index].actionable == 1
				&& OBJECTS[x].has_actions == 1)
			{
				printf("\tOBJECT ALREADY HAS AN ACTION, REDUNDENT ENTRY!\n");
			}

			OBJECTS[x].has_extra_info = 1;
			OBJECTS[x].door_type = OBJECT_INFO[obj_info_index].door_type;
			OBJECTS[x].is_probably_ddoor 
				= OBJECT_INFO[obj_info_index].is_probably_ddoor;
			OBJECTS[x].is_left_door = OBJECT_INFO[obj_info_index].is_left_door;
			OBJECTS[x].is_open = OBJECT_INFO[obj_info_index].is_open;
			OBJECTS[x].variant_id_diff 
				= OBJECT_INFO[obj_info_index].variant_id_diff;
			OBJECTS[x].actionable 
				= OBJECT_INFO[obj_info_index].actionable;
		}

		fprint_obj_struct(&OBJECTS[x], fp_obj);
	}
	fclose(fp_obj);
	
	printf("Finished reading object data, cleaning up.\n");

	for (x = 0; x < num_objects; x++)
	{
		/* Check if anything was malloc'ed to the struct. If so, free it.  */
		if (OBJECTS[x].model_ids != NULL)
		{
			free(OBJECTS[x].model_ids);
		}
		if (OBJECTS[x].model_types != NULL)
		{
			free(OBJECTS[x].model_types);
		}
		if (OBJECTS[x].original_model_colors != NULL)
		{
			free(OBJECTS[x].original_model_colors);
		}
		if (OBJECTS[x].modified_model_colors != NULL)
		{
			free(OBJECTS[x].modified_model_colors);
		}
		if (OBJECTS[x].child_ids != NULL)
		{
			free(OBJECTS[x].child_ids);
		}
	}

	free(buffer);
	free(index_buffer);
	free(obj_offsets);
	free(OBJECTS);
	free(OBJECT_INFO);
	printf("Done!\n");
	return 0;
}
