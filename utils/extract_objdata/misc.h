#ifndef _MISC_H_
#define _MISC_H_

int file_len(FILE *fp);
int file_lines(FILE *fp);
int is_alpha(char letter);
char to_lower(char letter);
char *string_to_lower(char *string);

#endif
