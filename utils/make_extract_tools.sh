echo "Removing old binaries..."
rm -r bin
echo "Creating bin folder..."
mkdir bin
echo "Building decoding tool..."
cd decode
make debug
cp decode ../bin/
cd ..
echo "Building extract_collision..."
cd extract_collision
make debug
cp extcol ../bin/
cd ..
echo "Building extract_interfaces..."
cd extract_interfaces
make debug
cp extintr ../bin/
cd ..
echo "Building extract_maps..."
cd extract_maps
make debug
cp extmaps ../bin/
cd ..
echo "Building extract_midis..."
cd extract_midis
make debug
cp extmidis ../bin/
cd ..
echo "Building extract_objdata..."
cd extract_objdata
make debug
cp extobjdata ../bin/
cp obj_info.tsv ../bin/
cd ..
echo "Copying run script..."
cp run_extract_tools.sh bin/
echo "Extraction tools build and placed in ./bin."
