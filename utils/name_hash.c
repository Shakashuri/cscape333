#include <stdio.h>
#include <string.h>

char to_upper(char letter)
{
	if (letter < 123 && letter > 96)
	{
		letter -= 32;
	}
	return letter;
}

int get_string_hash(char string[], int len)
{
	int hash = 0;
	int x;

	for (x = 0; x < len; x++)
	{
		hash = (hash * 61 + to_upper(string[x])) - 32;	
	}
	return hash;
}

int main(void)
{	
	int hash = 0;
	char buffer[100];
	char string[100];

	printf("Type in a file name to get its hashed equivalent.\n");
	while (1)
	{
		fgets(buffer, 100, stdin);
		sscanf(buffer, "%s", string); 
		hash = get_string_hash(string, strlen(string));
		printf("Hash: %d String: %s\n", hash, string);
	}
}
