#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

int ****MAP_DATA;
int min_region_id = 0;
int max_region_id = 0;
int max_map_size = 0;
int max_obj_size = 0;
int num_objects_loaded = 0;
char *MAP_DIR = {"map_data"};


/* There are more columns in the file than read here, but that data can be
   ignored right now.  */
int read_obj_data(struct obj obj_data[], int size, FILE *fp)
{
	char buffer[1000] = {0};
	int x;
	int door_type = 0, is_ddoor = 0, is_open = 0, var_id = 0;
	char actions[5][200];

	for (x = 0; x < size; x++)
	{
		/* Clear the comment at the top.  */
		fgets(buffer, sizeof(buffer), fp);
		
		if (sscanf(buffer, "%d\t"
						   "%[^\t]\t%[^\t]\t"
						   "%d\t%d\t"
						   "%d\t%d\t"
						   "%d\t%d\t%d\t%d\t"
						   "%d\t"
						   "%[^\t]\t"
						   "%[^\t]\t"
						   "%[^\t]\t"
						   "%[^\t]\t"
						   "%[^\n]\n",
				   &obj_data[x].id,
				   obj_data[x].name, obj_data[x].desc,
				   &obj_data[x].size_x, &obj_data[x].size_y,
				   &obj_data[x].solid, &obj_data[x].walkable,
				   &door_type, &is_ddoor, &is_open, &var_id,
				   &obj_data[x].has_actions,
				   actions[0],
				   actions[1],
				   actions[2],
				   actions[3],
				   actions[4]
				   ) == EOF)
		{
			printf("OBJ DATA READING ERROR! %d\n", x);
			return 1;
		}
		obj_data[x].count = 0;
	}
	num_objects_loaded = x;
	printf("Loaded %d object data\n", num_objects_loaded);
	return 0;
}

int read_region_list(int region_data[], int size, FILE *fp)
{
	char buffer[100] = {0};
	int x;

	for (x = 0; x < size; x++)
	{
		fgets(buffer, sizeof(buffer), fp);

		if (sscanf(buffer, "%d\t", &region_data[x]) == EOF)
		{
			printf("REGION LIST READING ERROR! %d\n", x);
			return 1;
		}
	}
	printf("Loaded %d regions.\n", x);
	return 0;
}

int main(void)
{
	FILE *fp_rl = fopen("region_list.tsv", "r");
	FILE *fp_obj = fopen("obj_data.tsv", "r");
	FILE *fp_objo = fopen("obj_locs.tsv", "w");
	FILE **map_files;
	
	int rl_lines = 0;
	int obj_lines = 0;
	int x;

	struct obj *obj_data;
	int *region_list;

	if (fp_rl == NULL)
	{
		printf("Region list file not found/could not be opened!\n");
		exit(1);
	}
	if (fp_obj == NULL)
	{
		printf("Object data file not found/could not be opened!\n");
		exit(1);
	}
	if (fp_objo == NULL)
	{
		printf("Object locations file not found/could not be opened!\n");
		exit(1);
	}

	map_files = calloc(4, sizeof(FILE));
	map_files[0] = fopen("map0", "w");
	map_files[1] = fopen("map1", "w");
	map_files[2] = fopen("map2", "w");
	map_files[3] = fopen("map3", "w");
						
	/* Add a comment to the top of the obj_locs file that explains the 
	   format.  */
	fprintf(fp_objo, "#ID\tObj Type\tRegion id\tRegion in x\tRegion in y\t"
					 "World x\tWorld y\theight\tface\n"); 

	/* Stores the lowest and highest x coordinate in the map data, along with
	   the lowest and highest y coordinate, in that order.  */
	int map_range[4] = {-1};

	for (x = 0; x < 4; x++)
	{
		if (map_files[x] == NULL)
		{
			printf("Map file %d loaded null!\n", x);
			return 1;
		}
	}
	if (fp_rl == NULL)
	{
		printf("Region list data file region_list failed to open.\n");
		return 1;
	}
	if (fp_obj == NULL)
	{
		printf("Object data file obj_data failed to open.\n");
		return 1;
	}

	rl_lines = file_num_lines(fp_rl);
	obj_lines = file_num_lines(fp_obj);

	printf("RL Lines: %d Obj_data lines: %d\n", rl_lines, obj_lines);

	obj_data = calloc(obj_lines, sizeof(struct obj));
	region_list = calloc(rl_lines, sizeof(int));

	read_obj_data(obj_data, obj_lines, fp_obj);
	read_region_list(region_list, rl_lines, fp_rl);

	/* Set tracking of region data.  */
	min_region_id = region_list[0];
	max_region_id = region_list[rl_lines - 1];
	printf("Lowest region id: %d Highest region id: %d\n", min_region_id, 
		   max_region_id);

	fclose(fp_obj);
	fclose(fp_rl);

	printf("Created map data array to store info. Min id: %d Max id: %d\n", 
		   0, max_region_id);

	/* Callocs the number of regions needed for storage.  */
	if ((MAP_DATA = calloc(max_region_id, sizeof(int***))) == NULL)
	{
		printf("Error in allocating 4D array pointer for map data!\n");
		return 1;
	}

	
	/* Callocs all the height levels for each region.  */
	for (x = 0; x < max_region_id; x++)
	{
		MAP_DATA[x] = calloc(4, sizeof(int**));

		/* Check if it worked.  */
		if (MAP_DATA[x] == NULL)
		{
			printf("Error in allocating spaces for map data!\n");
			return 1;
		}
	}

	/* Go through each region, get the data from it.  */
	for (x = 0; x < rl_lines; x++)
	{
		char obj_filename[100] = {0};
		char map_filename[100] = {0};
		
		int rv = 0;
		int region_array[4][64][64] = {0};
		int region_id = region_list[x];
		int world_x = (region_id >> 8) * 64;
		int world_y = (region_id & 0xff) * 64;

		int z = 0;
		int z1 = 0;
		int z2 = 0;
		int map_int = 0;
		
		char *map_buffer;
		char *obj_buffer;
		int map_size = 0;
		int obj_size = 0;
		int map_offset = 0;
		int obj_offset = 0;

		map_buffer = calloc(sizeof(char), MAX_MAP_SIZE);
		obj_buffer = calloc(sizeof(char), MAX_OBJ_SIZE);
		
		snprintf(obj_filename, 100, "%s/%d_obj.gz", MAP_DIR, region_id); 
		snprintf(map_filename, 100, "%s/%d_map.gz", MAP_DIR, region_id);

		printf("OPEN: %s %s\n", obj_filename, map_filename);

		gzFile map_dat = gzopen(map_filename, "r");
		gzFile obj_dat = gzopen(obj_filename, "r");
		
		if (map_dat == NULL)
		{
			printf("map null!\n");
			return 1;
		}
		if (obj_dat == NULL)
		{
			printf("obj null!\n");
			return 1;
		}

		/* Read map data into buffer.  */
		while (1)
		{
			rv = gzgetc(map_dat);

			if (rv == -1)
			{
				break;
			}
			else
			{
				if (map_size > MAX_MAP_SIZE)
				{
					printf("MAX MAP SIZE REACHED!\n");
					return 3;
				}
				map_buffer[map_size++] = (char) rv;
			}
		}

		/* Read obj data into buffer.  */
		while (1)
		{
			rv = gzgetc(obj_dat);

			if (rv == -1)
			{
				break;
			}
			else
			{
				if (obj_size > MAX_OBJ_SIZE)
				{
					printf("MAX OBJ SIZE REACHED!\n");
					return 3;
				}
				obj_buffer[obj_size++] = (char) rv;
			}
		}
		
		/* All map data has been read into the program.  */
		printf("ID: %d Map size: %d\n", region_list[x], map_size);
		printf("ID: %d Obj size: %d\n", region_list[x], obj_size);
		printf("X: %d Y: %d\n", world_x, world_y);

		if (map_size > max_map_size)
		{
			max_map_size = map_size;
		}
		if (obj_size > max_obj_size)
		{
			max_obj_size = obj_size;
		}

		/* Read info from map data buffer.  */
		for (z = 0; z < 4; z++)
		{
			for (z1 = 0; z1 < 64; z1++)
			{
				for (z2 = 0; z2 < 64; z2++)
				{
					while (1)
					{
						map_int = read_unsigned_byte(map_buffer, &map_offset);

						if (map_int == 0)
						{
							break;
						}
						else if (map_int == 1)
						{
							read_unsigned_byte(map_buffer, &map_offset);
							break;
						}
						else if (map_int <= 49)
						{
							read_unsigned_byte(map_buffer, &map_offset);
						}
						else if (map_int <= 81)
						{
							region_array[z][z1][z2] = (map_int - 49);
						}
					}
				}
			}
		}
		printf("	Landscape data read.\n");

		for (z = 0; z < 4; z++)
		{
			for (z1 = 0; z1 < 64; z1++)
			{
				for (z2 = 0; z2 < 64; z2++)
				{
					if ((region_array[z][z1][z2] & 1) == 1)
					{
						int height = z;
						if ((region_array[1][z1][z2] & 2) == 2)
						{
							height--;
						}
						if (height >= 0 && height <= 3)
						{
							/* Initialize storing of the map barriers.  */
							if (map_range[0] == -1 )
							{
								map_range[0] = map_range[1] = (world_x + z1);
								map_range[2] = map_range[3] = (world_y + z2);
							}

							/* Check if ranges need to be updated.  */
							if ((world_x + z1) < map_range[0])
							{
								map_range[0] = (world_x + z1);
							}
							if ((world_x + z1) > map_range[1])
							{
								map_range[1] = (world_x + z1);
							}
							if ((world_y + z2) < map_range[2])
							{
								map_range[2] = (world_y + z2);
							}
							if ((world_y + z2) > map_range[3])
							{
								map_range[3] = (world_y + z2);
							}

							add_collision(world_x + z1, world_y + z2, height, 
										  0x200000);
						}
					}
				}
			}
		}
		printf("	Landscape collision applied.\n");

		int incr = 0;
		int object_id = -1;
		int location = 0;
		int incr2 = 0;

		/* Read all of the object data for the region.  */
		do
		{
			incr = read_smart_b(obj_buffer, &obj_offset);
			if (incr == 0)
			{
				break;
			}
			object_id += incr;
			location = 0;
			incr2 = 0;

			do
			{
				incr2 = read_smart_b(obj_buffer, &obj_offset);
				if (incr2 == 0)
				{
					break;
				}
				location += (incr2 - 1);
				int local_x = (location >> 6 & 0x3f);
				int local_y = (location & 0x3f);
				int height = (location >> 12);
				int object_data = read_unsigned_byte(obj_buffer, &obj_offset);
				int type = (object_data >> 2);
				int direction = (object_data & 3);

				if (local_x < 0 || local_x >= 64 
					|| local_y < 0 || local_y >= 64)
				{
					continue;
				}

				if ((region_array[1][local_x][local_y] & 2) == 2)
				{
					height--;	
				}
				/* If the object is within the proper height ranges, apply
				   collision from it.  */
				if (height >= 0 && height <= 3)
				{
					/* Keep track of how many of each object is spawned.  */
					obj_data[object_id].count++;

					/* TODO: Check if object id 4439 needs to be ignored.  */
					if (type >= 0 && type <= 3)
					{
						if (obj_data[object_id].solid == 1)
						{
							mark_wall((world_x + local_x), (world_y + local_y),
									  height, direction, type,
									  obj_data[object_id].walkable);
						}
					}
					else if (type >= 4 && type <= 8)
					{
						/* Wall decoration, ignored.  */
					}
					else if (type >= 9 && type < 22)
					{
						if (obj_data[object_id].solid == 1)
						{
							mark_solid_occupant((world_x + local_x), 
												(world_y + local_y),
												height, 
												obj_data[object_id].size_x,
												obj_data[object_id].size_y,
												direction,
												obj_data[object_id].walkable);
						}
					}
					else if (type == 22)
					{
						if (obj_data[object_id].solid == 1 
							&& obj_data[object_id].has_actions >= 1)
						{
							add_collision(world_x + local_x, world_y + local_y,
										  height, BLOCKED_MASK);
						}
					}

					/* Check if ranges need to be updated.  */
					if ((world_x + local_x) < map_range[0])
					{
						map_range[0] = (world_x + local_x);
					}
					if ((world_x + local_x) > map_range[1])
					{
						map_range[1] = (world_x + local_x);
					}
					if ((world_y + local_y) < map_range[2])
					{
						map_range[2] = (world_y + local_y);
					}
					if ((world_y + local_y) > map_range[3])
					{
						map_range[3] = (world_y + local_y);
					}

					/* If the object has actions assigned to it, save its 
					   location to a file for later use by the server.  */
					if (obj_data[object_id].has_actions == 1
						|| obj_data[object_id].actionable == 1)
					{
						int reg_x = 0, reg_y = 0, reg_id = 0;

						reg_x = ((world_x + local_x) >> 3);
						reg_y = ((world_y + local_y) >> 3);
						reg_id = ((reg_x / 8) << 8) + (reg_y / 8);

						fprintf(fp_objo, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", 
								object_id, type, reg_id, local_x, local_y, 
								(local_x + world_x), (local_y + world_y), 
								height, direction);
					}
				}
			}
			while(1);
		}
		while (1);
		
		printf("	Object collision applied.\n");

		/* Clean up before we go to the next region.  */
		free(map_buffer);
		free(obj_buffer);
		gzclose(map_dat);
		gzclose(obj_dat);
	}

	printf("MIN X: %d MAX X: %d MIN Y: %d MAX Y: %d\n", map_range[0], 
		   map_range[1], map_range[2], map_range[3]);

	/* Save map collision data we gathered into a file for each height 
	   level.  */
	for (int fz = 0; fz < 4; fz++)
	{
		printf("Saving height level %d data...\n", fz);
		for (int fx = map_range[0]; fx <= map_range[1]; fx++)
		{
			for (int fy = map_range[2]; fy <= map_range[3]; fy++)
			{
				int ret = get_collision(fx, fy, fz);
				if (ret != 0)
				{
					fprintf(map_files[fz], "%d\t%d\t%d\n", fx, fy, ret); 
				}
			}
		}
	}
	printf("Done saving all collision data!\n");
	printf("Wrote a list of all objects with actions to the obj_locs.tsv "
		   "file.\n");

	fclose(map_files[0]);
	fclose(map_files[1]);
	fclose(map_files[2]);
	fclose(map_files[3]);

	FILE *freq = fopen("most_used.tsv", "w");
	if (freq != NULL)
	{
		printf("Writing object frequency data to most_used.tsv\n");
		for (x = 0; x < num_objects_loaded; x++)
		{
			fprintf(freq, "%d\t%s\t%s\t%d\t%d\n", obj_data[x].id, 
					obj_data[x].name, obj_data[x].desc, obj_data[x].solid, 
					obj_data[x].count);
		}
	}

	fclose(fp_objo);
	fclose(freq);
	free(obj_data);
	free(region_list);
	free(map_files);

	free_map_data();
	printf("Max map size: %d obj size: %d\n", max_map_size, max_obj_size);
	printf("Exiting.\n");
	return 0;
}
