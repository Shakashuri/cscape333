#ifndef _MISC_H_
#define _MISC_H_

int file_len(FILE *fp);
int file_num_lines(FILE *fp);
void free_map_data(void);
void add_collision(int x, int y, int height, int mask);
int get_collision(int x, int y, int height);
void mark_solid_occupant(int world_x, int world_y, int height, int x_size, 
						 int y_size, int direction, int walkable);
void mark_wall(int world_x, int world_y, int height, int direction, int type,
			   int walkable);

#endif
