#include <stdio.h>
#include <stdlib.h>


#include "config.h"

int file_len(FILE *fp)
{
	int len = 0;

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);

	return len;
}

int file_num_lines(FILE *fp)
{
	int lines = 0;
	char testc = '\0';
	
	rewind(fp);

	while ((testc = fgetc(fp)) != EOF)
	{
		if (testc == '\n')
		{
			lines++;
		}
	}
	rewind(fp);
	return lines;
}

/* If a region will have data in it, creates the array. This is done on
   a case by case basis to save memory usage.  */
void calloc_region(int region_id)
{
	int x, z;

	for (z = 0; z < 4; z++)
	{
		/* Mallocs the z of the 4D array.  */
		if ((MAP_DATA[region_id][z] = calloc(64, sizeof(int*))) == NULL)
		{
			printf("ERROR! Y\n");
			return;
		}
		
		/* Mallocs the x of the 4D array.  */
		for (x = 0; x < 64; x++)
		{
			if ((MAP_DATA[region_id][z][x] = calloc(64, sizeof(int))) == NULL)
			{
				printf("ERROR! Z: %d X: %d\n", z, x);
				return;
			}
		}
	}
}

void free_map_data(void)
{
	int x, y, z;

	for (x = 0; x < max_region_id; x++)
	{
		if (MAP_DATA[x][0] != NULL)
		{
			for (z = 0; z < 4; z++)
			{
				for (y = 0; y < 64; y++)
				{
					free(MAP_DATA[x][z][y]);
				}
				free(MAP_DATA[x][z]);
				MAP_DATA[x][z] = NULL;
			}
		}
		if (MAP_DATA[x] != NULL)
		{
			free(MAP_DATA[x]);
		}
	}
	free(MAP_DATA);
}

void add_collision(int x, int y, int height, int mask)
{
	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < max_region_id)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (MAP_DATA[region_id][height] == NULL)
		{
			calloc_region(region_id);
		}
		
		MAP_DATA[region_id][height][x - region_world_x][y - region_world_y] 
		|= mask;
	}
}

/* Returns the collision data for a specific point in the map.  */
int get_collision(int x, int y, int height)
{
	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < max_region_id)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (x < region_world_x || y < region_world_y)
		{
			return 0;
		}
		
		if (MAP_DATA[region_id][height] == NULL)
		{
			return 0;
		}
		
		return MAP_DATA[region_id][height][x - region_world_x][y - region_world_y];
	}
	return 0;
}

void mark_solid_occupant(int world_x, int world_y, int height, int x_size, 
						 int y_size, int direction, int walkable) 
{
	int x, y;
	int temp = 0;
	int mask = 256;

	/* This flag seems like it is used if the square blocks ranged projectiles
	 * or not. Normally, the check is == 0, but that looks like it makes things
	 * that shouldn't block, blocking. Here I swapped it so it should match
	 * up just fine.  */
	if (walkable != 0)
	{
		mask += PROJ_BLOCKED_MASK;
	}

	/* If facing east or west, swap sizes because object is turned.  */
	if (direction == 1 || direction == 3) 
	{
		temp = x_size;
		x_size = y_size;
		y_size = temp;
	}

	/* Set collisions where they are needed.  */
	for (x = world_x; x < (world_x + x_size); x++)
	{
		for (y = world_y; y < (world_y + y_size); y++)
		{
			add_collision(x, y, height, mask);
		}
	}
}

void mark_wall(int world_x, int world_y, int height, int direction, int type,
			   int walkable) 
{
	if (type == 0) 
	{
		if (direction == 0) 
		{
			add_collision(world_x, world_y, height, W_WALL_MASK);
			add_collision(world_x - 1, world_y, height, E_WALL_MASK);
		}
		if (direction == 1) 
		{
			add_collision(world_x, world_y, height, N_WALL_MASK);
			add_collision(world_x, world_y + 1, height, S_WALL_MASK);
		}
		if (direction == 2) 
		{
			add_collision(world_x, world_y, height, E_WALL_MASK);
			add_collision(world_x + 1, world_y, height, W_WALL_MASK);
		}
		if (direction == 3) 
		{
			add_collision(world_x, world_y, height, S_WALL_MASK);
			add_collision(world_x, world_y - 1, height, N_WALL_MASK);
		}
	}

	if (type == 1 || type == 3) 
	{
		if (direction == 0) 
		{
			add_collision(world_x, world_y, height, NW_WALL_MASK);
			add_collision(world_x - 1, world_y + 1, height, SE_WALL_MASK);
		}
		if (direction == 1)
		{
			add_collision(world_x, world_y, height, NE_WALL_MASK);
			add_collision(world_x + 1, world_y + 1, height, SW_WALL_MASK);
		}
		if (direction == 2) 
		{
			add_collision(world_x, world_y, height, SE_WALL_MASK);
			add_collision(world_x + 1, world_y - 1, height, NW_WALL_MASK);
		}
		if (direction == 3) 
		{
			add_collision(world_x, world_y, height, SW_WALL_MASK);
			add_collision(world_x - 1, world_y - 1, height, NE_WALL_MASK);
		}
	}
	if (type == 2)
	{
		if (direction == 0) 
		{
			add_collision(world_x, world_y, height, W_WALL_MASK);
			add_collision(world_x, world_y, height, N_WALL_MASK);
			add_collision(world_x - 1, world_y, height, E_WALL_MASK);
			add_collision(world_x, world_y + 1, height, S_WALL_MASK);
		}
		if (direction == 1) 
		{
			add_collision(world_x, world_y, height, E_WALL_MASK);
			add_collision(world_x, world_y, height, N_WALL_MASK);
			add_collision(world_x, world_y + 1, height, S_WALL_MASK);
			add_collision(world_x + 1, world_y, height, W_WALL_MASK);
		}
		if (direction == 2) 
		{
			add_collision(world_x, world_y, height, S_WALL_MASK);
			add_collision(world_x, world_y, height, E_WALL_MASK);
			add_collision(world_x + 1, world_y, height, W_WALL_MASK);
			add_collision(world_x, world_y - 1, height, N_WALL_MASK);
		}
		if (direction == 3) 
		{
			add_collision(world_x, world_y, height, W_WALL_MASK);
			add_collision(world_x, world_y, height, S_WALL_MASK);
			add_collision(world_x, world_y - 1, height, N_WALL_MASK);
			add_collision(world_x - 1, world_y, height, E_WALL_MASK);
		}
	}

	/* These affect projectile collision.  */
	if (walkable == 1) 
	{
		if (type == 0) 
		{
			if (direction == 0) 
			{
				add_collision(world_x, world_y, height, W_BLOCKED_MASK);
				add_collision(world_x - 1, world_y, height, E_BLOCKED_MASK);
			}
			if (direction == 1)
			{
				add_collision(world_x, world_y, height, N_BLOCKED_MASK);
				add_collision(world_x, world_y + 1, height, S_BLOCKED_MASK);
			}
			if (direction == 2)
			{
				add_collision(world_x, world_y, height, E_BLOCKED_MASK);
				add_collision(world_x + 1, world_y, height, W_BLOCKED_MASK);
			}
			if (direction == 3) 
			{
				add_collision(world_x, world_y, height, S_BLOCKED_MASK);
				add_collision(world_x, world_y - 1, height, N_BLOCKED_MASK);
			}
		}
		if (type == 1 || type == 3) 
		{
			if (direction == 0) 
			{
				add_collision(world_x, world_y, height, NW_BLOCKED_MASK);
				add_collision(world_x - 1, world_y + 1, height, 
							  SE_BLOCKED_MASK);
			}
			if (direction == 1) 
			{
				add_collision(world_x, world_y, height, NE_BLOCKED_MASK);
				add_collision(world_x + 1, world_y + 1, height,
							  SW_BLOCKED_MASK);
			}
			if (direction == 2) 
			{
				add_collision(world_x, world_y, height, SE_BLOCKED_MASK);
				add_collision(world_x + 1, world_y - 1, height,
							  NW_BLOCKED_MASK);
			}
			if (direction == 3) 
			{
				add_collision(world_x, world_y, height, SW_BLOCKED_MASK);
				add_collision(world_x - 1, world_y - 1, height,
							  NE_BLOCKED_MASK);
			}
		}
		if (type == 2) 
		{
			if (direction == 0) 
			{
				add_collision(world_x, world_y, height, W_BLOCKED_MASK);
				add_collision(world_x, world_y, height, N_BLOCKED_MASK);
				add_collision(world_x - 1, world_y, height, E_BLOCKED_MASK);
				add_collision(world_x, world_y + 1, height, S_BLOCKED_MASK);
			}
			if (direction == 1) 
			{
				add_collision(world_x, world_y, height, N_BLOCKED_MASK);
				add_collision(world_x, world_y, height, E_BLOCKED_MASK);
				add_collision(world_x, world_y + 1, height, S_BLOCKED_MASK);
				add_collision(world_x + 1, world_y, height, W_BLOCKED_MASK);
			}
			if (direction == 2) 
			{
				add_collision(world_x, world_y, height, E_BLOCKED_MASK);
				add_collision(world_x, world_y, height, S_BLOCKED_MASK);
				add_collision(world_x + 1, world_y, height, W_BLOCKED_MASK);
				add_collision(world_x, world_y - 1, height, N_BLOCKED_MASK);
			}
			if (direction == 3) 
			{
				add_collision(world_x, world_y, height, S_BLOCKED_MASK);
				add_collision(world_x, world_y, height, W_BLOCKED_MASK);
				add_collision(world_x, world_y - 1, height, N_BLOCKED_MASK);
				add_collision(world_x - 1, world_y, height, E_BLOCKED_MASK);
			}
		}
	}
}
