#ifndef _CONFIG_H_
#define _CONFIG_H_

/* Values for 333:
	MAX_MAP_SIZE 53752
	MAX_OBJ_SIZE 16206
	
   Values for 377:
   	MAX_MAP_SIZE 60688
	MAX_OBJ_SIZE 32160
*/

#define MAX_MAP_SIZE 60688
#define MAX_OBJ_SIZE 32160

extern int ****MAP_DATA;
extern int min_region_id;
extern int max_region_id;

struct obj
{
	int id;
	char name[100];
	char desc[200];
	int size_x;
	int size_y;
	int occupied;
	int solid;
	int walkable;
	char action[100];
	int has_actions;
	int actionable;
	int unknown;
	int count;
};

/**
 * Collision mask information, bit flags used for movement handling.
 *
 * @warning KEEP AS IS OR THINGS _WILL_ BREAK!
 * @warning If changing these are REALLY needed, ensure that the flags in the
 * collision extraction utility match these. If not synced, issues will occur.
 */
enum COLLISION_MASKS
{
	/**@{*/
	/** Wall masks.  */
	N_WALL_MASK  = 0x2,
	NE_WALL_MASK = 0x4,
	NW_WALL_MASK = 0x1,
	S_WALL_MASK  = 0x20,
	SE_WALL_MASK = 0x10,
	SW_WALL_MASK = 0x40,
	W_WALL_MASK  = 0x80,
	E_WALL_MASK  = 0x8,
	/**@}*/

	/**@{*/
	/** Projectile blocked masks.  */
	N_BLOCKED_MASK = 0x400,
	NE_BLOCKED_MASK = 0x800,
	NW_BLOCKED_MASK = 0x200,
	S_BLOCKED_MASK = 0x4000,
	SE_BLOCKED_MASK = 0x2000,
	SW_BLOCKED_MASK = 0x8000,
	W_BLOCKED_MASK = 0x10000,
	E_BLOCKED_MASK = 0x1000,
	/**@}*/

	/** Same as BLOCKED_MASK, may not really be needed.  */
	OCCUPIED_MASK = 0x100,
	/** Can't shoot a projectile over this space.  */
	PROJ_BLOCKED_MASK = 0x20000,
	/** Space completely blocked off.  */
	BLOCKED_MASK = 0x200000,

	/** Mask applied when an NPC or player enters the space, prevents multiple
	 * NPCs in one space.  */
	TAKEN_MASK = 0x400000,
	/** A few NPCs block players so that they can't move into their square,
	 * walk through them. This flag handles that.  */
	TAKEN_MASK_BLOCK_PLAYER = 0x800000
};

#endif
