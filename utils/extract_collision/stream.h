#ifndef _STREAM_H_
#define _STREAM_H_

int read_unsigned_byte(char buffer[], int *position);
int read_smart_b(char buffer[], int *position);

#endif
