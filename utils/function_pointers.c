#include <stdio.h>

int test[5] = {5, 66, 22, 3, 119};
int test2[5] = {203, 33, 3214, 32, 3};

int compare(int x, int y)
{
	if (y <= x)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void loop(int (*f_ptr)(int, int))
{
	int x = 0;

	for (x = 0; x < 5; x++)
	{
		printf("1: %d 2: %d\n", test[x], test2[x]);
		printf("comp: %d\n", (*f_ptr)(test[x], test2[x]));
	}
}

int main(void)
{
	int (*ptr)(int, int) = &compare;

	loop(ptr);
}
