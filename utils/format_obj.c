#include <stdio.h>
#include <stdlib.h>

struct object
{
	int id;
	char name[100];
	char desc[200];
	int size_x;
	int size_y;
};

struct obj_test
{
	int id;
	int size_x;
	int size_y;
};

struct object *OBJECTS[20000];
struct obj_test *OBJECTS_TEST[1000];

int load_objects(void)
{
	int count = 0, x = 0;
	FILE *fp;
	char buffer[200] = {0};
	struct object zeroed_object = {0};

	if ((fp = fopen("objects.txt", "r")) != NULL)
	{
		printf("Opened objects file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct object *obj = malloc(sizeof(struct object));
		*obj = zeroed_object;

		if (sscanf(buffer, "%d\t%[^\t]\t%[^\t]\t"
						   "%d\t%d",
					&obj->id, obj->name, obj->desc, 
					&obj->size_x, &obj->size_y) != EOF)
		{
			OBJECTS[count] = obj;
			count++;
		}
		else
		{
			break;
		}
	}
}

int load_objects_test(void)
{
	int count = 0, x = 0;
	FILE *fp;
	char buffer[200] = {0};
	struct obj_test zeroed_object = {0};

	if ((fp = fopen("obj_data.txt", "r")) != NULL)
	{
		printf("Opened objects file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct obj_test *obj = malloc(sizeof(struct obj_test));
		*obj = zeroed_object;

		if (sscanf(buffer, "%d Size X: %d Size Y: %d",
					&obj->id, &obj->size_x, &obj->size_y) != EOF)
		{
			OBJECTS_TEST[count] = obj;
			count++;
		}
		else
		{
			break;
		}
	}
}


int main(void)
{
	int x;
	
	load_objects();
	load_objects_test();
	printf("Finished loading objects\n");

	for (x = 0; x < 1000; x++)
	{
		if (OBJECTS_TEST[x] != NULL)
		{
			if (OBJECTS[OBJECTS_TEST[x]->id]->size_x != OBJECTS_TEST[x]->size_x)
			{
				printf("ID: %d ID: %d OLD_SIZE: %d NEW_SIZE: %d\n", OBJECTS[OBJECTS_TEST[x]->id]->id, 
							OBJECTS_TEST[x]->id, OBJECTS[OBJECTS_TEST[x]->id]->size_x, OBJECTS_TEST[x]->size_x);
			}
			if (OBJECTS[OBJECTS_TEST[x]->id]->size_y != OBJECTS_TEST[x]->size_y)
			{
				printf("YID: %d ID: %d OLD_SIZE: %d NEW_SIZE: %d\n", OBJECTS[OBJECTS_TEST[x]->id]->id, 
							OBJECTS_TEST[x]->id, OBJECTS[OBJECTS_TEST[x]->id]->size_y, OBJECTS_TEST[x]->size_y);
			}
		}
	}
}
