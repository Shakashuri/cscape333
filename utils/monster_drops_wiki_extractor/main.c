#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum DROP_TABLE_TYPE
{
	NORMAL = 0,
	ALWAYS = 1,
	PREROLL = 2,
	TERTIARY = 3
};

char *get_drop_table_type_string(int type)
{
	switch (type)
	{
		case NORMAL:
			return "normal";
		case ALWAYS:
			return "always";
		case PREROLL:
			return "preroll";
		case TERTIARY:
			return "tertiary";
		default:
			return "NO_TYPE!!!!!";
	}
}

struct drop_table
{
	int category;
	int num_drops;

	char **item;
	char **quantity;
	char **rarity; 

	int *quantity_min;
	int *quantity_max;

	float *rarity_numerator;
	float *rarity_denominator;
};

struct monster
{
	int id;
	char name[200];
	char is_jagex[10];

	struct drop_table drops[40];
	int num_drop_tables;

	int uses_gem_drop;
	float gem_drop_numerator;
	float gem_drop_denominator;

	int gem_drop_nature_talisman;
	int gem_drop_chaos_talisman;

	char gem_drop_notes[2000];
	
	int uses_rare_drop;
	float rare_drop_numerator;
	float rare_drop_denominator;

	int rare_drop_nature_talisman;
	int rare_drop_chaos_talisman;

	char rare_drop_notes[2000];

	/* Misc drops chance.  */
	float allotment_drop_denominator;
	float allotment_drop_numerator;
	
	float general_seed_drop_denominator;
	float general_seed_drop_numerator;
	
	float herb_drop_denominator;
	float herb_drop_numerator;
	
	float rare_seed_drop_denominator;
	float rare_seed_drop_numerator;
	
	float talisman_drop_denominator;
	float talisman_drop_numerator;
	
	float tree_herb_seed_drop_denominator;
	float tree_herb_seed_drop_numerator;
	
	float uncommon_seed_drop_denominator;
	float uncommon_seed_drop_numerator;
	
	float useful_herb_drop_denominator;
	float useful_herb_drop_numerator;
};

struct monster **MONSTER_DATA;

int HIGHEST_TABLE_COUNT = 0;
int HIGHEST_ENTRIES_COUNT = 0;

char highest_table_name[400] = {'\0'};
char highest_entries_name[400] = {'\0'};

void printf_monster_table(struct monster *mon, int idx)
{
	if (mon == NULL)
	{
		return;
	}

	printf("Category: %s\n", 
		get_drop_table_type_string(mon->drops[idx].category));

	for (int x = 0; x < mon->drops[idx].num_drops; x++)
	{
		if (mon->drops[idx].quantity_min[x] == mon->drops[idx].quantity_max[x])
		{
			printf("%d: %s %d %.2f/%.2f\n", x, mon->drops[idx].item[x], 
					mon->drops[idx].quantity_max[x], 
					mon->drops[idx].rarity_numerator[x],
					mon->drops[idx].rarity_denominator[x]);
		}
		else
		{
			printf("%d: %s %d-%d %.2f/%.2f\n", x, mon->drops[idx].item[x], 
					mon->drops[idx].quantity_min[x], 
					mon->drops[idx].quantity_max[x], 
					mon->drops[idx].rarity_numerator[x],
					mon->drops[idx].rarity_denominator[x]);
		}
	}
}

int fprintf_monster_drops(FILE *fp, struct monster *mon)
{
	if (mon == NULL)
	{
		return 1;
	}

	if (mon->name[0] != '\0')
	{
		fprintf(fp, "%s\t", mon->name);
	}
	else
	{
		fprintf(fp, "null\t");
	}

	if (mon->rare_drop_numerator != 0.0)
	{
		fprintf(fp, "1\t%f\t%f\t", mon->rare_drop_numerator, 
			mon->rare_drop_denominator);
	}
	else
	{
		fprintf(fp, "0\t0.0\t0.0\t");
	}

	fprintf(fp, "\n");
	return 1;
}

int parse_gem_drop_token(struct monster *mon, char *token)
{
	char buffer[200];

	if (token == NULL)
	{
		return 1;
	}

	printf("#################%s\n", token);

	sscanf(token, "%f/%f", &mon->gem_drop_numerator,
		&mon->gem_drop_denominator);

	if (sscanf(token, "naturetalisman=%s", buffer) == 1)
	{
		mon->gem_drop_nature_talisman = 1;
	}
	
	if (sscanf(token, "chaostalisman=%s", buffer) == 1)
	{
		mon->gem_drop_chaos_talisman = 1;
	}

	sscanf(token, "version=%s", mon->gem_drop_notes);

	return 0;
}

int parse_rare_drop_token(struct monster *mon, char *token)
{
	char buffer[200];

	if (token == NULL)
	{
		return 1;
	}

	printf("#################%s\n", token);

	if (mon->rare_drop_numerator != 0.0)
	{
		sscanf(token, "%f/%f", &mon->gem_drop_numerator,
			&mon->gem_drop_denominator);
	}
	else
	{
		sscanf(token, "%f/%f", &mon->rare_drop_numerator,
			&mon->rare_drop_denominator);
	}

	if (sscanf(token, "naturetalisman=%s", buffer) == 1)
	{
		mon->rare_drop_nature_talisman = 1;
	}
	
	if (sscanf(token, "chaostalisman=%s", buffer) == 1)
	{
		mon->rare_drop_chaos_talisman = 1;
	}

	sscanf(token, "version=%s", mon->rare_drop_notes);

	return 0;
}

int parse_chance_drop_token(struct monster *mon, char *token, 
	float *numerator, float *denominator)
{
	if (token == NULL)
	{
		return 1;
	}

	if ((*numerator) != 0.0)
	{
		sscanf(token, "%f/%f", numerator,
			denominator);
	}
	else
	{
		sscanf(token, "%f/%f", numerator, denominator);
	}

	return 0;
}

int parse_monster_info_string(struct monster *mon, FILE *fp)
{
	char buffer[10000];
	int category = NORMAL;
	long before_position = 0;

	while (before_position = ftell(fp), fgets(buffer, 10000, fp) != NULL)
	{
		if (strncmp("{{Infobox Monster", buffer, 17) == 0)
		{
			printf("Name: %s\n", mon->name);
			printf("This monster is done!\n");
			fseek(fp, before_position, SEEK_SET);
			return 1;
		}
		else if (strncmp("==Drops==", buffer, 9) == 0)
		{
			printf("start of drops!\n");
			category = NORMAL;
		}
		else if (strncmp("===100%===", buffer, 10) == 0
				 || strncmp("====100%====", buffer, 12) == 0
				 || strncmp("===100% drops===", buffer, 12) == 0)
		{
			printf("start of 100%% drops!\n");
			category = ALWAYS;
		}
		else if (strncmp("===Tertiary===", buffer, 14) == 0
				 || strncmp("====Tertiary====", buffer, 16) == 0)
		{
			printf("start of Tertiary drops!\n");
			category = TERTIARY;
		}
		else if (strncmp("===Pre-roll===", buffer, 14) == 0
				 || strncmp("====Pre-roll====", buffer, 16) == 0)
		{
			printf("start of Pre-roll drops!\n");
			category = PREROLL;
		}
		else if (strncmp("{{GemDropTable", buffer, 14) == 0)
		{
			char *token;
			printf("Start of gem drop!\n");

			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->gem_drop_numerator != 0.0)
			{
				continue;
			}

			parse_gem_drop_token(mon, token);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				printf("tok: %s\n", token);
				parse_gem_drop_token(mon, token);
			}

			printf("Gem chance: %f/%f\n", mon->gem_drop_numerator,
				mon->gem_drop_denominator);
			printf("\tNatty: %d Chaos: %d\n", mon->gem_drop_nature_talisman, 
				mon->gem_drop_chaos_talisman);
			printf("\tNotes: %s\n", mon->gem_drop_notes);
		}
		else if (strncmp("{{RareDropTable", buffer, 14) == 0)
		{
			char *token;
			printf("Start of rare drop!\n");

			token = strtok(buffer, "|");
			printf("tok: %s\n", token);

			parse_rare_drop_token(mon, token);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				printf("tok: %s\n", token);
				parse_rare_drop_token(mon, token);
			}

			printf("Rare chance: %f/%f\n", mon->rare_drop_numerator,
				mon->rare_drop_denominator);
			printf("\tNatty: %d Chaos: %d\n", mon->rare_drop_nature_talisman, 
				mon->rare_drop_chaos_talisman);
			printf("\tNotes: %s\n", mon->rare_drop_notes);
		}
		else if (strncmp("{{AllotmentSeedDropTable", buffer, 24) == 0)
		{
			char *token;

			printf("start of allotment drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->allotment_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->allotment_drop_numerator, 
				&mon->allotment_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->allotment_drop_numerator, 
					&mon->allotment_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Allot: %f/%f\n", mon->allotment_drop_numerator, 
				mon->allotment_drop_denominator);
		}
		else if (strncmp("{{GeneralSeedDropTable", buffer, 22) == 0)
		{
			char *token;

			printf("start of general_seed drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);

			if (mon->general_seed_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->general_seed_drop_numerator, 
				&mon->general_seed_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->general_seed_drop_numerator, 
					&mon->general_seed_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("General seed: %f/%f\n", mon->general_seed_drop_numerator, 
				mon->general_seed_drop_denominator);
		}
		else if (strncmp("{{HerbDropTable", buffer, 15) == 0)
		{
			char *token;

			printf("start of herb drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);

			if (mon->herb_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->herb_drop_numerator, 
				&mon->herb_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->herb_drop_numerator, 
					&mon->herb_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Herb: %f/%f\n", mon->herb_drop_numerator, 
				mon->herb_drop_denominator);
		}
		else if (strncmp("{{RareSeedDropTable", buffer, 19) == 0)
		{
			char *token;

			printf("start of rare seed drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->rare_seed_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->rare_seed_drop_numerator, 
				&mon->rare_seed_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->rare_seed_drop_numerator, 
					&mon->rare_seed_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Rare seed: %f/%f\n", mon->rare_seed_drop_numerator, 
				mon->rare_seed_drop_denominator);
		}
		else if (strncmp("{{TalismanDropTable", buffer, 19) == 0)
		{
			char *token;

			printf("start of talisman drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->talisman_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->talisman_drop_numerator, 
				&mon->talisman_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->talisman_drop_numerator, 
					&mon->talisman_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Talisman: %f/%f\n", mon->talisman_drop_numerator, 
				mon->talisman_drop_denominator);
		}
		else if (strncmp("{{TreeHerbSeedDropTable", buffer, 23) == 0)
		{
			char *token;

			printf("start of tree herb drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->tree_herb_seed_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->tree_herb_seed_drop_numerator, 
				&mon->tree_herb_seed_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->tree_herb_seed_drop_numerator, 
					&mon->tree_herb_seed_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Rare tree-herb seed: %f/%f\n", mon->tree_herb_seed_drop_numerator, 
				mon->tree_herb_seed_drop_denominator);
		}
		else if (strncmp("{{UncommonSeedDropTable", buffer, 23) == 0)
		{
			char *token;

			printf("start of uncommon seed drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->uncommon_seed_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->uncommon_seed_drop_numerator, 
				&mon->uncommon_seed_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->uncommon_seed_drop_numerator, 
					&mon->uncommon_seed_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Uncommon seed: %f/%f\n", mon->uncommon_seed_drop_numerator, 
				mon->uncommon_seed_drop_denominator);
		}
		else if (strncmp("{{UsefulHerbDropTable", buffer, 23) == 0)
		{
			char *token;

			printf("start of useful herb drop\n");
			
			token = strtok(buffer, "|");
			printf("tok: %s\n", token);
			
			if (mon->useful_herb_drop_numerator != 0.0)
			{
				continue;
			}

			parse_chance_drop_token(mon, token, 
				&mon->useful_herb_drop_numerator, 
				&mon->useful_herb_drop_denominator);

			while (token != NULL)
			{
				token = strtok(NULL, "|");
				parse_chance_drop_token(mon, token, 
					&mon->useful_herb_drop_numerator, 
					&mon->useful_herb_drop_denominator);
			}

			fgets(buffer, 10000, fp);

			printf("Uncommon seed: %f/%f\n", mon->uncommon_seed_drop_numerator, 
				mon->uncommon_seed_drop_denominator);
		}
		else if (strncmp("{{DropLogProject|jagex=", buffer, 9) == 0)
		{
			sscanf(buffer, "{{DropLogProject|jagex=%[^}]", mon->is_jagex);

			printf("Jagex drops: %s\n", mon->is_jagex);
		}
		else if (strncmp("{{DropsTableHead}}", buffer, 18) == 0)
		{
			printf("start of a drop table.\n");
			long start_pos = ftell(fp);
			int num_entries = 0;

			while (fgets(buffer, 10000, fp) != NULL)
			{
				if (strncmp("{{DropsTableBottom", buffer, 18) == 0)
				{
					/* Reset the category after a table has been read.  */
					printf("end of table\n");
					printf("num entries: %d\n", num_entries);
					fseek(fp, start_pos, SEEK_SET);
					break;
				}
				else
				{
					num_entries++;
				}
			}

			if (num_entries > 0)
			{
				int drop_table_index = mon->num_drop_tables;
				mon->drops[drop_table_index].category = category;
				mon->drops[drop_table_index].num_drops = num_entries;

				mon->drops[drop_table_index].item 
					= calloc(sizeof (char **), num_entries);
				mon->drops[drop_table_index].quantity 
					= calloc(sizeof (char **), num_entries);
				mon->drops[drop_table_index].rarity 
					= calloc(sizeof (char **), num_entries);
				
				mon->drops[drop_table_index].rarity 
					= calloc(sizeof (char **), num_entries);
						
				mon->drops[drop_table_index].rarity_numerator 
					= calloc(sizeof (float *), num_entries);
				mon->drops[drop_table_index].rarity_denominator
					= calloc(sizeof (float *), num_entries);

				mon->drops[drop_table_index].quantity_min
					= calloc(sizeof (int *), num_entries);
				mon->drops[drop_table_index].quantity_max
					= calloc(sizeof (int *), num_entries);
				

				for (int x = 0; x < num_entries; x++)
				{
					mon->drops[drop_table_index].item[x] 
						= calloc(sizeof (char *), 300);
					mon->drops[drop_table_index].quantity[x] 
						= calloc(sizeof (char *), 300);
					mon->drops[drop_table_index].rarity[x] 
						= calloc(sizeof (char *), 300);
					
					mon->drops[drop_table_index].rarity_numerator 
						= calloc(sizeof (float), num_entries);
					mon->drops[drop_table_index].rarity_denominator
						= calloc(sizeof (float), num_entries);

					mon->drops[drop_table_index].quantity_min
						= calloc(sizeof (int), num_entries);
					mon->drops[drop_table_index].quantity_max
						= calloc(sizeof (int), num_entries);
				}

				if (num_entries > HIGHEST_ENTRIES_COUNT)
				{
					HIGHEST_ENTRIES_COUNT = num_entries;
					strncpy(highest_entries_name, mon->name, 200);
				}

				for (int x = 0; x < num_entries; x++)
				{
					fgets(buffer, 1000, fp);

					sscanf(buffer, "{{DropsLine|Name=%[^|]|Quantity=%[^|&]|Rarity=%[^|&}]", 
						mon->drops[drop_table_index].item[x],
						mon->drops[drop_table_index].quantity[x],
						mon->drops[drop_table_index].rarity[x]);

					printf("Item: %s Quantity: %s Rarity: %s\n", 
						mon->drops[drop_table_index].item[x],
						mon->drops[drop_table_index].quantity[x],
						mon->drops[drop_table_index].rarity[x]);

					/* Rarity setup.  */
					if (strncmp(mon->drops[drop_table_index].rarity[x], "Always", 6) == 0
						|| strncmp(mon->drops[drop_table_index].rarity[x], "always", 6) == 0)
					{
						mon->drops[drop_table_index].rarity_numerator[x] = 
							mon->drops[drop_table_index].rarity_denominator[x]
							= 1.0;
					}
					else
					{
						sscanf(mon->drops[drop_table_index].rarity[x],
							"%f/%f", 
							&mon->drops[drop_table_index].rarity_numerator[x],
							&mon->drops[drop_table_index].rarity_denominator[x]);

					}
					
					printf("\tRarity Num: %f Denom: %f\n", 
						mon->drops[drop_table_index].rarity_numerator[x],
						mon->drops[drop_table_index].rarity_denominator[x]);

					/* Quantity setup.  */
					sscanf(mon->drops[drop_table_index].quantity[x],
						"%d-%d", 
						&mon->drops[drop_table_index].quantity_min[x],
						&mon->drops[drop_table_index].quantity_max[x]);

					if (mon->drops[drop_table_index].quantity_min[x] != 0 
						&& mon->drops[drop_table_index].quantity_max[x] == 0)
					{
						mon->drops[drop_table_index].quantity_max[x] 
							= mon->drops[drop_table_index].quantity_min[x];
					}
					
					printf("\tQuantity Min: %d Max: %d\n", 
						mon->drops[drop_table_index].quantity_min[x],
						mon->drops[drop_table_index].quantity_max[x]);
				}

				drop_table_index = mon->num_drop_tables++;

				if (mon->num_drop_tables > HIGHEST_TABLE_COUNT)
				{
					HIGHEST_TABLE_COUNT = mon->num_drop_tables;
					strncpy(highest_table_name, mon->name, 200);
				}
			}
			
			category = NORMAL;
		}
		else
		{
			if (mon->name[0] == '\0' 
				&& ((sscanf(buffer, "|name = %[^\n]", mon->name) == 1)
					|| sscanf(buffer, "|name1 = %[^\n]", mon->name) == 1))
			{
				printf("****************NAME: %s\n", mon->name);
			}
		}
	}

	return 0;
}

int main(void)
{
	char buffer[1000];
	FILE *wiki = fopen("osrs_wiki.xml", "r");
	int current_index = 0;

	MONSTER_DATA = calloc(sizeof(struct monster*), 2000);

	while(fgets(buffer, 1000, wiki) != NULL)
	{
		if (strncmp("{{Infobox Monster", buffer, 17) == 0)
		{
			printf("Start of a new monster! %d\n", current_index);
			MONSTER_DATA[current_index] = calloc(sizeof(struct monster), 1);

			if (MONSTER_DATA[current_index] == NULL)
			{
				printf(" F U C K\n");
				exit(1);
			}

			parse_monster_info_string(MONSTER_DATA[current_index], wiki);

			current_index++;
		}
	}

	for (int x = 0; x < current_index; x++)
	{
		for (int y = 0; y < MONSTER_DATA[x]->num_drop_tables; y++)
		{
			if (MONSTER_DATA[x]->drops[y].category == PREROLL)
			{
				printf("Monster %s has preroll!\n", MONSTER_DATA[x]->name);
				printf_monster_table(MONSTER_DATA[x], y);
			}
		}
	}

	printf("Highest num tables: %d\n", HIGHEST_TABLE_COUNT);
	printf("\tName: %s\n", highest_table_name);
	printf("Highest num entries: %d\n", HIGHEST_ENTRIES_COUNT);
	printf("\tName: %s\n", highest_entries_name);

	FILE *output = fopen("output.tsv", "w+");

	printf("starting output to output.tsv\n");

	fprintf(output, "NAME\tRARE_TABLE\tNUM\tDENOM\n");
	for (int x = 0; x < current_index; x++)
	{
		fprintf_monster_drops(output, MONSTER_DATA[x]);

	}

	fclose(output);
}
