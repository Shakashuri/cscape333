#include <stdio.h>
#include <stdlib.h>

int read_short(char buffer[], int *position) 
{
	int i = 0;
	*position += 2;
	i = ((buffer[*position - 2] & 0xff) << 8)
		 + (buffer[*position - 1] & 0xff);
	if (i > 32767)
	{
		i -= 0x10000;
	}
	return i;
}

int read_unsigned_le_short(char buffer[], int *position) 
{
	*position += 2;
	return ((buffer[*position - 2] & 0xff) << 8)
			+ (buffer[*position - 1] & 0xff);
}

int read_bits(int num_bits, char buffer[], int *position, int *bits_read,
			  int *char_unk) 
{
	int j;

	do 
	{
		if (*bits_read >= num_bits) 
		{
			int k;
			k = (*char_unk) >> ((*bits_read) - num_bits) 
				& ((1 << num_bits) - 1);
			*bits_read -= num_bits;
			j = k;
			break;
		}
		(*char_unk) = ((*char_unk) << 8 | (buffer[*position] & 0xff));
		*bits_read += 8;
		(*position)++;
	} 
	while (1);
	
	return j;
}

int read_int(char buffer[], int *position) 
{
	*position += 4;
	return ((buffer[*position - 4] & 0xff) << 24)
			+ ((buffer[*position - 3] & 0xff) << 16)
			+ ((buffer[*position - 2] & 0xff) << 8)
			+ (buffer[*position - 1] & 0xff);
}

char read_unsigned_char(char buffer[], int *position, int *bits_read,
						int *char_unk) 
{
	return (char) read_bits(8, buffer, position, bits_read, char_unk);
}

int read_unsigned_byte(char buffer[], int *position) 
{
	return buffer[(*position)++] & 0xff;
}

int read_value(char buffer[], int *position) 
{
	return buffer[(*position)++];
}

int read_string(char buffer[], int *position, char string[], int string_size) 
{
	int z, string_pos;
	int start_pos = *position;

	/* Read until a newline character is hit, this signals the end of the 
	   string in 333 format.  */
	while (buffer[(*position)++] != 10)
	{
		/* It looks like string formatting changes halfway through some caches,
		   this fixes it.  */
		if (buffer[(*position)] == '\0')
		{
			break;
		}
	}
	
	string_pos = 0;

	if ((*position - start_pos) > string_size)
	{
		printf("String not large enough! Requested: %d Have: %d\n", 
			   (*position - start_pos), string_size);
		exit(1);
	}
	
	/* Enter the chars into the new string from where it is needed.  */
	for (z = start_pos; z < *position;)
	{
		string[string_pos++] = read_unsigned_byte(buffer, &z);
		if (string[string_pos - 1] == '\n')
		{
			string[string_pos - 1] = '\0';
		}
	}
	
	/* Return string size.  */
	return (*position - start_pos);
}
