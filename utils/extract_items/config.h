#ifndef _CONFIG_H_
#define _CONFIG_H_

struct item_data
{
	char name[200];
	char description[200];
	int model_id;
	int model_zoom;
	int model_rotation_x;
	int model_rotation_y;
	int model_rotation_z;
	int model_scale_x;
	int model_scale_y;
	int model_scale_z;
	int model_offset1;
	int model_offset2;
	int light_modifier;
	int shadow_modifier;
	int team_id;
	int stackable;
	int value;
	int is_members;
	char **actions;
	char **ground_actions;

	int note_id;
	int note_template_id;
	int *stackable_ids;
	int *stackable_amounts;

};


#endif
