#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

/* Filenames need to be changed for each version.  */
/* 333 version filenames.  */
char *index_file_name = {"1/-1667598946.dat"};
char *data_file_name = {"1/-1667617738.dat"};
char *out_name = {"item_data.tsv"};

char note_desc[] = {"Swap this note at any bank for the equivalent item."};

char *item_data;
char *index_data;
int *item_offsets;

struct item_data **ITEM_DEFS;
int ITEM_DEFS_SIZE;

void load_files(void)
{
	FILE *fp_data = fopen(data_file_name, "r");
	FILE *fp_index = fopen(index_file_name, "r");
	int data_size = file_len(fp_data);
	int index_size = file_len(fp_index);
	int num_offsets_size = 0;
	int offset = 0;
	int read_offset = 0;
	int x = 0;

	printf("Data size: %d Index size: %d\n", data_size, index_size);

	/* Read all of the data file into the buffer.  */
	item_data = calloc(data_size, sizeof(char));

	for (x = 0; x < data_size; x++)
	{
		item_data[x] = fgetc(fp_data);
	}

	printf("Data read\n");

	/* Read the index file into the buffer, and read the offsets from it.  */
	index_data = calloc(index_size, sizeof(char));

	for (x = 0; x < index_size; x++)
	{
		index_data[x] = fgetc(fp_index);
	}
	
	/* Read how big the offset block is.  */
	ITEM_DEFS_SIZE = num_offsets_size 
		= read_unsigned_le_short(index_data, &read_offset);

	ITEM_DEFS = calloc(num_offsets_size, sizeof(struct item_data *));
	item_offsets = calloc(num_offsets_size, sizeof(int));

	printf("Num offsets: %d\n", num_offsets_size);

	offset = 2;
	for (x = 0; x < num_offsets_size; x++)
	{
		item_offsets[x] = offset;
		offset += read_unsigned_le_short(index_data, &read_offset);
	}
	
	/* Close the files.  */
	fclose(fp_index);
	fclose(fp_data);
}

struct item_data *load_definition(char buffer[], int *offset)
{
	/* Create the npc data struct to store the information in.  */
	struct item_data *n = calloc(1, sizeof(struct item_data));
	int x;

	while (1)
	{
		int type = read_unsigned_byte(buffer, offset);

		if (type == 0)
		{
			break;
		}
		else if (type == 1)
		{
			n->model_id = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 2)
		{
			read_string(buffer, offset, n->name, 200);
		}
		else if (type == 3)
		{
			read_string(buffer, offset, n->description, 200);
		}
		else if (type == 4)
		{
			n->model_zoom = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 5)
		{
			n->model_rotation_x = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 6)
		{
			n->model_rotation_y = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 7)
		{
			n->model_offset1 = read_unsigned_le_short(buffer, offset);

			if (n->model_offset1 > 32767)
			{
				n->model_offset1 -= 0x10000;
			}
		}
		else if (type == 8)
		{
			n->model_offset2 = read_unsigned_le_short(buffer, offset);

			if (n->model_offset2 > 32767)
			{
				n->model_offset2 -= 0x10000;
			}
		}
		else if (type == 10)
		{
			/* Unused by client.  */
			read_unsigned_le_short(buffer, offset);
		}
		else if (type == 11)
		{
			n->stackable = 1;
		}
		else if (type == 12)
		{
			n->value = read_int(buffer, offset);
		}
		else if (type == 16)
		{
			n->is_members = 1;
		}
		else if (type == 23 || type == 25)
		{
			read_unsigned_le_short(buffer, offset);
			read_value(buffer, offset);
		}
		else if (type == 24 || type == 26)
		{
			read_unsigned_le_short(buffer, offset);
		}
		else if (type >= 30 && type < 35)
		{
			if (n->ground_actions == NULL)
			{
				n->ground_actions = calloc(5, sizeof(char *));
				for (x = 0; x < 5; x++)
				{
					n->ground_actions[x] = calloc(100, sizeof(char));
				}
			}
			read_string(buffer, offset, n->ground_actions[type - 30], 100);
		}
		else if (type >= 35 && type < 40)
		{
			if (n->actions == NULL)
			{
				n->actions = calloc(5, sizeof(char *));
				for (x = 0; x < 5; x++)
				{
					n->actions[x] = calloc(100, sizeof(char));
				}
			}
			read_string(buffer, offset, n->actions[type - 35], 100);
		}
		else if (type == 40)
		{
			int color_count = read_unsigned_byte(buffer, offset);

			for (x = 0; x < color_count; x++)
			{
				read_unsigned_le_short(buffer, offset);
				read_unsigned_le_short(buffer, offset);
			}
		}
		else if (type == 78 || type == 79 || type == 90 || type == 91
				 || type == 92 || type == 93)
		{
			read_unsigned_le_short(buffer, offset);
		}
		else if (type == 95)
		{
			n->model_rotation_z = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 97)
		{
			n->note_id = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 98)
		{
			n->note_template_id = read_unsigned_le_short(buffer, offset);
		}
		else if (type >= 100 && type < 110)
		{
			if (n->stackable_ids == NULL)
			{
				n->stackable_ids = calloc(10, sizeof(int));
				n->stackable_amounts = calloc(10, sizeof(int));
			}
			n->stackable_ids[type - 100] = read_unsigned_le_short(buffer,
																  offset);
			n->stackable_amounts[type - 100] = read_unsigned_le_short(buffer,
																  offset);
		}
		else if (type == 110)
		{
			n->model_scale_x = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 111)
		{
			n->model_scale_y = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 112)
		{
			n->model_scale_z = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 113)
		{
			n->light_modifier = read_value(buffer, offset);
		}
		else if (type == 114)
		{
			n->shadow_modifier = (read_value(buffer, offset) * 5);
		}
		else if (type == 115)
		{
			n->team_id = read_value(buffer, offset);
		}
	}
	
	return n;
}

void write_data(void)
{
	FILE *fp = fopen(out_name, "w+");

	if (fp == NULL)
	{
		printf("ERROR!\n");
		return;
	}
	int x = 0, y = 0;

	/* Write header.  */
	fprintf(fp, "ID\tName\tDesc\tValue\tNote id\tAction 1\tAction 2\t Action 3\t"
			"Action 4\tAction 5");
	fprintf(fp, "\n");

	for (x = 0; x < ITEM_DEFS_SIZE; x++)
	{
		fprintf(fp, "%d\t", x);

		if (ITEM_DEFS[x]->name[0] == '\0')
		{
			if (ITEM_DEFS[x]->note_id != 0)
			{
				fprintf(fp, "%s\t", ITEM_DEFS[ITEM_DEFS[x]->note_id]->name);	
			}
			else
			{
				fprintf(fp, "null\t");
			}
		}
		else
		{
			fprintf(fp, "%s\t", ITEM_DEFS[x]->name);
		}

		if (ITEM_DEFS[x]->description[0] == '\0')
		{
			if (ITEM_DEFS[x]->note_id != 0)
			{
				fprintf(fp, note_desc);
				fprintf(fp, "\t");
			}
			else
			{
				fprintf(fp, "null\t");
			}
		}
		else
		{
			fprintf(fp, "%s\t", ITEM_DEFS[x]->description);
		}
		fprintf(fp, "%d\t", ITEM_DEFS[x]->value);
		fprintf(fp, "%d\t", ITEM_DEFS[x]->note_id);

		for (y = 0; y < 5; y++)
		{
			if (ITEM_DEFS[x]->actions == NULL)
			{
				fprintf(fp, "null\t");
			}
			else
			{
				if (ITEM_DEFS[x]->actions[y][0] == '\0')
				{
					fprintf(fp, "null\t");
				}
				else
				{
					fprintf(fp, "%s\t", ITEM_DEFS[x]->actions[y]);
				}
			}
		}
		fprintf(fp, "\n");
	}

	fclose(fp);
}

int main(void)
{
	int x = 0;
	printf("Loading data..\n");
	load_files();
	printf("Files loaded.\n");

	printf ("Loading item definitions.\n");
	for (x = 0; x < ITEM_DEFS_SIZE; x++)
	{
		ITEM_DEFS[x] = load_definition(item_data, &item_offsets[x]);
	}
	printf("Writing data to file...\n");
	write_data();
	printf("All done!\n");
	return 0;
}
