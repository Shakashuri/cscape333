echo "Decoding cache files..."
./decode
echo "Extracting object data..."
./extobjdata
echo "Extracting interfaces..."
./extintr
echo "Extracting midi music..."
./extmidis
echo "Extracting maps..."
./extmaps
echo "Extracting collision data..."
./extcol
echo "Finished running all extraction tools."
echo "Trying to gzip the map data files..."
gzip -f -9 map{0..3}
echo "All done!"
