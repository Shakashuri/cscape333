#ifndef _CONFIG_H_
#define _CONFIG_H_

struct npc_data
{
	char name[200];
	char description[200];
	char **actions;
	int combat_level;

	int stand_animation;
	int walk_animation;
	int turn_about_animation;
	int turn_right_animation;
	int turn_left_animation;

	int degrees_to_turn;

	int boundary_dimension;
	int not_clickable;

	int invisible_on_minimap;
	int visible;

	int brightness;
	int contrast;
	
	int scale_xy;
	int scale_z;
	int head_icon;
};


#endif
