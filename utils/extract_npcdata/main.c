#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

/* Filenames need to be changed for each version.  */
/* 333 version filenames.  */
char *index_file_name = {"1/1489126980.dat"};
char *data_file_name = {"1/1489108188.dat"};
char *out_name = {"npc_data.tsv"};

char *npc_data;
char *index_data;
int *npc_offsets;

struct npc_data **NPC_DEFS;
int NPC_DEFS_SIZE;

void load_files(void)
{
	FILE *fp_data = fopen(data_file_name, "r");
	FILE *fp_index = fopen(index_file_name, "r");
	int data_size = file_len(fp_data);
	int index_size = file_len(fp_index);
	int num_offsets_size = 0;
	int offset = 0;
	int read_offset = 0;
	int x = 0;

	printf("Data size: %d Index size: %d\n", data_size, index_size);

	/* Read all of the data file into the buffer.  */
	npc_data = calloc(data_size, sizeof(char));

	for (x = 0; x < data_size; x++)
	{
		npc_data[x] = fgetc(fp_data);
	}

	printf("Data read\n");

	/* Read the index file into the buffer, and read the offsets from it.  */
	index_data = calloc(index_size, sizeof(char));

	for (x = 0; x < index_size; x++)
	{
		index_data[x] = fgetc(fp_index);
	}
	
	/* Read how big the offset block is.  */
	NPC_DEFS_SIZE = num_offsets_size 
		= read_unsigned_le_short(index_data, &read_offset);

	NPC_DEFS = calloc(num_offsets_size, sizeof(struct npc_data *));
	npc_offsets = calloc(num_offsets_size, sizeof(int));

	printf("Num offsets: %d\n", num_offsets_size);

	offset = 2;
	for (x = 0; x < num_offsets_size; x++)
	{
		npc_offsets[x] = offset;
		offset += read_unsigned_le_short(index_data, &read_offset);
	}
	
	/* Close the files.  */
	fclose(fp_index);
	fclose(fp_data);
}

struct npc_data *load_definition(char buffer[], int *offset)
{
	/* Create the npc data struct to store the information in.  */
	struct npc_data *n = calloc(1, sizeof(struct npc_data));
	int x;

	while (1)
	{
		int type = read_unsigned_byte(buffer, offset);

		if (type == 0)
		{
			break;
		}
		else if (type == 1)
		{
			int model_count = read_unsigned_byte(buffer, offset);

			for (x = 0; x < model_count; x++)
			{
				read_unsigned_le_short(buffer, offset);
			}
		}
		else if (type == 2)
		{
			read_string(buffer, offset, n->name, 200);
		}
		else if (type == 3)
		{
			read_string(buffer, offset, n->description, 200);
		}
		else if (type == 12)
		{
			n->boundary_dimension = read_value(buffer, offset);
		}
		else if (type == 13)
		{
			n->stand_animation = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 14)
		{
			n->walk_animation = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 17)
		{
			n->walk_animation = read_unsigned_le_short(buffer, offset);
			n->turn_about_animation = read_unsigned_le_short(buffer, offset);
			n->turn_right_animation = read_unsigned_le_short(buffer, offset);
			n->turn_left_animation = read_unsigned_le_short(buffer, offset);
		}
		else if (type >= 30 && type < 40)
		{
			if (n->actions == NULL)
			{
				n->actions = calloc(5, sizeof(char *));
				for (x = 0; x < 5; x++)
				{
					n->actions[x] = calloc(100, sizeof(char));
				}
			}
			read_string(buffer, offset, n->actions[type - 30], 100);
		}
		else if (type == 40)
		{
			int color_count = read_unsigned_byte(buffer, offset);

			for (x = 0; x < color_count; x++)
			{
				read_unsigned_le_short(buffer, offset);
				read_unsigned_le_short(buffer, offset);
			}
		}
		else if (type == 60)
		{
			int add_model_count = read_unsigned_byte(buffer, offset);

			for (x = 0; x < add_model_count; x++)
			{
				read_unsigned_le_short(buffer, offset);
			}
		}
		else if (type == 90)
		{
			read_unsigned_le_short(buffer, offset);
		}
		else if (type == 91)
		{
			read_unsigned_le_short(buffer, offset);
		}
		else if (type == 92)
		{
			read_unsigned_le_short(buffer, offset);
		}
		else if (type == 93)
		{
			n->invisible_on_minimap = 1;
		}
		else if (type == 95)
		{
			n->combat_level = read_unsigned_le_short(buffer, offset);		
		}
		else if (type == 97)
		{
			n->scale_xy = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 98)
		{
			n->scale_z = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 99)
		{
			n->visible = 1;
		}
		else if (type == 100)
		{
			n->brightness = read_value(buffer, offset);
		}
		else if (type == 101)
		{
			n->contrast = (read_value(buffer, offset) * 5);
		}
		else if (type == 102)
		{
			n->head_icon = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 103)
		{
			n->degrees_to_turn = read_unsigned_le_short(buffer, offset);
		}
		else if (type == 106)
		{
			int var_bit = read_unsigned_le_short(buffer, offset);

			if (var_bit == 65535)
			{
				var_bit = -1;
			}
			int setting_id = read_unsigned_le_short(buffer, offset);
			if (setting_id == 65535)
			{
				setting_id = -1;
			}
			int child_count = read_unsigned_byte(buffer, offset);

			for (x = 0; x <= child_count; x++)
			{
				read_unsigned_le_short(buffer, offset);
			}

		}
		else if (type == 107)
		{
			n->not_clickable = 1;
		}
	}
	
	return n;
}

void write_data(void)
{
	FILE *fp = fopen(out_name, "w+");

	if (fp == NULL)
	{
		printf("ERROR!\n");
		return;
	}
	int x = 0, y = 0;

	/* Write header.  */
	fprintf(fp, "ID\tName\tDesc\tLevel\tAction 1\tAction 2\t Action 3\t"
			"Action 4\tAction 5");
	fprintf(fp, "\n");
	for (x = 0; x < NPC_DEFS_SIZE; x++)
	{
		fprintf(fp, "%d\t", x);

		if (NPC_DEFS[x]->name[0] == '\0')
		{
			fprintf(fp, "null\t");
		}
		else
		{
			fprintf(fp, "%s\t", NPC_DEFS[x]->name);
		}

		if (NPC_DEFS[x]->description[0] == '\0')
		{
			fprintf(fp, "null\t");
		}
		else
		{
			fprintf(fp, "%s\t", NPC_DEFS[x]->description);
		}
		fprintf(fp, "%d\t", NPC_DEFS[x]->combat_level);

		for (y = 0; y < 5; y++)
		{
			if (NPC_DEFS[x]->actions == NULL)
			{
				fprintf(fp, "null\t");
			}
			else
			{
				if (NPC_DEFS[x]->actions[y][0] == '\0')
				{
					fprintf(fp, "null\t");
				}
				else
				{
					fprintf(fp, "%s\t", NPC_DEFS[x]->actions[y]);
				}
			}
		}
		fprintf(fp, "%d\t", NPC_DEFS[x]->stand_animation);
		fprintf(fp, "\n");
	}

	fclose(fp);
}

int main(void)
{
	int x = 0;
	printf("Loading data..\n");
	load_files();
	printf("Files loaded.\n");

	printf ("Loading npc definitions.\n");
	for (x = 0; x < NPC_DEFS_SIZE; x++)
	{
		NPC_DEFS[x] = load_definition(npc_data, &npc_offsets[x]);
	}
	printf("Writing data to file...\n");
	write_data();
	printf("All done!\n");
	return 0;
}
