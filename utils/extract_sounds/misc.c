#include <stdio.h>


int file_len(FILE *fp)
{
	int len;

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);

	return len;
}
