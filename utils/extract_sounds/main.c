#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <errno.h>

#include "config.h"
#include "misc.h"
#include "stream.h"

/* Filenames need to be changed for each version.  */
/* 333 version filenames.  */
char *music_file_name = {"7/232787039.dat"};

char *music_file_buffer;
int music_buffer_size;

void load_files(void)
{
	FILE *music_fp = fopen(music_file_name, "r");
	char output[441000] = {0};
	int offset = 0;
	int x;

	/* Load file into buffer.  */
	music_buffer_size = file_len(music_fp);
	music_file_buffer = calloc(music_buffer_size, sizeof(char));

	for (x = 0; x < music_buffer_size; x++)
	{
		music_file_buffer[x] = fgetc(music_fp);
	}

	/* Check sign/signlink.java, and all files in audio folder.  */

	// init instrument. 

	do
	{
		int effect = read_unsigned_le_short(music_file_buffer, &offset);

		if (effect == 65535)
		{
			return;
		}

		/* effects.  */
	}

}
