#ifndef _STREAM_H_
#define _STREAM_H_

void read_bytes(char buffer[], int *position, char string[], int i, int j);
int read_short(char buffer[], int *position);
int read_unsigned_le_short(char *buffer, int *position);
int read_bits(int num_bits, char buffer[], int *position, int *bits_read,
			  int *char_unk); 

int read_int(char buffer[], int *position); 
char read_unsigned_char(char buffer[], int *position, int *bits_read,
						int *char_unk); 
int read_unsigned_byte(char buffer[], int *position);
int read_value(char buffer[], int *position);
int read_string(char buffer[], int *position,  char string[], int string_size);
#endif
