#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LOOT_TABLE_DROPS 35
#define MAX_LOOT_TABLE_SUB_TABLES 6

struct drop_table
{
	char name[100];

	int num_drops;
	int num_tables;

	/* Should be size 4, id min max chance.  */
	double **drops;
	double **tables;
	char table_names[MAX_LOOT_TABLE_SUB_TABLES][100];

	double total_weights;
};

struct drop_table **LOOT_TABLES;
int num_loot_tables = 0;

int file_lines(FILE *fp)
{
	int num_newlines = 0;
	int read_ch;

	while ((read_ch = fgetc(fp)) != EOF)
	{
		if (read_ch == '\n')
		{
			num_newlines++;
		}
	}
	rewind(fp);

	return num_newlines;
}

int load_loot_table(void)
{
	FILE *fp = NULL;
	char buffer[1000] = {0};
	struct drop_table zeroed_table = {0};

	double temp_drops[MAX_LOOT_TABLE_DROPS][4];
	double temp_tables[MAX_LOOT_TABLE_SUB_TABLES][4];
	int temp_count = 0;
	
	int file_length = 0;
	int x = 0, y = 0, z = 0;

	fp = fopen("drop_table.tsv", "r");

	if (fp == NULL)
	{
		printf("Failed to load drop table list!\n");
		return 1;
	}

	/* Remove one to account for the header comment.  */
	file_length = (file_lines(fp) - 1);

	LOOT_TABLES = calloc(file_length, sizeof(struct drop_table*));

	if (LOOT_TABLES == NULL)
	{
		printf("calloc failed, return\n");
		return 2;
	}

	/* Remove header from table.  */
	fgets(buffer, sizeof(buffer), fp);

	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct drop_table *drop_addr = malloc(sizeof(struct drop_table));
		*drop_addr = zeroed_table;

		temp_count = 0;

						/* Name.  */
		sscanf(buffer, "%[^\t]\t"
					   /* Drops.  */
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   "%lf\t%lf\t%lf\t%lf\t"
					   /* Tables.  */
					   "%[^\t]\t%lf\t%lf\t%lf\t"
					   "%[^\t]\t%lf\t%lf\t%lf\t"
					   "%[^\t]\t%lf\t%lf\t%lf\t"
					   "%[^\t]\t%lf\t%lf\t%lf\t"
					   "%[^\t]\t%lf\t%lf\t%lf\t"
					   "%[^\t]\t%lf\t%lf\t%lf",
					drop_addr->name,
					&temp_drops[0][0], &temp_drops[0][1], 
					&temp_drops[0][2], &temp_drops[0][3],
					&temp_drops[1][0], &temp_drops[1][1], 
					&temp_drops[1][2], &temp_drops[1][3],
					&temp_drops[2][0], &temp_drops[2][1], 
					&temp_drops[2][2], &temp_drops[2][3],
					&temp_drops[3][0], &temp_drops[3][1], 
					&temp_drops[3][2], &temp_drops[3][3],
					&temp_drops[4][0], &temp_drops[4][1], 
					&temp_drops[4][2], &temp_drops[4][3],
					&temp_drops[5][0], &temp_drops[5][1], 
					&temp_drops[5][2], &temp_drops[5][3],
					&temp_drops[6][0], &temp_drops[6][1], 
					&temp_drops[6][2], &temp_drops[6][3],
					&temp_drops[7][0], &temp_drops[7][1], 
					&temp_drops[7][2], &temp_drops[7][3],
					&temp_drops[8][0], &temp_drops[8][1], 
					&temp_drops[8][2], &temp_drops[8][3],
					&temp_drops[9][0], &temp_drops[9][1], 
					&temp_drops[9][2], &temp_drops[9][3],
					&temp_drops[10][0], &temp_drops[10][1], 
					&temp_drops[10][2], &temp_drops[10][3],
					&temp_drops[11][0], &temp_drops[11][1], 
					&temp_drops[11][2], &temp_drops[11][3],
					&temp_drops[12][0], &temp_drops[12][1], 
					&temp_drops[12][2], &temp_drops[12][3],
					&temp_drops[13][0], &temp_drops[13][1], 
					&temp_drops[13][2], &temp_drops[13][3],
					&temp_drops[14][0], &temp_drops[14][1], 
					&temp_drops[14][2], &temp_drops[14][3],
					&temp_drops[15][0], &temp_drops[15][1], 
					&temp_drops[15][2], &temp_drops[15][3],
					&temp_drops[16][0], &temp_drops[16][1], 
					&temp_drops[16][2], &temp_drops[16][3],
					&temp_drops[17][0], &temp_drops[17][1], 
					&temp_drops[17][2], &temp_drops[17][3],
					&temp_drops[18][0], &temp_drops[18][1], 
					&temp_drops[18][2], &temp_drops[18][3],
					&temp_drops[19][0], &temp_drops[19][1], 
					&temp_drops[19][2], &temp_drops[19][3],
					&temp_drops[20][0], &temp_drops[20][1], 
					&temp_drops[20][2], &temp_drops[20][3],
					&temp_drops[21][0], &temp_drops[21][1], 
					&temp_drops[21][2], &temp_drops[21][3],
					&temp_drops[22][0], &temp_drops[22][1], 
					&temp_drops[22][2], &temp_drops[22][3],
					&temp_drops[23][0], &temp_drops[23][1], 
					&temp_drops[23][2], &temp_drops[23][3],
					&temp_drops[24][0], &temp_drops[24][1], 
					&temp_drops[24][2], &temp_drops[24][3],
					&temp_drops[25][0], &temp_drops[25][1], 
					&temp_drops[25][2], &temp_drops[25][3],
					&temp_drops[26][0], &temp_drops[26][1], 
					&temp_drops[26][2], &temp_drops[26][3],
					&temp_drops[27][0], &temp_drops[27][1], 
					&temp_drops[27][2], &temp_drops[27][3],
					&temp_drops[28][0], &temp_drops[28][1], 
					&temp_drops[28][2], &temp_drops[28][3],
					&temp_drops[29][0], &temp_drops[29][1], 
					&temp_drops[29][2], &temp_drops[29][3],
					&temp_drops[30][0], &temp_drops[30][1], 
					&temp_drops[30][2], &temp_drops[30][3],
					&temp_drops[31][0], &temp_drops[31][1], 
					&temp_drops[31][2], &temp_drops[31][3],
					&temp_drops[32][0], &temp_drops[32][1], 
					&temp_drops[32][2], &temp_drops[32][3],
					&temp_drops[33][0], &temp_drops[33][1], 
					&temp_drops[33][2], &temp_drops[33][3],
					&temp_drops[34][0], &temp_drops[34][1], 
					&temp_drops[34][2], &temp_drops[34][3],
					drop_addr->table_names[0], &temp_tables[0][0],
					&temp_tables[0][1], &temp_tables[0][2],
					drop_addr->table_names[1], &temp_tables[1][0],
					&temp_tables[1][1], &temp_tables[1][2],
					drop_addr->table_names[2], &temp_tables[2][0],
					&temp_tables[2][1], &temp_tables[2][2],
					drop_addr->table_names[3], &temp_tables[3][0],
					&temp_tables[3][1], &temp_tables[3][2],
					drop_addr->table_names[4], &temp_tables[4][0],
					&temp_tables[4][1], &temp_tables[4][2],
					drop_addr->table_names[5], &temp_tables[5][0],
					&temp_tables[5][1], &temp_tables[5][2]
					);
		
		/* Find how many entries are not empty.  */
		for (x = 0; x < MAX_LOOT_TABLE_DROPS; x++)
		{
			if (temp_drops[x][3] != -1.0)
			{
				drop_addr->total_weights += temp_drops[x][3];
				temp_count++;
			}
		}
		/* Create the drops arrays.  */
		drop_addr->drops = calloc(temp_count, sizeof(double*));

		for (x = 0; x < temp_count; x++)
		{
			/* size 4 for id, min, max, and chance.  */
			drop_addr->drops[x] = calloc(4, sizeof(double));

			if (drop_addr->drops[x] == NULL)
			{
				printf("calloc error for drops!\n");
				return 3;
			}
		}

		drop_addr->num_drops = temp_count;

		/* Transfer the data to the drop struct.  */
		for (x = 0; x < drop_addr->num_drops; x++)
		{
			drop_addr->drops[x][0] = temp_drops[x][0];
			drop_addr->drops[x][1] = temp_drops[x][1];
			drop_addr->drops[x][2] = temp_drops[x][2];
			drop_addr->drops[x][3] = temp_drops[x][3];
		}

		/* Find how many table entries aren't empty.  */
		temp_count = 0;

		for (x = 0; x < MAX_LOOT_TABLE_SUB_TABLES; x++)
		{
			if (strcmp(drop_addr->table_names[x], "-1") != 0)
			{
				drop_addr->total_weights += temp_tables[x][2];
				temp_count++;
			}
		}

		drop_addr->num_tables = temp_count;

		drop_addr->tables = calloc(temp_count, sizeof(double*));

		for (x = 0; x < temp_count; x++)
		{
			/* size 4 for index, min, max, and chance.  */
			drop_addr->tables[x] = calloc(4, sizeof(double));

			if (drop_addr->tables[x] == NULL)
			{
				printf("calloc error for tables!\n");
				return 3;
			}
		}

		/* Transfer the data to the tables struct.  */
		for (x = 0; x < drop_addr->num_tables; x++)
		{
			/* We are adding an additional entry to the table array. The first
			 * entry will hold the index for the table in the LOOT_TABLES
			 * array. This offset here is intentional.  */
			drop_addr->tables[x][0] = -1.0;
			drop_addr->tables[x][1] = temp_tables[x][0];
			drop_addr->tables[x][2] = temp_tables[x][1];
			drop_addr->tables[x][3] = temp_tables[x][2];
		}

		LOOT_TABLES[num_loot_tables] = drop_addr;
		num_loot_tables++;
	}

	/* Close file pointer.  */
	fclose(fp);

	/* Now handle tables within tables. This loops through all loot tables that
	 * have tables within them, and finds the index of the loot table used.
	 * This makes sure that the string comparison for the names only has to be
	 * done once, and when dropping loot from a table, we can just hop to their
	 * index.
	 */
	for (x = 0; x < num_loot_tables; x++)
	{
		for (y = 0; y < LOOT_TABLES[x]->num_tables; y++)
		{
			for (z = 0; z < num_loot_tables; z++)
			{
				if (strcmp(LOOT_TABLES[x]->table_names[y], 
					LOOT_TABLES[z]->name) == 0)
				{
					LOOT_TABLES[x]->tables[y][0] = z;
				}
			}
		}
	}

	return 0;
}

void print_drop_table(int id)
{
	int x = 0;
	printf("*****************************************************\n");
	printf("Name: %s drops: %2d total weight: %4f\n", LOOT_TABLES[id]->name, 
		LOOT_TABLES[id]->num_drops, LOOT_TABLES[id]->total_weights);
	
	for (x = 0; x < LOOT_TABLES[id]->num_drops; x++)
	{
		printf("%2d: ID: %5d min %5d max %5d weight %5lf chance %2.2lf\n", x,
			(int) LOOT_TABLES[id]->drops[x][0],
			(int) LOOT_TABLES[id]->drops[x][1],
			(int) LOOT_TABLES[id]->drops[x][2],
			LOOT_TABLES[id]->drops[x][3],
			((LOOT_TABLES[id]->drops[x][3] / LOOT_TABLES[id]->total_weights) 
				* 100));
	}

	if (LOOT_TABLES[id]->num_tables != 0)
	{
		printf("Loot Tables:\n");

		for (x = 0; x < LOOT_TABLES[id]->num_tables; x++)
		{
			printf("%2d: %20s idx: %d min %5d max %5d weight %5lf chance %2.2lf\n", x,
				LOOT_TABLES[id]->table_names[x],
				(int) LOOT_TABLES[id]->tables[x][0],
				(int) LOOT_TABLES[id]->tables[x][1],
				(int) LOOT_TABLES[id]->tables[x][2],
				LOOT_TABLES[id]->tables[x][3],
				((LOOT_TABLES[id]->tables[x][3] 
				  / LOOT_TABLES[id]->total_weights) 
					* 100));
		}
	}
}

void free_loot_tables(void)
{
	int x = 0, y = 0;

	for (x = 0; x < num_loot_tables; x++)
	{
		/* Free all the drops.  */
		for (y = 0; y < LOOT_TABLES[x]->num_drops; y++)
		{
			free(LOOT_TABLES[x]->drops[y]);
		}

		free(LOOT_TABLES[x]->drops);

		/* Free all the tables.  */
		for (y = 0; y < LOOT_TABLES[x]->num_tables; y++)
		{
			free(LOOT_TABLES[x]->tables[y]);
		}

		free(LOOT_TABLES[x]->tables);

		free(LOOT_TABLES[x]);
	}

	free(LOOT_TABLES);
}

int main(int argc, char *argv[])
{
	int x = 0;

	load_loot_table();

	for (x = 0; x < num_loot_tables; x++)
	{
		print_drop_table(x);
	}

	free_loot_tables();

	return 0;
}
