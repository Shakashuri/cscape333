#include <stdio.h>
#include <stdlib.h>

struct npc
{
	int id;
	char name[30];
	char desc[100];

	/* 0 - Max hit,
	   1 - Atk speed,
	   2 - Atk anim,
	   3 - Atk gfx
	   4 - Damage type  */
	int melee_emote;
	int ranged_emote;

	int melee_damage_type;
	int ranged_damage_type;

	int found_melee;
	int found_ranged;
};


struct emote_data
{
	int id;
	char desc[100];
	int damage_type;
};

int num_npcs;
int num_emotes;

struct npc **NPCS;
struct emote_data **EMOTE_DATAS;

int file_num_lines(FILE *fp)
{
	int lines = 0;
	char testc = '\0';
	
	rewind(fp);

	while ((testc = fgetc(fp)) != EOF)
	{
		if (testc == '\n')
		{
			lines++;
		}
	}
	rewind(fp);
	return lines;
}

int load_npcs(void)
{
	/* Loads up all the npc definitions from a textfile database into 
	   structs for the server to use.  */
	int x = 0, count = 0;
	FILE *fp;
	char buffer[1000] = {0};

	/* Checks if npc def list can be loaded.  */
	printf("Loading npc definitions...\n");

	if ((fp = fopen("npcs.txt", "r")) != NULL)
	{
		printf("Opened npc file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}

	num_npcs = (file_num_lines(fp) - 1);

	printf("num npcs lines: %d\n", num_npcs);

	NPCS = calloc(num_npcs, sizeof(struct npc*));

	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	for (x = 0; x < 2633; x++)
	{
		struct npc *npc_addr = calloc(1, sizeof(struct npc));
		
		fgets(buffer, sizeof(buffer), fp);
		/* In order to load strings with spaces in them, such as item
		   descriptions, note the %[^\t]. This tells sscanf to read up to
		   a tab character, ignoring other whitespace.  */
		if (sscanf(buffer, "%d\t%[^\t]\t%[^\t]\t"
							"%*d\t%*d\t%*d\t"
							"%*d\t%*d\t"
							"%*d\t%*d\t"
							"%*d\t%*d\t"
							"%*d\t%*d\t"
							
							/* Melee attack info.  */
							"%*d\t%d\t%*d\t%*d\t%*d\t%*d\t"
							/* Ranged attack info.  */
							"%*d\t%d\t%*d\t%*d\t%*d\t%*d\t%*d\t",
					&npc_addr->id, npc_addr->name, npc_addr->desc,
					&npc_addr->melee_emote, 
					&npc_addr->ranged_emote) == 5)
		{
		 	count++;
			NPCS[x] = npc_addr;
		}
		else
		{
			printf("ERROR! ID: %d\n", x);
			free(npc_addr);
		}
	}
	printf("Loaded %d NPC definitions!\n", count);
	
	fclose(fp);
	return 0;
}

int load_npcs_emotes(void)
{
	int count = 0, x = 0;
	FILE *fp;
	char buffer[300] = {0};

	if ((fp = fopen("333-dmg-type-emotes.txt", "r")) != NULL)
	{
		printf("Opened objects file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}

	num_emotes = (file_num_lines(fp) - 1);

	printf("num lines: %d\n", num_emotes);

	EMOTE_DATAS = calloc(num_emotes, sizeof(struct emote_data*));

	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct emote_data *emote = calloc(1, sizeof(struct emote_data));

		if (sscanf(buffer, "%d\t%[^\t]\t%d",
					&emote->id, emote->desc, &emote->damage_type) != EOF)
		{
			EMOTE_DATAS[count] = emote;
			count++;
		}
		else
		{
			break;
		}
	}
}


int main(void)
{
	int x, y;
	
	load_npcs();
	load_npcs_emotes();
	printf("Finished loading objects\n");

	/* Go through and compare data if any npc has an attacking emote that
	   matches one in the list. If so, apply the damage type it should be.  */
	for (x = 0; x < num_npcs; x++)
	{
		for (y = 0; y < num_emotes; y++)
		{
			if (NPCS[x]->melee_emote == EMOTE_DATAS[y]->id)
			{
				NPCS[x]->melee_damage_type = EMOTE_DATAS[y]->damage_type;
				NPCS[x]->found_melee = 1;
			}
			if (NPCS[x]->ranged_emote == EMOTE_DATAS[y]->id)
			{
				NPCS[x]->ranged_damage_type = EMOTE_DATAS[y]->damage_type;
				NPCS[x]->found_ranged = 1;
			}
		}
		if (NPCS[x]->melee_emote != 0 && NPCS[x]->found_melee == 0)
		{
			printf("NPC %d melee emote not handled: %d\n", NPCS[x]->id, 
				   NPCS[x]->melee_emote);
		}
		if (NPCS[x]->ranged_emote != 0 && NPCS[x]->found_ranged == 0)
		{
			printf("NPC %d ranged emote not handled: %d\n", NPCS[x]->id, 
				   NPCS[x]->ranged_emote);
		}
	}

	for (x = 0; x < num_npcs; x++)
	{
		printf("%d\t%d\t%d\n", NPCS[x]->id, NPCS[x]->melee_damage_type, 
			   NPCS[x]->ranged_damage_type);
	}
}
