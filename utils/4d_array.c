/* 4d_array.c - Test to see how 4D arrays would be initialized.  */
#include <stdio.h>
#include <stdlib.h>

int ****world_data;

void malloc_region(int region_id)
{
	/* If a region will have data in it, creates the array. This is done on
	   a case by case basis to save memory usage.  */
	int x, z;

	for (z = 0; z < 4; z++)
	{
		/* Mallocs the y of the 4D array.  */
		if ((world_data[region_id][z] = malloc(sizeof(int*) * 64)) == NULL)
		{
			printf("ERROR! Y\n");
			return;
		}
		
		/* Mallocs the x of the 4D array.  */
		for (x = 0; x < 64; x++)
		{
			if ((world_data[region_id][z][x] = malloc(sizeof(int) * 64)) == NULL)
			{
				printf("ERROR! Z: %d X: %d\n", z, x);
				return;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	int x;
	
	/* Mallocs the number of regions needed for storage.  */
	if ((world_data = malloc(sizeof(int***) * 1000)) == NULL)
	{
		printf("ERROD!\n");
	}
	
	/* Mallocs all the height levels for each region.  */
	for (x = 0; x < 1000; x++)
	{
		if ((world_data[x] = malloc(sizeof(int**) * 4)) == NULL)
		{
			printf("ERROR!\n");
			exit(1);
		}
	}

	/* Creates region 10.  */
	malloc_region(10);

	/* Sets 0,0 in region 10 at height level 1 to 5.  */
	world_data[10][1][0][0] = 5;
	
	printf("WORLD: %d\n", world_data[10][1][0][0]);

	int y, z;

	for (z = 0; z < 4; z++)
	{
		for (y = 0; y < 64; y++)
		{
			for (x = 0; x < 64; x++)
			{
				printf("WORLD: %d\n", world_data[10][z][y][x]);
			}
		}
	}
	/* Finished smoothly, end program.  */
	return 0;
}

