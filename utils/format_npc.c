#include <stdio.h>
#include <stdlib.h>

struct npc
{
	int id;
	char name[30];
	char desc[100];
	int level;
	int size_x;
	int size_y;
	int attackable;
	int aggressive;
	int retreats;
	int retaliates;
	int undead;
	int poisonous;
	int prefer_ranged;
	int respawn_ticks;
	int health;
	
	/* 0 - Max hit,
	   1 - Atk speed,
	   2 - Atk anim,
	   3 - Atk gfx
	   4 - Damage type  */
	int melee_stats[5];
	int ranged_stats[5];
	
	/* Holds ids of spells that can be cast.  */
	int magic_spells[5];
	int num_spells;
	int magic_range;

	int ranged_projectile_id;
	int max_ranged;
	
	int def_anim;
	int stand_anim;
	int walk_anim;
	int turn_about_anim;
	int turn_cw_anim;
	int turn_ccw_anim;
	int death_anim;
	int death_timer;
	int atk_bonus;
	int def_melee;
	int def_range;
	int def_magic;
	
	int rare_chance;
	/* Can hold up to thirty different drops.
	   Format is, as follows:
	   drop[x][0] = item id
	   drop[x][1] = minimum amount
	   drop[x][2] = maximum amount
	   drop[x][3] = chance

	   Chance is set up as 1 out of chance.
	   A random int is rolled from 1 to the item's chance,
	   and if it is equal to the chance, it will drop.  */
	int drops[30][4];
};


struct npc_test
{
	int id;
	int size;
	int level;
	int stand_anim;
	int walk_anim;
	int turn_about_anim;
	int turn_cw_anim;
	int turn_ccw_anim;
};

struct npc *NPCS[3000];
struct npc_test *NPCS_TEST[3000];

int load_npcs(void)
{
	/* Loads up all the npc definitions from a textfile database into 
	   structs for the server to use.  */
	int x = 0, count = 0;
	FILE *fp;
	char buffer[1000] = {0};
	struct npc zeroed_npc = {0};

	/* Checks if npc def list can be loaded.  */
	printf("Loading npc definitions...\n");

	if ((fp = fopen("npcs.txt", "r")) != NULL)
	{
		printf("Opened npc file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}

	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	for (x = 0; x < 2633; x++)
	{
		struct npc *npc_addr = malloc(sizeof(struct npc));
		*npc_addr = zeroed_npc;
		
		fgets(buffer, sizeof(buffer), fp);
		/* In order to load strings with spaces in them, such as item
		   descriptions, note the %[^\t]. This tells sscanf to read up to
		   a tab character, ignoring other whitespace.  */
		if (sscanf(buffer, "%d\t%[^\t]\t%[^\t]\t"
							"%d\t%d\t%d\t"
							"%d\t"
							"%d\t%d\t"
							"%d\t%d\t"
							"%d\t%d\t"
							"%d\t%d\t"
							
							/* Melee attack info.  */
							"%d\t%d\t%d\t%d\t%d\t"
							/* Ranged attack info.  */
							"%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
							/* Number of spells.  */
							"%d\t"
							/* Magic range.  */
							"%d\t"
							/* Magic spell ids.  */
							"%d\t%d\t%d\t%d\t%d\t"
							/* Stand, walk, turn about, turn cw, turn ccw 
							   anims.  */
							"%d\t%d\t%d\t%d\t%d\t"
							/* Defence animation.  */
							"%d\t"
							/* Death animation, death timer.  */
							"%d\t%d\t"
							/* Stat bonuses.  */
							"%d\t"
							/* Npc defence stats.  */
							"%d\t%d\t%d",
					&npc_addr->id, npc_addr->name, npc_addr->desc,
					&npc_addr->level, &npc_addr->size_x, &npc_addr->size_y, 
					&npc_addr->attackable, &npc_addr->aggressive,
					&npc_addr->retaliates,
					&npc_addr->retreats, 
					&npc_addr->undead,
					&npc_addr->prefer_ranged,
					&npc_addr->poisonous,
					&npc_addr->respawn_ticks, &npc_addr->health,
					&npc_addr->melee_stats[0], &npc_addr->melee_stats[1],
					&npc_addr->melee_stats[2], &npc_addr->melee_stats[3],
					&npc_addr->melee_stats[4],
					&npc_addr->ranged_stats[0], &npc_addr->ranged_stats[1],
					&npc_addr->ranged_stats[2], &npc_addr->ranged_stats[3],
					&npc_addr->ranged_stats[4],
					&npc_addr->ranged_projectile_id,
					&npc_addr->max_ranged,
					&npc_addr->num_spells,
					&npc_addr->magic_range,
					&npc_addr->magic_spells[0], &npc_addr->magic_spells[1],
					&npc_addr->magic_spells[2], &npc_addr->magic_spells[3],
					&npc_addr->magic_spells[4],
					&npc_addr->stand_anim,
					&npc_addr->walk_anim,
					&npc_addr->turn_about_anim,
					&npc_addr->turn_cw_anim,
					&npc_addr->turn_ccw_anim,
					&npc_addr->def_anim,
					&npc_addr->death_anim, &npc_addr->death_timer, 
					&npc_addr->atk_bonus,
					&npc_addr->def_melee, &npc_addr->def_range,
					&npc_addr->def_magic) == 46)
		{
			count++;
		}
		else
		{
			printf("ERROR! ID: %d\n", x);
		}
		NPCS[x] = npc_addr;
	}
	printf("Loaded %d NPC definitions!\n", count);
	
	fclose(fp);
	return 0;
}

int load_npcs_test(void)
{
	int count = 0, x = 0;
	FILE *fp;
	char buffer[300] = {0};
	struct npc_test zeroed_npc = {0};

	if ((fp = fopen("npc_data.txt", "r")) != NULL)
	{
		printf("Opened objects file...\n");
	}
	else
	{
		printf("ERROR OPENING FILE!\n");
		return 1;
	}
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		struct npc_test *npc = malloc(sizeof(struct npc_test));
		*npc = zeroed_npc;

		if (sscanf(buffer, "%d Level: %d S: %d Stand: %d Walk: %d TurnA: %d"
						   " TurnR: %d TurnL: %d",
					&npc->id, &npc->level, &npc->size, &npc->stand_anim, &npc->walk_anim,
					&npc->turn_about_anim, &npc->turn_cw_anim, &npc->turn_ccw_anim) != EOF)
		{
			NPCS_TEST[count] = npc;
			count++;
		}
		else
		{
			break;
		}
	}
}


int main(void)
{
	int x;
	
	load_npcs();
	printf("test\n");
	load_npcs_test();
	printf("Finished loading objects\n");

	for (x = 0; x < 2633; x++)
	{
		if (NPCS_TEST[x] != NULL)
		{
			NPCS[NPCS_TEST[x]->id]->size_x = NPCS[NPCS_TEST[x]->id]->size_y = NPCS_TEST[x]->size;
			NPCS[NPCS_TEST[x]->id]->level = NPCS_TEST[x]->level;
			NPCS[NPCS_TEST[x]->id]->stand_anim = NPCS_TEST[x]->stand_anim;
			NPCS[NPCS_TEST[x]->id]->walk_anim = NPCS_TEST[x]->walk_anim;
			NPCS[NPCS_TEST[x]->id]->turn_about_anim = NPCS_TEST[x]->turn_about_anim;
			NPCS[NPCS_TEST[x]->id]->turn_cw_anim = NPCS_TEST[x]->turn_cw_anim;
			NPCS[NPCS_TEST[x]->id]->turn_ccw_anim = NPCS_TEST[x]->turn_ccw_anim;
			
		}
	}
	printf("ID\tLevel\tSize\tStand\tWalk\tTurn about\tcw\tccw\n");
	for (x = 0; x < 2633; x++)
	{
		printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t\n", NPCS[x]->id, NPCS[x]->level, 
				NPCS[x]->size_x, NPCS[x]->stand_anim, NPCS[x]->walk_anim, 
				NPCS[x]->turn_about_anim, NPCS[x]->turn_cw_anim, NPCS[x]->turn_ccw_anim); 
	}
}
