/**
 * @file collide.c 
 * @brief Utility program for checking collision masks for tiles.
 */
#include <stdio.h>

void print_bit(int test)
{
	/* Non-zero values mean that the direction in question is blocked off.  */
	printf("Test &: %9d North Wall         (0x2)\n", (test & 0x2));
	printf("Test &: %9d East Wall          (0x8)\n", (test & 0x8));
	printf("Test &: %9d South Wall         (0x20)\n", (test & 0x20));
	printf("Test &: %9d West Wall          (0x80)\n", (test & 0x80));
	printf("Test &: %9d NE Wall            (0x4)\n", (test & 0x4));
	printf("Test &: %9d NW Wall            (0x1)\n", (test & 0x1));
	printf("Test &: %9d SE Wall            (0x10)\n", (test & 0x10));
	printf("Test &: %9d SW Wall            (0x40)\n", (test & 0x40));
	printf("Test &: %9d North Blocked      (0x400)\n", (test & 0x400));
	printf("Test &: %9d East Blocked       (0x1000)\n", (test & 0x1000));
	printf("Test &: %9d South Blocked      (0x4000)\n", (test & 0x4000));
	printf("Test &: %9d West Blocked       (0x10000)\n", (test & 0x10000));
	printf("Test &: %9d NE Blocked         (0x800)\n", (test & 0x800));
	printf("Test &: %9d NW Blocked         (0x200)\n", (test & 0x200));
	printf("Test &: %9d SE Blocked         (0x2000)\n", (test & 0x2000));
	printf("Test &: %9d SW Blocked         (0x8000)\n", (test & 0x8000));
	printf("Test &: %9d Shot Blocked       (0x20000)\n", (test & 0x20000));
	printf("Test &: %9d Blocked            (0x200000)\n", (test & 0x200000));
	printf("Test &: %9d Occupied           (0x100)\n", (test & 0x100));
	printf("Test &: %9d Closed             (0xFFFFFF)\n", (test & 0xFFFFFF));
	printf("#############\n");
}

int main(void)
{
	printf("Bitwise test.\n");
	int test;
	char input[100];

	for(;;)
	{
		fgets(input, 100, stdin);
		input[99] = '\0';
		sscanf(input, "%d", &test);
		print_bit(test);
	}
}
