Utility Programs Readme
=======================

**These programs are for the extraction of data from client cache files.**

## Programs:

	decode - Preforms the initial decoding and extraction of cache files,
		required to run before any of the other tools can do their work.
	extract_objdata - Extracts all object definitions and data from the cache.
		Reads special information from the obj_info.tsv file and applies it to
		the object data when it is extracted.
	extract_maps - Extracts all of the region gzip'd files.
	extract_collision - Relies on decode, extract_maps, and extract_objdata
		being run first, formats map info into collision listing that the
		server can use.
	extract_items - Extract all item definitions.
	extract_interfaces - Extracts all interface definitions.
	extract_midis - Extracts all .mid song files.
	extract_npcdata - Extracts all npc definitions and data from the cache.
	monster_spawn_wiki_extractor - Converts wiki monster spawn data to a format
		that the server can use. Current list at:
		https://docs.google.com/spreadsheets/d/1xYGrQMtT44KhsvBZSJMLlagFOPJ_OduRLBoHVtzuC9U/edit#gid=733551313 

More specific information about each program may be located in the README file
in their respective directories.

To run these programs, they must be placed, along with any data files they may 
need, in a directory containing the client cache files. Files needed and the
checksums are listed below. Be aware that some of the files listed may not be
needed by the tools, or are not used yet. The tools have been tested on cache
data for versions 333 up to, and including, 377.

**Note that all these programs may have issues with caches later than 333
as testing is not done on those very often.**

## MD5 Checksums for 333 Cache data.

	008258534074bbb0f83c8f6746ab0fc6  main_file_cache.dat
	927cce129053c89574aa91fd8544aab2  uid.dat
	3e2aba67028ad54d018c2f666ab763a6  main_file_cache.idx0
	dfbe9ce289db3ec5ba1650d13bbb5fb1  main_file_cache.idx1
	a4ae0541d7d840d20f3a4725ea0bcd9f  main_file_cache.idx2
	6db18801a45f68d4b548bddae06bf6a1  main_file_cache.idx3
	864464b939265c80183a5a8a709f57c1  main_file_cache.idx4

## MD5 Checksums for 357 Cache data.

	c228c7eec737dc8987bedbcce5405cae  code.dat
	b6898b265226568c8901e1fe571276ba  main_file_cache.dat
	ba736b6987f983e7cc8ed19b765b39fa  shared_game_unpacker.dat
	b1263647569782ece69bd7c2a9bbcc07  uid.dat
	5e9ba6c224f3b6369c6f186822d25b67  worldmap.dat
	eacb5ebf90e3f45a287e3599804a6346  main_file_cache.idx0
	9ad3cbe1270fb76f43ec8424ade56e72  main_file_cache.idx1
	6f686ff44f1ea4e390d380af7529eec8  main_file_cache.idx2
	60e84992cd5892589886a857ec17c176  main_file_cache.idx3
	3549158fa26f941e5165d6184fe0c96d  main_file_cache.idx4

## MD5 Checksums for 377 Cache data.

	8f4ed10a7efd4262c95fe12f3e919048  main_file_cache.dat
	c4d349a05c8e18579c6cb4fefdb0846e  uid.dat
	0c0cd809ff7b4d2319a2cb932d89eb97  worldmap.dat
	5ae4ab066b0011049587f37d967fe1f0  main_file_cache.idx0
	e3bf46ced52529fd08c94b0fe8011460  main_file_cache.idx1
	f76d82358be4f845a1e371fc9d422000  main_file_cache.idx2
	b4326d066deab3230e833901be2aaf56  main_file_cache.idx3
	9764111682868772828511b903d8a5a3  main_file_cache.idx4
