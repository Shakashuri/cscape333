#ifndef _CONFIG_H_
#define _CONFIG_H_

/* FIXME: Can I replace this?  */
#define DECOMP_SIZE 10000000
#define ARCHIVE_NUM 8

struct file_cache
{
	char buffer[520];
	FILE *data_file;
	FILE *index_file;
	int store_id;
};

struct archive
{
	/* Base archive data, size of archive.  */
	char *archive_data;
	unsigned int archive_data_size;
	char *archive_compressed_data;
	char *archive_decompressed_data;
	int archive_data_offset;
	int archive_compressed_data_offset;
	int archive_decompressed_data_offset;
	int archive_compressed_data_size;
	int archive_comp_len;
	int archive_decomp_len;
	int archive_is_decompressed;

	/* Data for the individual files in the archive.  */

	/* 2D arrays: file_comp_data[filecount[x]][file_compressed_size[x]; 
				  file_decomp_data[filecount[x]][file_decompressed_size[x]; 
	*/
	char **file_comp_data;
	char **file_decomp_data;
	int filecount;
	int file_offset;
	/* 1D arrays, size from filecount.  */
	int *file_hashes;
	int *file_start_offset; 
	int *file_compressed_size;
	int *file_decompressed_size;
};

#endif
