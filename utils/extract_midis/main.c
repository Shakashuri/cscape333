/* Extract midi data from the client files.  */
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <zlib.h>

#include "config.h"
#include "misc.h"

FILE *main_cache;
FILE *cache_idx[5];

struct file_cache *caches[5];

char midi_folder[50] = {"midi_data"};

int main(void)
{
	FILE *midi_index;
	char *midi_index_stream;
	int midi_index_len = 0;
	int x, z;

	/* Open main cache.  */
	main_cache = fopen("main_file_cache.dat", "r+");

	/* Open secondary caches.  */
	cache_idx[0] = fopen("main_file_cache.idx0", "r+");
	cache_idx[1] = fopen("main_file_cache.idx1", "r+");
	cache_idx[2] = fopen("main_file_cache.idx2", "r+");
	cache_idx[3] = fopen("main_file_cache.idx3", "r+");
	cache_idx[4] = fopen("main_file_cache.idx4", "r+");
	
	if (main_cache == NULL)
	{
		printf("ERROR! MAIN CHACHE ERROR!\n");
		printf("Trying to open the main cache file returned a NULL.\n");
		printf("Make sure the cache files are in the same directory as the ");
		printf("program.\n");
		exit(1);
	}

	for (x = 0; x < 5; x++)
	{
		if (cache_idx[x] == NULL)
		{
			printf("ERROR! IDX CACHE %d ERROR!\n", x);
			printf("Trying to open the index cache file number %d "
				   "returned a NULL.\n", x);
			printf("Make sure the cache files are in the same directory"
				   " as the ");
			printf("program.\n");
			exit(1);
		}
	}

	/* Setup the file cache structs.  */
	for (x = 0; x < 5; x++)
	{
		caches[x] = return_file_cache(main_cache, cache_idx[x], (x + 1));
	}

	/* Open 333 midi index, version files.  */
	midi_index = fopen("4/-1691482954.dat", "r");

	if (midi_index == NULL)
	{
		printf("Midi index file not found. Make sure you decode all client\n"
			   "files with decode first.\n");
		return 1;
	}

	printf("Midi index file opened, reading data.\n");
	
	midi_index_len = file_len(midi_index);
	midi_index_stream = malloc(midi_index_len * sizeof(char));
	int characters_read = fread(midi_index_stream, 1, midi_index_len, midi_index);
	printf("Chars read: %d\n", characters_read);

	mkdir(midi_folder, 0755);

	/* Extract all the midi data files.  */
	for (z = 0; z < midi_index_len; z++)
	{
		char *midi_data = malloc(DECOMP_SIZE * sizeof(char));
		int midi_data_len = 0;
		int rv = 0;
		char output_name[100] = {0};
		char output_name_ext[100] = {0};
		FILE *midi_file;
		FILE *midi_file_uncomp;
		gzFile midi_file_gz;

		snprintf(output_name, 100, "%s/%d.gz", midi_folder, z);
		snprintf(output_name_ext, 100, "%s/%d.mid", midi_folder, z);

		midi_data_len = decode_archive(caches[3], z, midi_data);

		printf("midi_index[%d]: %d\n", z, midi_index_stream[z]);
		printf("midi_data len: %d\n", midi_data_len);
		
		if (midi_data_len >= 0)
		{
			midi_file = fopen(output_name, "w+");

			if (midi_file == NULL)
			{
				printf("Midi file %s failed to be created!\n", output_name);
				continue;
			}

			for (x = 0; x < midi_data_len; x++)
			{
				fputc(midi_data[x], midi_file);
			}

			fclose(midi_file);
			
			midi_file_gz = gzopen(output_name, "r");
			midi_file_uncomp = fopen(output_name_ext, "w+");

			if (midi_file_gz == NULL)
			{
				printf("Gzip'd file %s failed to be read!\n", output_name);
				continue;
			}
			if (midi_file_uncomp == NULL)
			{
				printf("Midi destination file %s failed to be created!\n", 
					   output_name);
				continue;
			}

			while (1)
			{
				rv = gzgetc(midi_file_gz);
				if (rv != -1)
				{
					fputc(rv, midi_file_uncomp);
				}
				else
				{
					break;
				}
			}
			
			gzclose(midi_file_gz);
			fclose(midi_file_uncomp);
			remove(output_name);
		}

		free(midi_data);
	}

	fclose(main_cache);
	fclose(midi_index);
	free(midi_index_stream);

	/* Close all cache files, free malloc'd structs relating to them.  */
	for (x = 0; x < 5; x++)
	{
		fclose(cache_idx[x]);
		free(caches[x]);
	}

	return 0;
}
