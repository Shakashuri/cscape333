/**
 * @file packet_decode.c
 * @brief Tries a bunch of possible formats for inputted packet data in order 
 * to determine what format it is supposed to use.
 */

#include <stdio.h>

int main (int argc, char *argv[])
{
	char input[100];
	int rt_value1, rt_value2;
	int sscanf_return;
	int work;

	printf("Enter packet data in the format given from an error. Only the\n"
		   "relevent info (everything after the unhandled packet, or only\n"
		   "parts of it for closer examination).\n");

	for (;;)
	{
		fgets(input, 100, stdin);
		sscanf_return = sscanf(input, "%d,%d", &rt_value1, &rt_value2);
		
		if (sscanf_return == 2)
		{
			printf("Unsigned word: %d\n", ((rt_value1 & 0xff) << 8) 
									 + (rt_value2 & 0xff));
			printf("Unsigned word a: %d\n", ((rt_value1 & 0xff) << 8) 
									 + ((rt_value2 - 128) & 0xff));

			work = 0;
			work = ((rt_value1 & 0xff) << 8) + ((rt_value2) & 0xff);
			if (work > 32767)
			{
				work -= 0x10000;
			}
			printf("Signed word: %d\n", work);
			
			work = 0;
			work = ((rt_value1 & 0xff) << 8) + ((rt_value2 - 128) & 0xff);
			if (work > 32767)
			{
				work -= 0x10000;
			}
			printf("Signed word a: %d\n", work);
			
			work = 0;
			work = ((rt_value2 & 0xff) << 8) + ((rt_value1) & 0xff);
			if (work > 32767)
			{
				work -= 0x10000;
			}
			printf("Signed word BE: %d\n", work);
			
			work = 0;
			work = ((rt_value2 & 0xff) << 8) + ((rt_value1 - 128) & 0xff);
			if (work > 32767)
			{
				work -= 0x10000;
			}
			printf("Signed word BE a: %d\n", work);
			printf("Big endian: %d\n", ((rt_value2 & 0xff) << 8) 
									 + (rt_value1 & 0xff));
			printf("Big endian a: %d\n", ((rt_value2 & 0xff) << 8) 
									 + ((rt_value1 - 128) & 0xff));
		}
		else if (sscanf_return == 1)
		{
			printf("Signed byte: %d\n", rt_value1);
			printf("Signed byte a: %d\n", (rt_value1 - 128));
			printf("Signed byte c: %d\n", (-1 * rt_value1));
			printf("Signed byte s: %d\n", (128 - rt_value1));
			printf("Unsigned byte: %d\n", (rt_value1 & 0xff));
			printf("Unsigned byte a: %d\n", (rt_value1 - 128) & 0xff);
			printf("Unsigned byte c: %d\n", (-1 * rt_value1) & 0xff);
			printf("Unsigned byte s: %d\n", (128 - rt_value1) & 0xff);
		}
		else
		{
			printf("Incorrect input. Try again.\n");
		}
		printf("***************\n");
		
	}
}
