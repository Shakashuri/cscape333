#include <stdio.h>
#include <stdlib.h>

#define LIST_SIZE 100

/* Define everything we're going to use here.  */

/* Map that will be used for pathfinding test.  */
char map[10][10];

/* Starting point.  */
int start_x = 5;
int start_y = 5;

/* Goal point.  */
int end_x = 7;
int end_y = 1;

/* Offsets of every square around one.  */
int dir_x[8] = {0, 0, 1, 1, -1, -1, 1,-1};
int dir_y[8] = {1,-1,-1, 0,  0, -1, 1, 1};

/* x, y - Position of node
   p_x, p_y - "parent's" position in map
   g - movement cost from start to this point.  
   h - estimated movement cost from here to the end.
   f - (g + h)  */
struct node
{
	int x;
	int y;
	int p_x;
	int p_y;
	int g;
	int h;
	int f;
};

/* Returns the list location of a node in a list if it matches the coord values
   given.  */
int is_in_list(struct node *node[], int size, int c_x, int c_y)
{
	int i;

	for (i = 0; i < size; i++)
	{
		if (node[i] != NULL)
		{
			if ((node[i]->x == c_x) && (node[i]->y == c_y))
			{
				return i;
			}
		}
	}
	return -1;
}

/* Returns the number of non NULL objects in a list.  */
int num_entries_in_list(struct node *node[], int size)
{
	int x;
	int found = 0;
	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
			found++;
	}
	return found;
}

/* Returns the list address of the first NULL object in a list.  */
int get_free_slot(struct node *node[], int size)
{
	int x;
	for (x = 0; x < size; x++)
	{
		if (node[x] == NULL)
			return x;
	}
	return -1;
}

/* Returns the list address of the first non NULL object in a list.  */
int get_first(struct node *node[], int size)
{
	int x;
	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
		{
			return x;
		}
	}
	return -1;
}

/* Returns the list address of the object with the lowest cost value 
   in a list.  */
int get_lowest_cost(struct node *node[], int size)
{
	int current_low = get_first(node, size);
	int x = 0;

	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
		{
			if (node[x]->f < node[current_low]->f)
				current_low = x;
		}
	}
	return current_low;
}

/* This is our heuristic that estimates the distance from a point 
   to the end.  */
int manhattan(int c_x, int e_x, int c_y, int e_y)
{
	return (abs(c_x - e_x) + abs(c_y - e_y));
}

void print_map(void)
{
	int x = 0, y = 0;
	for (y = 9; y > -1; y--)
	{
		for (x = 0; x < 10; x++)
		{
			/* Print the start position.  */
			if (x == start_x && y == start_y)
				printf("%c", '@');
			else if (x == end_x && y == end_y)
				printf("%c", 'X');
			else
				printf("%c", map[x][y]);
		}
		printf("\n");
	}
}

int main(void)
{
	int x, y, z;
	int lowest_open_node = 0;

	/* Define the lists to hold our data.  */
	struct node *open_list[LIST_SIZE];
	struct node *closed_list[LIST_SIZE];
	/* Create the starting node.  */
	struct node *start = malloc(sizeof(struct node));

	/* Zero out the lists before we use them.  */
	for (x = 0; x < LIST_SIZE; x++)
	{
		open_list[x] = closed_list[x] = NULL;
	}

	/* Clear the map.  */
	for (x = 0; x < 10; x++)
	{
		for (y = 0; y < 10; y++)
		{
			map[x][y] = '.';
		}
	}

	/* Set walls in the map.  */
	map[9][2] = '#';
	map[6][2] = '#';
	map[7][2] = '#';

	print_map();

	/* Setup the starting node.  */
	start->x = start_x;
	start->y = start_y;
	start->p_x = start->p_y = 0;
	start->g = 0;
	start->h = manhattan(start->x, end_x, start->y, end_y);
	start->f = (start->h + start->g);
	/* Assign start to open_list.  */
	open_list[0] = start;
	
	/* While there are still entries in the open list, continue.  */
	while (num_entries_in_list(open_list, LIST_SIZE) != 0)
	{
		/* Get the lowest cost node in the open_list, transfer it to closed,
		   then create its neighbors.  */
		lowest_open_node = get_lowest_cost(open_list, LIST_SIZE);

		z = get_free_slot(closed_list, LIST_SIZE);
		closed_list[z] = open_list[lowest_open_node];
		open_list[lowest_open_node] = NULL;

		/* If the open node is the endpoint, end the loop and print results.  */
		if (closed_list[z]->x == end_x 
			&& closed_list[z]->y == end_y)
		{
			int x, hold;
			/* Free everything in the open_list. We have the path and this
			   isn't needed anymore.  */
			for (x = 0; x < LIST_SIZE; x++)
			{
				if (open_list[x] != NULL)
				{
					free(open_list[x]);
					open_list[x] = NULL;
				}
			}
			
			printf("MATCH!\n");
			printf("X: %d Y: %d\n", end_x, end_y);
			printf("X: %d Y: %d\n", closed_list[z]->p_x,
									  closed_list[z]->p_y);	
			x = is_in_list(closed_list, LIST_SIZE, closed_list[z]->p_x, 
							closed_list[z]->p_y);
			free(closed_list[z]);
			closed_list[z] = NULL;
			/* Hold stores position data so that when things are not needed,
			   they can be freed.  */
			hold = x;
		
			while (x != -1)
			{
				x = is_in_list(closed_list, LIST_SIZE, closed_list[hold]->p_x, 
								closed_list[hold]->p_y);
				free(closed_list[hold]);
				closed_list[hold] = NULL;
				
				if (x != -1)
				{
					printf("X: %d Y: %d\n", closed_list[x]->x, closed_list[x]->y);	
					hold = x;
				}
				else
				{
					/* Path is printed out, free everything in the 
					   closed list.  */
					for (z = 0; z < LIST_SIZE; z++)
					{
						if (closed_list[z] != NULL)
						{
							free(closed_list[z]);
							closed_list[z] = NULL;
						}
					}
				}
			}	
			break;
		}
		
		/* Go through each adjacent tile to the one picked.  */
		for (x = 0; x < 8; x++)
		{
			int return_v;
			/* Create struct for the neighbor node.  */
			struct node *next = malloc(sizeof(struct node));
			/* Connect the node to its parent.  */
			next->p_x = closed_list[z]->x;
			next->p_y = closed_list[z]->y;
			next->x = (next->p_x + dir_x[x]);
			next->y = (next->p_y + dir_y[x]);
			/* The node's cost is one plus its parent's cost.  */
			next->g = (closed_list[z]->g + 1);
			next->h = manhattan(next->x, end_x, next->y, end_y);
			next->f = (next->g + next->h);
			
			/* If node is in a out of bounds location, or blocked, free it and
			   continue to the next neighbor.  */
			if (map[next->x][next->y] == '#' || (next->x < 0 || next->x > 9)
				|| (next->y < 0 || next->y > 9))
			{
				free(next);
				continue;
			}
			
			/* If next already exists in the open list...  */
			return_v = is_in_list(open_list, LIST_SIZE, next->x, next->y);
			if (return_v != -1)
			{
				/* If the cost of this one is lower, overwite the old one.
				   Otherwise, free it and continue.  */
				if (open_list[return_v]->f > next->f)
				{
					free(open_list[return_v]);
					open_list[return_v] = next;
					continue;
				}
				else
				{
					free(next);
					continue;
				}
			}
			/* If next is in the closed list, free it and continue.  */
			return_v = is_in_list(closed_list, LIST_SIZE, next->x, next->y);
			if (return_v != -1)
			{
				free(next);
				continue;
			}
			/* If we got here, that means that this node is a newly discovered
			   one, so add it to the open_list.  */
			open_list[get_free_slot(open_list, LIST_SIZE)] = next;
		}
	}
	return 0;
}
