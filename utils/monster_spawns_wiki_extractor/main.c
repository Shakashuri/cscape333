#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SPAWN_SIZE_DEFAULT 50
#define BUFFER_SIZE 4000

struct monster
{
	char name[200];
	char desc[400];
	int world_z;
	int levels;

	int spawn_size;
	int num_spawns;

	int *spawn_x;
	int *spawn_y;

	int id;

	int def_index;

	int walks;
	char dir;
};

struct monster *NPCS;
struct monster *SPAWNS;
struct monster *ORIG_SPAWNS;

/* Defaults.  */
int NPCS_NUM = 3000;
int SPAWNS_NUM = 3000;
int ORIG_SPAWNS_NUM = 20000;

int max_monster_id = 0;

/** 
 * Converts a char to its lowercase version.
 * @param letter Char to change case of.
 * @return Returns the lowercase version of a letter.
 */
char to_lower(char letter)
{
	if (letter < 91 && letter > 64)
	{
		letter += 32;
	}
	return letter;
}

/**
 * Takes a string and converts all characters in it to their lowercase version.
 * @param *string Pointer to the string.
 * @return Returns the string pointer.
 */
char *string_to_lower(char *string)
{
	int x;

	for (x = 0; string[x] != '\0'; x++)
	{
		string[x] = to_lower(string[x]);
	}
	return string;
}

/**
 * Returns the number of lines in a file.
 * @param *fp File pointer.
 * @return Number of lines in the file.
 */
int file_lines(FILE *fp)
{
	int num_newlines = 0;
	int read_ch;

	while ((read_ch = fgetc(fp)) != EOF)
	{
		if (read_ch == '\n')
		{
			num_newlines++;
		}
	}
	rewind(fp);

	return num_newlines;
}

void fprint_monster_spawn_formatted(int index, FILE *fp)
{
	int x;

	if (SPAWNS[index].spawn_size == 0 || SPAWNS[index].id == -1)
	{
		return; }

	for (x = 0; x < SPAWNS[index].num_spawns; x++)
	{
		fprintf(fp, "%d\t", SPAWNS[index].id);
		fprintf(fp, "%d\t%d\t", SPAWNS[index].spawn_x[x], 
				SPAWNS[index].spawn_y[x]);
		fprintf(fp, "%d\t", SPAWNS[index].world_z);
		/* Walking, facing direction.  */
		fprintf(fp, "%d\t", 1);
		fprintf(fp, "%c\n", 'N');
	}
}

void fprint_orig_monster_spawn_formatted(int index, FILE *fp)
{
	int x;

	/* Only print old spawns that aren't monsters that can be attacked.  */
	if (NPCS[ORIG_SPAWNS[index].def_index].levels != 0)
	{
		return;	
	}

	fprintf(fp, "%d\t", ORIG_SPAWNS[index].id);
	fprintf(fp, "%d\t%d\t", ORIG_SPAWNS[index].spawn_x[0], 
			ORIG_SPAWNS[index].spawn_y[0]);
	fprintf(fp, "%d\t", ORIG_SPAWNS[index].world_z);
	/* Walking, facing direction.  */
	fprintf(fp, "%d\t", ORIG_SPAWNS[index].walks);
	fprintf(fp, "%c\n", ORIG_SPAWNS[index].dir);
}

int main(void)
{
	FILE *wiki_fp = fopen("osrs monster spawns.tsv", "r");
	FILE *npc_fp = fopen("npcs.tsv", "r");
	FILE *orig_fp = fopen("npc_spawns.tsv", "r");

	int x = 0, y = 0;
	int monster_index = 0;
	int count = 0;

	/* Make sure we loaded the needed files alright.  */
	if (wiki_fp == NULL)
	{
		printf("failed to load \"osrs monster spawns.tsv\"!\n");
		return 1;
	}

	if (npc_fp == NULL)
	{
		printf("failed to load \"npcs.tsv\"!\n");
		return 1;
	}

	if (orig_fp == NULL)
	{
		printf("failed to load \"npc_spawns.tsv\"!\n");
		return 1;
	}

	/* Get how many lines are in each file.  */
	NPCS_NUM = file_lines(npc_fp);
	SPAWNS_NUM = file_lines(wiki_fp);
	ORIG_SPAWNS_NUM = file_lines(orig_fp);

	/* Create spawns array.  */
	SPAWNS = calloc(SPAWNS_NUM, sizeof(struct monster));
	NPCS = calloc(NPCS_NUM, sizeof(struct monster));
	ORIG_SPAWNS = calloc(ORIG_SPAWNS_NUM, sizeof(struct monster));

	for (x = 0; x < NPCS_NUM; x++)
	{
		NPCS[x].id = -1;
		NPCS[x].def_index =  -1;
		NPCS[x].walks = 1;
		NPCS[x].dir = 'N';
	}

	for (x = 0; x < SPAWNS_NUM; x++)
	{
		SPAWNS[x].id = -1;
		SPAWNS[x].def_index =  -1;
		SPAWNS[x].walks = 1;
		SPAWNS[x].dir = 'N';
	}

	for (x = 0; x < ORIG_SPAWNS_NUM; x++)
	{
		ORIG_SPAWNS[x].id = -1;
		ORIG_SPAWNS[x].def_index =  -1;
		ORIG_SPAWNS[x].walks = 1;
		ORIG_SPAWNS[x].dir = 'N';
	}

	char buffer[BUFFER_SIZE] = {0};

	/* get the comment line.  */
	fgets(buffer, BUFFER_SIZE, orig_fp);

	/* load the NPC defs file first.  */
	while (fgets(buffer, BUFFER_SIZE, orig_fp) != NULL)
	{
		ORIG_SPAWNS[count].spawn_x 
			= calloc(1, sizeof(int*));
		ORIG_SPAWNS[count].spawn_y 
			= calloc(1, sizeof(int*));
		
		ORIG_SPAWNS[count].spawn_size = 1;

		sscanf(buffer, "%d\t%d\t%d\t%d\t%d\t%c\n", 
			&ORIG_SPAWNS[count].id,
			&ORIG_SPAWNS[count].spawn_x[0],
			&ORIG_SPAWNS[count].spawn_y[0],
			&ORIG_SPAWNS[count].world_z,
			&ORIG_SPAWNS[count].walks,
			&ORIG_SPAWNS[count].dir
			);

		count++;
	}

	ORIG_SPAWNS_NUM = count;
	count = 0;

	fclose(orig_fp);

	/* get the comment line.  */
	fgets(buffer, BUFFER_SIZE, npc_fp);

	/* load the NPC defs file first.  */
	while (fgets(buffer, BUFFER_SIZE, npc_fp) != NULL)
	{
		sscanf(buffer, "%d\t%[^\t]\t%[^\t]%d\t", 
			&NPCS[count].id,
			NPCS[count].name,
			NPCS[count].desc,
			&NPCS[count].levels);

		string_to_lower(NPCS[count].name);

		/* Make sure we don't print out any monsters that have an ID greater
		 * than what is supported by this revision.  */
		if (NPCS[count].id > max_monster_id)
		{
			max_monster_id = NPCS[count].id;
		}
		count++;
	}

	NPCS_NUM = count;
	fclose(npc_fp);


	/* Clear the header line.  */
	fgets(buffer, BUFFER_SIZE, wiki_fp);

	/* Load the spawn list dump file now.  */
	while (fgets(buffer, BUFFER_SIZE, wiki_fp) != NULL)
	{
		count = 0;

		if (buffer[0] == '\0')
		{
			break;
		}

		/* Read the name and convert it to all lowercase to help with 
		 * parsing.  */
		sscanf(buffer, "%[^\t]\t", SPAWNS[monster_index].name);
		string_to_lower(SPAWNS[monster_index].name);

		/* Start extracting monster spawn info.  */
		for (x = 0; buffer[x] != '\0';  x++)
		{
			/* Add in spawn location data.  */
			if (buffer[x] == 'x' && buffer[x + 1] == ':')
			{
				SPAWNS[monster_index].num_spawns = (count + 1);

				/* Calloc the spawn locations array if it hasn't been done
				 * yet.  */
				if (SPAWNS[monster_index].spawn_x == NULL)
				{
					SPAWNS[monster_index].spawn_x 
						= calloc(SPAWN_SIZE_DEFAULT, sizeof(int*));
					SPAWNS[monster_index].spawn_y 
						= calloc(SPAWN_SIZE_DEFAULT, sizeof(int*));
					
					SPAWNS[monster_index].spawn_size = SPAWN_SIZE_DEFAULT;
				}
				
				/* Expand the spawn locations array if needed.  */
				if (count >= SPAWNS[monster_index].spawn_size)
				{
					SPAWNS[monster_index].spawn_x = 
						realloc(SPAWNS[monster_index].spawn_x, 
							(SPAWNS[monster_index].spawn_size * 2 
								* sizeof(int*)));

					SPAWNS[monster_index].spawn_y = 
						realloc(SPAWNS[monster_index].spawn_y, 
							(SPAWNS[monster_index].spawn_size * 2 
								* sizeof(int*)));
				}

				/* Put the spawn coordinates into the next available slot.  */
				sscanf(buffer + x, "x:%d,y:%d", 
					&SPAWNS[monster_index].spawn_x[count], 
					&SPAWNS[monster_index].spawn_y[count]);

				count++;
			}
			else if (strncmp(buffer + x, "levels", 6) == 0)
			{
				sscanf(buffer + x, "levels = %d", 
					&SPAWNS[monster_index].levels);
			}
			else if (strncmp(buffer + x, "plane", 5) == 0)
			{
				sscanf(buffer + x, "plane = %d", 
					&SPAWNS[monster_index].world_z);
			}
		}

		monster_index++;
		printf("index; %d\n", monster_index);
	}

	fclose(wiki_fp);

	/* Go through all the spawns, see if there is one that matches up with a
	 * monster definition.  */
	for (x = 0; x < SPAWNS_NUM; x++)
	{
		for (y = 0; y < NPCS_NUM; y++)
		{
			if ((strcmp(SPAWNS[x].name, NPCS[y].name) == 0)
				&& (SPAWNS[x].levels == NPCS[y].levels))
			{
				SPAWNS[x].id = NPCS[y].id;
				SPAWNS[x].def_index = y;
			}
		}

		if (SPAWNS[x].id == -1)
		{
			printf("No match found for: %s, level %d\n", SPAWNS[x].name, 
				SPAWNS[x].levels);
		}
	}

	for (x = 0; x < ORIG_SPAWNS_NUM; x++)
	{
		for (y = 0; y < NPCS_NUM; y++)
		{
			if (ORIG_SPAWNS[x].id == NPCS[y].id)
			{
				ORIG_SPAWNS[x].def_index = y;
			}
		}
	}

	FILE *output_fp = fopen("new_spawns.tsv", "w");

	printf("Writing output to new_spawns.tsv...\n");

	/* Write the comment line at the top.  */
	fprintf(output_fp, "ID\tX\tY\tZ\twalk\tFacing\n");

	for (x = 0; x < SPAWNS_NUM; x++)
	{
		fprint_monster_spawn_formatted(x, output_fp);
	}
	
	for (x = 0; x < ORIG_SPAWNS_NUM; x++)
	{
		fprint_orig_monster_spawn_formatted(x, output_fp);
	}

	printf("Finished!\n");

	fclose(output_fp);

	return 0;
}
