
int get_3_bytes(char buffer[], int *position) 
{
	*position += 3;
	return ((buffer[*position - 3] & 0xff) << 16)
			+ ((buffer[*position - 2] & 0xff) << 8)
			+ (buffer[*position - 1] & 0xff);
}

int get_unsigned_le_short(char buffer[], int *position) 
{
	*position += 2;
	return ((buffer[*position - 2] & 0xff) << 8)
			+ (buffer[*position - 1] & 0xff);
}

int get_int(char buffer[], int *position) 
{
	*position += 4;
	return ((buffer[*position - 4] & 0xff) << 24)
			+ ((buffer[*position - 3] & 0xff) << 16)
			+ ((buffer[*position - 2] & 0xff) << 8)
			+ (buffer[*position - 1] & 0xff);
}

int read_bits(int num_bits, char buffer[], int *position, int *bits_read,
			  int *char_unk) 
{
	int j;

	do 
	{
		if (*bits_read >= num_bits) 
		{
			int k;
			k = (*char_unk) >> ((*bits_read) - num_bits) 
				& ((1 << num_bits) - 1);
			*bits_read -= num_bits;
			j = k;
			break;
		}
		(*char_unk) = ((*char_unk) << 8 | (buffer[*position] & 0xff));
		*bits_read += 8;
		(*position)++;
	} 
	while (1);
	
	return j;
}

char read_unsigned_char(char buffer[], int *position, int *bits_read,
						int *char_unk) 
{
	return (char) read_bits(8, buffer, position, bits_read, char_unk);
}
