#ifndef _MISC_C_
#define _MISC_C_

int file_len(FILE *fp);
void bzip2_error(int error_code);
struct file_cache *return_file_cache(FILE *main_cache, FILE *index_file, int id);
char *decode_archive(struct file_cache *fc, int index, char *decoded_data);

#endif
