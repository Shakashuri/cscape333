#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>


#include "config.h"

FILE *main_cache;
FILE *cache_idx[5];

struct file_cache *caches[5];
struct archive *archives[10];

int main(int argc, char *argv[])
{
	int x, y;

	/* Open main cache.  */
	main_cache = fopen("main_file_cache.dat", "r+");

	/* Open secondary caches.  */
	cache_idx[0] = fopen("main_file_cache.idx0", "r+");
	cache_idx[1] = fopen("main_file_cache.idx1", "r+");
	cache_idx[2] = fopen("main_file_cache.idx2", "r+");
	cache_idx[3] = fopen("main_file_cache.idx3", "r+");
	cache_idx[4] = fopen("main_file_cache.idx4", "r+");
	
	if (main_cache == NULL)
	{
		printf("ERROR! MAIN CHACHE ERROR!\n");
		printf("Trying to open the main cache file returned a NULL.\n");
		printf("Make sure the cache files are in the same directory as the ");
		printf("program.\n");
		exit(1);
	}

	for (x = 0; x < 5; x++)
	{
		if (cache_idx[x] == NULL)
		{
			printf("ERROR! IDX CACHE %d ERROR!\n", x);
			printf("Trying to open the index cache file number %d "
				   "returned a NULL.\n", x);
			printf("Make sure the cache files are in the same directory"
				   " as the ");
			printf("program.\n");
			exit(1);
		}
	}

	/* Setup the file cache structs.  */
	for (x = 0; x < 5; x++)
	{
		caches[x] = return_file_cache(main_cache, cache_idx[x], (x + 1));
	}

	/* Setup the archive structs.  */
	for (x = 0; x < ARCHIVE_NUM; x++)
	{
		/* Empty struct to zero out the structs we will malloc below.  */
		struct archive null_a = {0};
		archives[x] = malloc(sizeof(struct archive));
		*archives[x] = null_a;
		archives[x]->archive_data = malloc(DECOMP_SIZE * sizeof(char));
		archives[x]->archive_data_size 
			= (unsigned int) (DECOMP_SIZE * sizeof(char));
		/* Fetches archive data and places it in a buffer.  */
		decode_archive(caches[0], (x + 1), archives[x]->archive_data);

		/* Get archive compressed and decompressed sizes.  */
		archives[x]->archive_decomp_len 
			= get_3_bytes(archives[x]->archive_data, 
						  &archives[x]->archive_data_offset);
		archives[x]->archive_comp_len 
			= get_3_bytes(archives[x]->archive_data, 
						  &archives[x]->archive_data_offset);

		/* Decompress the archive file if needed.  */
		if (archives[x]->archive_comp_len == archives[x]->archive_decomp_len)
		{
			printf("Archive is not compressed, but individual files are.\n");
			archives[x]->archive_is_decompressed = 0;
		}
		else
		{
			int error = 0;
			int z;
			int y = 0;
			unsigned int decomp_size 
				= (unsigned int) archives[x]->archive_decomp_len;
			/* Compensate for adding the magic numbers to the front.  */
			archives[x]->archive_compressed_data 
				= malloc((archives[x]->archive_comp_len + 4) * sizeof(char));
			archives[x]->archive_decompressed_data 
				= malloc((archives[x]->archive_decomp_len) * sizeof(char));

			printf("Archive is compressed, decompressing...\n");

			/* Add the Bzip2 magic numbers to the front, they are not there
			   originally.  */
			strcpy(archives[x]->archive_compressed_data, "BZh1");

			z = 4;
			for (y = archives[x]->archive_data_offset; 
				 y < (archives[x]->archive_comp_len 
				 		+ archives[x]->archive_data_offset); y++)
			{
				archives[x]->archive_compressed_data[z] 
					= archives[x]->archive_data[y];
				z++;
			}

			printf("\n");

			error = BZ2_bzBuffToBuffDecompress(
					archives[x]->archive_decompressed_data, 
					&decomp_size,
					archives[x]->archive_compressed_data, 
					archives[x]->archive_comp_len + 4, 0, 1);
			bzip2_error(error);
			archives[x]->archive_is_decompressed = 1;
		}
	}


	/* Setup each archive with the needed data structs.  */
	for (x = 0; x < ARCHIVE_NUM; x++)
	{
		/* Get number of files in the archive, and their offsets.  */
		int filecount;

		if (archives[x]->archive_is_decompressed == 0)
		{
			filecount = get_unsigned_le_short(archives[x]->archive_data, 
								&archives[x]->archive_data_offset);
			archives[x]->file_offset 
				= archives[x]->archive_data_offset + filecount * 10;
		}
		else
		{
			filecount 
				= get_unsigned_le_short(archives[x]->archive_decompressed_data, 
								&archives[x]->archive_decompressed_data_offset);
			archives[x]->file_offset 
				= archives[x]->archive_decompressed_data_offset 
					+ filecount * 10;
		}

		printf("Filecount: %d\n", filecount);
		archives[x]->filecount = filecount;
		archives[x]->file_hashes = malloc(filecount * sizeof(int));
		archives[x]->file_start_offset = malloc(filecount * sizeof(int));
		archives[x]->file_comp_data = (malloc(filecount * sizeof(int)));
		archives[x]->file_decomp_data = (malloc(filecount * sizeof(int)));
		archives[x]->file_compressed_size = malloc(filecount * sizeof(int));
		archives[x]->file_decompressed_size = malloc(filecount * sizeof(int));

		printf("Archive %d has %d files.\n", x, archives[x]->filecount);
		printf("Offset: %d\n", archives[x]->file_offset);
	}

	/* Decompress each file in each archive.  */
	for (x = 0; x < ARCHIVE_NUM; x++)
	{
		for (y = 0; y < archives[x]->filecount; y++)
		{
			int i;
			int j;
			int z;
			int error;
			char output_dir[50] = {0};
			char dir_and_filename[100] = {0};

			snprintf(output_dir, 50, "%d/", x);
			mkdir(output_dir, 0755);

			if (archives[x]->archive_is_decompressed == 0)
			{
				archives[x]->file_hashes[y] 
					= get_int(archives[x]->archive_data, 
							  &archives[x]->archive_data_offset);

				archives[x]->file_decompressed_size[y] 
					= get_3_bytes(archives[x]->archive_data, 
								  &archives[x]->archive_data_offset);

				archives[x]->file_compressed_size[y] 
					= get_3_bytes(archives[x]->archive_data, 
								  &archives[x]->archive_data_offset);
			}
			else
			{
				archives[x]->file_hashes[y] 
					= get_int(archives[x]->archive_decompressed_data, 
							  &archives[x]->archive_decompressed_data_offset);

				archives[x]->file_decompressed_size[y] 
					= get_3_bytes(archives[x]->archive_decompressed_data, 
								  &archives[x]->archive_decompressed_data_offset);

				archives[x]->file_compressed_size[y] 
					= get_3_bytes(archives[x]->archive_decompressed_data, 
								  &archives[x]->archive_decompressed_data_offset);
			}

			snprintf(dir_and_filename, 100, "%d/%d.dat", x, archives[x]->file_hashes[y]);
			archives[x]->file_start_offset[y] =  archives[x]->file_offset;
			archives[x]->file_offset += archives[x]->file_compressed_size[y];

			if (archives[x]->archive_is_decompressed == 0)
			{
				unsigned int unsigned_decomp_size;
				archives[x]->file_decomp_data[y] 
					= (malloc(archives[x]->file_decompressed_size[y] * sizeof(int)));

				archives[x]->file_comp_data[y] 
					= (malloc((archives[x]->file_compressed_size[y] + 4) * sizeof(int)));
				/* Add the Bzip2 magic numbers to the front, they are not there
				   originally.  */
				strcpy(archives[x]->file_comp_data[y], "BZh1");

				z = 4;
				for (i = archives[x]->file_start_offset[y]; 
					 i < archives[x]->file_offset; i++)
				{
					archives[x]->file_comp_data[y][z] 
						= archives[x]->archive_data[i];
					z++;
				}

				unsigned_decomp_size 
					= (unsigned int) archives[x]->file_decompressed_size[y];
				
				error = BZ2_bzBuffToBuffDecompress(
						archives[x]->file_decomp_data[y], 
						&unsigned_decomp_size,
						archives[x]->file_comp_data[y], 
						archives[x]->file_compressed_size[y] + 4,
						0, 1);
				bzip2_error(error);
			}
			else
			{
				/* Archive is decompressed, just straight output.  */
				FILE *fp = fopen(dir_and_filename, "w+");

				if (fp == NULL)
				{
					printf("NULL FILE OPENED!\n");
					exit(1);
				}
				for (j = archives[x]->file_start_offset[y]; 
					 j < (archives[x]->file_decompressed_size[y] + archives[x]->file_start_offset[y]); 
					 j++)
				{
					fputc(archives[x]->archive_decompressed_data[j], fp);
				}
				fclose(fp);
				continue;
			}

			printf("File %d\n", y);
			printf("	Output: %s\n", dir_and_filename);
			printf("	Hash: %d\n", archives[x]->file_hashes[y]);
			printf("	Compressed size: %d\n", archives[x]->file_compressed_size[y]);
			printf("	Decomped size: %d\n", archives[x]->file_decompressed_size[y]);
			printf("	Init Offset: %d\n", archives[x]->file_start_offset[y]);
			printf("	Offset: %d\n", archives[x]->file_offset);

			FILE *fp = fopen(dir_and_filename, "w+");

			if (fp == NULL)
			{
				printf("NULL FILE OPENED!\n");
				exit(1);
			}
			for (j = 0; 
				 j < archives[x]->file_decompressed_size[y]; 
				 j++)
			{
				fputc(archives[x]->file_decomp_data[y][j], fp);
			}
			fclose(fp);
		}
	}
	int g;
	fclose(main_cache);
	
	for (g = 0; g < 5; g++)
	{
		fclose(cache_idx[g]);
	}
	return 0;
}
