#ifndef _STREAM_H_
#define _STREAM_H_

int get_3_bytes(char *buffer, int *position);
int get_unsigned_le_short(char *buffer, int *position);
int get_int(char *buffer, int *position);
int read_bits(int num_bits, char buffer[], int *position, int *bits_read,
			  int *char_unk); 

char read_unsigned_char(char buffer[], int *position, int *bits_read,
						int *char_unk); 
#endif
