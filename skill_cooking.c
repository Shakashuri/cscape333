/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"

int VALID_RAW_MEAT_ITEMS[] = {2132, 2134, 2136};

int get_cooking_level_needed(int cooking_id)
{
	switch (cooking_id)
	{
	/* MEAT.  */
		case COOKING_MEAT:
		case COOKING_CHICKEN:
		case COOKING_RABBIT:
			return 1;
	/* Breads.  */
		case COOKING_BREAD:
			return 1;
		case COOKING_PITTA_BREAD:
			return 58;
	
	/* FISH.  */
		case COOKING_SHRIMP:
		case COOKING_KARAMBWANJI:
		case COOKING_SARDINE:
		case COOKING_ANCHOVIES:
		case COOKING_POISON_KARAMBWAN:
			return 1;
		case COOKING_HERRING:
			return 5;
		case COOKING_MACKEREL:
			return 10;
		case COOKING_TROUT:
			return 15;
		case COOKING_COD:
			return 18;
		case COOKING_PIKE:
			return 20;
		case COOKING_SALMON:
			return 25;
		case COOKING_SLIMEY_EEL:
			return 28;
		case COOKING_TUNA:
		case COOKING_KARAMBWAN:
			return 30;
		case COOKING_CAVE_EEL:
			return 38;
		case COOKING_LOBSTER:
			return 40;
		case COOKING_BASS:
			return 43;
		case COOKING_SWORDFISH:
			return 45;
		case COOKING_LAVA_EEL:
			return 53;
		case COOKING_SHARK:
			return 80;
		case COOKING_SEA_TURTLE:
			return 82;
		case COOKING_MANTA_RAY:
			return 91;
	/* PIES.  */
		case COOKING_REDBERRY_PIE:
			return 10;
		case COOKING_MEAT_PIE:
			return 20;
		case COOKING_APPLE_PIE:
			return 30;
	/* PIZZAS.  */
		case COOKING_PLAIN_PIZZA:
			return 35;
		case COOKING_MEAT_PIZZA:
			return 45;
		case COOKING_ANCHOVY_PIZZA:
			return 55;
		case COOKING_PINEAPPLE_PIZZA:
			return 65;
	/* CAKES.  */
		case COOKING_CAKE:
			return 40;
		case COOKING_CHOCOLATE_CAKE:
			return 50;
	/* OTHER.  */
		case COOKING_WINE:
			return 35;
		case COOKING_SWEETCORN:
			return 28;
	/* BREWING.  */
		case COOKING_CIDER:
			return 14;
		case COOKING_DWARVEN_STOUT:
			return 19;
		case COOKING_ASGARNIAN_ALE:
			return 24;
		case COOKING_GREENMANS_ALE:
			return 29;
		case COOKING_WIZARDS_MIND_BOMB:
			return 34;
		case COOKING_DRAGON_BITTER:
			return 39;
		case COOKING_MOONLIGHT_MEAD:
			return 44;
		case COOKING_AXEMANS_FOLLY:
			return 49;
		case COOKING_CHEFS_DELIGHT:
			return 54;
		case COOKING_SLAYERS_RESPITE:
			return 59;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		case COOKING_FRUIT_BLAST:
			return 6;
		case COOKING_PINEAPPLE_BLAST:
			return 8;
		case COOKING_TOAD_CRUNCHIES:
			return 10;
		case COOKING_SPICY_CRUNCHIES:
			return 12;
		case COOKING_WORM_CRUNCHIES:
			return 14;
		case COOKING_CHOCCHIP_CRUNCHIES:
			return 16;
		case COOKING_WIZARD_BLIZZARD:
			return 18;
		case COOKING_SHORT_GREEN_GUY:
			return 20;
		case COOKING_FRUIT_BATTA:
			return 25;
		case COOKING_TOAD_BATTA:
			return 26;
		case COOKING_WORM_BATTA:
			return 27;
		case COOKING_VEGETABLE_BATTA:
			return 28;
		case COOKING_CHEESE_AND_TOMATO_BATTA:
			return 29;
		case COOKING_WORM_HOLE:
			return 29;
		case COOKING_DRUNK_DRAGON:
			return 32;
		case COOKING_CHOC_SATURDAY:
			return 33;
		case COOKING_VEG_BALL:
			return 34;
		case COOKING_BLUEBERRY_SPECIAL:
			return 37;
		case COOKING_TANGLED_TOADS_LEGS:
			return 40;
		case COOKING_CHOCOLATE_BOMB:
			return 42;
		default:
			printf("Unknown cooking id: %d level!\n", cooking_id);
			return -1;
	}
}

float get_cooking_xp(int cooking_id)
{
	switch (cooking_id)
	{
	/* MEAT.  */
		case COOKING_MEAT:
		case COOKING_CHICKEN:
		case COOKING_RABBIT:
			return 30.0;
	/* Breads.  */
		case COOKING_BREAD:
			return 40.5;
		case COOKING_PITTA_BREAD:
			return 40.0;
	
	/* FISH.  */
		case COOKING_SHRIMP:
			return 30.0;
		case COOKING_KARAMBWANJI:
			return 10.0;
		case COOKING_SARDINE:
			return 40.0;
		case COOKING_ANCHOVIES:
			return 30.0;
		case COOKING_POISON_KARAMBWAN:
			return 80.0;
		case COOKING_HERRING:
			return 50.0;
		case COOKING_MACKEREL:
			return 60.0;
		case COOKING_TROUT:
			return 70.0;
		case COOKING_COD:
			return 75.0;
		case COOKING_PIKE:
			return 80.0;
		case COOKING_SALMON:
			return 90.0;
		case COOKING_SLIMEY_EEL:
			return 95.0;
		case COOKING_TUNA:
			return 100.0;
		case COOKING_KARAMBWAN:
			return 190.0;
		case COOKING_CAVE_EEL:
			return 115.0;
		case COOKING_LOBSTER:
			return 120.0;
		case COOKING_BASS:
			return 130.0;
		case COOKING_SWORDFISH:
			return 140.0;
		case COOKING_LAVA_EEL:
			return 30.0;
		case COOKING_SHARK:
			return 210.0;
		case COOKING_SEA_TURTLE:
			return 211.3;
		case COOKING_MANTA_RAY:
			return 216.2;
	/* PIES.  */
		case COOKING_REDBERRY_PIE:
			return 78.0;
		case COOKING_MEAT_PIE:
			return 104.0;
		case COOKING_APPLE_PIE:
			return 130.0;
	/* PIZZAS.  */
		case COOKING_PLAIN_PIZZA:
			return 143.0;
		/* XP for adding on top of base item.  */
		case COOKING_MEAT_PIZZA:
			return 26.0;
		case COOKING_ANCHOVY_PIZZA:
			return 39.0;
		case COOKING_PINEAPPLE_PIZZA:
			return 52.0;
	/* CAKES.  */
		case COOKING_CAKE:
			return 180.0;
		/* XP for adding on top of base item.  */
		case COOKING_CHOCOLATE_CAKE:
			return 30.0;
	/* OTHER.  */
		case COOKING_WINE:
			return 200.0;
		case COOKING_SWEETCORN:
			return 104.0;
	/* BREWING.  */
		case COOKING_CIDER:
			return 182.0;
		case COOKING_DWARVEN_STOUT:
			return 215.0;
		case COOKING_ASGARNIAN_ALE:
			return 248.0;
		case COOKING_GREENMANS_ALE:
			return 281.0;
		case COOKING_WIZARDS_MIND_BOMB:
			return 314.0;
		case COOKING_DRAGON_BITTER:
			return 347.0;
		case COOKING_MOONLIGHT_MEAD:
			return 380.0;
		case COOKING_AXEMANS_FOLLY:
			return 413.0;
		case COOKING_CHEFS_DELIGHT:
			return 446.0;
		case COOKING_SLAYERS_RESPITE:
			return 479.0;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		case COOKING_FRUIT_BLAST:
			return 50.0;
		case COOKING_PINEAPPLE_BLAST:
			return 70.0;
		case COOKING_TOAD_CRUNCHIES:
			return 100.0;
		case COOKING_SPICY_CRUNCHIES:
			return 100.0;
		case COOKING_WORM_CRUNCHIES:
			return 100.0;
		case COOKING_CHOCCHIP_CRUNCHIES:
			return 100.0;
		case COOKING_WIZARD_BLIZZARD:
			return 110.0;
		case COOKING_SHORT_GREEN_GUY:
			return 120.0;
		case COOKING_FRUIT_BATTA:
			return 150.0;
		case COOKING_TOAD_BATTA:
			return 152.0;
		case COOKING_WORM_BATTA:
			return 154.0;
		case COOKING_VEGETABLE_BATTA:
			return 156.0;
		case COOKING_CHEESE_AND_TOMATO_BATTA:
			return 158.0;
		case COOKING_WORM_HOLE:
			return 170.0;
		case COOKING_DRUNK_DRAGON:
			return 160.0;
		case COOKING_CHOC_SATURDAY:
			return 170.0;
		case COOKING_VEG_BALL:
			return 175.0;
		case COOKING_BLUEBERRY_SPECIAL:
			return 180.0;
		case COOKING_TANGLED_TOADS_LEGS:
			return 185.0;
		case COOKING_CHOCOLATE_BOMB:
			return 190.0;
		default:
			printf("Unknown cooking id: %d xp!\n", cooking_id);
			return -1;
	}
}

/* There are multiple kinds of meats for cooking that all act the same but
   have different ids. This checks for that.  */
int get_cooking_valid_raw_meats(int food_id)
{
	switch (food_id)
	{
		case 2132:
		case 2134:
		case 2136:
			return 1;

		default:
			return 0;
	}
}

int get_cooking_raw_id(int cooking_id)
{
	switch (cooking_id)
	{
	/* MEAT.  */
		/* Note that there are many different meats, handled seperately.  */
		case COOKING_MEAT:
			return 2132;
		case COOKING_CHICKEN:
			return 2138;
		case COOKING_RABBIT:
			return 3226;
	/* Breads.  */
		case COOKING_BREAD:
			return 2307;
		case COOKING_PITTA_BREAD:
			return 1863;
	
	/* FISH.  */
		case COOKING_SHRIMP:
			return 317;
		case COOKING_KARAMBWANJI:
			return 3150;
		case COOKING_SARDINE:
			return 327;
		case COOKING_ANCHOVIES:
			return 321;
		case COOKING_POISON_KARAMBWAN:
			return 3142;
		case COOKING_HERRING:
			return 345;
		case COOKING_MACKEREL:
			return 353;
		case COOKING_TROUT:
			return 335;
		case COOKING_COD:
			return 341;
		case COOKING_PIKE:
			return 349;
		case COOKING_SALMON:
			return 331;
		case COOKING_SLIMEY_EEL:
			return 3379;
		case COOKING_TUNA:
			return 359;
		case COOKING_KARAMBWAN:
			return 3142;
		case COOKING_CAVE_EEL:
			return 5001;
		case COOKING_LOBSTER:
			return 377;
		case COOKING_BASS:
			return 363;
		case COOKING_SWORDFISH:
			return 371;
		case COOKING_LAVA_EEL:
			return 2148;
		case COOKING_SHARK:
			return 383;
		case COOKING_SEA_TURTLE:
			return 395;
		case COOKING_MANTA_RAY:
			return 389;
	/* PIES.  */
		case COOKING_REDBERRY_PIE:
			return 2321;
		case COOKING_MEAT_PIE:
			return 2319;
		case COOKING_APPLE_PIE:
			return 2317;
	/* PIZZAS.  */
		case COOKING_PLAIN_PIZZA:
			return 2287;
		case COOKING_MEAT_PIZZA:
			return 2293;
		case COOKING_ANCHOVY_PIZZA:
			return 2297;
		case COOKING_PINEAPPLE_PIZZA:
			return 2301;
	/* CAKES.  */
		case COOKING_CAKE:
			return 1889;
		case COOKING_CHOCOLATE_CAKE:
			return 1897;
	/* OTHER.  */
		case COOKING_WINE:
			return 1995;
		case COOKING_SWEETCORN:
			return 5986;
	/* BREWING.  */
		case COOKING_CIDER:
			return 5763;
		case COOKING_DWARVEN_STOUT:
			return 1913;
		case COOKING_ASGARNIAN_ALE:
			return 1905;
		case COOKING_GREENMANS_ALE:
			return 1909;
		case COOKING_WIZARDS_MIND_BOMB:
			return 1907;
		case COOKING_DRAGON_BITTER:
			return 1911;
		case COOKING_MOONLIGHT_MEAD:
			return 2955;
		case COOKING_AXEMANS_FOLLY:
			return 5751;
		case COOKING_CHEFS_DELIGHT:
			return 5755;
		case COOKING_SLAYERS_RESPITE:
			return 5759;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		case COOKING_FRUIT_BLAST:
			return 2084;
		case COOKING_PINEAPPLE_BLAST:
			return 2048;
		case COOKING_TOAD_CRUNCHIES:
			return 2217;
		case COOKING_SPICY_CRUNCHIES:
			return 2213;
		case COOKING_WORM_CRUNCHIES:
			return 2205;
		case COOKING_CHOCCHIP_CRUNCHIES:
			return 2209;
		case COOKING_WIZARD_BLIZZARD:
			return 2054;
		case COOKING_SHORT_GREEN_GUY:
			return 2080;
		case COOKING_FRUIT_BATTA:
			return 2277;
		case COOKING_TOAD_BATTA:
			return 2255;
		case COOKING_WORM_BATTA:
			return 2253;
		case COOKING_VEGETABLE_BATTA:
			return 2281;
		case COOKING_CHEESE_AND_TOMATO_BATTA:
			return 2259;
		case COOKING_WORM_HOLE:
			return 2191;
		case COOKING_DRUNK_DRAGON:
			return 2092;
		case COOKING_CHOC_SATURDAY:
			return 2074;
		case COOKING_VEG_BALL:
			return 2195;
		case COOKING_BLUEBERRY_SPECIAL:
			return 2064;
		case COOKING_TANGLED_TOADS_LEGS:
			return 2187;
		case COOKING_CHOCOLATE_BOMB:
			return 2185;
		default:
			printf("Unknown cooking id: %d level!\n", cooking_id);
			return -1;
	}
}

int get_cooking_cooked_id(int cooking_id)
{
	switch (cooking_id)
	{
	/* MEAT.  */
		case COOKING_MEAT:
			return 2142;
		case COOKING_CHICKEN:
			return 2140;
		case COOKING_RABBIT:
			return 3228;
	/* Breads.  */
		case COOKING_BREAD:
			return 2309;
		case COOKING_PITTA_BREAD:
			return 1865;
	
	/* FISH.  */
		case COOKING_SHRIMP:
			return 315;
		case COOKING_KARAMBWANJI:
			return 3151;
		case COOKING_SARDINE:
			return 325;
		case COOKING_ANCHOVIES:
			return 319;
		case COOKING_POISON_KARAMBWAN:
			return 3146;
		case COOKING_HERRING:
			return 347;
		case COOKING_MACKEREL:
			return 355;
		case COOKING_TROUT:
			return 333;
		case COOKING_COD:
			return 339;
		case COOKING_PIKE:
			return 351;
		case COOKING_SALMON:
			return 329;
		case COOKING_SLIMEY_EEL:
			return 3381;
		case COOKING_TUNA:
			return 361;
		case COOKING_KARAMBWAN:
			return 3144;
		case COOKING_CAVE_EEL:
			return 5003;
		case COOKING_LOBSTER:
			return 379;
		case COOKING_BASS:
			return 365;
		case COOKING_SWORDFISH:
			return 373;
		case COOKING_LAVA_EEL:
			return 2149;
		case COOKING_SHARK:
			return 385;
		case COOKING_SEA_TURTLE:
			return 397;
		case COOKING_MANTA_RAY:
			return 391;
	/* PIES.  */
		case COOKING_REDBERRY_PIE:
			return 2325;
		case COOKING_MEAT_PIE:
			return 2327;
		case COOKING_APPLE_PIE:
			return 2323;
	/* PIZZAS.  */
		case COOKING_PLAIN_PIZZA:
			return 2289;
		case COOKING_MEAT_PIZZA:
			return 2293;
		case COOKING_ANCHOVY_PIZZA:
			return 2297;
		case COOKING_PINEAPPLE_PIZZA:
			return 2301;
	/* CAKES.  */
		case COOKING_CAKE:
			return 1891;
		case COOKING_CHOCOLATE_CAKE:
			return 1897;
	/* OTHER.  */
		case COOKING_WINE:
			return 1993;
		case COOKING_SWEETCORN:
			return 5988;
	/* BREWING.  */
		case COOKING_CIDER:
			return 5763;
		case COOKING_DWARVEN_STOUT:
			return 1913;
		case COOKING_ASGARNIAN_ALE:
			return 1905;
		case COOKING_GREENMANS_ALE:
			return 1909;
		case COOKING_WIZARDS_MIND_BOMB:
			return 1907;
		case COOKING_DRAGON_BITTER:
			return 1911;
		case COOKING_MOONLIGHT_MEAD:
			return 2955;
		case COOKING_AXEMANS_FOLLY:
			return 5751;
		case COOKING_CHEFS_DELIGHT:
			return 5755;
		case COOKING_SLAYERS_RESPITE:
			return 5759;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		case COOKING_FRUIT_BLAST:
			return 2084;
		case COOKING_PINEAPPLE_BLAST:
			return 2048;
		case COOKING_TOAD_CRUNCHIES:
			return 2217;
		case COOKING_SPICY_CRUNCHIES:
			return 2213;
		case COOKING_WORM_CRUNCHIES:
			return 2205;
		case COOKING_CHOCCHIP_CRUNCHIES:
			return 2209;
		case COOKING_WIZARD_BLIZZARD:
			return 2054;
		case COOKING_SHORT_GREEN_GUY:
			return 2080;
		case COOKING_FRUIT_BATTA:
			return 2277;
		case COOKING_TOAD_BATTA:
			return 2255;
		case COOKING_WORM_BATTA:
			return 2253;
		case COOKING_VEGETABLE_BATTA:
			return 2281;
		case COOKING_CHEESE_AND_TOMATO_BATTA:
			return 2259;
		case COOKING_WORM_HOLE:
			return 2191;
		case COOKING_DRUNK_DRAGON:
			return 2092;
		case COOKING_CHOC_SATURDAY:
			return 2074;
		case COOKING_VEG_BALL:
			return 2195;
		case COOKING_BLUEBERRY_SPECIAL:
			return 2064;
		case COOKING_TANGLED_TOADS_LEGS:
			return 2187;
		case COOKING_CHOCOLATE_BOMB:
			return 2185;
		default:
			printf("Unknown cooking id: %d level!\n", cooking_id);
			return -1;
	}
}

int get_cooking_burnt_id(int cooking_id)
{
	switch (cooking_id)
	{
	/* MEAT.  */
		case COOKING_MEAT:
			return 2146;
		case COOKING_CHICKEN:
			return 2144;
		/* FIXME  */
		case COOKING_RABBIT:
			return 3228;
	/* Breads.  */
		case COOKING_BREAD:
			return 2311;
		case COOKING_PITTA_BREAD:
			return 1867;
	
	/* FISH.  */
		case COOKING_SHRIMP:
			return 323;
		/* FIXME: No burnt version?  */
		case COOKING_KARAMBWANJI:
			return 3151;
		case COOKING_SARDINE:
			return 369;
		case COOKING_ANCHOVIES:
			return 323;
		case COOKING_KARAMBWAN:
		case COOKING_POISON_KARAMBWAN:
			return 3148;
		case COOKING_HERRING:
			return 357;
		case COOKING_MACKEREL:
			return 357;
		case COOKING_TROUT:
			return 343;
		case COOKING_COD:
			return 343;
		case COOKING_PIKE:
			return 343;
		case COOKING_SALMON:
			return 343;
		case COOKING_SLIMEY_EEL:
			return 3383;
		case COOKING_TUNA:
			return 367;
		case COOKING_CAVE_EEL:
			return 5002;
		case COOKING_LOBSTER:
			return 381;
		case COOKING_BASS:
			return 367;
		case COOKING_SWORDFISH:
			return 375;
		case COOKING_LAVA_EEL:
			return 3383;
		case COOKING_SHARK:
			return 387;
		case COOKING_SEA_TURTLE:
			return 399;
		case COOKING_MANTA_RAY:
			return 393;
	/* PIES.  */
		case COOKING_REDBERRY_PIE:
		case COOKING_MEAT_PIE:
		case COOKING_APPLE_PIE:
			return 2329;
	/* PIZZAS.  */
		case COOKING_PLAIN_PIZZA:
		case COOKING_MEAT_PIZZA:
		case COOKING_ANCHOVY_PIZZA:
		case COOKING_PINEAPPLE_PIZZA:
			return 2305;
	/* CAKES.  */
		case COOKING_CAKE:
		case COOKING_CHOCOLATE_CAKE:
			return 1903;
	/* OTHER.  */
		case COOKING_SWEETCORN:
			return 5990;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		case COOKING_TOAD_CRUNCHIES:
		case COOKING_SPICY_CRUNCHIES:
		case COOKING_WORM_CRUNCHIES:
		case COOKING_CHOCCHIP_CRUNCHIES:
			return 2199;
		case COOKING_FRUIT_BATTA:
		case COOKING_TOAD_BATTA:
		case COOKING_WORM_BATTA:
		case COOKING_VEGETABLE_BATTA:
		case COOKING_CHEESE_AND_TOMATO_BATTA:
			return 2247;
		case COOKING_WORM_HOLE:
		case COOKING_VEG_BALL:
		case COOKING_TANGLED_TOADS_LEGS:
		case COOKING_CHOCOLATE_BOMB:
			return 2175;
		default:
			printf("Unknown cooking id: %d level!\n", cooking_id);
			return -1;
	}
}

int get_cooking_level_stop_burn(int cooking_id, int has_cooking_gauntlets)
{
	int stop_burn_level = 0;
	int cooking_level = 0;
	int bonus = 0;

	switch (cooking_id)
	{
	/* MEAT.  */
		case COOKING_MEAT:
		case COOKING_CHICKEN:
		case COOKING_RABBIT:
			stop_burn_level = 31;
			break;
	/* BREAD.  */
		case COOKING_BREAD:
			stop_burn_level = 10;
			break;
	/* FISH.  */
		case COOKING_SHRIMP:
			stop_burn_level = 33;
			break;
		case COOKING_POISON_KARAMBWAN:
			stop_burn_level = 99;
			break;
		case COOKING_SARDINE:
			stop_burn_level = 35;
			break;
		case COOKING_ANCHOVIES:
			stop_burn_level = 34;
			break;
		case COOKING_HERRING:
			stop_burn_level = 41;
			break;
		case COOKING_MACKEREL:
			stop_burn_level = 45;
			break;
		case COOKING_TROUT:
			stop_burn_level = 50;
			break;
		case COOKING_COD:
			stop_burn_level = 52;
			break;
		case COOKING_PIKE:
			stop_burn_level = 64;
			break;
		case COOKING_SALMON:
			stop_burn_level = 58;
			break;
		case COOKING_SLIMEY_EEL:
			stop_burn_level = 58;
			break;
		case COOKING_TUNA:
			stop_burn_level = 64;
			break;
		case COOKING_KARAMBWAN:
			stop_burn_level = 99;
			break;
		case COOKING_LOBSTER:
			stop_burn_level = 74;
			break;
		case COOKING_BASS:
			stop_burn_level = 80;
			break;
		case COOKING_SWORDFISH:
			stop_burn_level = 86;
			break;
		case COOKING_SHARK:
			stop_burn_level = 100;
			break;
		default:
			printf("Unknown cooking no burn id: %d level!\n", cooking_id);
			cooking_level = get_cooking_level_needed(cooking_id);
			stop_burn_level = (cooking_level + (cooking_level / 2));		
			printf("\tlevel: %d gen no burn level: %d\n", cooking_level, 
				   stop_burn_level);
			break;
	}
	
	if (has_cooking_gauntlets)
	{
		switch (cooking_id)
		{
			case COOKING_SHRIMP:
				bonus = 5;
				break;
			case COOKING_TUNA:
				bonus = 1;
				break;
			case COOKING_LOBSTER:
				bonus = 10;
				break;
			case COOKING_SWORDFISH:
				bonus = 5;
				break;
			case COOKING_SHARK:
				bonus = 4;
				break;
		}
	}
	
	return (stop_burn_level - bonus);
}

/* Returns 1 if fire, 2 if range.  */
int get_cooking_fire_type(int object_id)
{
	switch (object_id)
	{
		/* Fires.  */
		case 2732:
		case 3038:
		case 3769:
		case 4265:
		case 4266:
		case 5249:
		case 5499:
		case 5631:
		case 5632:
			return COOKING_FIRE_NORMAL;

		/* Ranges.  */
		case 2728:
		case 2729:
		case 2730:
		case 2731:
		/* XXX: Test this one, may not be a valid cooking range.  */
		case 2859:
		case 3039:
		case 4172:
		case 5275:
		case 8750:
			return COOKING_FIRE_RANGE;

		/* Lumbridge cooking range.  */
		case 114:
			return COOKING_FIRE_LUMBRIDGE;

		default:
			return 0;
	}
}

int get_cooking_needs_range(int cooking_id)
{
	switch (cooking_id)
	{
		/* Breads.  */
		case COOKING_BREAD:
		case COOKING_PITTA_BREAD:
		/* Pies.  */
		case COOKING_REDBERRY_PIE:
		case COOKING_MEAT_PIE:
		case COOKING_APPLE_PIE:
		/* Pizza.  */
		case COOKING_PLAIN_PIZZA:
		/* Cake.  */
		case COOKING_CAKE:
			return 1;

		default:
			return 0;
	}
}

/**
 * Handles the player milking a dairy cow.
 * @param *c Client instance.
 *
 * @return Returns 1 on failure, 0 on success.
 */
int milk_dairy_cow(struct client *c)
{
	int empty_bucket_id = 1925;
	int milk_bucket_id = 1927;
	int milking_animation_id = 2305;

	int bucket_slot = player_has_item(c, empty_bucket_id, 1);

	if (bucket_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have a bucket to hold the milk.");
		return 1;
	}
	else
	{
		player_play_animation(c, milking_animation_id);
		packet_send_chatbox_message(c, "You milk the dairy cow.");
		transform_item(c, bucket_slot, milk_bucket_id);

		c->plr->frozen_ticks = 3;
		c->plr->frozen_type = EFFECT_FREEZE;
		return 0;
	}
}

/**
 * Calculates the chance a player has to burn a specific food.
 * @param *c Client instance.
 * @param cooking_id What they are cooking.
 * @param bonus Any bonus modifiers to their burn chance.
 *
 * @return Returns their burn chance, out of 100.
 */
int calculate_cooking_burn_chance(struct client *c, int cooking_id, int bonus)
{
	int burn_chance = (skills_cooking_base_burn_chance - bonus);
	int plr_cooking_lvl = get_player_skill_level(c, COOKING_SKILL);
	int food_lvl_req = get_cooking_level_needed(cooking_id);
	int stop_burn_lvl = 0;
	int plr_lvls_left = 0;
	int food_lvls_left = 0;
	double burn_change_per_lvl = 0.0;
	
	/* Check for cooking gauntlets.  */
	if (c->plr->equipment[HANDS_SLOT] == 775)
	{
		stop_burn_lvl = get_cooking_level_stop_burn(cooking_id, 1);
	}
	else
	{
		stop_burn_lvl = get_cooking_level_stop_burn(cooking_id, 0);
	}

	food_lvls_left = (stop_burn_lvl - food_lvl_req); 
	plr_lvls_left = (plr_cooking_lvl - food_lvl_req);
	burn_change_per_lvl = ((double) burn_chance / (double) food_lvls_left);
	
	return (burn_chance - (plr_lvls_left * burn_change_per_lvl));
}

/**
 * Creates the flour bins for a player, based off of what state they are in.
 * @param *c Client instance.
 */
void spawn_flour_bins(struct client *c)
{
	int x;

	if (c->plr->grain_bins_flour_count > 0
		&& c->plr->flour_bins_spawned == 0)
	{
		for (x = 0; x < PROTOCOL_MAX_FLOUR_BINS; x++)
		{
			c->plr->flour_bins_indexes[x] 
				= create_inst_object(c, 1782, 10, flour_bin_x[x], 
									 flour_bin_y[x], 0, flour_bin_face[x], -1);
		}
		c->plr->flour_bins_spawned = 1;
	}
}

/**
 * Handles when the player uses the grain hopper controls, makes the flour.
 * @param *c Client instance.
 */
void hopper_controls(struct client *c)
{
	/*
	What animation is used here? FIXME
	player_play_animation(c, 
	*/

	if (c->plr->grain_hopper_count == 0)
	{
		packet_send_chatbox_message(c, "There's no grain in the hopper.");
		return;
	}
	else
	{
		packet_send_chatbox_message(c, "You operate the hopper. " 
			"The grain slides down the chute.");
	}

	c->plr->grain_bins_flour_count += c->plr->grain_hopper_count;

	/* Check if the number of loads of flour is greater than the limit,
	 * reduce it if so.  */
	if (c->plr->grain_bins_flour_count > skills_cooking_max_flour_in_bin)
	{
		c->plr->grain_hopper_count =
		(c->plr->grain_bins_flour_count - skills_cooking_max_flour_in_bin);

		c->plr->grain_bins_flour_count = skills_cooking_max_flour_in_bin;
	}

	c->plr->grain_hopper_count = 0;

	spawn_flour_bins(c);
}

/**
 * Handles when the player uses the flour bins, takes flour.
 * @param *c Client instance.
 */
void handle_flour_bins(struct client *c)
{
	int x = 0;
	int pot_slot = player_has_item(c, 1931, 1);

	if (pot_slot == -1)
	{
		packet_send_chatbox_message(c, "You need an empty pot to gather the flour!");
		return;
	}

	transform_item(c, pot_slot, 1933);
	c->plr->grain_bins_flour_count--;

	/* Handle removal of flour bins.  */
	if (c->plr->grain_bins_flour_count == 0
		&& c->plr->flour_bins_spawned == 1)
	{
		for (x = 0; x < PROTOCOL_MAX_FLOUR_BINS; x++)
		{
			handle_transforming_inst_object(c, c->plr->flour_bins_indexes[x], 
				1781, 0);
			c->plr->inst_objects_on_ground[c->plr->flour_bins_indexes[x]] = 0;
			spawn_inst_object_on_ground(c, c->plr->flour_bins_indexes[x]);
			remove_inst_object(c, c->plr->flour_bins_indexes[x], 0);
		}

		c->plr->flour_bins_spawned = 0;
		packet_send_chatbox_message(c, "You fill a pot with the last of the flour "
					"in the bin.");
	}
	else
	{
		packet_send_chatbox_message(c, "You fill a pot with flour from the bin.");
	}
}

/**
 * Creates dough out of flour and water.
 * @param *c Client instance.
 * @param choice What kind of dough being made.
 */
void create_dough(struct client *c, int choice)
{
	int free_slots = get_free_inv_slots(c);
	int cooking_level = get_player_skill_level(c, COOKING_SKILL);
	/* 1933 for flour pot is hardcoded here, FIXME if needed.  */
	int flour_slot = player_has_item(c, 1933, 1);
	int flour_id = 1933;
	int water_slot = -1;
	int water_id = 0;
	int x = 0;

	/* Check for needed level.  */
	switch (choice)
	{
		/* Pastry dough.  */
		case 1:
			if (cooking_level < 10)
			{
				packet_send_chatbox_message(c, "You must have at least level 10 cooking to"
							 " make that!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			break;
		/* Pizza dough.  */
		case 2:
			if (cooking_level < 35)
			{
				packet_send_chatbox_message(c, "You must have at least level 35 cooking to"
							 " make that!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			break;
		/* Pitta dough.  */
		case 3:
			if (cooking_level < 58)
			{
				packet_send_chatbox_message(c, "You must have at least level 58 cooking to"
							 " make that!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			break;
	}

	/* Check for needed items.  */
	if (free_slots < 1)
	{
		packet_send_chatbox_message(c, "You don't have enough inventory space to make that!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (flour_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have any flour for that!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	for (x = 0; x < INV_SIZE; x++)
	{
		if (check_for_water_container(c->plr->items[x]) == 1 
			&& c->plr->items_n[x] >= 1)
		{
			water_slot = x;
			water_id = c->plr->items[x];
			break;
		}
	}

	if (water_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have a water container for that!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Change flour and water containers to their empty versions.  */
	transform_item(c, flour_slot, ITEMS[flour_id]->item_consume_id);
	transform_item(c, water_slot, ITEMS[water_id]->item_consume_id);

	/* Begin making dough.  */
	switch (choice)
	{
		/* Bread dough.  */
		case 0:
			add_item_to_inv(c, 2307, 1, -1);
			packet_send_chatbox_message(c, "You mix the water and flour and make some "
						 "bread dough");
			break;
		/* Pastry dough.  */
		case 1:
			add_item_to_inv(c, 1953, 1, -1);
			packet_send_chatbox_message(c, "You mix the water and flour and make some "
						 "pastry dough");
			break;
		/* Pizza dough.  */
		case 2:
			add_item_to_inv(c, 2283, 1, -1);
			packet_send_chatbox_message(c, "You mix the water and flour and make some "
						 "pizza dough");
			add_skill_xp(c, COOKING_SKILL, 1.0);
			break;
		/* Pitta dough.  */
		case 3:
			add_item_to_inv(c, 1863, 1, -1);
			packet_send_chatbox_message(c, "You mix the water and flour and make some "
						 "pitta dough");
			add_skill_xp(c, COOKING_SKILL, 1.0);
			break;
	}
}

/**
 * Initialize all cooking related variables before processing of it begins.
 * Checks fire used, recipe type, level requirements, etc.
 * @param *c Client instance.
 */
void init_cooking(struct client *c)
{
	int fire_type = get_cooking_fire_type(c->plr->action_id);
	int needs_range = get_cooking_needs_range(c->plr->action_id_type);
	int free_slots = get_free_inv_slots(c);
	int cooking_level = get_player_skill_level(c, COOKING_SKILL);

	c->plr->action_skill_id = COOKING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_COOKING);
	
	/* Check if player has enough space for the action.  */
	if (c->plr->action_item_space_needed > free_slots)
	{
		packet_send_chatbox_message(c, "You don't have enough inventory space to make that!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Store what level is needed to cook the food.  */
	c->plr->action_skill_lvl_req 
	= get_cooking_level_needed(c->plr->action_id_type);

	if (cooking_level < c->plr->action_skill_lvl_req)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "cooking to make that!\n", 
				 c->plr->action_skill_lvl_req);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Change cooking animation based on fire or range used.  */
	switch (fire_type)
	{
		/* Fires.  */
		case COOKING_FIRE_NORMAL:
			c->plr->skill_tool_bonus = 1;
			c->plr->skill_tool_emote = 897;
			break;
		/* Ranges.  */
		case COOKING_FIRE_RANGE:
			c->plr->skill_tool_bonus = 2;
			c->plr->skill_tool_emote = 883;
			break;
		/* Lumbridge range.  */
		case COOKING_FIRE_LUMBRIDGE:
			c->plr->skill_tool_bonus = 3;
			c->plr->skill_tool_emote = 883;
			break;
		/*
		default:
			printf("id: %d\n", c->plr->action_id);
			break;
			*/
	}

	/* Check if the food needs to be cooked on a range.  */
	if (needs_range == 1 && fire_type == COOKING_FIRE_NORMAL)
	{
		packet_send_chatbox_message(c, "You need to cook that on a range!");
		set_player_state(c, STATE_IDLE);
		return;
	}
}

/**
 * Handles the creation of all food dishes.
 * @param *c Client instance.
 */
void handle_cooking(struct client *c)
{
	int burn_chance = 0;
	int burn_bonus = 0;
	int food_cooked_id = get_cooking_cooked_id(c->plr->action_id_type);
	int food_raw_id = get_cooking_raw_id(c->plr->action_id_type); 
	int base_item = ITEMS[food_raw_id]->item_consume_id;
	int food_raw_slot = 0;
	int burn_roll = 0;
	char result_str[100];

	/* Handle cooking different raw meats like beef, rat, and bear.  */
	if (c->plr->action_id_type == COOKING_MEAT)
	{
		food_raw_slot = get_player_item_location_multiple_valid(c, 
			VALID_RAW_MEAT_ITEMS, 3);
	}
	else
	{
		food_raw_slot = player_has_item(c, food_raw_id, 1);
	}

	printf("handle_cooking\n");

	printf("Cooked_id: %d\n", food_cooked_id);
	printf("Raw_id: %d\n", food_raw_id);
	printf("Action waiting: %d\n", c->plr->action_skill_waiting_for_resource);

	/* Food was put on the fire, get it back.  */
	if (c->plr->action_skill_waiting_for_resource == 1)
	{
		/* Based on the fire used, apply bonus to burn chance.  */
		switch (c->plr->skill_tool_bonus)
		{
			case COOKING_FIRE_NORMAL:
				burn_bonus = 0;
				break;
			case COOKING_FIRE_RANGE:
				burn_bonus = skills_cooking_range_burn_mod;
				break;
			case COOKING_FIRE_LUMBRIDGE:
				burn_bonus = skills_cooking_lumbridge_range_burn_mod;
				break;
		}

		burn_chance 
			= calculate_cooking_burn_chance(c, c->plr->action_id_type, 
											burn_bonus);
		burn_roll = rand_int(1, 100);

		delete_item_from_inv_slot(c, food_raw_slot, 1);
		
		/* Check if the dish needs to give back a pan or something.  */
		if (base_item != -1)
		{
			add_item_to_inv(c, base_item, 1, -1);	
		}

		if (DEBUG == 1)
		{
			printf("burn percent: %d roll: %d\n", burn_chance, burn_roll);
		}

		if (burn_roll > burn_chance)
		{
			/* Food is cooked.  */
			add_item_to_inv(c, food_cooked_id, 1, -1);
			add_skill_xp(c, COOKING_SKILL, 
						 get_cooking_xp(c->plr->action_id_type));
			snprintf(result_str, 100, "You nicely cook the %s.", 
					 ITEMS[food_cooked_id]->name);
			packet_send_chatbox_message(c, result_str);
		}
		else
		{
			/* Food is burnt.  */
			add_item_to_inv(c, get_cooking_burnt_id(c->plr->action_id_type), 1, 
							-1);
			snprintf(result_str, 100, "You burnt the %s.", 
					 ITEMS[food_cooked_id]->name);
			packet_send_chatbox_message(c, result_str);
		}
		
		c->plr->action_skill_waiting_for_resource = 0;
		player_replay_animation(c);
		return;
	}

	/* Out of food to cook.  */
	if (food_raw_slot == -1 || c->plr->action_skill_remaining_actions == 0)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	player_play_animation(c, c->plr->skill_tool_emote);
	printf("\t\t\tplay!\n");
	c->plr->action_skill_waiting_for_resource = 1;

	/* Retarded change in cooking delay.  */
	if (c->plr->action_total_time_acted >= 2 
		&& skills_cooking_enable_time_increase)
	{
		c->plr->action_ticks = 4;
	}
	else
	{
		c->plr->action_ticks = 3;
	}

	c->plr->action_total_time_acted++;
	c->plr->action_skill_remaining_actions--;
}
