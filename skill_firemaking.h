#ifndef _SKILL_FIREMAKING_H_
#define _SKILL_FIREMAKING_H_

int get_firemaking_level_needed(int firemaking_id);
float get_firemaking_chance(int firemaking_id, int firemaking_level);
float get_firemaking_xp(int firemaking_id);
int get_sacred_oil_by_dose(int doses);
int get_annointed_log_id(int log_id);
int get_pyre_log_id(int log_id);
int get_pyre_remains_id(int remains_id);

void anoint_logs(struct client *c, int oil_id, int oil_slot, int logs_id,
				  int logs_slot, int firemaking_id);
void add_logs_to_pyre(struct client *c, int logs_id, int logs_slot, 
					  int firemaking_id);
void add_remains_to_pyre(struct client *c, int remains_id, int remains_slot);
void init_light_pyre_remains(struct client *c);
void light_pyre_remains(struct client *c);
void init_firemaking(struct client *c);
void handle_firemaking(struct client *c);

#endif
