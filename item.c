/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file item.c
 * @brief Functions relating to items, equipment, item spawning, stats, etc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"
#include "client.h"
#include "combat.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "stream.h"
#include "skills.h"
#include "skill_magic.h"

/**@{*/
/** The following functions are to detect if certain player appearance features
 * need to be hidden based on equipment worn.
 * @param item Item id.
 * @return 1 if item has the corresponding display mask.
 * @return 0 if item doesn't hide the corresponding features.
 */
int does_item_hide_hair(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 1)
		{
			return 1;
		}
	}
	return 0;
}

int does_item_hide_beard(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 2)
		{
			return 1;
		}
	}
	return 0;
}

int does_item_hide_hair_and_beard(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 3)
		{
			return 1;
		}
	}
	return 0;
}

int does_item_hide_arms(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 4)
		{
			return 1;
		}
	}
	return 0;
}

int does_item_hide_hands(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 5)
		{
			return 1;
		}
	}
	return 0;
}

int does_item_hide_arms_and_hands(int item)
{
	if (item > 1)
	{
		if (ITEMS[item]->display_mask == 6)
		{
			return 1;
		}
	}
	return 0;
}
/**@}*/

static int add_item_to_inv_base(struct client *c, int item_id, int amount, 
								int charges, int show_msg, int refresh) 
{
	int i;
	int free_slots = 0;
	int empty_slot = -1;

	/* Check within item id bounds.  */
	if (item_id < 0 || item_id >= PROTOCOL_MAX_ITEMS)
	{
		return 0;
	}

	/* If item doesn't stack, set amount to 1. Check for amount within 
	 * bounds.  */
	if (ITEMS[item_id]->stacking == 0 || amount < 1) 
	{
		amount = 1;
	}

	/* Get empty inventory slots.  */
	free_slots = get_free_inv_slots(c);
	
	/* We need an empty slot if the item is non-stacking, otherwise we can
	 * add it to the previous stack in their inventory, if applicable.  */
	if (free_slots >= amount 
		|| (ITEMS[item_id]->stacking == 1))
	{
		for (i = 0; i < INV_SIZE; i++) 
		{
			if (c->plr->items[i] == item_id
					&& ITEMS[item_id]->stacking == 1)
			{
				c->plr->items[i] = item_id;
				c->plr->items_charges[i] = charges;

				if ((c->plr->items_n[i] + amount) < MAX_ITEM_STACK 
						&& (c->plr->items_n[i] + amount) > -1) 
				{
					c->plr->items_n[i] += amount;
				} 
				else 
				{
					c->plr->items_n[i] = MAX_ITEM_STACK;
				}

				packet_set_item_in_interface(c, INTR_INV_PLR_ITEMS, i, item_id, 
									c->plr->items_n[i]);
				if (refresh == 1)
				{
					packet_reload_items_in_inventory_interface(c);
				}
				packet_send_total_carried_weight(c);
				return 1;
			}
			else if (c->plr->items[i] <= 0 && empty_slot == -1) 
			{
				empty_slot = i;
			}
		}

		/* If an empty slot for the item was found, add it there.  */
		if (empty_slot != -1)
		{
			c->plr->items[empty_slot] = item_id;
			c->plr->items_charges[empty_slot] = charges;

			if ((c->plr->items_n[empty_slot] + amount) < MAX_ITEM_STACK 
					&& (c->plr->items_n[empty_slot] + amount) > -1) 
			{
				c->plr->items_n[empty_slot] += amount;
			} 
			else 
			{
				c->plr->items_n[empty_slot] = MAX_ITEM_STACK;
			}

			packet_set_item_in_interface(c, INTR_INV_PLR_ITEMS, i, item_id, 
								c->plr->items_n[empty_slot]);
			if (refresh == 1)
			{
				packet_reload_items_in_inventory_interface(c);
			}
			packet_send_total_carried_weight(c);

			return 1;
		}
		else
		{
			if (show_msg == 1)
			{
				packet_send_chatbox_message(c, "Not enough space in your inventory.");
			}
			return 0;
		}
	} 
	else 
	{
		if (show_msg == 1)
		{
			packet_send_chatbox_message(c, "Not enough space in your inventory.");
		}
		return 0;
	}
}

/**
 * Adds an item to a player's inventory if able. Default function, prints out
 * no inventory message if needed, refreshes interface.
 * @param *c Client instance.
 * @param item_id ID of the item to add.
 * @param amount How many of that item to add.
 * @param charges How many charges the item has.
 * @return Returns 1 if adding item to inventory succeeded.
 * @return 0 if item was not able to be added.
 */
int add_item_to_inv(struct client *c, int item_id, int amount, int charges) 
{
	return add_item_to_inv_base(c, item_id, amount, charges, 1, 1);
}

/**
 * Follows the same structure as add_item_to_inv, but doesn't print out the
 * no inventory space message.
 * @see add_item_to_inv
 * @param *c Client instance.
 * @param item_id ID of the item to add.
 * @param amount How many of that item to add.
 * @param charges How many charges the item has.
 * @return Returns 1 if adding item to inventory succeeded.
 * @return 0 if item was not able to be added.
 */
int add_item_to_inv_silent(struct client *c, int item_id, int amount, 
						   int charges) 
{
	return add_item_to_inv_base(c, item_id, amount, charges, 0, 1);
}

/**
 * Follows the same structure as add_item_to_inv, but doesn't refresh the
 * inventory interface.
 * @see add_item_to_inv
 * @param *c Client instance.
 * @param item_id ID of the item to add.
 * @param amount How many of that item to add.
 * @param charges How many charges the item has.
 * @return Returns 1 if adding item to inventory succeeded.
 * @return 0 if item was not able to be added.
 */
int add_item_to_inv_no_refresh(struct client *c, int item_id, int amount, 
							   int charges) 
{
	return add_item_to_inv_base(c, item_id, amount, charges, 1, 0);
}

/**
 * Follows the same structure as add_item_to_inv, but doesn't print out the
 * no inventory space message and doesn't refresh inventory interface.
 * @see add_item_to_inv
 * @param *c Client instance.
 * @param item_id ID of the item to add.
 * @param amount How many of that item to add.
 * @param charges How many charges the item has.
 * @return Returns 1 if adding item to inventory succeeded.
 * @return 0 if item was not able to be added.
 */
int add_item_to_inv_silent_no_refresh(struct client *c, int item_id, 
									  int amount, int charges) 
{
	return add_item_to_inv_base(c, item_id, amount, charges, 0, 0);
}

/**
 * Adds an item to the player's inventory, but drops it on the ground if there
 * isn't any space for it.
 * @see add_item_to_inv
 * @param *c Client instance.
 * @param item_id ID of the item to add.
 * @param amount How many of that item to add.
 * @param charges How many charges the item has.
 */
void add_item_to_inv_drop_if_no_space(struct client *c, int item_id, 
									  int amount, int charges)
{
	char loot_msg[100];
	int rv = -1;

	if (charges == -1)
	{
		charges = get_item_default_charges(item_id);
	}

	rv = add_item_to_inv_silent(c, item_id, amount, charges);

	/* No space, drop item on ground and alert player.  */
	if (rv == 0)
	{
		add_to_item_drops_exclusive(c, item_id, amount, charges,
									c->plr->world_x, c->plr->world_y,
									c->plr->world_z, 1);
		snprintf(loot_msg, 100, 
				 "You drop the %s on the ground.",
				 string_to_lower(ITEMS[item_id]->name));
		packet_send_chatbox_message(c, loot_msg);
	}
}

/**
 * Adds an item to a player's inventory after a specified delay.
 * @param *c Client instance.
 * @param item_id Item to add.
 * @param num Amount of item to add.
 * @param charges How many charges the item has.
 * @param delay How many ticks to wait until the item is added.
 */
void add_item_to_inv_delay(struct client *c, int item_id, int num, int charges, 
						   int delay)
{
	c->plr->item_add_timer = delay;
	c->plr->item_add_id = item_id;
	c->plr->item_add_num = num;
	c->plr->item_add_charges = charges;
}

/**
 * Removes an item from a player's inventory.
 * @param *c Client instance.
 * @param item_id Item id to remove.
 * @param amount How many of the item to remove.
 */
void delete_item_from_inv(struct client *c, int item_id, int amount)
{
	int x;

	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] == item_id)
		{
			if (c->plr->items_n[x] > amount)
			{
				c->plr->items_n[x] -= amount;
			}
			else
			{
				c->plr->items[x] = -1;
				c->plr->items_n[x] = 0;
				c->plr->items_charges[x] = -1;
			}
			break;
		}
	}
	
	packet_reload_items_in_inventory_interface(c);
	packet_send_total_carried_weight(c);
}

/**
 * Removes items from the player's inventory when they don't stack.
 * @param *c Client instance.
 * @param item_id Item to remove.
 * @param amount How many of the item to remove.
 */
void delete_item_from_inv_nonstack(struct client *c, int item_id, int amount)
{
	int x;
	int num_had = 0;
	int left = amount;

	/*
	printf("ID: %d AMOUNT: %d\n", item_id, amount);
	*/

	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] == item_id)
		{
			num_had++;
		}
	}

	if (num_had >= amount)
	{
		for (x = 0; x < INV_SIZE; x++)
		{
			if (c->plr->items[x] == item_id)
			{
				c->plr->items[x] = -1;
				c->plr->items_n[x] = 0;
				c->plr->items_charges[x] = -1;
				left--;
				
				if (left == 0)
				{
					break;
				}
			}
		}
	}
	else
	{
		printf("Player does not have enough of the items!\n");
	}
	
	/*
	printf("Amount: %d Left: %d\n", amount, left);
	*/
	packet_reload_items_in_inventory_interface(c);
	packet_send_total_carried_weight(c);
}

/**
 * Removes an item from a player's inventory, from a specific slot.
 * @param *c Client instance.
 * @param slot What inventory slot the item is in.
 * @param amount How many of the item to remove.
 */
void delete_item_from_inv_slot(struct client *c, int slot, int amount)
{
	if ((slot < 0) || (slot >= INV_SIZE)) 
	{
		return;
	}

	if (is_item_id_invalid(c->plr->items[slot]))
	{
		return;
	}

	if (ITEMS[c->plr->items[slot]]->stacking == 1 || amount == 1)
	{
		if (c->plr->items_n[slot] > amount)
		{
			c->plr->items_n[slot] -= amount;
		}
		else
		{
			c->plr->items[slot] = -1;
			c->plr->items_n[slot] = 0;
			c->plr->items_charges[slot] = -1;
		}

		packet_reload_items_in_inventory_interface(c);
		packet_send_total_carried_weight(c);
	}
	else
	{
		delete_item_from_inv_nonstack(c, (c->plr->items[slot]), amount);
	}
}

/**
 * Removes an item from a player's equipment, from a specific slot.
 * @param *c Client instance.
 * @param slot What equipment slot the item is in.
 * @param amount How many of the item to remove.
 */
void delete_item_from_equipment_slot(struct client *c, int slot, int amount)
{
	if ((slot > -1) && (slot < EQUIP_SIZE))
	{
		if (c->plr->equipment_n[slot] > amount)
		{
			c->plr->equipment_n[slot] -= amount;
		}
		else
		{
			c->plr->equipment[slot] = -1;
			c->plr->equipment_n[slot] = 0;
			c->plr->equipment_charges[slot] = -1;
		}

		packet_set_item_in_interface(c, INTR_EQUIP_PLR_ITEMS, slot, 
						   c->plr->equipment[slot], 
						   c->plr->equipment_n[slot]);
		packet_send_total_carried_weight(c);
	}
}

/**
 * Moves an item from a slot in an interface to another.
 * @param *c Client instance.
 * @param from What item slot the item starts in.
 * @param to What item slot the item should be placed into.
 * @param interface_id What interface the items are in.
 */
void move_item(struct client *c, int from, int to, int interface_id)
{
	int temp_i = 0, temp_n = 0, temp_c = 0;
	int x = 0, first_blank_slot = -1;

	/* If the interface the items are moving in is player inventory.  */
	if (interface_id == INTR_INV_PLR_ITEMS)
	{
		temp_i = c->plr->items[from];
		temp_n = c->plr->items_n[from];
		temp_c = c->plr->items_charges[from];

		c->plr->items[from] = c->plr->items[to];
		c->plr->items_n[from] = c->plr->items_n[to];
		c->plr->items_charges[from] = c->plr->items_charges[to];
		c->plr->items[to] = temp_i;
		c->plr->items_n[to] = temp_n;
		c->plr->items_charges[to] = temp_c;

		packet_reload_items_in_inventory_interface(c);
	}
	/* If the interface the items are moving in is player bank.  */
	if (interface_id == INTR_BANK_ITEMS)
	{
		/* Use the swap mode if the items to move are right next to each other,
		 * if the "to" slot is empty, or if the use swap mode is set.  */
		if (c->plr->bank_is_insert_mode == 0 || c->plr->bank_items[to] == -1
			|| (((from + 1) == to) || ((from - 1) == to)))
		{
			temp_i = c->plr->bank_items[from];
			temp_n = c->plr->bank_items_n[from];
			temp_c = c->plr->bank_items_charges[from];

			c->plr->bank_items[from] = c->plr->bank_items[to];
			c->plr->bank_items_n[from] = c->plr->bank_items_n[to];
			c->plr->bank_items_charges[from] = c->plr->bank_items_charges[to];
			c->plr->bank_items[to] = temp_i;
			c->plr->bank_items_n[to] = temp_n;
			c->plr->bank_items_charges[to] = temp_c;
		}
		else if (c->plr->bank_is_insert_mode == 1)
		{
			temp_i = c->plr->bank_items[from];
			temp_n = c->plr->bank_items_n[from];
			temp_c = c->plr->bank_items_charges[from];

			/* Moving an item from left to right.  */
			if (from < to)
			{
				/* Find first blank slot after the insertion place.  */
				for (x = to; x >= 0; x--)
				{
					/* If slot is empty.  */
					if (c->plr->bank_items[x] == -1 || x == from)
					{
						first_blank_slot = x;
						break;
					}
				}

				if (first_blank_slot == -1)
				{
					packet_send_chatbox_message(c, "Your bank is too full to insert that item!");
					packet_reload_items_in_bank_interface(c);
					return;
				}

				/* Shift all items over one slot.  */
				for (x = first_blank_slot; x < to; x++)
				{
					if (from == (x - 1))
					{
						from = x;
					}

					c->plr->bank_items[x] = c->plr->bank_items[x + 1];
					c->plr->bank_items_n[x] = c->plr->bank_items_n[x + 1];
					c->plr->bank_items_charges[x] 
						= c->plr->bank_items_charges[x + 1];
				}

				c->plr->bank_items[to] = temp_i;
				c->plr->bank_items_n[to] = temp_n;
				c->plr->bank_items_charges[to] = temp_c;
			}
			/* Moving an item from right to left.  */
			else
			{
				/* Find first blank slot after the insertion place.  */
				for (x = to; x < PLAYER_BANK_SIZE; x++)
				{
					/* If slot is empty.  */
					if (c->plr->bank_items[x] == -1 || x == from)
					{
						first_blank_slot = x;
						break;
					}
				}
				
				if (first_blank_slot == -1)
				{
					packet_send_chatbox_message(c, "Your bank is too full to insert that item!");
					packet_reload_items_in_bank_interface(c);
					return;
				}

				/* Shift all items over one slot.  */
				for (x = first_blank_slot; x > to; x--)
				{
					if (from == (x - 1))
					{
						from = x;
					}

					c->plr->bank_items[x] = c->plr->bank_items[x - 1];
					c->plr->bank_items_n[x] = c->plr->bank_items_n[x - 1];
					c->plr->bank_items_charges[x] 
						= c->plr->bank_items_charges[x - 1];
				}

				c->plr->bank_items[to] = temp_i;
				c->plr->bank_items_n[to] = temp_n;
				c->plr->bank_items_charges[to] = temp_c;
			}
		}

		packet_reload_items_in_bank_interface(c);
	}
	/* If the interface the items are moving in is player's inventory, when
	 * banking.  */
	if (interface_id == INTR_BANK_PLR_ITEMS)
	{
		temp_i = c->plr->items[from];
		temp_n = c->plr->items_n[from];
		temp_c = c->plr->items_charges[from];

		c->plr->items[from] = c->plr->items[to];
		c->plr->items_n[from] = c->plr->items_n[to];
		c->plr->items_charges[from] = c->plr->items_charges[to];
		c->plr->items[to] = temp_i;
		c->plr->items_n[to] = temp_n;
		c->plr->items_charges[to] = temp_c;

		/* Refresh both, as the inventory will change based on changes made
		 * when banking.  */
		packet_reload_items_in_inventory_interface(c);
		packet_reload_items_in_bank_sidebar_interface(c);
	}
}

/**
 * Changes an item in a player's inventory to another.
 * @param *c Client instance.
 * @param slot Where the item is.
 * @param id_to What the item transforms to.
 */
void transform_item(struct client *c, int slot, int id_to)
{
	c->plr->items[slot] = id_to;
	packet_reload_items_in_inventory_interface(c);
}

/**
 * Changes an item in a player's inventory to another, but doesn't 
 * automatically refresh the inventory. Used for loops where only after the
 * last transform the inventory should be refreshed.
 * @param *c Client instance.
 * @param slot Where the item is.
 * @param id_to What the item transforms to.
 */
void transform_item_no_refresh(struct client *c, int slot, int id_to)
{
	c->plr->items[slot] = id_to;
}

/**
 * Fills each item in the player's inventory with water on each run.
 * @param *c Client instance.
 * @param *pos_store Stores the position to start the loop at, save cpu time.
 * @return Returns 0 when no fillable object was found, 1 when was able to
 * fill something.
 */
int fill_water_containers(struct client *c, int *pos_store)
{
	int x = 0;
	int item_id = 0;
	int to_id = 0;
	int found_one = 0;

	for (x = *pos_store; x < INV_SIZE; x++)
	{
		item_id = c->plr->items[x];
		to_id = get_container_water_id(item_id);

		if (to_id != -1)
		{
			transform_item(c, x, to_id);
			*pos_store = x;
			found_one = 1;
			break;
		}
	}

	return found_one;
}

static int get_potion_id_from_doses(int doses, int base_id)
{
	switch (doses)
	{
		case 4:
			return base_id;
		case 3:
			return ITEMS[base_id]->item_consume_id;
		case 2:
			return ITEMS[ITEMS[base_id]->item_consume_id]->item_consume_id;
		case 1:
			return ITEMS[ITEMS[ITEMS[base_id]->item_consume_id]->item_consume_id]->item_consume_id;
		case 0:
			return ITEMS[base_id]->item_container_id;

		default:
			return -1;
	}
}

/**
 * Handles when the player uses a potion on another. This processes combining
 * doses if it can be done.
 * @param *c Client instance.
 * @param first_potion Id of the first potion clicked.
 * @param first_potion_slot Inventory slot that the first potion is in.
 * @param second_potion Id of the second potion clicked.
 * @param second_potion_slot Inventory slot that the second potion is in.
 */
void decant_potions(struct client *c, int first_potion, int first_potion_slot, 
					int second_potion, int second_potion_slot)
{
	int first_base_id = ITEMS[first_potion]->base_id;
	int second_base_id = ITEMS[second_potion]->base_id;
	int first_uses = get_item_uses(first_potion);
	int second_uses = get_item_uses(second_potion);
	int total_doses = 0;
	
	int finished_first_doses = 0;
	int finished_second_doses = 0;

	int finished_first_potion = 0;
	int finished_second_potion = 0;

	/* Make sure the two potions are of the same type.  */
	if (first_base_id != second_base_id)
	{
		return;
	}

	/* If either of potions are full, return.  */
	if (first_potion == first_base_id || second_potion == second_base_id)
	{
		return;
	}

	/* Figure out the split of doses between the two potions.  */
	total_doses = (first_uses + second_uses);	

	if (total_doses > 4)
	{
		finished_first_doses = 4;
		finished_second_doses = (total_doses - 4);
	}
	else
	{
		finished_first_doses = total_doses;
		finished_second_doses = 0;
	}

	/* Get the resulting potion ids.  */
	finished_first_potion 
		= get_potion_id_from_doses(finished_first_doses, first_base_id);
	finished_second_potion 
		= get_potion_id_from_doses(finished_second_doses, second_base_id);

	if (finished_first_potion == -1)
	{
		printf("FIRST: get_potion_id_from_doses returned -1!\n");
		printf("doses: %d base: %d result: %d\n", finished_first_doses, 
			   first_base_id, finished_first_potion);
		return;
	}
	if (finished_second_potion == -1)
	{
		printf("SECOND: get_potion_id_from_doses returned -1!\n");
		printf("doses: %d base: %d result: %d\n", finished_second_doses, 
			   second_base_id, finished_second_potion);
	   return;
	}

	/*
	printf("result:\n");
	printf("From %d with %d doses used with %d with %d doses.\n", 
		   first_potion, first_uses, second_potion, second_uses);
	printf("To %d with %d doses used with %d with %d doses.\n",
		   finished_first_potion, get_item_uses(finished_first_potion), 
		   finished_second_potion, get_item_uses(finished_second_potion));
	*/

	transform_item(c, first_potion_slot, finished_first_potion);	
	transform_item(c, second_potion_slot, finished_second_potion);	

	player_play_animation_no_overwrite(c, HERBLORE_EMOTE_MIXING, 3);
	packet_send_chatbox_message(c, "You decant the potions.");
}

/**
 * Removes a specified number of charges from an item in a specific inventory
 * slot.
 * @param *c Client instance.
 * @param slot Inventory slot of the targeted item.
 * @param charges Number of charges to remove.
 */
void remove_inventory_item_charges(struct client *c, int slot, int charges)
{
	/* Items in the inventory are stored as one id # higher than they actually
	   are.  */
	int item_id = c->plr->items[slot];
	
	c->plr->items_charges[slot] -= charges;
	
	if (c->plr->items_charges[slot] <= 0)
	{
		/* If the item is one that stacks, remove one and reset charges.  */
		if (ITEMS[item_id]->stacking == 1)
		{
			/*
			printf("Stacking remove.\n");
			*/
			delete_item_from_inv_slot(c, slot, 1);
			
			/* If there is still something there, reset charges.  */
			if (c->plr->items_n[slot] > 0)
			{
				/*
				printf("	Reseting charges.\n");
				*/
				c->plr->items_charges[slot] = get_item_default_charges(item_id);
			}
		}
		/* TODO: Crystal bows wil use this, but need to be handled 
		   specially.  */
	}
}

/**
 * Removes a specified number of charges from an item in a specific inventory
 * slot.
 * @param *c Client instance.
 * @param slot Inventory slot of the targeted item.
 * @param charges Number of charges to remove.
 */
void remove_equipment_item_charges(struct client *c, int slot, int charges)
{
	int item_id = c->plr->equipment[slot];
	
	c->plr->equipment_charges[slot] -= charges;
	
	if (c->plr->equipment_charges[slot] <= 0)
	{
		/* If the item is one that stacks, remove one and reset charges.  */
		if (ITEMS[item_id]->stacking == 1)
		{
			/*
			printf("Stacking remove.\n");
			*/
			delete_item_from_equipment_slot(c, slot, 1);
			
			/* If there is still something there, reset charges.  */
			if (c->plr->items_n[slot] > 0)
			{
				/*
				printf("	Reseting charges.\n");
				*/
				c->plr->equipment_charges[slot] = get_item_default_charges(item_id);
			}
		}
		else
		{
			apply_equipment_info(c, -1, AMULET_SLOT, 0, -1);
		}
		/* TODO: Crystal bows wil use this, but need to be handled 
		   specially.  */
	}
}

/**
 * Attempts to equip an item in the player's inventory to the respective
 * equipment slot.
 * @param *c Client instance.
 * @param item_id Item id to equip.
 * @param slot Slot where the item is located.
 * @return Returns 0 if equiped properly, anything else otherwise.
 */
int equip_item(struct client *c, int item_id, int slot)
{
	int target_slot = 0, i = 0, amount = 0, level_msg = 0, free_slots = 0;
	int charges = 0;
	int item_slot = player_has_item_slot(c, item_id, 1, slot);
	char level_string[100] = {0};
	
	if (item_slot == -1)
	{
		printf("Player %s tried to equip a non exisitant item!\n", 
			   c->plr->name);
		return -1;
	}

	amount = c->plr->items_n[slot];
	charges = c->plr->items_charges[slot];	
	target_slot = ITEMS[item_id]->slot;
	free_slots = get_free_inv_slots(c);

	/* If item is a 2h sword, make sure inventory has enough space for other
	   weapon, shield.  */
	if ((ITEMS[item_id]->two_handed == 1) 
		&& (c->plr->equipment[SHIELD_SLOT] > 0))
	{
		/* Handles moving items to inventory when equipping a 
		   two-handed weapon. */

		/* If there isn't a weapon equipped.  */
		if (c->plr->equipment[WEAPON_SLOT] == -1)
		{
			if (free_slots > 0)
			{
				add_item_to_inv(c, c->plr->equipment[SHIELD_SLOT], 1, 
								c->plr->equipment_charges[SHIELD_SLOT]);
				apply_equipment_info(c, -1, SHIELD_SLOT, 0, -1);
			}
			else
			{
				packet_send_chatbox_message(c, "Not enough space in your inventory.");
				return 1;
			}
		}
		/* If there is a weapon equipped.  */
		else
		{
			/* Are there at least two inventory slots free for the weapon,
			   and the shield?  */
			if (free_slots > 1)
			{
				add_item_to_inv(c, c->plr->equipment[SHIELD_SLOT], 1,
								c->plr->equipment_charges[SHIELD_SLOT]);
				add_item_to_inv(c, c->plr->equipment[WEAPON_SLOT], 
								   c->plr->equipment_n[WEAPON_SLOT],
								   c->plr->equipment_charges[WEAPON_SLOT]);
				apply_equipment_info(c, -1, SHIELD_SLOT, 0, -1);
			}
			else
			{
				packet_send_chatbox_message(c, "Not enough space in your inventory.");
				return 1;
			}
		}
	}
			
	if ((target_slot == SHIELD_SLOT)
		&& c->plr->equipment[WEAPON_SLOT] != -1) 
	{
		if (ITEMS[c->plr->equipment[WEAPON_SLOT]]->two_handed == 1)
		{
			if (free_slots > 0)
			{
				add_item_to_inv(c, c->plr->equipment[WEAPON_SLOT], 
								   c->plr->equipment_n[WEAPON_SLOT],
								   c->plr->equipment_charges[WEAPON_SLOT]);
				
				apply_equipment_info(c, -1, WEAPON_SLOT, 0, -1);
			}
			else
			{
				packet_send_chatbox_message(c, "Not enough space in your inventory.");
				return 1;
			}
		}
	}
	/* Check if skills are too low to wear.  */
	for (i = 0; i < PROTOCOL_NUM_SKILLS; i++)
	{
		if (ITEMS[item_id]->skill_req[i] > get_level_for_xp(c->plr->xp[i]))
		{
			if (level_msg == 0)
			{
				packet_send_chatbox_message(c, "You are not a high enough level to use this item.");
				level_msg++;
			}
			if (i == ATTACK_SKILL || i == AGILITY_SKILL)
			{
				snprintf(level_string, 100, "You need to have an %s level of %d.",
						SKILL_NAMES[i], ITEMS[item_id]->skill_req[i]);
			}	
			else
			{
				snprintf(level_string, 100, "You need to have a %s level of %d.",
						SKILL_NAMES[i], ITEMS[item_id]->skill_req[i]);
			}
			packet_send_chatbox_message(c, level_string);
		}
	}
	if (level_msg > 0)
	{
		return 1;
	}

	/* Gets how many of the item is there.  */
	if (amount < 1)
	{
		printf("Trying to equip too low of an amount!\n");
		return 1;
	}
	
	/* Remove the item being equipped from the inventory.  */
	delete_item_from_inv_slot(c, slot, amount);

	/* Check if something already exists in the equipment slot, if so,
	 * handle it.  */
	if ((slot >= 0) && (item_id >= 0))
	{
		/* If item to equip is able to stack, and there are already multiples
		   of it equipped, add the inventory amount to what's equipt.  */
		if ((ITEMS[item_id]->stacking == 1)
				 && c->plr->equipment[target_slot] == item_id)
		{
			/* Increase the number of that item by how many was in the 
			 * inventory space.  */
			amount = c->plr->equipment_n[target_slot] + amount;
		}
		else
		{
			add_item_to_inv(c, c->plr->equipment[target_slot],
							c->plr->equipment_n[target_slot],
							c->plr->equipment_charges[target_slot]);
		}
	}
	
	/* Equip the item, delete it from the inventory.  */
	apply_equipment_info(c, item_id, target_slot, amount, charges);

	/* If item equipped is a weapon.  */
	if (target_slot == WEAPON_SLOT)
	{
		/* Reset all variables needed on weapon change.  */
		c->plr->autocasting = 0;
		c->plr->autocast_spell_id = 0;
		packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 0);
	}

	packet_player_update_viewport_equipment_bonuses(c);

	/* Update weights in case they equipped an item that reduces total weight
	 * when worn.  */
	packet_send_total_carried_weight(c);
	
	c->plr->update_required = 1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	return 0;
}

/**
 * Attempts to unequip an item and return it to the player's inventory.
 * @param *c Client instance.
 * @param item_id Item id to unequip.
 * @param slot Slot where the item is located.
 */
void unequip_item(struct client *c, int item_id, int slot)
{
	int item_slot = player_has_item_equip(c, item_id, 1);
	
	if (item_slot == -1)
	{
		printf("Player %s tried to unequip a non existent item!\n", 
			   c->plr->name);
		return;
	}

	if (item_id >= 0)
	{
		packet_close_all_open_interfaces(c);
	}

	/* If there is space in inventory for the equipment.  */
	if (add_item_to_inv(c, c->plr->equipment[slot], c->plr->equipment_n[slot], 
						c->plr->equipment_charges[slot]))
	{
		c->plr->equipment[slot] = -1;
		c->plr->equipment_n[slot] = 0;
		c->plr->equipment_charges[slot] = -1;

		packet_player_update_viewport_equipment_bonuses(c);

		packet_set_item_in_interface(c, INTR_EQUIP_PLR_ITEMS, slot, -1, 0);
		
		/* If it was a weapon.  */
		if (slot == WEAPON_SLOT)
		{
			apply_weapon_info(c, -1);
		}
		else if (slot == SHIELD_SLOT)
		{
			if (c->plr->equipment[WEAPON_SLOT] != -1)
			{
				c->plr->blocking_emote 
					= ITEMS[c->plr->equipment[WEAPON_SLOT]]->block_anim;
			}
			else
			{
				c->plr->blocking_emote = 404;
			}
		}
		else if (slot == HAT_SLOT)
		{
			/* Reset runecrafting tiara config.  */
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 0);
		}

		/* Update weights in case they removed an item that reduces total
		 * weight when worn.  */
		packet_send_total_carried_weight(c);
		c->plr->update_masks[APPEARANCE_UPDATE] = 1;
		c->plr->update_required = 1;
	}
}
	
/**
 * Removes all items from the player's inventory and refreshes the interface.
 * @param *c Client instance.
 */
void empty_player_inventory(struct client *c)
{
	int x;

	for (x = 0; x < INV_SIZE; x++)
	{
		c->plr->items[x] = -1;
		c->plr->items_n[x] = 0;
		c->plr->items_charges[x] = -1;
	}

	packet_reload_items_in_inventory_interface(c);
}

/**
 * Removes all items from the player's equipment and refreshes the interface.
 * @param *c Client instance.
 */
void empty_player_equipment(struct client *c)
{
	int x;

	for (x = 0; x < EQUIP_SIZE; x++)
	{
		apply_equipment_info(c, -1, x, 0, -1);
	}
}

/**
 * Sets any configuration values for equipment if they exist. Only for rune
 * tiaras at the moment.
 * @param *c Client instance to check equipment, set configs for.
 */
void set_special_equipment_configs(struct client *c)
{
	/* Head items.  */
	switch (c->plr->equipment[HAT_SLOT])
	{
	/* Wearing tiara, affects runecrafting ruins.  */
		/* Air tiara.  */
		case 5527:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 1);
			break;
		/* Mind tiara.  */
		case 5529:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 2);
			break;
		/* Water tiara.  */
		case 5531:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 4);
			break;
		/* Earth tiara.  */
		case 5535:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 8);
			break;
		/* Fire tiara.  */
		case 5537:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 16);
			break;
		/* Body tiara.  */
		case 5533:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 32);
			break;
		/* Cosmic tiara.  */
		case 5539:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 64);
			break;
	/* For some reason, these all use the same config.  */
		/* Chaos tiara.  */
		case 5543:
		/* Nature tiara.  */
		case 5541:
		/* Law tiara.  */
		case 5545:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 128);
			break;
		default:
			packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 0);
			break;
	}
}

/**
 * Equips a specified item to an equipment slot, and sets up all variables
 * needed.
 * @param *c Client instance.
 * @param item_id Item to equip.
 * @param slot Equipment slot to place the item in.
 * @param amount Number of items to equip.
 * @param charges Number of charges the item has.
 */
void apply_equipment_info(struct client *c, int item_id, int slot, int amount, 
				   int charges)
{
	c->plr->equipment[slot] = item_id;
	c->plr->equipment_n[slot] = amount;
	c->plr->equipment_charges[slot] = charges;
	create_frame_vsize_word(c, SERVER_PKT_SET_INTERFACE_ITEM);
	write_word(c, INTR_EQUIP_PLR_ITEMS);
	write_byte(c, slot);
	write_word(c, (item_id + 1));

	/* If there is more than can be expressed in one byte, extend it.  */
	if (amount > 254)
	{
		write_byte(c, 255);
		write_dword(c, amount);
	}
	else
	{
		write_byte(c, amount);
	}
	end_frame_vsize_word(c);

	if (slot == WEAPON_SLOT)
	{
		/* Removing weapon configs needed.  */
		packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 0);
		packet_set_client_config_value(c, RUNECRAFTING_RUIN_TIARA, 128);
		c->plr->autocasting = 0;
		c->plr->autocast_spell_id = 0;

		apply_weapon_info(c, item_id);

		/* Reset attack interval so the player can't swap weapons right before
		 * they hit.  */
		c->plr->last_attack = c->plr->attack_speed;
	}
	/* Sets blocking animation if shield is worn.  */
	else if ((slot == SHIELD_SLOT))
	{
		c->plr->blocking_emote = 1156;
	}

	/* If any equipment needs a special config value to be set, do it here.  */
	set_special_equipment_configs(c);

	c->plr->update_required = 1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
}

/**
 * Handles all the information that needs to be sent to the client when
 * a weapon is equipped.
 * @param *c Client instance.
 * @param item_id ID of the weapon.
 */
void apply_weapon_info(struct client *c, int item_id)
{
	/* Apply defaults for moving emotes.  */
	c->plr->standing_emote = 808;
	c->plr->walking_emote = 819;
	c->plr->running_emote = 824;
	c->plr->blocking_emote = 404;

	/* If no weapon equipped.  */
	if (item_id == -1)
	{
		packet_show_sidebar_interface(c, 0, 5855);
		packet_set_interface_text(c, "Unarmed", 5857);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_CRUSH;
		c->plr->attack_speed = 4;
		
		c->plr->attacking_emote = 422;
	}
	/* Whip type weapon.  */
	else if (ITEMS[item_id]->atk_type == 12290)
	{
		packet_show_sidebar_interface(c, 0, 12290);
		packet_set_item_in_interface_model(c, 12291, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 12293);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Scythe type weapon.  */
	else if (ITEMS[item_id]->atk_type == 776)
	{
		packet_show_sidebar_interface(c, 0, 776);
		packet_set_item_in_interface_model(c, 777, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 779);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Crossbow type weapon.  */
	else if (ITEMS[item_id]->atk_type == 1749)
	{
		packet_show_sidebar_interface(c, 0, 1749);
		packet_set_item_in_interface_model(c, 1750, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 1752);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE_RANGE;
		c->plr->damage_type = DMG_TYPE_RANGE;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Bow type weapon.  */
	else if (ITEMS[item_id]->atk_type == 1764)
	{
		packet_show_sidebar_interface(c, 0, 1764);
		packet_set_item_in_interface_model(c, 1765, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 1767);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE_RANGE;
		c->plr->damage_type = DMG_TYPE_RANGE;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Staff type weapon.  */
	else if (ITEMS[item_id]->atk_type == 328)
	{
		packet_show_sidebar_interface(c, 0, 328);
		packet_set_item_in_interface_model(c, 329, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 331);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_CRUSH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Dagger type weapon.  */
	else if (ITEMS[item_id]->atk_type == 2276)
	{
		packet_show_sidebar_interface(c, 0, 2276);
		packet_set_item_in_interface_model(c, 2277, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 2279);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_STAB;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Pickaxe type weapon.  */
	else if (ITEMS[item_id]->atk_type == 5570)
	{
		packet_show_sidebar_interface(c, 0, 5570);
		packet_set_item_in_interface_model(c, 5571, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 5573);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_STAB;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Warhammer type weapon.  */
	else if (ITEMS[item_id]->atk_type == 425)
	{
		packet_show_sidebar_interface(c, 0, 425);
		packet_set_item_in_interface_model(c, 426, 200, item_id); 
		packet_set_interface_text(c, ITEMS[item_id]->name, 428);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_CRUSH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Mace type weapon.  */
	else if (ITEMS[item_id]->atk_type == 3796)
	{
		packet_show_sidebar_interface(c, 0, 3796);
		packet_set_item_in_interface_model(c, 3797, 200, item_id); 
		packet_set_interface_text(c, ITEMS[item_id]->name, 3799);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_CRUSH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Axe type weapon.  */
	else if (ITEMS[item_id]->atk_type == 1698)
	{
		packet_show_sidebar_interface(c, 0, 1698);
		packet_set_item_in_interface_model(c, 1699, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 1701);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Throwing weapon.  */
	else if (ITEMS[item_id]->atk_type == 4446)
	{
		packet_show_sidebar_interface(c, 0, 4446);
		packet_set_item_in_interface_model(c, 4447, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 4449);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE_RANGE;
		c->plr->damage_type = DMG_TYPE_RANGE;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Sword type weapon.  */	
	else if (ITEMS[item_id]->atk_type == 2423)
	{
		packet_show_sidebar_interface(c, 0, 2423);
		packet_set_item_in_interface_model(c, 2424, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 2426);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Spear type weapon.  */
	else if (ITEMS[item_id]->atk_type == 4679)
	{
		packet_show_sidebar_interface(c, 0, 4679);
		packet_set_item_in_interface_model(c, 4680, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 4682);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = CONTROLLED_STYLE;
		c->plr->damage_type = DMG_TYPE_STAB;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Halberd type weapons.  */
	else if (ITEMS[item_id]->atk_type == 8460)
	{
		packet_show_sidebar_interface(c, 0, 8460);
		packet_set_item_in_interface_model(c, 8461, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 8463);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = CONTROLLED_STYLE;
		c->plr->damage_type = DMG_TYPE_STAB;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	} /* Two-handed sword type weapons.  */
	else if (ITEMS[item_id]->atk_type == 4705)
	{
		packet_show_sidebar_interface(c, 0, 4705);
		packet_set_item_in_interface_model(c, 4706, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 4708);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* Claw type weapons.  */
	else if (ITEMS[item_id]->atk_type == 7762)
	{
		packet_show_sidebar_interface(c, 0, 7762);
		packet_set_item_in_interface_model(c, 7763, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 7765);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}
	/* FIXME: Not complete yet!  */
	/* Only used for the Fixed Device.  */
	else if (ITEMS[item_id]->atk_type == 13975)
	{
		packet_show_sidebar_interface(c, 0, 13975);
		packet_set_item_in_interface_model(c, 13981, 200, item_id);
		packet_set_interface_text(c, ITEMS[item_id]->name, 13977);
		packet_set_client_config_value(c, COMBAT_ATTACK_MODE, 0);
		c->plr->fighting_style = ACCURATE_STYLE;
		c->plr->damage_type = DMG_TYPE_SLASH;
		c->plr->attack_speed = ITEMS[item_id]->atk_speed;
		c->plr->attacking_emote = ITEMS[item_id]->attacking_emotes[0];
		c->plr->blocking_emote = ITEMS[item_id]->block_anim;
	}

	/* Correctly sets the blocking animation to use the shield's instead
	   of the weapon's if a shield is worn.  */
	if (c->plr->equipment[SHIELD_SLOT] != -1)
	{
		c->plr->blocking_emote 
			= ITEMS[c->plr->equipment[SHIELD_SLOT]]->block_anim;
	}

	if (item_id != -1)
	{
		c->plr->standing_emote = ITEMS[item_id]->stand_anim;
		
		/* Changing all of the turning emotes to the walking one for the
		   weapon equipped ensures that turning looks smooth and not
		   jerking from one animation to another.  */
		c->plr->turn_emote = c->plr->turn_about_emote 
		= c->plr->turn_cw_emote = c->plr->turn_ccw_emote 
		= c->plr->walking_emote = ITEMS[item_id]->walk_anim;
		
		c->plr->running_emote = ITEMS[item_id]->run_anim;
	}
	else
	{
		/* Default standing emote.  */
		c->plr->standing_emote = 808;
		
		/* Changing all of the turning emotes to the walking one for the
		   weapon equipped ensures that turning looks smooth and not
		   jerking from one animation to another.  */
		c->plr->turn_emote = c->plr->turn_about_emote 
			= c->plr->turn_cw_emote = c->plr->turn_ccw_emote 
			= c->plr->walking_emote = 819;
		
		c->plr->running_emote = 824;
	}

	c->plr->update_masks[ANIMATION_REQ] = 1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	c->plr->update_required = 1;
}

/**
 * Attempts to spawn an item on the ground for the client.
 * @param *c Client instance.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void spawn_item_on_ground(struct client *c, int drop_id)
{
	/* If a player is within 60 squares of an item, it will spawn it on their
	   client. It also ensures that it wont spawn items that have been 
	   picked up. It will spawn items on different levels than the player, but
	   the player won't see them until they are on the same level.  */
	if (get_distance_from_point(c->plr->world_x,
		c->plr->world_y, 
		ITEM_DROPS[drop_id]->world_x, 
		ITEM_DROPS[drop_id]->world_y) <= 60
		&& c->plr->world_z == ITEM_DROPS[drop_id]->world_z
		&& c->plr->items_on_ground[drop_id] == 0)
	{
		/* Item's respawn counter has hit 0, respawn it.  */
		if (ITEM_DROPS[drop_id]->tick_picked_up == -1
			|| ITEM_DROPS[drop_id]->tick_picked_up == 0)
		{
			packet_draw_item_in_world(c, drop_id);
			c->plr->items_on_ground[drop_id] = 1;
			ITEM_DROPS[drop_id]->tick_picked_up = -1;
		}
	}
}

/**
 * Attempts to despawn an item on the ground for the client.
 * @param *c Client instance.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void despawn_item_on_ground(struct client *c, int drop_id)
{
	/* Handling for removing items from a client if he moved location. Do not
	 * check for z level matching here, as that can cause duplicate items when
	 * the player keeps jumping from z level to z level.  */
	if ((get_distance_from_point(c->plr->world_x,
		c->plr->world_y, 
		ITEM_DROPS[drop_id]->world_x, 
		ITEM_DROPS[drop_id]->world_y) > 60
		|| ITEM_DROPS[drop_id]->tick_dropped == 0)
		&& c->plr->items_on_ground[drop_id] == 1)
	{
		if (ITEM_DROPS[drop_id]->world_x != 0)
		{
			packet_erase_item_from_world(c, drop_id);
			c->plr->items_on_ground[drop_id] = 0;
		}
	}
}

/**
 * Applies default settings to an item_drop struct.
 * @param *i Item drop struct.
 */
void apply_item_drop_defaults(struct item_drop *i)
{
	i->world_x = i->world_y = 0;
	i->timer = 0;
	i->tick_picked_up = -1;
	i->tick_dropped = -1;
	i->tick_spawn_delay = -1;
	i->player_index = -1;
}

/**
 * Attempts to spawn an item to everyone on the server.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void spawn_item_globally(int drop_id)
{
	int x;
	if (ITEM_DROPS[drop_id] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			spawn_item_on_ground(CONNECTED_CLIENTS[x], drop_id);
		}
	}
}

/**
 * Attempts to spawn an item to everyone on the server, except the "dropper"
 * of the item.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void spawn_item_globally_except_owner(int drop_id)
{
	int x;
	if (ITEM_DROPS[drop_id] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			if (x != ITEM_DROPS[drop_id]->player_index)
			{
				spawn_item_on_ground(CONNECTED_CLIENTS[x], drop_id);
			}
		}
	}
}

/**
 * Attempts to despawn an item for everyone on the server.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void despawn_item_globally(int drop_id)
{
	int x;
	if (ITEM_DROPS[drop_id] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			despawn_item_on_ground(CONNECTED_CLIENTS[x], drop_id);
		}
	}
	delete_from_item_drops(drop_id, 0);
}

/**
 * Removes an item drop from the client's view, and enables it to respawn
 * later.
 * @param drop_id Index of the drop in the ITEM_DROPS array.
 */
void reset_item_drop_globally(int drop_id)
{
	int x;
	if (ITEM_DROPS[drop_id] == NULL)
	{
		return;
	}
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			/* Ensures that previous versions of the item will not remain.  */
			packet_erase_item_from_world(CONNECTED_CLIENTS[x], drop_id);
			CONNECTED_CLIENTS[x]->plr->items_on_ground[drop_id] = 0;
		}
	}
}

/**
 * Creates a specific item drop at a specific location.
 * @param item_id ID of the item.
 * @param amount Number of that item to drop.
 * @param charges Number of charges the item has.
 * @param pos_x X world location.
 * @param pos_y Y world location.
 * @param height Z world location.
 * @param dropped If item is dropped, apply despawn timer.
 * @return Returns ITEM_DROPS index if succeeded, -1 if not.
 */
int add_to_item_drops(int item_id, int amount, int charges, 
					  int pos_x, int pos_y, int height, int dropped)
{
	int x;
	int existing_drop;

	if (((existing_drop = is_item_drop_in_world(item_id, pos_x, pos_y, 
													height)) 
		  != -1) && (ITEMS[item_id]->stacking != 0))
	{
		ITEM_DROPS[existing_drop]->amount += amount;
		ITEM_DROPS[existing_drop]->tick_dropped = LOOT_DESPAWN_TICKS;
		reset_item_drop_globally(existing_drop);
		return existing_drop;	
	}
	else
	{
		for (x = 0; x < MAX_ITEM_DROPS; x++)
		{
			/* If world_x is set to zero, there is nothing there.  */
			if (ITEM_DROPS[x]->world_x == 0)
			{
				ITEM_DROPS[x]->id = item_id;
				ITEM_DROPS[x]->amount = amount;
				ITEM_DROPS[x]->charges = charges;
				ITEM_DROPS[x]->world_x = pos_x;
				ITEM_DROPS[x]->world_y = pos_y;
				ITEM_DROPS[x]->world_z = height;
				ITEM_DROPS[x]->timer = 0;
				
				if (dropped == 1)
				{
					ITEM_DROPS[x]->tick_dropped = LOOT_DESPAWN_TICKS;
				}
				else
				{
					ITEM_DROPS[x]->tick_dropped = -1;
				}
				return x;
			}
		}
	}
	return -1;
}

/**
 * Creates a specific item drop at a specific location. At first, the item is
 * only visible to the client passed as one of the arguments.
 * @param *c Client to only show the item to initially.
 * @param item_id ID of the item.
 * @param amount Number of that item to drop.
 * @param charges Number of charges the item has.
 * @param pos_x X world location.
 * @param pos_y Y world location.
 * @param height Z world location.
 * @param dropped If item is dropped, apply despawn timer.
 * @return Returns ITEM_DROPS index if succeeded, -1 if not.
 */
int add_to_item_drops_exclusive(struct client *c, int item_id, int amount, 
								int charges, int pos_x, int pos_y, int height,
								int dropped)
{
	int drop_id = add_to_item_drops(item_id, amount, charges, pos_x, pos_y, 
									height, dropped);

	if (c != NULL)
	{
		ITEM_DROPS[drop_id]->player_index = c->plr->index;
		spawn_item_on_ground(c, drop_id);
	}
	else
	{
		spawn_item_globally(drop_id);
	}

	return drop_id;
}

/**
 * Removes an item from the item drops.
 * @param item_index Index the item_drop struct is located in the ITEM_DROPS
 * array.
 * @param remove Whether or not to remove the item from the client views.
 */
void delete_from_item_drops(int item_index, int remove)
{
	/* Remove the item from the client's view.  */
	if (remove == 1)
	{
		int x;

		for (x = 0; x < MAX_PLAYERS; x++)
		{
			if (CONNECTED_CLIENTS[x] != NULL)
			{
				if (CONNECTED_CLIENTS[x]->plr->items_on_ground[item_index] == 1)
				{
					packet_erase_item_from_world(CONNECTED_CLIENTS[x], item_index);
					CONNECTED_CLIENTS[x]->plr->items_on_ground[item_index] = 0;
				}
			}
		}
	}
	if (ITEM_DROPS[item_index]->timer == 0)
	{
		ITEM_DROPS[item_index]->id = ITEM_DROPS[item_index]->amount = 
		ITEM_DROPS[item_index]->world_x = ITEM_DROPS[item_index]->world_y = 
		ITEM_DROPS[item_index]->world_z = ITEM_DROPS[item_index]->timer = 0;
		ITEM_DROPS[item_index]->charges = -1;
		ITEM_DROPS[item_index]->tick_picked_up = -1;
		ITEM_DROPS[item_index]->tick_dropped = -1;
	}
	else
	{
		ITEM_DROPS[item_index]->tick_picked_up = ITEM_DROPS[item_index]->timer;
	}
}

/**
 * Checks if an item drop that matches the arguments exists.
 * @param item_id Item ID to located.
 * @param pos_x, pos_y, pos_z World coordinates to check.
 * @return Returns -1 if not found, anything else is the ITEM_DROPS index it is
 * located.
 */
int is_item_drop_in_world(int item_id, int pos_x, int pos_y, int pos_z)
{
	int x;
	for (x = 0; x < MAX_ITEM_DROPS; x++)
	{
		if (ITEM_DROPS[x]->world_x != 0)
		{
			if (ITEM_DROPS[x]->id == item_id
				&& ITEM_DROPS[x]->world_x == pos_x
				&& ITEM_DROPS[x]->world_y == pos_y
				&& ITEM_DROPS[x]->world_z == pos_z)
			{
				return x;
			}
		}
	}
	return -1;
}

/**
 * Checks if the player is close enough to the item they are trying to pickup,
 * and adds it to their inventory if they can.
 * @param *c Client instance.
 */
void scan_for_pickup(struct client *c)
{
	int x = is_item_drop_in_world(c->plr->action_id, c->plr->action_x,
									  c->plr->action_y, c->plr->action_z);

	if (x == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Check if the item has despawned, first.  */
	if (ITEM_DROPS[x]->tick_picked_up != -1)
	{
		printf("Item has despawned!\n");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* If within distance of the item, pick it up.  */
	if (check_good_distance(c->plr->world_x, c->plr->world_y, 
		c->plr->action_x, c->plr->action_y, 1)
		&& (c->plr->walking_dir == -1 && c->plr->running_dir == -1))
	{
		if (add_item_to_inv(c, ITEM_DROPS[x]->id, ITEM_DROPS[x]->amount, 
							ITEM_DROPS[x]->charges) == 1)
		{
			delete_from_item_drops(x, 1);
			set_player_state(c, STATE_IDLE);
		}
		else
		{
			set_player_state(c, STATE_IDLE);
		}
	}

	return;
}

/**
 * Figures out total weight the player is carrying from their equipment, and
 * their inventory.
 * @param *c Client instance.
 * @return Returns total weight of all items held, float.
 */
float get_total_carried_weight(struct client *c)
{
	int x;
	/* reset carried weight.  */
	c->plr->weight = 0.0;

	for (x = 0; x < EQUIP_SIZE; x++)
	{
		if (!is_item_id_invalid(c->plr->equipment[x]))
		{
			/* Check if they are wearing boots of lightness, and if so, remove
			 * weight from them. This is the only item in this version of the
			 * game that works like this.  */
			if (c->plr->equipment[x] == 88 || c->plr->equipment[x] == 89)
			{
				c->plr->weight += -4.5;
			}
			else
			{
				c->plr->weight += 
					(ITEMS[c->plr->equipment[x]]->weight 
					 * c->plr->equipment_n[x]);
			}
		}
	}
	for (x = 0; x < INV_SIZE; x++)
	{
		if (!is_item_id_invalid(c->plr->items[x]))
		{
			c->plr->weight += (float) (ITEMS[c->plr->items[x]]->weight 
									   * c->plr->items_n[x]);
		}
	}

	return c->plr->weight;
}

/**
 * Takes what is in the ammo slot, and returns if its ID is in the
 * required range.
 * @param ammo_equip ID of the ammo item in the ammo equipment slot.
 * @param ammo_num Number of ammo_equip the player has.
 * @param min Minimum ID range to check for.
 * @param max Maximum ID range to check for.
 * @return Returns 1 on valid, 0 on invalid.
 */
int check_ammo_type(int ammo_equip, int ammo_num, int min, int max)
{
	if (min <= ammo_equip && ammo_equip <= max)
	{
		if (ammo_num > 0)
		{
			return 1;
		}
		return 0;
	}
	else
	{
		return 0;
	}
}

/**
 * Returns the inventory index of the first item it finds.
 * @param *c Client instance.
 * @param start Where in the inventory array to start the search.
 * @return Returns -1 on failure, anything else is the index of the first item
 * in the player's inventory.
 */
int player_first_item(struct client *c, int start)
{
	int x;
	
	if (start < 0 || start >= INV_SIZE)
	{
		return -1;
	}

	for (x = start; x < INV_SIZE; x++)
	{
		if (c->plr->items_n[x] != 0)
		{
			return x;
		}
	}
	return -1;
}

/**
 * Returns the equipment index of the first item it finds.
 * @param *c Client instance.
 * @param start Where in the equipment array to start the search.
 * @return Returns -1 on failure, anything else is the index of the first item
 * in the player's equipment.
 */
int player_first_equipment(struct client *c, int start)
{
	int x;

	if (start < 0 || start >= EQUIP_SIZE)
	{
		return -1;
	}

	for (x = start; x < EQUIP_SIZE; x++)
	{
		if (c->plr->equipment_n[x] != 0)
		{
			return x;
		}
	}
	return -1;
}

/**
 * Checks a player's inventory for the highest value item they have, ignoring
 * any that have already been taken.
 * @param *c Client instance.
 * @param slots_used[] Array holding ID's of items already checked for value.
 * @param index Size of the slots_used[] array.
 * @param *value Pointer to an int where the value of the item will be stored.
 * @return Returns slot for the found valuable item.
 */
int get_most_valuable_inv_item(struct client *c, int slots_used[], int index, 
							   int *value)
{
	int x;
	int y;
	int inv_store_slot = -1, inv_store_value = -1;
	int cont = 0;

	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] != -1
			&& c->plr->items_n[x] > 0)
		{
			if (inv_store_slot == -1
				|| ITEMS[c->plr->items[x]]->high_alch > inv_store_value)
			{
				/* Ensure that this slot is not one that has been already 
				 * taken.  */
				for (y = 0; y < index; y++)
				{
					if (x == slots_used[y])
					{
						cont = 1;
						break;
					}
				}

				if (cont == 1)
				{
					cont = 0;
					continue;
				}

				inv_store_slot = x;
				inv_store_value = ITEMS[c->plr->items[x]]->high_alch;
			}
		}
	}

	*value = inv_store_value;
	return inv_store_slot;
}

/**
 * Checks a player's equipment for the highest value item they have, ignoring
 * any that have already been taken.
 * @param *c Client instance.
 * @param slots_used[] Array holding ID's of items already checked for value.
 * @param index Size of the slots_used[] array.
 * @param *value Pointer to an int where the value of the item will be stored.
 * @return Returns slot for the found valuable item.
 */
int get_most_valuable_equipment_item(struct client *c, int slots_used[], 
									 int index, int *value)
{
	int x;
	int y;
	int equipment_store_slot = -1, equipment_store_value = -1;
	int cont = 0;

	for (x = 0; x < EQUIP_SIZE; x++)
	{
		if (c->plr->equipment[x] != -1
			&& c->plr->equipment_n[x] > 0)
		{
			if (equipment_store_slot == -1
				|| ITEMS[c->plr->equipment[x]]->high_alch > equipment_store_value)
			{
				/* Ensure that this slot is not one that has been already 
				 * taken.  */
				for (y = 0; y < index; y++)
				{
					if (x == slots_used[y])
					{
						cont = 1;
						break;
					}
				}

				if (cont == 1)
				{
					cont = 0;
					continue;
				}

				equipment_store_slot = x;
				equipment_store_value = ITEMS[c->plr->equipment[x]]->high_alch;
			}
		}
	}

	*value = equipment_store_value;
	return equipment_store_slot;
}

/** 
 * Prints out what the 3 most valuable items the player has are. Used for 
 * seeing what items are kept on death.
 * @note Value is based off of the individual item's value. Stack does not
 * matter, only one of the item's value is used.
 * @param *c Client instance.
 * @param items[] Array to store the item slots kept.
 * @param location[] Array that stores where the item is, located in equipment
 * or items.
 * @param num_items Number of items to be checked. Also size of the items and
 * location arrays.
 */
void get_most_valuable_items(struct client *c, int items[], int location[], 
							 int num_items)
{
	int x;
	int items_index = 0;
	int location_index = 0;
	
	int inv_store_slot = -1;
	int inv_store_value = -1;
	int inv_slots_used[INV_SIZE] = {-1};
	int inv_slots_index = 0;

	int equipment_store_slot = -1;
	int equipment_store_value = -1;
	int equipment_slots_used[EQUIP_SIZE] = {-1};
	int equipment_slots_index = 0;

	if (num_items > (INV_SIZE + EQUIP_SIZE))
	{
		printf("too much\n");
		return;
	}

	/* Find the most valuable item in inventory and equipment. Then compare
	 * which is worth more and add it to the list.  */
	for (x = 0; x < num_items; x++)
	{
		inv_store_slot = get_most_valuable_inv_item(c, inv_slots_used,
											   inv_slots_index,
											   &inv_store_value);

		equipment_store_slot
			= get_most_valuable_equipment_item(c, equipment_slots_used, 
										  equipment_slots_index, 
										  &equipment_store_value);
		
		if (inv_store_value > equipment_store_value)
		{
			items[items_index++] = inv_store_slot;
			location[location_index++] = 0;
			inv_slots_used[inv_slots_index++] = inv_store_slot;
		}
		else if (equipment_store_value >= inv_store_value)
		{
			items[items_index++] = equipment_store_slot;
			location[location_index++] = 1;
			equipment_slots_used[equipment_slots_index++] 
				= equipment_store_slot;
		}
	}

	return;
}

/**
 * Returns the number of items the player will keep on death.
 * @param *c Client instance.
 * @return Returns number of items kept.
 * @return -1 if keeping all.
 * @return anything else is the number to drop.
 */
int get_num_items_kept_on_death(struct client *c)
{
	int num_kept = 3;

	/* If player isn't in an area where there is risk of item loss, keep them.  */
	if (c->plr->in_unsafe_zone != 1)
	{
		return -1;
	}

	/* If skulled, reduce num kept to 0  */
	if (c->plr->is_skulled == 1)
	{
		num_kept = 0;
	}

	/* If player has the protect item prayer active, add one more item that
	 * they keep on death.  */
	if (c->plr->prayers_active[PRAYER_PROTECT_ITEMS] == 1)
	{
		num_kept++;
	}

	return num_kept;
}

/**
 * Handles dropping of items on a player's death, and what items are kept.
 * @param *c Client instance.
 * @param num_to_keep How many items the player can keep, are not dropped on
 * death.
 * @bug Jagexlike behavior seems to be that only one item in a stack is kept,
 * this doesn't do that currently. Should add it as a config, too.
 */
void packet_player_drop_item_on_grounds_on_death(struct client *c, int num_to_keep)
{
	/* If number is not valid or too high, player keeps their items.  */
	if (num_to_keep == -1)
	{
		return;
	}

	int *items_kept_slot = calloc(num_to_keep, sizeof(int));
	int *items_kept_id = calloc(num_to_keep, sizeof(int));
	int *items_kept_loc = calloc(num_to_keep, sizeof(int));
	int *items_kept_num = calloc(num_to_keep, sizeof(int));
	int *items_kept_charges = calloc(num_to_keep, sizeof(int));
	int x = 0, y = 0, found = 0;
	
	get_most_valuable_items(c, items_kept_slot, items_kept_loc, num_to_keep);

	for (x = 0; x < num_to_keep; x++)
	{
		switch (items_kept_loc[x])
		{
			/* Inventory items.  */
			case 0:
				if (items_kept_slot[x] != -1)
				{
					items_kept_id[x] = c->plr->items[items_kept_slot[x]];
					items_kept_num[x] = c->plr->items_n[items_kept_slot[x]];
					items_kept_charges[x] 
						= c->plr->items_charges[items_kept_slot[x]];
				}
				break;
			/* Equipment items.  */
			case 1:
				if (items_kept_slot[x] != -1)
				{
					items_kept_id[x] = c->plr->equipment[items_kept_slot[x]];
					items_kept_num[x] = c->plr->equipment_n[items_kept_slot[x]];
					items_kept_charges[x] 
						= c->plr->equipment_charges[items_kept_slot[x]];
				}
				break;
		}
	}
	
	/* Drop inventory items that aren't saved.  */
	for (x = 0; x < INV_SIZE; x++)
	{
		found = 0;

		/* Check if item is one that is kept.  */
		for (y = 0; y < num_to_keep; y++)
		{
			if (x == items_kept_slot[y] 
				&& items_kept_loc[y] == 0)
			{
				found = 1;
				break;
			}
		}

		if (found == 1)
		{
			printf("found item.\n");
			continue;
		}
		else
		{
			printf("non stored item, drop.\n");
			if (c->plr->items[x] != 0)
			{
				printf("drop slot: %d\n", x);
				packet_player_drop_item_on_ground(c, c->plr->items[x], INTR_INV_PLR_ITEMS, x);
			}
		}
	}

	/* Drop equipment that isn't saved.  */
	for (x = 0; x < EQUIP_SIZE; x++)
	{
		found = 0;

		/* Check if item is one that is kept.  */
		for (y = 0; y < num_to_keep; y++)
		{
			if (x == items_kept_slot[y] 
				&& items_kept_loc[y] == 1)
			{
				found = 1;
				break;
			}
		}

		if (found == 1)
		{
			printf("found item.\n");
			continue;
		}
		else
		{
			printf("non stored item, drop.\n");
			if (c->plr->equipment[x] != -1)
			{
				printf("drop slot: %d\n", x);
				packet_player_drop_item_on_ground(c, c->plr->equipment[x], INTR_EQUIP_PLR_ITEMS, x);
			}
		}
	}

	empty_player_inventory(c);
	empty_player_equipment(c);

	for (x = 0; x < num_to_keep; x++)
	{
		printf("%d: ID: %d SLOT: %d LOC: %d NUM: %d CHARGES: %d\n", x, 
			   items_kept_id[x], items_kept_slot[x], items_kept_loc[x], 
			   items_kept_num[x], items_kept_charges[x]);	
		add_item_to_inv(c, items_kept_id[x], items_kept_num[x], 
						items_kept_charges[x]);
	}

	free(items_kept_id);
	free(items_kept_slot);
	free(items_kept_loc);
	free(items_kept_num);
	free(items_kept_charges);

	return;
}

/**
 * Sends a chat message to the player listing all the items that they will
 * keep on death.
 * @param *c Client instance.
 * @param items[] Array holding slots of items kept.
 * @param location[] Array holding where each item is, 0 for inventory, 1 for 
 * equipment.
 * @param num_items Size of the items and location arrays.
 */
void print_valueable_items(struct client *c, int items[], int location[], 
						   int num_items)
{
	int x = 0;
	char return_msg[100] = {'\0'};

	if (num_items <= 0)
	{
		packet_send_chatbox_message(c, "You will not keep any items on death!");
		return;
	}

	snprintf(return_msg, 100, "You will keep the %d following items on death"
			 " and lose everything else!", num_items);
	packet_send_chatbox_message(c, return_msg);

	/* Print out results.  */
	for (; x < num_items; x++)
	{
		if (items[x] == -1)
		{
			snprintf(return_msg, 100, "#%d kept item is nothing!\n", x);
			packet_send_chatbox_message(c, return_msg);
			continue;
		}

		if (location[x] == 0)
		{
			snprintf(return_msg, 100, "%s in inventory.", 
					 ITEMS[c->plr->items[items[x]]]->name);
			packet_send_chatbox_message(c, return_msg);
		}
		else
		{
			snprintf(return_msg, 100, "%s in equipment.", 
					 ITEMS[(c->plr->equipment[items[x]])]->name);
			packet_send_chatbox_message(c, return_msg);
		}
	}
}

/**
 * Returns inventory slot the item is in if it exists, otherwise it returns
 * -1.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @param amount Number of that item needed, only for stacking items.
 * @return Returns item slot if found, -1 if not.
 */
int player_has_item(struct client *c, int item_id, int amount)
{
	int x;

	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] == item_id && c->plr->items_n[x] >= amount)
		{
			/* returns inventory slot item is in.  */
			return x;
		}
	}
	return -1;
}

/**
 * Returns the first inventory slot that an item from the passed list is in.
 *
 * @param *c Client instance.
 * @param item_ids Array of item_ids to look for.
 * @param size Size of the passed array.
 * @return Returns the item slot if found, -1 if not.
 */
int get_player_item_location_multiple_valid(struct client *c, int *item_ids, 
		int size)
{
	int x = 0;
	int rv = -1;

	if (item_ids == NULL || size <= 0)
	{
		return -1;
	}

	for (x = 0; x < size; x++)
	{
		rv = player_has_item(c, item_ids[x], 1);

		if (rv != -1)
		{
			return rv;
		}
	}

	return -1;
}

/**
 * Returns inventory slot the item is in if it exists, otherwise it returns
 * -1.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @param amount Number of that item needed, only for stacking items.
 * @param slot Inventory slot to check for the item.
 * @return Returns item slot if found, -1 if not.
 */
int player_has_item_slot(struct client *c, int item_id, int amount, int slot)
{
	if (slot < 0 || slot >= INV_SIZE)
	{
		printf("INVALID SLOT: %d\n", slot);
		return -1;
	}

	if (item_id < 0 || item_id >= PROTOCOL_MAX_ITEMS)
	{
		printf("INVALID ID: %d\n", item_id);
		return -1;
	}

	if (c->plr->items[slot] == item_id && c->plr->items_n[slot] >= amount)
	{
		/* returns inventory slot item is in.  */
		return slot;
	}
	return -1;
}

/**
 * Returns inventory slot the item is in if it exists, otherwise it returns
 * -1.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @param amount Number of that item needed, only for stacking items.
 * @param start Where in the inventory array to start the search.
 * @return Returns item slot if found, -1 if not.
 */
int player_has_item_start(struct client *c, int item_id, int amount, int start)
{
	int x;

	for (x = start; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] == item_id && c->plr->items_n[x] >= amount)
		{
			/* returns inventory slot item is in.  */
			return x;
		}
	}
	return -1;
}

/**
 * Returns inventory slot the item is in if it exists, otherwise it returns
 * -1. Handles non-stacking items.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @param amount Number of that item needed, only for stacking items.
 * @return Returns item slot if found, -1 if not.
 */
int player_has_item_non_stack(struct client *c, int item_id, int amount)
{
	if (amount <= 0)
	{
		return -1;
	}

	int x;
	int *found = malloc(sizeof(int) * amount);

	for (x = 0; x < amount; x++)
	{
		found[x] = -1;
	}
	
	found[0] = player_has_item(c, item_id, 1);
	found[1] = player_has_item_start(c, item_id, 1, (found[0] + 1));

	if (amount > 2)
	{
		for (x = 2; x < amount; x++)
		{
			found[x] 
				= player_has_item_start(c, item_id, 1, (found[(x - 1)] + 1));
		}
	}


	/* Go through the list of items, make sure we actually have the needed
	 * amount. If not, return -1.  */
	for (x = 0; x < amount; x++)
	{
		if (found[x] == -1)
		{
			free(found);
			return -1;
		}
	}	
	
	free(found);
	return amount;
}

/**
 * Looks to see how many of a non-stacking item the player has.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @return Returns number of items found.
 */
int player_has_num_items_non_stack(struct client *c, int item_id)
{
	int x = 0, counter = 0;

	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] == item_id)
		{
			counter++;
		}
	}
	return counter;
}

/**
 * Returns equipment slot the item is in if it exists, otherwise it returns
 * -1.
 * @param *c Client instance.
 * @param item_id Item ID to look for.
 * @param amount Number of that item needed, only for stacking items.
 * @return Returns item slot if found, -1 if not.
 */
int player_has_item_equip(struct client *c, int item_id, int amount)
{
	int x;

	for (x = 0; x < EQUIP_SIZE; x++)
	{
		if (c->plr->equipment[x] == item_id 
			&& c->plr->equipment_n[x] >= amount)
		{
			/* returns equipment slot item is in.  */
			return x;
		}
	}
	return -1;
}

/**
 * Returns item_id in a specified player's inventory slot.
 * @param *c Client instance.
 * @param slot Inventory slot.
 * @return Returns inventory index, -1 if nothing exists.
 */
int get_item_in_inventory_slot(struct client *c, int slot)
{
	int rv = c->plr->items[slot];
	
	if (c->plr->items_n[slot] > 0)
	{
		return rv;
	}

	return -1;
}

/**
 * Removes two different items from the player's inventory and replaces it
 * with another.
 * @param *c Client instance.
 * @param item_1, item_2 Item id's for the regents.
 * @param item_1_n, item_2_n Number of the items needed.
 * @param item_3 Item ID result.
 * @param item_3_n Number of items as the result.
 * @return Returns number of the result created, -1 if error.
 */
int combine_items(struct client *c, int item_1, int item_1_n, 
				  int item_2, int item_2_n, int item_3, int item_3_n)
{
	int item_1_slot = 0, item_2_slot = 0;
	int free_spot_created = 0;

	/* player_has_item checks if they have the required amount.  */
	item_1_slot = player_has_item(c, item_1, item_1_n);
	item_2_slot = player_has_item(c, item_2, item_2_n);

	/* Check if the player has the materials.  */
	if (item_1_slot == -1 || item_2_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have the required materials to do that!");
		return -1;
	}

	/* Check if a free spot will be created by the removal of a regent.  */
	if ((c->plr->items_n[item_1_slot] == item_1_n)
	   || (c->plr->items_n[item_2_slot] == item_2_n))
	{
		free_spot_created = 1;
	}

	/* If player has the free space for the result, continue.  */
	if (get_free_inv_slots(c) > 0 || free_spot_created == 1)
	{
		delete_item_from_inv_slot(c, item_1_slot, item_1_n);
		delete_item_from_inv_slot(c, item_2_slot, item_2_n);
		
		add_item_to_inv(c, item_3, item_3_n, -1);
		return item_3_n;
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have the space to do that!");
		return -1;
	}
}

/**
 * Removes two different items from the player's inventory and replaces it
 * with another. Tries to make as many as possible for the player's items.
 * @param *c Client instance.
 * @param item_1, item_2 Item id's for the regents.
 * @param item_3 Item ID result.
 * @param num_to_try Number of items as the result, NOT the minimum.
 * @return Returns number of the result created, -1 if error.
 */
int combine_items_permissive(struct client *c, int item_1, int item_2, 
							 int item_3, int num_to_try)
{
	int item_1_slot = 0, item_2_slot = 0;
	int item_1_num = 0, item_2_num = 0;
	int item_3_num = 0;
	int free_spot_created = 0;

	/* If there is at least one.  */
	item_1_slot = player_has_item(c, item_1, 1);
	item_2_slot = player_has_item(c, item_2, 1);

	/* Check if the player has the materials.  */
	if (item_1_slot == -1 || item_2_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have the required materials to do that!");
		return -1;
	}

	item_1_num = c->plr->items_n[item_1_slot];
	item_2_num = c->plr->items_n[item_2_slot];

	/* Enough for a full batch.  */
	if (item_1_num >= num_to_try && item_2_num >= num_to_try)
	{
		item_1_num = item_2_num = item_3_num = num_to_try;
	}
	else
	{
		/* If not enough for a full batch, ensure that we don't try to make
		   more than we have the ingredients for.  */
		if (item_1_num < item_2_num)
		{
			item_2_num = item_1_num;
		}
		else if (item_1_num > item_2_num)
		{
			item_1_num = item_2_num;
		}
		else
		{
			item_1_num = item_2_num;
		}
		item_3_num = item_1_num;
	}

	/* Check if a free spot will be created by the removal of a regent.  */
	if ((c->plr->items_n[item_1_slot] == item_1_num)
	   || (c->plr->items_n[item_2_slot] == item_2_num))
	{
		free_spot_created = 1;
	}

	/* If player has the free space for the result, continue.  */
	if (get_free_inv_slots(c) > 0 || free_spot_created == 1)
	{
		delete_item_from_inv_slot(c, item_1_slot, item_1_num);
		delete_item_from_inv_slot(c, item_2_slot, item_2_num);
		
		add_item_to_inv(c, item_3, item_3_num, -1);
		return item_3_num;
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have the space to do that!");
		return -1;
	}
}

/**
 * Removes two different items from the player's inventory and replaces it
 * with another. Allows for non-stacking items.
 * @param *c Client instance.
 * @param item_1, item_2 Item id's for the regents.
 * @param item_1_n, item_2_n Number of the items needed.
 * @param item_3 Item ID result.
 * @param item_3_n Number of items as the result.
 * @return Returns number of the result created, -1 if error.
 */
int combine_items_non_stack(struct client *c, int item_1, int item_1_n, 
				  int item_2, int item_2_n, int item_3, int item_3_n)
{
	int item_1_slots[8] = {-1};
	int item_2_slots[8] = {-1}; 
	int free_spots_created = 0;
	int x;

	/* Find first item, go from there.  */
	item_1_slots[0] = player_has_item(c, item_1, 1);
	for (x = 1; x < item_1_n; x++)
	{
		item_1_slots[x] 
			= player_has_item_start(c, item_1, 1, item_1_slots[x - 1] + 1);
	}


	item_2_slots[0] = player_has_item(c, item_2, 1);
	for (x = 1; x < item_2_n; x++)
	{
		item_2_slots[x] 
			= player_has_item_start(c, item_2, 1, item_2_slots[x - 1] + 1);
	}

	/* Check if the player has the materials.  */
	for (x = 0; x < item_1_n; x++)
	{
		if (item_1_slots[x] == -1) 
		{
			packet_send_chatbox_message(c, "You don't have the required materials to do that!");
			return -1;
		}
	}
	for (x = 0; x < item_2_n; x++)
	{
		if (item_2_slots[x] == -1)
		{	
		packet_send_chatbox_message(c, "You don't have the required materials to do that!");
			return -1;
		}
	}

	/* Since the materials are not stacking, the number of them total is the 
	   number of spots created.  */
	free_spots_created = (item_1_n + item_2_n);

	/* If player has the free space for the result, continue.  */
	if (get_free_inv_slots(c) > 0 || free_spots_created >= 1)
	{
		for (x = 0; x < 8; x++)
		{
			delete_item_from_inv_slot(c, item_1_slots[x], item_1_n);
			delete_item_from_inv_slot(c, item_2_slots[x], item_2_n);
		}

		add_item_to_inv(c, item_3, item_3_n, -1);
		return item_3_n;
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have the space to do that!");
		return -1;
	}
}

static int lowest_index(int one, int two, int three, int four)
{
	int lowest = -1;
	
	/* Find a valid number here.  */
	if (one != -1)
	{
		lowest = one;
	}
	else if (two != -1)
	{
		lowest = two;
	}
	else if (three != -1)
	{
		lowest = three;
	}
	else if (four != -1)
	{
		lowest = four;
	}

	if (one < two && one != -1)
	{
		lowest = one;	
	}
	else if (one > two && two != -1)
	{
		lowest = two;
	}

	if (lowest > three && three != -1)
	{
		lowest = three;
	}
	
	if (lowest > four && four != -1)
	{
		lowest = four;
	}

	return lowest;
}

/**
 * Scans through the player's inventory to check if they have a rune that fits
 * what is needed for a spell.
 * @param *c Client instance.
 * @param rune_id ID of the rune needed.
 * @param num_needed Number of runes needed.
 * @return Returns the inventory slot that contains the rune to use.
 * @return If -1, no needed rune was found.
 */
int look_for_rune(struct client *c, int rune_id, int num_needed)
{
	int rune_1_slot = -1, rune_2_slot = -1, rune_3_slot = -1, rune_4_slot = -1;
	int lowest = -1;

	if (rune_id == AIR_RUNE)
	{
		rune_1_slot = player_has_item(c, rune_id, num_needed);	
		rune_2_slot = player_has_item(c, MIST_RUNE, num_needed);
		rune_3_slot = player_has_item(c, DUST_RUNE, num_needed);
		rune_4_slot = player_has_item(c, SMOKE_RUNE, num_needed);
		lowest = lowest_index(rune_1_slot, rune_2_slot, rune_3_slot,
							  rune_4_slot);

		if (rune_id >= 0 && rune_id < PROTOCOL_MAX_ITEMS)
		{
			printf("rune type: %d %s\n", rune_id, ITEMS[rune_id]->name);
			printf("slots: %d %d %d %d\n", rune_1_slot, rune_2_slot,
				   rune_3_slot, rune_4_slot);
			if (lowest != -1)
			{
				printf("lowest: %d %s\n", lowest,
					   ITEMS[c->plr->items[lowest]]->name);
			}
		}

		return lowest;
	}
	else if (rune_id == WATER_RUNE)
	{
		rune_1_slot = player_has_item(c, rune_id, num_needed);	
		rune_2_slot = player_has_item(c, MIST_RUNE, num_needed);
		rune_3_slot = player_has_item(c, MUD_RUNE, num_needed);
		rune_4_slot = player_has_item(c, STEAM_RUNE, num_needed);
		lowest = lowest_index(rune_1_slot, rune_2_slot, rune_3_slot,
							  rune_4_slot);

		if (rune_id >= 0 && rune_id < PROTOCOL_MAX_ITEMS)
		{
			printf("rune type: %d %s\n", rune_id, ITEMS[rune_id]->name);
			printf("slots: %d %d %d %d\n", rune_1_slot, rune_2_slot,
				   rune_3_slot, rune_4_slot);
			if (lowest != -1)
			{
				printf("lowest: %d %s\n", lowest,
					   ITEMS[c->plr->items[lowest]]->name);
			}
		}

		return lowest;
	}
	else if (rune_id == EARTH_RUNE)
	{
		rune_1_slot = player_has_item(c, rune_id, num_needed);	
		rune_2_slot = player_has_item(c, DUST_RUNE, num_needed);
		rune_3_slot = player_has_item(c, MUD_RUNE, num_needed);
		rune_4_slot = player_has_item(c, LAVA_RUNE, num_needed);
		lowest = lowest_index(rune_1_slot, rune_2_slot, rune_3_slot,
							  rune_4_slot);

		if (rune_id >= 0 && rune_id < PROTOCOL_MAX_ITEMS)
		{
			printf("rune type: %d %s\n", rune_id, ITEMS[rune_id]->name);
			printf("slots: %d %d %d %d\n", rune_1_slot, rune_2_slot,
				   rune_3_slot, rune_4_slot);
			if (lowest != -1)
			{
				printf("lowest: %d %s\n", lowest,
					   ITEMS[c->plr->items[lowest]]->name);
			}
		}

		return lowest;
	}
	else if (rune_id == FIRE_RUNE)
	{
		rune_1_slot = player_has_item(c, rune_id, num_needed);	
		rune_2_slot = player_has_item(c, SMOKE_RUNE, num_needed);
		rune_3_slot = player_has_item(c, STEAM_RUNE, num_needed);
		rune_4_slot = player_has_item(c, LAVA_RUNE, num_needed);
		lowest = lowest_index(rune_1_slot, rune_2_slot, rune_3_slot,
							  rune_4_slot);

		if (rune_id >= 0 && rune_id < PROTOCOL_MAX_ITEMS)
		{
			printf("rune type: %d %s\n", rune_id, ITEMS[rune_id]->name);
			printf("slots: %d %d %d %d\n", rune_1_slot, rune_2_slot,
				   rune_3_slot, rune_4_slot);
			if (lowest != -1)
			{
				printf("lowest: %d %s\n", lowest,
					   ITEMS[c->plr->items[lowest]]->name);
			}
		}

		return lowest;
	}
	else
	{
		rune_1_slot = player_has_item(c, rune_id, num_needed);
		return rune_1_slot;
	}

	return -1;
}

/**
 * Accepts an item_d, and returns how many charges it has.
 * @param item_id Item id to check.
 * @return Returns number of charges the item has.
 */
int get_item_default_charges(int item_id)
{
	switch (item_id)
	{
		/* Thread, has 4 to 5 uses before it is removed.  */
		case 1734:
			return rand_int(4, 5);
	
		/* New crystal bow and shield.  */
		case 4212:
		case 4214:
		case 4224:
		case 4225:
			return 2500;
		/* Crystal bow and shield, 9/10.  */
		case 4215:
		case 4226:
			return 2250;
		/* Crystal bow and shield, 8/10.  */
		case 4216:
		case 4227:
			return 2000;
		/* Crystal bow and shield, 7/10.  */
		case 4217:
		case 4228:
			return 1750;
		/* Crystal bow and shield, 6/10.  */
		case 4218:
		case 4229:
			return 1500;
		/* Crystal bow and shield, 5/10.  */
		case 4219:
		case 4230:
			return 1250;
		/* Crystal bow and shield, 4/10.  */
		case 4220:
		case 4231:
			return 1000;
		/* Crystal bow and shield, 3/10.  */
		case 4221:
		case 4232:
			return 750;
		/* Crystal bow and shield, 2/10.  */
		case 4222:
		case 4233:
			return 500;
		/* Crystal bow and shield, 1/10.  */
		case 4223:
		case 4234:
			return 250;
	/* Rings.  */
		/* Ring of recoil.  */
		case 2550:
			return 40;
		/* Ring of forging.  */
		case 2568:
			return 140;
		/* Ring of life.  */
		case 2570:
			return 1;
	/* Necklaces.  */
		/* Binding necklace.  */
		case 5521:
			return 16;

		default:
			return -1;
	}
}

/**
 * Checks if the player is using the correct ammo for the type of ranged
 * weapon they have.
 * @param *c Client instance.
 * @return Returns 1 on valid ammo used, 0 on invalid.
 */
int get_valid_ammo_type(struct client *c)
{
	if (c->plr->equipment[WEAPON_SLOT] != -1)
	{
		int ammo_equip = c->plr->equipment[AMMO_SLOT];
		int ammo_num = c->plr->equipment_n[AMMO_SLOT];
		int ammo_needed = ITEMS[c->plr->equipment[WEAPON_SLOT]]->ammo_type;

		switch (ammo_needed)
		{
			case 0:
				printf("ERROR! NO AMMO TYPE SET!\n");
				break;
			case AMMO_FROM_WEAPON_SLOT:
				if (c->plr->equipment[WEAPON_SLOT] > 0)
				{
					return 1;
				}
				return 0;
			case AMMO_WEAPON_CHARGES:
				/* Implement weapon charge managing.  */
				break;
			case AMMO_BRONZE_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 883);
			case AMMO_IRON_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 885);
			case AMMO_STEEL_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 887);
			case AMMO_MITHRIL_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 889);
			case AMMO_ADAMANT_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 891);
			case AMMO_RUNE_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 882, 893);

			case AMMO_BRONZE_BOLTS:
			case AMMO_IRON_BOLTS:
			case AMMO_STEEL_BOLTS:
			case AMMO_MITHRIL_BOLTS:
			case AMMO_ADAMANT_BOLTS:
			case AMMO_RUNE_BOLTS:
				/* Note: 333 only has bronze bolts, change this when
				   supporting later revisions.  */
				return check_ammo_type(ammo_equip, ammo_num, 877, 881);

			case AMMO_BRONZE_BRUTAL:
			case AMMO_IRON_BRUTAL:
			case AMMO_STEEL_BRUTAL:
			case AMMO_BLACK_BRUTAL:
			case AMMO_MITHRIL_BRUTAL:
			case AMMO_ADAMANT_BRUTAL:
			case AMMO_RUNE_BRUTAL:
				/* Only the ogre comp bow can use brutal arrows, can also use
				 * ogre arrows.  */
				return (check_ammo_type(ammo_equip, ammo_num, 4773, 4807) 
						|| check_ammo_type(ammo_equip, ammo_num, 2866, 2866));
			/* The ogre bow can only use ogre arrows.  */	
			case AMMO_OGRE_ARROWS:
				return check_ammo_type(ammo_equip, ammo_num, 2866, 2866);
			default:
				printf("UNHANDLED AMMO TYPE!\n");
				break;
		}
	}
	return 1;
}

/**
 * Check if player has the ammo/runes needed for an attack.
 * @param *c Client instance.
 * @return Returns 1 if has ammo, else if not.
 */
int get_player_weapon_has_ammo(struct client *c)
{
	if (c->plr->equipment[WEAPON_SLOT] != -1 
			&& is_player_using_magic_attacks(c) == 0)
	{
		if (ITEMS[c->plr->equipment[WEAPON_SLOT]]->ranged == 1)
		{
			return get_valid_ammo_type(c);
		}
		return 1;
	}
	else if (c->plr->cast_spell_id != 0)
	{
		return has_needed_runes(c, get_spell_index(c->plr->cast_spell_id), 1);
	}
	else if (c->plr->autocasting != 0)
	{
		return has_needed_runes(c, get_spell_index(c->plr->autocasting), 0);
	}
	return 1;
}

/**
 * Checks the attack reach for a player, based off of what attack style they
 * are using.
 * @param *c Client instance.
 * @return Returns the reach of the weapon a player is using. Returns 10 for 
 * spells.
 */
int get_player_weapon_reach(struct client *c)
{
	int is_player_using_magic = is_player_using_magic_attacks(c);

	if (c->plr->equipment[WEAPON_SLOT] != -1 && is_player_using_magic == 0)
	{
		/* If using longrange fighting style, increase weapon reach.  */
		if (c->plr->fighting_style == LONGRANGED_STYLE_RANGE)
		{
			int reach = ITEMS[c->plr->equipment[WEAPON_SLOT]]->reach;
			
			/* If adding two to the reach would exceed ten, return ten.
			   Otherwise, return reach plus 2.  */
			reach += 2;

			return get_smaller(reach, 10);
		}
		else
		{
			return ITEMS[c->plr->equipment[WEAPON_SLOT]]->reach;
		}
	}
	/* From what I have read, spell reach is always 10. Check back later
	   if this needs adjusting.  */
	else if (is_player_using_magic)
	{
		return 10;
	}

	/* If player has no weapon equipped, return 1 for range.  */
	return 1;
}

/**
 * Detects if the player is using a ranged weapon, or is autocasting with a
 * staff.
 * @param *c Client instance.
 * @return Returns if they are using ranged or not.
 */
int is_player_weapon_ranged(struct client *c)
{
	if (c->plr->equipment[WEAPON_SLOT] != -1)
	{
		if (is_player_using_magic_attacks(c) == 0)
		{
			return ITEMS[c->plr->equipment[WEAPON_SLOT]]->ranged;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}

/**
 * Returns the ranged projectile id that should be used.
 * @param *c Client instance.
 * @return Returns the projectile id to use.
 */
int get_player_weapon_projectile_id(struct client *c)
{
	if (c->plr->equipment[WEAPON_SLOT] != -1)
	{
		if (ITEMS[c->plr->equipment[WEAPON_SLOT]]->shoot_gfx != 0)
		{
			return ITEMS[c->plr->equipment[WEAPON_SLOT]]->shoot_gfx;
		}
	}
	if (c->plr->equipment[AMMO_SLOT] != -1)
	{
		if (ITEMS[c->plr->equipment[AMMO_SLOT]]->shoot_gfx != 0)
		{
			return ITEMS[c->plr->equipment[AMMO_SLOT]]->shoot_gfx;
		}
	}
	return 0;
}

/**
 * Returns the attack speed of the weapon/attack style the player is using.
 * @param *c Client instance.
 * @return Returns the delay in ticks before they can make attacks.
 */
int get_player_weapon_speed(struct client *c)
{
	if (is_player_using_magic_attacks(c) == 0)
	{
		return 5;
	}
	else if (c->plr->equipment[WEAPON_SLOT] != -1)
	{
		/* If using rapid attack type, reduce the delay between attacks by
		   one tick. Otherwise, return normal speed.  */
		if (c->plr->fighting_style == RAPID_STYLE_RANGE)
		{
			return (ITEMS[c->plr->equipment[WEAPON_SLOT]]->atk_speed - 1);
		}
		else
		{
			return ITEMS[c->plr->equipment[WEAPON_SLOT]]->atk_speed;
		}
	}
	else
	{
		/* Default hand-to-hand speed.  */
		return 4;
	}
}

static int check_for_ring_of_wealth(struct client *c, int table_idx)
{
	/* If the player is wearing a ring of wealth, make sure that the rare drop
	 * gem table, and the rare drop mega tables are swapped to their ring of
	 * wealth variant.  */
	if (c == NULL)
	{
		return table_idx;
	}

	if (c->plr->equipment[RING_SLOT] == 2572)
	{
		if (strcmp(LOOT_TABLES[table_idx]->name, "RDT_GEM") == 0)
		{
			return RDT_GEM_WEALTH_IDX;
		}
		else if (strcmp(LOOT_TABLES[table_idx]->name, "RDT_MEGA") == 0)
		{
			return RDT_MEGA_WEALTH_IDX;
		}
		else
		{
			return table_idx;
		}
	}
	else
	{
		return table_idx;
	}
}

/**
 * Returns a random item from the loot table specified.
 * @param *c Client instance to get the item.
 * @param table_idx Index of the table in the LOOT_TABLES array to roll.
 * @param *amount Modified by this function, changes it to the number of that
 * item rolled for.
 * @return Returns the ID of the item that was rolled for.
 */
int get_loot_table_item(struct client *c, int table_idx, int *amount)
{
	double weight = 0.0;
	double roll = 0.0;
	int x = 0;
	int loops = 0;
	int drop_id = -1;

	if (table_idx < 0)
	{
		return -1;
	}

	table_idx = check_for_ring_of_wealth(c, table_idx);

	if (is_double_about_equal(LOOT_TABLES[table_idx]->total_weights, 0.0, EPSILON))
	{
		return -1;
	}

	while (1)
	{
		weight = LOOT_TABLES[table_idx]->total_weights;
		roll = rand_double(0.0, weight);

		for (x = 0; x < LOOT_TABLES[table_idx]->num_drops; x++)
		{
			roll -= LOOT_TABLES[table_idx]->drops[x][3];

			if (roll <= 0.0)
			{
				drop_id = (int) LOOT_TABLES[table_idx]->drops[x][0];
				*amount = rand_int((int) LOOT_TABLES[table_idx]->drops[x][1],
									(int) LOOT_TABLES[table_idx]->drops[x][2]);

				return drop_id;
			}
		}

		/* If roll was not exhausted, go through the loot tables now.  */
		if (roll >= 0.0)
		{
			for (x = 0; x < LOOT_TABLES[table_idx]->num_tables; x++)
			{
				roll -= LOOT_TABLES[table_idx]->tables[x][3];

				/* Found table, drop into it now.  */
				if (roll <= 0.0)
				{
					table_idx = check_for_ring_of_wealth(c, 
						(int) LOOT_TABLES[table_idx]->tables[x][0]);
					break;
				}
			}

			if (roll >= 0.0)
			{
				printf("ERROR! NO DROP FOUND!\n");
				printf("TABLE ID: %d\n", table_idx);
				printf("TABLE NAME: %s\n", LOOT_TABLES[table_idx]->name);
				return -1;
			}
		}
		if (loops > 5)
		{
			printf("Failsafe out, no drop.\n");
			printf("Check table %d, should never loop like this.\n", 
				   table_idx);
			return -1;
		}
		loops++;
	}
}

/**
 * Drops all loot from an NPC's loot tables when killed. Will drop all the
 * NPC's guaranteed drops first, then will go through all the different loot
 * tables they have.
 * @param *npc NPC spawn instance.
 */
void drop_npc_loot(struct npc_spawn *npc)
{
	struct client *c;
	int drops[MAX_NPCS_LOOT_TABLES] = {-1};
	int amounts[MAX_NPCS_LOOT_TABLES] = {-1};
	int x = 0;

	/* Check if the npc has a targeted player or not, will be used for the
	 * ring of wealth check, and item visibility when dropped.  */
	if (npc->targeted_player != -1)
	{
		c = CONNECTED_CLIENTS[npc->targeted_player];
	}
	else
	{
		c = NULL;
	}

	/* Get the drops for each loot table the NPC has.  */
	for (x = 0; x < MAX_NPCS_LOOT_TABLES; x++)
	{
		drops[x] = get_loot_table_item(c, 
						NPCS[npc->npc_id]->loot_table_indexes[x], 
						&amounts[x]);
	}

	/* If the NPC has a loot table for all the always drops, drop all them 
	   now.  */
	if (NPCS[npc->npc_id]->always_loot_index != -1)
	{
		int index = NPCS[npc->npc_id]->always_loot_index;

		for (x = 0; 
			 x < LOOT_TABLES[index]->num_drops;
			 x++)
		{
			add_to_item_drops_exclusive(c, LOOT_TABLES[index]->drops[x][0], 
				rand_int(LOOT_TABLES[index]->drops[x][1], 
						 LOOT_TABLES[index]->drops[x][2]),
				get_item_default_charges(LOOT_TABLES[index]->drops[x][0]),
				npc->world_x, npc->world_y, npc->world_z, 1);
		}
	}
	
	/* Drop each item onto the ground. If the NPC was targeting a player, drop
	 * it only for him. Otherwise, drop so everyone can see it.  */
	for (x = 0; x < MAX_NPCS_LOOT_TABLES; x++)
	{
		if (drops[x] != -1)
		{
			add_to_item_drops_exclusive(c, drops[x], amounts[x],
										get_item_default_charges(drops[x]),
										npc->world_x, npc->world_y, 
										npc->world_z, 1);
		}
	}
}

/**
 * Handles depositing of items from the player's inventory into their bank
 * account.
 * @param *c Client instance.
 * @param item_id ID of the item being deposited.
 * @param item_slot Where the item is in their inventory.
 * @param item_num How many they requested to be deposited.
 */
void deposit_into_bank(struct client *c, int item_id, int item_slot, 
					   int item_num)
{
	int x = 0;
	int stacking = ITEMS[item_id]->stacking;
	int noted = ITEMS[item_id]->noted;
	int first_empty = 99999;
	int exists_slot = -1;
	int remainder = -1;
	int num_check = 0;

	/* Check if the item is able to be inserted into the bank.  */
	if (ITEMS[item_id]->bankable != 1)
	{
		packet_send_chatbox_message(c, "A magical force prevents you from banking this!");
		return;
	}

	/* The item_num is passed from interface clicks on bank 5, bank all, etc.
	 * Check how many items the player has.  */
	if (stacking == 1)
	{
		if (item_num > c->plr->items_n[item_slot])
		{
			item_num = c->plr->items_n[item_slot];
		}
	}
	else
	{
		num_check = player_has_num_items_non_stack(c, item_id);

		if (item_num > num_check)
		{
			item_num = num_check;
		}
	}
	
	/* Check if the item trying to be deposited is noted, if so, change the
	 * item to be added to the bank to its unnoted version.  */
	if (noted == 1)
	{
		item_id = ITEMS[item_id]->unnoted_id;
	}

	/* Go through the bank to see if they already have the item somewhere, or
	 * if it is new to the bank.  */
	for (x = 0; x < PLAYER_BANK_SIZE; x++)
	{
		if (c->plr->bank_items[x] == -1)
		{
			if (x < first_empty)
			{
				first_empty = x;
			}
		}
		else if (c->plr->bank_items[x] == item_id)
		{
			exists_slot = x;	
		}
	}

	/* If item doesn't exist in bank before now.  */
	if (exists_slot == -1)
	{
		if (first_empty == 99999)
		{
			packet_send_chatbox_message(c, "You don't have any bank space left for that!");
			return;
		}
		else
		{
			c->plr->bank_items[first_empty] = item_id;
			c->plr->bank_items_n[first_empty] = item_num;
			c->plr->bank_items_charges[first_empty] 
				= c->plr->items_charges[item_slot];

			packet_reload_items_in_inventory_interface(c);
			packet_reload_items_in_bank_interface(c);
			packet_reload_items_in_bank_sidebar_interface(c);
		}
	}
	/* Item is already in bank, add it to the existing slot.  */
	else
	{
		if (c->plr->bank_items_n[exists_slot] == MAX_ITEM_STACK)
		{
			packet_send_chatbox_message(c, "You can't fit any more of that in your bank!");
			return;
		}
		else
		{
			if ((long) ((long) c->plr->bank_items_n[exists_slot] 
						+ (long) item_num) >= MAX_ITEM_STACK)
			{
				/* Overflow that stays in the player's inventory.  */
				remainder 
					= ((long) ((long) c->plr->bank_items_n[exists_slot] 
							   + (long) item_num) 
							   - MAX_ITEM_STACK);	
				
				if (item_num - remainder > 0)
				{
					delete_item_from_inv_slot(c, item_slot, 
											  (item_num - remainder));
					c->plr->bank_items_n[exists_slot] 
						+= (item_num - remainder);
					c->plr->bank_items_charges[exists_slot] 
						= c->plr->items_charges[item_slot];

					packet_reload_items_in_inventory_interface(c);
					packet_reload_items_in_bank_interface(c);
					packet_reload_items_in_bank_sidebar_interface(c);
					return;
				}
			}
			else
			{
				c->plr->bank_items_n[exists_slot] += item_num;
				c->plr->bank_items_charges[exists_slot] 
					= c->plr->items_charges[item_slot];
				packet_reload_items_in_inventory_interface(c);
				packet_reload_items_in_bank_interface(c);
				packet_reload_items_in_bank_sidebar_interface(c);
			}
		}
	}

	delete_item_from_inv_slot(c, item_slot, item_num);

	/* Refresh all needed interfaces.  */
	packet_reload_items_in_inventory_interface(c);
	packet_reload_items_in_bank_interface(c);
	packet_reload_items_in_bank_sidebar_interface(c);
}

/**
 * Handles removing items from the player's bank into their inventory.
 * @param *c Client instance.
 * @param item_id ID of item to take out.
 * @param item_slot Slot of the item in the bank.
 * @param item_num Number to take out.
 */
void withdraw_from_bank(struct client *c, int item_id, int item_slot, 
						int item_num)
{
	int x = 0;
	int rv = 0;
	int free = get_free_inv_slots(c);
	int stacking = ITEMS[item_id]->stacking;

	/* The item_num is passed from interface clicks on bank 5, bank all, etc.
	 * Check how many items the player has.  */
	if (c->plr->bank_items_n[item_slot] < item_num)
	{
		item_num = c->plr->bank_items_n[item_slot];
	}
	
	/* Withdraw the item as a bank note if set, but check first if the item
	 * even has a noted variant.  */
	if (c->plr->bank_is_note_mode == 1)
	{
		if (ITEMS[item_id]->note_id != -1)
		{
			item_id = ITEMS[item_id]->note_id;
			stacking = ITEMS[item_id]->stacking;
		}
	}

	/* If item is stacking, then there we don't need to handle anything
	 * specially. Let the add_item_to_inv function handle it.  */
	if (stacking == 1)
	{
		rv = add_item_to_inv(c, item_id, item_num, 
							 c->plr->bank_items_charges[item_slot]);
	}
	else
	{
		/* Check for free inventory slots, make sure numbers line up.  */
		if (free < item_num)
		{
			item_num = free;
		}

		/* All all non-stacking items.  */
		for (x = 0; x < item_num; x++)
		{
			rv = add_item_to_inv(c, item_id, item_num, 
								 c->plr->bank_items_charges[item_slot]);
			
			if (rv != 1)
			{
				printf("FAILURE FAILURE WITHDRAW BANK!\n");
				return;
			}
		}
	}

	/* Handle modification of items left in bank.  */
	if (rv == 1)
	{
		c->plr->bank_items_n[item_slot] -= item_num;

		if (c->plr->bank_items_n[item_slot] == 0)
		{
			c->plr->bank_items[item_slot] = -1;
			c->plr->bank_items_charges[item_slot] = -1;
		}
	}

	/* Refresh all needed interfaces.  */
	packet_reload_items_in_bank_interface(c);
	packet_reload_items_in_bank_sidebar_interface(c);
}

/**
 * Creates a projectile coming from the player and hitting an npc.
 * @param *c Client instance.
 * @param distance How far from the target the NPC is.
 * @param size How big the npc target is.
 */
void player_shoot_npc(struct client *c, int distance, int size)
{
	/* Setup adjustments for npcs of different sizes.  */
	int new_x = NPC_SPAWNS[c->plr->targeted_npc]->world_x; 
	int new_y = NPC_SPAWNS[c->plr->targeted_npc]->world_y; 

	/* If npc is of a large size, try to shoot the projectile into their 
	 * center.  */
	if (size > 1)
	{
		size /= 2;
		new_x += (size);
		new_y += (size);
	}
	
	/* Handles charged weapons, and stacking ranged weapons.  */
	/* If a specific projectile gfx is not set, send weapon one.  */
	if (c->plr->projectile_type == 0)
	{
		packet_create_projectile_global(c, c->plr->world_x, c->plr->world_y, 
			new_x, new_y,
			50, 70,
			ITEMS[c->plr->projectile_id]->shoot_gfx, 
			43, 31, c->plr->targeted_npc, 53, 10);
			
		NPC_SPAWNS[c->plr->targeted_npc]->ammo_to_drop = c->plr->projectile_id;
		c->plr->projectile_id = 0;
	}
	/* If casting a spell.  */
	else
	{
		printf("spell id: %d index: %d dist: %d speed: %d\n", 
			   c->plr->cast_spell_id, 
			   c->plr->cast_spell_index, distance, 
			   magic_attack_speed[distance]);

		packet_create_projectile_global(c, c->plr->world_x, c->plr->world_y, 
			new_x, new_y,
			SPELLS[c->plr->cast_spell_index]->projectile_angle, 
			magic_attack_speed[distance],
			SPELLS[c->plr->cast_spell_index]->projectile_gfx, 
			SPELLS[c->plr->cast_spell_index]->projectile_start_height, 
			SPELLS[c->plr->cast_spell_index]->projectile_end_height, 
			c->plr->targeted_npc,
			SPELLS[c->plr->cast_spell_index]->projectile_delay, 
			SPELLS[c->plr->cast_spell_index]->projectile_slope); 
		
		/* Reset this if not autocasting.  */
		if (c->plr->autocasting != 1)
		{
			c->plr->cast_spell_id = 0;
			c->plr->cast_spell_index = -1;
		}
		c->plr->projectile_type = 0;
	}
}

/**
 * Creates a projectile coming from an npc and hitting a player.
 * @param *npc NPC spawn instance the projectile is coming from.
 * @param distance How far from the target the NPC is.
 * @todo Add in handling for projectile speeds based on if the NPC is using a
 * magic or a ranged attack.
 */
void npc_shoot_player(struct npc_spawn *npc, int distance)
{
	/* Handles charged weapons, and stacking ranged weapons.  */
	/* If a specific projectile gfx is not set, send weapon one.  */
	packet_create_projectile_global(CONNECTED_CLIENTS[npc->targeted_player], 
							 npc->world_x, npc->world_y,
							 CONNECTED_CLIENTS[npc->targeted_player]->plr->world_x, 
							 CONNECTED_CLIENTS[npc->targeted_player]->plr->world_y, 
							 50, ranged_arrow_speed[distance],
							 npc->projectile_type, 
							 43, 31, npc->targeted_player - 1, 53, 10);
							/* TODO: Check if targeted_player needs to be
							   changed here due to the player index 
							   addition.  */

	npc->projectile_type = 0;
}

/**
 * When an consumable has been used, this applies any effect that it may have.
 * @param *c Client instance.
 * @param item_id ID of the consumable used.
 */
void apply_consumable_boosts(struct client *c, int item_id)
{
	int temp;

	switch (item_id)
	{
		/* Agility potion.  */
		case 3032:
		case 3034:
		case 3036:
		case 3038:
			add_temp_skill_modifier(c, AGILITY_SKILL, 3);
			break;
		/* Magic potion.  */
		case 3040:
		case 3042:
		case 3044:
		case 3046:
			add_temp_skill_modifier(c, MAGIC_SKILL, 4);
			break;
		/* Strength potion.  */
		case 113:
		case 115:
		case 117:
		case 119:
			temp = get_level_for_xp(c->plr->xp[STRENGTH_SKILL]) * .1;
			add_temp_skill_modifier(c, STRENGTH_SKILL, temp + 3);
			break;
		/* Attack potion.  */
		case 2428:
		case 121:
		case 123:
		case 125:
			temp = get_level_for_xp(c->plr->xp[ATTACK_SKILL]) * .1;
			add_temp_skill_modifier(c, ATTACK_SKILL, temp + 3);
			break;
		/* Defence potion.  */
		case 2432:
		case 133:
		case 135:
		case 137:
			temp = get_level_for_xp(c->plr->xp[DEFENCE_SKILL]) * .1;
			add_temp_skill_modifier(c, DEFENCE_SKILL, temp + 3);
			break;
		/* Prayer potion.  */
		case 2434:
		case 139:
		case 141:
		case 143:
			restore_skill_points(c, PRAYER_SKILL, 
				((get_level_for_xp(c->plr->xp[PRAYER_SKILL]) * .25) + 7));
			break;
		/* Fishing potion.  */
		case 2438:
		case 151:
		case 153:
		case 155:
			add_temp_skill_modifier(c, FISHING_SKILL, 3);
			break;
		/* Super attack potion.  */
		case 2436:
		case 145:
		case 147:
		case 149:
			temp = get_level_for_xp(c->plr->xp[ATTACK_SKILL]) * .15;
			add_temp_skill_modifier(c, ATTACK_SKILL, temp + 5);
			break;
		/* Super strength potion.  */
		case 2440:
		case 157:
		case 159:
		case 161:
			temp = get_level_for_xp(c->plr->xp[STRENGTH_SKILL]) * .15;
			add_temp_skill_modifier(c, STRENGTH_SKILL, temp + 5);
			break;
		/* Super defence potion.  */
		case 2442:
		case 163:
		case 165:
		case 167:
			temp = get_level_for_xp(c->plr->xp[DEFENCE_SKILL]) * .15;
			add_temp_skill_modifier(c, DEFENCE_SKILL, temp + 5);
			break;
		/* Ranging potion.  */
		case 2444:
		case 169:
		case 171:
		case 173:
			temp = get_level_for_xp(c->plr->xp[RANGED_SKILL]) * .1;
			add_temp_skill_modifier(c, RANGED_SKILL, temp + 4);
			break;
		/* Zamorak brew.  */
		case 2450:
		case 189:
		case 191:
		case 193:
			temp = get_level_for_xp(c->plr->xp[ATTACK_SKILL]) * .2;
			add_temp_skill_modifier(c, ATTACK_SKILL, temp + 2);

			temp = get_level_for_xp(c->plr->xp[STRENGTH_SKILL]) * .12;
			add_temp_skill_modifier(c, STRENGTH_SKILL, temp + 2);

			temp = get_level_for_xp(c->plr->xp[DEFENCE_SKILL]) * .12;
			add_temp_skill_modifier(c, DEFENCE_SKILL, (-1 * (temp + 2)));

			temp = c->plr->level[HITPOINTS_SKILL] * .12;
			c->plr->level[HITPOINTS_SKILL] 
				= (c->plr->level[HITPOINTS_SKILL] - temp);

			restore_skill_points(c, PRAYER_SKILL,
				(get_level_for_xp(c->plr->xp[PRAYER_SKILL] * .1)));
			break;
		/* Restore potion.  */
		case 2430:
		case 127:
		case 129:
		case 131:
			restore_skill_points(c, ATTACK_SKILL,
				(10 + (get_level_for_xp(c->plr->xp[ATTACK_SKILL]) * .3)));
			restore_skill_points(c, STRENGTH_SKILL,
				(10 + (get_level_for_xp(c->plr->xp[STRENGTH_SKILL]) * .3)));
			restore_skill_points(c, MAGIC_SKILL,
				(10 + (get_level_for_xp(c->plr->xp[MAGIC_SKILL]) * .3)));
			restore_skill_points(c, RANGED_SKILL,
				(10 + (get_level_for_xp(c->plr->xp[RANGED_SKILL]) * .3)));
			break;
		/* Super restore potion.  */
		case 3024:
		case 3026:
		case 3028:
		case 3030:
			for (temp = 0; temp < PROTOCOL_NUM_SKILLS; temp++)
			{
				if (temp == HITPOINTS_SKILL)
				{
					continue;
				}
				restore_skill_points(c, temp,
					(8 + (get_level_for_xp(c->plr->xp[temp]) * .25)));
			}
			break;
		/* Guthix rest tea.  */
		case 4417:
		case 4419:
		case 4421:
		case 4423:
			if (c->plr->poison_damage > 0)
			{
				c->plr->poison_damage--;
			}

			if (c->plr->poison_damage == 0)
			{
				cure_poison_player(c, -1);
			}
			restore_run_energy(c, 5);
			break;
		/* Energy potion.  */
		case 3008:
		case 3010:
		case 3012:
		case 3014:
			restore_run_energy(c, 10);
			break;
		/* Super energy potion.  */
		case 3016:
		case 3018:
		case 3020:
		case 3022:
			restore_run_energy(c, 20);
			break;
		/* Antipoison.  */
		case 2446:
		case 175:
		case 177:
		case 179:
			/* Cure player of poison, provide immunity for 90 seconds.  */
			cure_poison_player(c, 150);
			break;
		/* Super Antipoison.  */
		case 2448:
		case 181:
		case 183:
		case 185:
			/* Cure player of poison, provide immunity for 6 minutes.  */
			cure_poison_player(c, 600);
			break;
		/* Antidote+.  */
		case 5943:
		case 5945:
		case 5947:
		case 5949:
			/* Cure player of poison, provide immunity for 9 minutes.  */
			cure_poison_player(c, 900);
			break;
		/* Antidote++.  */
		case 5952:
		case 5954:
		case 5956:
		case 5958:
			/* Cure player of poison, provide immunity for 12 minutes.  */
			cure_poison_player(c, 1200);
			break;
		/* Draynor cabbage.  */
		case 1967:
			if (c->plr->level[DEFENCE_SKILL] >= 50)
			{
				add_temp_skill_modifier(c, DEFENCE_SKILL, 2);
			}
			else
			{
				add_temp_skill_modifier(c, DEFENCE_SKILL, 1);
			}
			break;
		/* Grog.  */
		case 1915:
			add_temp_skill_modifier(c, STRENGTH_SKILL, 3);
			add_temp_skill_modifier(c, ATTACK_SKILL, -6);
			break;
		/* Beer.  */
		case 1917:
		case 3083:
			temp = c->plr->level[STRENGTH_SKILL] * .02;
			add_temp_skill_modifier(c, STRENGTH_SKILL, (temp + 1));
			temp = c->plr->level[ATTACK_SKILL] * .06;
			add_temp_skill_modifier_stacking(c, ATTACK_SKILL, -(temp + 1));
			break;
		/* Bad wine.  */
		case 1991:
			add_temp_skill_modifier_stacking(c, ATTACK_SKILL, -3);
			break;
		/* Vodka, whisky, gin, brandy.  */
		case 2015:
		case 2017:
		case 2019:
		case 2021:
			temp = c->plr->level[STRENGTH_SKILL] * .05;
			add_temp_skill_modifier(c, STRENGTH_SKILL, (temp + 1));
			temp = ((c->plr->level[ATTACK_SKILL] * .02) - 3);
			add_temp_skill_modifier(c, ATTACK_SKILL, -temp);
			break;
		/* Bandit's brew.  */
		case 4627:
			add_temp_skill_modifier(c, THIEVING_SKILL, 1);
			add_temp_skill_modifier(c, ATTACK_SKILL, 1);
			temp = c->plr->level[STRENGTH_SKILL] * 0.06;
			add_temp_skill_modifier(c, STRENGTH_SKILL, -(temp + 3));
			temp = c->plr->level[DEFENCE_SKILL] * 0.06;
			add_temp_skill_modifier(c, DEFENCE_SKILL, -(temp + 3));
			break;
		/* Chef's Delight.  */
		case 5755:
			temp = c->plr->level[COOKING_SKILL] * .05;
			add_temp_skill_modifier(c, COOKING_SKILL, (temp + 1));

			add_temp_skill_modifier(c, ATTACK_SKILL, -2);

			add_temp_skill_modifier(c, STRENGTH_SKILL, -2);
			break;
		/* Chef's Delight(m).  */
		case 5757:
			temp = c->plr->level[COOKING_SKILL] * .05;
			add_temp_skill_modifier(c, COOKING_SKILL, (temp + 2));

			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -3);
			break;
		/* Cider.  */
		case 5763:
			add_temp_skill_modifier(c, FARMING_SKILL, 1);
			add_temp_skill_modifier(c, ATTACK_SKILL, -2);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -2);
			break;
		/* Mature Cider.  */
		case 5765:
			add_temp_skill_modifier(c, FARMING_SKILL, 2);
			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -3);
			break;
		/* Dwarven Stout.  */
		case 1913:
			add_temp_skill_modifier(c, MINING_SKILL, 1);
			add_temp_skill_modifier(c, SMITHING_SKILL, 1);
			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -3);
			add_temp_skill_modifier(c, DEFENCE_SKILL, -3);
			break;
		/* Dwarven Stout(m).  */
		case 5747:
			add_temp_skill_modifier(c, MINING_SKILL, 2);
			add_temp_skill_modifier(c, SMITHING_SKILL, 2);
			add_temp_skill_modifier(c, ATTACK_SKILL, -7);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -7);
			add_temp_skill_modifier(c, DEFENCE_SKILL, -7);
			break;
		/* Asgarnian Ale.  */
		case 1905:
			add_temp_skill_modifier(c, ATTACK_SKILL, -4);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 2);
			break;
		/* Asgarnian Ale(m).  */
		case 5739:
			add_temp_skill_modifier(c, ATTACK_SKILL, -6);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 3);
			break;
		/* Greenman's Ale.  */
		case 1909:
			add_temp_skill_modifier(c, HERBLORE_SKILL, 1);
			add_temp_skill_modifier(c, ATTACK_SKILL, -1);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -1);
			break;
		/* Greenman's Ale(m).  */
		case 5743:
			add_temp_skill_modifier(c, HERBLORE_SKILL, 2);
			add_temp_skill_modifier(c, ATTACK_SKILL, -1);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -1);
			break;
		/* Wizard's Mind Bomb.  */
		case 1907:
			if (get_level_for_xp(c->plr->xp[MAGIC_SKILL] < 50))
			{
				add_temp_skill_modifier(c, MAGIC_SKILL, 2);
			}
			else
			{
				add_temp_skill_modifier(c, MAGIC_SKILL, 3);
			}
			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -4);
			add_temp_skill_modifier(c, DEFENCE_SKILL, -4);
			break;
		/* Wizard's Mind Bomb(m).  */
		case 5741:
			if (get_level_for_xp(c->plr->xp[MAGIC_SKILL] < 50))
			{
				add_temp_skill_modifier(c, MAGIC_SKILL, 3);
			}
			else
			{
				add_temp_skill_modifier(c, MAGIC_SKILL, 4);
			}
			add_temp_skill_modifier(c, ATTACK_SKILL, -5);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -5);
			add_temp_skill_modifier(c, DEFENCE_SKILL, -5);
			break;
		/* Dragon bitter.  */
		case 1911:
			add_temp_skill_modifier(c, ATTACK_SKILL, -4);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 2);
			break;
		/* Dragon bitter(m).  */
		case 5745:
			add_temp_skill_modifier(c, ATTACK_SKILL, -6);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 3);
			break;
		/* Moonlight mead.  */
		case 2955:
			add_temp_skill_modifier(c, ATTACK_SKILL, -4);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 2);
			break;
		/* Moonlight mead(m).  */
		case 5749:
			add_temp_skill_modifier(c, ATTACK_SKILL, -6);
			add_temp_skill_modifier(c, STRENGTH_SKILL, 3);
			break;
		/* Axeman's folly.  */
		case 5751:
			add_temp_skill_modifier(c, WOODCUTTING_SKILL, 1);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -3);
			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			break;
		/* Axeman's folly(m).  */
		case 5753:
			add_temp_skill_modifier(c, WOODCUTTING_SKILL, 2);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -4);
			add_temp_skill_modifier(c, ATTACK_SKILL, -4);
			break;
		/* Slayer's respite.  */
		case 5759:
			add_temp_skill_modifier(c, SLAYER_SKILL, 1);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -2);
			add_temp_skill_modifier(c, ATTACK_SKILL, -2);
			break;
		/* Slayer's respite(m).  */
		case 5761:
			add_temp_skill_modifier(c, SLAYER_SKILL, 2);
			add_temp_skill_modifier(c, STRENGTH_SKILL, -3);
			add_temp_skill_modifier(c, ATTACK_SKILL, -3);
			break;
		default:
			break;
	}
}

/**
 * Handles the player eating food, the amount healed from it, and the stat
 * effects it may have had.
 * @param *c Client instance.
 * @param food_id Id of the food item consumed.
 * @param food_slot Inventory slot where the food is.
 */
void eat_food(struct client *c, int food_id, int food_slot)
{
	if (c->plr->last_action > 0)
	{
		return;
	}

	int heal_amount = get_food_healing_amount(food_id);
	int eaten_id = ITEMS[food_id]->item_consume_id;
	int eat_animation = 829;
	char food_name[100] = {'\0'};
	char eat_msg[100] = {'\0'};
	int x;


	/* Handle special foods.  */
	switch (food_id)
	{
		/* Strange fruit, restores 30% run energe and heals poison.  */
		case 464:
			cure_poison_player(c, 600);
			restore_run_energy(c, 30);
			break;
		/* Strange smelling Ugthanki kebab. Randomly heals either 0 or 9 when
		 * eaten. FIXME: Not accurate formula.  */
		case 1883:
			if (rand_int(0, 4) == 4)
			{
				heal_amount = 9;
			}
			else
			{
				heal_amount = 0;
			}
			break;
		/* Kebab. FIXME: Needs to be implemented. */
		case 1917:
			break;
		/* Cooked oomlie wrap, heals a varying amount based on HP level.  */
		/* FIXME: Not accurate, need to work out a formula for it.  */
		case 2343:
			heal_amount = 14;
			break;
		/* Snails.  */
		/* Thin snail meat.  */
		case 3369:
			heal_amount = rand_int(5, 7);
			break;
		/* Lean snail meat.  */
		case 3371:
			heal_amount = rand_int(5, 8);
			break;
		/* Fat snail meat.  */
		case 3373:
			heal_amount = rand_int(7, 9);
			break;
		/* Keg of beer. FIXME: Need to figure out the screen shake effect.  */
		case 3801:
			break;
		/* Super kebab. FIXME: Needs to be implemented. */
		case 4608:
			break;
		/* Strawberry.  */
		case 5504:
			heal_amount = 1 
				+ (get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]) * .06);
			break;
		/* Sweetcorn heals 10% of max health, handle specially here.  */
		case 5988:
			heal_amount = (get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]) * .1);
			break;
		/* Papaya fruit.  */
		case 5972:
			restore_run_energy(c, 5);
			break;
		/* Watermelon slices.  */
		case 5982:
			heal_amount = 
				+ (get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]) * .05);
			break;
		/* Gout tuber.  */
		case 6311:
			restore_run_energy(c, 50);
			break;
		/* White tree fruit.  */
		case 6469:
			restore_run_energy(c, rand_int(5, 10));
			break;
	}

	for (x = 0; x < 30; x++)
	{
		food_name[x] = to_lower(ITEMS[food_id]->name[x]);

		if (ITEMS[food_id]->name[x] == '\0')
		{
			break;
		}
	}
	
	get_food_consume_message(food_id, food_name, eat_msg, 100);
	packet_send_chatbox_message(c, eat_msg);
	player_play_animation(c, eat_animation);

	/* Prevents food from being eaten too quickly.  */
	c->plr->last_action = 3;

	/* Ensure player cannot attack while the eating animation is playing.  */
	c->plr->last_attack += 3;

	/* Heal if the food heals, damage if the food is unhealthy.  */
	if (heal_amount > 0)
	{
		if (c->plr->level[HITPOINTS_SKILL] 
			< get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]))
		{
			packet_send_chatbox_message(c, "It heals some health.");
			heal_player(c, heal_amount);
		}
	}
	else if (heal_amount == 0)
	{
		/* Item doesn't heal, must only do a stat effect.  */
	}
	else
	{
		deal_damage_to_player(c, (-1 * heal_amount), -1, 0, -1, -1, -1); 
	}

	/* Apply any stat bonuses the food may have.  */
	apply_consumable_boosts(c, food_id);

	/* If the food being eaten has a 1/2 state, change the food to it.  */
	if (eaten_id == -1)
	{
		delete_item_from_inv_slot(c, food_slot, 1);
	}
	else
	{
		transform_item(c, food_slot, eaten_id);
	}
}

int *search_item_names_and_desc_for_string(char *search_string, int *num_results)
{
	int x = 0;
	int *results;
	int results_size = 10;

	char *name_string = NULL;
	char *desc_string = NULL;

	*num_results = 0;

	results = calloc(results_size, sizeof(int));

	for (x = 0; x < PROTOCOL_MAX_ITEMS; x++)
	{
		/* Search the lowercase versions of name and desc.  */
		name_string = return_lowercase_string(ITEMS[x]->name);
		desc_string = return_lowercase_string(ITEMS[x]->desc);

		if (strstr(name_string, search_string) == NULL
			&& strstr(desc_string, search_string) == NULL)
		{
			free(name_string);
			free(desc_string);
			continue;
		}
		else
		{
			free(name_string);
			free(desc_string);

			results[(*num_results)] = x;
			(*num_results)++;

			if (*num_results == results_size)
			{
				results_size *= 2;
				results = realloc(results, results_size * sizeof(int));
			}
		}
	}

	return results;
}
