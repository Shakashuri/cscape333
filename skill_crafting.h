#ifndef _SKILL_CRAFTING_H_
#define _SKILL_CRAFTING_H_

int get_crafting_level_needed(int crafting_id);
float get_crafting_xp(int crafting_id);
int get_gem_cut_id(int crafting_id);
double get_gem_cut_chance(int gem_id, int crafting_level);
int get_gem_cut_emote(int crafting_id);
int get_leather_tanning_cost(int crafting_id);
int get_leather_tanned_id(int crafting_id);
int get_crafting_strung_id(int crafting_id);
int get_crafting_furnace_used(int id);

int get_crafting_furnace_item(struct client *c, float xp_gain);
void tan_leather(struct client *c, int leather_id);
void craft_snelm(struct client *c, int shell_id, int snelm_id, int crafting_id);
void craft_battlestaff(struct client *c, int staff_id, int staff_slot, 
					   int orb_id, int orb_slot, int crafting_id);
void cut_gem(struct client *c, int gem_id, int crafting_id);
void crafting_string_item(struct client *c, int item_id, int item_slot,
						  int string_id, int string_slot);
void put_jewelry_in_furnace(struct client *c, int crafting_id);
void init_crafting(struct client *c);
void handle_crafting(struct client *c);

#endif
