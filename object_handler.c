/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file object_handler.c
 * @brief Contains functions for handling different types of objects.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"

#include "client.h"
#include "combat.h"
#include "dialogue_handler.h"
#include "fileio.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "regions.h"
#include "rng.h"
#include "skills.h"
#include "skill_agility.h"
#include "skill_cooking.h"
#include "skill_firemaking.h"
#include "skill_magic.h"
#include "skill_mining.h"
#include "skill_runecrafting.h"
#include "skill_thieving.h"
#include "skill_woodcutting.h"

/** What direction the stair movement is, used in the handle_stairs 
 * function.  */
enum stair_direction
{
	/** Stair moves the player down a z level.  */
	STAIR_DOWN = 0,
	/** Stair moves the player up a z level.  */
	STAIR_UP,
	/** Stair doesn't have a set direction, but uses the last z movement of the
	 * player. Typically used when there are stairs on a middle level, and
	 * there are stairs above and below it. Allows for clicking the stairs
	 * to travel all the way up or down without having to use the right click
	 * menu.  */
	STAIR_LAST
};

/**
 * Returns the length size of an object.
 * @param object_id Object to check.
 * @return Size of object, -1 if invalid object id.
 */
int get_object_size_x(int object_id)
{
	if (object_id > -1 && object_id < PROTOCOL_MAX_OBJECTS)
	{
		return OBJECTS[object_id]->size_x;
	}

	return -1;
}

/**
 * Returns the width size of an object.
 * @param object_id Object to check.
 * @return Size of object, -1 if invalid object id.
 */
int get_object_size_y(int object_id)
{
	if (object_id > -1 && object_id < PROTOCOL_MAX_OBJECTS)
	{
		return OBJECTS[object_id]->size_y;
	}

	return -1;
}

static void spiral_staircase(struct client *c, int obj_face)
{
	switch (obj_face)
	{
		case OBJECT_FACE_WEST:
			c->plr->stored_tele_x = c->plr->action_x + 2;
			c->plr->stored_tele_y = c->plr->action_y;
			break;
		case OBJECT_FACE_NORTH:
			c->plr->stored_tele_x = c->plr->action_x;
			c->plr->stored_tele_y = c->plr->action_y - 1;
			break;
		case OBJECT_FACE_EAST:
			c->plr->stored_tele_x = c->plr->action_x - 1;
			c->plr->stored_tele_y = c->plr->action_y + 1;
			break;
		case OBJECT_FACE_SOUTH:
			c->plr->stored_tele_x = c->plr->action_x + 1;
			c->plr->stored_tele_y = c->plr->action_y + 2;
			break;
	}
}

static void spiral_staircase_down_only(struct client *c, int obj_face)
{
	switch (obj_face)
	{
		case OBJECT_FACE_WEST:
			c->plr->stored_tele_x = c->plr->action_x + 1;
			c->plr->stored_tele_y = c->plr->action_y;
			break;
		case OBJECT_FACE_NORTH:
			c->plr->stored_tele_x = c->plr->action_x;
			c->plr->stored_tele_y = c->plr->action_y - 1;
			break;
		case OBJECT_FACE_EAST:
			c->plr->stored_tele_x = c->plr->action_x - 1;
			c->plr->stored_tele_y = c->plr->action_y;
			break;
		case OBJECT_FACE_SOUTH:
			c->plr->stored_tele_x = c->plr->action_x;
			c->plr->stored_tele_y = c->plr->action_y + 1;
			break;
	}
}

static void long_staircase(struct client *c, int obj_face,
								int z_level_change_type, int max_size)
{
	if (z_level_change_type == STAIR_DOWN)
	{
		if (obj_face == OBJECT_FACE_WEST)
		{
			c->plr->stored_tele_y -= max_size;
		}
		else if (obj_face == OBJECT_FACE_NORTH)
		{
			c->plr->stored_tele_x -= max_size;
		}
		else if (obj_face == OBJECT_FACE_EAST)
		{
			c->plr->stored_tele_y += (max_size + 1);
		}
		else if (obj_face == OBJECT_FACE_SOUTH)
		{
			c->plr->stored_tele_x += (max_size + 1);
		}
	}
	else
	{
		if (obj_face == OBJECT_FACE_WEST)
		{
			c->plr->stored_tele_y += max_size;
		}
		else if (obj_face == OBJECT_FACE_NORTH)
		{
			c->plr->stored_tele_x += max_size;
		}
		else if (obj_face == OBJECT_FACE_EAST)
		{
			c->plr->stored_tele_y -= 1;
		}
		else if (obj_face == OBJECT_FACE_SOUTH)
		{
			c->plr->stored_tele_x -= 1;
		}
	}
}

/* Handle moving player when using stairs.  */
static void handle_stairs(struct client *c, int obj_id, int obj_face, 
						  int z_level_change_type)
{
	/* z level 0 - down
	 * z level 1 - up
	 * z level 2 - use last
	 */
	int stair_size_x = OBJECTS[obj_id]->size_x;
	int stair_size_y = OBJECTS[obj_id]->size_y;

	printf("STAIR STATS:\n");
	printf("\tID: %d Face: %d\n", obj_id, obj_face);
	printf("\tWorld X: %d Y: %d Z: %d\n", c->plr->action_x, c->plr->action_y,
		   c->plr->action_z);
	printf("\tSize x: %d Size y: %d\n", stair_size_x, stair_size_y);
	printf("Z level: %d\n", z_level_change_type);

	reset_player_combat(c);
	
	c->plr->stored_tele_x = c->plr->action_x;
	c->plr->stored_tele_y = c->plr->action_y;

	/* Handle stairs that don't have a direction, but work based off of the
	 * player's last direction.  */
	if (z_level_change_type == STAIR_LAST)
	{
		if (c->plr->last_tele_z_diff < 0)
		{
			z_level_change_type = STAIR_DOWN;
		}
		else if (c->plr->last_tele_z_diff >= 0)
		{
			z_level_change_type = STAIR_UP;
		}
	}

	/* Handle ladders based on type.  */
	if (z_level_change_type == STAIR_UP)
	{
		/* Check if there is a ladder object directly above, if so, change
		 * the player's level. Otherwise, it could be a dungeon. Only move
		 * to a dungeon location if the positioning of the stair makes sense.
		 */
		if (c->plr->action_z != 3)
		{
			if (get_collision(c->plr->action_x, c->plr->action_y, 
							  (c->plr->action_z + 1)) != 0
				|| c->plr->action_y <= 6400)
			{
				c->plr->stored_tele_z = c->plr->world_z + 1;
			}
			else
			{
				c->plr->stored_tele_y -= 6400;
			}
		}
		else
		{
			c->plr->stored_tele_y -= 6400;
		}
	}
	else if (z_level_change_type == STAIR_DOWN)
	{
		if (c->plr->action_z == 0)
		{
			c->plr->stored_tele_y += 6400;
		}
		else
		{
			c->plr->stored_tele_z = c->plr->world_z - 1;
		}
	}

	/* Handle stairs like the ones in falador.  */
	if (stair_size_y > stair_size_x)
	{
		long_staircase(c, obj_face, z_level_change_type, stair_size_y);
	}
	/* Handle stairs that are oddly shaped, needed for testing.  */
	else if (stair_size_y < stair_size_x)
	{
		printf("Y > X Stair!\n");
		printf("obj_id: %d\n", obj_id);
		switch (obj_id)
		{
			case 6088:
				long_staircase(c, obj_face, z_level_change_type,
							   (stair_size_x - 1));
				break;

			default:
				long_staircase(c, obj_face, z_level_change_type,
							   stair_size_x);
				break;
		}
	}
	/* Handle spiral stairs.  */
	else if (stair_size_x == stair_size_y)
	{
		if (stair_size_x == 1 && stair_size_y == 1)
		{
			/* TODO: De-hardcode this.  */
			if (obj_id != 6086)
			{
				spiral_staircase_down_only(c, obj_face);
			}
			else
			{
				long_staircase(c, obj_face, z_level_change_type, 2);
			}
		}
		else
		{
			switch (obj_id)
			{
				/* Spiral staircases.  */
				case 1740:
				case 1741:
				case 1745:
				case 3415:
					spiral_staircase_down_only(c, obj_face);
					break;

				/* Spiral staircases.  */
				case 1738:
				case 1739:
				case 1742:
				case 1743:
				case 3416:
				case 4568:
				case 4569:
					spiral_staircase(c, obj_face);
					break;
				default:
					long_staircase(c, obj_face, z_level_change_type, 2);
					break;
			}
		}
	}

	c->plr->teleport_timer = 0;
	c->plr->teleport_type = 5;
}

static void handle_ladders(struct client *c, int obj_x, int obj_y,
						   int obj_z, int z_level_change_type)
{
	reset_player_combat(c);
	player_play_animation(c, 828);
	
	c->plr->stored_tele_x = c->plr->world_x;
	c->plr->stored_tele_y = c->plr->world_y;
	c->plr->stored_tele_z = c->plr->world_z;

	if (z_level_change_type == STAIR_LAST)
	{
		if (c->plr->last_tele_z_diff < 0)
		{
			z_level_change_type = STAIR_DOWN;
		}
		else if (c->plr->last_tele_z_diff >= 0)
		{
			z_level_change_type = STAIR_UP;
		}
	}
	
	/* Handle ladders based on type.  */
	if (z_level_change_type == STAIR_UP)
	{
		/* Check if there is a ladder object directly above, if so, change
		 * the player's level. Otherwise, it could be a dungeon.  */
		if (c->plr->world_z != 3)
		{
			if (get_collision(obj_x, obj_y, (obj_z + 1)) != 0)
			{
				c->plr->stored_tele_z = c->plr->world_z + 1;
			}
			else
			{
				c->plr->stored_tele_y -= 6400;
			}
		}
		else
		{
			c->plr->stored_tele_y -= 6400;
		}
	}
	else if (z_level_change_type == STAIR_DOWN)
	{
		if (obj_z == 0)
		{
			c->plr->stored_tele_y += 6400;
		}
		else
		{
			c->plr->stored_tele_z = c->plr->world_z - 1;
		}
	}

	c->plr->teleport_timer = 1;
	c->plr->teleport_type = 5;
}

static void handle_portals(struct client *c)
{
	reset_player_combat(c);
	
	/* Portal from air altar.  */
	if (c->plr->action_x == 2841 && c->plr->action_y == 4828)
	{
		c->plr->tele_x = 2983;
		c->plr->tele_y = 3288;
		c->plr->tele_z = 0;
	}
	/* Portal from water altar.  */
	if (c->plr->action_x == 2727 && c->plr->action_y == 4832)
	{
		c->plr->tele_x = 3182;
		c->plr->tele_y = 3162;
		c->plr->tele_z = 0;
	}
	/* Portal from earth altar.  */
	if (c->plr->action_x == 2655 && c->plr->action_y == 4829)
	{
		c->plr->tele_x = 3303;
		c->plr->tele_y = 3477;
		c->plr->tele_z = 0;
	}
	/* Portal from fire altar.  */
	if (c->plr->action_x == 2574 && c->plr->action_y == 4850)
	{
		c->plr->tele_x = 3310;
		c->plr->tele_y = 3252;
		c->plr->tele_z = 0;
	}
	/* Portal from mind altar.  */
	if (c->plr->action_x == 2793 && c->plr->action_y == 4827)
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 2980;
		c->plr->tele_y = 3511;
		c->plr->tele_z = 0;
	}
	/* Portal from body altar.  */
	if (c->plr->action_x == 2523 && c->plr->action_y == 4825)
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 3051;
		c->plr->tele_y = 3443;
		c->plr->tele_z = 0;
	}
	/* Portals from cosmic altar.  */
	if ((c->plr->action_x == 2163 && c->plr->action_y == 4833)
		|| (c->plr->action_x == 2142 && c->plr->action_y == 4812)
		|| (c->plr->action_x == 2121 && c->plr->action_y == 4833)
		|| (c->plr->action_x == 2142 && c->plr->action_y == 4854))
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 2406;
		c->plr->tele_y = 4374;
		c->plr->tele_z = 0;
	}
	/* Portals from chaos altar.  */
	if ((c->plr->action_x == 2273 && c->plr->action_y == 4856)
		|| (c->plr->action_x == 2282 && c->plr->action_y == 4837))
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 3058;
		c->plr->tele_y = 3588;
		c->plr->tele_z = 0;
	}
	/* Portal from nature altar.  */
	if (c->plr->action_x == 2400 && c->plr->action_y == 4834)
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 2867;
		c->plr->tele_y = 3016;
		c->plr->tele_z = 0;
	}
	/* Portal from law altar.  */
	if (c->plr->action_x == 2464 && c->plr->action_y == 4817)
	{
		/* TODO FIXME These return coordinates may not be 100% correct.  */
		c->plr->tele_x = 2857;
		c->plr->tele_y = 3378;
		c->plr->tele_z = 0;
	}
}

/**
 * Returns what item is dropped from a plant object in the game world.
 * @param obj_id ID of the used object.
 * @return Item id that is received from the object.
 */
int get_plant_drop(int obj_id)
{
	switch (obj_id)
	{
		/* Cabbage.  */
		case 1161:
			return 1965;
		/* Potato.  */
		case 312:
			return 1942;	
		/* Nettles.  */
		case 1181:
		case 5253:
		case 5254:
		case 5255:
		case 5256:
		case 5257:
		case 5258:
			return 4241;
		/* Grain.  */
		case 313:
		case 5583:
		case 5584:
		case 5585:
			return 1947;
		/* Flax.  */
		case 2646:
			return 1779;
		default:
			printf("Unhandled plant drop %d!\n", obj_id);
			return 0;
	}
}

/**
 * If plant can be picked more than once, return 1.
 * @param obj_id Object id.
 * @return 0 on only one pick allowed.
 * @return 1 on multiple picks allowed.
 */
int get_plant_multi(int obj_id)
{
	switch (obj_id)
	{
		case 1161:
			return 0;
		/* Potato.  */
		case 312:
			return 0;
		/* Nettles.  */
		case 1181:
			return 0;
		case 5253:
		case 5254:
		case 5255:
		case 5256:
		case 5257:
		case 5258:
			return 1;
		/* Grain.  */
		case 313:
		case 5583:
		case 5584:
		case 5585:
			return 0;
		/* Flax.  */
		case 2646:
			return 1;
		default:
			printf("Unhandled plant drop %d!\n", obj_id);
			return 0;
	}
}

/**
 * Returns respawn ticks for a plant object.
 * @param obj_id Object id.
 * @return Number of server ticks before plant respawns.
 */
int get_plant_respawn_ticks(int obj_id)
{
	switch (obj_id)
	{
		/* Cabbage.  */
		case 1161:
			return 2;
		/* Potato.  */
		case 312:
			return 10;	
		/* Nettles.  */
		case 1181:
		case 5253:
		case 5254:
		case 5255:
		case 5256:
		case 5257:
		case 5258:
			return 10;
		/* Grain.  */
		case 313:
		case 5583:
		case 5584:
		case 5585:
			return 10;
		/* Flax.  */
		case 2646:
			return 10;
		default:
			printf("Unhandled plant respawn %d!\n", obj_id);
			return 2;
	}
}

/**
 * Handles when a player tries to harvest something from a plant object.
 * @param *c Client instance.
 */
void pick_plant(struct client *c)
{
	int multi = get_plant_multi(c->plr->action_id);
	int drop = get_plant_drop(c->plr->action_id);
	int respawn = get_plant_respawn_ticks(c->plr->action_id);
	int obj_index = 0;
	int null_obj_id = 0;
	char msg[150];

	if (get_free_inv_slots(c) == 0)
	{
		packet_send_chatbox_message(c, "You don't have enough space for that!");
		return;
	}

	/* If player is picking nettles, but doesn't have gloves on, 
	   deal 2 damage (green hitsplat, but not poison) 
	   and don't give nettles.  */
	if (c->plr->action_id == 1181 
		|| (c->plr->action_id >= 5253 && c->plr->action_id <= 5258))
	{
		if (c->plr->equipment[HANDS_SLOT] == -1)
		{
			if (c->plr->update_masks[HIT_UPDATE] == 1)
			{
				c->plr->poison_hit2 = 1;
			}
			else
			{
				c->plr->poison_hit = 1;
			}
			deal_damage_to_player(c, 2, DMG_TYPE_STAB, 3, -1, -1, -1);
			packet_send_chatbox_message(c, "Ow! My fingers!");
			return;
		}
	}

	player_play_animation(c, 827);
	add_item_to_inv_delay(c, drop, 1, -1, 0);
	
	/* Send a pick up message based on item.  */
	if (drop == 4241)
	{
		snprintf(msg, 150, "You pick a handful of %s.", ITEMS[drop]->name); 
	}
	else
	{
		snprintf(msg, 150, "You pick a %s.", ITEMS[drop]->name); 
	}
	packet_send_chatbox_message(c, msg);



	/* If it is rolled to be removed, or it is a one pick plant, finish.  */
	if ((rand_int(1, 8) == 8) || multi == 0)
	{
		/* Check if an object already exists at the plant's
		   location. If not, create it.  */
		obj_index = get_object_spawn_index(c->plr->action_x, 
					c->plr->action_y, c->plr->world_z);
		
		/* There is not an object at the position, create it.  */
		if (obj_index == -1)
		{
			null_obj_id = add_to_object_list(c->plr->action_id, 10, 
							   c->plr->action_x, c->plr->action_y, 
							   c->plr->world_z, 0, respawn + 1, 0);
			set_object_transforms(null_obj_id, 448, respawn);
			spawn_object_globally(null_obj_id);
		}
	}
}

static void get_new_door_coord(int door_type, int face, 
							   int is_open, int *new_x, 
							   int *new_y, int *new_face)
{
	/* Regular , right hinged door.  */
	if (door_type == DOOR_TYPE_RIGHT || door_type == DOOR_TYPE_AUTO_RIGHT)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				/* West edge to North edge.  */
				case 0:
					(*new_face)++;
					(*new_x)--;
					break;
				/* North edge to East edge.  */
				case 1:
					(*new_face)++;
					(*new_y)++;
					break;
				/* East edge to South edge.  */
				case 2:
					(*new_face)++;
					(*new_x)++;
					break;
				/* South edge to West edge.  */
				case 3:
					(*new_face) = 0;
					(*new_y)--;
					break;
			}
		}
		else
		{
			switch (face)
			{
				case 0:
					(*new_face) = 3;
					(*new_y)++;
					break;
				case 1:
					(*new_face)--;
					(*new_x)++;
					break;
				case 2:
					(*new_face)--;
					(*new_y)--;
					break;
				case 3:
					(*new_face)--;
					(*new_x)--;
					break;
			}
		}
	}
	/* Left hinged doors, including the hinged part of fences.  */
	else if (door_type == DOOR_TYPE_LEFT 
			 || door_type == DOOR_TYPE_AUTO_LEFT
			 || door_type == DOOR_TYPE_FENCE_HINGED)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				case 0:
					(*new_face) = 3;
					(*new_x)--;
					break;
				case 1:
					(*new_face) = 0;
					(*new_y)++;
					break;
				case 2:
					(*new_face) = 1;
					(*new_x)++;
					break;
				case 3:
					(*new_face) = 2;
					(*new_y)--;
					break;
			}
		}
		else
		{
			switch (face)
			{
				case 0:
					(*new_face) = 3;
					(*new_y)++;
					break;
				case 1:
					(*new_face)--;
					(*new_x)++;
					break;
				case 2:
					(*new_face)--;
					(*new_y)--;
					break;
				case 3:
					(*new_face)--;
					(*new_x)--;
					break;
			}
		}
	}
	/* Free part of fences.  */
	else if (door_type == DOOR_TYPE_FENCE_UNHINGED)
	{
		if (is_open == 0)
		{
			switch (face)
			{
                case 0:
					(*new_face) = 3;
					(*new_x) -= 2;
					(*new_y)--;
					break;
                case 1:
					(*new_face) = 0;
					(*new_y) += 2;
					(*new_x)--;
					break;
                case 2:
					(*new_face) = 1;
					(*new_x) += 2;
					(*new_y)++;
					break;
                case 3:
					(*new_face) = 2;
					(*new_y) -= 2;
					(*new_x)++;
					break;
			}
		}
		else
		{
			/* XXX: There are no fences that are open by default, so this is
			   not used.  */
			switch (face)
			{
				/* FIXME  */
				case 0:
					(*new_face) = 3;
					(*new_y)++;
					break;
				/* FIXME  */
				case 1:
					(*new_face)--;
					(*new_x)++;
					break;
				/* FIXME  */
				case 2:
					(*new_face)--;
					(*new_y)--;
					break;
				/* FIXME  */
				case 3:
					(*new_face)--;
					(*new_x)--;
					break;
			}
		}
	}
	/* Non moving door.  */
	else if (door_type == DOOR_TYPE_NONMOVING)
	{
		printf("no move\n");
	}
}

static int check_for_ddoor(int door_type, int ddoor_id, int face, int is_open,
						   int door_x, int door_y, int door_z,
						   int *new_x, int *new_y, int *dd_face, int *new_reg_index)
{
	int reg_id = 0;
	int obj_index = 0;

	/* Setup defaults here.  */
	*new_x = door_x;
	*new_y = door_y;

	printf("type: %d open: %d face: %d\n", door_type, is_open, face);
	/* Regular doors.  */
	if (door_type == DOOR_TYPE_RIGHT || door_type == DOOR_TYPE_AUTO_RIGHT)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				case 0:
					(*new_y)--;
					break;
				case 1:
					(*new_x)--;
					break;
				case 2:
					(*new_y)++;
					break;
				case 3:
					(*new_x)++;
					break;
			}
		}
		else
		{
			switch (face)
			{
				case 0:
					(*new_x)++;
					break;
				case 1:
					(*new_y)--;
					break;
				case 2:
					(*new_x)--;
					break;
				case 3:
					(*new_y)++;
					break;
			}
		}
	}
	/* Left doors.  */
	else if (door_type == DOOR_TYPE_LEFT || door_type == DOOR_TYPE_AUTO_LEFT)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				case 0:
					(*new_y)++;
					break;
				case 1:
					(*new_x)++;
					break;
				case 2:
					(*new_y)--;
					break;
				case 3:
					(*new_x)--;
					break;
			}
		}
		else
		{
			switch (face)
			{
				case 0:
					(*new_x)++;
					break;
				case 1:
					(*new_y)--;
					break;
				case 2:
					(*new_x)--;
					break;
				case 3:
					(*new_y)++;
					break;
			}
		}
	}
	/* Free part of fences.  */
	if (door_type == DOOR_TYPE_FENCE_UNHINGED)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				case 0:
					(*new_y)--;
					break;
				case 1:
					(*new_x)--;
					break;
				case 2:
					(*new_y)++;
					break;
				case 3:
					(*new_x)++;
					break;
			}
		}
		else
		{
			/* XXX: This is never used within normal usage as far as 
			   I can tell, may need to be updated later.  */
			printf("freefence: %d\n", face);
			switch (face)
			{
				/* FIXME */
				case 0:
					(*new_x)++;
					break;
				/* FIXME */
				case 1:
					(*new_y)--;
					break;
				/* FIXME */
				case 2:
					(*new_x)--;
					break;
				/* FIXME */
				case 3:
					(*new_y)++;
					break;
			}
		}
	}
	/* Hinged part of fences.  */
	else if (door_type == DOOR_TYPE_FENCE_HINGED)
	{
		if (is_open == 0)
		{
			switch (face)
			{
				case 0:
					(*new_y)++;
					break;
				case 1:
					(*new_x)++;
					break;
				case 2:
					(*new_y)--;
					break;
				case 3:
					(*new_x)--;
					break;
			}
		}
		else
		{
			switch (face)
			{
				case 0:
					(*new_y)++;
					break;
				case 1:
					(*new_x)++;
					break;
				case 2:
					(*new_y)--;
					break;
				case 3:
					(*new_x)--;
					break;
			}
		}
	}

	printf("orig\n");
	printf("x: %d y: %d z: %d\n", door_x, door_y, door_z);
	printf("x: %d y: %d z: %d\n", *new_x, *new_y, door_z);

	/* If the door is shut, look into the original object spawns.  */
	if (is_open == 0)
	{
		reg_id = get_region_id(*new_x, *new_y);
		*new_reg_index = get_region_index(reg_id);
		obj_index = get_region_object_index(*new_reg_index, *new_x, *new_y, 
											   door_z, ddoor_id);
		*dd_face = get_region_object_face(*new_reg_index, *new_x, *new_y, 
											   door_z, ddoor_id);
	}
	else
	{
		*new_reg_index = -1;
		obj_index = get_object_spawn_index(*new_x, *new_y, door_z);
		*dd_face = OBJECT_SPAWNS[obj_index]->face;
	}

	return obj_index;
}

/**
 * Toggles door collision for the specified square and object face.
 * @param x World x coordinate.
 * @param y World y coordinate.
 * @param z World z coordinate.
 * @param face Door/wall facing position.
 * @param type Object type, if diagonal special handling is done.
 */
static void toggle_door_face_collision(int x, int y, int z, int face, int type)
{
	if (type == OBJECT_TYPE_DIAGONAL)
	{
		mod_collision(x, y, z, OCCUPIED_MASK);
		return;
	}

	switch (face)
	{
		case 0:
			mod_collision(x, y, z, (W_WALL_MASK | W_BLOCKED_MASK));
			mod_collision(x - 1, y, z, (E_WALL_MASK | E_BLOCKED_MASK));
			break;
		case 1:
			mod_collision(x, y, z, (N_WALL_MASK | N_BLOCKED_MASK));
			mod_collision(x, y + 1, z, (S_WALL_MASK | S_BLOCKED_MASK));
			break;
		case 2:
			mod_collision(x, y, z, (E_WALL_MASK | E_BLOCKED_MASK));
			mod_collision(x + 1, y, z, (W_WALL_MASK | W_BLOCKED_MASK));
			break;
		case 3:
			mod_collision(x, y, z, (S_WALL_MASK | S_BLOCKED_MASK));
			mod_collision(x, y - 1, z, (N_WALL_MASK | N_BLOCKED_MASK));
			break;
	}
}

/**
 * Handles the opening and closing of doors.
 * @param door_id Object ID of the door to close.
 * @param obj_type Object type of the door, determines if door is a diagonal
 * set one or not.
 * @param obj_face Direction the object is facing. @see OBJECT_DATA
 * @param index Index of the object in its array.
 * @param region_index If the door hasn't been moved from its default state,
 * this is the region index it is located in.
 * @param door_x World x position of the door.
 * @param door_y World y position of the door.
 * @param door_z World z position of the door.
 * @param check_ddoor If 1, check to see if there is a matching door right next
 * to this one to open as well.
 * @param mod_collision If 1, collision should be changed when the doors open,
 * 0 means no collision changes should be done, used for auto doors.
 */
static int move_door_base(int door_id, int obj_type, int obj_face, int index, 
						   int region_index, int door_x, int door_y, 
						   int door_z, int check_ddoor, int mod_collision)
{
	if (door_id < 0 || door_id >= PROTOCOL_MAX_OBJECTS)
	{
		return -1;
	}

	int door_type = OBJECTS[door_id]->door_type;
	int is_double_door = OBJECTS[door_id]->is_double_door;
	int is_open = OBJECTS[door_id]->is_open;
	int id_diff = OBJECTS[door_id]->id_diff;
	int new_door_id = 0;

	/* Find what the door transforms into when opened/closed.  */
	new_door_id = door_id + id_diff;
	
	/* For when a new door spawn needs to be created.  */
	int new_x = door_x;
	int new_y = door_y;
	int new_face = obj_face;
	
	/* Used for when doors are moved from their default positions.  */
	int blank_index = -1;
	int new_door_index = -1;

	/* Double door checking stuff.  */
	int ddoor_x = 0;
	int ddoor_y = 0;
	/* Needed to know what face the paired door is at. Otherwise, collision
	 * issues will occur.  */
	int dd_face = 0;
	int ddoor_reg_index = 0;
	int ddoor_obj_index = 0;

	/* Try and find a double door.  */
	if (check_ddoor == 1) 
	{
		/* If these criteria are not met, skip checking for a double door.  */
		if (is_double_door == 0 || obj_type == OBJECT_TYPE_DIAGONAL)
		{
			check_ddoor = 0;
		}
		else
		{
			ddoor_obj_index = check_for_ddoor(door_type, is_double_door, 
											  obj_face,
											  is_open,
											  door_x, door_y, door_z,
											  &ddoor_x, &ddoor_y, 
											  &dd_face,
											  &ddoor_reg_index);

			/* If we found a double door, ensure it gets moved too. Otherwise,
			   skip.  */
			if (ddoor_obj_index != -1)
			{
				check_ddoor = 1;
			}
			else
			{
				check_ddoor = 0;
			}
		}
	}

	/* If state has not changed from default.  */
	if (region_index != -1)
	{
		if (door_type != DOOR_TYPE_NONMOVING)
		{
			/* Create the overwriting blank object to hide the default
			   door, then spawn it to clients.  */
			blank_index = add_to_object_list(BLANK_OBJECT_ID, 
											 obj_type,
											 door_x, door_y, door_z,
											 obj_face, -1, 1);
		}

		/* Find the new position the door should be at when opened.  */
		get_new_door_coord(door_type, obj_face, is_open, &new_x, 
						   &new_y, &new_face);

		new_door_index = add_to_object_list(new_door_id, 
											obj_type,
											new_x, new_y, door_z,
											new_face, -1, 1);

		
		if (door_type != DOOR_TYPE_NONMOVING)
		{
			/* Tie both of these together for later.  */
			OBJECT_SPAWNS[blank_index]->paired_object = new_door_index;
			OBJECT_SPAWNS[new_door_index]->paired_object = blank_index;
		}

		if (mod_collision == 1)
		{
			toggle_door_face_collision(door_x, door_y, door_z, obj_face, 
									   obj_type);
			toggle_door_face_collision(new_x, new_y, door_z, new_face, 
									   obj_type);
		}

		/* Spawn door at opened position, and place blank object on its
		   default position to hide it.  */
		spawn_object_globally(new_door_index);

		/* Only spawn the blank object if needed.  */
		if (door_type != DOOR_TYPE_NONMOVING)
		{
			spawn_object_globally(blank_index);
		}
	}
	else
	{
		/* Get the location of the overwriting blank object.  */
		if (door_type != DOOR_TYPE_NONMOVING)
		{
			int p_obj = OBJECT_SPAWNS[index]->paired_object;
			int p_x = OBJECT_SPAWNS[p_obj]->world_x;
			int p_y = OBJECT_SPAWNS[p_obj]->world_y;
			int p_z = OBJECT_SPAWNS[p_obj]->world_z;
			new_face = OBJECT_SPAWNS[p_obj]->face;

			delete_from_object_list(p_obj, 1);
			/* Spawn the original door.  */
			spawn_default_object_globally(p_x, p_y, p_z);
			delete_from_object_list(index, 1);

			/* Apply the collision from the moved door.  */
			if (mod_collision == 1)
			{
				toggle_door_face_collision(door_x, door_y, door_z,
										   obj_face, obj_type);
				toggle_door_face_collision(p_x, p_y, p_z, new_face, 
										   obj_type);
			}
		}
		else
		{
			/* Special handling for when the door doesn't move.  */
			int p_x = OBJECT_SPAWNS[index]->world_x;
			int p_y = OBJECT_SPAWNS[index]->world_y;
			int p_z = OBJECT_SPAWNS[index]->world_z;

			spawn_default_object_globally(p_x, p_y, p_z);
			delete_from_object_list(index, 0);

			if (mod_collision == 1)
			{
				toggle_door_face_collision(p_x, p_y, p_z, obj_face, 
										   obj_type);
			}
		}
	}
		
	/* If we did find a double door, move it as well.  */
	if (check_ddoor == 1)
	{
		move_door_base(is_double_door, obj_type, dd_face, ddoor_obj_index,
					   ddoor_reg_index, ddoor_x, ddoor_y, door_z, 0, 
					   mod_collision);
	}

	return new_door_index;
}

/**
 * Handles the opening and closing of doors. Applies collision changes when
 * opening and closing, default door behavior. 
 * @see move_door_no_collision_change
 * @param door_id Object ID of the door to close.
 * @param obj_type Object type of the door, determines if door is a diagonal
 * set one or not.
 * @param obj_face Direction the object is facing. @see OBJECT_DATA
 * @param index Index of the object in its array.
 * @param region_index If the door hasn't been moved from its default state,
 * this is the region index it is located in.
 * @param door_x World x position of the door.
 * @param door_y World y position of the door.
 * @param door_z World z position of the door.
 * @param check_ddoor If 1, check to see if there is a matching door right next
 * to this one to open as well.
 * @return Returns the index of the new door object in the OBJECT_SPAWNS array
 * if it worked, -1 if not needed, or door is being closed.
 */
int move_door(int door_id, int obj_type, int obj_face, int index, 
			   int region_index, int door_x, int door_y, int door_z,
			   int check_ddoor)
{
	return move_door_base(door_id, obj_type, obj_face, index,
				   region_index, door_x, door_y, door_z, check_ddoor, 
				   1);
}

/**
 * Handles the opening and closing of doors, but doesn't change any collision
 * when moving them. This is used for autodoors.
 * @see move_door
 * @param door_id Object ID of the door to close.
 * @param obj_type Object type of the door, determines if door is a diagonal
 * set one or not.
 * @param obj_face Direction the object is facing. @see OBJECT_DATA
 * @param index Index of the object in its array.
 * @param region_index If the door hasn't been moved from its default state,
 * this is the region index it is located in.
 * @param door_x World x position of the door.
 * @param door_y World y position of the door.
 * @param door_z World z position of the door.
 * @param check_ddoor If 1, check to see if there is a matching door right next
 * to this one to open as well.
 * @return Returns the index of the new door object in the OBJECT_SPAWNS array
 * if it worked, -1 if not needed, or door is being closed.
 */
int move_door_no_collision_change(int door_id, int obj_type, int obj_face,
								   int index, int region_index, int door_x,
								   int door_y, int door_z, int check_ddoor)
{
	return move_door_base(door_id, obj_type, obj_face, index,
				   region_index, door_x, door_y, door_z, check_ddoor, 
				   0);
}

/**
 * Moves the player through the auto door and handles their proper facing 
 * direction when the get through.
 * @param *c Client instance.
 * @param obj_face Facing direction of the door.
 * @param door_x World X coordinate of the door.
 * @param door_y World Y coordinate of the door.
 * @todo May need more testing.
 */
void set_player_movement_auto_door(struct client *c, int obj_face, 
								   int door_x, int door_y)
{
	/* Handle doors that are on the same square as the player.  */
	if (c->plr->world_x == door_x && c->plr->world_y == door_y)
	{
		switch (obj_face)
		{
			case OBJECT_FACE_WEST:
				player_move_one_square(c, door_x - 1, door_y);
				player_turn_to_world_coord(c, door_x - 2, door_y);
				break;
			case OBJECT_FACE_NORTH:
				player_move_one_square(c, door_x, door_y + 1);
				player_turn_to_world_coord(c, door_x, door_y + 2);
				break;
			case OBJECT_FACE_EAST:
				player_move_one_square(c, door_x + 1, door_y);
				player_turn_to_world_coord(c, door_x + 2, door_y);
				break;
			case OBJECT_FACE_SOUTH:
				player_move_one_square(c, door_x, door_y - 1);
				player_turn_to_world_coord(c, door_x, door_y - 2);
				break;
		}
		
		return;
	}

	if (obj_face == OBJECT_FACE_WEST || obj_face == OBJECT_FACE_EAST)
	{
		if (c->plr->world_x < door_x)
		{
			if (obj_face == OBJECT_FACE_WEST)
			{
				player_move_one_square(c, door_x, door_y);
				player_turn_to_world_coord(c, door_x + 2, door_y);
			}
			else
			{
				player_move_one_square(c, door_x + 1, door_y);
				player_turn_to_world_coord(c, door_x + 2, door_y);
			}
		}
		else
		{
			if (obj_face == OBJECT_FACE_WEST)
			{
				player_move_one_square(c, door_x - 1, door_y);
				player_turn_to_world_coord(c, door_x - 2, door_y);
			}
			else
			{
				player_move_one_square(c, door_x, door_y);
				player_turn_to_world_coord(c, door_x - 2, door_y);
			}
		}
	}
	if (obj_face == OBJECT_FACE_NORTH || obj_face == OBJECT_FACE_SOUTH)
	{
		if (c->plr->world_y < door_y)
		{
			if (obj_face == OBJECT_FACE_NORTH)
			{
				player_move_one_square(c, door_x, door_y + 1);
				player_turn_to_world_coord(c, door_x, door_y + 2);
			}
			else
			{
				player_move_one_square(c, door_x, door_y);
				player_turn_to_world_coord(c, door_x, door_y + 2);
			}
		}
		else
		{
			if (obj_face == OBJECT_FACE_NORTH)
			{
				player_move_one_square(c, door_x, door_y);
				player_turn_to_world_coord(c, door_x, door_y - 2);
			}
			else
			{
				player_move_one_square(c, door_x, door_y - 1);
				player_turn_to_world_coord(c, door_x, door_y - 2);
			}
		}
	}
}

/**
 * Handles opening of an auto door and moves the player through it.
 * @param *c Client instance.
 * @param door_id ID of the autodoor.
 * @param obj_type Object type of the door.
 * @param obj_face Facing direction of the door.
 * @param index Index of the door in its objects array.
 * @param region_index Index of the region the door is located in, used for
 * doors that haven't moved from their default location.
 * @param door_x World x coordinate of the door.
 * @param door_y World y coordinate of the door.
 * @param door_z World z coordinate of the door.
 */
static void handle_auto_door_base(struct client *c, int door_id, int obj_type, 
								  int obj_face, int index, int region_index, 
								  int door_x, int door_y, int door_z)
{
	int check_for_empty = -1;

	check_for_empty = get_object_spawn_index(door_x, door_y, door_z);

	set_player_movement_auto_door(c, obj_face, door_x, door_y);

	freeze_player(c, EFFECT_FREEZE_AUTODOOR, 90);

	if (check_for_empty == -1)
	{
		c->plr->action_store_one 
			= move_door_no_collision_change(door_id, obj_type,
				obj_face, index, region_index, door_x, door_y, door_z, 1);	
	}
}

/**
 * Handles opening of an auto door and moves the player through it. Works the
 * same as handle_auto_door, just doesn't require as much information. Used for
 * auto doors outside of the handle_objects function.
 * @param *c Client instance.
 * @param door_x World x coordinate of the door.
 * @param door_y World y coordinate of the door.
 * @param door_z World z coordinate of the door.
 */
void handle_auto_door_at_pos(struct client *c, int door_x, int door_y, 
							 int door_z)
{
	int region_index = get_region_index(get_region_id(door_x, door_y));
	int index = -1;
	int door_id, obj_face, obj_type;

	if (region_index == -1)
	{
		return;
	}

	index = get_region_object_data(region_index, door_x, door_y, door_z, 
								   &door_id, &obj_face, &obj_type);
	
	if (index == -1)
	{
		return;
	}
	
	handle_auto_door_base(c, door_id, obj_type, obj_face, index, region_index, 
						  door_x, door_y, door_z);
}

/**
 * Closes the auto door located at the index in OBJECT_SPAWNS. Makes sure that
 * the door is only closed if it hasn't already been, prevent errors.
 * @param index Index of the door object in the OBJECT_SPAWNS array.
 */
void close_auto_door(int index)
{
	/* make sure the index is valid.  */
	if (index == -1)
	{
		return;
	}

	int obj_id = OBJECT_SPAWNS[index]->id;
	int obj_face = OBJECT_SPAWNS[index]->face;
	int obj_type = OBJECT_SPAWNS[index]->type;
	int door_x = OBJECT_SPAWNS[index]->world_x;
	int door_y = OBJECT_SPAWNS[index]->world_y;
	int door_z = OBJECT_SPAWNS[index]->world_z;

	/* If object id is -1, return. Door has already been closed or lost.  */
	if (obj_id == -1)
	{
		return;
	}

	move_door_no_collision_change(obj_id, obj_type,
		obj_face, index, -1, door_x, door_y, door_z, 1);	
}

/**
 * Based on object id, handles actions for it.
 * @param *c Client that used the object.
 * @param obj_id Object id used.
 */
void handle_objects(struct client *c, int obj_id)
{
	if (is_player_frozen(c, 1))
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* For output.  */
	char return_msg[100];

	/* Check if player is not moving before checking in full.  */
	if (c->plr->walking_dir == -1 && c->plr->running_dir == -1
		&& c->plr->walking_queue_writing_offset == 0)
	{
		int obj_in_instanced = 0;
		int obj_index = -1;
		int obj_face = -1;
		int obj_type = -1;
		int rv = 0, rv2 = 0;

		/* Find the object in the game world.  */
		obj_index = find_object_at_position(c, c->plr->action_x, 
				c->plr->action_y, c->plr->world_z, &obj_in_instanced, 
				&obj_face, &obj_type);

		c->plr->action_region_index 
			= get_region_index(get_region_id(c->plr->action_x, 
											 c->plr->action_y));

		/* If find_object_at_position returned -1, couldn't find the object
		 * anywhere.  */
		if (obj_index == -1)
		{
			printf("Object does not exist anywhere!\n");
			set_player_state(c, STATE_IDLE);
			return;
		}
	
		/* If not within range, print out position and return.  */
		/* FIXME: Test replacing this with checking if the player's current
		 * position matches the best perpendicular square for this object.  */
		if (check_within_object_range(c->plr->world_x, c->plr->world_y, 
									  c->plr->action_x, c->plr->action_y, 
									  c->plr->action_range_x, 
									  c->plr->action_range_y, 
									  obj_index, obj_face) == 0)
		{
			printf("WX: %d WY: %d AX: %d AY: %d ARX: %d ARY: %d IND: %d\n",
				   c->plr->world_x, c->plr->world_y, 
				   c->plr->action_x, c->plr->action_y, 
				   c->plr->action_range_x, 
				   c->plr->action_range_y, obj_index);
			printf("F: %d\n", obj_face);
			packet_send_chatbox_message(c, "You can't reach that.");
			set_player_state(c, STATE_IDLE);
			return;
		}

		player_turn_to_object(c, c->plr->action_x, c->plr->action_y, 
							c->plr->action_range_x, c->plr->action_range_y,
							obj_face);

		/* First action of an object.  */
		if (c->plr->action_click_num == 1)
		{
			switch (obj_id)
			{
		/* WOODCUTTING HANDLING.  */
			/* ALL TREES GO HERE.  */
				/* Regular trees.  */
				case 1276:
				case 1277:
				case 1278:
				case 1279:
				case 1280:
				case 3033:
				case 3034:
				/* Dead trees.  */
				case 1282:
				case 1283:
				case 1284:
				case 1285:
				case 1286:
				case 1289:
				case 1291:
				case 1365:
				case 1383:
				case 1384:
				case 3035:
				case 3036:
				case 5902:
				case 5903:
				case 5904:
				/* Evergreen.  */
				case 1315:
				case 1316:
				case 1318:
				case 1319:
				case 1330:
				case 1331:
				case 1332:
				case 3879:
				case 3881:
				case 3882:
				case 3883:
				/* Jungle trees, might need special handling.  */
				case 2887:
				case 2889:
				case 2890:
				case 4818:
				case 4820:
				/* Jungle bushes?  */
				case 2892:
				case 2893:
					c->plr->action_id_type = WOODCUTTING_REGULAR;
					init_woodcutting(c);
					break;
				/* Achey trees.  */
				case 2023:
					c->plr->action_id_type = WOODCUTTING_ACHEY;
					init_woodcutting(c);
					break;
				/* Oak trees.  */
				case 1281:
				case 3037:
					c->plr->action_id_type = WOODCUTTING_OAK;
					init_woodcutting(c);
					break;
				/* Willow trees.  */
				case 1308:
					c->plr->action_id_type = WOODCUTTING_WILLOW;
					init_woodcutting(c);
					break;
				/* Teak trees.  */
				case 9036:
				case 9039:
					c->plr->action_id_type = WOODCUTTING_TEAK;
					init_woodcutting(c);
					break;
				/* Maple trees.  */
				case 1307:
				case 4674:
					c->plr->action_id_type = WOODCUTTING_MAPLE;
					init_woodcutting(c);
					break;
				/* Hollow trees.  */
				case 2289:
				case 4060:
				case 5551:
				case 5552:
				case 5553:
					c->plr->action_id_type = WOODCUTTING_HOLLOW;
					init_woodcutting(c);
					break;
				/* Yew trees.  */
				case 1309:
					c->plr->action_id_type = WOODCUTTING_YEW;
					init_woodcutting(c);
					break;
				/* Magic trees.  */
				case 1306:
					c->plr->action_id_type = WOODCUTTING_MAGIC;
					init_woodcutting(c);
					break;
			
			/* MINING HANDLING.  */
				
				/* Rune Essence.  */
				case 2491:
					c->plr->action_id_type = MINING_RUNE_ESSENCE;
					init_mining(c);
					break;
				/* Clay.  */
				case 2108:
				case 2109:
					c->plr->action_id_type = MINING_CLAY;
					init_mining(c);
					break;
				/* Copper.  */
				case 2090:
				case 2091:
					c->plr->action_id_type = MINING_COPPER;
					init_mining(c);
					break;
				/* Tin.  */
				case 2094:
				case 2095:
					c->plr->action_id_type = MINING_TIN;
					init_mining(c);
					break;
				/* Iron.  */
				case 2092:
				case 2093:
					c->plr->action_id_type = MINING_IRON;
					init_mining(c);
					break;
				/* Silver.  */
				case 2100:
				case 2101:
					c->plr->action_id_type = MINING_SILVER;
					init_mining(c);
					break;
				/* Coal.  */
				case 2096:
				case 2097:
					c->plr->action_id_type = MINING_COAL;
					init_mining(c);
					break;
				/* Gold.  */
				case 2098:
				case 2099:
					c->plr->action_id_type = MINING_GOLD;
					init_mining(c);
					break;
				/* Mithril.  */
				case 2102:
				case 2103:
					c->plr->action_id_type = MINING_MITHRIL;
					init_mining(c);
					break;
				/* Adamant.  */
				case 2104:
				case 2105:
					c->plr->action_id_type = MINING_ADAMANT;
					init_mining(c);
					break;
				/* Rune.  */
				case 2106:
				case 2107:
					c->plr->action_id_type = MINING_RUNE;
					init_mining(c);
					break;
				/* Blurite.  */
				case 2110:
					c->plr->action_id_type = MINING_BLURITE;
					init_mining(c);
					break;
				/* Gem rock.  */
				case 2111:
					c->plr->action_id_type = MINING_GEM;
					init_mining(c);
					break;
				/* Furnace.  */
				case 2781:
					send_smelting_choice_menu(c);
					break;
			/* Runecrafting handling.  */
				/* Air ruins.  */
				case 2452:
					if (c->plr->equipment[HAT_SLOT] == 5527)
					{
						c->plr->stored_tele_x = 2841;
						c->plr->stored_tele_y = 4830;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Mind ruins.  */
				case 2453:
					if (c->plr->equipment[HAT_SLOT] == 5529)
					{
						c->plr->stored_tele_x = 2793;
						c->plr->stored_tele_y = 4829;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Water ruins.  */
				case 2454:
					if (c->plr->equipment[HAT_SLOT] == 5531)
					{
						c->plr->stored_tele_x = 2725;
						c->plr->stored_tele_y = 4832;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Earth ruins.  */
				case 2455:
					if (c->plr->equipment[HAT_SLOT] == 5535)
					{
						c->plr->stored_tele_x = 2657;
						c->plr->stored_tele_y = 4830;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Fire ruins.  */
				case 2456:
					if (c->plr->equipment[HAT_SLOT] == 5537)
					{
						c->plr->stored_tele_x = 2576;
						c->plr->stored_tele_y = 4848;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Body ruins.  */
				case 2457:
					if (c->plr->equipment[HAT_SLOT] == 5533)
					{
						c->plr->stored_tele_x = 2520;
						c->plr->stored_tele_y = 4839;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Cosmic ruins.  */
				case 2458:
					if (c->plr->equipment[HAT_SLOT] == 5539)
					{
						switch (rand_int(0, 3))
						{
							/* East portal.  */
							case 0:
								c->plr->stored_tele_x = 2162;
								c->plr->stored_tele_y = 4833;
								c->plr->stored_tele_z = 0;
								break;
							/* South portal.  */
							case 1:
								c->plr->stored_tele_x = 2142;
								c->plr->stored_tele_y = 4813;
								c->plr->stored_tele_z = 0;
								break;
							/* West portal.  */
							case 2:
								c->plr->stored_tele_x = 2122;
								c->plr->stored_tele_y = 4833;
								c->plr->stored_tele_z = 0;
								break;
							/* North portal.  */
							case 3:
								c->plr->stored_tele_x = 2142;
								c->plr->stored_tele_y = 4853;
								c->plr->stored_tele_z = 0;
								break;
						}
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Law ruins.  */
				case 2459:
					if (c->plr->equipment[HAT_SLOT] == 5545)
					{
						c->plr->stored_tele_x = 2464;
						c->plr->stored_tele_y = 4818;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Nature ruins.  */
				case 2460:
					if (c->plr->equipment[HAT_SLOT] == 5541)
					{
						c->plr->stored_tele_x = 2400;
						c->plr->stored_tele_y = 4835;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Chaos ruins.  */
				case 2461:
					if (c->plr->equipment[HAT_SLOT] == 5543)
					{
						c->plr->stored_tele_x = 2273;
						c->plr->stored_tele_y = 4855;
						c->plr->stored_tele_z = 3;
						c->plr->teleport_timer = 0;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					break;
				/* Air altar.  */
				case 2478:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_AIR;
					init_runecrafting(c);
					break;
				/* Mind altar.  */
				case 2479:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_MIND;
					init_runecrafting(c);
					break;
				/* Water altar.  */
				case 2480:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_WATER;
					init_runecrafting(c);
					break;
				/* Earth altar.  */
				case 2481:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_EARTH;
					init_runecrafting(c);
					break;
				/* Fire altar.  */
				case 2482:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_FIRE;
					init_runecrafting(c);
					break;
				/* Body altar.  */
				case 2483:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_BODY;
					init_runecrafting(c);
					break;
				/* Cosmic altar.  */
				case 2484:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_COSMIC;
					init_runecrafting(c);
					break;
				/* Law altar.  */
				case 2485:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_LAW;
					init_runecrafting(c);
					break;
				/* Nature altar.  */
				case 2486:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_NATURE;
					init_runecrafting(c);
					break;
				/* Chaos altar.  */
				case 2487:
					c->plr->action_skill_id = RUNECRAFTING_SKILL;
					c->plr->action_id_type = RUNECRAFTING_RUNE_CHAOS;
					init_runecrafting(c);
					break;

				/* Doors.  */
				case 3:
				case 4:
				case 22:
				case 24:
				case 37:
				case 38:
				case 47:
				case 48:
				case 52:
				case 53:
				case 59:
				case 71:
				case 72:
				case 73:
				case 74:
				case 79:
				case 80:
				case 81:
				case 82:
				case 89:
				case 90:
				case 92:
				case 93:
				case 94:
				case 95:
				case 99:
				case 102:
				case 131:
				case 134:
				case 135:
				case 136:
				case 137:
				case 138:
				case 139:
				case 140:
				case 141:
				case 142:
				case 143:
				case 144:
				case 145:
				case 166:
				case 167:
				case 190:
				case 779:
				case 1506:
				case 1507:
				case 1508:
				case 1511:
				case 1512:
				case 1513:
				case 1514:
				case 1515:
				case 1516:
				case 1517:
				case 1519:
				case 1520:
				case 1526:
				case 1528:
				case 1529:
				case 1530:
				case 1531:
				case 1533:
				case 1534:
				case 1536:
				case 1537:
				case 1539:
				case 1540:
				case 1542:
				case 1543:
				case 1544:
				case 1545:
				case 1551:
				case 1552:
				case 1553:
				case 1556:
				case 1557:
				case 1558:
				case 1560:
				case 1561:
				case 1573:
				case 1574:
				case 1575:
				case 1576:
				case 1589:
				case 1590:
				case 1591:
				case 1594:
				case 1595:
				case 1596:
				case 1597:
				case 1598:
				case 1599:
				case 1600:
				case 1601:
				case 1804:
				case 1805:
				case 1967:
				case 1968:
				case 1980:
				case 1981:
				case 1991:
				case 2000:
				case 2002:
				case 2010:
				case 2011:
				case 2012:
				case 2025:
				case 2032:
				case 2034:
				case 2036:
				case 2039:
				case 2041:
				case 2048:
				case 2049:
				case 2050:
				case 2051:
				case 2054:
				case 2058:
				case 2060:
				case 2069:
				case 2112:
				case 2115:
				case 2116:
				case 2143:
				case 2144:
				case 2154:
				case 2155:
				case 2184:
				case 2199:
				case 2200:
				case 2240:
				case 2241:
				case 2242:
				case 2243:
				case 2246:
				case 2247:
				case 2248:
				case 2249:
				case 2250:
				case 2253:
				case 2254:
				case 2255:
				case 2256:
				case 2259:
				case 2260:
				case 2261:
				case 2262:
				case 2266:
				case 2267:
				case 2307:
				case 2308:
				case 2309:
				case 2337:
				case 2338:
				case 2339:
				case 2340:
				case 2391:
				case 2392:
				case 2394:
				case 2397:
				case 2398:
				case 2399:
				case 2406:
				case 2407:
				case 2411:
				case 2427:
				case 2428:
				case 2429:
				case 2430:
				case 2431:
				case 2432:
				case 2433:
				case 2438:
				case 2439:
				case 2514:
				case 2526:
				case 2528:
				case 2534:
				case 2535:
				case 2537:
				case 2546:
				case 2547:
				case 2548:
				case 2549:
				case 2586:
				case 2595:
				case 2596:
				case 2597:
				case 2598:
				case 2599:
				case 2600:
				case 2601:
				case 2602:
				case 2606:
				case 2607:
				case 2608:
				case 2621:
				case 2622:
				case 2623:
				case 2624:
				case 2625:
				case 2626:
				case 2627:
				case 2628:
				case 2631:
				case 2647:
				case 2673:
				case 2674:
				case 2675:
				case 2676:
				case 2685:
				case 2686:
				case 2687:
				case 2688:
				case 2689:
				case 2690:
				case 2691:
				case 2692:
				case 2705:
				case 2706:
				case 2712:
				case 2786:
				case 2787:
				case 2788:
				case 2789:
				case 2814:
				case 2815:
				case 2828:
				case 2829:
				case 2861:
				case 2862:
				case 2863:
				case 2865:
				case 2866:
				case 2881:
				case 2896:
				case 2897:
				case 2912:
				case 2913:
				case 2922:
				case 2923:
				case 2930:
				case 2997:
				case 2998:
				case 3014:
				case 3015:
				case 3016:
				case 3017:
				case 3018:
				case 3019:
				case 3020:
				case 3021:
				case 3022:
				case 3023:
				case 3024:
				case 3025:
				case 3026:
				case 3027:
				case 3197:
				case 3198:
				case 3220:
				case 3221:
				case 3270:
				case 3271:
				case 3333:
				case 3334:
				case 3444:
				case 3445:
				case 3463:
				case 3489:
				case 3490:
				case 3506:
				case 3507:
				case 3626:
				case 3628:
				case 3629:
				case 3630:
				case 3631:
				case 3632:
				case 3725:
				case 3726:
				case 3727:
				case 3728:
				case 3743:
				case 3745:
				case 3746:
				case 3747:
				case 3761:
				case 3762:
				case 3763:
				case 3764:
				case 3776:
				case 3777:
				case 3782:
				case 3783:
				case 3785:
				case 3786:
				case 3810:
				case 4106:
				case 4107:
				case 4108:
				case 4109:
				case 4132:
				case 4133:
				case 4139:
				case 4140:
				case 4148:
				case 4165:
				case 4166:
				case 4247:
				case 4248:
				case 4250:
				case 4251:
				case 4311:
				case 4312:
				case 4423:
				case 4424:
				case 4425:
				case 4426:
				case 4427:
				case 4428:
				case 4429:
				case 4430:
				case 4487:
				case 4488:
				case 4489:
				case 4490:
				case 4491:
				case 4492:
				case 4545:
				case 4546:
				case 4629:
				case 4630:
				case 4631:
				case 4632:
				case 4633:
				case 4634:
				case 4636:
				case 4637:
				case 4638:
				case 4639:
				case 4640:
				case 4672:
				case 4696:
				case 4697:
				case 4710:
				case 4787:
				case 4788:
				case 4807:
				case 4962:
				case 4963:
				case 4964:
				case 5056:
				case 5057:
				case 5060:
				case 5061:
				case 5126:
				case 5128:
				case 5170:
				case 5172:
				case 5174:
				case 5183:
				case 5184:
				case 5185:
				case 5186:
				case 5187:
				case 5188:
				case 5244:
				case 5245:
				case 5453:
				case 5454:
				case 5501:
				case 5887:
				case 5888:
				case 5889:
				case 5890:
				case 6100:
				case 6101:
				case 6102:
				case 6103:
				case 6104:
				case 6105:
				case 6106:
				case 6107:
				case 6108:
				case 6109:
				case 6110:
				case 6111:
				case 6112:
				case 6113:
				case 6114:
				case 6115:
				case 6238:
				case 6240:
				case 6363:
				case 6451:
				case 6452:
				case 6494:
				case 6545:
				case 6547:
				case 6553:
				case 6555:
				case 6588:
				case 6589:
				case 6615:
				case 6624:
				case 6625:
				case 6626:
				case 6627:
				case 6643:
				case 6714:
				case 6733:
				case 6871:
				case 6872:
				case 6975:
				case 6976:
				case 6977:
				case 7049:
				case 7050:
				case 7051:
				case 7052:
				case 7222:
				case 7231:
				case 7232:
				case 7233:
				case 7234:
				case 7255:
				case 7256:
				case 7259:
				case 7260:
				case 7274:
				case 7302:
				case 7317:
				case 7320:
				case 7323:
				case 7326:
				case 7354:
				case 7373:
				case 7374:
				case 8695:
				case 8696:
				case 8738:
				case 8739:
				case 8740:
				case 8741:
				case 8786:
				case 8787:
				case 8788:
				case 8789:
				case 8791:
				case 8810:
				case 8811:
				case 8812:
				case 8813:
				case 8818:
				case 8819:
				case 8820:
				case 8821:
				case 8958:
				case 8959:
				case 8960:
				case 9038:
				case 9140:
				case 9141:
					move_door(obj_id, obj_type, obj_face, 
							  c->plr->action_index,
							  c->plr->action_region_index, 
							  c->plr->action_x, c->plr->action_y, 
							  c->plr->world_z,
							  1);
					break;
				/* Gates/Doors that open, player walks through. Typically
				   no id incrementation.  */
				/* TODO: Al-Kharid gates, handle toll.  */
				case 2882:
				case 2883:
					if (obj_index != -1)
					{
						handle_auto_door_base(c, obj_id, obj_type, 
							obj_face, c->plr->action_index,
							c->plr->action_region_index, 
							c->plr->action_x, c->plr->action_y, 
							c->plr->world_z);
					}
					else
					{
						printf("DOOR %d at x %d y %d doesn't exist!\n", 
							   obj_id, c->plr->action_x, c->plr->action_y);
					}
					break;
			/* Locked doors.  */
				/* 10 coin.  */
				case 2550:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_10_COIN;
					init_thieving(c);
					break;
				/* Nature rune.  */
				case 2551:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_NATURE_RUNE;
					init_thieving(c);
					break;
				/* Ardougne stove.  */
				case 2556:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_STOVE;
					init_thieving(c);
					break;
				/* HAM Trap Door.  */
				case 5492:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_HAM;
					init_thieving(c);
					break;
				/* Wilderness Axe.  */
				case 2557:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_WILDERNESS_AXE;
					init_thieving(c);
					break;
				/* Ardougne Sewer.  */
				case 2552:
				case 2553:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_SEWER;
					init_thieving(c);
					break;
				/* Wilderness Pirate.  */
				case 2558:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_WILDERNESS_PIRATE;
					init_thieving(c);
					break;
				/* Chaos Druid Tower.  */
				case 2554:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_CHAOS_DRUID_TOWER;
					init_thieving(c);
					break;
				/* Ardougne Palace.  */
				case 2555:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_PALACE;
					init_thieving(c);
					break;
				/* Yanille Dungeon.  */
				case 2559:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_YANILLE_DUNGEON;
					init_thieving(c);
					break;
			/* Thieving from chests without searching for traps.  */
				/* 10 Coin.  */
				case 2566:
					activate_chest_trap(c, THIEVING_CHEST_10_COIN);
					break;
				/* Nature Rune.  */
				case 2567:
					activate_chest_trap(c, THIEVING_CHEST_NATURE_RUNE);
					break;
				/* 50 coin.  */
				case 2568:
					activate_chest_trap(c, THIEVING_CHEST_50_COIN);
					break;
				/* Arrow tip.  */
				case 2573:
					activate_chest_trap(c, THIEVING_CHEST_STEEL_ARROWTIPS);
					break;
				/* Blood rune.  */
				case 2569:
					activate_chest_trap(c, THIEVING_CHEST_BLOOD_RUNE);
					break;
				/* King Lathas.  */
				case 2570:
					activate_chest_trap(c, THIEVING_CHEST_KING_LATHAS);
					break;
				/* Empty chest, used when it has been looted.  */
				case 2571:
					packet_send_chatbox_message(c, "It looks like this chest has already been"
								 " looted.");
					break;
			/* Agility.  */
				/* Gnome course ladder objects.  */
				/* net.  */
				case 2285:
				/* tree branch up.  */
				case 2313:
				/* tree branch down.  */
				case 2314:
				case 2315:
				/* Gnome course log.  */
				case 2295:
				/* Gnome course tightrope.  */
				case 2312:
				/* standalone net.  */
				case 2286:
				/* pipes.  */
				case 154:
				case 4058:
				/* Barbarian agility stuff.  */
				/* Ropeswing.  */
				case 2282:
				/* Log cross.  */
				case 2294:
				/* Intro pipe, before course.  */
				case 2287:
				/* Climbing net.  */
				case 2284:
				/* Balancing ledge.  */
				case 2302:
				/* Rubble.  */
				case 1948:
				/* Wilderness agility stuff.  */
				/* Pipe.  */
				case 2288:
				/* Ropeswing.  */
				case 2283:
				/* Stepping stones.  */
				case 2311:
				/* Log.  */
				case 2297:
				/* Climbing stones.  */
				case 2328:
				/* Agility shortcut stuff.  */
				/* Fence stiles.  */
				case 993:
				/* Swamp bridge.  */
				case 3522:
				/* Falador crumbling wall.  */
				case 1947:
					c->plr->action_skill_id = AGILITY_SKILL;
					init_agility(c);
					break;
				   
			/* ALL LADDERS/STAIRS.  */
			/* Ladders.  */
				/* Climb up.  */
				case 11:
				case 60:
				case 101:
				case 132:
				case 195:
				case 1747:
				case 1750:
				case 1752:
				case 1755:
				case 1757:
				case 1766:
				case 1768:
				case 2148:
				case 2188:
				case 2189:
				case 2268:
				case 2352:
				case 2353:
				case 2405:
				case 2410:
				case 2517:
				case 2592:
				case 2641:
				case 2796:
				case 2833:
				case 2872:
				case 3028:
				case 3030:
				case 3618:
				case 3829:
				case 3832:
				case 4159:
				case 4160:
				case 4161:
				case 4163:
				case 4164:
				case 4188:
				case 4643:
				case 4645:
				case 4647:
				case 4966:
				case 4968:
				case 4970:
				case 5054:
				case 5130:
				case 5251:
				case 5264:
				case 5488:
				case 5493:
				case 5812:
				case 6261:
				case 6280:
				case 6281:
				case 6418:
				case 6419:
				case 6436:
				case 6439:
				case 6450:
				case 6501:
				case 6502:
				case 6503:
				case 6504:
				case 6645:
				case 6708:
				case 7433:
				case 8744:
				case 8785:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_UP);
					break;

				/* Ladder down.  */
				case 10:
				case 133:
				case 287:
				case 882:
				case 1570:
				case 1571:
				case 1746:
				case 1749:
				case 1753:
				case 1754:
				case 1756:
				case 1758:
				case 1759:
				case 1765:
				case 1767:
				case 2113:
				case 2147:
				case 2187:
				case 2190:
				case 2269:
				case 2408:
				case 2590:
				case 2591:
				case 2605:
				case 2611:
				case 2797:
				case 2812:
				case 2871:
				case 3029:
				case 3031:
				case 3205:
				case 3617:
				case 4158:
				case 4189:
				case 4644:
				case 4646:
				case 4648:
				case 4911:
				case 4912:
				case 4965:
				case 4967:
				case 4969:
				case 5250:
				case 5813:
				case 6260:
				case 6497:
				case 6498:
				case 6499:
				case 6500:
				case 6561:
				case 7221:
				case 8746:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_DOWN);
					break;
				/* Climb.  */
				case 1748:
				case 1751:
				case 1762:
				case 1764:
				case 2610:
				case 2825:
				case 2884:
				case 2958:
				case 3610:
				case 4383:
				case 4412:
				case 4413:
				case 4444:
				case 4485:
				case 4728:
				case 4881:
				case 4889:
				case 5946:
				case 6262:
				case 8745:
				case 8956:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_LAST);
					break;
			/* Stairs.  */
				/* Up.  */
				case 54:
				case 56:
				case 96:
				case 1722:
				case 1725:
				case 1729:
				case 1734:
				case 1737:
				case 1738:
				case 1742:
				case 1761:
				case 2316:
				case 2711:
				case 2853:
				case 3416:
				case 3788:
				case 4493:
				case 4495:
				case 4497:
				case 4568:
				case 4622:
				case 4623:
				case 4626:
				case 4627:
				case 4756:
				case 5206:
				case 5262:
				case 5280:
				case 6085:
				case 6087:
				case 6089:
				case 6242:
				case 6244:
				case 6312:
				case 6372:
				case 6648:
				case 6702:
				case 6703:
				case 6704:
				case 6705:
				case 6706:
				case 6707:
				case 6842:
				case 7057:
				case 9138:
					handle_stairs(c, obj_id, obj_face, STAIR_UP);
					break;
				/* Down.  */
				case 55:
				case 57:
				case 98:
				case 1723:
				case 1724:
				case 1726:
				case 1727:
				case 1728:
				case 1732:
				case 1733:
				case 1736:
				case 1740:
				case 1741:
				case 1744:
				case 1745:
				case 3415:
				case 3789:
				case 4494:
				case 4496:
				case 4498:
				case 4570:
				case 4620:
				case 4621:
				case 4624:
				case 4625:
				case 4755:
				case 5207:
				case 5263:
				case 5281:
				case 6086:
				case 6088:
				case 6090:
				case 6243:
				case 6245:
				case 6313:
				case 6373:
				case 6649:
				case 6841:
				case 7056:
				case 9084:
					handle_stairs(c, obj_id, obj_face, STAIR_DOWN);
					break;

				/* Climb action, not up or down.  */
				case 1739:
				case 1743:
				case 3223:
				case 4415:
				case 4416:
				case 4417:
				case 4418:
				case 4419:
				case 4420:
				case 4569:
				case 8966:
					handle_stairs(c, obj_id, obj_face, STAIR_LAST);
					break;
			/* Take-axe Stumps.  */
			/* TODO: verify this is accurate.  */
				case 5581:
					rv = add_to_object_list(5581, 10, 
							c->plr->action_x , 
							c->plr->action_y, 
							c->plr->action_z, 0, 
							100, 0);
					packet_send_chatbox_message(c, "You take an axe from the stump.");
					set_object_transforms(rv, 5582, 100);
					spawn_object_globally(rv);
					add_item_to_inv(c, 1351, 1, -1);
					break;
		/* TODO: FIXME: Does not work currently.  */
			/* Trapdoors.  */
				/* Opening up a manhole.  */
				case 881:
					printf("open manhole test.\n");
					/* Spawn the manhold cover.  */
					rv = add_to_object_list(883, 10, 
							c->plr->action_x + 1, 
							c->plr->action_y, 
							c->plr->action_z, 0, 
							100, 1);
					/* Spawn the manhole, use the covered version and then
					 * transform it to the open one.  */
					rv2 = add_to_object_list(881, 10, 
							c->plr->action_x, 
							c->plr->action_y, 
							c->plr->action_z, 0, 
							101, 0);

					set_object_transforms(rv2, 882, 100);
					/* Set the manhole cover's double door to the index of the
					   manhole. This will be used for closing it manually.  */
					OBJECT_SPAWNS[rv]->double_door = obj_index;

					/* Spawn the stuff.  */
					spawn_object_globally(rv);
					spawn_object_globally(rv2);
					break;
				/* Manhole cover, close opened manhole.  */
				case 883:
					OBJECT_SPAWNS[obj_index]->despawn_ticks = 0;
					OBJECT_SPAWNS[OBJECT_SPAWNS[obj_index]->double_door]->transform_ticks = 0;
					break;
				/* Trapdoor.  */
				case 1568:
					printf("trap door\n");
					rv = add_to_object_list(1568, obj_type, 
							c->plr->action_x, 
							c->plr->action_y, 
							c->plr->action_z, obj_face, 
							101, 0);
					set_object_transforms(rv, 1570, 100);
					spawn_object_globally(rv);
					break;
			/* Pickable Plants.  */
				/* Nettles.  */
				case 1181:
				case 5253:
				case 5254:
				case 5255:
				case 5256:
				case 5257:
				case 5258:
					pick_plant(c);
					break;
				/* Pottery oven.  */
				case 2643:
					c->plr->action_menu_id = 123;
					c->plr->action_menu_skill_id = CRAFTING_SKILL;
					c->plr->chat_type = SKILL_MENU_CHAT;
					c->plr->next_chat_id = 0;
					handle_dialogue(c);
					break;

			/* Portals.  */
				/* Portals from runecrafting altars.  */
				case 2465:
				case 2466:
				case 2467:
				case 2468:
				case 2469:
				case 2470:
				case 2471:
				case 2472:
				case 2473:
				case 2474:
				case 2475:
				case 2476:
				case 2477:
					handle_portals(c);
					break;

			/* Abyss rifts.  */
				/* Fire rift.  */
				case 7129:
					c->plr->tele_x = 2576;
					c->plr->tele_y = 4848;
					c->plr->tele_z = 0;
					break;
				/* Earth rift.  */
				case 7130:
					c->plr->tele_x = 2657;
					c->plr->tele_y = 4830;
					c->plr->tele_z = 0;
					break;
				/* Body rift.  */
				case 7131:
					c->plr->tele_x = 2520;
					c->plr->tele_y = 4839;
					c->plr->tele_z = 0;
					break;
				/* Cosmic rift.  */
				case 7132:
					/* There are multiple portals for this one, choose
					 * randomly among them.  */
					switch (rand_int(0, 3))
					{
						/* East portal.  */
						case 0:
							c->plr->tele_x = 2162;
							c->plr->tele_y = 4833;
							c->plr->tele_z = 0;
							break;
						/* South portal.  */
						case 1:
							c->plr->tele_x = 2142;
							c->plr->tele_y = 4813;
							c->plr->tele_z = 0;
							break;
						/* West portal.  */
						case 2:
							c->plr->tele_x = 2122;
							c->plr->tele_y = 4833;
							c->plr->tele_z = 0;
							break;
						/* North portal.  */
						case 3:
							c->plr->tele_x = 2142;
							c->plr->tele_y = 4853;
							c->plr->tele_z = 0;
							break;
					}
					break;
				/* Nature rift.  */
				case 7133:
					c->plr->tele_x = 2400;
					c->plr->tele_y = 4835;
					c->plr->tele_z = 0;
					break;
				/* Chaos rift.  */
				case 7134:
					c->plr->tele_x = 2273;
					c->plr->tele_y = 4855;
					c->plr->tele_z = 3;
					break;
				/* Law rift.  */
				case 7135:
					c->plr->tele_x = 2464;
					c->plr->tele_y = 4818;
					c->plr->tele_z = 0;
					break;
				/* Water rift.  */
				case 7137:
					c->plr->tele_x = 2725;
					c->plr->tele_y = 4832;
					c->plr->tele_z = 0;
					break;
				/* Air rift.  */
				case 7139:
					c->plr->tele_x = 2841;
					c->plr->tele_y = 4830;
					c->plr->tele_z = 0;
					break;
				/* Mind rift.  */
				case 7140:
					c->plr->tele_x = 2793;
					c->plr->tele_y = 4829;
					c->plr->tele_z = 0;
					break;
				
				/* Prayer altar.  */
				case 409:
				case 410:
				case 411:
				case 412:
				/* FIXME: Does this one count?  */
				case 2640:
				case 4008:
					player_play_animation(c, 645);
					packet_send_chatbox_message(c, "You recharge your Prayer Points.");
					c->plr->level[PRAYER_SKILL]
						= c->plr->base_level[PRAYER_SKILL];
					packet_send_updated_skill_stats(c, PRAYER_SKILL);
					c->plr->frozen_ticks = 4;
					c->plr->frozen_type = EFFECT_FREEZE_NORMAL;
					break;

				/* Dairy cow.  */
				case 8689:
					milk_dairy_cow(c);
					break;

				/* Flour bin.  */
				case 1782:
					handle_flour_bins(c);
					break;

				/* Grain hopper controls.  */
				case 2718:
				case 2719:
				case 2720:
				case 2721:
				case 2722:
					hopper_controls(c);
					break;

				/* Bank deposit box.  */
				case 9398:
					send_bank_deposit_box_menu(c);
					break;

				default:
					printf("OBJECT ID: %d NOT HANDLED YET\n", obj_id);
					set_player_state(c, STATE_IDLE);
					break;
			}
		}
		else if (c->plr->action_click_num == 2)
		{
			switch (obj_id)
			{
			/* ALL LADDERS/STAIRS.  */
				case 1739:
				case 1743:
				case 4569:
					handle_stairs(c, obj_id, obj_face, STAIR_UP);
					break;
				case 1748:
				case 1751:
				case 1753:
				case 2884:
				case 6262:
				case 8746:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_UP);
					break;
				case 4187:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_DOWN);
					break;
			/* PICKABLE PLANTS.  */
				case 312:
				case 313:
				case 1161:
				case 2646:
				case 5583:
				case 5584:
				case 5585:
					pick_plant(c);
					break;
			/* Furnace.  */
				case 2781:
					c->plr->action_menu_id = SMITHING_ACTION_SMELT;
					c->plr->action_menu_skill_id = SMITHING_SKILL;
					c->plr->chat_type = SKILL_MENU_CHAT;
					c->plr->next_chat_id = 0;
					handle_dialogue(c);
					break;
			/* Spinning wheel.  */
				case 2644:
				case 8748:
					c->plr->action_menu_id = CRAFTING_ACTION_SPINNING;
					c->plr->action_menu_skill_id = CRAFTING_SKILL;
					c->plr->chat_type = SKILL_MENU_CHAT;
					c->plr->next_chat_id = 0;
					handle_dialogue(c);
					break;
			/* Weaving loom.  */
				case 8717:
					c->plr->action_menu_id = CRAFTING_ACTION_WEAVING;
					c->plr->action_menu_skill_id = CRAFTING_SKILL;
					c->plr->chat_type = SKILL_MENU_CHAT;
					c->plr->next_chat_id = 0;
					handle_dialogue(c);
					break;
			/* ORE - PROSPECTING.  */
				case 2108:
				case 2109:
					packet_send_chatbox_message(c, "This rock contains clay.");
					break;
				case 2090:
				case 2091:
					packet_send_chatbox_message(c, "This rock contains copper.");
					break;
				case 2094:
				case 2095:
					packet_send_chatbox_message(c, "This rock contains tin.");
					break;
				case 2092:
				case 2093:
					packet_send_chatbox_message(c, "This rock contains iron.");
					break;
				case 2100:
				case 2101:
					packet_send_chatbox_message(c, "This rock contains silver.");
					break;
				case 2096:
				case 2097:
					packet_send_chatbox_message(c, "This rock contains coal.");
					break;
				case 2098:
				case 2099:
					packet_send_chatbox_message(c, "This rock contains gold.");
					break;
				case 2102:
				case 2103:
					packet_send_chatbox_message(c, "This rock contains mithril.");
					break;
				case 2104:
				case 2105:
					packet_send_chatbox_message(c, "This rock contains adamant.");
					break;
				case 2106:
				case 2107:
					packet_send_chatbox_message(c, "This rock contains rune.");
					break;
				case 2491:
					packet_send_chatbox_message(c, "The rock contains rune essence.");
					break;
				/* Use bank booth quickly.  */
				case 2213:
					send_bank_menu(c);
					break;
				/* Close a trapdoor.  */
				case 1570:
					if (OBJECT_SPAWNS[obj_index]->transform_ticks > 0)
					{
						OBJECT_SPAWNS[obj_index]->transform_ticks = 0;
					}
					break;
			/* Thieving from stalls.  */
				/* Veg stalls.  */
				case 4706:
				case 4708:
					c->plr->action_id_type = THIEVING_STALL_VEGETABLE;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Bakery stalls.  */
				case 630:
				case 6569:
					c->plr->action_id_type = THIEVING_STALL_BAKERY;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Dwarven bakery stall.  */
				case 6163:
					c->plr->action_id_type = THIEVING_STALL_BAKERY_DWARF;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Crafting stalls.  */
				case 4874:
				case 6166:
					c->plr->action_id_type = THIEVING_STALL_CRAFTING;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Monkey food stalls.  */
				case 4875:
					c->plr->action_id_type = THIEVING_STALL_MONKEY_FOOD;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Monkey general stalls.  */
				case 4876:
					c->plr->action_id_type = THIEVING_STALL_MONKEY_GENERAL;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Tea stalls.  */
				case 635:
				case 6574:
					c->plr->action_id_type = THIEVING_STALL_TEA;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Silk stalls.  */
				case 629:
				case 2560:
				case 6568:
					c->plr->action_id_type = THIEVING_STALL_SILK;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Seed stalls.  */
				case 7053:
					c->plr->action_id_type = THIEVING_STALL_SEED;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Fur stalls.  */
				case 632:
				case 2563:
				case 4278:
				case 6571:
					c->plr->action_id_type = THIEVING_STALL_FUR;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Fish stalls.  */
				case 4277:
				case 4705:
				case 4707:
					c->plr->action_id_type = THIEVING_STALL_FISH;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Silver stalls.  */
				case 628:
				case 2565:
					c->plr->action_id_type = THIEVING_STALL_SILVER;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Dwarven silver stall.  */
				case 6164:
					c->plr->action_id_type = THIEVING_STALL_SILVER_DWARF;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Spice stalls.  */
				case 633:
				case 2564:
				case 6572:
					c->plr->action_id_type = THIEVING_STALL_SPICE;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Magic stalls.  */
				case 4877:
					c->plr->action_id_type = THIEVING_STALL_MAGIC;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Scimitar stalls.  */
				case 4878:
					c->plr->action_id_type = THIEVING_STALL_SCIMITAR;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Gem stalls.  */
				case 631:
				case 2562:
				case 6162:
					c->plr->action_id_type = THIEVING_STALL_GEM;
					c->plr->action_id_type2 = obj_id;
					c->plr->action_skill_type = THIEVING_ACTION_STALL;
					c->plr->action_store_one = obj_id;
					c->plr->action_store_two = obj_type;
					c->plr->action_store_three = obj_face;
					init_thieving(c);
					break;
				/* Dwarven clothing stall.  */
				case 6165:
					packet_send_chatbox_message(c, "You don't really see anything you'd want "
								 "to steal from this stall.");
					break;
			/* Locked doors.  */
				/* 10 coin.  */
				case 2550:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_10_COIN;
					init_thieving(c);
					break;
				/* Nature rune.  */
				case 2551:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_NATURE_RUNE;
					init_thieving(c);
					break;
				/* Ardougne stove.  */
				case 2556:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_STOVE;
					init_thieving(c);
					break;
				/* HAM Trap Door.  */
				case 5492:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_HAM;
					init_thieving(c);
					break;
				/* Wilderness Axe.  */
				case 2557:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_WILDERNESS_AXE;
					init_thieving(c);
					break;
				/* Ardougne Sewer.  */
				case 2552:
				case 2553:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_SEWER;
					init_thieving(c);
					break;
				/* Wilderness Pirate.  */
				case 2558:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_WILDERNESS_PIRATE;
					init_thieving(c);
					break;
				/* Chaos Druid Tower.  */
				case 2554:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_CHAOS_DRUID_TOWER;
					init_thieving(c);
					break;
				/* Ardougne Palace.  */
				case 2555:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_ARDOUGNE_PALACE;
					init_thieving(c);
					break;
				/* Yanille Dungeon.  */
				case 2559:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type = THIEVING_ACTION_LOCKPICK_DOOR;
					c->plr->action_id_type = THIEVING_DOOR_YANILLE_DUNGEON;
					init_thieving(c);
					break;
			/* Thieving from chests and searching for traps.  */
				/* 10 Coin.  */
				case 2566:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_10_COIN;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* Nature Rune.  */
				case 2567:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_NATURE_RUNE;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* 50 coin.  */
				case 2568:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_50_COIN;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* Arrow tip.  */
				case 2573:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_STEEL_ARROWTIPS;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* Blood rune.  */
				case 2569:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_BLOOD_RUNE;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* King Lathas.  */
				case 2570:
					c->plr->action_skill_id = THIEVING_SKILL;
					c->plr->action_skill_type 
						= THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST;
					c->plr->action_id_type = THIEVING_CHEST_KING_LATHAS;
					c->plr->action_store_one = obj_face;
					init_thieving(c);
					break;
				/* Empty chest, used when it has been looted.  */
				case 2571:
					packet_send_chatbox_message(c, "It looks like this chest has already been"
								 " looted.");
					break;

				default:
					printf("OBJECT ID: %d 2ND CLICK NOT HANDLED!\n", obj_id);
					set_player_state(c, STATE_IDLE);
					break;
			}
		}
		else if (c->plr->action_click_num == 3)
		{
			switch (obj_id)
			{
			/* ALL LADDERS/STAIRS.  */
				case 1739:
				case 1743:
				case 4569:
					handle_stairs(c, obj_id, obj_face, STAIR_DOWN);
					break;
				case 4187:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_UP);
					break;
				case 1748:
				case 1749:
				case 1751:
				case 2884:
				case 6262:
				case 6523:
				case 8745:
					handle_ladders(c, c->plr->action_x, 
								  c->plr->action_y, c->plr->action_z,
								  STAIR_DOWN);
					break;
				default:
					printf("OBJECT ID: %d 3RD CLICK NOT HANDLED!\n", obj_id);
					set_player_state(c, STATE_IDLE);
					break;
			}
		}
		/* If an item is used on an object.  */
		else if (c->plr->action_click_num == 4)
		{
			printf ("Object on item handling here!\n");
			
			if (player_has_item_slot(c, c->plr->action_item_id, 1, 
									 c->plr->action_item_slot) == -1)
			{
				printf("Player %s tried to use a nonexistant item on a"
					   "object!\n", c->plr->name);
				set_player_state(c, STATE_IDLE);
				return;
			}

			switch (obj_id)
			{
				/* Sources of water that work for a jug.  */
				/* No wells! Only buckets can be filled from them. 
				   They are handled right below this.  */
				/* FIXME: Allow this to be repeated without extra clicking,
				 * fill every vial possible.  */
				case 873:
				case 874:
				case 879:
				case 880:
				case 2864:
				case 4176:
				case 4285:
				case 4482:
				case 6151:
				case 6232:
				case 9143:
					switch (c->plr->action_item_id)
					{
						/* Vial.  */
						case 229:
						/* Waterskins.  */
						case 1825:
						case 1827:
						case 1829:
						case 1831:
						/* Bowl.  */
						case 1923:
						/* Bucket.  */
						case 1925:
						/* Jug.  */
						case 1935:
						/* Watering cans.  */
						case 5331:
						case 5333:
						case 5334:
						case 5335:
						case 5336:
						case 5337:
						case 5338:
						case 5339:
							set_timed_action(c, TA_FILL_WITH_WATER, 0);
							break;
					}
					break;
			/* Runecrafting.  */
				/* Air ruins.  */
				case 2452:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1438
						|| c->plr->action_item_id == 5516)
					{
						c->plr->stored_tele_x = 2841;
						c->plr->stored_tele_y = 4830;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Mind ruins.  */
				case 2453:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1448)
					{
						c->plr->stored_tele_x = 2793;
						c->plr->stored_tele_y = 4829;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Water ruins.  */
				case 2454:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1445
						|| c->plr->action_item_id == 5516)
					{
						c->plr->stored_tele_x = 2725;
						c->plr->stored_tele_y = 4832;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Earth ruins.  */
				case 2455:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1440
						|| c->plr->action_item_id == 5516)
					{
						c->plr->stored_tele_x = 2657;
						c->plr->stored_tele_y = 4830;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Fire ruins.  */
				case 2456:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1442 
						|| c->plr->action_item_id == 5516)
					{
						c->plr->stored_tele_x = 2576;
						c->plr->stored_tele_y = 4848;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Body ruins.  */
				case 2457:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1446)
					{
						c->plr->stored_tele_x = 2520;
						c->plr->stored_tele_y = 4839;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Cosmic ruins.  */
				case 2458:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1454)
					{
						/* There are multiple portals for this one, choose
						 * randomly among them.  */
						switch (rand_int(0, 3))
						{
							/* East portal.  */
							case 0:
								c->plr->stored_tele_x = 2162;
								c->plr->stored_tele_y = 4833;
								c->plr->stored_tele_z = 0;
								break;
							/* South portal.  */
							case 1:
								c->plr->stored_tele_x = 2142;
								c->plr->stored_tele_y = 4813;
								c->plr->stored_tele_z = 0;
								break;
							/* West portal.  */
							case 2:
								c->plr->stored_tele_x = 2122;
								c->plr->stored_tele_y = 4833;
								c->plr->stored_tele_z = 0;
								break;
							/* North portal.  */
							case 3:
								c->plr->stored_tele_x = 2142;
								c->plr->stored_tele_y = 4853;
								c->plr->stored_tele_z = 0;
								break;
						}
						c->plr->teleport_type = -1;
						c->plr->teleport_timer = 2;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Law ruins.  */
				case 2459:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1458)
					{
						c->plr->stored_tele_x = 2464;
						c->plr->stored_tele_y = 4818;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Nature ruins.  */
				case 2460:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1462)
					{
						c->plr->stored_tele_x = 2400;
						c->plr->stored_tele_y = 4835;
						c->plr->stored_tele_z = 0;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
				/* Chaos ruins.  */
				case 2461:
					snprintf(return_msg, 100, "You hold the %s towards the"
							 " mysterious ruins.", 
							 ITEMS[c->plr->action_item_id]->name);
					packet_send_chatbox_message(c, return_msg);
					player_play_animation(c, 827);

					if (c->plr->action_item_id == 1452)
					{
						c->plr->stored_tele_x = 2273;
						c->plr->stored_tele_y = 4855;
						c->plr->stored_tele_z = 3;
						c->plr->teleport_timer = 2;
						c->plr->teleport_type = -1;
						packet_send_chatbox_message(c, "You feel a powerful force take "
									 "hold of you...");
					}
					else
					{
						packet_send_chatbox_message(c, "Nothing interesting happens.");
					}
					break;
			/* Runecrafting altars.  */
				/* Tiara infusing.  */
				case 2478:
					switch (c->plr->action_item_id)
					{
						case 5525:
						case 1438: 
							infuse_tiara(c, RUNECRAFTING_TIARA_AIR, 
										 c->plr->action_item_id);
							break;

						default:
							create_combination_rune(c, RUNECRAFTING_RUNE_AIR, 
													c->plr->action_item_id, 
													c->plr->action_item_slot);
							break;
					}
					break;
				case 2479:
					infuse_tiara(c, RUNECRAFTING_TIARA_MIND, 
								 c->plr->action_item_id);
					break;
				case 2480:
					switch (c->plr->action_item_id)
					{
						case 5525:
						case 1444: 
							infuse_tiara(c, RUNECRAFTING_TIARA_WATER, 
										 c->plr->action_item_id);
							break;
						default:
							create_combination_rune(c, RUNECRAFTING_RUNE_WATER, 
													c->plr->action_item_id, 
													c->plr->action_item_slot);
							break;
					}
					break;
				case 2481:
					switch (c->plr->action_item_id)
					{
						case 5525:
						case 1440: 
							infuse_tiara(c, RUNECRAFTING_TIARA_EARTH, 
										 c->plr->action_item_id);
							break;
						default:
							create_combination_rune(c, RUNECRAFTING_RUNE_EARTH, 
													c->plr->action_item_id, 
													c->plr->action_item_slot);
							break;
					}
					break;
				case 2482:
					switch (c->plr->action_item_id)
					{
						case 5525:
						case 1440: 
							infuse_tiara(c, RUNECRAFTING_TIARA_FIRE, 
										 c->plr->action_item_id);
							break;
						default:
							create_combination_rune(c, RUNECRAFTING_RUNE_FIRE, 
													c->plr->action_item_id, 
													c->plr->action_item_slot);
							break;
					}
					break;
				case 2483:
					infuse_tiara(c, RUNECRAFTING_TIARA_BODY, 
								 c->plr->action_item_id);
					break;
				case 2484:
					infuse_tiara(c, RUNECRAFTING_TIARA_COSMIC, 
								 c->plr->action_item_id);
					break;
				case 2485:
					infuse_tiara(c, RUNECRAFTING_TIARA_LAW, 
								 c->plr->action_item_id);
					break;
				case 2486:
					infuse_tiara(c, RUNECRAFTING_TIARA_NATURE, 
								 c->plr->action_item_id);
					break;
				case 2487:
					infuse_tiara(c, RUNECRAFTING_TIARA_CHAOS, 
								 c->plr->action_item_id);
					break;

			#if 0
			/* Orb obelisks.  */
				/* Earth.  */
				case 2150:
					break;
				/* Water.  */
				case 2151:
					break;
				/* Air.  */
				case 2152:
					break;
				/* Fire.  */
				case 2153:
					break;
			#endif
			/* Abyss.  */
			/* Cooking fires.  */
				case 114:
				case 2728:
				case 2732:
					/* TODO: Replace this with a skills_data function.  */
					switch (c->plr->action_item_id)
					{
						/* Raw meat.  */
						case 2132:
						case 2134:
						case 2136:
							c->plr->action_id_type = COOKING_MEAT;
							break;
						/* Raw chicken.  */
						case 2138:
							c->plr->action_id_type = COOKING_CHICKEN;
							break;
						/* Raw rabbit.  */
						case 3226:
							c->plr->action_id_type = COOKING_RABBIT;
							break;
						/* Bread dough.  */
						case 2307:
							c->plr->action_id_type = COOKING_BREAD;
							break;
						/* Pitta bread dough.  */
						case 1863:
							c->plr->action_id_type = COOKING_PITTA_BREAD;
							break;
						/* Raw shrimps.  */
						case 317:
							c->plr->action_id_type = COOKING_SHRIMP;
							break;
						/* Raw karambwanji.  */
						case 3150:
							c->plr->action_id_type = COOKING_KARAMBWANJI;
							break;
						/* Raw sardine.  */
						case 327:
							c->plr->action_id_type = COOKING_SARDINE;
							break;
						/* Raw anchovies.  */
						case 321:
							c->plr->action_id_type = COOKING_ANCHOVIES;
							break;
						/* Poison karambwan.  */
						/* TODO: These have a special method of cooking.  */
						case -1:
							c->plr->action_id_type = COOKING_POISON_KARAMBWAN;
							break;
						/* Raw herring.  */
						case 345:
							c->plr->action_id_type = COOKING_HERRING;
							break;
						/* Raw mackerel.  */
						case 353:
							c->plr->action_id_type = COOKING_MACKEREL;
							break;
						/* Raw trout.  */
						case 335:
							c->plr->action_id_type = COOKING_TROUT;
							break;
						/* Raw cod.  */
						case 341:
							c->plr->action_id_type = COOKING_COD;
							break;
						/* Raw pike.  */
						case 349:
							c->plr->action_id_type = COOKING_PIKE;
							break;
						/* Raw salmon.  */
						case 331:
							c->plr->action_id_type = COOKING_SALMON;
							break;
						/* Raw slimey eel.  */
						case 3379:
							c->plr->action_id_type = COOKING_SLIMEY_EEL;
							break;
						/* Raw tuna.  */
						case 359:
							c->plr->action_id_type = COOKING_TUNA;
							break;
						/* Cooked karambwan.  */
						/* TODO: Uses a special cooking method.  */
						case -2:
							c->plr->action_id_type = COOKING_KARAMBWAN;
							break;
						/* Raw cave eel.  */
						case 5001:
							c->plr->action_id_type = COOKING_CAVE_EEL;
							break;
						/* Raw lobster.  */
						case 377:
							c->plr->action_id_type = COOKING_LOBSTER;
							break;
						/* Raw bass.  */
						case 363:
							c->plr->action_id_type = COOKING_BASS;
							break;
						/* Raw swordfish.  */
						case 371:
							c->plr->action_id_type = COOKING_SWORDFISH;
							break;
						/* Raw lava eel.  */
						case 2148:
							c->plr->action_id_type = COOKING_LAVA_EEL;
							break;
						/* Raw shark.  */
						case 383:
							c->plr->action_id_type = COOKING_SHARK;
							break;
						/* Raw sea turtle.  */
						case 395:
							c->plr->action_id_type = COOKING_SEA_TURTLE;
							break;
						/* Raw manta ray.  */
						case 389:
							c->plr->action_id_type = COOKING_MANTA_RAY;
							break;
						/* Redberry pie.  */
						case 2321:
							c->plr->action_id_type = COOKING_REDBERRY_PIE;
							break;
						/* Meat pie.  */
						case 2319:
							c->plr->action_id_type = COOKING_MEAT_PIE;
							break;
						/* Apple pie.  */
						case 2317:
							c->plr->action_id_type = COOKING_APPLE_PIE;
							break;
						/* Plain pizza.  */
						case 2287:
							c->plr->action_id_type = COOKING_PLAIN_PIZZA;
							break;
						/* Cake.  */
						case 1889:
							c->plr->action_id_type = COOKING_CAKE;
							break;
						/* Sweetcorn.  */
						case 5986:
							c->plr->action_id_type = COOKING_SWEETCORN;
							break;
					}
					c->plr->action_menu_id = COOKING_ACTION_COOK;
					c->plr->action_menu_skill_id = COOKING_SKILL;
					c->plr->chat_type = SKILL_MENU_CHAT;
					c->plr->next_chat_id = 0;

					printf("cooking\n");
					
					/* Check if we need to return a dish or pan when cooking.
					   If so, set spaces needed accordingly.  */
					if (ITEMS[c->plr->action_item_id]->item_consume_id != -1)
					{
						c->plr->action_item_space_needed = 1;
					}

					if (player_has_num_items_non_stack(c, c->plr->action_item_id) > 1)
					{
						printf("more than one!\n");
						handle_dialogue(c);
					}
					else
					{
						printf("single!\n");
						init_cooking(c);
					}
					break;
		/* MINING HANDLING.  */
				/* Rune Essence.  */
				case 2491:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_RUNE_ESSENCE;
							init_mining(c);
							break;
					}
					break;
				/* Clay.  */
				case 2108:
				case 2109:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_CLAY;
							init_mining(c);
							break;
					}
					break;
				/* Copper.  */
				case 2090:
				case 2091:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_COPPER;
							init_mining(c);
							break;
					}
					break;
				/* Tin.  */
				case 2094:
				case 2095:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_TIN;
							init_mining(c);
							break;
					}
					break;
				/* Iron.  */
				case 2092:
				case 2093:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_IRON;
							init_mining(c);
							break;
					}
					break;
				/* Silver.  */
				case 2100:
				case 2101:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_SILVER;
							init_mining(c);
							break;
					}
					break;
				/* Coal.  */
				case 2096:
				case 2097:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_COAL;
							init_mining(c);
							break;
					}
					break;
				/* Gold.  */
				case 2098:
				case 2099:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_GOLD;
							init_mining(c);
							break;
					}
					break;
				/* Mithril.  */
				case 2102:
				case 2103:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_MITHRIL;
							init_mining(c);
							break;
					}
					break;
				/* Adamant.  */
				case 2104:
				case 2105:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_ADAMANT;
							init_mining(c);
							break;
					}
					break;
				/* Rune.  */
				case 2106:
				case 2107:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_RUNE;
							init_mining(c);
							break;
					}
					break;
				/* Blurite.  */
				case 2110:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_BLURITE;
							init_mining(c);
							break;
					}
					break;
				/* Gem rock.  */
				case 2111:
					/* All pickaxes.  */
					switch (c->plr->action_item_id)
					{
						case 1265:
						case 1267:
						case 1269:
						case 1273:
						case 1271:
						case 1275:
							c->plr->action_id_type = MINING_GEM;
							init_mining(c);
							break;
					}
					break;

		/* WOODCUTTING HANDLING.  */
			/* ALL TREES GO HERE.  */
				/* Regular trees.  */
				case 1276:
				case 1277:
				case 1278:
				case 1279:
				case 1280:
				case 3033:
				case 3034:
				/* Dead trees.  */
				case 1282:
				case 1283:
				case 1284:
				case 1285:
				case 1286:
				case 1289:
				case 1291:
				case 1365:
				case 1383:
				case 1384:
				case 3035:
				case 3036:
				case 5902:
				case 5903:
				case 5904:
				/* Evergreen.  */
				case 1315:
				case 1316:
				case 1318:
				case 1319:
				case 1330:
				case 1331:
				case 1332:
				case 3879:
				case 3881:
				case 3882:
				case 3883:
				/* Jungle trees, might need special handling.  */
				case 2887:
				case 2889:
				case 2890:
				case 4818:
				case 4820:
				/* Jungle bushes?  */
				case 2892:
				case 2893:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_REGULAR;
							init_woodcutting(c);
							break;
					}
					break;
				/* Achey trees.  */
				case 2023:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_ACHEY;
							init_woodcutting(c);
							break;
					}
					break;
				/* Oak trees.  */
				case 1281:
				case 3037:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_OAK;
							init_woodcutting(c);
							break;
					}
					break;
				/* Willow trees.  */
				case 1308:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_WILLOW;
							init_woodcutting(c);
							break;
					}
					break;
				/* Teak trees.  */
				case 9036:
				case 9039:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_TEAK;
							init_woodcutting(c);
							break;
					}
					break;
				/* Maple trees.  */
				case 1307:
				case 4674:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_MAPLE;
							init_woodcutting(c);
							break;
					}
					break;
				/* Hollow trees.  */
				case 2289:
				case 4060:
				case 5551:
				case 5552:
				case 5553:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_HOLLOW;
							init_woodcutting(c);
							break;
					}
					break;
				/* Yew trees.  */
				case 1309:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_YEW;
							init_woodcutting(c);
							break;
					}
					break;
				/* Magic trees.  */
				case 1306:
					switch (c->plr->action_item_id)
					{
						/* All the woodcutting axes available.  */
						case 1349:
						case 1351:
						case 1353:
						case 1355:
						case 1357:
						case 1359:
						case 1361:
							c->plr->action_id_type = WOODCUTTING_MAGIC;
							init_woodcutting(c);
							break;
					}
					break;
				/* Pottery wheel.  */
				case 2642:
					switch (c->plr->action_item_id)
					{
						/* Soft clay.  */
						case 1761:
							printf("Soft clay!\n");
							c->plr->action_menu_id = 122;
							c->plr->action_menu_skill_id = CRAFTING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
					}
					break;
				/* Pottery oven.  */
				case 2643:
					switch (c->plr->action_item_id)
					{
						/* Unfired pot.  */
						case 1787:
						/* Unfired pie dish.  */
						case 1789:
						/* Unfired bowl.  */
						case 1791:
						/* Unfired pot lid.  */
						case 4438:
						/* Unfired plant pot.  */
						case 5352:
							c->plr->action_menu_id = 123;
							c->plr->action_menu_skill_id = CRAFTING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
					}
					break;
				/* Grain hopper.  */
				case 2713:
				case 2714:
				case 2715:
				case 2716:
				case 2717:
					switch (c->plr->action_item_id)
					{
						/* Grain.  */
						case 1947:
							if (c->plr->grain_hopper_count 
								<= skills_cooking_max_flour_in_bin)
							{
								packet_send_chatbox_message(c, 
									"You put the grain in the hopper.");
								c->plr->grain_hopper_count++;
								delete_item_from_inv_slot(c, 
									c->plr->action_item_slot, 1);
								player_play_animation(c, 832);
							}
							else
							{
								packet_send_chatbox_message(c, 
									"You can't fit anymore grain in there!");
							}
							break;
					}
					break;
				/* Furnace.  */
				case 2781:
					switch (c->plr->action_item_id)
					{
					/* Making molten glass.  */
						/* Soda ash.  */
						case 1781:
						/* Bucket of sand.  */
						case 1783:
							c->plr->action_menu_id 
								= CRAFTING_ACTION_MOLTEN_GLASS;
							c->plr->action_menu_skill_id = CRAFTING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
					/* Making jewelry.  */
						/* Gold bar.  */
						case 2357:
							c->plr->action_menu_id 
								= CRAFTING_ACTION_MAKING_JEWELRY;
							c->plr->action_menu_skill_id = CRAFTING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
						/* Silver bar.  */
						case 2355:
							c->plr->action_menu_id = CRAFTING_ACTION_SILVER;
							c->plr->action_menu_skill_id = CRAFTING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
						/* Regular ores, smelting.  */
						case 436:
						case 438:
						case 440:
						case 442:
						case 444:
						case 447:
						case 449:
						case 451:
						case 453:
							c->plr->action_menu_id = SMITHING_ACTION_SMELT;
							c->plr->action_menu_skill_id = SMITHING_SKILL;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
					}
					break;
				/* Anvil.  */
				case 2783:
					switch (c->plr->action_item_id)
					{
						/* Metal bars.  */
						case 2349:
						case 2351:
						case 2353:
						case 2355:
						case 2357:
						case 2359:
						case 2361:
						case 2363:
							c->plr->action_menu_id = SMITHING_ACTION_SMITH;
							c->plr->action_menu_skill_id = SMITHING_SKILL;
							c->plr->chat_type = SKILL_MENU_CHAT;
							c->plr->next_chat_id = 0;
							handle_dialogue(c);
							break;
					}
					break;
				/* Empty funeral pyre.  */
				case 4093:
					switch (c->plr->action_item_id)
					{
					/* Firemaking, adding logs.  */
						case 3438:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 40); 
							break;
						case 3440:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 41); 
							break;
						case 3442:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 42); 
							break;
						case 3444:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 43); 
							break;
						case 3446:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 44); 
							break;
						case 3448:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 45); 
							break;
						case 6211:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 46); 
							break;
						case 6213:
							add_logs_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot, 47); 
							break;
						/* Trying to add remains.  */
						case 3396:
						case 3398:
						case 3400:
						case 3402:
						case 3404:
							packet_send_chatbox_message(c, "You need to add logs first!");
							break;
					}
					break;
				
				/* Funeral pyre with logs.  */
				case 4094:
					switch (c->plr->action_item_id)
					{
					/* Firemaking, adding remains.  */
						case 3396:
						case 3398:
						case 3400:
						case 3402:
						case 3404:
							add_remains_to_pyre(c, c->plr->action_item_id, 
											 c->plr->action_item_slot); 
							break;
					}
					break;
				/* Funeral pyre all ready to go.  */
				case 4100:
					switch (c->plr->action_item_id)
					{
						/* Tinderbox.  */
						case 590:
							init_light_pyre_remains(c);
							break;
					}
					break;
				/* Dairy cow.  */
				case 8689:
					switch (c->plr->action_item_id)
					{
						case 1925:
							milk_dairy_cow(c);
							break;
					}
					break;
				default:
					packet_send_chatbox_message(c, "Nothing interesting happens.");
					printf("Unhandled item on object!\n");
					printf("Obj id: %d item id: %d\n", obj_id, 
							c->plr->action_item_id);
					set_player_state(c, STATE_IDLE);
					break;
			}
		}
		/* If a spell is used on an object.  */
		else if (c->plr->action_click_num == 5)
		{
			switch (obj_id)
			{
				/* Charging an orb on an obelisk.  */
				case 2150:
				case 2151:
				case 2152:
				case 2153:
					cast_charge_orb_on_obelisk(c, c->plr->action_id, 
											   c->plr->action_spell_id);
					break;
			}
		}

		/* If the player isn't going to start skilling, clear the state.  */
		if (is_player_state_type_skilling(c) == 0)
		{
			set_player_state(c, STATE_IDLE);
		}
	}
}
