/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
/* SMITHING - ID 13.  */

int get_smithing_number_made(int smithing_id)
{
	switch (smithing_id)
	{
		case SMITHING_ITEM_NAILS:
			return 15;
		case SMITHING_ITEM_DARTTIPS:
			return 10;
		case SMITHING_ITEM_ARROWTIPS:
			return 15;
		case SMITHING_ITEM_THROWINGKNIVES:
			return 5;
		default:
			return 1;
	}
}

int get_smithing_bar_id(int metal_bar_id)
{
	switch (metal_bar_id)
	{
		case 2349:
			return SMITHING_BAR_BRONZE;
		case 2351:
			return SMITHING_BAR_IRON;
		case 2355:
			return SMITHING_BAR_SILVER;
		case 2893:
			return SMITHING_BAR_ELEMENTAL;
		case 2353:
			return SMITHING_BAR_STEEL;
		case 2357:
			return SMITHING_BAR_GOLD;
		case 2359:
			return SMITHING_BAR_MITHRIL;
		case 2361:
			return SMITHING_BAR_ADAMANT;
		case 2363:
			return SMITHING_BAR_RUNE;
		default:
			return -1;
	}
}

int get_smithing_bar_id_rev(int choice_id)
{
	switch (choice_id)
	{
		case SMITHING_BAR_BRONZE:
			return 2349;
		case SMITHING_BAR_IRON:
			return 2351;
		case SMITHING_BAR_SILVER:
			return 2355;
		case SMITHING_BAR_ELEMENTAL:
			return 2893;
		case SMITHING_BAR_STEEL:
			return 2353;
		case SMITHING_BAR_GOLD:
			return 2357;
		case SMITHING_BAR_MITHRIL:
			return 2359;
		case SMITHING_BAR_ADAMANT:
			return 2361;
		case SMITHING_BAR_RUNE:
			return 2363;
		default:
			return -1;
	}
}

int get_smithing_level_needed(int smithing_id)
{
	switch (smithing_id)
	{
	/* Metal bar requirements.  */
		case SMITHING_BAR_BRONZE:
			return 0;
		case SMITHING_BAR_IRON:
			return 15;
		case SMITHING_BAR_SILVER:
			return 20;
		case SMITHING_BAR_ELEMENTAL:
			return 20;
		case SMITHING_BAR_STEEL:
			return 30;
		case SMITHING_BAR_GOLD:
			return 40;
		case SMITHING_BAR_MITHRIL:
			return 50;
		case SMITHING_BAR_ADAMANT:
			return 70;
		case SMITHING_BAR_RUNE:
			return 85;
	/* Smelting items, standard.  */
	/* 1st column.  */
		case SMITHING_ITEM_DAGGER:
			return 1;
		case SMITHING_ITEM_SWORD:
			return 4;
		case SMITHING_ITEM_SCIMY:
			return 5;
		case SMITHING_ITEM_LSWORD:
			return 6;
		case SMITHING_ITEM_2HSWORD:
			return 14;
	/* 2nd column.  */
		case SMITHING_ITEM_AXE:
			return 1;
		case SMITHING_ITEM_MACE:
			return 2;
		case SMITHING_ITEM_WARHAMMER:
			return 9;
		case SMITHING_ITEM_BAXE:
			return 10;
		case SMITHING_ITEM_CLAWS:
			return 13;
	/* 3rd column.  */
		case SMITHING_ITEM_CHAINBODY:
			return 11;
		case SMITHING_ITEM_PLATELEGS:
			return 16;
		case SMITHING_ITEM_PLATESKIRT:
			return 16;
		case SMITHING_ITEM_PLATEBODY:
			return 18;
		case SMITHING_ITEM_LANTERNFRAME:
			return 11;
	/* 4th column.  */
		case SMITHING_ITEM_MEDHELM:
			return 3;
		case SMITHING_ITEM_FULLHELM:
			return 7;
		case SMITHING_ITEM_SQSHIELD:
			return 8;
		case SMITHING_ITEM_KITESHIELD:
			return 12;
		case SMITHING_ITEM_NAILS:
			return 4;
	/* 5th column.  */
		case SMITHING_ITEM_DARTTIPS:
			return 4;
		case SMITHING_ITEM_ARROWTIPS:
			return 5;
		case SMITHING_ITEM_THROWINGKNIVES:
			return 7;
		case SMITHING_ITEM_BRONZEWIRE:
			return 4;
		case SMITHING_ITEM_STEELSTUDS:
			return 6;

		default:
			return -1;
	}
}

int get_smithing_bars_needed(int smithing_id)
{
	switch (smithing_id)
	{
	/* Smelting items, standard.  */
	/* 1st column.  */
		case SMITHING_ITEM_DAGGER:
			return 1;
		case SMITHING_ITEM_SWORD:
			return 1;
		case SMITHING_ITEM_SCIMY:
			return 2;
		case SMITHING_ITEM_LSWORD:
			return 2;
		case SMITHING_ITEM_2HSWORD:
			return 3;
	/* 2nd column.  */
		case SMITHING_ITEM_AXE:
			return 1;
		case SMITHING_ITEM_MACE:
			return 1;
		case SMITHING_ITEM_WARHAMMER:
			return 3;
		case SMITHING_ITEM_BAXE:
			return 3;
		case SMITHING_ITEM_CLAWS:
			return 2;
	/* 3rd column.  */
		case SMITHING_ITEM_CHAINBODY:
			return 3;
		case SMITHING_ITEM_PLATELEGS:
			return 3;
		case SMITHING_ITEM_PLATESKIRT:
			return 3;
		case SMITHING_ITEM_PLATEBODY:
			return 5;
		case SMITHING_ITEM_LANTERNFRAME:
			return 1;
	/* 4th column.  */
		case SMITHING_ITEM_MEDHELM:
			return 1;
		case SMITHING_ITEM_FULLHELM:
			return 2;
		case SMITHING_ITEM_SQSHIELD:
			return 2;
		case SMITHING_ITEM_KITESHIELD:
			return 3;
		case SMITHING_ITEM_NAILS:
			return 1;
	/* 5th column.  */
		case SMITHING_ITEM_DARTTIPS:
			return 1;
		case SMITHING_ITEM_ARROWTIPS:
			return 1;
		case SMITHING_ITEM_THROWINGKNIVES:
			return 1;
		case SMITHING_ITEM_BRONZEWIRE:
			return 1;
		case SMITHING_ITEM_STEELSTUDS:
			return 1;

		default:
			return -1;
	}
}

int get_smithing_id_from_ore(int ore_id)
{
	switch (ore_id)
	{
		case 436:
		case 438:
			return SMITHING_BAR_BRONZE;
		case 440:
			return SMITHING_BAR_IRON;
		case 442:
			return SMITHING_BAR_SILVER;
		case 2892:
			return SMITHING_BAR_ELEMENTAL;
		case 444:
			return SMITHING_BAR_GOLD;
		case 447:
			return SMITHING_BAR_MITHRIL;
		case 449:
			return SMITHING_BAR_ADAMANT;
		case 451:
			return SMITHING_BAR_RUNE;
		
		default:
			printf("Ore id %d not handled!\n", ore_id);
			return -1;
	}
}

/**
 * @return Returns the xp value for smelting bars.
 */
float get_smelting_xp(int smithing_id)
{
	/* Making bars from ore.  */
	switch (smithing_id)
	{
		case SMITHING_BAR_BRONZE:
			return 6.25;
		case SMITHING_BAR_IRON:
			return 12.5;
		case SMITHING_BAR_SILVER:
			return 13.67;
		case SMITHING_BAR_ELEMENTAL:
			return 7.5;
		case SMITHING_BAR_STEEL:
			return 17.5;
		case SMITHING_BAR_GOLD:
			return 22.5;
		case SMITHING_BAR_MITHRIL:
			return 30.0;
		case SMITHING_BAR_ADAMANT:
			return 37.5;
		case SMITHING_BAR_RUNE:
			return 50.0;

		default:
			return 0.0;
	}
}

float get_smithing_xp(int smithing_id)
{
	switch (smithing_id)
	{
		case SMITHING_BAR_BRONZE:
			return 12.5;
		case SMITHING_BAR_IRON:
			return 25.0;
		case SMITHING_BAR_SILVER:
			return 0.0;
		case SMITHING_BAR_ELEMENTAL:
			return 0.0; 
		case SMITHING_BAR_STEEL:
			return 37.5;
		case SMITHING_BAR_GOLD:
			return 30.0;
		case SMITHING_BAR_MITHRIL:
			return 50.0;
		case SMITHING_BAR_ADAMANT:
			return 62.5;
		case SMITHING_BAR_RUNE:
			return 75.0;

		default:
			return 0.0;
	}
}

int get_smelting_ore_needed(int smithing_id, int secondary_ore)
{
	if (secondary_ore == 0)
	{
		/* Making bars from ore.  */
		switch (smithing_id)
		{
			case SMITHING_BAR_BRONZE:
				return 436;
			case SMITHING_BAR_IRON:
				return 440;
			case SMITHING_BAR_SILVER:
				return 442;
			case SMITHING_BAR_ELEMENTAL:
				return 2892;
			case SMITHING_BAR_STEEL:
				return 440;
			case SMITHING_BAR_GOLD:
				return 444;
			case SMITHING_BAR_MITHRIL:
				return 447;
			case SMITHING_BAR_ADAMANT:
				return 449;
			case SMITHING_BAR_RUNE:
				return 451;

			default:
				return -1;
		}
	}
	else
	{
		/* Making bars from ore.  */
		switch (smithing_id)
		{
			case SMITHING_BAR_BRONZE:
				return 438;
			case SMITHING_BAR_IRON:
				return -1;
			case SMITHING_BAR_SILVER:
				return -1;
			case SMITHING_BAR_ELEMENTAL:
				return 453;
			case SMITHING_BAR_STEEL:
				return 453;
			case SMITHING_BAR_GOLD:
				return -1;
			case SMITHING_BAR_MITHRIL:
				return 453;
			case SMITHING_BAR_ADAMANT:
				return 453;
			case SMITHING_BAR_RUNE:
				return 453;

			default:
				return -1;
		}
	}
}

int get_smelting_ore_number_needed(int smithing_id, int secondary_ore)
{
	if (secondary_ore == 0)
	{
		/* Primary ore needed, how many.  */
		switch (smithing_id)
		{
			case SMITHING_BAR_BRONZE:
			case SMITHING_BAR_IRON:
			case SMITHING_BAR_SILVER:
			case SMITHING_BAR_ELEMENTAL:
			case SMITHING_BAR_STEEL:
			case SMITHING_BAR_GOLD:
			case SMITHING_BAR_MITHRIL:
			case SMITHING_BAR_ADAMANT:
			case SMITHING_BAR_RUNE:
				return 1;

			default:
				return -1;
		}
	}
	else
	{
		/* Making bars from ore.  */
		switch (smithing_id)
		{
			case SMITHING_BAR_BRONZE:
				return 1;
			case SMITHING_BAR_IRON:
				return -1;
			case SMITHING_BAR_SILVER:
				return -1;
			case SMITHING_BAR_ELEMENTAL:
				return 4;
			case SMITHING_BAR_STEEL:
				return 2;
			case SMITHING_BAR_GOLD:
				return -1;
			case SMITHING_BAR_MITHRIL:
				return 4;
			case SMITHING_BAR_ADAMANT:
				return 6;
			case SMITHING_BAR_RUNE:
				return 8;

			default:
				return -1;
		}
	}
}

/**
 * Sends the proper message to the client based off of what they are making.
 * @param *c Client instance.
 * @param metal_id Metal bar used.
 * @param smithing_id ID of the item type they are crafting.
 */
void smithing_return_message(struct client *c, int metal_id, int smithing_id)
{
	char metal_name[10];
	char return_msg[100];

	/* Get the name of the metal bars.  */
	switch (metal_id)
	{
		case SMITHING_BAR_BRONZE:
			strncpy(metal_name, "bronze", 10);
			break;
		case SMITHING_BAR_IRON:
			strncpy(metal_name, "iron", 10);
			break;
		case SMITHING_BAR_SILVER:
			strncpy(metal_name, "silver", 10);
			break;
		case SMITHING_BAR_ELEMENTAL:
			strncpy(metal_name, "elemental", 10);
			break;
		case SMITHING_BAR_STEEL:
			strncpy(metal_name, "steel", 10);
			break;
		case SMITHING_BAR_GOLD:
			strncpy(metal_name, "gold", 10);
			break;
		case SMITHING_BAR_MITHRIL:
			strncpy(metal_name, "mithril", 10);
			break;
		case SMITHING_BAR_ADAMANT:
			strncpy(metal_name, "adamant", 10);
			break;
		case SMITHING_BAR_RUNE:
			strncpy(metal_name, "rune", 10);
			break;
	}

	switch (smithing_id)
	{
		case SMITHING_ITEM_DAGGER:
			snprintf(return_msg, 100, "You hammer the %s and make a dagger.", 
				metal_name);
			break;
		case SMITHING_ITEM_SWORD:
			snprintf(return_msg, 100, "You hammer the %s and make a sword.", 
				metal_name);
			break;
		case SMITHING_ITEM_SCIMY:
			snprintf(return_msg, 100, "You hammer the %s and make a scimitar.", 
				metal_name);
			break;
		case SMITHING_ITEM_LSWORD:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"longsword.", metal_name);
			break;
		case SMITHING_ITEM_2HSWORD:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"two-handed sword.", metal_name);
			break;
	/* 2nd column.  */
		case SMITHING_ITEM_AXE:
			snprintf(return_msg, 100, "You hammer the %s and make an axe.", 
				metal_name);
			break;
		case SMITHING_ITEM_MACE:
			snprintf(return_msg, 100, "You hammer the %s and make a mace.", 
				metal_name);
			break;
		case SMITHING_ITEM_WARHAMMER:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"warhammer.", metal_name);
			break;
		case SMITHING_ITEM_BAXE:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"battleaxe.", metal_name);
			break;
		case SMITHING_ITEM_CLAWS:
			snprintf(return_msg, 100, "You hammer the %s and make a pair of "
				"claws.", metal_name);
			break;
	/* 3rd column.  */
		case SMITHING_ITEM_CHAINBODY:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"chainbody.", metal_name);
			break;
		case SMITHING_ITEM_PLATELEGS:
			snprintf(return_msg, 100, "You hammer the %s and make a pair of "
				"platelegs.", metal_name);
			break;
		case SMITHING_ITEM_PLATESKIRT:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"plateskirt.", metal_name);
			break;
		case SMITHING_ITEM_PLATEBODY:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"platebody.", metal_name);
			break;
		case SMITHING_ITEM_LANTERNFRAME:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"oil lantern frame.", metal_name);
			break;
	/* 4th column.  */
		case SMITHING_ITEM_MEDHELM:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"medium helm.", metal_name);
			break;
		case SMITHING_ITEM_FULLHELM:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"full helm.", metal_name);
			break;
		case SMITHING_ITEM_SQSHIELD:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"square shield.", metal_name);
			break;
		case SMITHING_ITEM_KITESHIELD:
			snprintf(return_msg, 100, "You hammer the %s and make a "
				"kite shield.", metal_name);
			break;
		case SMITHING_ITEM_NAILS:
			snprintf(return_msg, 100, "You hammer the %s and make ten "
				"nails.", metal_name);
			break;
	/* 5th column.  */
		case SMITHING_ITEM_DARTTIPS:
			snprintf(return_msg, 100, "You hammer the %s and make ten "
				"dart tips.", metal_name);
			break;
		case SMITHING_ITEM_ARROWTIPS:
			snprintf(return_msg, 100, "You hammer the %s and make fifteen "
				"arrow tips.", metal_name);
			break;
		case SMITHING_ITEM_THROWINGKNIVES:
			snprintf(return_msg, 100, "You hammer the %s and make five "
				"knives.", metal_name);
			break;
		case SMITHING_ITEM_BRONZEWIRE:
			snprintf(return_msg, 100, "You hammer the %s and make bronze "
				"wire.", metal_name);
			break;
		case SMITHING_ITEM_STEELSTUDS:
			snprintf(return_msg, 100, "You hammer the %s and make a set of "
				"steel studs.", metal_name);
			break;
	}

	packet_send_chatbox_message(c, return_msg);
}

/**
 * Sends the client the proper message for what metal they are smelting.
 * @param *c Client instance.
 * @param metal_id Metal bar being made.
 * @param state What part of the smelting process the player is at.
 */
void smelting_messages(struct client *c, int metal_id, int state)
{
	char metal_name[10];
	char return_msg[100];

	/* Get the name of the metal bars.  */
	switch (metal_id)
	{
		case SMITHING_BAR_BRONZE:
			strncpy(metal_name, "bronze", 10);
			break;
		case SMITHING_BAR_IRON:
			strncpy(metal_name, "iron", 10);
			break;
		case SMITHING_BAR_SILVER:
			strncpy(metal_name, "silver", 10);
			break;
		case SMITHING_BAR_ELEMENTAL:
			strncpy(metal_name, "elemental", 10);
			break;
		case SMITHING_BAR_STEEL:
			strncpy(metal_name, "steel", 10);
			break;
		case SMITHING_BAR_GOLD:
			strncpy(metal_name, "gold", 10);
			break;
		case SMITHING_BAR_MITHRIL:
			strncpy(metal_name, "mithril", 10);
			break;
		case SMITHING_BAR_ADAMANT:
			strncpy(metal_name, "adamant", 10);
			break;
		case SMITHING_BAR_RUNE:
			strncpy(metal_name, "rune", 10);
			break;
	}
	
	/* Putting the ore into the furnace.  */
	if (state == SMITHING_ACTION_SMELT_BEGIN)
	{
		snprintf(return_msg, 100, "You smelt the %s in the furnace.", 
				 metal_name);
		packet_send_chatbox_message(c, return_msg);
	}
	/* Taking the bar out of the furnace.  */
	else if (state == SMITHING_ACTION_SMELT_RETRIEVE)
	{
		snprintf(return_msg, 100, "You retrieve a bar of %s.", metal_name);
		packet_send_chatbox_message(c, return_msg);
	}
	/* Failure state, iron bars.  */
	else if (state == SMITHING_ACTION_SMELT_FAILURE)
	{
		packet_send_chatbox_message(c, "The ore is too impure and you fail to refine it.");
	}
}

/**
 * Make sure that everything is all setup before the smithing process is
 * begun.
 * @param *c Client instance.
 */
void init_smithing(struct client *c)
{
	char return_msg[100];
	int number_of_bars
		= player_has_num_items_non_stack(c, c->plr->action_item_id);
	int number_of_bars_needed = 0;
	int smithing_level = get_player_skill_level(c, SMITHING_SKILL);

	/* First ore that is required to make the bar, and how many are needed.  */
	int ore_type_needed = 0;
	int ore_type_num_needed = 0;
	/* Second ore that is required to make the bar, and how many are 
	 * needed.  */
	int ore_type_needed2 = 0;
	int ore_type_num_needed2 = 0;
	
	int primary_ore_has = 0;
	int secondary_ore_has = 0;

	c->plr->action_skill_id = SMITHING_SKILL;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_SMITHING);

	/* Set up checking for requirements met.  */
	if (c->plr->action_skill_type == SMITHING_ACTION_SMELT)
	{
		c->plr->action_skill_lvl_req 
		= get_smithing_level_needed(c->plr->action_id_type);

		/* Get ores needed, how many.  */
		ore_type_needed = get_smelting_ore_needed(c->plr->action_id_type, 0);
		ore_type_num_needed 
			= get_smelting_ore_number_needed(c->plr->action_id_type, 0);

		ore_type_needed2 = get_smelting_ore_needed(c->plr->action_id_type, 1);
		ore_type_num_needed2 
			= get_smelting_ore_number_needed(c->plr->action_id_type, 1);
	}
	else if (c->plr->action_skill_type == SMITHING_ACTION_SMITH)
	{
		c->plr->action_skill_lvl_req 
		= get_smithing_level_needed(c->plr->action_id_type);

		/* Add the base bar level.  */
		c->plr->action_skill_lvl_req 
			+= get_smithing_level_needed(c->plr->action_id_type2);

		/* Set to 99 if over.  */
		if (c->plr->action_skill_lvl_req > 99)
		{
			c->plr->action_skill_lvl_req = 99;
		}

		/* Find number of metal bars required.  */
		number_of_bars_needed 
			= get_smithing_bars_needed(c->plr->action_id_type);
	}

	/* Check level requirement.  */
	if (smithing_level < c->plr->action_skill_lvl_req)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "smithing to make that!\n", 
				 c->plr->action_skill_lvl_req);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Smithing bar number check.  */
	if (c->plr->action_skill_type == SMITHING_ACTION_SMITH 
		&& number_of_bars < number_of_bars_needed)
	{
		snprintf(return_msg, 100, "You need %d metal bars to make that!", 
				 number_of_bars_needed);
		packet_send_chatbox_message(c, return_msg);
			
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Smelting ore check.  */
	if (c->plr->action_skill_type == SMITHING_ACTION_SMELT)
	{
		primary_ore_has = player_has_item_non_stack(c, ore_type_needed, 
			ore_type_num_needed);		

		if (ore_type_needed2 != -1)
		{
			secondary_ore_has = player_has_item_non_stack(c, ore_type_needed2, 
				ore_type_num_needed2);
			
			if ((primary_ore_has < ore_type_num_needed) 
				|| (secondary_ore_has < ore_type_num_needed2))
			{
				packet_send_chatbox_message(c, "You don't have enough ore to make that!");	
				set_player_state(c, STATE_IDLE);
				return;
			}
		}
		else
		{
			if ((primary_ore_has < ore_type_num_needed))
			{
				packet_send_chatbox_message(c, "You don't have enough ore to make that!");	
				set_player_state(c, STATE_IDLE);
				return;
			}
		}
	}

	printf("All ready, being making the item!\n");
}

/**
 * Handle creating the finished process from the smithing task.
 * @param *c Client instance.
 */
void handle_smithing(struct client *c)
{
	int item_to_make = c->plr->action_skill_result_item;
	int num_to_make = get_smithing_number_made(c->plr->action_id_type);
	int number_of_bars
		= player_has_num_items_non_stack(c, c->plr->action_item_id);
	int number_of_bars_needed 
		= get_smithing_bars_needed(c->plr->action_id_type);
	float bar_base_xp = get_smithing_xp(c->plr->action_id_type2);
	int x = 0;

	/* Time to get back the resulting item.  */
	if (c->plr->action_skill_waiting_for_resource == 1)
	{
		/* Remove all bars needed to make the item.  */
		for (x = 0; x < number_of_bars_needed; x++)
		{
			delete_item_from_inv(c, c->plr->action_item_id, 1);
		}
		
		add_item_to_inv(c, item_to_make, num_to_make, -1);	
		c->plr->action_skill_waiting_for_resource = 0;
		add_skill_xp(c, SMITHING_SKILL, 
			(bar_base_xp * (float) number_of_bars_needed));
		smithing_return_message(c, c->plr->action_id_type2, 
			c->plr->action_id_type);
		player_replay_animation(c);
		return;
	}

	/* Out of metal bars.  */
	if (c->plr->action_skill_remaining_actions == 0 
		|| number_of_bars < number_of_bars_needed)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	player_play_animation(c, SMITHING_EMOTE_SMITH);
	c->plr->action_skill_waiting_for_resource = 1;

	c->plr->action_ticks = 4;

	c->plr->action_total_time_acted++;
	c->plr->action_skill_remaining_actions--;
}

/**
 * Process smelting ore and receive the metal bar created.
 * @param *c Client instance.
 */
void handle_smelting(struct client *c)
{
	int item_to_make = c->plr->action_skill_result_item;
	float bar_xp = get_smithing_xp(c->plr->action_id_type);
	
	/* First ore that is required to make the bar, and how many are needed.  */
	int ore_type_needed = get_smelting_ore_needed(c->plr->action_id_type, 0);
	int ore_type_num_needed 
		= get_smelting_ore_number_needed(c->plr->action_id_type, 0);

	/* Second ore that is required to make the bar, and how many are 
	 * needed.  */
	int ore_type_needed2 = get_smelting_ore_needed(c->plr->action_id_type, 1);
	int ore_type_num_needed2 
		= get_smelting_ore_number_needed(c->plr->action_id_type, 1);

	int primary_ore_has = 0;
	int secondary_ore_has = 0;

	int x = 0;

	/* Time to get back the resulting item.  */
	if (c->plr->action_skill_waiting_for_resource == 1)
	{
		/* Remove all ores needed to make the metal bar.  */
		for (x = 0; x < ore_type_num_needed; x++)
		{
			delete_item_from_inv(c, ore_type_needed, 1);
		}

		if (ore_type_num_needed2 != -1 && ore_type_needed2 != -1)
		{
			for (x = 0; x < ore_type_num_needed2; x++)
			{
				delete_item_from_inv(c, ore_type_needed2, 1);
			}
		}
		
		/* Check for success on making an iron bar.  */
		if (c->plr->action_id_type == SMITHING_BAR_IRON)
		{
			/* If player is wearing a ring of forging with charges left, use 
			 * it.  */
			if (c->plr->equipment[RING_SLOT] == 2568 
				&& c->plr->equipment_charges[RING_SLOT] > 0)
			{
				smelting_messages(c, SMITHING_BAR_IRON, 
								  SMITHING_ACTION_SMELT_RETRIEVE);
				add_item_to_inv(c, item_to_make, 1, -1);	
				add_skill_xp(c, SMITHING_SKILL, bar_xp);
				remove_equipment_item_charges(c, RING_SLOT, 1);
			}
			else
			{
				switch (rand_int(0, 1))
				{
					/* Failure.  */
					case 0:
						smelting_messages(c, SMITHING_BAR_IRON, 
										  SMITHING_ACTION_SMELT_FAILURE);
						break;
					case 1:
						smelting_messages(c, SMITHING_BAR_IRON, 
										  SMITHING_ACTION_SMELT_RETRIEVE);
						add_item_to_inv(c, item_to_make, 1, -1);	
						add_skill_xp(c, SMITHING_SKILL, bar_xp);
						break;
				}
			}
		}
		else
		{
			smelting_messages(c, c->plr->action_id_type, 
							  SMITHING_ACTION_SMELT_RETRIEVE);
			add_item_to_inv(c, item_to_make, 1, -1);	
			add_skill_xp(c, SMITHING_SKILL, bar_xp);
		}

		c->plr->action_skill_waiting_for_resource = 0;
		return;
	}

	/* Check if number of actions chosen has been completed.  */
	if (c->plr->action_skill_remaining_actions == 0)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Check if player has enough ores, reset skilling if not.  */
	primary_ore_has = player_has_item_non_stack(c, ore_type_needed, 
		ore_type_num_needed);		

	if (ore_type_needed2 != -1)
	{
		secondary_ore_has = player_has_item_non_stack(c, ore_type_needed2, 
			ore_type_num_needed2);
		
		if ((primary_ore_has < ore_type_num_needed) 
			|| (secondary_ore_has < ore_type_num_needed2))
		{
			set_player_state(c, STATE_IDLE);
			return;
		}
	}
	else
	{
		if ((primary_ore_has < ore_type_num_needed))
		{
			set_player_state(c, STATE_IDLE);
			return;
		}
	}

	smelting_messages(c, c->plr->action_id_type, SMITHING_ACTION_SMELT_BEGIN);
	player_play_animation(c, SMITHING_EMOTE_SMELT);
	c->plr->action_skill_waiting_for_resource = 1;

	c->plr->action_ticks = 4;

	c->plr->action_total_time_acted++;
	c->plr->action_skill_remaining_actions--;
}
