/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file item_data.c
 * @brief Functions relating to item infomation, stats.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"
#include "misc.h"
#include "rng.h"

/**
 * Returns the id of an item's variant that has water in it.
 * @param container_id Id of the container object to check for a water filled
 * version.
 * @return Returns id of the container with water, -1 if non found.
 */
int get_container_water_id(int container_id)
{
	switch (container_id)
	{
		/* Vial.  */
		case 229:
			return 227;
		/* Bowl.  */
		case 1923:
			return 1921;
		/* Bucket.  */
		case 1925:
			return 1929;
		/* Jugs.  */
		case 1935:
			return 1937;
		/* Cup.  */
		case 1980:
			return 4458;
		/* Waterskins.  */
		case 1825:
		case 1827:
		case 1829:
		case 1831:
			return 1823;
		/* Watering cans.  */
		case 5331:
		case 5333:
		case 5334:
		case 5335:
		case 5336:
		case 5337:
		case 5338:
		case 5339:
			return 5340;
		default:
			return -1;
	}
}

/**
 * Checks if an item is one that holds water, can be used for skills.
 * @param item_id Id of the item to check.
 * @return Returns one if the item id is a water container that can be used for
 * crafting, cooking, etc.
 */
int check_for_water_container(int item_id)
{
	switch (item_id)
	{
		case 277:
		case 1921:
		case 1929:
		case 1937:
		case 4458:
			return 1;
		default:
			return 0;
	}
}

/**
 * Fetches the amount of health a food heals.
 * @param food_id Id of the food to check healing amount for.
 * @returns Returns the number of hitpoints a food heals.
 */
int get_food_healing_amount(int food_id)
{
	switch (food_id)
	{
	/* MEAT.  */
		/* Meat, chicken, rabbit.  */
		case 2142:
		case 2140:
		case 3228:
		/* Ugthanki meat.  */
		case 1861:
			return 3;
	/* Ugthanki kebab.  */
		case 1885:
			return 19;
	/* FISH.  */
		/* Shrimp, karambwanji, sardine, anchovies, poison karambwan.  */
		case 315:
			return 3;
		case 3151:
			return 3;
		case 325:
			return 4;
		case 319:
			return 1;
		case 3146:
			return -5;
		/* Herring.  */
		case 347:
			return 5;
		/* Mackerel.  */
		case 355:
			return 6;
		/* Trout.  */
		case 333:
			return 7;
		/* Cod.  */
		case 339:
			return 7;
		/* Pike.  */
		case 351:
			return 8;
		/* Salmon.  */
		case 329:
			return 9;
		/* Slimey eel.  */
		case 3381:
			return rand_int(6, 10);
		/* Tuna, cooked karambwan.  */
		case 361:
			return 10;
		case 3144:
			return 18;
		/* Lobster.  */
		case 379:
			return 12;
		/* Bass.  */
		case 365:
			return 13;
		/* Swordfish.  */
		case 373:
			return 14;
		/* Lava eel.  */
		case 2149:
			return 14;
		/* Shark.  */
		case 385:
			return 20;
		/* Sea turtle.  */
		case 397:
			return 21;
		/* Manta ray.  */
		case 391:
			return 22;

		/* Bread.  */
		case 2309:
			return 5;
	/* PIES.  */
		/* Redberry.  */
		case 2325:
		case 2333:
			return 5;
		/* Meat pie.  */
		case 2327:
		case 2331:
			return 6;
		/* Apple pie.  */
		case 2323:
		case 2335:
			return 7;
	/* PIZZAS.  */
		/* Plain pizza.  */
		case 2289:
		case 2291:
			return 7;
		/* Meat pizza.  */
		case 2293:
		case 2295:
			return 8;
		/* Anchovy pizza.  */
		case 2297:
		case 2299:
			return 9;
		/* Pineapple pizza.  */
		case 2301:
		case 2303:
			return 11;
	/* CAKES.  */
		/* Cake.  */
		case 1891:
		case 1893:
		case 1895:
			return 4;
		/* Chocolate cake.  */
		case 1897:
		case 1899:
		case 1901:
			return 5;
	/* WINES.  */
		/* Wine.  */
		case 1993:
			return 11;
	/* BREWING.  */
		/* Cider.  */
		case 5763:
		case 5765:
			return 2;
		/* Dwarven stout.  */
		case 1913:
		case 5747:
			return 1;
		/* Asgarnian ale.  */
		case 1905:
		case 5739:
			return 2;
		/* Greenman's ale.  */
		case 1909:
		case 5743:
			return 1;
		/* Wizard's mind bomb.  */
		case 1907:
		case 5741:
			return 1;
		/* Dragon bitter.  */
		case 1911:
		case 5745:
			return 1;
		/* Moonlight mead.  */
		case 2955:
			return 4;
		case 5749:
			return 6;
		/* Axeman's folly.  */
		case 5751:
		case 5753:
			return 1;
		/* Chef's delight.  */
		case 5755:
		case 5757:
			return 1;
		/* Slayer's respite.  */
		case 5759:
		case 5761:
			return 1;
	/* TODO: Add in progress foods before this.  */
	/* GNOME COOKING.  */
		/* Fruit blast.  */
		case 2084:
			return 9;
		/* Pineapple punch.  */
		case 2048:
			return 9;
		/* Toad crunchies.  */
		case 2217:
			return 8;
		/* Spicy crunchies.  */
		case 2213:
			return 7;
		/* Worm crunchies.  */
		case 2205:
			return 8;
		/* Chocchip crunchies.  */
		case 2209:
			return 7;
		/* Wizard blizzard.  */
		case 2054:
			return 5;
		/* Short green guy.  */
		case 2080:
			return 5;
		/* Fruit batta.  */
		case 2277:
			return 11;
		/* Toad batta.  */
		case 2255:
			return 11;
		/* Worm batta.  */
		case 2253:
			return 11;
		/* Vegetable batta.  */
		case 2281:
			return 11;
		/* Cheese and tomato batta.  */
		case 2259:
			return 11;
		/* Worm hole.  */
		case 2191:
			return 12;
		/* Drunk dragon.  */
		case 2092:
			return 5;
		/* Choc saturday.  */
		case 2074:
			return 5;
		/* Veg ball.  */
		case 2195:
			return 12;
		/* Blurberry special.  */
		case 2064:
			return 7;
		/* Tangled toad's legs.  */
		case 2187:
			return 15;
		/* Chocolate bomb.  */
		case 2185:
			return 15;

		/* Unfinished cocktails. FIXME: Healing amount not accurate.  */
		case 2042:
		case 2044:
		case 2046:
		case 2050:
		case 2052:
		case 2056:
		case 2058:
		case 2060:
		case 2062:
		case 2066:
		case 2068:
		case 2070:
		case 2072:
		case 2076:
		case 2078:
		case 2082:
		case 2086:
		case 2088:
		case 2090:
			return 1;
		/* Cabbage.  */
		case 1965:
		/* Draynor cabbage.  */
		case 1967:
			return 1;
		/* Spinach roll.  */
		case 1969:
			return 2;
		/* Chocolate bar.  */
		case 1973:
			return 3;
		/* Chocolately milk.  */
		case 1977:
			return 4;
		/* Guthix rest tea.  */
		case 4417:
		case 4419:
		case 4421:
		case 4423:
			return 5;
		/* Banana, sliced.  */
		case 1963:
		case 3162:
			return 2;
		/* Edible Seaweed.  */
		case 403:
			return 4;
		/* Grog.  */
		case 1915:
			return 3;
		/* Beer.  */
		case 1917:
		case 3803:
			return 1;
		/* Potato.  */
		case 1942:
		/* Onion.  */
		case 1957:
			return 1;
		/* Tomato.  */
		case 1982:
			return 2;
		/* Cheese.  */
		case 1985:
			return 2;
		/* Pumpkin, easter egg.  */
		case 1959:
		case 1961:
			return 14;
		/* Stew.  */
		case 2003:
			return 11;
		/* Curry.  */
		case 2011:
			return 19;
		/* Vodka.  */
		case 2015:
			return 5;
		/* Whisky.  */
		case 2017:
			return 5;
		/* Gin.  */
		case 2019:
			return 5;
		/* Brandy.  */
		case 2021:
			return 5;
		/* Lemon, slices, chunks.  */
		case 2102:
		case 2014:
		case 2106:
			return 2;
		/* Orange, slices, chunks.  */
		case 2108:
		case 2110:
		case 2112:
			return 2;
		/* Pineapple chunks, ring.  */
		case 2116:
		case 2118:
			return 2;
		/* Lime, slices, chunks.  */
		case 2120:
		case 2122:
		case 2124:
			return 2;
		/* Dwellberries.  */
		case 2126:
			return 2;
		/* Equa leaves.  */
		case 2128:
			return 1;
		/* Pot of cream.  */
		case 2130:
			return 1;
		/* Toad legs.  */
		case 2152:
			return 3;
		/* King worm.  */
		case 2162:
			return 2;
		/* Keg of beer.  */
		case 3801:
			return 15;
		/* Monkey nuts.  */
		case 4012:
			return 4;
		/* Monkey bar.  */
		case 4014:
			return 5;
		/* Banana stew.  */
		case 4016:
			return 11;
		/* Giant frog legs.  */
		case 4517:
			return 6;
		/* Bandit's brew.  */
		case 4627:
			return 1;
		/* Cave eel.  */
		case 5003:
			return rand_int(8, 12);
		/* Frog spawn.  */
		case 5004:
			return rand_int(3, 6);
		/* Papaya fruit.  */
		case 5972:
			return 8;
		/* Spider on a stick, shaft.  */
		case 6297:
		case 6299:
			return rand_int(7, 10);
		/* Gout tuber.  */
		case 6311:
			return 12;
		/* White tree fruit.  */
		case 6469:
			return 3;
		/* No healing.  */
		case 247:
		case 464:
		case 712:
		case 1991:
		case 4620:
			return 0;
		default:
			printf("Unknown food id: %d!\n", food_id);
			return 0;
	}
}

/**
 * Checks the type of a consume message a food needs to play.
 * @param food_id Id of the food to check.
 * @param *food_name Name of the food.
 * @param *eat_msg String to modify with the end message.
 * @param size Size of the eat_msg string.
 * @todo Change this to use data from the items database. Shouldn't need this
 * here.
 */
void get_food_consume_message(int food_id, char *food_name, char *eat_msg, 
							  int size)
{
	switch (food_id)
	{
		/* Drinks.  */
		case 1993:
		case 113:
		case 114:
		case 115:
		case 116:
		case 117:
		case 118:
		case 119:
		case 120:
		case 121:
		case 122:
		case 123:
		case 124:
		case 125:
		case 126:
		case 127:
		case 128:
		case 129:
		case 130:
		case 131:
		case 132:
		case 133:
		case 134:
		case 135:
		case 136:
		case 137:
		case 138:
		case 139:
		case 140:
		case 141:
		case 142:
		case 143:
		case 144:
		case 145:
		case 146:
		case 147:
		case 148:
		case 149:
		case 150:
		case 151:
		case 152:
		case 153:
		case 154:
		case 155:
		case 156:
		case 157:
		case 158:
		case 159:
		case 160:
		case 161:
		case 162:
		case 163:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 170:
		case 171:
		case 172:
		case 173:
		case 174:
		case 175:
		case 176:
		case 177:
		case 178:
		case 179:
		case 180:
		case 181:
		case 182:
		case 183:
		case 184:
		case 185:
		case 186:
		case 189:
		case 190:
		case 191:
		case 192:
		case 193:
		case 194:
		case 2428:
		case 2429:
		case 2430:
		case 2431:
		case 2432:
		case 2433:
		case 2434:
		case 2435:
		case 2436:
		case 2437:
		case 2438:
		case 2439:
		case 2440:
		case 2441:
		case 2442:
		case 2443:
		case 2444:
		case 2445:
		case 2446:
		case 2447:
		case 2448:
		case 2449:
		case 2450:
		case 2451:
		case 2452:
		case 2453:
		case 2454:
		case 2455:
		case 2456:
		case 2457:
		case 2458:
		case 2459:
		case 3008:
		case 3010:
		case 3012:
		case 3014:
		case 3016:
		case 3018:
		case 3020:
		case 3022:
		case 3024:
		case 3026:
		case 3028:
		case 3030:
		case 3032:
		case 3034:
		case 3036:
		case 3038:
		case 3040:
		case 3042:
		case 3044:
		case 3046:
		case 4417:
		case 4419:
		case 4421:
		case 4423:
			snprintf(eat_msg, size, "You drink the %s.", food_name);
			break;
		/* Cabbage.  */
		case 1965:
			snprintf(eat_msg, size, "You eat the %s. Yuck!", food_name);
			break;
		/* Moonlight mead.  */
		case 2955:
		case 5749:
			strncpy(eat_msg, "It tastes like something just died in your "
					"mouth.", size);
			break;
		/* Black mushroom.  */
		case 4620:
			strncpy(eat_msg, "Eugh! It tastes horrible, and stains your "
					"fingers black.", size);
			break;
		/* Frog spawn.  */
		case 5004:
			strncpy(eat_msg, "You eat the frogspawn. Yuck.", size);
			break;
		case 6311:
		/* Gout tuber.  */
			strncpy(eat_msg, "You eat the tuber. Hmmm, peculiar taste! It "
					"heals some run energy. It heals some health anyway.", 
					size);
			break;
		/* Send eat message.  */
		default:
			snprintf(eat_msg, size, "You eat the %s.", food_name);
			break;
	}
}

/**
 * Gets the number of uses an item has before being exhausted.
 * @param item_id Id of the item to check.
 * @return Returns number of uses the item has.
 */
int get_item_uses(int item_id)
{
	switch (item_id)
	{
		/* Potion doses.  */
		case 2428:
		case 2446:
		case 113:
		case 3408:
		case 2430:
		case 3008:
		case 2432:
		case 2434:
		case 3032:
		case 2436:
		case 2448:
		case 2438:
		case 3016:
		case 2440:
		case 3024:
		case 2442:
		case 5943:
		case 2444:
		case 2450:
		case 5952:
			return 4;
		case 121:
		case 175:
		case 115:
		case 3410:
		case 127:
		case 3010: 
		case 133:
		case 139:
		case 3034:
		case 145:
		case 181:
		case 151:
		case 3018:
		case 157:
		case 3026:
		case 163:
		case 5945:
		case 169:
		case 189:
		case 5954:
			return 3;
		case 123:
		case 177:
		case 117:
		case 3412:
		case 129:
		case 3012:
		case 135:
		case 141:
		case 3036:
		case 147:
		case 183:
		case 153:
		case 3020:
		case 159:
		case 3028:
		case 165:
		case 5947:
		case 171:
		case 191:
		case 5956:
			return 2;
		case 125:
		case 179:
		case 119:
		case 3414:
		case 131:
		case 3014:
		case 137:
		case 143:
		case 3038:
		case 149:
		case 185:
		case 155:
		case 3022:
		case 161:
		case 3030:
		case 167:
		case 5949:
		case 173:
		case 193:
		case 5958:
			return 1;

		default:
			return 0;
	}
}
