/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
/* FLETCHING - ID 9.  */

/* Note: the required level for stringing and making a bow is the same.  */
/* Bow creation id + 2 = stringing bow id.  */
int get_fletching_level_needed(int fletching_id)
{
	switch (fletching_id)
	{
	/* Arrow stuff.  */
		case FLETCHING_ADDING_FEATHERS:
			return 0;
		case FLETCHING_BRONZE_ARROWS:
			return 0;
		case FLETCHING_IRON_ARROWS:
			return 15;
		case FLETCHING_STEEL_ARROWS:
			return 30;
		case FLETCHING_MITHRIL_ARROWS:
			return 45;
		case FLETCHING_ADAMANT_ARROWS:
			return 60;
		case FLETCHING_RUNE_ARROWS:
			return 75;
		case FLETCHING_WOLFBONE_ARROWS:
			return 5;
	/* Bolt stuff.  */
		case FLETCHING_OPAL_BOLTS:
			return 17;
		case FLETCHING_PEARL_BOLTS:
			return 33;
		case FLETCHING_BARBED_BOLTS:
			return 51;
	/* Dart stuff.  */
		case FLETCHING_BRONZE_DARTS:
			return 0;
		case FLETCHING_IRON_DARTS:
			return 22;
		case FLETCHING_STEEL_DARTS:
			return 37;
		case FLETCHING_MITHRIL_DARTS:
			return 52;
		case FLETCHING_ADAMANT_DARTS:
			return 67;
		case FLETCHING_RUNE_DARTS:
			return 81;
	/* Normal wood logs.  */
		case FLETCHING_ARROW_SHAFTS:
			return 0;
		case FLETCHING_CARVE_SBOW:
		case FLETCHING_STRING_SBOW:
			return 5;
		case FLETCHING_CARVE_BOW:
		case FLETCHING_STRING_BOW:
			return 10;
	/* Oak logs.  */
		case FLETCHING_CARVE_OAK_SBOW:
		case FLETCHING_STRING_OAK_SBOW:
			return 20;
		case FLETCHING_CARVE_OAK_BOW:
		case FLETCHING_STRING_OAK_BOW:
			return 25;
	/* Willow logs.  */
		case FLETCHING_CARVE_WILLOW_SBOW:
		case FLETCHING_STRING_WILLOW_SBOW:
			return 35;
		case FLETCHING_CARVE_WILLOW_BOW:
		case FLETCHING_STRING_WILLOW_BOW:
			return 40;
	/* Maple logs.  */
		case FLETCHING_CARVE_MAPLE_SBOW:
		case FLETCHING_STRING_MAPLE_SBOW:
			return 50;
		case FLETCHING_CARVE_MAPLE_BOW:
		case FLETCHING_STRING_MAPLE_BOW:
			return 55;
	/* Yew logs.  */
		case FLETCHING_CARVE_YEW_SBOW:
		case FLETCHING_STRING_YEW_SBOW:
			return 65;
		case FLETCHING_CARVE_YEW_BOW:
		case FLETCHING_STRING_YEW_BOW:
			return 70;
	/* Magic logs.  */
		case FLETCHING_CARVE_MAGIC_SBOW:
		case FLETCHING_STRING_MAGIC_SBOW:
			return 80;
		case FLETCHING_CARVE_MAGIC_BOW:
		case FLETCHING_STRING_MAGIC_BOW:
			return 85;
	/* Achey logs.  */
		case FLETCHING_OGRE_ARROW_SHAFTS:
			return 5;
		case FLETCHING_CARVE_COMP_BOW:
			return 30;

		default:
			printf("ERROR! UNKNOWN ID: %d!\n", fletching_id);
			return -1;
	}
}

/* Note: the required level for stringing and making a bow is the same.  */
/* Bow creation id + 2 = stringing bow id.  */
float get_fletching_xp(int fletching_id)
{
	switch (fletching_id)
	{
		case FLETCHING_ADDING_FEATHERS:
			return 1.0;
		case FLETCHING_BRONZE_ARROWS:
			return 1.3;
		case FLETCHING_IRON_ARROWS:
			return 2.5;
		case FLETCHING_STEEL_ARROWS:
			return 5.0;
		case FLETCHING_MITHRIL_ARROWS:
			return 7.5;
		case FLETCHING_ADAMANT_ARROWS:
			return 10.0;
		case FLETCHING_RUNE_ARROWS:
			return 12.5;
		case FLETCHING_WOLFBONE_ARROWS:
			return 0.9;
	/* Bolt stuff.  */
		case FLETCHING_OPAL_BOLTS:
			return 1.6;
		case FLETCHING_PEARL_BOLTS:
			return 3.2;
		case FLETCHING_BARBED_BOLTS:
			return 9.5;
	/* Dart stuff.  */
		case FLETCHING_BRONZE_DARTS:
			return 1.8;
		case FLETCHING_IRON_DARTS:
			return 3.8;
		case FLETCHING_STEEL_DARTS:
			return 7.5;
		case FLETCHING_MITHRIL_DARTS:
			return 11.2;
		case FLETCHING_ADAMANT_DARTS:
			return 15.0;
		case FLETCHING_RUNE_DARTS:
			return 18.8;
	/* Normal wood logs.  */
		case FLETCHING_ARROW_SHAFTS:
			return 5.0;
		case FLETCHING_CARVE_SBOW:
		case FLETCHING_STRING_SBOW:
			return 5.0;
		case FLETCHING_CARVE_BOW:
		case FLETCHING_STRING_BOW:
			return 10.0;
	/* Oak logs.  */
		case FLETCHING_CARVE_OAK_SBOW:
		case FLETCHING_STRING_OAK_SBOW:
			return 16.5;
		case FLETCHING_CARVE_OAK_BOW:
		case FLETCHING_STRING_OAK_BOW:
			return 25.0;
	/* Willow logs.  */
		case FLETCHING_CARVE_WILLOW_SBOW:
		case FLETCHING_STRING_WILLOW_SBOW:
			return 33.3;
		case FLETCHING_CARVE_WILLOW_BOW:
		case FLETCHING_STRING_WILLOW_BOW:
			return 41.5;
	/* Maple logs.  */
		case FLETCHING_CARVE_MAPLE_SBOW:
		case FLETCHING_STRING_MAPLE_SBOW:
			return 50.0;
		case FLETCHING_CARVE_MAPLE_BOW:
		case FLETCHING_STRING_MAPLE_BOW:
			return 58.3;
	/* Yew logs.  */
		case FLETCHING_CARVE_YEW_SBOW:
		case FLETCHING_STRING_YEW_SBOW:
			return 67.5;
		case FLETCHING_CARVE_YEW_BOW:
		case FLETCHING_STRING_YEW_BOW:
			return 75.0;
	/* Magic logs.  */
		case FLETCHING_CARVE_MAGIC_SBOW:
		case FLETCHING_STRING_MAGIC_SBOW:
			return 83.3;
		case FLETCHING_CARVE_MAGIC_BOW:
		case FLETCHING_STRING_MAGIC_BOW:
			return 91.5;
	/* Achey logs.  */
		case FLETCHING_OGRE_ARROW_SHAFTS:
			return 1.6;
		case FLETCHING_CARVE_COMP_BOW:
			return 45.0;

		default:
			printf("ERROR! UNKNOWN ID: %d!\n", fletching_id);
			return -1;
	}
}

/**
 * Handles cutting a bow out of some logs.
 * @param *c Client instance.
 * @param log_id Item ID of the logs used.
 * @param bow_id Item ID of the bow to make.
 * @param xp XP gained from the action.
 * @param is_long If this bow is a longbow or not.
 */
void cut_bow(struct client *c, int log_id, int bow_id, float xp, int is_long)
{
	int item_1_slot = player_has_item(c, log_id, 1);
	
	if (item_1_slot != -1)
	{
		delete_item_from_inv_slot(c, item_1_slot, 1);
		add_item_to_inv(c, bow_id, 1, -1);
		add_skill_xp(c, FLETCHING_SKILL, xp); 
		player_play_animation(c, 1248);

		if (is_long == 0)
		{
			packet_send_chatbox_message(c, "You carefully cut the wood into a shortbow.");
		}
		else
		{
			packet_send_chatbox_message(c, "You carefully cut the wood into a longbow.");
		}

		c->plr->action_ticks = 4;
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Handle adding the bowstring to an unstrung bow.
 * @param *c Client instance.
 * @param ubow_id ID of the unstrung bow.
 * @param bow_id ID of the bow being made.
 * @param fletching_id ID representing the fletching task.
 */
void string_bow(struct client *c, int ubow_id, int bow_id, int fletching_id)
{
	int num_made = 0;
	int fletching_level = get_player_skill_level(c, FLETCHING_SKILL);
	int required_level = get_fletching_level_needed(fletching_id);

	/* Check if level requirements are met. If not, exit.  */
	if (fletching_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "fletching to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	/* 1777 - bow string.  */
	num_made = combine_items(c, ubow_id, 1, 1777, 1, bow_id, 1);

	if (num_made != -1)
	{
		add_skill_xp(c, FLETCHING_SKILL, get_fletching_xp(fletching_id)); 
		packet_send_chatbox_message(c, "You add a string to the bow.");
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Handle adding tips to unfinished bolts.
 * @param *c Client instance.
 * @param tips_id ID of the bolt tips used.
 * @param fbolts_id ID of the finished bolts to be made.
 * @param fletching_id ID representing the fletching task.
 */
void add_tips_to_bolts(struct client *c, int tips_id, int fbolts_id, 
					   int fletching_id)
{
	int num_made = 0;
	int fletching_level = get_player_skill_level(c, FLETCHING_SKILL);
	int required_level = get_fletching_level_needed(fletching_id);

	/* Check if level requirements are met. If not, exit.  */
	if (fletching_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "fletching to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	num_made = combine_items_permissive(c, tips_id, 877, fbolts_id, 10);

	if (num_made != -1)
	{
		add_skill_xp(c, FLETCHING_SKILL, 
					 get_fletching_xp(fletching_id) * num_made); 
		packet_send_chatbox_message(c, "You add tips to the bolts.");
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Ensure level requirements are met for fletching task, setup variables.
 * @param *c Client instance.
 */
void init_fletching(struct client *c)
{
	int required_level = get_fletching_level_needed(c->plr->action_id_type);
	int fletching_level = get_player_skill_level(c, FLETCHING_SKILL);

	/* Set action related ids.  */
	c->plr->action_skill_id = FLETCHING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_FLETCHING);
	
	if (fletching_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "fletching to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	/* FIXME: In order to make darts, "The Tourist Trap" quest must have been
	   completed. Implement this for when quests are implemented.  */

	/* NOTE: Darts, bolts are made in batches of 10, arrows in 15.  */
	printf("Fletching setup. No errors.\n");
}

/**
 * Handle creation of the fletching item.
 * @param *c Client instance.
 */
void handle_fletching(struct client *c)
{
	char msg[150];
	int num_made = 0;
	int item_1_slot = 0;

	/* Check if there are actions left to do, if not, end skilling.  */
	if (c->plr->action_skill_remaining_actions == 0)
	{
		set_player_state(c, STATE_IDLE);
	}

	switch (c->plr->action_id_type)
	{
		case FLETCHING_ADDING_FEATHERS:
			num_made = combine_items_permissive(c, 52, 314, 53, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, (float) num_made); 
				snprintf(msg, 150, "You attach feathers to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_BRONZE_ARROWS:
			num_made = combine_items_permissive(c, 53, 39, 882, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_IRON_ARROWS:
			num_made = combine_items_permissive(c, 53, 40, 884, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_STEEL_ARROWS:
			num_made = combine_items_permissive(c, 53, 41, 886, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_MITHRIL_ARROWS:
			num_made = combine_items_permissive(c, 53, 42, 888, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_ADAMANT_ARROWS:
			num_made = combine_items_permissive(c, 53, 43, 890, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_RUNE_ARROWS:
			num_made = combine_items_permissive(c, 53, 44, 892, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_WOLFBONE_ARROWS:
			num_made = combine_items_permissive(c, 2865, 2861, 2866, 15);
			
			if (num_made != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 (get_fletching_xp(c->plr->action_id_type) 
							  * num_made)); 
				snprintf(msg, 150, "You attach arrow heads to %d arrow shafts.", 
						 num_made);
				packet_send_chatbox_message(c, msg);
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
	/* Normal logs.  */
		/* Making arrow shafts.  */
		case FLETCHING_ARROW_SHAFTS:
			item_1_slot = player_has_item(c, 1511, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 52, 15, -1);
				add_skill_xp(c, FLETCHING_SKILL, 
							 get_fletching_xp(c->plr->action_id_type)); 
				player_play_animation(c, 1248);
				packet_send_chatbox_message(c, "You cut the log into 15 arrow shafts.");
				c->plr->action_ticks = 4;
				/* FIXME: Add delay not allowing other actions for this
				 * duration.  */
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		case FLETCHING_CARVE_SBOW:
			cut_bow(c, 1511, 50, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_BOW:
			cut_bow(c, 1511, 48, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Oak logs.  */
		case FLETCHING_CARVE_OAK_SBOW:
			cut_bow(c, 1521, 54, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_OAK_BOW:
			cut_bow(c, 1521, 56, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Willow logs.  */
		case FLETCHING_CARVE_WILLOW_SBOW:
			cut_bow(c, 1519, 60, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_WILLOW_BOW:
			cut_bow(c, 1519, 58, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Maple logs.  */
		case FLETCHING_CARVE_MAPLE_SBOW:
			cut_bow(c, 1517, 64, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_MAPLE_BOW:
			cut_bow(c, 1517, 62, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Yew logs.  */
		case FLETCHING_CARVE_YEW_SBOW:
			cut_bow(c, 1515, 68, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_YEW_BOW:
			cut_bow(c, 1515, 66, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Magic logs.  */
		case FLETCHING_CARVE_MAGIC_SBOW:
			cut_bow(c, 1513, 72, get_fletching_xp(c->plr->action_id_type), 0);
			break;
		case FLETCHING_CARVE_MAGIC_BOW:
			cut_bow(c, 1513, 70, get_fletching_xp(c->plr->action_id_type), 1);
			break;
	/* Achey logs.  */
	/* TODO: Check for quest completion before making when quests are 
	   implemented.  */
		/* Making ogre arrow shafts.  */
		case FLETCHING_OGRE_ARROW_SHAFTS:
			item_1_slot = player_has_item(c, 2862, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 2864, 15, -1);
				add_skill_xp(c, FLETCHING_SKILL, 
							 get_fletching_xp(c->plr->action_id_type)); 
				player_play_animation(c, 1248);
				packet_send_chatbox_message(c, "You cut the log into 15 ogre arrow shafts.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Ogre comp bow.  */
		case FLETCHING_CARVE_COMP_BOW:
			if (combine_items(c, 2862, 1, 2859, 1, 4825, 1) != -1)
			{
				add_skill_xp(c, FLETCHING_SKILL, 
							 get_fletching_xp(c->plr->action_id_type)); 
				player_play_animation(c, 1248);
				packet_send_chatbox_message(c, "You carefully cut the wood into a ogre comp bow.");
				c->plr->action_ticks = 4;
			}
	}

	player_replay_animation(c);
	c->plr->action_skill_remaining_actions--;
}
