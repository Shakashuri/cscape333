#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "audio.h"
#include "client.h"
#include "packet.h"
#include "player.h"
#include "regions.h"
#include "stream.h"

/**
 * Return what music a specific region is set to.
 * @param region_id ID of the region to get the music from.
 * @return Returns -1 on error, anything else is the music id.
 */
int get_region_music_id(int region_id)
{
	int x;

	for (x = 0; x < REGION_LIST_NUM; x++)
	{
		if (REGION_LIST[x]->id == region_id)
		{
			return REGION_LIST[x]->music_id;
		}
	}

	return -1;
}

/**
 * Return the index in the MUSIC_DATA array that a specific song is located at.
 * @param music_id ID of the song to search for in the array.
 * @return Returns -1 if not found, anything else is the song's index in the
 * MUSIC_DATA array.
 */
int get_music_index(int music_id)
{
	int x;

	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		if (MUSIC_DATA[x] != NULL)
		{
			if (MUSIC_DATA[x]->id == music_id)
			{
				return x;
			}
		}
	}

	return -1;
}

/**
 * Returns the name of a song from its ID.
 * @param music_id ID of the song to get the name for.
 * @return Returns the name if the song has one/is found, otherwise, returns
 * the string "null".
 */
char *get_music_name(int music_id)
{
	int x = get_music_index(music_id);

	if (x != -1)
	{
		return MUSIC_DATA[x]->name;
	}

	return "null";
}

/**
 * Goes through all the music that the player is set to have unlocked, and
 * updates the client's music playlist to show the unlocked songs as
 * green.
 * @param *c Client instance.
 */
void update_unlocked_music_list(struct client *c)
{
	int music_size = 10;
	int empty_slot = -1;
	int set_bit = 0;
	int config_ids[10] = {0};
	unsigned int values[10] = {0};
	int x = 0, y = 0;

	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		if (c->plr->unlocked_music[x] == 1)
		{
			set_bit = 0;
			empty_slot = -1;

			if (MUSIC_DATA[x]->config_id == -1)
			{
				if (MUSIC_DATA[x]->unlocked_by_default == 0)
				{
					printf("Invalid config id for song %s!\n", 
						   MUSIC_DATA[x]->name);
				}
				continue;
			}

			for (y = 0; y < music_size; y++)
			{
				if (config_ids[y] == MUSIC_DATA[x]->config_id)
				{
					values[y] ^= MUSIC_DATA[x]->bit;
					set_bit = 1;
					break;
				}
				else if (config_ids[y] == 0 && empty_slot == -1)
				{
					empty_slot = y;	
				}
			}

			/* If this config id wasn't found yet, add it to the list and
			 * apply the bits.  */
			if (set_bit == 0 && empty_slot != -1)
			{
				config_ids[empty_slot] = MUSIC_DATA[x]->config_id;
				values[empty_slot] ^= MUSIC_DATA[x]->bit;
			}
		}
	}

	for (x = 0; x < music_size; x++)
	{
			if (config_ids[x] != 0)
			{
				packet_set_client_config_value_int(c, config_ids[x], 
												   values[x]);
			}
	}
}

/**
 * Handles when a player clicks the name of a song in their jukebox interface.
 * Checks if they have the song unlocked, if so, start playing it. If they
 * don't have the song unlocked yet, tell them.
 * @param *c Client instance.
 * @param interface_id ID of the song name that was clicked.
 */
void handle_clicked_song(struct client *c, int interface_id)
{
	int music_idx = -1;
	int x = 0;

	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		if (MUSIC_DATA[x]->interface_id == interface_id)
		{
			music_idx = x;
			break;
		}
	}

	if (music_idx == -1)
	{
		printf("Client cheating, incorrect music!\n");
		return;
	}

	if (c->plr->unlocked_music[music_idx] == 0
		&& MUSIC_DATA[music_idx]->unlocked_by_default == 0)
	{
		packet_send_chatbox_message(c, 
				"You have not unlocked this piece of music yet!");
	}
	else
	{
		packet_play_song(c, MUSIC_DATA[music_idx]->id);
	}
}

int get_npc_attack_sound(int npc_id)
{
	switch (npc_id)
	{
		default:
			return 417;
	}
}

int get_npc_block_sound(int npc_id)
{
	switch(npc_id)
	{
		default:
			return 791;
	}
}

int get_npc_death_sound(int npc_id)
{
	switch(npc_id)
	{
		default:
			return 70;
	}
}

int get_player_attack_sounds(struct client *c)
{
	switch (c->plr->damage_type)
	{
		case DMG_TYPE_SLASH:
			return 396;
		case DMG_TYPE_STAB:
			return 398;
		case DMG_TYPE_CRUSH:
			return 1069;
		case DMG_TYPE_RANGE:
			return 370;

		default:
			return 396;
	}
}
