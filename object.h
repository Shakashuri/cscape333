/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OBJECTS_H_
#define _OBJECTS_H_

/**
 * @brief Holds the object definition data for each object.
 */
struct object
{
	/** Object id.  */
	int id;
	/** Object name.  */
	char name[100];
	/** Object examine description.  */
	char desc[100];
	/** Object x size in squares.  */
	int size_x;
	/** Object y size in squares.  */
	int size_y;
	/* Corresponds with the collision flags.  */
	/** Blocks arrows/magic shot over.  */
	int occupied;
	/** Cannot be walked on.  */
	int blocked;
	
	/* Special data on the object, used for doors only at the moment.  */
	/* FIXME: If more special data is needed for other object operation,
	   look into renaming these into something more generic.  */
	/** Types of doors:
	 * 0 - Normal, right hinged door.
	 * 1 - Left hinged door.
	 * 2 - Fence, free part
	 * 3 - Fence, hinged part
	 * 4 - Auto gates, right hinged
	 * 5 - Auto gates, left hinged
	 * 6 - Non-moving door (curtains and such).
	 */
	int door_type;
	/** Is this part of a double door setup?  */
	int is_double_door;
	/** Is the door open at the moment?  */
	int is_open;
	/** Does the object id change into another when opening/closing?
	 * This is the difference added to the original id.  */
	int id_diff;
	/** Has some sort of action that can be preformed with it.  */
	int actionable;

	/** Stores the index for each table the object uses for quick access. Can
	 * just punch this into LOOT_TABLES[] and use the data.  */
	int loot_table_indexes[MAX_OBJECTS_LOOT_TABLES];
	/** Stores the name for each loot table the object has.  */
	char loot_table_names[MAX_OBJECTS_LOOT_TABLES][100];
};

/**
 * @brief Data for each spawned object instance.
 */
struct obj_spawn
{
	/** Object id.  */
	int id;
	/** Object spawn's original id, used if the object transforms.  */
	int spawn_id;

	/**@{*/
	/** Generic counters in case some sort of data needs to be tracked.  */
	int internal_counter;
	int internal_counter2;
	/**@}*/

	/**
	 * Id for how the client needs to processes this object:
	 * 	- 0 - 3: Wall object
	 * 	- 4 - 8: Wall decoration
	 * 	- 9 - 21: Interactable object (default)
	 * 	- 22: Ground decoration.  */
	int type;
	/**@{*/
	/** Object's location in the world.  */
	int world_x;
	int world_y;
	int world_z;
	/**@}*/
	
	/**@{*/
	/** Where the object spawns/respawns.  */
	int spawn_x;
	int spawn_y;
	/**@}*/
	
	/**
	 * If set to 1, show. Otherwise, it is just there for server purposes.
	 * 
	 * @todo May not be used, investigate.
	 */
	int should_spawn;

	/**
	 * What direction the object is facing.
	 * @note Face is generally used, but internal_face is used and set in cases
	 * where an object is spawned using only two faces (ex: funeral pyres)
	 * but what the face 'should' be needs to be stored for other purposes.
	 */
	int face;
	/** If the visible facing direction needs to differ for server purposes,
	 * what face should the object be treated as.  */
	int internal_face;
	/** What direction the object faces when spawned/respawned.  */
	int spawn_face;

	/** @todo Might not actually be used, check back later.  */
	int timer;
	/** Ticks until object turns back into original id.  */
	int transform_ticks;
	
	/** Ticks until object is removed from game.  */
	int despawn_ticks;
	
	/** If object should be despawned from player's screens, or just have their
	 * object_spawn deleted.
	 */
	int despawn_globally;

	/** Ticks until object is spawned into the game.  */
	int spawn_ticks;

	/** Ticks until an object can be used again.  */
	int cooldown_ticks;
	/** Id offset of the objects's pair, if any. Used for double doors.  */
	int double_door;
	/** For doors, is this in its open state.  */
	int open;
	
	/** If the object is paired with another, different from double doors.  */
	int paired_object;
	/** Ticks to wait until door is closed. Used for doors that the player
	 * passes through automatically.
	 */
	int close_delay;
	/** If the door is open or closed by default.  */
	int default_open_state;
};

/**
 * @brief Object location data in the world, original locations.
 * This is for when objects are loaded into the database from where they are
 * in the default world state.
 */
struct obj_spawn_org
{
	/** Object id.  */
	int id;
	/** Object type.  */
	short int type;
	/** Object x coordinate relative to its region.  */
	short int in_reg_x;
	/** Object y coordinate relative to its region.  */
	short int in_reg_y;
	/** World x coordinate.  */
	short int world_x;
	/** World y coordinate.  */
	short int world_y;
	/** World z coordinate.  */
	short int world_z;

	/** Which way the object is facing.  */
	short int face;
};

int get_inst_object_index_at_position(struct client *c, int pos_x, int pos_y, int pos_z);
int create_inst_object(struct client *c, int obj_id, int obj_type, 
					   int pos_x, int pos_y, int pos_z, int face, 
					   int despawn_ticks);
void despawn_inst_object_on_ground(struct client *c, int obj_index);
void spawn_inst_object_on_ground(struct client *c, int obj_index);
void handle_transforming_inst_object(struct client *c, int obj_index, 
									 int new_id, int duration);
void remove_inst_object(struct client *c, int obj_index, int clear_on_client);
void apply_obj_spawn_defaults(struct obj_spawn *obj);

void spawn_object_on_ground(struct client *c, int obj_index);
void despawn_object_on_ground(struct client *c, int obj_index);
void reset_player_objects(struct client *c);
void spawn_object_globally(int obj_index);
void spawn_default_object_globally(int world_x, int world_y, int world_z);
void despawn_object_globally(int obj_index);
void reset_object_globally(int obj_index);
void reset_object_globally_spawn(int obj_index);
int add_to_object_list(int obj_id, int obj_type, int pos_x, int pos_y, 
					   int pos_z, int face, int despawn_ticks, 
					   int despawn_globally);
void delete_from_object_list(int obj_index, int remove);
int get_object_spawn_index(int obj_x, int obj_y, int obj_z);
int get_region_object_index(int reg_index, int world_x, int world_y, 
							   int world_z, int id);
int get_region_object_face(int reg_index, int world_x, int world_y, 
							  int world_z, int id);
int get_region_object_type(int reg_index, int world_x, int world_y, 
							  int world_z, int id);
int get_region_object_data(int reg_index, int world_x, int world_y, 
							  int world_z, int *id, int *obj_face, 
							  int *obj_type);
void set_object_transforms(int obj_index, int new_id, int duration);
void set_object_spawning_time(int obj_index, int ticks);

int find_object_at_position(struct client *c, int pos_x, int pos_y, int pos_z, 
		int *instance_type, int *obj_face, int *obj_type);
#endif
