/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PATHFINDING_H_
#define _PATHFINDING_H_

int get_closest_perpendicular_square_around_entity(int entity_world_x, 
		int entity_world_y, int size_x, int size_y, int *result_x, 
		int *result_y, int height);
void get_best_object_usage_square(int entity_world_x, 
		int entity_world_y, int size_x, int size_y, int *result_x, 
		int *result_y, int height);
int is_valid_path_to_point(int start_x, int start_y, int end_x, int end_y, 
		int height);

int get_num_walking_spaces_to_point(int start_x, int start_y, int end_x, 
		int end_y, int height);
int player_move_to_location(struct client *c, int start_x, 
		int start_y, int end_x, int end_y, int height);
int player_move_is_within_combat_range(struct client *c, int start_x, int start_y,
									int end_x, int end_y, int height, 
									int npc_size, int attack_range);
#endif
