#ifndef _QUEST_H_
#define _QUEST_H_

/** Quest ids, corresponds with the quest database file.  */
enum QUESTS
{
	BLACK_KNIGHTS_FORTRESS = 0,
	COOKS_ASSISTANT,
	DEMON_SLAYER,
	RUNE_MYSTERIES_QUEST,
	DORICS_QUEST,
	THE_RESTLESS_GHOST,
	GOBLIN_DIPLOMACY,
	ERNEST_THE_CHICKEN,
	IMP_CATCHER,
	PIRATES_TREASURE,
	PRINCE_ALI_RESCUE,
	ROMEO_AND_JULIET,
	SHEEP_SHEARER,
	SHIELD_OF_ARRAV,
	THE_KNIGHTS_SWORD,
	VAMPIRE_SLAYER,
	WITCHS_POTION,
	WATCH_TOWER,
	WATERFALL_QUEST,
	WITCHS_HOUSE,
	BIOHAZARD,
	CLOCK_TOWER,
	DIGSITE_QUEST,
	BIG_CHOMPY_BIRD_HUNTING,
	DWARF_CANNON,
	FAMILY_CREST,
	FIGHT_ARENA,
	FISHING_CONTEST,
	GERTRUDES_CAT,
	THE_GRAND_TREE,
	HAZEEL_CULT,
	HEROES_QUEST,
	IN_SEARCH_OF_THE_MYREQUE,
	JUNGLE_POTION,
	LEGENDS_QUEST,
	LOST_CITY,
	MERLINS_CRYSTAL,
	MONKS_FRIEND,
	MURDER_MYSTERY,
	OBSERVATORY_QUEST,
	PLAGUE_CITY,
	SCORPION_CATCHER,
	SEA_SLUG_QUEST,
	SHEEP_HERDER,
	SHILO_VILLAGE,
	TEMPLE_OF_IKOV,
	THE_TOURIST_TRAP,
	TREE_GNOME_VILLAGE,
	TRIBAL_TOTEM,
	UNDERGROUND_PASS,
	DRAGON_SLAYER,
	ELEMENTAL_WORKSHOP,
	DRUIDIC_RITUAL,
	PRIEST_IN_PERIL,
	NATURE_SPIRIT,
	DEATH_PLATEAU,
	TROLL_STRONGHOLD,
	EADGARS_RUSE,
	TAI_BWO_WANNAI_TRIO,
	HAUNTED_MINE,
	REGICIDE,
	SHADES_OF_MORTTON,
	THRONE_OF_MISCELLANIA,
	THE_FREMENNIK_TRIALS,
	MONKEY_MADNESS,
	ROVING_ELVES,
	HOLY_GRAIL,
	HORROR_FROM_THE_DEEP,
	TROLL_ROMANCE,
	CREATURE_OF_FENKENSTRAIN,
	ONE_SMALL_FAVOUR,
	MOUNTAIN_DAUGHTER,
	GHOSTS_AHOY,
	BETWEEN_A_ROCK,
	THE_FEUD,
	THE_GOLEM,
	DESERT_TREASURE,
	ICTHLARINS_LITTLE_HELPER,
	TEARS_OF_GUTHIX,
	ZOGRE_FLESH_EATERS,
	THE_GIANT_DWARF,
	THE_LOST_TRIBE,
	RECRUITMENT_DRIVE,
	MOURNINGS_ENDS_PART_1,
	FORGETTABLE_TALE,
	GARDEN_OF_TRANQUILLITY
};

/**
 * Stores all information needed for each quest in the game.
 */
struct quest_info
{
	/** Name of the quest.  */
	char name[200];
	/** ID number of the quest.  */
	int id;
	/** ID of the button in the client quest list that corresponds to this 
	 * quest.  */
	int interface_id;
	/** Levels needed in skills to start this quest.  */
	int skill_req[PROTOCOL_NUM_SKILLS];
	/** Number of quest points required for a player to start this quest.  */
	int quest_point_req;
	/** Number of quest points that are given as a reward for finishing this
	 * quest.  */
	int quest_point_reward;
};

int is_quest_started(struct client *c, int quest_id);
int is_quest_in_progress(struct client *c, int quest_id);
int is_quest_complete(struct client *c, int quest_id);
void update_quest_points(struct client *c);
void update_quest_log_status(struct client *c, int quest_id);
void update_quest_log(struct client *c);
void set_quest_state(struct client *c, int quest_idx, int state);
void finish_quest(struct client *c, int quest_idx);

void write_quest_log_line(struct client *c, char *text, int line);
#endif
