/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file combat_calc.c
 * @brief Holds functions for various combat calculations.
 */

#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "constants.h"

#include "client.h"
#include "combat.h"
#include "misc.h"
#include "npc.h"
#include "player.h"
#include "skills.h"
#include "skill_magic.h"

/* These functions return the player's level in a skill after taking into
 * account all the modifiers they may have to it.  */
static int get_player_attack_level(struct client *c)
{
	float base = ((float) (c->plr->level[ATTACK_SKILL] 
						   + c->plr->temp_bonuses[ATTACK_SKILL]) 
				  * (float) c->plr->prayer_attack_bonus);
	base = floorf(base);

	return (int) (base - (base * c->plr->atk_reduced));
}

static int get_player_strength_level(struct client *c)
{
	float base = ((float) (c->plr->level[STRENGTH_SKILL] 
						   + c->plr->temp_bonuses[STRENGTH_SKILL]) 
				  * (float) c->plr->prayer_strength_bonus);
	base = floorf(base);

	return (int) (base - (base * c->plr->str_reduced));
}

static int get_player_defence_level(struct client *c)
{
	float base = ((float) (c->plr->level[DEFENCE_SKILL] 
						   + c->plr->temp_bonuses[DEFENCE_SKILL]) 
				  * (float) c->plr->prayer_defence_bonus);
	base = floorf(base);

	return (int) (base - (base * c->plr->def_reduced));
}

static int get_player_ranged_level(struct client *c)
{
	return (int) ((float) (c->plr->level[RANGED_SKILL] 
						   + c->plr->temp_bonuses[RANGED_SKILL]) 
				  * c->plr->prayer_ranged_bonus);
}

static int get_player_magic_level(struct client *c)
{
	return (int) ((float) (c->plr->level[MAGIC_SKILL] 
						   + c->plr->temp_bonuses[MAGIC_SKILL]) 
				  * c->plr->prayer_magic_bonus);
}

/**
 * Uses player skill levels to calculate maximum damage hit, stores result in 
 * c->plr->max_hit.
 * @param *c Client instance.
 */
void calculate_max_hit(struct client *c) 
{
	/* Strength stats.  */
	int str_bonus = c->plr->player_bonus[STRENGTH_BONUS];
	int str_lvl = get_player_strength_level(c);
	/* Ranged stats.  */
	int ranged_bonus = c->plr->ranged_strength;
	int ranged_lvl = get_player_ranged_level(c);

	int effective_strength = 0;
	int attack_style = 0;
	float base_damage = 0.0;

	/* If using a melee fighting style, get the strength bonus from it.  */
	if (c->plr->fighting_style < 4)
	{
		switch (c->plr->fighting_style)
		{
			case AGGRESSIVE_STYLE:
				attack_style = 3;
				break;
			case CONTROLLED_STYLE:
				attack_style = 1;
				break;
			default:
				attack_style = 0;
				break;
		}

		effective_strength 
		= ((int) ((str_lvl) 
		  * c->plr->prayer_strength_bonus))
		  + attack_style + 8;
		
		base_damage 
		= (0.5 + (effective_strength * (str_bonus + 64) / 640.0));

		if (c->plr->special_attack_bonus != 0.0)
		{
			c->plr->max_hit 
			= (int) floor(base_damage * c->plr->special_attack_bonus);
		}
		else
		{
			c->plr->max_hit = (int) floor(base_damage);
		}
	}
	else
	{
		/* Check for damage bonus from ranged styles.  */
		switch (c->plr->fighting_style)
		{
			case ACCURATE_STYLE_RANGE:
				attack_style = 3;
				break;
			default:
				attack_style = 0;
				break;
		}

		effective_strength 
		= ((int) ((ranged_lvl) 
				  * c->plr->prayer_ranged_bonus))
				  + attack_style + 8;
		
		base_damage 
		= (0.5 + (effective_strength * (ranged_bonus + 64) / 640.0));

		if (c->plr->special_attack_bonus != 0.0)
		{
			c->plr->max_hit 
			= (int) floor(base_damage * c->plr->special_attack_bonus);
		}
		else
		{
			c->plr->max_hit = (int) floor(base_damage);
		}
	}
}

/**
 * @brief Uses NPC skill levels to calculate maximum damage hit, stores
 * result in the relevent npc->melee_stats[MAX_HIT] or 
 * npc->ranged_stats[MAX_HIT.
 *
 * @param *npc NPC instance.
 * @param ranged_dmg 1 if calculating ranged damage, 0 if melee.
 */
void calculate_npc_max_hit(struct npc_spawn *npc, int ranged_dmg) 
{
	double str_bonus = (double) npc->str_bonus; /* Strength Bonus */
	/* Handle strength drain effects.  */
	int str_lvl = (npc->str_skill
				  - (npc->str_skill * npc->str_reduced));
	/* Strength level */
	int ranged_bonus = npc->ranged_strength; /* Ranged Bonus */
	int ranged_lvl = npc->range_skill; /* Range level */

	int effective_strength = 0;
	int attack_style = 0;
	double base_damage = 0.0;

	if (ranged_dmg == 0)
	{
		effective_strength = ((str_lvl) + attack_style + 8);
		
		base_damage 
		= (0.5 + ((effective_strength * (str_bonus + 64)) / 640.0));

		/* Special attack bonus handling, may be needed later.  */
		/*
		if (npc->special_attack_bonus != 0.0)
		{
			npc->max_hit 
			= (int) floor(base_damage * npc->special_attack_bonus);
		}
		else
		{
		*/
		npc->melee_stats[MAX_HIT] = (int) floor(base_damage);
	}
	else
	{
		effective_strength = (double) ((ranged_lvl) + attack_style + 8);
		
		base_damage 
		= (0.5 + (effective_strength * (ranged_bonus + 64) / 640.0));

		/* Special attack bonus handling, may be needed later.  */
		/*
		if (npc->special_attack_bonus != 0.0)
		{
			npc->max_hit 
			= (int) floor(base_damage * npc->special_attack_bonus);
		}
		else
		{
		*/
		npc->ranged_stats[MAX_HIT] = (int) floor(base_damage);
	}
}

/**
 * Sets what a player's combat level is based on all their skill levels 
 * that affect that. Result stored in c->plr->combat_level. 
 * @param *c Client instance.
 */
void calculate_player_combat_level(struct client *c) 
{
	float attack = (float) get_level_for_xp(c->plr->xp[ATTACK_SKILL]);
	float defence = (float) get_level_for_xp(c->plr->xp[DEFENCE_SKILL]);
	float strength = (float) get_level_for_xp(c->plr->xp[STRENGTH_SKILL]);
	float hitpoints = (float) get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]);
	float prayer = (float) get_level_for_xp(c->plr->xp[PRAYER_SKILL]);
	float ranged = (float) get_level_for_xp(c->plr->xp[RANGED_SKILL]);
	float magic = (float) get_level_for_xp(c->plr->xp[MAGIC_SKILL]);
	
	double combat_level = (defence + hitpoints + floor(prayer / 2)) * 0.25;

	double warrior = (attack + strength) * 0.325;

	double ranger = floor((ranged / 2) + ranged) * 0.325;
	
	double mage = floor((magic / 2) + magic) * 0.325;

	c->plr->combat_level = (int) (combat_level 
									+ f_max(warrior, f_max(ranger, mage)));
}

/**
 * Calculates what a monster's combat level should be based on their stats.
 * @param *n NPC definition.
 * @returns Returns the calculated combat level based on the NPC's stats.
 */
int get_npc_combat_level(struct npc *n) 
{
	float attack = n->atk_skill;
	float strength = n->str_skill;
	float defence = n->def_skill;
	float ranged = n->range_skill;
	float magic = n->magic_skill;
	float hitpoints = n->health;
	
	double combat_level = (defence + hitpoints ) * 0.25;

	double warrior = (attack + strength) * 0.325;
	double ranger = floor((ranged / 2) + ranged) * 0.325;
	double mage = floor((magic / 2) + magic) * 0.325;

	return (int) (combat_level + f_max(warrior, f_max(ranger, mage)));
}

/**
 * @brief Calculates the attack roll that the player has when trying to attack
 * an NPC or player. 
 *
 *
 * @param *c Client instance.
 *
 * @code
 * rand_int(0, calc_player_hit_chance(CONNECTED_CLIENTS[d]));
 * @endcode
 *
 * @return Returns the float value of their upper roll for pvp hit chance.
 */
int get_player_attacking_roll(struct client *c)
{
	int effective_attack_level;
	int attack_style_bonus;
	int damage_type_bonus;
	float max_attack_roll;

	if (c->plr->fighting_style == ACCURATE_STYLE 
		|| c->plr->fighting_style == ACCURATE_STYLE_RANGE)
	{
		attack_style_bonus = 3;
	}
	else if (c->plr->fighting_style == CONTROLLED_STYLE)
	{
		attack_style_bonus = 1;
	}
	else
	{
		attack_style_bonus = 0;
	}

	if (c->plr->cast_spell_id  != 0
		|| c->plr->autocast_spell_id != 0)
	{
		effective_attack_level 
		= floor((float) get_player_magic_level(c) + 8 + attack_style_bonus);

		damage_type_bonus = c->plr->player_bonus[MAGIC_ATTACK_BONUS];
	}
	else if (c->plr->damage_type == DMG_TYPE_RANGE)
	{
		effective_attack_level 
		= floor((float) get_player_ranged_level(c) + 8 + attack_style_bonus);

		damage_type_bonus = c->plr->player_bonus[RANGE_ATTACK_BONUS];
	}
	else
	{
		effective_attack_level 
		= floor((float) get_player_attack_level(c) + 8 + attack_style_bonus);

		switch(c->plr->damage_type)
		{
			case DMG_TYPE_SLASH:
				damage_type_bonus = c->plr->player_bonus[SLASH_ATTACK_BONUS];
				break;
			case DMG_TYPE_CRUSH:
				damage_type_bonus = c->plr->player_bonus[CRUSH_ATTACK_BONUS];
				break;
			case DMG_TYPE_STAB:
				damage_type_bonus = c->plr->player_bonus[STAB_ATTACK_BONUS];
				break;
			default:
				damage_type_bonus = -1;
				printf("Invalid damage type: %d\n", c->plr->damage_type);
				break;
		}
	}
	
	max_attack_roll = (effective_attack_level * (damage_type_bonus + 64));

	/* NOTE: Stuff like kris attack bonus against scarabs would go here, before
	 * returning the attack roll.  */

	return max_attack_roll;
}

/**
 * @brief Calculates the max range that the player has when rolling to see
 * if they block an attack from another player.
 *
 * The number returned from this is the ceiling for a rand_int call. 
 * This will then get a number between those ranges, and if it is higher
 * than the enemy's hit roll, blocking succeeds.
 *
 * @param *c Client instance of the defender.
 * @param *attacker Client instance of the attacker.
 *
 * @code
 * rand_int(0, calc_player_guard_chance(CONNECTED_CLIENTS[d], 
 * 			CONNECTED_CLIENTS[c]));
 * @endcode
 *
 * @return Returns the float value of their upper roll for pvp block chance.
 */
float calculate_pvp_guard_chance(struct client *c, struct client *attacker)
{
	int effective_defence_level;
	int attack_style_bonus;
	int damage_type_bonus;
	float max_defence_roll;

	/* If using defensive or longrange attack types, add 3 to def.  */
	if (c->plr->fighting_style == DEFENSIVE_STYLE 
		|| c->plr->fighting_style == LONGRANGED_STYLE_RANGE)
	{
		attack_style_bonus = 3;
	}
	/* If using controlled attack type, add 1 to def.  */
	else if (c->plr->fighting_style == CONTROLLED_STYLE)
	{
		attack_style_bonus = 1;
	}
	else
	{
		attack_style_bonus = 0;
	}

	effective_defence_level 
	= floor((float) get_player_defence_level(c) + 8 + attack_style_bonus);

	if (attacker->plr->cast_spell_id  != 0
		|| attacker->plr->autocast_spell_id != 0)
	{
		int vis_def = (int) ((float) effective_defence_level * 0.30);
		int vis_mag = (int) ((float) get_player_magic_level(c) * 0.70);

		effective_defence_level = vis_def + vis_mag; 
		damage_type_bonus = c->plr->player_bonus[MAGIC_DEFENCE_BONUS];
	}
	else
	{
		/* Checks what attack style the enemy is using, adds 
		   defender defence bonus.  */
		switch(attacker->plr->damage_type)
		{
			case DMG_TYPE_SLASH:
				damage_type_bonus = c->plr->player_bonus[SLASH_DEFENCE_BONUS];
				break;
			case DMG_TYPE_CRUSH:
				damage_type_bonus = c->plr->player_bonus[CRUSH_DEFENCE_BONUS];
				break;
			case DMG_TYPE_STAB:
				damage_type_bonus = c->plr->player_bonus[STAB_DEFENCE_BONUS];
				break;
			case DMG_TYPE_RANGE:
				damage_type_bonus = c->plr->player_bonus[RANGE_DEFENCE_BONUS];
				break;
			default:
				damage_type_bonus = -1;
				printf("Invalid damage type: %d\n", c->plr->damage_type);
				break;
		}
	}
	
	max_defence_roll = effective_defence_level * (damage_type_bonus + 64);
	return max_defence_roll;
}

/**
 * @brief Returns the upper limit for the player's roll to defend against a 
 * npc's attack.  
 * @param *c Client instance.
 * @param *attacker npc_spawn instance of attacker.
 * @return Returns the max range of the defence roll.
 */
float calculate_pve_guard_chance(struct client *c, struct npc_spawn *attacker)
{
	int effective_defence_level;
	int attack_style_bonus;
	int damage_type_bonus;
	float max_defence_roll;

	/* If using defensive or longrange attack types, add 3 to def.  */
	if (c->plr->fighting_style == DEFENSIVE_STYLE 
		|| c->plr->fighting_style == LONGRANGED_STYLE_RANGE)
	{
		attack_style_bonus = 3;
	}
	/* If using controlled attack type, add 1 to def.  */
	else if (c->plr->fighting_style == CONTROLLED_STYLE)
	{
		attack_style_bonus = 1;
	}
	else
	{
		attack_style_bonus = 0;
	}

	effective_defence_level 
	= floor(get_player_defence_level(c) + 8 + attack_style_bonus);

	/* If npc is using magic, calc defence this way.  */
	if (attacker->damage_type == DMG_TYPE_MAGIC)
	{
		int vis_def = (int) ((float) effective_defence_level * 0.30);
		int vis_mag = (int) ((float) get_player_magic_level(c) * 0.70);

		effective_defence_level = vis_def + vis_mag; 
	}

	/* Checks what attack style the enemy is using, adds 
	   defender defence bonus.  */
	switch(attacker->damage_type)
	{
		case DMG_TYPE_SLASH:
			damage_type_bonus = c->plr->player_bonus[SLASH_DEFENCE_BONUS];
			break;
		case DMG_TYPE_CRUSH:
			damage_type_bonus = c->plr->player_bonus[CRUSH_DEFENCE_BONUS];
			break;
		case DMG_TYPE_STAB:
			damage_type_bonus = c->plr->player_bonus[STAB_DEFENCE_BONUS];
			break;
		case DMG_TYPE_RANGE:
			damage_type_bonus = c->plr->player_bonus[RANGE_DEFENCE_BONUS];
			break;
		case DMG_TYPE_MAGIC:
			damage_type_bonus = c->plr->player_bonus[MAGIC_DEFENCE_BONUS];
			break;
		default:
			printf("UNKNOWN DAMAGE TYPE FOUND!\n");
			damage_type_bonus = 0;
			break;
	}
	max_defence_roll = effective_defence_level * (damage_type_bonus + 64);
	return max_defence_roll;
}

/**
 * @brief Calculates the range for checking if a npc can hit a player.
 * @param *npc npc_spawn instance being checked for attack roll.
 * @return Returns the max range of the attack roll.
 *
 * @todo Verify accuracy for melee and ranged calculations. Magic should
 * be accurate now.
 */
float calculate_npc_hit_chance(struct npc_spawn *npc)
{
	/* Calculates the max range that the npc has when rolling to see
	   if they hit a player.  */ 
	int effective_attack_level = 0;
	int damage_type = 0;
	int damage_type_bonus = 0;
	float max_attack_roll = 0.0;

	/* Set damage type based on attack style.  */
	if (npc->attack_style == NPC_USE_MELEE)
	{
		damage_type = npc->melee_stats[5];

		effective_attack_level 
		= floor(((float) (npc->atk_skill 
						  - (npc->atk_reduced * npc->atk_bonus))) + 8.0);
	}
	else if (npc->attack_style == NPC_USE_RANGE)
	{
		damage_type = npc->ranged_stats[5];

		effective_attack_level = npc->range_skill + 8;
	}
	else
	{
		effective_attack_level = npc->magic_skill + 9;
	}

	switch (damage_type)
	{
		case DMG_TYPE_SLASH:
			damage_type_bonus = npc->npc_bonus[1];
			break;
		case DMG_TYPE_CRUSH:
			damage_type_bonus = npc->npc_bonus[2];
			break;
		case DMG_TYPE_STAB:
			damage_type_bonus = npc->npc_bonus[0];
			break;
		case DMG_TYPE_RANGE:
			damage_type_bonus = npc->npc_bonus[4];
			break;
		case DMG_TYPE_MAGIC:
			damage_type_bonus = npc->npc_bonus[3];
			break;
		default:
			printf("INVALID DAMAGE TYPE FOR NPC: %d\n", damage_type);
			damage_type_bonus = 0;
			break;
	}

	max_attack_roll = (effective_attack_level * (damage_type_bonus + 64));

	return max_attack_roll;
}

/**
 * @brief Calculates upper limit for npc defence rolls.
 * @param *npc npc_spawn doing the blocking
 * @param *damage_type Type of damage they're blocking against
 * 
 * @return Returns the max range of the defence roll.
 */
float calculate_npc_guard_chance(struct npc_spawn *npc, int damage_type)
{
	int effective_defence_level;
	int damage_type_bonus;
	float max_defence_roll;

	effective_defence_level 
	= floor((npc->def_skill - (npc->def_skill * npc->def_reduced)) + 8);

	/* If player is using magic, calc defence this way.  */
	if (damage_type == DMG_TYPE_MAGIC)
	{
		/*
		int vis_def = (int) ((float) effective_defence_level * 0.30);
		int vis_mag = (int) ((float) npc->magic_skill * 0.70);
		*/

		/* Recently learned that NPC magic defence is based ONLY on their
		 * magic level, 
		 * see: https://twitter.com/JagexAsh/status/649260515525566464
		 */
		effective_defence_level = npc->magic_skill; 
	}

	/* Checks what attack style the enemy is using, adds 
	   defender defence bonus.  */
	switch(damage_type)
	{
		case DMG_TYPE_SLASH:
			damage_type_bonus = npc->npc_bonus[6];
			break;
		case DMG_TYPE_CRUSH:
			damage_type_bonus = npc->npc_bonus[7];
			break;
		case DMG_TYPE_STAB:
			damage_type_bonus = npc->npc_bonus[5];
			break;
		case DMG_TYPE_RANGE:
			damage_type_bonus = npc->npc_bonus[9];
			break;
		case DMG_TYPE_MAGIC:
			damage_type_bonus = npc->npc_bonus[8];
			break;
		default:
			printf("UNKNOWN DAMAGE TYPE FOUND!\n");
			damage_type_bonus = 0;
			break;
	}

	max_defence_roll = effective_defence_level * (damage_type_bonus + 64);
	return max_defence_roll;
}

/**
 * @brief Applies a spell's effect to an npc, reduces their stats.
 * @param *npc npc_spawn affected by the spell.
 * @param spell_index What spell was used on them.
 */
void apply_spell_effects_to_npc(struct npc_spawn *npc, int spell_index)
{
	switch (SPELLS[spell_index]->spell_effect)
	{
		case EFFECT_REDUCE_ATK:
			npc->atk_reduced = SPELLS[spell_index]->spell_effect_mag;
			npc->atk_reduced_ticks = SPELLS[spell_index]->duration;
			break;
		case EFFECT_REDUCE_STR:
			npc->str_reduced = SPELLS[spell_index]->spell_effect_mag;
			npc->str_reduced_ticks = SPELLS[spell_index]->duration;
			break;
		case EFFECT_REDUCE_DEF:
			npc->def_reduced = SPELLS[spell_index]->spell_effect_mag;
			npc->def_reduced_ticks = SPELLS[spell_index]->duration;
			break;
		case EFFECT_FREEZE:
			npc->frozen_ticks = SPELLS[spell_index]->duration;
			break;
	}

	if (SPELLS[spell_index]->poison_dmg != 0)
	{
		poison_npc(npc, SPELLS[spell_index]->poison_dmg);
	}
}

/**
 * @brief Calculates how long it takes for the hitsplat to appear on a target
 * when attacked with ranged, based on distance.
 * @param distance How many squares away the target is.
 * @return Delay in server ticks.
 */
int get_ranged_hit_delay(int distance)
{
	return (2 + floor((3 + distance) / 6));
}

/**
 * @brief Calculates how long it takes for the hitsplat to appear on a target
 * when attacked with magic, based on distance.
 * @param distance How many squares away the target is.
 * @return Delay in server ticks.
 */
int get_magic_hit_delay(int distance)
{
	return (2 + floor((1 + distance) / 3));
}

/**
 * @brief Takes the coords from the target and the attacker and checks if 
 * they are diagonal or perpendicular to each other. 
 *
 * @param s_x Starting x coord.
 * @param s_y Starting y coord.
 * @param e_x Target x coord.
 * @param e_y Target y coord.
 *
 * @return Returns 1 if target is diagonal, 0 if perpendicular.
 */
int is_diagonal_from_target(int s_x, int s_y, int e_x, int e_y)
{
	if ((s_y == e_y) || (s_x == e_x))
	{
		return 0;
	}
	
	return 1;
}
