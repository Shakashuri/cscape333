/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file quests.c
 * @brief Functions relating to handling quests in the game.
 */
#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "item.h"
#include "packet.h"
#include "player.h"
#include "quests.h"
#include "skills.h"

int is_quest_started(struct client *c, int quest_id)
{
	if (quest_id < 0 || quest_id > PROTOCOL_NUM_QUESTS)
	{
		printf("Invalid quest id! %d\n", quest_id);
		return -1;
	}

	if (c->plr->quests[quest_id] > 0)
	{
		return 1;
	}

	return 0;
}

int is_quest_in_progress(struct client *c, int quest_id)
{
	if (quest_id < 0 || quest_id > PROTOCOL_NUM_QUESTS)
	{
		printf("Invalid quest id! %d\n", quest_id);
		return -1;
	}

	if (c->plr->quests[quest_id] > 0 && c->plr->quests[quest_id] < 255)
	{
		return 1;
	}

	return 0;
}

int is_quest_complete(struct client *c, int quest_id)
{
	if (quest_id < 0 || quest_id > PROTOCOL_NUM_QUESTS)
	{
		printf("Invalid quest id! %d\n", quest_id);
		return -1;
	}

	if (c->plr->quests[quest_id] == 255)
	{
		return 1;
	}

	return 0;
}

int get_quest_index_from_id(int id)
{
	for (int x = 0; x < PROTOCOL_NUM_QUESTS; x++)
	{
		if (id == QUEST_DATA[x]->id)
		{
			return x;
		}
	}

	printf("Quest with id: %d not found!\n", id);
	return -1;
}

int get_quest_index_from_intr_id(int intr_id)
{
	for (int x = 0; x < PROTOCOL_NUM_QUESTS; x++)
	{
		if (intr_id == QUEST_DATA[x]->interface_id)
		{
			return x;
		}
	}

	printf("Quest with intr id: %d not found!\n", intr_id);
	return -1;
}

void update_quest_points(struct client *c)
{
	char temp[200];

	snprintf(temp, 100, "Quest Points: %d", c->plr->quest_points);
	packet_set_interface_text(c, temp, 640);
	
	snprintf(temp, 100, "QP: %d", c->plr->quest_points);
	packet_set_interface_text(c, temp, 3985);
}

void update_quest_log_status(struct client *c, int quest_id)
{
	char temp[400];

	if (is_quest_started(c, quest_id) == 0)
	{
		snprintf(temp, 400, "@red@%s", QUEST_DATA[quest_id]->name);
	}
	else if (is_quest_in_progress(c, quest_id) == 1)
	{
		snprintf(temp, 400, "@yel@%s", QUEST_DATA[quest_id]->name);
	}
	else if (is_quest_complete(c, quest_id) == 1)
	{
		snprintf(temp, 400, "@gre@%s", QUEST_DATA[quest_id]->name);
	}

	packet_set_interface_text(c, temp, QUEST_DATA[quest_id]->interface_id);
}

void update_quest_log(struct client *c)
{
	int i = 0;

	for (i = 0; i < PROTOCOL_NUM_QUESTS; i++)
	{
		update_quest_log_status(c, i);
	}
}

/**
 * Returns the interface id number for the given line in the quest log.
 * @param line Line number in the quest log to write to.
 * @return Returns the interface text ID number that corresponds to the passed
 * line number.
 */
int get_quest_line_id(int line)
{
	int base = 8145;

	if (line == 0)
	{
		return base; 
	}
	else if (line >= 1 && line < 50)
	{
		return base + 1 + line;
	}
	else
	{
		base = 12174;

		return base + (line - 50);
	}
}

void write_quest_log_line(struct client *c, char *text, int line)
{
	packet_set_interface_text(c, text, get_quest_line_id(line));

	return;
}

void send_quest_log(struct client *c, int intr_id)
{
	int x = 0, quest_idx = -1, quest_id = -1;
	char temp[300];
	int temp_one = 0;

	quest_idx = get_quest_index_from_intr_id(intr_id);
	if (quest_idx != -1)
	{
		quest_id = QUEST_DATA[quest_idx]->id;
	}
	else
	{
		return;
	}

	printf("quest id: %d idx: %d\n", quest_id, quest_idx);
	snprintf(temp, 300, "@dre@%s", QUEST_DATA[quest_idx]->name);
	packet_set_interface_text(c, temp, 8144);

	/* Clear quest journal text.  */
	for (x = 0; x < 100; x++)
	{
		write_quest_log_line(c, "\0", x);
	}
	
	/* Send the correct log for the type of quest.  */
	switch (quest_idx)
	{
		case SHEEP_SHEARER:
			switch (c->plr->quests[quest_idx])
			{
				case 0:
					write_quest_log_line(c, "@dbl@I can start this quest by "
						"speaking to @dre@Farmer Fred@dbl@ at his", 0);
					write_quest_log_line(c, "@dre@farm@dbl@ just a little way "
						"@dre@North West of Lumbridge.", 1);
					break;
				case 1:
					write_quest_log_line(c, "@str@I asked Farmer Fred, near "
						"Lumbridge, for a quest. Fred", 0);
					write_quest_log_line(c, "@str@said he'd pay me for shearing "
						"his sheep for him!", 1);

					if ((temp_one = player_has_num_items_non_stack(c, 1759) 
						 + c->plr->quest_data[SHEEP_SHEARER][0]) >= 20)
					{
						write_quest_log_line(c, "@dbl@I have enough @dre@balls "
							"of wool@dbl@ to give @dre@Fred@dbl@ and get "
							"my @dre@reward", 3);
						write_quest_log_line(c, "@dre@money!", 4);
					}
					else
					{
						if (temp_one == 19)
						{
							snprintf(temp, 300, "@dbl@I need to collect %d "
								"more @dre@ball of wool.", (20 - temp_one));
						}
						else
						{
							snprintf(temp, 300, "@dbl@I need to collect %d "
								"more @dre@balls of wool.", (20 - temp_one));
						}
						write_quest_log_line(c, temp, 3);
					}
					break;
				case 255:
					write_quest_log_line(c, "@str@I asked Farmer Fred, near "
						"Lumbridge, for a quest. Fred", 0);
					write_quest_log_line(c, "@str@said he'd pay me for shearing "
						"his sheep for him!", 1);
					write_quest_log_line(c, "@str@I brought him 20 balls of "
						"wool and got paid!", 2);
					write_quest_log_line(c, "@red@QUEST COMPLETE", 4);
					write_quest_log_line(c, "@red@Reward", 5);
					write_quest_log_line(c, "@blu@1 Quest Point", 6);
					write_quest_log_line(c, "@blu@150 Crafting XP", 7);
					write_quest_log_line(c, "@blu@60 Coins", 8);
					break;
			}
			break;
		default:
			write_quest_log_line(c, "@dbl@This quest is not implemented yet.", 
				1);
			break;
	}

	packet_show_interface(c, 8134);
}

void set_quest_state(struct client *c, int quest_idx, int state)
{
	if (quest_idx < 0 || quest_idx > PROTOCOL_NUM_QUESTS)
	{
		printf("Invalid quest idx! %d\n", quest_idx);
		return;
	}

	c->plr->quests[quest_idx] = state;	
	update_quest_log_status(c, quest_idx);
}

void give_quest_rewards(struct client *c, int quest_idx)
{
	if (quest_idx < 0 || quest_idx > PROTOCOL_NUM_QUESTS)
	{
		printf("Invalid quest idx! %d\n", quest_idx);
		return;
	}

	switch (quest_idx)
	{
		case SHEEP_SHEARER:
			add_skill_xp(c, CRAFTING_SKILL, 150);
			add_item_to_inv_drop_if_no_space(c, 995, 60, -1);
			c->plr->quest_points += QUEST_DATA[quest_idx]->quest_point_reward;
			printf("qp: %d\n", c->plr->quest_points);
			break;
	}
}

/**
 * Show the quest complete menu for a specific quest, and set it to completed
 * for that player.
 * @param *c Client instance.
 * @param quest_idx Quest to finish.
 */
void finish_quest(struct client *c, int quest_idx)
{
	char temp[300];

	snprintf(temp, 300, "You have completed the %s quest!", QUEST_DATA[quest_idx]->name);
	packet_set_interface_text(c, temp, 12144);

	/* Clear reward text real quick.  */
	packet_set_interface_text(c, "", 12150);
	packet_set_interface_text(c, "", 12151);
	packet_set_interface_text(c, "", 12152);
	packet_set_interface_text(c, "", 12153);
	packet_set_interface_text(c, "", 12154);
	packet_set_interface_text(c, "", 12155);

	switch (quest_idx)
	{
		case SHEEP_SHEARER:
			packet_set_interface_text(c, "1 Quest Point", 12150);
			packet_set_interface_text(c, "150 Crafting XP", 12151);
			packet_set_interface_text(c, "60 Coins", 12152);
			packet_set_interface_text(c, "", 12153);
			packet_set_interface_text(c, "", 12154);
			packet_set_interface_text(c, "", 12155);
			packet_set_item_in_interface_model(c, 12145, 300, 1735);
			break;
	}

	packet_send_chatbox_message(c, "Congratulations! Quest complete!");
	give_quest_rewards(c, quest_idx);
	set_quest_state(c, quest_idx, 255);
	update_quest_points(c);
	/* Show number of current quest points.  */
	snprintf(temp, 300, "%d", c->plr->quest_points);
	packet_set_interface_text(c, temp, 12147);

	packet_show_interface(c, 12140);
}
