/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file dialogue_handler.c
 * @brief Contains handle_dialogue function, handles all chat related events.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"

#include "client.h"
#include "dialogue.h"
#include "interface.h"
#include "item.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "quests.h"
#include "skills.h"

/** Dialogue head animations, emotions.  */
enum HEAD_ANIMS
{
	ANIM_HAPPY = 588,
	ANIM_CALM = 589,
	ANIM_DEFAULT = 591,
	ANIM_EVIL = 592,
	ANIM_HAPPY_EVIL = 594,
	ANIM_ANNOYED = 595,
	ANIM_DISTRESSED = 596,
	ANIM_NEAR_TEARS = 598,
	ANIM_SAD = 599,
	ANIM_SLEEPY_LEFT = 600,
	ANIM_SLEEPY_RIGHT = 601,
	ANIM_SLEEPY = 602,
	ANIM_SLEEPY2 = 603,
	ANIM_GLARING = 604,
	ANIM_LAUGH = 605,
	ANIM_LAUGH2 = 606,
	ANIM_EVIL_LAUGH = 609,
	ANIM_ANGRY = 614
};

/**
 * Resets player and npc variables when last dialogue has been sent.
 * @param *c Client instance.
 * @bug Needs to only reset NPC talk state if the player is the last one
 * that was talking with them.
 */
void end_dialogue(struct client *c)
{
	c->plr->next_chat_id = 0;

	if (c->plr->action_index != -1 && c->plr->chat_type == NPC_CHAT)
	{
		c->plr->action_id = -1;
		c->plr->action_mask = 0;
		c->plr->is_pursuing_target = 0;
		player_stop_facing_npc(c);
		/* Reset the NPC's state and clear their facing setting.  */
		set_npc_ai_state(NPC_SPAWNS[c->plr->action_index], AI_IDLE);
		reset_npc_face_mob(NPC_SPAWNS[c->plr->action_index]);
	}

	c->plr->chat_type = -1;
}

/**
 * Sends NPC chat events to the user, and skill dialogue menus.
 * @param *c Client instance.
 */
void handle_dialogue(struct client *c)
{
	char temp_str[300];
	int temp_int = 0;

	/* Handle skill action menus.  */
	if (c->plr->action_menu_id != -1 && c->plr->chat_type == SKILL_MENU_CHAT)
	{
		/* Sends skill related menus when needed based on their id.  */
		switch (c->plr->action_menu_skill_id)
		{
			case FLETCHING_SKILL:
				fletching_action_menus(c);
				break;
			case COOKING_SKILL:
				cooking_action_menus(c);
				break;
			case CRAFTING_SKILL:
				crafting_action_menus(c);
				break;
			case SMITHING_SKILL:
				smithing_action_menus(c);
				break;
			default:
				printf("ERROR: No action menu function set for skill id: %d\n",
					   c->plr->action_menu_skill_id);
				break;
		}

		return;
	}

	/* Show the initial level up screen, then show the level up benefits menu 
	 * after, if applicable.  */
	if (c->plr->chat_type == LEVEL_UP_CHAT)
	{
		int skill_id = get_next_level_up_to_handle(c);

		/* If there aren't any skills left, close out the dialogue.  */
		if (skill_id == -1)
		{
			end_dialogue(c);
		}

		/* Show the initial level up box.  */
		if (c->plr->next_chat_id == 0)
		{
			switch (skill_id)
			{
				/* Show the initial level up dialogue box.  */
				default:
					player_draw_gfx(c, 199);
					send_level_up_dialogue(c, skill_id);
					c->plr->next_chat_id = c->plr->level[skill_id];
					break;
			}
		}
		else
		{
			/* Show the next level up interface, if it is queued up.  */
			switch (skill_id)
			{
				default:
					c->plr->needs_level_up_dialogue[skill_id] = 0;

					skill_id = get_next_level_up_to_handle(c);

					if (skill_id == -1)
					{
						end_dialogue(c);
						packet_close_all_open_interfaces(c);
					}
					else
					{
						c->plr->next_chat_id = 0;
						c->plr->chat_type = LEVEL_UP_CHAT;
						handle_dialogue(c);
					}
					break;
			}
		}
		
		return;
	}

	/* Dialogue is sorted by npc's id.  */
	switch (c->plr->action_id)
	{
		/* Default man.  */
		case 1:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_npc_chat(c, "Testing dialogue.", "", "", "", 
								  c->plr->action_id, ANIM_SAD);
					c->plr->next_chat_id++;
					break;
				case 1:
					send_player_chat(c, "Get a job.", "", "", "",
									 ANIM_EVIL);
					c->plr->next_chat_id++;
					break;
				case 2:
					send_npc_chat(c, "What do you suggest I do?", "", 
									 "", "",
								  c->plr->action_id, ANIM_DEFAULT);
					c->plr->next_chat_id++;
					break;
				case 3:
					send_chat_choice(c, "Robbery.", "Ayy lmao.", "", 
									 "", "");
					c->plr->next_chat_id++;
					break;
				case 4:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_npc_chat(c, "Isn't that illegal?", "", "", "",
										  c->plr->action_id, ANIM_ANGRY);
							end_dialogue(c);
							break;
						case 1:
							send_npc_chat(c, "What does that even mean?", 
											 "", "", "",
										  c->plr->action_id, ANIM_ANNOYED);
							end_dialogue(c);
							break;
					}
					break;
			}
			break;
		/* Horvik in Varrock's Armour Shop.  */
		case 549:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_npc_chat(c, "Hello, do you need any help?", 
								  "", "", "", c->plr->action_id, ANIM_HAPPY);
					c->plr->next_chat_id++;
					break;
				case 1:
					send_chat_choice(c, 
						"No, thanks. I'm just looking around.", 
						"Do you want to trade?",
						"",
						"",
						"");
					c->plr->next_chat_id++;
					break;
				case 2:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, 
								"No, thanks. I'm just looking around.", 
								"", "", "", ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 1:
							send_shop_menu(c, NPCS[c->plr->action_id]->shop_id);
							break;
					}
					break;
				case 3:
					send_npc_chat(c, 
							"Well, come and see me if you're ever in need of armour!",
							"", "", "", c->plr->action_id, ANIM_ANNOYED);
					end_dialogue(c);
					break;
			}
			break;
		/* Lowe in Varrock's Archery Emporium.  */
		case 550:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_npc_chat(c, "Welcome to Lowe's Archery Emporium. Do you want", 
								  "to see my wares?", "", "", c->plr->action_id, ANIM_HAPPY);
					c->plr->next_chat_id++;
					break;
				case 1:
					send_chat_choice(c, "Yes, please.", 
						"No, I prefer to bash things close up.", 
						"",
						"",
						"");
					c->plr->next_chat_id++;
					break;
				case 2:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_shop_menu(c, NPCS[c->plr->action_id]->shop_id);
							break;
						case 1:
							send_player_chat(c, "No, I prefer to bash things close up.", 
								"", "", "", ANIM_EVIL_LAUGH);
							c->plr->next_chat_id++;
							break;
					}
					break;
				case 3:
					send_npc_chat(c, "Humph, philistine.", "", "", "", c->plr->action_id, ANIM_ANNOYED);
					end_dialogue(c);
					break;
			}
			break;
		/* Farmer Fred.  */
		case 758:
			switch (c->plr->next_chat_id)
			{
				case 0:
					if (is_quest_started(c, SHEEP_SHEARER) != 1)
					{
						send_npc_chat(c, "What are you doing on my land? You're not the one",
							"who keeps leaving all my gates open and letting out all",
							"my sheep are you?", "", c->plr->action_id, ANIM_ANNOYED);
						c->plr->next_chat_id++;
					}
					else if (is_quest_in_progress(c, SHEEP_SHEARER) == 1)
					{
						send_npc_chat_simple(c, 
							"What are you doing on my land?",
							c->plr->action_id,
							ANIM_ANGRY);
						c->plr->next_chat_id = 200;
					}
					else
					{
						end_dialogue(c);
						packet_send_chatbox_message(c, "They have nothing to say to you.");
					}
					break;
				case 1:
					send_chat_choice(c, "I'm looking for a quest.", 
						"I'm looking for something to kill", 
						"I'm lost.",
						"",
						"");
					c->plr->next_chat_id++;
					break;
				case 2:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, "I'm looking for a quest.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 1:
							send_player_chat(c, "I'm looking for something to kill.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 2:
							send_player_chat(c, "I'm lost.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
					}
					break;
				case 3:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_npc_chat_simple(c, 
								"You're after a quest, you say? Actually, "
								"I could do with a bit of help.", 
								c->plr->action_id, ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 1:
							send_npc_chat(c, "What, on my land? Leave my livestock alone you",
								"scoundrel!",
								"",
								"", c->plr->action_id, ANIM_ANGRY);
							end_dialogue(c);
							break;
						case 2:
							send_npc_chat(c, "How can you be lost? Just follow the road east and",
								"south. You'll end up in Lumbridge fairly quickly.",
								"",
								"", c->plr->action_id, ANIM_CALM);
							end_dialogue(c);
							break;
					}
					break;
				case 4:
					send_npc_chat_simple(c, 
								"My sheep are getting mighty woolly. "
								"I'd be much obliged if you could shear "
								"them. And while you're at it spin the "
								"wool for me too.", c->plr->action_id,
								ANIM_HAPPY);
					c->plr->next_chat_id++;
					break;
				case 5:
					send_npc_chat_simple(c, 
						"Yes, that's it. Bring me 20 balls of wool. And "
						"I'm sure I could sort out some sort of payment.",
						c->plr->action_id,
						ANIM_HAPPY);
					c->plr->next_chat_id++;
					break;
				case 6:
					send_chat_choice(c, "Yes okay. I can do that.",
									 "That doesn't sound like a very exciting quest.", 
									 "", 
									 "", 
									 "");
					c->plr->next_chat_id++;
					break;
				case 7:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, "Yes okay. I can do that.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 1:
							send_player_chat(c, "That doesn't sound like a very exciting quest.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
					}
					break;
				case 8:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_npc_chat_simple(c, "Good! Now one more "
							"thing, do you actually know how to shear a sheep?",
								c->plr->action_id,
								ANIM_CALM);
							c->plr->next_chat_id++;
							break;
						case 1:
							send_npc_chat_simple(c, "Well what do you "
								"expect if you ask a farmer for a quest? "
								"Now are you going to help me or not?",
								c->plr->action_id,
								ANIM_ANNOYED);
							c->plr->next_chat_id = 40;
							break;
					}
					break;
				case 9:
					send_chat_choice(c, "Of course!", 
						"Err. No, I don't actually.", "", "", "");
					c->plr->next_chat_id++;
					break;
				case 10:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, "Of course!",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id = 20;
							break;
						case 1:
							send_player_chat(c, "Err. No, I don't actually.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id++;
							break;
					}
					break;
				case 11:
					if (player_has_item(c, 1735, 1) != -1)
					{
						send_npc_chat_simple(c,
						"Well, you're halfway there already! "
						"You have a set of shears in your inventory. "
						"Just use those on a Sheep to shear it.",
						c->plr->action_id,
						ANIM_CALM);
					}
					else
					{
						send_npc_chat_simple(c,
						"Well, first things first, you need a pair of shears. "
						"I've got some here you can use.",
						c->plr->action_id,
						ANIM_CALM);
					}
					c->plr->next_chat_id++;
					break;
				case 12:
					send_npc_chat_simple(c,
					"You just need to go and use them on the sheep out in my field.",
					c->plr->action_id,
					ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 13:
					send_player_chat(c, "Sounds easy!",
						"", "", "",
									 ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 14:
					send_npc_chat_simple(c,
					"That's what they all say! Some of the sheep don't like it too much... Persistence is the key.",
					c->plr->action_id,
					ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 15:
					send_npc_chat_simple(c,
					"Once you've collected some wool you can spin it into balls.",
					c->plr->action_id,
					ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 16:
					send_npc_chat_simple(c,
					"Do you know how to spin wool?",
					c->plr->action_id,
					ANIM_CALM);
					c->plr->next_chat_id = 21;
					break;
				case 20:
					send_npc_chat_simple(c, 
						"And you know how to spin wool into balls?",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				case 21:
					send_chat_choice(c, "I'm something of an expert actually!",
									 "I don't know how to spin wool, sorry.",
									 "", 
									 "", 
									 "");
					c->plr->next_chat_id++;
					break;
				case 22:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, "I'm something of an expert actually!",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id = 100;
							break;
						case 1:
							send_player_chat(c,
								"I don't know how to spin wool, sorry.",
								"", "", "", ANIM_CALM);
							c->plr->next_chat_id++;
							break;
					}
					break;
				case 23:
					send_npc_chat_simple(c, 
						"Don't worry, it's quite simple! The nearest "
						"Spinning Wheel can be found on the first floor "
						"of Lumbridge Castle.",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				case 24:
					send_npc_chat_simple(c, 
						"To get to Lumbridge Castle just follow the road east.",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				case 25:
					send_player_chat(c, "Thank you!",
						"", "", "",
									 ANIM_HAPPY);
					c->plr->next_chat_id++;
					set_quest_state(c, SHEEP_SHEARER, 1);
					end_dialogue(c);
					break;
				case 40:
					send_chat_choice(c, "Yes okay. I can do that.",
									 "No I'll give it a miss.",
									 "", 
									 "", 
									 "");
					c->plr->next_chat_id++;
					break;
				case 41:
					switch (c->plr->chat_choice)
					{
						case 0:
							send_player_chat(c, "Yes okay. I can do that.",
								"", "", "",
											 ANIM_CALM);
							c->plr->next_chat_id = 8;
							break;
						case 1:
							send_player_chat(c, "No I'll give it a miss.",
								"", "", "",
											 ANIM_CALM);
							end_dialogue(c);
							break;
					}
					break;
				case 100:
					send_npc_chat_simple(c, 
						"Well you can stop grinning and get to work then.",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				case 101:
					send_npc_chat_simple(c, 
						"I'm not paying you by the hour!",
						c->plr->action_id,
						ANIM_ANGRY);
						set_quest_state(c, SHEEP_SHEARER, 1);
						end_dialogue(c);
					break;
				/* In progress messages.  */
				case 200:
					send_player_chat(c, "I need to talk to you about shearing those sheep.",
						"", "", "",
									 ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 201:
					send_npc_chat_simple(c, 
						"Oh. How are you doing getting those balls of wool?",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				case 202:
					/* If player has the wool.  */
					if (player_has_num_items_non_stack(c, 1759) > 0)
					{
						send_player_chat(c, "I got some.",
							"", "", "",
										 ANIM_CALM);
						c->plr->next_chat_id++;
					}
					else
					{
						send_player_chat(c, "How many more do I need to get you?",
							"", "", "",
										 ANIM_CALM);
						c->plr->next_chat_id = 210;
					}
					break;
				case 203:
					send_npc_chat_simple(c, 
						"Give 'em here then.",
						c->plr->action_id,
						ANIM_CALM);

						temp_int = player_has_num_items_non_stack(c, 1759);

						printf("temp int: %d\n", temp_int);

						/* Cap number of items we're give away.  */
						if (temp_int + c->plr->quest_data[SHEEP_SHEARER][0] 
							> 20)
						{
							temp_int 
								= (20 - c->plr->quest_data[SHEEP_SHEARER][0]);
						}

						printf("temp int: %d\n", temp_int);
						
						if (temp_int > 0)
						{
							delete_item_from_inv_nonstack(c, 1759, temp_int);
							c->plr->quest_data[SHEEP_SHEARER][0] += temp_int;
						}

						if (c->plr->quest_data[SHEEP_SHEARER][0] == 20)
						{
							c->plr->next_chat_id++;
						}
						else
						{
							c->plr->next_chat_id = 210;
						}
					break;
				case 204:
					send_player_chat(c, "That's the last of them.",
						"", "", "",
									 ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 205:
					send_npc_chat_simple(c, 
						"I guess I'd better pay you then.",
						c->plr->action_id,
						ANIM_CALM);
						c->plr->next_chat_id++;
					break;
				/* Quest complete.  */
				case 206:
					end_dialogue(c);
					finish_quest(c, SHEEP_SHEARER);	
					break;

				/* Remaining balls of wool.  */
				case 210:
					snprintf(temp_str, 300, "You need to collect %d more balls of wool.", 
							 (20 - c->plr->quest_data[SHEEP_SHEARER][0]));
					send_npc_chat_simple(c, 
						temp_str,
						c->plr->action_id,
						ANIM_CALM);
					/* If the player only has unspun wool, show dialogue on
					 * how to spin it.  */
					if (player_has_num_items_non_stack(c, 1759) == 0
						&& player_has_num_items_non_stack(c, 1737) != 0)
					{
						c->plr->next_chat_id++;
					}
					else
					{
						end_dialogue(c);
					}
					break;
				case 211:
					send_player_chat(c, "I've got some wool. I've not managed to make it into",
						"a ball, though.", "", "",
									 ANIM_CALM);
					c->plr->next_chat_id++;
					break;
				case 212:
					send_npc_chat_simple(c, 
						"Well go find a spinning wheel then. You can find "
						"one on the first floor of Lumbridge Castle, just "
						"walk east on the road outside my house and you'll "
						"find Lumbridge.",
						c->plr->action_id,
						ANIM_CALM);
					end_dialogue(c);
					break;
			}
			break;
					
		/* There is no dialogue for this npc.  */
		default:
			if (c->plr->action_id != -1)
			{
				printf("NPC %d has no dialogue!\n", c->plr->action_id);
				/* FIXME: This may not be the accurate string to send when
				 * an NPC has no dialogue. Need to investigate.  */
				packet_send_chatbox_message(c, "They have nothing to say to you.");
			}
			
			end_dialogue(c);
			packet_close_all_open_interfaces(c);
			c->plr->next_chat_id = -1;
			break;
	}
}
