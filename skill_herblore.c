/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
/* Herblore - ID 15.  */
int get_herblore_level_needed(int herblore_id)
{
	switch (herblore_id)
	{
	/* Identifying herbs.  */
		case HERBLORE_IDENT_GUAM:
			return 3;
		case HERBLORE_IDENT_MARRENTIL:
			return 5;
		case HERBLORE_IDENT_TARROMIN:
			return 11;
		case HERBLORE_IDENT_HARRALANDER:
			return 20;
		case HERBLORE_IDENT_RANARR_WEED:
			return 25;
		case HERBLORE_IDENT_TOADFLAX:
			return 30;
		case HERBLORE_IDENT_IRIT:
			return 40;
		case HERBLORE_IDENT_AVANTOE:
			return 48; 
		case HERBLORE_IDENT_KWUARM:
			return 54;
		case HERBLORE_IDENT_SNAPDRAGON:
			return 59;
		case HERBLORE_IDENT_CADANTINE:
			return 65;
		case HERBLORE_IDENT_LANTADYME:
			return 67;
		case HERBLORE_IDENT_DWARF_WEED:
			return 70;
		case HERBLORE_IDENT_TORSTOL:
			return 75;
		/* Note: Quest requirement for these, Jungle Potion.  */
		case HERBLORE_IDENT_SNAKE_WEED:
			return 3;
		case HERBLORE_IDENT_ARDRIGAL:
			return 3;
		case HERBLORE_IDENT_SITO_FOIL:
			return 3;
		case HERBLORE_IDENT_VOLENCIA_MOSS:
			return 3;
		case HERBLORE_IDENT_ROGUES_PURSE:
			return 3;
	/* Adding herbs to water.  */
		case HERBLORE_ADD_GUAM:
			return 3;
		case HERBLORE_ADD_MARRENTIL:
			return 5;
		case HERBLORE_ADD_TARROMIN:
			return 12;	
		case HERBLORE_ADD_HARRALANDER:
			return 21;
		case HERBLORE_ADD_RANARR_WEED:
			return 30;
		case HERBLORE_ADD_TOADFLAX:
			return 34;
		case HERBLORE_ADD_IRIT:
			return 45;
		case HERBLORE_ADD_AVANTOE:
			return 50;
		case HERBLORE_ADD_KWUARM:
			return 55;
		case HERBLORE_ADD_SNAPDRAGON:
			return 63;
		case HERBLORE_ADD_CADANTINE:
			return 66;
		case HERBLORE_ADD_DWARF_WEED:
			return 72;
		case HERBLORE_ADD_TORSTOL:
			return 78;
	/* Adding herb to coconut.  */
		case HERBLORE_ADD_TOADFLAX_COCONUT:
			return 68;
		case HERBLORE_ADD_IRIT_COCONUT:
			return 79;

	/* Making the finished potion level needed.  */
		case HERBLORE_ATTACK_POTION:
			return 3; 
		case HERBLORE_ANTIPOISON:
			return 5;
		case HERBLORE_STRENGTH_POTION:
			return 12;
		case HERBLORE_SERUM_207:
			return 15;
		case HERBLORE_RESTORE_POTION:
			return 22;
		case HERBLORE_BLAMISH_OIL:
			return 25;
		case HERBLORE_ENERGY_POTION:
			return 26;
		case HERBLORE_DEFENCE_POTION:
			return 30;
		case HERBLORE_AGILITY_POTION:
			return 34;
		case HERBLORE_PRAYER_POTION:
			return 38;
		case HERBLORE_SUPER_ATTACK:
			return 45;
		case HERBLORE_SUPER_ANTIPOISON:
			return 48;
		case HERBLORE_FISHING_POTION:
			return 50;
		case HERBLORE_SUPER_ENERGY:
			return 52;
		case HERBLORE_SUPER_STRENGTH:
			return 55;
		case HERBLORE_WEAPON_POISON:
			return 60;
		case HERBLORE_SUPER_RESTORE:
			return 63;
		case HERBLORE_SUPER_DEFENCE:
			return 66;
		case HERBLORE_ANTIDOTE_P:
			return 68;
		case HERBLORE_RANGING_POTION:
			return 72;
		case HERBLORE_WEAPON_POISON_P:
			return 73;
		case HERBLORE_ZAMORAK_BREW:
			return 78;
		case HERBLORE_ANTIDOTE_PP:
			return 79;
		case HERBLORE_WEAPON_POISON_PP:
			return 82;

		default:
			printf("ID %d unhandled! herblore level unknown!\n", herblore_id);
			return -1;
	}
}

float get_herblore_xp(int  herblore_id)
{
	switch (herblore_id)
	{
	/* Identifying herbs.  */
		case HERBLORE_IDENT_GUAM:
			return 2.5;
		case HERBLORE_IDENT_MARRENTIL:
			return 3.8;
		case HERBLORE_IDENT_TARROMIN:
			return 5.0;
		case HERBLORE_IDENT_HARRALANDER:
			return 6.3;
		case HERBLORE_IDENT_RANARR_WEED:
			return 7.5;
		case HERBLORE_IDENT_TOADFLAX:
			return 8.0;
		case HERBLORE_IDENT_IRIT:
			return 8.8;
		case HERBLORE_IDENT_AVANTOE:
			return 10.0; 
		case HERBLORE_IDENT_KWUARM:
			return 11.3;
		case HERBLORE_IDENT_SNAPDRAGON:
			return 11.8;
		case HERBLORE_IDENT_LANTADYME:
			return 13.1;
		case HERBLORE_IDENT_CADANTINE:
			return 12.5;
		case HERBLORE_IDENT_DWARF_WEED:
			return 13.8;
		case HERBLORE_IDENT_TORSTOL:
			return 15.0;
		/* Note: Quest requirement for these, Jungle Potion.  */
		case HERBLORE_IDENT_SNAKE_WEED:
		case HERBLORE_IDENT_ARDRIGAL:
		case HERBLORE_IDENT_SITO_FOIL:
		case HERBLORE_IDENT_VOLENCIA_MOSS:
		case HERBLORE_IDENT_ROGUES_PURSE:
			return 2.5;

		/* Making the finished potion xp.  */
		case HERBLORE_ATTACK_POTION:
			return 25.0;
		case HERBLORE_ANTIPOISON:
			return 37.5;
		case HERBLORE_STRENGTH_POTION:
			return 50.0;
		case HERBLORE_SERUM_207:
			return 50.0;
		case HERBLORE_RESTORE_POTION:
			return 62.5;
		case HERBLORE_BLAMISH_OIL:
			return 80.0;
		case HERBLORE_ENERGY_POTION:
			return 67.5;
		case HERBLORE_DEFENCE_POTION:
			return 75.0;
		case HERBLORE_PRAYER_POTION:
			return 87.5;
		case HERBLORE_AGILITY_POTION:
			return 80.0;
		case HERBLORE_SUPER_ATTACK:
			return 100.0;
		case HERBLORE_SUPER_ANTIPOISON:
			return 106.3;
		case HERBLORE_FISHING_POTION:
			return 112.5;
		case HERBLORE_SUPER_ENERGY:
			return 117.5;
		case HERBLORE_SUPER_STRENGTH:
			return 125.0;
		case HERBLORE_WEAPON_POISON:
			return 137.5;
		case HERBLORE_SUPER_RESTORE:
			return 142.5;
		case HERBLORE_SUPER_DEFENCE:
			return 150.0;
		case HERBLORE_ANTIDOTE_P:
			return 155.0;
		case HERBLORE_RANGING_POTION:
			return 162.5;
		case HERBLORE_WEAPON_POISON_P:
			return 165.0;
		case HERBLORE_ZAMORAK_BREW:
			return 175.0;
		case HERBLORE_ANTIDOTE_PP:
			return 177.5;
		case HERBLORE_WEAPON_POISON_PP:
			return 190.0;

		default:
			printf("ID %d unhandled! herblore xp unknown!\n", herblore_id);
			return -1;
	}
}

int get_herblore_id_from_herb(int herb_id)
{
	switch (herb_id)
	{
	/* Unidentified herb ids.  */
		case 199:
			return HERBLORE_IDENT_GUAM;
		case 201:
			return HERBLORE_IDENT_MARRENTIL;
		case 203:
			return HERBLORE_IDENT_TARROMIN;
		case 205:
			return HERBLORE_IDENT_HARRALANDER;
		case 207:
			return HERBLORE_IDENT_RANARR_WEED;
		case 3049:
			return HERBLORE_IDENT_TOADFLAX;
		case 209:
			return HERBLORE_IDENT_IRIT;
		case 211: 
			return HERBLORE_IDENT_AVANTOE;
		case 213:
			return HERBLORE_IDENT_KWUARM;
		case 3051:
			return HERBLORE_IDENT_SNAPDRAGON;
		case 2485:
			return HERBLORE_IDENT_LANTADYME;
		case 215:
			return HERBLORE_IDENT_CADANTINE;
		case 217:
			return HERBLORE_IDENT_DWARF_WEED;
		case 219:
			return HERBLORE_IDENT_TORSTOL;
		/* Note; Quest requirement for these, Jungle Potion.  */
		case 1525:
			return HERBLORE_IDENT_SNAKE_WEED;
		case 1527:
			return HERBLORE_IDENT_ARDRIGAL;
		case 1529:
			return HERBLORE_IDENT_SITO_FOIL;
		case 1531:
			return HERBLORE_IDENT_VOLENCIA_MOSS;
		case 1533:
			return HERBLORE_IDENT_ROGUES_PURSE;
	/* Identified herb ids. Used for adding herbs to vials as the first 
	 * ingredient.  */
		case 249:
			return HERBLORE_ADD_GUAM;
		case 251:
			return HERBLORE_ADD_MARRENTIL;
		case 253:
			return HERBLORE_ADD_TARROMIN;
		case 255:
			return HERBLORE_ADD_HARRALANDER;
		case 257:
			return HERBLORE_ADD_RANARR_WEED;
		case 2998:
			return HERBLORE_ADD_TOADFLAX;
		case 259:
			return HERBLORE_ADD_IRIT;
		case 261: 
			return HERBLORE_ADD_AVANTOE;
		case 263:
			return HERBLORE_ADD_KWUARM;
		case 3000:
			return HERBLORE_ADD_SNAPDRAGON;
		case 265:
			return HERBLORE_ADD_CADANTINE;
		case 267:
			return HERBLORE_ADD_DWARF_WEED;
		case 269:
			return HERBLORE_ADD_TORSTOL;
		default:
			printf("Herb ID %d not handled, or not an herb!\n", herb_id);
			return -1;
	}
}

int get_herblore_id_from_herb_coconut(int herb_id)
{
	switch (herb_id)
	{
		case 3049:
			return HERBLORE_ADD_TOADFLAX_COCONUT;
		case 3051:
			return HERBLORE_ADD_IRIT_COCONUT;
		default:
			printf("Herb ID %d not handled for coconut, or not an ", herb_id);
			printf("herb!\n");
			return -1;
	}
}

int get_herblore_unidentified_herb_id(int herblore_id)
{
	switch (herblore_id)
	{
		case HERBLORE_IDENT_GUAM:
			return 199;
		case HERBLORE_IDENT_MARRENTIL:
			return 201;
		case HERBLORE_IDENT_TARROMIN:
			return 203;
		case HERBLORE_IDENT_HARRALANDER:
			return 205;
		case HERBLORE_IDENT_RANARR_WEED:
			return 207;
		case HERBLORE_IDENT_TOADFLAX:
			return 3049;
		case HERBLORE_IDENT_IRIT:
			return 209;
		case HERBLORE_IDENT_AVANTOE:
			return 211; 
		case HERBLORE_IDENT_KWUARM:
			return 213;
		case HERBLORE_IDENT_SNAPDRAGON:
			return 3051;
		case HERBLORE_IDENT_LANTADYME:
			return 2485;
		case HERBLORE_IDENT_CADANTINE:
			return 215;
		case HERBLORE_IDENT_DWARF_WEED:
			return 217;
		case HERBLORE_IDENT_TORSTOL:
			return 219;
		/* Note: Quest requirement for these, Jungle Potion.  */
		case HERBLORE_IDENT_SNAKE_WEED:
			return 1525;
		case HERBLORE_IDENT_ARDRIGAL:
			return 1527;
		case HERBLORE_IDENT_SITO_FOIL:
			return 1529;
		case HERBLORE_IDENT_VOLENCIA_MOSS:
			return 1531;
		case HERBLORE_IDENT_ROGUES_PURSE:
			return 1533;
		
		default:
			printf("Unidentified herb for id %d not found!\n", herblore_id);
			return -1;
	}
}

int get_herblore_identified_herb_id(int herblore_id)
{
	switch (herblore_id)
	{
		case HERBLORE_IDENT_GUAM:
			return 249;
		case HERBLORE_IDENT_MARRENTIL:
			return 251;
		case HERBLORE_IDENT_TARROMIN:
			return 253;
		case HERBLORE_IDENT_HARRALANDER:
			return 255;
		case HERBLORE_IDENT_RANARR_WEED:
			return 257;
		case HERBLORE_IDENT_TOADFLAX:
			return 2998;
		case HERBLORE_IDENT_IRIT:
			return 259;
		case HERBLORE_IDENT_AVANTOE:
			return 261; 
		case HERBLORE_IDENT_KWUARM:
			return 263;
		case HERBLORE_IDENT_SNAPDRAGON:
			return 3000;
		case HERBLORE_IDENT_LANTADYME:
			return 2481;
		case HERBLORE_IDENT_CADANTINE:
			return 265;
		case HERBLORE_IDENT_DWARF_WEED:
			return 267;
		case HERBLORE_IDENT_TORSTOL:
			return 269;
		/* Note: Quest requirement for these, Jungle Potion.  */
		case HERBLORE_IDENT_SNAKE_WEED:
			return 1526;
		case HERBLORE_IDENT_ARDRIGAL:
			return 1528;
		case HERBLORE_IDENT_SITO_FOIL:
			return 1530;
		case HERBLORE_IDENT_VOLENCIA_MOSS:
			return 1532;
		case HERBLORE_IDENT_ROGUES_PURSE:
			return 1534;
		
		default:
			printf("Identified herb for id %d not found!\n", herblore_id);
			return -1;
	}
}

int get_herblore_id_from_vial_ingredient(int vial_id, int ingredient_id)
{
	switch (vial_id)
	{
	/* Potions in water.  */
		/* Guam potion.  */
		case 91:
			switch (ingredient_id)
			{
				/* Eye of newt.  */
				case 221:
					return HERBLORE_ATTACK_POTION;
				default:
					return -1;
			}
			break;
		/* Marrentil potion.  */
		case 93:
			switch (ingredient_id)
			{
				/* Unicorn horn dust.  */
				case 235:
					return HERBLORE_ANTIPOISON;
				default:
					return -1;
			}
			break;
		/* Tarromin potion.  */
		case 95:
			switch (ingredient_id)
			{
				/* Limpwurt root.  */
				case 225:
					return HERBLORE_STRENGTH_POTION;
				/* Ashes.  */
				/* Note, learned during a quest.  */
				case 592:
					return HERBLORE_SERUM_207;
				default:
					return -1;
			}
			break;
		/* Harralander potion.  */
		case 97:
			switch (ingredient_id)
			{
				/* Red spider eggs.  */
				case 223:
					return HERBLORE_RESTORE_POTION;
				/* Blamish snail slime.  */
				/* Note, learned during a quest.  */
				case 1581:
					return HERBLORE_BLAMISH_OIL;
				/* Chocolate dust.  */
				case 1975:
					return HERBLORE_ENERGY_POTION;
				default:
					return -1;
			}
			break;
		/* Ranarr potion.  */
		case 99:
			switch (ingredient_id)
			{
				/* White berries.  */
				case 239:
					return HERBLORE_DEFENCE_POTION;
				/* Snape grass.  */
				case 231:
					return HERBLORE_PRAYER_POTION;
				default:
					return -1;
			}
			break;
		/* Toadflax potion.  */
		case 3002:
			switch (ingredient_id)
			{
				/* Toad's legs.  */
				case 2152:
					return HERBLORE_AGILITY_POTION;
				default:
					return -1;
			}
			break;
		/* Irit potion.  */
		case 101:
			switch (ingredient_id)
			{
				/* Eye of newt.  */
				case 221:
					return HERBLORE_SUPER_ATTACK;
				/* Unicorn horn dust.  */
				case 235:
					return HERBLORE_SUPER_ANTIPOISON;
				default:
					return -1;
			}
			break;
		/* Avantoe potion.  */
		case 103:
			switch (ingredient_id)
			{
				/* Snape grass.  */
				case 231:
					return HERBLORE_FISHING_POTION;
				/* Mort myre fungi.  */
				case 2970:
					return HERBLORE_SUPER_ENERGY;
				default:
					return -1;
			}
			break;
		/* Kwuarm potion.  */
		case 105:
			switch (ingredient_id)
			{
				/* Limpwurt root.  */
				case 225:
					return HERBLORE_SUPER_STRENGTH;
				/* Dragon scale dust.  */
				case 241:
					return HERBLORE_WEAPON_POISON;
				default:
					return -1;
			}
			break;
		/* Snapdragon potion.  */
		case 3004:
			switch (ingredient_id)
			{
				/* Red spider eggs.  */
				case 223:
					return HERBLORE_SUPER_RESTORE;
				default:
					return -1;
			}
			break;
		/* Cadantine potion.  */
		case 107:
			switch (ingredient_id)
			{
				/* White berries.  */
				case 239:
					return HERBLORE_SUPER_DEFENCE;
				default:
					return -1;
			}
			break;
		/* Dwarf weed potion.  */
		case 109:
			switch (ingredient_id)
			{
				/* Wine of Zamorak.  */
				case 245:
					return HERBLORE_RANGING_POTION;
				default:
					return -1;
			}
			break;
		/* Torstol potion.  */
		case 111:
			switch (ingredient_id)
			{
				/* Jangerberries.  */
				case 247:
					return HERBLORE_ZAMORAK_BREW;
				default:
					return -1;
			}
			break;
	/* Coconut vials.  */
		/* Coconut with cactus spine.  */
		case 5936:
			switch (ingredient_id)
			{
				/* Red spider eggs.  */
				case 223:
					return HERBLORE_WEAPON_POISON_P;
				default:
					return -1;
			}
			break;
		/* Coconut with nightshade.  */
		case 5939:
			switch (ingredient_id)
			{
				/* Nightshade.  */
				case 2398:
					return HERBLORE_WEAPON_POISON_PP;
				default:
					return -1;
			}
			break;
		/* Coconut with toadflax.  */
		case 5942:
			switch (ingredient_id)
			{
				/* Yew roots.  */
				case 6049:
					return HERBLORE_ANTIDOTE_P;
				default:
					return -1;
			}
			break;
		/* Coconut with Irit.  */
		case 5951:
			switch (ingredient_id)
			{
				/* Magic roots.  */
				case 6051:
					return HERBLORE_ANTIDOTE_PP;
				default:
					return -1;
			}
			break;

		default:
			return -1;
	}
}

int get_herblore_vial_id(int herblore_id)
{
	switch (herblore_id)
	{
	/* Adding herbs to water.  */
		case HERBLORE_ADD_GUAM:
			return 91;
		case HERBLORE_ADD_MARRENTIL:
			return 93;
		case HERBLORE_ADD_TARROMIN:
			return 95;	
		case HERBLORE_ADD_HARRALANDER:
			return 97;
		case HERBLORE_ADD_RANARR_WEED:
			return 99;
		case HERBLORE_ADD_TOADFLAX:
			return 3002;
		case HERBLORE_ADD_IRIT:
			return 101;
		case HERBLORE_ADD_AVANTOE:
			return 103;
		case HERBLORE_ADD_KWUARM:
			return 105;
		case HERBLORE_ADD_SNAPDRAGON:
			return 3004;
		case HERBLORE_ADD_CADANTINE:
			return 107;
		case HERBLORE_ADD_DWARF_WEED:
			return 109;
		case HERBLORE_ADD_TORSTOL:
			return 111;
	/* Adding herb to coconut.  */
		case HERBLORE_ADD_TOADFLAX_COCONUT:
			return 5942;
		case HERBLORE_ADD_IRIT_COCONUT:
			return 5951;
	/* Finished potions. Note that these are the 3 dose versions, as those
	 * are the only ones the player makes.  */
		case HERBLORE_ATTACK_POTION:
			return 121;
		case HERBLORE_ANTIPOISON:
			return 175;
		case HERBLORE_STRENGTH_POTION:
			return 115;
		case HERBLORE_SERUM_207:
			return 3410;
		case HERBLORE_RESTORE_POTION:
			return 127;
		case HERBLORE_BLAMISH_OIL:
			return 1582;
		case HERBLORE_ENERGY_POTION:
			return 3010; 
		case HERBLORE_DEFENCE_POTION:
			return 133;
		case HERBLORE_PRAYER_POTION:
			return 139;
		case HERBLORE_AGILITY_POTION:
			return 3034;
		case HERBLORE_SUPER_ATTACK:
			return 145;
		case HERBLORE_SUPER_ANTIPOISON:
			return 181;
		case HERBLORE_FISHING_POTION:
			return 151;
		case HERBLORE_SUPER_ENERGY:
			return 3018;
		case HERBLORE_SUPER_STRENGTH:
			return 157;
		case HERBLORE_WEAPON_POISON:
			return 187;
		case HERBLORE_SUPER_RESTORE:
			return 3026;
		case HERBLORE_SUPER_DEFENCE:
			return 163;
		case HERBLORE_ANTIDOTE_P:
			return 5945;
		case HERBLORE_RANGING_POTION:
			return 169;
		case HERBLORE_WEAPON_POISON_P:
			return 5937;
		case HERBLORE_ZAMORAK_BREW:
			return 189;
		case HERBLORE_ANTIDOTE_PP:
			return 5954;
		case HERBLORE_WEAPON_POISON_PP:
			return 5940;

		default:
			printf("Vial with herb for id %d not found!\n", herblore_id);
			return -1;
	}
}

/**
 * Handles players identifying herbs.
 * @param *c Client instance.
 * @param herb_id Unidentified herb id clicked.
 * @param herb_slot Inventory slot the herb is in.
 * @todo Check if the success message is correct.
 */
void identify_herb(struct client *c, int herb_id, int herb_slot)
{
	int herblore_id = get_herblore_id_from_herb(herb_id);
	int level_needed = get_herblore_level_needed(herblore_id);
	int herblore_level = get_player_skill_level(c, HERBLORE_SKILL);
	int identified_herb_id = get_herblore_identified_herb_id(herblore_id);

	if (herblore_level < level_needed)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You cannot clean this herb. "
				 "You need an Herblore level of %d to attempt this.", 
				 level_needed);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	if (herb_slot == -1)
	{
		printf("Player %s tried to identify a herb without it in their", 
			   c->plr->name);
		printf("inventory!\n");
		return;
	}

	/* If success.  */
	transform_item(c, herb_slot, identified_herb_id);
	add_skill_xp(c, HERBLORE_SKILL, get_herblore_xp(herblore_id));
	packet_send_chatbox_message(c, "You identify the herb.");
}

/**
 * Process when player adds the first ingredient to a vial for herblore.
 * @param *c Client instance.
 * @param vial_id Id of the vial the player is using.
 * @param herb_id Id of the herb to add to the vial.
 */
void add_first_ingredient_to_vial(struct client *c, int vial_id, int herb_id)
{
	int herblore_id = -1, level_needed = -1;
	char return_msg[150];
	int herblore_level = get_player_skill_level(c, HERBLORE_SKILL);
	int herb_slot, vial_slot;
	int vial_mix_id;

	/* Check if using water, or coconut milk.  */
	if (vial_id == 227)
	{
		herblore_id = get_herblore_id_from_herb(herb_id);
	}
	else if (vial_id == 5935)
	{
		herblore_id = get_herblore_id_from_herb_coconut(herb_id);
	}

	if (herblore_id == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	herb_slot = player_has_item(c, herb_id, 1);
	vial_slot = player_has_item(c, vial_id, 1);

	if (herb_slot == -1 || vial_slot == -1)
	{
		printf("Player %s tried to add an herb to a vial ", c->plr->name);
		printf("without the proper items!\n");
		set_player_state(c, STATE_IDLE);
		return;
	}

	vial_mix_id = get_herblore_vial_id(herblore_id);
	level_needed = get_herblore_level_needed(herblore_id);

	if (vial_mix_id == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (herblore_level < level_needed)
	{
		snprintf(return_msg, 150, "You must be at least level %d in "
				 "herblore to do that!\n", level_needed);
		packet_send_chatbox_message(c, return_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Handle making the potion now.  */
	transform_item_no_refresh(c, vial_slot, vial_mix_id); 
	delete_item_from_inv_slot(c, herb_slot, 1);

	/* Send the finish message when done.  */
	/* TODO: This may not be 100% accurate, double check.  */
	player_play_animation_no_overwrite(c, HERBLORE_EMOTE_MIXING, 3);
	snprintf(return_msg, 150, "You put the %s into the %s.", 
			 string_to_lower(ITEMS[herb_id]->name), 
			 string_to_lower(ITEMS[vial_id]->name));
	packet_send_chatbox_message(c, return_msg);

	/* Adding an herb to a vial of water/coconut milk gives 1 xp as far as I
	 * could find.  */
	add_skill_xp(c, HERBLORE_SKILL, 1.0);
}

/**
 * Process when player adds the second (usually final) ingredient to a vial
 * for herblore.
 * @param *c Client instance.
 * @param vial_id Id of the vial the player is using.
 * @param ingredient_id Id of the ingredient to add to the vial.
 */
void add_second_ingredient_to_vial(struct client *c, int vial_id, 
								   int ingredient_id)
{
	int herblore_id, level_needed;
	char return_msg[150];
	int herblore_level = get_player_skill_level(c, HERBLORE_SKILL);
	int ingredient_slot, vial_slot;
	int potion_id;

	/* Check if using water, or coconut milk.  */
	herblore_id = get_herblore_id_from_vial_ingredient(vial_id, ingredient_id);

	if (herblore_id == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	ingredient_slot = player_has_item(c, ingredient_id, 1);
	vial_slot = player_has_item(c, vial_id, 1);

	if (ingredient_slot == -1 || vial_slot == -1)
	{
		printf("Player %s tried to add the final ingredient to a vial ", 
			   c->plr->name);
		printf("without the proper items!\n");
		set_player_state(c, STATE_IDLE);
		return;
	}

	potion_id = get_herblore_vial_id(herblore_id);
	level_needed = get_herblore_level_needed(herblore_id);

	if (potion_id == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (herblore_level < level_needed)
	{
		snprintf(return_msg, 150, "You must be at least level %d in "
				 "herblore to do that!\n", level_needed);
		packet_send_chatbox_message(c, return_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Handle making the potion now.  */
	transform_item_no_refresh(c, vial_slot, potion_id); 
	delete_item_from_inv_slot(c, ingredient_slot, 1);

	/* Send the finish message when done.  */
	/* TODO: This may not be 100% accurate, double check.  */
	player_play_animation_no_overwrite(c, HERBLORE_EMOTE_MIXING, 3);
	snprintf(return_msg, 150, "You mix the %s into your potion.", 
			 string_to_lower(ITEMS[ingredient_id]->name));
	packet_send_chatbox_message(c, return_msg);

	/* Adding an herb to a vial of water/coconut milk gives 1 xp as far as I
	 * could find.  */
	add_skill_xp(c, HERBLORE_SKILL, get_herblore_xp(herblore_id));
}
