/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NPC_H_
#define _NPC_H_

/**
 * @brief NPC definition information.
 */
struct npc
{
	/** Id of the NPC.  */
	int id;
	/** NPC's name.  */
	char name[30];
	/** NPC's examine description.  */
	char desc[100];
	/** Combat level of the NPC.  */
	int level;
	/** How big the npc is, size by size squares.  */
	int size;
	/** If the npc is male (0) or female (1).  */
	int sex;
	/** 0 - can't be attacked, 1 - can be.  */
	int attackable;
	/** 
	 * 0 - doesn't attack unless hit first
	 * 1 - attacks players within range based on their level.
	 * 		(If player's level is less than npc level * 2)
	 * 2 - attacks players within range regardless of their level.
	 */
	int aggressive;
	/** Max distance to initiate an attack on a player from.  */
	int aggressive_distance;
	/** 0 - doesn't retreat when low on health. 1 - does.  */
	int retreats;
	/** 0 - doesn't retaliate when hit, 1 - does.  */
	int retaliates;
	/** Distance that the NPC wanders around.  */
	int wander_distance;
	/** 0 - is not undead, 1 - is.  */
	int undead;
	/** 0 - NPC does not do poison damage, 1 - does.  */
	int poisonous;
	/** 0 - NPC prefers to use ranged attacks, 1 - no preference.  */
	int prefer_ranged;
	/** How many ticks it takes for the NPC to respawn.  */
	int respawn_ticks;
	/** How many hit points the NPC has.  */
	int health;
	
	/**
	 * 0 - Max hit,
	 * 1 - Atk speed,
	 * 2 - Atk anim,
	 * 3 - Atk gfx,
	 * 4 - Gfx height,
	 * 5 - Damage type
	 */
	int melee_stats[6];
	/** How many squares away can their target be to attack in melee.  */
	int melee_range;
	/**
	 * 0 - Max hit,
	 * 1 - Atk speed,
	 * 2 - Atk anim,
	 * 3 - Atk gfx,
	 * 4 - Gfx height,
	 * 5 - Damage type
	 */
	int ranged_stats[6];
	
	/** Holds ids of spells that can be cast.  */
	int magic_spells[5];
	/** Number of spells that can be cast.  */
	int num_spells;
	/** Chance out of 100 to cast a spell, used for when an NPC has both a
	 * melee attack, and magic attacks.  */
	int chance_to_use_magic;
	/** How many squares away can their target be to attack with magic.  */
	int magic_range;

	/** If they attack with ranged, what id is their projectile.  */
	int ranged_projectile_id;
	/** How many squares away can their target be to attack with ranged.  */
	int max_ranged;
	
	/**@{*/
	/** Animation ids played at different times.  */
	int blocking_emote;
	int stand_anim;
	int walk_anim;
	int turn_about_anim;
	int turn_cw_anim;
	int turn_ccw_anim;
	int death_emote;
	/**@}*/

	/** Ticks until the NPC is removed when died.  */
	int death_timer;

	/**@{*/
	/** NPC skills.  */
	int atk_skill;
	int str_skill;
	int def_skill;
	int range_skill;
	int magic_skill;
	/**@}*/

	/** Stat bonuses.  */
	int npc_bonus[10];
	/** Attack bonus.  */
	int atk_bonus;
	/** Strength bonus.  */
	int str_bonus;
	/** Strength value for ranged attacks.  */
	int ranged_strength;
	
	/** Stores the index for each table the NPC uses for quick access. Can just
	 * punch this into LOOT_TABLES[] and use the data.  */
	int loot_table_indexes[MAX_NPCS_LOOT_TABLES];
	/** Stores the index for each table the NPC uses for quick access. Can just
	 * punch this into LOOT_TABLES[] and use the data.  */
	int thieving_loot_indexes[MAX_NPCS_THIEVING_TABLES];

	/** Stores the index for the always table the NPC uses. Every item on this
		kind of table is dropped.  */
	int always_loot_index;
	/** Stores the index for the preroll type table for the NPC. In this type,
		instead of a roulete wheel style roll for items like a normal loot
		table, each item on it is rolled for individually. If the player gets
		a hit on this, no main drop is rolled. Otherwise, the other drop tables
		are rolled for next.  */
	int preroll_loot_index;

	/** Name of the loot table that the NPC will always drop its contents on 
	 * death.  */
	char always_loot_table_name[100];
	/** Name of the loot table that is prerolled before all the main drops.
	 */
	char preroll_loot_table_name[100];

	/** Stores the name for each loot table the NPC has.  */
	char loot_table_names[MAX_NPCS_LOOT_TABLES][100];
	/** Stores the name for each thieving loot table the NPC has.  */
	char thieving_loot_table_names[MAX_NPCS_THIEVING_TABLES][100];
	
	/** If this NPC acts as a guard when thieving from a stall.  */
	int thieving_guard;

	/** Objects that this NPC will protect. Currently used only for detecting
	 * who owns a stall.  */
	int objects_protected[MAX_NPCS_OBJECTS_PROTECTED];

	/** If this NPC runs a shop, add it here.  */
	int shop_id;
};

/**
 * @brief Holds information for when an npc is spawned in the world.
 */
struct npc_spawn
{
	/** This struct's position in the npc_spawns array.  */
	int index;
	/** What npc id this instance started out as.  */
	int spawn_id;
	/** What npc id is this spawn.  */
	int npc_id;
	/** If this is treated as undead or not.  */
	int undead;
	/** World x position.  */
	int world_x;
	/** World y position.  */
	int world_y;
	/** World z position.  */
	int world_z;
	/** World x spawn location.  */
	int spawn_x;
	/** World y spawn location.  */
	int spawn_y;
	/** World z spawn location.  */
	int spawn_z;

	/** Used if a new npc is spawned, have to wait a tick before placing them
	 * within player locations.  */
	int teleport_timer;
	/** Will be used for special effects when spawned in, like smoke for the
	 * genie random event.  */
	int teleport_type;
	/**@{*/
	/** Used if the npc needs to teleport somewhere after time has passed.  */
	int stored_tele_x;
	int stored_tele_y;
	int stored_tele_z;
	/**@}*/

	/**@{*/
	/** Used to keep track of the last location of the NPC's combat target.
	 * Helps ensure movement and range updates are only done when needed.
	 */
	int target_x;
	int target_y;
	/**@}*/

	/**@{*/
	/** Coordinates for npc movement.  */
	int walk_to_x;
	int walk_to_y;
	/**@}*/

	/**@{*/
	/** Holds coords for npc movement.  */
	int walk_queue_x[NPC_WALK_QUEUE_SIZE];
	int walk_queue_y[NPC_WALK_QUEUE_SIZE];
	/**@}*/

	/** What position the walking queue is being read at.  */
	int walk_queue_ptr;

	/** What position the walking queue is being read at.  */
	int walk_queue_last_index;
	
	/** Last direction that the npc walked.  */
	int last_walked_direction;
	/** Timer for when the npc should walk again, used for random 
	 * wandering.  */
	int last_movement;

	/** Current HP.  */
	int health;
	/** Max HP.  */
	int max_health;
	/** Combat level.  */
	int level;
	/** If players can attack this npc or not.  */
	int attackable;
	/** If this npc will seek out players to attack.  */
	int aggressive;
	/** If attacked, if this npc will fight back.  */
	int retaliates;

	/**@{*/
	/** Used for hit updates, showing the "hit splats".  */
	int hit_diff;
	int hit_diff_effect;
	int hit_diff_from;
	int hit_diff_index;

	int hit_diff2;
	int hit_diff2_effect;
	int hit_diff2_from;
	int hit_diff2_index;

	int poison_hit;
	int poison_hit2;
	/**@{*/
	
	/** How many spaces reach the npc can hit in melee.  */
	int melee_range;
	
	int poison_damage;
	/* When poisoned, starts at 400, reduced by one each tick. When it hits
	   300, 200, 100, 0, deals poison_damage. When it hits 0, reduce 
	   poison_damage by one and reset this to 400 if poison_damage is not at 0
	   now.  */
	int poison_timer;

	/** Ticks before the npc changes back to their original ID.  */
	int transform_timer;
	
	/** Queue of attacks dealt to the npc.  */
	int hit_queued[HIT_QUEUE_SIZE];
	/** Used with hit_queued, ticks before the attack is applied.  */
	int hit_delay[HIT_QUEUE_SIZE];
	/** Effect dealt to the player when the hit is applied.  */
	int hit_effect[HIT_QUEUE_SIZE];
	/** If the hit is from a player or an npc. 0 - npc, 1 - player.  */ 
	int hit_from[HIT_QUEUE_SIZE];
	/** Index of the npc/player struct the attack is from.  */ 
	int hit_index[HIT_QUEUE_SIZE];

	/** Used to track the farthest a search needs to go when looking for hit
	 * updates.  */
	int highest_hit_offset;

	/**@{*/
	/** Play a graphic when hit if needed.  */
	int hit_gfx;
	int hit_gfx_height;
	int hit_gfx_delay;
	/**@}*/

	/**@{*/
	/** Controls when the npc's health bar gets updated.  */
	int need_hp_update;
	int need_hp_update2;
	/**@}*/

	/** 
	 * Holds id of ranged ammo to check if it will drop.
	 * Used for when the the player uses ranged attacks, and arrows have a
	 * chance of being able to be recovered.
	 */
	int ammo_to_drop;

	/**@{*/
	/** Percentage values for how much a npc's stat is reduced when
	 * cursed, and how long it will last.  */
	float atk_reduced;
	int atk_reduced_ticks;
	float str_reduced;
	int str_reduced_ticks;
	float def_reduced;
	int def_reduced_ticks;
	/**@}*/

	/** Time the npc can't move from a freeze, vine spell.  */
	int frozen_ticks;

	/** If npc is alive, dead, or respawning.  */
	char dead;
	/** If the death animation has been played or not.  */
	int played_death_animation;
	/** How long until the corpse is removed once the npc is killed.  */
	int death_timer;
	/** Takes from death_timer, is decremented until 0 when the npc is removed
	 * from the game world.  */
	int tick_killed;

	/** Does the npc walk by default.  */
	int default_walks;
	/** If npc wanders around or not.  */
	int walks;
	/** How many ticks to stop moving for.  */
	int walks_stop_ticks;
	/** If npc is currently walking to a specific point or not.  */
	int walking_to_point;
	/** If npc is walking to their spawn or not.  */
	int walking_to_home;
	/** Direction the npc is currently facing.
	 * @todo Rename this to something less ambiguous.  */
	int direction;
	/** If npc is not set to move, but face a certain direction, this holds
	 * that direction.  */
	int spawn_facing_direction;

	/** ID of the mobile for the NPC to face.  */
	int face_mob;
	
	/**@{*/
	/** For telling the npc to turn and face a specific point in the world.  */
	int focus_point_x;
	int focus_point_y;
	/**@}*/
	
	/** AI state the NPC is in.  */
	int ai_state;

	/** If npc is fighting something.  */
	int in_combat;
	/** If npc is being talked to or not.  */
	int in_dialogue;
	/** If npc prefers to use ranged attacks or not.  */
	int prefer_ranged;
	
	/**
	 * How is the npc attacking?
	 * 0 - melee
	 * 1 - ranged
	 * 2 - magic
	 */
	int attack_style;

	/** Damage type used below.
	 * @todo May not be used or needed, investigate.
	 */
	int damage_type;
	/**@{*/
	/** 
	 * NPC attack values.
	 * 0 - Max hit,
	 * 1 - Atk speed,
	 * 2 - Atk anim,
	 * 3 - Atk gfx,
	 * 4 - Atk gfx height,
	 * 5 - Damage type
	 */
	int melee_stats[6];
	int ranged_stats[6];
	/**@}*/
	
	/** Holds ids of spells that can be cast.  */
	int magic_spells[5];
	/** Number of spells they can use.  */
	int num_spells;
	/** Chance out of 100 to cast a spell, used for when an NPC has both a
	 * melee attack, and magic attacks.  */
	int chance_to_use_magic;
	/** How far away can they be for their spells?  */
	int magic_range;

	/** ID of the ranged projectile graphic, if any.  */
	int ranged_projectile_id;
	/** Max distance they can use ranged attacks from.  */
	int max_ranged;
	/** Delay until projectile appears.  */
	int projectile_delay;
	/** Is it a magic projectile, or a ranged projectile?
	 * @todo May not be needed or used, invesitage.  */
	int projectile_type;

	/** Index of the player they are attacking.  */
	int targeted_player;
	/** Index of the npc they are attacking.  */
	int targeted_npc;

	/**@{*/
	/** Coordinates of the target, used to track position.  */
	int targeted_location_x;
	int targeted_location_y;
	/**@}*/

	/** Tracks if the NPC's movement needs to be updated. 
	 * -1 - Not pursuing.
	 *  0 - Pursuing.
	 *  Higher than 0 - Ticks until pursuit.  */
	int pursue_target;

	/**@{*/
	/** Combat emotes, animations they play.  */
	int attacking_emote;
	int blocking_emote;
	int death_emote;
	/**@}*/

	/**@{*/
	/** NPC combat stats.  */
	int atk_skill;
	int str_skill;
	int def_skill;
	int range_skill;
	int magic_skill;
	int npc_bonus[10];
	int atk_bonus;
	int str_bonus;
	int ranged_strength;
	/**@}*/

	/** Time until next attack.  */
	int last_attack;
	
	/** Holds number of ticks until respawn.  */
	int respawn;
	/** Will the npc be removed when killed, or respawn?  */
	int will_respawn;
	/** Copies number from respawn, counts down until 0, then respawns.  */
	int respawn_ticks;

	/** Tracks if the npc needs to update their appearace, animations, etc.  */
	int update_masks[11];
	/** When set to 1, check the flags in update_masks and update if 
	 * needed.  */
	int update_required;

	/** Animation to play.  */
	int animation_request;
	/** Delay until animation will play.  */
	int animation_wait_cycles;

	/** GFX to play.  */
	int gfx;
	/** What height the gfx is playing at.  */
	int gfx_height;

	/** Holds NPC text for display above head.  */
	char chat_text[CHAT_TEXT_SIZE];
};


void set_npc_initial_state(struct npc_spawn *npc);

void set_npc_ai_state(struct npc_spawn *npc, int new_state);
int is_npc_dead(struct npc_spawn *npc);

void apply_npc_spawn_defaults(struct npc_spawn *npc);

void update_npc(struct client *c);
int is_npc_within_viewpoint_distance(struct client *c, struct npc_spawn *npc);
void update_npc_movement(struct client *c, struct npc_spawn *npc);

void turn_npc_to_coord(struct npc_spawn *npc, int x, int y);
void turn_npc_to_current_direction(struct npc_spawn *npc);
void npc_face_player(struct npc_spawn *npc, int player_idx);
void npc_face_npc(struct npc_spawn *npc, int npc_idx);
void reset_npc_face_mob(struct npc_spawn *npc);
void move_npc_to_next_point(struct npc_spawn *npc);
void get_npc_movement_direction(struct npc_spawn *npc);

void clear_npc_update_flags(struct npc_spawn *npc);
void set_npc_animation(struct npc_spawn *npc, int emote);
void reset_npc_animation(struct npc_spawn *npc); 
void set_npc_gfx(struct npc_spawn *npc, int gfx);
void set_npc_gfx_height(struct npc_spawn *npc, int gfx, int height);
void set_npc_overhead_text(struct npc_spawn *npc, char string[]);

void process_npc_stats(struct npc_spawn *npc);

void clear_npc_walking_queue(struct npc_spawn *npc);
int add_to_npc_walk_queue(struct npc_spawn *npc, int x, int y);
int npc_walk_from_queue(struct npc_spawn *npc);

void adjust_large_npc_path(int npc_x, int npc_y, int npc_size, 
						   int *t_x, int *t_y);
int set_npc_path(struct npc_spawn *npc, int x0, int y0, 
										int x1, int y1, int height,
										int include_last); 

int get_npc_random_spell_index(struct npc_spawn *npc);
int get_npc_atk_range(struct npc_spawn *npc);

int is_npc_able_to_use_ranged_attack(struct npc_spawn *npc, int target);
void process_npc_damage(struct npc_spawn *npc);
void player_process_poison_damage_npc(struct npc_spawn *npc);
void process_npc_movement(struct npc_spawn *npc);
void process_npc_ai(struct npc_spawn *npc);
void transform_npc(struct npc_spawn *npc, int to_id, int duration);
void process_npc_timed_teleport(struct npc_spawn *npc);
void process_npc_transformation(struct npc_spawn *npc);
int get_index_of_npc_at_world_coord(int npc_x, int npc_y, int npc_z);

int spawn_new_npc(int npc_id, int spawn_x, int spawn_y, int spawn_z, int walks, 
				  int will_respawn, int timer);
#endif
