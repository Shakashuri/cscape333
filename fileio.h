/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FILEIO_H_
#define _FILEIO_H_

int is_directory_present(char pathname[]);
int create_dir(char pathname[]);

int save_player_data(struct client *c);
int save_player_data_new(struct client *c);
int load_player_data(struct client *c);

int load_spell_data(void);
int load_item_definitions(void);
int load_item_spawns(void);
int load_object_definitions(void);
int load_object_spawns(void);
int load_npc_definitions(void);
int load_npc_spawns(void);


int load_region_list(void);
int load_loot_table_definitions(void);
int load_interfaces(void);
int load_config_data(void);

int load_agility_data(void);

int load_shop_data(void);
int load_quest_data(void);
int load_music_data(void);

#endif
