/*  CScape333
 *  Copyright (C) 2015-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file fileio.c
 * @brief Contains functions for creating files, loading data, saving data.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <zlib.h>

#include "constants.h"
#include "audio.h"
#include "csv.h"
#include "client.h"
#include "debug.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "object.h"
#include "player.h"
#include "quests.h"
#include "regions.h"
#include "shops.h"
#include "skills.h"
#include "skill_agility.h"
#include "skill_magic.h"

/**
 * Checks if a directory exists.
 * @param pathname[] Path to check.
 * @return 0 on directory not found, or not a directory
 * @return 1 on directory found
 */
int is_directory_present(char pathname[])
{
	struct stat info;
	
	/* Can't stat the directory, doesn't exist.  */
	if (stat(pathname, &info) != 0)
	{
		return 0;
	}
	/* Directory *does* exist, return 1.  */
	if (S_ISDIR(info.st_mode) != 0)
	{
		return 1;
	}
	/* Something else happened, not a directory.  */
	else
	{
		return 0;
	}
}

/**
 * Creates a directory.
 * @param pathname[] Path to create directory at.
 * @return 0 on successful directory creation, -1 on failure.
 * @return on -1, errno will be set.
 */
int create_dir(char pathname[])
{
	return mkdir(pathname, 0755);
}

/**
 * Returns the length of a file in bytes.
 * @param *fp File pointer.
 * @return Length of the file in bytes.
 */
int get_file_length(FILE *fp)
{
	int len = -1;

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);

	return len;
}

/**
 * Returns the number of lines in a file.
 * @param *fp File pointer.
 * @return Number of lines in the file.
 */
int get_file_line_count(FILE *fp)
{
	int num_newlines = 0;
	int read_ch;

	while ((read_ch = fgetc(fp)) != EOF)
	{
		if (read_ch == '\n')
		{
			num_newlines++;
		}
	}
	rewind(fp);

	return num_newlines;
}

/**
 * Saves player data to a text file.
 * @param *c Client instance to save.
 * @return 1 on failure
 * @return Returns value of fclose at end, 0 if success.
 */
int save_player_data(struct client *c)
{
	int x;
	FILE *fp;
	
	fp = fopen(c->plr->save_location, "w");

	if (fp == NULL)
	{
		printf("Could not open file %s for saving!\n", c->plr->save_location);
		return 1;
	}
	
	fprintf(fp, "%d\n", PLAYER_SAVE_VERSION);
	fprintf(fp, "%s\n", c->plr->name);
	for (x = 0; x < 13; x++)
	{
		c->plr->file_name[x] = to_lower(c->plr->name[x]);
	}

	fprintf(fp, "%s\n", c->plr->password);
	fprintf(fp, "%d\n", c->plr->player_rights);
	fprintf(fp, "%d\n", c->plr->sex);

	fprintf(fp, "%d,%d,%d\n", c->plr->world_x, c->plr->world_y, 
			c->plr->world_z);
	fprintf(fp, "%d,%d,%d\n", c->plr->home_x, c->plr->home_y, c->plr->home_z);
	fprintf(fp, "%d\n", c->plr->running_energy);
	fprintf(fp, "%d\n", c->plr->spellbook);
	fprintf(fp, "%d\n", c->plr->will_retaliate);
	fprintf(fp, "%d\n", c->plr->accepts_aid);

	fprintf(fp, "# Player appearance\n");
	for (x = 0; x < 12; x++)
	{
		fprintf(fp, "%d\n", c->plr->player_look[x]);
	}

	fprintf(fp, "# Player skills with xp\n");
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fprintf(fp, "%d\t%f\n", c->plr->level[x], c->plr->xp[x]);
	}

	fprintf(fp, "# Player temp skill bonsues\n");
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fprintf(fp, "%d\n", c->plr->temp_bonuses[x]);
	}

	fprintf(fp, "# Player items with amounts, charges\n");
	for (x = 0; x < INV_SIZE; x++)
	{
		fprintf(fp, "%d\t%d\t%d\n", c->plr->items[x], 
									c->plr->items_n[x],
									c->plr->items_charges[x]);
	}
	fprintf(fp, "# Player equipment with amounts, charges\n");
	for (x = 0; x < EQUIP_SIZE; x++)
	{
		fprintf(fp, "%d\t%d\t%d\n", c->plr->equipment[x], 
									c->plr->equipment_n[x],
									c->plr->equipment_charges[x]);
	}
	fprintf(fp, "# Skill related variables\n");
	fprintf(fp, "%d\n", c->plr->grain_bins_flour_count);
	fprintf(fp, "%d\n", c->plr->grain_hopper_count);
	
	fprintf(fp, "# Player bank with amounts, charges\n");
	for (x = 0; x < PLAYER_BANK_SIZE; x++)
	{
		fprintf(fp, "%d\t%d\t%d\n", c->plr->bank_items[x], 
									c->plr->bank_items_n[x],
									c->plr->bank_items_charges[x]);
	}

	fprintf(fp, "# Quest progress\n");
	fprintf(fp, "%d\n", c->plr->quest_points);

	for (x = 0; x < PROTOCOL_NUM_QUESTS; x++)
	{
		fprintf(fp, "%d\t%d\t%d\t%d\t%d\t%d\n", 
				c->plr->quests[x],
				c->plr->quest_data[x][0],
				c->plr->quest_data[x][1],
				c->plr->quest_data[x][2],
				c->plr->quest_data[x][3],
				c->plr->quest_data[x][4]
				);
	}
	fprintf(fp, "# Music unlocked\n");
	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		fprintf(fp, "%d\n", c->plr->unlocked_music[x]);
	}

	return fclose(fp);
}

/**
 * Saves player data to a text file.
 * @param *c Client instance to save.
 * @return 1 on failure
 * @return Returns value of fclose at end, 0 if success.
 */
int save_player_data_new(struct client *c)
{
	int x;
	FILE *fp;
	
	fp = fopen(c->plr->save_location, "w");

	if (fp == NULL)
	{
		printf("Could not open file %s for saving!\n", c->plr->save_location);
		return 1;
	}
	
	fprintf(fp, "save_version=%d\n", PLAYER_SAVE_VERSION);
	fprintf(fp, "name=%s\n", c->plr->name);

	for (x = 0; x < 13; x++)
	{
		c->plr->file_name[x] = to_lower(c->plr->name[x]);
	}

	fprintf(fp, "password=%s\n", c->plr->password);
	fprintf(fp, "player_rights=%d\n", c->plr->player_rights);
	fprintf(fp, "sex=%d\n", c->plr->sex);

	fprintf(fp, "world_x=%d\n", c->plr->world_x);
	fprintf(fp, "world_y=%d\n", c->plr->world_y);
	fprintf(fp, "world_z=%d\n", c->plr->world_z);
	fprintf(fp, "home_x=%d\n", c->plr->home_x);
	fprintf(fp, "home_y=%d\n", c->plr->home_y);
	fprintf(fp, "home_z=%d\n", c->plr->home_z);

	fprintf(fp, "running_energy=%d\n", c->plr->running_energy);
	fprintf(fp, "spellbook=%d\n", c->plr->spellbook);
	fprintf(fp, "will_retaliate=%d\n", c->plr->will_retaliate);
	fprintf(fp, "accepts_aid=%d\n", c->plr->accepts_aid);

	fprintf(fp, "# Player appearance\n");
	for (x = 0; x < 12; x++)
	{
		fprintf(fp, "player_look_%d=%d\n", x, c->plr->player_look[x]);
	}

	fprintf(fp, "# Player skills with xp\n");
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fprintf(fp, "skill_%d_level=%d\n", x, c->plr->level[x]);
		fprintf(fp, "skill_%d_xp=%f\n", x, c->plr->xp[x]);
	}

	fprintf(fp, "# Player temp skill bonsues\n");
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fprintf(fp, "skill_%d_temp_bonus=%d\n", x, c->plr->temp_bonuses[x]);
	}

	fprintf(fp, "# Player items with amounts, charges\n");
	for (x = 0; x < INV_SIZE; x++)
	{
		fprintf(fp, "item_%d_id=%d\n", x, c->plr->items[x]);
		fprintf(fp, "item_%d_num=%d\n", x, c->plr->items_n[x]);
		fprintf(fp, "item_%d_charges=%d\n", x, c->plr->items_charges[x]);
	}
	fprintf(fp, "# Player equipment with amounts, charges\n");
	for (x = 0; x < EQUIP_SIZE; x++)
	{
		fprintf(fp, "equipment_%d_id=%d\n", x, c->plr->equipment[x]);
		fprintf(fp, "equipment_%d_num=%d\n", x, c->plr->equipment_n[x]);
		fprintf(fp, "equipment_%d_charges=%d\n", x, c->plr->equipment_charges[x]);
	}
	fprintf(fp, "# Skill related variables\n");
	fprintf(fp, "grain_bins_flour_count=%d\n", c->plr->grain_bins_flour_count);
	fprintf(fp, "grain_hopper_count=%d\n", c->plr->grain_hopper_count);
	
	fprintf(fp, "# Player bank with amounts, charges\n");
	for (x = 0; x < PLAYER_BANK_SIZE; x++)
	{
		fprintf(fp, "bank_items_%d_id=%d\n", x, c->plr->bank_items[x]);
		fprintf(fp, "bank_items_%d_num=%d\n", x, c->plr->bank_items_n[x]);
		fprintf(fp, "bank_items_%d_charges=%d\n", x, c->plr->bank_items_charges[x]);
	}

	fprintf(fp, "# Quest progress\n");
	fprintf(fp, "quest_points=%d\n", c->plr->quest_points);

	for (x = 0; x < PROTOCOL_NUM_QUESTS; x++)
	{
		fprintf(fp, "quest_%d_status=%d\n", x, c->plr->quests[x]);
		fprintf(fp, "quest_%d_data_0=%d\n", x, c->plr->quest_data[x][0]);
		fprintf(fp, "quest_%d_data_1=%d\n", x, c->plr->quest_data[x][1]);
		fprintf(fp, "quest_%d_data_2=%d\n", x, c->plr->quest_data[x][2]);
		fprintf(fp, "quest_%d_data_3=%d\n", x, c->plr->quest_data[x][3]);
		fprintf(fp, "quest_%d_data_4=%d\n", x, c->plr->quest_data[x][4]);
	}
	fprintf(fp, "# Music unlocked\n");
	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		fprintf(fp, "unlocked_music_%d=%d\n", x, c->plr->unlocked_music[x]);

	}

	return fclose(fp);
}

/**
 * Load data from a player's save file.
 * @param *c Client instance.
 * @return 1 on failure
 * @return 0 on success
 */
int load_player_data(struct client *c)
{
	char buffer[80] = {0};
	int x, file_version = 0;
	FILE *fp;
	
	fp = fopen(c->plr->save_location, "r");

	if (fp == NULL)
	{
		printf("Could not open %s for loading!\n", c->plr->save_location);
		return 1;
	}

	if (fscanf(fp, "%d\n", &file_version) != 1)
	{
		printf("save has no version!\n");
		file_version = 0;
		rewind(fp);
	}
	else if (file_version != PLAYER_SAVE_VERSION)
	{
		printf("Save is version %d, current is %d.\n", file_version, 
			   PLAYER_SAVE_VERSION);
		printf("Save file will be updated to latest version on next save.\n");
	}

	fscanf(fp, "%13s\n", c->plr->name);
	fscanf(fp, "%13s\n", c->plr->password);
	fscanf(fp, "%d\n", &c->plr->player_rights);
	fscanf(fp, "%d\n", &c->plr->sex);
	
	fscanf(fp, "%d,%d,%d\n", &c->plr->tele_x, &c->plr->tele_y, 
			&c->plr->tele_z);
	fscanf(fp, "%d,%d,%d\n", &c->plr->home_x, &c->plr->home_y, 
		   &c->plr->home_z);
	fscanf(fp, "%d\n", &c->plr->running_energy);
	fscanf(fp, "%d\n", &c->plr->spellbook);
	fscanf(fp, "%d\n", &c->plr->will_retaliate);
	fscanf(fp, "%d\n", &c->plr->accepts_aid);

	/* The fgets calls are used to get the comments.  */
	fgets(buffer, 80, fp);	
	for (x = 0; x < 12; x++)
	{
		fscanf(fp, "%d\n", &c->plr->player_look[x]);
	}

	fgets(buffer, 80, fp);
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fscanf(fp, "%d\t%f\n", &c->plr->level[x], &c->plr->xp[x]);
		c->plr->base_level[x] = get_level_for_xp(c->plr->xp[x]);
	}

	fgets(buffer, 80, fp);
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		fscanf(fp, "%d\n", &c->plr->temp_bonuses[x]);
	}

	fgets(buffer, 80, fp);
	for (x = 0; x < INV_SIZE; x++)
	{
		fscanf(fp, "%d\t%d\t%d\n", &c->plr->items[x], 
								   &c->plr->items_n[x],
								   &c->plr->items_charges[x]);

		/* Ensure we don't have incorrect values for number of items here.  */
		if (c->plr->items_n[x] < 0)
		{
			c->plr->items_n[x] = 0;
		}
	}
	fgets(buffer, 80, fp);
	for (x = 0; x < EQUIP_SIZE; x++)
	{
		fscanf(fp, "%d\t%d\t%d\n", &c->plr->equipment[x], 
								   &c->plr->equipment_n[x],
								   &c->plr->equipment_charges[x]);

		/* Ensure we don't have incorrect values for number of items here.  */
		if (c->plr->equipment_n[x] < 0)
		{
			c->plr->equipment_n[x] = 0;
		}
	}

	fgets(buffer, 80, fp);
	fscanf(fp, "%d\n", &c->plr->grain_bins_flour_count);
	fscanf(fp, "%d\n", &c->plr->grain_hopper_count);
	
	fgets(buffer, 80, fp);
	for (x = 0; x < PLAYER_BANK_SIZE; x++)
	{
		fscanf(fp, "%d\t%d\t%d\n", &c->plr->bank_items[x], 
								   &c->plr->bank_items_n[x],
								   &c->plr->bank_items_charges[x]);

		/* Ensure we don't have incorrect values for number of items here.  */
		if (c->plr->bank_items_n[x] < 0)
		{
			c->plr->bank_items_n[x] = 0;
		}
	}

	if (file_version >= 1)
	{
		/* Version 3 has quest variable storage, quest points.  */
		if (file_version >= 3)
		{
			fgets(buffer, 80, fp);
			fscanf(fp, "%d\n", &c->plr->quest_points);

			for (x = 0; x < PROTOCOL_NUM_QUESTS; x++)
			{
				fscanf(fp, "%d\t%d\t%d\t%d\t%d\t%d\n", &c->plr->quests[x],
					   &c->plr->quest_data[x][0],
					   &c->plr->quest_data[x][1],
					   &c->plr->quest_data[x][2],
					   &c->plr->quest_data[x][3],
					   &c->plr->quest_data[x][4]
					   );

				/* Ensure we don't have incorrect values for number of items
				 * here.  */
				if (c->plr->quests[x] < 0)
				{
					c->plr->quests[x] = 0;
				}
			}
		}
		else
		{
			fgets(buffer, 80, fp);
			for (x = 0; x < PROTOCOL_NUM_QUESTS; x++)
			{
				fscanf(fp, "%d\n", &c->plr->quests[x]);

				/* Ensure we don't have incorrect values for number of items
				 * here.  */
				if (c->plr->quests[x] < 0)
				{
					c->plr->quests[x] = 0;
				}
			}
		}
	}
	if (file_version >= 2)
	{
		fgets(buffer, 80, fp);
		for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
		{
			fscanf(fp, "%d\n", &c->plr->unlocked_music[x]);

			/* Ensure we don't have incorrect values for number of items 
			 * here.  */
			if (c->plr->unlocked_music[x] < 0)
			{
				c->plr->unlocked_music[x] = 0;
			}
		}
	}
	
	fclose(fp);

	c->plr->update_required = 1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	return 0;
}

/**
 * Loads spell data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_spell_data(void)
{
	/* Loads up all the spell info from a text file for the server to use.  */
	int count = 0;
	FILE *fp;
	struct spell zeroed_spell = {0};
	int x = 0, y = 0;

	if ((fp = fopen(SPELL_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING SPELL DATA FILE!\n");
		return 1;
	}


	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	/* Number of spells is one less than the number of lines in the database,
	 * due to the comment line at the top.  */
	NUM_SPELLS = (csv->num_lines - 1);

	/* Create the SPELLS array.  */
	SPELLS = calloc(NUM_SPELLS, (sizeof(struct spell*)));

	for (x = 1; x < csv->num_lines; x++)
	{
		struct spell *spell = malloc(sizeof(struct spell));
		*spell = zeroed_spell;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &spell->id);
		strcpy(spell->name, get_csv_token(csv, x, y++));

		sscanf(get_csv_token(csv, x, y++), "%d", &spell->cast_emote);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->cast_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->cast_gfx_height);

		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_angle);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_delay);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_start_height);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_end_height);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->projectile_slope);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->impact_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->impact_gfx_height);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->level_req);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->dmg_type);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->max_dmg);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->cast_ticks);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->tele_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->tele_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->spell_effect);
		sscanf(get_csv_token(csv, x, y++), "%f", &spell->spell_effect_mag);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->duration);
		sscanf(get_csv_token(csv, x, y++), "%f", &spell->xp);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->spellbook);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[6]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[7]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[8]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[9]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[10]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[11]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[12]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->runes_needed[13]);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->item_needed);
		sscanf(get_csv_token(csv, x, y++), "%d", &spell->object_needed);

		SPELLS[count] = spell;
		count++;
	}

	free_csv_file(csv);
	
	printf("Loaded %d spell(s)!\n", count);
	return 0;
}

/**
 * Used to save the new item definition info to a file, used if new values
 * should be loaded into memory, and then applied down the line to the original
 * database file.
 */
void save_item_definitions(void)
{
	int x;
	FILE *fp;
	
	fp = fopen("data/new_price", "w");

	if (fp == NULL)
	{
		printf("Could not open file for item saving!\n");
		return;
	}

	for (x = 0; x < PROTOCOL_MAX_ITEMS; x++)
	{
		fprintf(fp, "%d\t", ITEMS[x]->id);
		fprintf(fp, "%s\t", ITEMS[x]->name);
		fprintf(fp, "%d\t", ITEMS[x]->spec_price);
		fprintf(fp, "%d\t", ITEMS[x]->high_alch);
		fprintf(fp, "%d\n", ITEMS[x]->low_alch);
	}

	fclose(fp);
}


/**
 * Loads items data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_item_definitions(void)
{
	/* Loads up all the items from a textfile database into structs for the
	   server to use.  */
	int x = 0, y = 0, count = 0;
	FILE *fp;
	struct item zeroed_item = {0};

	if ((fp = fopen(ITEM_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING ITEM DEFINITIONS FILE!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	printf("Checking item data for errors...\n");

	for (x = 1; x < csv->num_lines; x++)
	{
		struct item *item = malloc(sizeof(struct item));
		*item = zeroed_item;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &item->id);
		strcpy(item->name, get_csv_token(csv, x, y++));
		strcpy(item->desc, get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &item->slot);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->noted);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->stacking);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->unnoted_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->note_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->base_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->tradable);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bankable);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->spec_price);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->high_alch);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->low_alch);
		sscanf(get_csv_token(csv, x, y++), "%f", &item->weight);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->item_consume_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->item_container_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->display_mask);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[6]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[7]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[8]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[9]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[10]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->bonuses[11]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->ranged_strength);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->ranged);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->reach);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->ammo_type);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->draw_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->shoot_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->two_handed);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->atk_speed);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[6]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[7]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[8]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[9]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[10]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[11]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[12]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[13]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[14]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[15]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[16]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[17]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[18]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[19]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_req[20]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->skill_use_req);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->atk_type);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->stand_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->walk_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->run_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->attacking_emotes[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->attacking_emotes[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->attacking_emotes[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->attacking_emotes[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->block_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->spec_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->self_spec_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->other_spec_gfx);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->spec_bar_id);
		sscanf(get_csv_token(csv, x, y++), "%f", &item->spec_bar_drain);

		check_item_data_for_errors(item, count);
		ITEMS[count] = item;
		count++;
	}

	free_csv_file(csv);
	printf("Loaded %d item(s)!\n", count);

	return 0;
}

/**
 * Loads item drop data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_item_spawns(void)
{
	/* Loads up all the global drops from a textfile into structs for the
	   server to use.  */
	int count = 0, x = 0, y = 0;
	FILE *fp;

	if ((fp = fopen(ITEM_DROP_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING GLOBAL DROPS FILE!\n");
		return 1;
	}
	
	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	/* Loop through all the lines of the CSV, skipping the header line.
	 * For each line, go through and assign the data from it to the item
	 * drop struct.  */
	for (x = 1; x < csv->num_lines; x++)
	{
		struct item_drop *item = malloc(sizeof(struct item_drop));
		apply_item_drop_defaults(item);
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &item->id);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->amount);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->world_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->world_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->world_z);
		sscanf(get_csv_token(csv, x, y++), "%d", &item->timer);
		strcpy(item->note, get_csv_token(csv, x, y++));

		ITEM_DROPS[count] = item;
		count++;
	}

	free_csv_file(csv);

	/* Fills up the rest of the with blank items drops.  */
	for (x = count; x < MAX_ITEM_DROPS; x++)
	{
		struct item_drop *item_addr = malloc(sizeof(struct item_drop));
		apply_item_drop_defaults(item_addr);

		ITEM_DROPS[x] = item_addr;
	}

	printf("Loaded %d global drop(s)!\n", count);
	return 0;
}

/**
 * Loads object data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_object_definitions(void)
{
	int count = 0;
	FILE *fp;
	struct object zeroed_object = {0};
	int x = 0, y = 0, z = 0;

	if ((fp = fopen(OBJECT_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING OBJECT DEFINITIONS FILE!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	for (x = 1; x < csv->num_lines; x++)
	{
		struct object *obj = malloc(sizeof(struct object));
		*obj = zeroed_object;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &obj->id);
		strcpy(obj->name, get_csv_token(csv, x, y++));
		strcpy(obj->desc, get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->size_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->size_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->occupied);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->blocked);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->door_type);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->is_double_door);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->is_open);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->id_diff);
		strcpy(obj->loot_table_names[0], get_csv_token(csv, x, y++));
		strcpy(obj->loot_table_names[1], get_csv_token(csv, x, y++));
		strcpy(obj->loot_table_names[2], get_csv_token(csv, x, y++));
		strcpy(obj->loot_table_names[3], get_csv_token(csv, x, y++));

		check_object_data_for_errors(obj, count);
		OBJECTS[count] = obj;
		count++;
	}

	free_csv_file(csv);

	/* Clear all loot table indexs.  */
	for (x = 0; x < PROTOCOL_MAX_OBJECTS; x++)
	{
		for (y = 0; y < MAX_OBJECTS_LOOT_TABLES; y++)
		{
			OBJECTS[x]->loot_table_indexes[y] = -1;
		}
	}

	/* Now get the indexes for each of the drops.  */
	for (x = 0; x < NUM_LOOT_TABLES; x++)
	{
		if (is_double_about_equal(LOOT_TABLES[x]->total_weights, 0.0, EPSILON))
		{
			continue;
		}

		for (y = 0; y < PROTOCOL_MAX_OBJECTS; y++)
		{
			for (z = 0; z < MAX_OBJECTS_LOOT_TABLES; z++)
			{
				if (strcmp(OBJECTS[y]->loot_table_names[z], 
						   LOOT_TABLES[x]->name) == 0)
				{
					OBJECTS[y]->loot_table_indexes[z] = x;
					/*
					printf("Found table %s (%d) in NPC %s (%d)\n", 
						LOOT_TABLES[x]->name, x, OBJECTS[y]->name, y);
					*/
				}
			}
		}
	}

	printf("Loaded %d object(s)!\n", count);
	return 0;
} 

/**
 * Loads object spawn data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_object_spawns(void)
{
	/* Loads up all the global objects from a textfile into structs for the
	   server to use.  */
	int count = 0, x = 0, y = 0;
	FILE *fp;
	struct obj_spawn zeroed_object = {0};

	if ((fp = fopen(OBJECT_SPAWN_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING OBJECT SPAWNS FILE!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	for (x = 1; x < csv->num_lines; x++)
	{
		struct obj_spawn *obj = malloc(sizeof(struct obj_spawn));
		*obj = zeroed_object;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &obj->id);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->world_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->world_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->world_z);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->face);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->internal_face);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->type);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->should_spawn);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->double_door);
		sscanf(get_csv_token(csv, x, y++), "%d", &obj->open);

		apply_obj_spawn_defaults(obj);
		OBJECT_SPAWNS[count] = obj;
		count++;
	}
	
	/* Fills up the rest of the list.  */
	for (x = count; x < MAX_OBJECT_SPAWNS; x++)
	{
		struct obj_spawn *obj = malloc(sizeof(struct obj_spawn));
		
		obj->id = obj->spawn_id = obj->face = obj->world_x =
		obj->world_y = obj->spawn_x = obj->spawn_y = obj->world_z = 
		obj->type = obj->double_door = obj->open =
		obj->spawn_face = obj->default_open_state = obj->should_spawn = 0;

		OBJECT_SPAWNS[x] = obj;
	}

	free_csv_file(csv);

	printf("Loaded %d object spawn(s)!\n", count);
	return 0;
}

/**
 * Loads npc data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_npc_definitions(void)
{
	/* Loads up all the npc definitions from a textfile database into 
	   structs for the server to use.  */
	int x = 0, y = 0, z = 0, count = 0;
	FILE *fp;
	struct npc zeroed_npc = {0};

	/* Checks if npc def list can be loaded.  */
	if ((fp = fopen(NPC_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING NPC DEFINITIONS FILE!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	for (x = 1; x < csv->num_lines; x++)
	{
		struct npc *npc_addr = malloc(sizeof(struct npc));
		*npc_addr = zeroed_npc;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->id);
		strcpy(npc_addr->name, get_csv_token(csv, x, y++));
		strcpy(npc_addr->desc, get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->level);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->size);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->sex);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->attackable);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->aggressive);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->aggressive_distance);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->retaliates);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->retreats);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->wander_distance);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->undead);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->prefer_ranged);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->poisonous);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->respawn_ticks);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->health);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_stats[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_stats[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_stats[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_stats[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_stats[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->melee_range);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_stats[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_stats[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_stats[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_stats[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_stats[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_projectile_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->max_ranged);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->num_spells);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_range);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->chance_to_use_magic);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_spells[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_spells[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_spells[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_spells[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_spells[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->stand_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->walk_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->turn_about_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->turn_cw_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->turn_ccw_anim);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->blocking_emote);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->death_emote);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->death_timer);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->atk_skill);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->str_skill);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->def_skill);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->range_skill);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->magic_skill);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[4]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[5]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[6]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[7]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[8]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_bonus[9]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->str_bonus);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->ranged_strength);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->atk_bonus);
		strcpy(npc_addr->always_loot_table_name, get_csv_token(csv, x, y++));
		strcpy(npc_addr->preroll_loot_table_name, get_csv_token(csv, x, y++));
		strcpy(npc_addr->loot_table_names[0], get_csv_token(csv, x, y++));
		strcpy(npc_addr->loot_table_names[1], get_csv_token(csv, x, y++));
		strcpy(npc_addr->loot_table_names[2], get_csv_token(csv, x, y++));
		strcpy(npc_addr->thieving_loot_table_names[0], get_csv_token(csv, x, y++));
		strcpy(npc_addr->thieving_loot_table_names[1], get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->thieving_guard);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->objects_protected[0]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->objects_protected[1]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->objects_protected[2]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->objects_protected[3]);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->shop_id);

		check_npc_data_for_errors(npc_addr, count);
		NPCS[count++] = npc_addr;
	}

	/* Clear all loot table indexs.  */
	for (x = 0; x < PROTOCOL_MAX_NPCS; x++)
	{
		for (y = 0; y < MAX_NPCS_LOOT_TABLES; y++)
		{
			NPCS[x]->loot_table_indexes[y] = -1;
		}
		for (y = 0; y < MAX_NPCS_THIEVING_TABLES; y++)
		{
			NPCS[x]->thieving_loot_indexes[y] = -1;
		}

		NPCS[x]->always_loot_index = -1;
		NPCS[x]->preroll_loot_index = -1;
	}

	/* Now get the indexes for each of the drops.  */
	for (x = 0; x < NUM_LOOT_TABLES; x++)
	{
		if (is_double_about_equal(LOOT_TABLES[x]->total_weights, 0.0, EPSILON))
		{
			continue;
		}

		for (y = 0; y < PROTOCOL_MAX_NPCS; y++)
		{
			for (z = 0; z < MAX_NPCS_LOOT_TABLES; z++)
			{
				if (strcmp(NPCS[y]->loot_table_names[z], LOOT_TABLES[x]->name) == 0)
				{
					NPCS[y]->loot_table_indexes[z] = x;
					/*
					printf("Found table %s (%d) in NPC %s (%d)\n", 
						LOOT_TABLES[x]->name, x, NPCS[y]->name, y);
					*/
				}
			}

			for (z = 0; z < MAX_NPCS_THIEVING_TABLES; z++)
			{
				if (strcmp(NPCS[y]->thieving_loot_table_names[z], 
						   LOOT_TABLES[x]->name)
					== 0)
				{
					NPCS[y]->thieving_loot_indexes[z] = x;

					/*
					printf("Found thieving table %s (%d) in NPC %s (%d)\n", 
						LOOT_TABLES[x]->name, x, NPCS[y]->name, y);
					*/
				}
			}

			if (strcmp(NPCS[y]->always_loot_table_name, 
					   LOOT_TABLES[x]->name)
				== 0)
			{
				NPCS[y]->always_loot_index = x;

				/*
				printf("Found always table %s (%d) in NPC %s (%d)\n", 
					LOOT_TABLES[x]->name, x, NPCS[y]->name, y);
				*/
			}

			if (strcmp(NPCS[y]->preroll_loot_table_name, 
					   LOOT_TABLES[x]->name)
				== 0)
			{
				NPCS[y]->preroll_loot_index = x;

				/*
				printf("Found preroll table %s (%d) in NPC %s (%d)\n", 
					LOOT_TABLES[x]->name, x, NPCS[y]->name, y);
				*/
			}
		}
	}

	/* Check to see if any NPCs have stuff in the loot table slot, but there
	 * is no match for it.  */
	for (y = 0; y < PROTOCOL_MAX_NPCS; y++)
	{
		for (z = 0; z < MAX_NPCS_LOOT_TABLES; z++)
		{
			if (strcmp(NPCS[y]->loot_table_names[z], "-1") != 0
				&& NPCS[y]->loot_table_indexes[z] == -1)
			{
				printf("NPC %s (%d) has unmatched table %s!\n", 
					   NPCS[y]->name, y, NPCS[y]->loot_table_names[z]);
			}
		}

		if (strcmp(NPCS[y]->always_loot_table_name, 
				   "-1") != 0
			&& NPCS[y]->always_loot_index == -1)
		{
			printf("NPC %s (%d) has unmatched always table %s!\n", 
				   NPCS[y]->name, y, NPCS[y]->always_loot_table_name);
		}

		if (strcmp(NPCS[y]->preroll_loot_table_name, 
				   "-1") != 0
			&& NPCS[y]->preroll_loot_index == -1)
		{
			printf("NPC %s (%d) has unmatched preroll table %s!\n", 
				   NPCS[y]->name, y, NPCS[y]->preroll_loot_table_name);
		}

	}

	printf("Loaded %d NPC definition(s)!\n", count);
	
	free_csv_file(csv);
	return 0;
}

/**
 * Loads npc spawn data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_npc_spawns(void)
{
	/* Loads up all the npc spawns from a textfile into structs for the
	   server to use.  */
	int count = 0;
	int x = 0, y = 0;
	FILE *fp;
	struct npc_spawn zeroed_npc_spawn = {0};
	char temp_facing[3] = {0};

	if ((fp = fopen(NPC_SPAWN_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING NPC SPAWNS FILE!\n");
		return 1;
	}

	NUM_NPC_SPAWNS = (get_file_line_count(fp) - 1);

	NUM_NPC_SPAWNS += 500;

	NPC_SPAWNS = calloc(NUM_NPC_SPAWNS, sizeof(struct npc_spawn*));

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	for (x = 1; x < csv->num_lines; x++)
	{
		struct npc_spawn *npc_addr = malloc(sizeof(struct npc_spawn));
		*npc_addr = zeroed_npc_spawn;
		y = 0;

		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->npc_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->world_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->world_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->world_z);
		sscanf(get_csv_token(csv, x, y++), "%d", &npc_addr->walks);
		strcpy(temp_facing, get_csv_token(csv, x, y++));

		npc_addr->index = count;

		/* Apply facing direction.  */
		if (temp_facing[0] == 'N')
		{
			if (temp_facing[1] == '\0')
			{
				npc_addr->spawn_facing_direction = NORTH;
			}
			else if (temp_facing[1] == 'W')
			{
				npc_addr->spawn_facing_direction = NORTH_WEST;
			}
			else if (temp_facing[1] == 'E')
			{
				npc_addr->spawn_facing_direction = NORTH_EAST;
			}
		}
		else if (temp_facing[0] == 'S')
		{
			if (temp_facing[1] == '\0')
			{
				npc_addr->spawn_facing_direction = SOUTH;
			}
			else if (temp_facing[1] == 'W')
			{
				npc_addr->spawn_facing_direction = SOUTH_WEST;
			}
			else if (temp_facing[1] == 'E')
			{
				npc_addr->spawn_facing_direction = SOUTH_EAST;
			}
		}
		else if (temp_facing[0] == 'E')
		{
			npc_addr->spawn_facing_direction = EAST;
		}
		else if (temp_facing[0] == 'W')
		{
			npc_addr->spawn_facing_direction = WEST;
		}

		apply_npc_spawn_defaults(npc_addr);
		NPC_SPAWNS[count] = npc_addr;
		check_npc_spawn_data_for_errors(NPC_SPAWNS[count], count);
		count++;
	}
	
	printf("Loaded %d NPC spawn(s)!\n", count);

	if (NUM_NPC_SPAWNS >= MAX_NPC_SPAWNS)
	{
		printf("*****************\n");
		printf("WARNING!\n");
		printf("There are more NPC spawns than the server is set up for!\n");
		printf("Modify the MAX_NPC_SPAWNS variable, or clients WILL crash!\n");
		printf("Num spawns: %d Current MAX_NPC_SPAWNS: %d\n", NUM_NPC_SPAWNS, 
			MAX_NPC_SPAWNS);
		printf("Modify MAX_NPC_SPAWNS in constants.h to be greater than %d at "
			   "least!\n", NUM_NPC_SPAWNS);
		printf("*****************\n");

	}

	free_csv_file(csv);
	return 0;
}


/* Loads all of the objects in the data/obj_locs.tsv file. That contains
   where each object that the player can interact with in the game world.  */
static int load_server_objects(void)
{
	FILE *fp = fopen(OBJECT_LOCATION_LIST, "r");
	char buffer[100] = {0};

	long int start_pos = 0;

	int x = 0, y = 0;
	int total_num_objects = 0, num_objects = 0;
	int obj_x = 0, obj_y = 0, obj_id = 0, obj_reg_x = 0, obj_reg_y = 0;
	int obj_height = 0, obj_face = 0, obj_reg_id = 0, obj_type = 0;
	int cur_id = 0;
	int will_end = 0;

	/* Check if the file loaded.  */
	if (fp == NULL)
	{
		printf("Could not open object location list! Ensure that the file:\n");
		printf("obj_locs.tsv exists in the data folder!\n");
		exit(2);
	}

	/* Remove the comment line.  */
	fgets(buffer, 100, fp);

	/* Now we just need to load all the objects in the server.  */
	do
	{
		/* Get position of the file so far.  */
		start_pos = ftell(fp);
		num_objects = 0;

		/* Read the first actual data line.  */
		fgets(buffer, 100, fp);

		sscanf(buffer, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &obj_id, 
				&obj_type, &obj_reg_id, &obj_reg_x, &obj_reg_y, &obj_x, &obj_y, 
				&obj_height, &obj_face);
		
		/* We will be reading for this region_id.  */
		cur_id = obj_reg_id;

		/* Get the bounds of the objects for a region.  */
		while (obj_reg_id == cur_id)
		{
			if (fgets(buffer, 100, fp) != NULL)
			{	
				sscanf(buffer, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &obj_id, 
						&obj_type, &obj_reg_id, &obj_reg_x, &obj_reg_y, &obj_x, 
						&obj_y, &obj_height, &obj_face);
				
				num_objects++;
			}
			else
			{
				/* This is the last time we need to loop, end after this
				   region's objects have been read.  */
				will_end = 1;
				break;
			}
		}

		/* Get what region struct in the REGION_LIST array contains data
		   for the region we are getting data for.  */
		for (x = 0; x < REGION_LIST_NUM; x++)
		{
			if (REGION_LIST[x]->id == cur_id)
			{
				/* x is found, we will use that.  */
				break;
			}
		}

		/* Allocate memory.  */
		REGION_LIST[x]->OBJECTS = calloc(num_objects, 
										 sizeof(struct obj_spawn_org*));
		REGION_LIST[x]->obj_spawns_size = num_objects;

		fseek(fp, start_pos, SEEK_SET);

		/* Read all of the objects for the region, and put them into 
		   structs.  */
		for (y = 0; y < num_objects; y++)
		{
			fgets(buffer, 100, fp);
			
			sscanf(buffer, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &obj_id, 
					&obj_type, &obj_reg_id, &obj_reg_x, &obj_reg_y, &obj_x, 
					&obj_y, &obj_height, &obj_face);

			REGION_LIST[x]->OBJECTS[y] = malloc(sizeof(struct obj_spawn_org));	
			
			REGION_LIST[x]->OBJECTS[y]->id = obj_id;
			REGION_LIST[x]->OBJECTS[y]->type = obj_type;
			REGION_LIST[x]->OBJECTS[y]->in_reg_x = obj_reg_x;
			REGION_LIST[x]->OBJECTS[y]->in_reg_y = obj_reg_y;
			REGION_LIST[x]->OBJECTS[y]->world_x = obj_x;
			REGION_LIST[x]->OBJECTS[y]->world_y = obj_y;
			REGION_LIST[x]->OBJECTS[y]->world_z = obj_height;
			REGION_LIST[x]->OBJECTS[y]->face = obj_face;
		}

		total_num_objects += num_objects;
	} 
	while (will_end == 0);

	/* Clean up.  */
	fclose(fp);

	return total_num_objects;
}

/** 
 * Opens the file defined in MAP_REGION_LIST that contains a list of every
 * region id used by this cache version. 
 * @return Returns the number of regions loaded if it succeeds.
 */
int load_region_list(void)
{
	FILE *fp = fopen(MAP_REGION_LIST, "r");
	int x = 0, num_regions = 0;
	int highest_x = 0, highest_y = 0, region_id = 0;
	int read_x = 0, read_y = 0;
	int has_music = 0;
	char buffer[100] = {0};

	/* Check files opened correctly.  */
	if (fp == NULL)
	{
		printf("Could not open region list! Ensure that the file:\n");
		printf("region_list.txt exists in the data folder!\n");
		exit(1);
	}

	/* Get size of the region list, minus one from the comment line.  */
	REGION_LIST_NUM = num_regions = (get_file_line_count(fp) - 1);
	
	if (num_regions == -1)
	{
		printf("Incorrect number of lines returned!\n");
		fclose(fp);
		return 0;
	}

	REGION_LIST = calloc(num_regions, sizeof(struct region_data*));
	
	/* Remove comment line.  */
	fgets(buffer, 100, fp);

	while (fgets(buffer, 100, fp) != NULL)
	{
		sscanf(buffer, "%d\t%d\t%d\t%d\n", &region_id, &read_x, &read_y, 
			   &has_music);
	
		REGION_LIST[x] = calloc(1, sizeof(struct region_data));
		
		REGION_LIST[x]->id = region_id;

		/* Convert the region coord to the region x, y pos.  */
		REGION_LIST[x]->world_x = read_x;
		REGION_LIST[x]->world_y = read_y;

		read_x = get_region_coord(read_x);
		read_y = get_region_coord(read_y);

		/* Store it in the region struct.  */
		REGION_LIST[x]->region_x = read_x;
		REGION_LIST[x]->region_y = read_y;

		REGION_LIST[x]->has_music = has_music;
		/* Fill it in later when the music info is loaded.  */
		REGION_LIST[x]->music_id = -1;

		/* Find what the highest region coord is.  */
		if (read_x > highest_x)
		{
			highest_x = read_x;
		}
		
		if (read_y > highest_y)
		{
			highest_y = read_y;
		}

		x++;
	}

	/* Add one to it because that id is what we need to access.  */
	REGION_MAP_SIZE_X = highest_x + 1;
	REGION_MAP_SIZE_Y = highest_y + 1;

	REGION_MAP = calloc(REGION_MAP_SIZE_X, sizeof(struct region_data**));

	for (x = 0; x < REGION_MAP_SIZE_X; x++)
	{
		REGION_MAP[x] = calloc(REGION_MAP_SIZE_Y, sizeof(struct region_data*));
	}

	/* Now, in the 2D map, point the coords to the list.  */
	for (x = 0; x < num_regions; x++)
	{
		int x_pos = REGION_LIST[x]->region_x;
		int y_pos = REGION_LIST[x]->region_y;

		REGION_MAP[x_pos][y_pos] = REGION_LIST[x];
	}

	fclose(fp);

	printf("Loaded %d server object(s)!\n", load_server_objects());
	return num_regions;
}

/**
 * Loads all the data from the loot table file.
 * @return Returns number of tables read.
 */
int load_loot_table_definitions(void)
{
	FILE *fp = NULL;
	struct loot_table zeroed_table = {0};

	double temp_drops[MAX_LOOT_TABLE_DROPS][4];
	double temp_tables[MAX_LOOT_TABLE_SUB_TABLES][4];
	int temp_count = 0;
	int count = 0;

	int line_idx = 0;
	
	int x = 0, y = 0, z = 0, z1 = 0;

	fp = fopen(LOOT_TABLE_LIST, "r");

	if (fp == NULL)
	{
		printf("Failed to load drop table list!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	/* Remove one to account for the header comment.  */
	NUM_LOOT_TABLES = (csv->num_lines - 1);

	LOOT_TABLES = calloc(NUM_LOOT_TABLES, sizeof(struct loot_table*));

	if (LOOT_TABLES == NULL)
	{
		printf("calloc failed, return\n");
		return 2;
	}

	for (line_idx = 1; line_idx < csv->num_lines; line_idx++)
	{
		struct loot_table *drop = malloc(sizeof(struct loot_table));
		*drop = zeroed_table;

		temp_count = 0;
		y = 0;

		strcpy(drop->name, get_csv_token(csv, line_idx, y++));

		for (z1 = 0; z1 < MAX_LOOT_TABLE_DROPS; z1++)
		{
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_drops[z1][0]);
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_drops[z1][1]);
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_drops[z1][2]);
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_drops[z1][3]);
		}

		for (z1 = 0; z1 < MAX_LOOT_TABLE_SUB_TABLES; z1++)
		{
			strcpy(drop->table_names[z1], get_csv_token(csv, line_idx, y++));
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_tables[z1][0]);
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_tables[z1][1]);
			sscanf(get_csv_token(csv, line_idx, y++), "%lf", 
					&temp_tables[z1][2]);
		}

		/* Make sure we have the index of the modified tables that use the ring
		 * of wealth chances.  */
		if (strcmp(drop->name, "RDT_GEM_WEALTH") == 0)
		{
			RDT_GEM_WEALTH_IDX = count;
		}
		else if (strcmp(drop->name, "RDT_MEGA_WEALTH") == 0)
		{
			RDT_MEGA_WEALTH_IDX = count;
		}
		/* Add in these tables for mining, gem chances.  */
		else if (strcmp(drop->name, "MINING_PRECIOUS_GEM_TABLE") == 0)
		{
			MINING_PRECIOUS_GEM_TABLE = count;
		}
		else if (strcmp(drop->name, "MINING_GEM_ROCK_TABLE") == 0)
		{
			MINING_GEM_ROCK_TABLE = count;
		}

		/* Find how many entries are not empty.  */
		for (x = 0; x < MAX_LOOT_TABLE_DROPS; x++)
		{
			if (temp_drops[x][3] != -1.0)
			{
				drop->total_weights += temp_drops[x][3];
				temp_count++;
			}
		}

		/* Create the drops arrays.  */
		drop->drops = calloc(temp_count, sizeof(double*));

		for (x = 0; x < temp_count; x++)
		{
			/* size 4 for id, min, max, and chance.  */
			drop->drops[x] = calloc(4, sizeof(double));

			if (drop->drops[x] == NULL)
			{
				printf("calloc error for drops!\n");
				return 3;
			}
		}

		drop->num_drops = temp_count;

		/* Transfer the data to the drop struct.  */
		for (x = 0; x < drop->num_drops; x++)
		{
			drop->drops[x][0] = temp_drops[x][0];
			drop->drops[x][1] = temp_drops[x][1];
			drop->drops[x][2] = temp_drops[x][2];
			drop->drops[x][3] = temp_drops[x][3];
		}

		/* Find how many table entries aren't empty.  */
		temp_count = 0;

		for (x = 0; x < MAX_LOOT_TABLE_SUB_TABLES; x++)
		{
			if (strcmp(drop->table_names[x], "-1") != 0)
			{
				drop->total_weights += temp_tables[x][2];
				temp_count++;
			}
		}

		drop->num_tables = temp_count;

		drop->tables = calloc(temp_count, sizeof(double*));

		for (x = 0; x < temp_count; x++)
		{
			/* size 4 for index, min, max, and chance.  */
			drop->tables[x] = calloc(4, sizeof(double));

			if (drop->tables[x] == NULL)
			{
				printf("calloc error for tables!\n");
				return 3;
			}
		}

		/* Transfer the data to the tables struct.  */
		for (x = 0; x < drop->num_tables; x++)
		{
			/* We are adding an additional entry to the table array. The first
			 * entry will hold the index for the table in the LOOT_TABLES
			 * array. This offset here is intentional.  */
			drop->tables[x][0] = -1.0;
			drop->tables[x][1] = temp_tables[x][0];
			drop->tables[x][2] = temp_tables[x][1];
			drop->tables[x][3] = temp_tables[x][2];
		}

		LOOT_TABLES[count] = drop;
		count++;
	}

	/* Now handle tables within tables. This loops through all loot tables that
	 * have tables within them, and finds the index of the loot table used.
	 * This makes sure that the string comparison for the names only has to be
	 * done once, and when dropping loot from a table, we can just hop to their
	 * index.
	 */
	for (x = 0; x < NUM_LOOT_TABLES; x++)
	{
		for (y = 0; y < LOOT_TABLES[x]->num_tables; y++)
		{
			for (z = 0; z < NUM_LOOT_TABLES; z++)
			{
				if (strcmp(LOOT_TABLES[x]->table_names[y], 
					LOOT_TABLES[z]->name) == 0)
				{
					LOOT_TABLES[x]->tables[y][0] = z;
				}
			}
		}
	}

	printf("Loaded %d loot table(s)!\n", NUM_LOOT_TABLES);

	free_csv_file(csv);

	return 0;
}

/**
 * Loads the list of interface id's into memory, used for anti-cheat. When
 * a player clicks an interface button, the server uses this data to check
 * if that button is part of an interface they have open. If not, they must
 * be cheating.
 * @return 0 on success, 1 on failure.
 */
int load_interfaces(void)
{
	FILE *fp = fopen(INTERFACE_LIST, "r");
	char buffer[100] = {'\0'};
	int parent_id, child_number;
	int number = 0;
	int x = 0, y = 0;

	if (fp == NULL)
	{
		printf("Failed to load interface data from file. Check that it has\n");
		printf("been copied over properly from the cache extraction tools\n");
		printf("to the data directory.\n");
		printf("File is named: intr_data.dat\n");
		exit(1);
	}

	/* First off, we must figure out how many interfaces there are total.  */
	while (fgets(buffer, 100, fp) != NULL)
	{
		if (sscanf(buffer, "%d\t%d\n", &parent_id, &child_number) == 2)
		{
			number++;
		}
	}

	rewind(fp);

	INTERFACES = calloc(number, sizeof(struct parent_interface));
	INTERFACES_SIZE = number;

	while (x < number)
	{
		fgets(buffer, 100, fp);

		if (sscanf(buffer, "%d\t%d\n", &parent_id, &child_number) == 2)
		{
			INTERFACES[x].id = parent_id;
			INTERFACES[x].children = calloc(child_number, sizeof(int));
			INTERFACES[x].size = child_number;
			
			for (y = 0; y < child_number; y++)
			{
				fgets(buffer, 100, fp);
				sscanf(buffer, "\t%d\n", &INTERFACES[x].children[y]);
			}
		}
		x++;
	}

	fclose(fp);
	printf("Loaded %d interface(s)!\n", number);
	return 0;
}

/**
 * Loads configuration data and sets values. Checks if the configuration file
 * is up to date.
 * @todo Handle reloading of the configuration while server is running.
 * @return 0 on success, 1 on failure.
 */
int load_config_data(void)
{
	FILE *fp;
	char read_line[200];
	char read_string[100];
	char read_value[100];
	int rv = 0;

	fp = fopen(CONFIG_FILE, "r");	

	if (fp == NULL)
	{
		printf("Could not open %s for reading configuration!\n", CONFIG_FILE);
		return 1;
	}

	while (fgets(read_line, 200, fp) != NULL)
	{
		rv = sscanf(read_line, "%99s = %99s", read_string, read_value);
		
		if (rv == 2)
		{
			if (strncmp(read_string, "config_version", 100) == 0)
			{
				sscanf(read_value, "%d", &config_version);
			}
			if (strncmp(read_string, "server_name", 100) == 0)
			{
				strncpy(server_name, read_value, CFG_SERVER_NAME_SIZE);
			}
			if (strncmp(read_string, "server_port", 100) == 0)
			{
				strncpy(server_port, read_value, CFG_SERVER_PORT_SIZE);
			}
			if (strncmp(read_string, "server_xp_multiplier", 100) == 0)
			{
				sscanf(read_value, "%f", &server_xp_multiplier);
			}
			if (strncmp(read_string, "player_unlimited_stamina", 100) == 0)
			{
				sscanf(read_value, "%d", &player_unlimited_stamina);
			}
			if (strncmp(read_string, "npc_random_walk_chance", 100) == 0)
			{
				sscanf(read_value, "%d", &npc_random_walk_chance);
			}
			if (strncmp(read_string, "combat_arrow_break_chance", 100) == 0)
			{
				sscanf(read_value, "%d", &combat_arrow_break_chance);
			}
			if (strncmp(read_string, "combat_pvp_everywhere", 100) == 0)
			{
				sscanf(read_value, "%d", &combat_pvp_everywhere);
			}
			if (strncmp(read_string, "skills_cooking_base_burn_chance", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_cooking_base_burn_chance);
			}
			if (strncmp(read_string, "skills_cooking_range_burn_mod", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_cooking_range_burn_mod);
			}
			if (strncmp(read_string, "skills_cooking_lumbridge_range_burn_mod",
						100) 
				== 0)
			{
				sscanf(read_value, "%d", 
					   &skills_cooking_lumbridge_range_burn_mod);
			}
			if (strncmp(read_string, "skills_cooking_enable_time_increase", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_cooking_enable_time_increase);
			}
			if (strncmp(read_string, "skills_cooking_max_flour_in_bin", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_cooking_max_flour_in_bin);
			}
			if (strncmp(read_string, "skills_mining_ore_depletion_method", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_mining_ore_depletion_method);
			}
			if (strncmp(read_string, "skills_mining_rune_essence_method", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &skills_mining_rune_essence_method);
			}
			if (strncmp(read_string, "shops_restock_ticks", 100) 
				== 0)
			{
				sscanf(read_value, "%d", &shops_restock_ticks);
			}
			if (strncmp(read_string, "music_show_song_unlock_message", 100)
				== 0)
			{
				sscanf(read_value, "%d", &music_show_song_unlock_message);
			}
		}
	}
	
	/* Check if the server is using the config file it should be, if version
	 * numbers do not match, notify the user.  */
	if (config_version != SERVER_CONFIG_VERSION)
	{
		printf("\tNot using latest configuration file! Please update your\n");
		printf("\tserver's config file.\n");
		printf("\tExpected version number: %d Received: %d\n", 
			   SERVER_CONFIG_VERSION, config_version);
	}
	printf("Finished reading config data!\n");
	fclose(fp);
	return 0;
}

/**
 * Loads object spawn data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_agility_data(void)
{
	/* Loads up all the global objects from a textfile into structs for the
	   server to use.  */
	int count = 0;
	int x = 0, y = 0, z = 0;
	FILE *fp;
	char obstacle_type_temp[100] = {0};
	char obstacle_desc_temp[200] = {0};
	struct agility_obstacle_data zeroed_agi = {0};

	if ((fp = fopen(AGILITY_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING AGILITY DATA FILE!\n");
		return 1;
	}

	struct csv_file *csv = read_csv_file(fp, "\t\n");
	fclose(fp);

	NUM_AGILITY_OBSTACLES = (csv->num_lines - 1);
	int num_agility_stages = 6;

	/* Create the SPELLS array.  */
	AGILITY_DATA = calloc(NUM_AGILITY_OBSTACLES, 
						  (sizeof(struct agility_obstacle_data*)));
	
	for (x = 1; x < csv->num_lines; x++)
	{
		struct agility_obstacle_data *agi 
			= malloc(sizeof(struct agility_obstacle_data));
		*agi = zeroed_agi;
		y = 0;
		
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->is_course);
		strcpy(obstacle_desc_temp, get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->id);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->world_x); 
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->world_y); 
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->world_z);
		sscanf(get_csv_token(csv, x, y++), "%f", &agi->xp);
		sscanf(get_csv_token(csv, x, y++), "%f", &agi->xp_on_failure);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->level_needed);
		strcpy(obstacle_type_temp, get_csv_token(csv, x, y++));
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->turn);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->turn_two);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->use_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->use_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->use_x_two);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->use_y_two);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->start_emote);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->num_stages);

		for (z = 0; z < num_agility_stages; z++)
		{
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].x);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].y);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].z);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].failable);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].win_emote);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].lose_emote);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].is_walk);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].walk_emote);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].stand_emote);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].phase_delay);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].phase_time);
			sscanf(get_csv_token(csv, x, y++), "%d", &agi->stages[z].ticks);
		}

		sscanf(get_csv_token(csv, x, y++), "%d", &agi->course_id);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->course_obstacle_id);
		sscanf(get_csv_token(csv, x, y++), "%d", 
				&agi->course_is_duplicate_obstacle);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->is_final_obstacle);

		strcpy(agi->start_message, get_csv_token(csv, x, y++));
		strcpy(agi->win_message, get_csv_token(csv, x, y++));
		strcpy(agi->lose_message, get_csv_token(csv, x, y++));

		sscanf(get_csv_token(csv, x, y++), "%d", &agi->fail_type);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->fail_ticks);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->fail_x);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->fail_y);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->fail_z);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->win_base_chance);
		sscanf(get_csv_token(csv, x, y++), "%d", &agi->win_max_chance);
		sscanf(get_csv_token(csv, x, y++), "%f", &agi->dmg);

		/* Apply the types of obstacles.  */
		/* Move to own function maybe?  */
		if (strcmp(obstacle_type_temp, "WALK") == 0)
		{
			agi->obstacle_type = AGILITY_WALK;
		}
		else if (strcmp(obstacle_type_temp, "LADDER") == 0)
		{
			agi->obstacle_type = AGILITY_LADDER;
		}
		else
		{
			agi->obstacle_type = -1;
		}

		if (agi->is_course == 1)
		{
			/* Zero this, we use this for courses in tracking the number
			 * of obstacles they have.  */
			agi->course_obstacle_id = 0;
		}

		/* Clear index for now.  */
		agi->course_index = -1;
		AGILITY_DATA[count] = agi;
		count++;
	}

	/** Find how many obstacles are listed for a certain course.  */
	for (x = 0; x < count; x++)
	{
		/* If we found a course, see how many obstacles are for it.  */
		if (AGILITY_DATA[x]->is_course == 1)
		{
			int course_id_to_match = AGILITY_DATA[x]->course_id;
			
			for (y = 0; y < count; y++)
			{
				if (AGILITY_DATA[y]->is_course == 0 
					&& AGILITY_DATA[y]->course_id == course_id_to_match)
				{
					/* Set index for quicker lookup for later.  */
					AGILITY_DATA[y]->course_index = x;
					if (AGILITY_DATA[y]->course_is_duplicate_obstacle == 0)
					{
						(AGILITY_DATA[x]->course_obstacle_id)++;
					}
				}
			}

			/*
			printf("Course %d num obs: %d\n", x, 
				AGILITY_DATA[x]->course_obstacle_id);
			*/	
		}
	}

	/* Check agility data for errors.  */
	for (x = 0; x < count; x++)
	{
		check_agility_data_for_errors(AGILITY_DATA[x], x);
	}

	free_csv_file(csv);

	printf("Loaded %d agility obstacle(s)!\n", count);
	return 0;
}

/**
 * Loads store data from database into the server arrays.
 * @return 1 on failure
 * @return 0 on success
 */
int load_shop_data(void)
{
	/* Loads up all the global objects from a textfile into structs for the
	   server to use.  */
	int count = 0;
	int x = 0, y = 0;
	FILE *fp;
	char buffer[5000] = {0};
	struct shop_info zeroed_shop = {0};

	if ((fp = fopen(SHOPS_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING SHOP DATA FILE!\n");
		return 1;
	}

	NUM_SHOPS = (get_file_line_count(fp) - 1);

	/* Create the SPELLS array.  */
	SHOP_DATA = calloc(NUM_SHOPS, (sizeof(struct shop_info*)));
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		int rv = 0;
		struct shop_info *shop = malloc(sizeof(struct shop_info));
		*shop = zeroed_shop;

		if ((rv = sscanf(buffer, 
					/* Description.  */
					"%200[^\t]\t"
					/* Can buy, sell.  */
					"%d\t%d\t"
					/* Is specialty shop.  */
					"%d\t"
					/* Max sell value.  */
					"%f\t"
					/* Load items, ID then amount.  */
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t"
					"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",

					shop->name,
					&shop->can_buy, &shop->can_sell,
					&shop->is_specialty,
					&shop->max_sell_value,

					&shop->items[0], &shop->items_n[0],
					&shop->items[1], &shop->items_n[1],
					&shop->items[2], &shop->items_n[2],
					&shop->items[3], &shop->items_n[3],
					&shop->items[4], &shop->items_n[4],
					&shop->items[5], &shop->items_n[5],
					&shop->items[6], &shop->items_n[6],
					&shop->items[7], &shop->items_n[7],
					&shop->items[8], &shop->items_n[8],
					&shop->items[9], &shop->items_n[9],
					&shop->items[10], &shop->items_n[10],
					&shop->items[11], &shop->items_n[11],
					&shop->items[12], &shop->items_n[12],
					&shop->items[13], &shop->items_n[13],
					&shop->items[14], &shop->items_n[14],
					&shop->items[15], &shop->items_n[15],
					&shop->items[16], &shop->items_n[16],
					&shop->items[17], &shop->items_n[17],
					&shop->items[18], &shop->items_n[18],
					&shop->items[19], &shop->items_n[19],
					&shop->items[20], &shop->items_n[20],
					&shop->items[21], &shop->items_n[21],
					&shop->items[22], &shop->items_n[22],
					&shop->items[23], &shop->items_n[23],
					&shop->items[24], &shop->items_n[24],
					&shop->items[25], &shop->items_n[25],
					&shop->items[26], &shop->items_n[26],
					&shop->items[27], &shop->items_n[27],
					&shop->items[28], &shop->items_n[28],
					&shop->items[29], &shop->items_n[29],
					&shop->items[30], &shop->items_n[30],
					&shop->items[31], &shop->items_n[31],
					&shop->items[32], &shop->items_n[32],
					&shop->items[33], &shop->items_n[33],
					&shop->items[34], &shop->items_n[34],
					&shop->items[35], &shop->items_n[35],
					&shop->items[36], &shop->items_n[36],
					&shop->items[37], &shop->items_n[37],
					&shop->items[38], &shop->items_n[38],
					&shop->items[39], &shop->items_n[39]

					)) == 85)
		{
			/* Keep track of what the shop originally had. Used for restocking
			 * later.  */
			for (y = 0; y < SHOP_SIZE; y++)
			{
				shop->orig_items[y] = shop->items[y];
				shop->orig_items_n[y] = shop->items_n[y];
			}
			shop->id = count;
			SHOP_DATA[count] = shop;
			count++;
		}
		else
		{
			printf("wrong! got %d points read at line %d\n", rv, count);
			printf("Please check that the shop data file is in the\n"
				   "data/folder and has not been corrupted.\n");
			exit(1);
			break;
		}
	}

	/* Check shop data for errors.  */
	for (x = 0; x < count; x++)
	{
		check_shop_data_for_errors(SHOP_DATA[x], x);
	}

	fclose(fp);
	printf("Loaded %d shop(s)!\n", count);
	return 0;
}

/**
 * Load all quest data from the quest database file.
 * @return 0 on success, 1 on failure.
 */
int load_quest_data(void)
{
	/* Loads up all the global objects from a textfile into structs for the
	   server to use.  */
	int count = 0;
	FILE *fp;
	char buffer[5000] = {0};
	struct quest_info zeroed_quest = {0};

	if ((fp = fopen(QUESTS_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING QUEST DATA FILE!\n");
		return 1;
	}

	/* Create the Quests array.  */
	QUEST_DATA = calloc(PROTOCOL_NUM_QUESTS, (sizeof(struct quest_info*)));
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		int rv = 0;
		struct quest_info *quest = malloc(sizeof(struct quest_info));
		*quest = zeroed_quest;

		if ((rv = sscanf(buffer, 
					/* ID.  */
					"%d\t"
					/* Name.  */
					"%200[^\t]\t"
					/* Skill Reqs  */
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					/* Quest points needed, rewarded.  */
					"%d\t%d\t",

					&quest->id,
					quest->name,
					&quest->interface_id,

					&quest->skill_req[0], &quest->skill_req[1],
					&quest->skill_req[2], &quest->skill_req[3],
					&quest->skill_req[4], &quest->skill_req[5],
					&quest->skill_req[6], &quest->skill_req[7],
					&quest->skill_req[8], &quest->skill_req[9],
					&quest->skill_req[10], &quest->skill_req[11],
					&quest->skill_req[12], &quest->skill_req[13],
					&quest->skill_req[14], &quest->skill_req[15],
					&quest->skill_req[16], &quest->skill_req[17],
					&quest->skill_req[18], &quest->skill_req[19],
					&quest->skill_req[20],

					&quest->quest_point_req, &quest->quest_point_reward


					)) != -1)
		{
			QUEST_DATA[count] = quest;
			count++;
		}
		else
		{
			printf("wrong! got %d points read at line %d\n", rv, count);
			printf("Please check that the quest data file is in the\n"
				   "data/folder and has not been corrupted.\n");
			exit(1);
			break;
		}
	}

	/* Check quest data for errors.  */
	/*
	for (x = 0; x < count; x++)
	{
		check_quest_data(QUEST_DATA[x], x);
	}
	*/

	fclose(fp);
	printf("Loaded %d quest(s)!\n", count);
	return 0;
}

/**
 * Load all music data from the music database file.
 * @return 0 on success, 1 on failure.
 */
int load_music_data(void)
{
	/* Loads up all the global objects from a textfile into structs for the
	   server to use.  */
	int count = 0;
	int x = 0, y = 0, z = 0, f = 0;
	FILE *fp;
	char buffer[5000] = {0};
	struct music_info zeroed_music = {0};

	if ((fp = fopen(MUSIC_LIST, "r")) == NULL)
	{
		printf("ERROR OPENING MUSIC DATA FILE!\n");
		return 1;
	}

	/* Create the MUSIC array.  */
	MUSIC_DATA = calloc(PROTOCOL_NUM_SONGS, (sizeof(struct music_info*)));
	
	/* Removes the comment line at the top for spreadsheet software.  */
	fgets(buffer, sizeof(buffer), fp);

	/* Reads until fgets recieves a NULL, signifying the end of the file
	   or an error.  */
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		int rv = 0;
		struct music_info *music = malloc(sizeof(struct music_info));
		*music = zeroed_music;

		if ((rv = sscanf(buffer, 
					/* ID.  */
					"%d\t"
					/* Name.  */
					"%200[^\t]\t"
					/* Interface id.  */
					"%d\t"
					/* If song is unlocked by default.  */
					"%d\t"
					/* Config id, bit position.  */
					"%d\t%u\t"
					/* Music regions  */
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t"
					"%d\t%d\t",

					&music->id,
					music->name,
					&music->interface_id,
					&music->unlocked_by_default,
					&music->config_id,
					&music->bit,

					&music->regions[0],
					&music->regions[1],
					&music->regions[2],
					&music->regions[3],
					&music->regions[4],
					&music->regions[5],
					&music->regions[6],
					&music->regions[7]

					)) != -1)
		{
			MUSIC_DATA[count] = music;
			count++;
		}
		else
		{
			printf("wrong! got %d points read at line %d\n", rv, count);
			printf("Please check that the music data file is in the\n"
				   "data/folder and has not been corrupted.\n");
			exit(1);
			break;
		}
	}

	/** Assign music ids to each region, make sure that we don't overwrite 
	 * anything.  */
	for (x = 0; x < count; x++)
	{
		for (y = 0; y < MUSIC_MAX_REGIONS; y++)
		{
			if (MUSIC_DATA[x]->regions[y] != 0)
			{
				for (z = 0; z < REGION_LIST_NUM; z++)
				{
					if (REGION_LIST[z]->id == MUSIC_DATA[x]->regions[y])
					{
						if (REGION_LIST[z]->music_id != -1)
						{
							printf("Region %d already has music set! %d\n", 
								   REGION_LIST[z]->id,
								   REGION_LIST[z]->music_id); 
							
							for (f = 0; f < PROTOCOL_NUM_SONGS; f++)
							{
								if (MUSIC_DATA[f]->id
									== REGION_LIST[z]->music_id)
								{
									printf("Song: %s\n", MUSIC_DATA[f]->name);
									break;
								}
							}
							break;
						}
						REGION_LIST[z]->music_id = MUSIC_DATA[x]->id;
					}
				}
			}
		}
	}

	/* Check music data for errors.  */
	/*
	for (x = 0; x < count; x++)
	{
		check_music_data(MUSIC_DATA[x], x);
		printf("Song id: %d Name: %s\n", MUSIC_DATA[x]->id, MUSIC_DATA[x]->name);
	}
	*/

	fclose(fp);
	printf("Loaded %d song(s)!\n", count);
	return 0;
}
