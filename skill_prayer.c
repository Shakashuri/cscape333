/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "misc.h"
#include "packet.h"
#include "player.h"
#include "skills.h"

int get_prayer_level_needed(int prayer_id)
{
	switch (prayer_id)
	{
		case PRAYER_THICK_SKIN:
			return 1;
		case PRAYER_BURST_OF_STRENGTH:
			return 4;
		case PRAYER_CLARITY_OF_THOUGHT:
			return 7;
		case PRAYER_ROCK_SKIN:
			return 10;
		case PRAYER_SUPERHUMAN_STRENGTH:
			return 13;
		case PRAYER_IMPROVED_REFLEXES:
			return 16;
		case PRAYER_RAPID_RESTORE:
			return 19;
		case PRAYER_RAPID_HEAL:
			return 22;
		case PRAYER_PROTECT_ITEMS:
			return 25;
		case PRAYER_STEEL_SKIN:
			return 28;
		case PRAYER_ULTIMATE_STRENGTH:
			return 31;
		case PRAYER_INCREDIBLE_REFLEXES:
			return 34;
		case PRAYER_PROTECT_FROM_MAGIC:
			return 37;
		case PRAYER_PROTECT_FROM_MISSILES:
			return 40;
		case PRAYER_PROTECT_FROM_MELEE:
			return 43;
		case PRAYER_RETRIBUTION:
			return 46;
		case PRAYER_REDEMPTION:
			return 49;
		case PRAYER_SMITE:
			return 52;

		default:
			return -1;
	}
}

float get_prayer_xp(int prayer_id)
{
	switch (prayer_id)
	{
		/* Regular bones, burnt bones, wolf bones.  */
		case 526:
		case 528:
		case 2859:
			return 4.5;
		/* Monkey bones.  */
		case 3179:
		case 3180:
		case 3183:
		case 3185:
		case 3186:
			return 5.0;
		/* Bat bones.  */
		case 530:
			return 5.33;
		/* Big bones, jogre bones.  */
		case 532:
		case 3125:
			return 15.0;
		/* Monkey guard bones.  */
		case 3181:
		case 3182:
			return 18.0;
		/* Baby dragon bones.  */
		case 534:
			return 30.0;
		/* Dragon bones.  */
		case 536:
			return 72.0;
		/* Zogre bones.  */
		case 4812:
			return 22.5;
		/* Fayrg bones.  */
		case 4830:
			return 84.0;
		/* Raurg bones.  */
		case 4832:
			return 96.0;
		/* Ourg bones.  */
		case 4834:
			return 140.0;
		
		default:
			return 0.0;
	}
}

int get_prayer_drain(int prayer_id)
{
	switch (prayer_id)
	{
		case PRAYER_THICK_SKIN:
			return 3;
		case PRAYER_BURST_OF_STRENGTH:
			return 3;
		case PRAYER_CLARITY_OF_THOUGHT:
			return 3;
		case PRAYER_ROCK_SKIN:
			return 6;
		case PRAYER_SUPERHUMAN_STRENGTH:
			return 6;
		case PRAYER_IMPROVED_REFLEXES:
			return 6;
		case PRAYER_RAPID_RESTORE:
			return 1;
		case PRAYER_RAPID_HEAL:
			return 2;
		case PRAYER_PROTECT_ITEMS:
			return 2;
		case PRAYER_STEEL_SKIN:
			return 12;
		case PRAYER_ULTIMATE_STRENGTH:
			return 12;
		case PRAYER_INCREDIBLE_REFLEXES:
			return 12;
		case PRAYER_PROTECT_FROM_MAGIC:
			return 12;
		case PRAYER_PROTECT_FROM_MISSILES:
			return 12;
		case PRAYER_PROTECT_FROM_MELEE:
			return 12;
		case PRAYER_RETRIBUTION:
			return 3;
		case PRAYER_REDEMPTION:
			return 6;
		case PRAYER_SMITE:
			return 18;

		default:
			return -1;
	}
}

int get_prayer_config_id(int prayer_id)
{
	switch (prayer_id)
	{
		case PRAYER_THICK_SKIN:
			return 83;
		case PRAYER_BURST_OF_STRENGTH:
			return 84;
		case PRAYER_CLARITY_OF_THOUGHT:
			return 85;
		case PRAYER_ROCK_SKIN:
			return 86;
		case PRAYER_SUPERHUMAN_STRENGTH:
			return 87;
		case PRAYER_IMPROVED_REFLEXES:
			return 88;
		case PRAYER_RAPID_RESTORE:
			return 89;
		case PRAYER_RAPID_HEAL:
			return 90;
		case PRAYER_PROTECT_ITEMS:
			return 91;
		case PRAYER_STEEL_SKIN:
			return 92;
		case PRAYER_ULTIMATE_STRENGTH:
			return 93;
		case PRAYER_INCREDIBLE_REFLEXES:
			return 94;
		case PRAYER_PROTECT_FROM_MAGIC:
			return 95;
		case PRAYER_PROTECT_FROM_MISSILES:
			return 96;
		case PRAYER_PROTECT_FROM_MELEE:
			return 97;
		case PRAYER_RETRIBUTION:
			return 98;
		case PRAYER_REDEMPTION:
			return 99;
		case PRAYER_SMITE:
			return 100;

		default:
			return -1;
	}
}

float get_prayer_stat_change(int prayer_id)
{
	switch (prayer_id)
	{
		case PRAYER_THICK_SKIN:
			return 1.05;
		case PRAYER_BURST_OF_STRENGTH:
			return 1.05;
		case PRAYER_CLARITY_OF_THOUGHT:
			return 1.05;
		case PRAYER_ROCK_SKIN:
			return 1.10;
		case PRAYER_SUPERHUMAN_STRENGTH:
			return 1.10;
		case PRAYER_IMPROVED_REFLEXES:
			return 1.10;
		case PRAYER_STEEL_SKIN:
			return 1.15;
		case PRAYER_ULTIMATE_STRENGTH:
			return 1.15;
		case PRAYER_INCREDIBLE_REFLEXES:
			return 1.15;

		default:
			return 0.0;
	}
}


/**
 * Handles animation and xp gain for burying bones.
 * @param *c Client instance.
 * @param item_id Bone item id.
 * @param item_slot Where the bones are in the inventory.
 */
void bury_bones(struct client *c, int item_id, int item_slot)
{
	float xp;

	if (item_id != get_item_in_inventory_slot(c, item_slot)
		|| c->plr->animation_play_type == ANIMATION_NO_OVERWRITE)
	{
		return;
	}
	
	delete_item_from_inv_slot(c, item_slot, 1);
	xp = get_prayer_xp(item_id);
	add_skill_xp(c, PRAYER_SKILL, xp);
	player_play_animation_no_overwrite(c, 827, 2);
	packet_play_sfx(c, 380, 100, -1);
	packet_send_chatbox_message(c, "You dig a hole in the ground.");
	packet_send_chatbox_message(c, "You bury the bones.");
}

/**
 * Calculates and sets a player's resistance to their prayer points draining.
 * @param *c Client instance.
 */
void calculate_prayer_drain_resistance(struct client *c)
{
	c->plr->prayer_drain_resistance 
		= (60 + (c->plr->player_bonus[PRAYER_BONUS] * 2));
}

static void set_prayer_icon(struct client *c, int icon_id)
{
	c->plr->prayer_icon = icon_id;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	c->plr->update_required = 1;
}

/**
 * Handles enabling of a prayer.
 * @param *c Client instance.
 * @param prayer_id ID of the prayer to be enabled.
 */
void enable_prayer(struct client *c, int prayer_id)
{
	int drain = get_prayer_drain(prayer_id);
	int config_id = get_prayer_config_id(prayer_id);

	if (c->plr->prayers_active[prayer_id] == 0)
	{
		switch (prayer_id)
		{
			case PRAYER_THICK_SKIN:
				c->plr->prayer_defence_bonus = 1.05;
				break;
			case PRAYER_BURST_OF_STRENGTH:
				c->plr->prayer_strength_bonus = 1.05;
				calculate_max_hit(c);
				break;
			case PRAYER_CLARITY_OF_THOUGHT:
				c->plr->prayer_attack_bonus = 1.05;
				break;
			case PRAYER_ROCK_SKIN:
				c->plr->prayer_defence_bonus = 1.10;
				break;
			case PRAYER_SUPERHUMAN_STRENGTH:
				c->plr->prayer_strength_bonus = 1.10;
				calculate_max_hit(c);
				break;
			case PRAYER_IMPROVED_REFLEXES:
				c->plr->prayer_attack_bonus = 1.10;
				break;
			case PRAYER_STEEL_SKIN:
				c->plr->prayer_defence_bonus = 1.15;
				break;
			case PRAYER_ULTIMATE_STRENGTH:
				c->plr->prayer_strength_bonus = 1.15;
				calculate_max_hit(c);
				break;
			case PRAYER_INCREDIBLE_REFLEXES:
				c->plr->prayer_attack_bonus = 1.15;
				break;
			case PRAYER_PROTECT_FROM_MAGIC:
				set_prayer_icon(c, 2);
				break;
			case PRAYER_PROTECT_FROM_MISSILES:
				set_prayer_icon(c, 1);
				break;
			case PRAYER_PROTECT_FROM_MELEE:
				set_prayer_icon(c, 0);
				break;
			case PRAYER_RETRIBUTION:
				set_prayer_icon(c, 3);
				break;
			case PRAYER_REDEMPTION:
				set_prayer_icon(c, 5);
				break;
			case PRAYER_SMITE:
				set_prayer_icon(c, 4);
				break;
			/* XXX These are handled outside this function, do not need to be
			 * enabled here.
			case PRAYER_RAPID_RESTORE:
				return 89;
			case PRAYER_RAPID_HEAL:
				return 90;
			case PRAYER_PROTECT_ITEMS:
				return 91;
			*/
		}

		c->plr->prayer_total_drain += drain;
		c->plr->prayers_active[prayer_id] = 1;
		packet_set_client_config_value(c, config_id, 1);
	}
}

/**
 * Handles disabling of a prayer.
 * @param *c Client instance.
 * @param prayer_id ID of the prayer to be disabled.
 */
void disable_prayer(struct client *c, int prayer_id)
{
	int drain = get_prayer_drain(prayer_id);
	int config_id = get_prayer_config_id(prayer_id);

	if (c->plr->prayers_active[prayer_id] == 1)
	{
		switch (prayer_id)
		{
			case PRAYER_THICK_SKIN:
			case PRAYER_ROCK_SKIN:
			case PRAYER_STEEL_SKIN:
				c->plr->prayer_defence_bonus = 1.00;
				break;
			case PRAYER_BURST_OF_STRENGTH:
			case PRAYER_SUPERHUMAN_STRENGTH:
			case PRAYER_ULTIMATE_STRENGTH:
				c->plr->prayer_strength_bonus = 1.00;
				calculate_max_hit(c);
				break;
			case PRAYER_CLARITY_OF_THOUGHT:
			case PRAYER_IMPROVED_REFLEXES:
			case PRAYER_INCREDIBLE_REFLEXES:
				c->plr->prayer_attack_bonus = 1.00;
				break;
			/* These are here for later, if needed.
			case PRAYER_RAPID_RESTORE:
				break;
			case PRAYER_RAPID_HEAL:
				break;
			case PRAYER_PROTECT_ITEMS:
				break;
			case PRAYER_PROTECT_FROM_MAGIC:
				break;
			case PRAYER_PROTECT_FROM_MISSILES:
				break;
			case PRAYER_PROTECT_FROM_MELEE:
				break;
			case PRAYER_RETRIBUTION:
				break;
			case PRAYER_REDEMPTION:
				break;
			case PRAYER_SMITE:
				break;
			*/
		}

		/* Reset overhead icon.  */
		set_prayer_icon(c, -1);
		c->plr->prayer_total_drain -= drain;
		c->plr->prayers_active[prayer_id] = 0;
		packet_set_client_config_value(c, config_id, 0);
	}
}

/**
 * Handles setting of prayer points drain when prayers are enabled, prayer
 * requirements, and handles prayers that can't be enabled along with certain
 * others.
 * @param *c Client id.
 * @param prayer_id Prayer being processed at the moment.
 */
void handle_prayer(struct client *c, int prayer_id)
{
	int config_id = get_prayer_config_id(prayer_id);
	int level_needed = get_prayer_level_needed(prayer_id);
	char return_msg[100];

	/* If prayer is toggled off.  */
	if (c->plr->prayers_active[prayer_id] == 1)
	{
		disable_prayer(c, prayer_id);
		return;
	}

	/* If prayer is toggled on.  */
	if (c->plr->prayers_active[prayer_id] == 0)
	{
		/* Check level requirements.  */
		if (c->plr->base_level[PRAYER_SKILL] < level_needed)
		{
			snprintf(return_msg, 100, "You need a prayer level of %d to use "
					 "that.", level_needed);
			packet_send_chatbox_message(c, return_msg);
			packet_set_client_config_value(c, config_id, 0);
			return;
		}
		/* Check if they have prayer points left.  */
		if (c->plr->level[PRAYER_SKILL] == 0)
		{
			packet_send_chatbox_message(c, "You have run out of prayer "
					"points, you must recharge at an altar.");
			packet_set_client_config_value(c, config_id, 0);
			return;
		}

		/* If prayer is one that doesn't allow certain others, apply it.  */
		switch (prayer_id)
		{
			case PRAYER_THICK_SKIN:
			case PRAYER_ROCK_SKIN:
			case PRAYER_STEEL_SKIN:
				disable_prayer(c, PRAYER_THICK_SKIN);
				disable_prayer(c, PRAYER_ROCK_SKIN);
				disable_prayer(c, PRAYER_STEEL_SKIN);
				break;
			case PRAYER_BURST_OF_STRENGTH:
			case PRAYER_SUPERHUMAN_STRENGTH:
			case PRAYER_ULTIMATE_STRENGTH:
				disable_prayer(c, PRAYER_BURST_OF_STRENGTH);
				disable_prayer(c, PRAYER_SUPERHUMAN_STRENGTH);
				disable_prayer(c, PRAYER_ULTIMATE_STRENGTH);
				break;
			case PRAYER_CLARITY_OF_THOUGHT:
			case PRAYER_IMPROVED_REFLEXES:
			case PRAYER_INCREDIBLE_REFLEXES:
				disable_prayer(c, PRAYER_CLARITY_OF_THOUGHT);
				disable_prayer(c, PRAYER_IMPROVED_REFLEXES);
				disable_prayer(c, PRAYER_INCREDIBLE_REFLEXES);
				break;
			/* Only one overhead is allowed at a time.  */
			case PRAYER_PROTECT_FROM_MAGIC:
			case PRAYER_PROTECT_FROM_MISSILES:
			case PRAYER_PROTECT_FROM_MELEE:
			case PRAYER_RETRIBUTION:
			case PRAYER_REDEMPTION:
			case PRAYER_SMITE:
				disable_prayer(c, PRAYER_PROTECT_FROM_MAGIC);
				disable_prayer(c, PRAYER_PROTECT_FROM_MISSILES);
				disable_prayer(c, PRAYER_PROTECT_FROM_MELEE);
				disable_prayer(c, PRAYER_RETRIBUTION);
				disable_prayer(c, PRAYER_REDEMPTION);
				disable_prayer(c, PRAYER_SMITE);
				break;
		}

		/* Everything okay, enable prayer.  */
		enable_prayer(c, prayer_id);
	}
}

