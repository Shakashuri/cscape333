/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "constants.h"
#include "client.h"
#include "combat.h"
#include "item.h"
#include "npc.h"
#include "object.h"
#include "object_handler.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "misc.h"

int get_thieving_level_needed(int thieving_id)
{
	switch (thieving_id)
	{
		/* Pickpocketing.  */
		case THIEVING_PP_MAN:
			return 1;
		case THIEVING_PP_FARMER:
			return 10;
		case THIEVING_PP_FEMALE_HAM:
			return 15;
		case THIEVING_PP_MALE_HAM:
			return 20;
		case THIEVING_PP_WARRIOR:
			return 25;
		case THIEVING_PP_ROGUE:
			return 32;
		case THIEVING_PP_MASTER_FARMER:
			return 38;
		case THIEVING_PP_GUARD:
			return 40;
		case THIEVING_PP_FREMENNIK:
			return 45;
		case THIEVING_PP_BEARDED_P_BANDIT:
			return 45;
		case THIEVING_PP_DESERT_BANDIT:
			return 53;
		case THIEVING_PP_ARDOUGNE_KNIGHT:
			return 55;
		case THIEVING_PP_P_BANDIT:
			return 55;
		case THIEVING_PP_WATCHMEN:
			return 65;
		case THIEVING_PP_MENAPHITE:
			return 65;
		case THIEVING_PP_PALADIN:
			return 70;
		case THIEVING_PP_GNOME:
			return 75;
		case THIEVING_PP_HERO:
			return 80;
		case THIEVING_PP_ELF:
			return 85;
		/* Stalls.  */
		case THIEVING_STALL_VEGETABLE:
			return 2;
		case THIEVING_STALL_BAKERY:
		case THIEVING_STALL_BAKERY_DWARF:
		case THIEVING_STALL_CRAFTING:
		case THIEVING_STALL_CRAFTING_DWARF:
		case THIEVING_STALL_MONKEY_FOOD:
		case THIEVING_STALL_MONKEY_GENERAL:
		case THIEVING_STALL_TEA:
			return 5;
		case THIEVING_STALL_SILK:
			return 20;
		case THIEVING_STALL_SEED:
			return 27;
		case THIEVING_STALL_FUR:
			return 35;
		case THIEVING_STALL_FISH:
			return 42;
		case THIEVING_STALL_SILVER:
		case THIEVING_STALL_SILVER_DWARF:
			return 50;
		case THIEVING_STALL_SPICE:
		case THIEVING_STALL_MAGIC:
		case THIEVING_STALL_SCIMITAR:
			return 65;
		case THIEVING_STALL_GEM:
			return 75;
		/* Doors.  */
		case THIEVING_DOOR_10_COIN:
			return 1;
		case THIEVING_DOOR_NATURE_RUNE:
			return 14;
		case THIEVING_DOOR_ARDOUGNE_STOVE:
			return 16;
		case THIEVING_DOOR_HAM:
			return 17;
		case THIEVING_DOOR_WILDERNESS_AXE:
			return 32;
		case THIEVING_DOOR_ARDOUGNE_SEWER:
			return 32;
		case THIEVING_DOOR_WILDERNESS_PIRATE:
			return 39;
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			return 46;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			return 61;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			return 82;
		/* Chests.  */
		case THIEVING_CHEST_10_COIN:
			return 13;
		case THIEVING_CHEST_NATURE_RUNE:
			return 28;
		case THIEVING_CHEST_50_COIN:
			return 43;
		case THIEVING_CHEST_STEEL_ARROWTIPS:
			return 47;
		case THIEVING_CHEST_BLOOD_RUNE:
			return 59;
		case THIEVING_CHEST_KING_LATHAS:
			return 72;

		default:
			printf("Invalid thieving level id: %d\n", thieving_id);
			return -1;
	}
}

float get_thieving_xp(int thieving_id)
{
	switch (thieving_id)
	{
		/* Pickpocketing.  */
		case THIEVING_PP_MAN:
			return 8.0;
		case THIEVING_PP_FARMER:
			return 14.5;
		case THIEVING_PP_FEMALE_HAM:
			return 18.5;
		case THIEVING_PP_MALE_HAM:
			return 22.5;
		case THIEVING_PP_WARRIOR:
			return 26.0;
		case THIEVING_PP_ROGUE:
			return 35.5;
		case THIEVING_PP_MASTER_FARMER:
			return 43.0;
		case THIEVING_PP_GUARD:
			return 46.8;
		case THIEVING_PP_FREMENNIK:
			return 65.0;
		case THIEVING_PP_BEARDED_P_BANDIT:
			return 65.0;
		case THIEVING_PP_DESERT_BANDIT:
			return 79.5;
		case THIEVING_PP_ARDOUGNE_KNIGHT:
			return 84.3;
		case THIEVING_PP_P_BANDIT:
			return 84.3;
		case THIEVING_PP_WATCHMEN:
			return 137.5;
		case THIEVING_PP_MENAPHITE:
			return 137.5;
		case THIEVING_PP_PALADIN:
			return 151.75;
		case THIEVING_PP_GNOME:
			return 198.5;
		case THIEVING_PP_HERO:
			return 275.0;
		case THIEVING_PP_ELF:
			return 353.0;
		/* Stalls.  */
		case THIEVING_STALL_VEGETABLE:
			return 10.0;
		case THIEVING_STALL_BAKERY:
		case THIEVING_STALL_CRAFTING:
		case THIEVING_STALL_CRAFTING_DWARF:
		case THIEVING_STALL_MONKEY_FOOD:
		case THIEVING_STALL_MONKEY_GENERAL:
		case THIEVING_STALL_TEA:
			return 16.0;
		/* Conflicting data on this, but 2x bakery sounds reasonable.  */
		case THIEVING_STALL_BAKERY_DWARF:
			return 32.0;
		case THIEVING_STALL_SILK:
			return 24.0;
		case THIEVING_STALL_SEED:
			return 10.0;
		case THIEVING_STALL_FUR:
			return 36.0;
		case THIEVING_STALL_FISH:
			return 42.0;
		case THIEVING_STALL_SILVER:
		case THIEVING_STALL_SILVER_DWARF:
			return 54.0;
		case THIEVING_STALL_SPICE:
			return 81.0;
		case THIEVING_STALL_MAGIC:
			return 100.0;
		case THIEVING_STALL_SCIMITAR:
			return 160.0;
		case THIEVING_STALL_GEM:
			return 160.0;
		/* Doors.  */
		case THIEVING_DOOR_10_COIN:
			return 3.8;
		case THIEVING_DOOR_NATURE_RUNE:
			return 15.0;
		case THIEVING_DOOR_ARDOUGNE_STOVE:
			return 15.0;
		case THIEVING_DOOR_HAM:
			return 4.0;
		case THIEVING_DOOR_WILDERNESS_AXE:
			return 25.0;
		case THIEVING_DOOR_ARDOUGNE_SEWER:
			return 25.0;
		case THIEVING_DOOR_WILDERNESS_PIRATE:
			return 35.0;
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			return 37.5;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			return 50.0;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			return 50.0;
		/* Chests.  */
		case THIEVING_CHEST_10_COIN:
			return 7.8;
		case THIEVING_CHEST_NATURE_RUNE:
			return 25.0;
		case THIEVING_CHEST_50_COIN:
			return 125.0;
		case THIEVING_CHEST_STEEL_ARROWTIPS:
			return 150.0;
		case THIEVING_CHEST_BLOOD_RUNE:
			return 250.0;
		case THIEVING_CHEST_KING_LATHAS:
			return 500.0;

		default:
			printf("Invalid thieving xp id: %d\n", thieving_id);
			return -1;
	}
}

int get_thieving_pickpocket_chance_base(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_PP_MAN:
			return 180;
		case THIEVING_PP_FARMER:
			return 180;
		case THIEVING_PP_FEMALE_HAM:
			return 140;
		case THIEVING_PP_MALE_HAM:
			return 120;
		case THIEVING_PP_WARRIOR:
			return 90;
		case THIEVING_PP_ROGUE:
			return 90;
		case THIEVING_PP_MASTER_FARMER:
			return 90;
		case THIEVING_PP_GUARD:
			return 50;
		case THIEVING_PP_FREMENNIK:
			return 50;
		case THIEVING_PP_BEARDED_P_BANDIT:
			return 50;
		case THIEVING_PP_DESERT_BANDIT:
			return 50;
		case THIEVING_PP_ARDOUGNE_KNIGHT:
			return 50;
		case THIEVING_PP_P_BANDIT:
			return 40;
		case THIEVING_PP_WATCHMEN:
			return 20;
		case THIEVING_PP_MENAPHITE:
			return 1;
		case THIEVING_PP_PALADIN:
			return 1;
		case THIEVING_PP_GNOME:
			return 1;
		case THIEVING_PP_HERO:
			return 1;
		case THIEVING_PP_ELF:
			return 1;

		default:
			printf("Invalid thieving pp base chance id: %d\n", thieving_id);
			return -1;
	}
}

int get_thieving_pickpocket_chance_max(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_PP_MAN:
			return 240;
		case THIEVING_PP_FARMER:
			return 240;
		case THIEVING_PP_FEMALE_HAM:
			return 240;
		case THIEVING_PP_MALE_HAM:
			return 240;
		case THIEVING_PP_WARRIOR:
			return 240;
		case THIEVING_PP_ROGUE:
			return 240;
		case THIEVING_PP_MASTER_FARMER:
			return 240;
		case THIEVING_PP_GUARD:
			return 240;
		case THIEVING_PP_FREMENNIK:
			return 240;
		case THIEVING_PP_BEARDED_P_BANDIT:
			return 240;
		case THIEVING_PP_DESERT_BANDIT:
			return 240;
		case THIEVING_PP_ARDOUGNE_KNIGHT:
			return 240;
		case THIEVING_PP_P_BANDIT:
			return 240;
		case THIEVING_PP_WATCHMEN:
			return 240;
		case THIEVING_PP_MENAPHITE:
			return 240;
		case THIEVING_PP_PALADIN:
			return 240;
		case THIEVING_PP_GNOME:
			return 240;
		case THIEVING_PP_HERO:
			return 230;
		case THIEVING_PP_ELF:
			return 180;

		default:
			printf("Invalid thieving pp max chance id: %d\n", thieving_id);
			return -1;
	}
}

int get_thieving_pickpocket_damage(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_PP_MAN:
			return 1;
		case THIEVING_PP_FARMER:
			return 1;
		case THIEVING_PP_FEMALE_HAM:
			return rand_int(1, 3);
		case THIEVING_PP_MALE_HAM:
			return rand_int(1, 3);
		case THIEVING_PP_WARRIOR:
			return 2;
		case THIEVING_PP_ROGUE:
			return 2;
		case THIEVING_PP_MASTER_FARMER:
			return 3;
		case THIEVING_PP_GUARD:
			return 2;
		case THIEVING_PP_FREMENNIK:
			return 2;
		case THIEVING_PP_BEARDED_P_BANDIT:
			return 5;
		case THIEVING_PP_DESERT_BANDIT:
			return 3;
		case THIEVING_PP_ARDOUGNE_KNIGHT:
			return 3;
		case THIEVING_PP_P_BANDIT:
			return 5;
		case THIEVING_PP_WATCHMEN:
			return 3;
		case THIEVING_PP_MENAPHITE:
			return 5;
		case THIEVING_PP_PALADIN:
			return 3;
		case THIEVING_PP_GNOME:
			return 1;
		case THIEVING_PP_HERO:
			return 4;
		case THIEVING_PP_ELF:
			return 5;

		default:
			printf("Invalid thieving pp damage id: %d\n", thieving_id);
			return -1;
	}
}

int get_thieving_pickpocket_stun_duration(int thieving_id)
{
	switch (thieving_id)
	{
		/* 7 ticks, 4.2 seconds.  */
		case THIEVING_PP_FEMALE_HAM:
			return 7;
		/* 10 ticks, 6 seconds.  */
		case THIEVING_PP_ELF:
			return 10;

		/* 8 ticks, 4.8 seconds.  */
		default:
			return 8;
	}
}

void get_thieving_fail_text(int thieving_id, char *name, char *return_msg, 
							int size)
{
	switch (thieving_id)
	{
		case THIEVING_PP_FEMALE_HAM:
		case THIEVING_PP_MALE_HAM:
			switch (rand_int(0, 2))
			{
				case 0:
				default:
					snprintf(return_msg, size, 
							 "What do you think you're doing?");
					break;
				case 1:
					snprintf(return_msg, size, 
							 "We deal harsly with thieves around here.");
					break;
				case 2:
					snprintf(return_msg, size, 
							 "Keep thine hands to thineself %s.", name); 
					break;
			}
			break;
		case THIEVING_PP_MASTER_FARMER:
			snprintf(return_msg, size, 
					 "Cor blimey, mate! What are ye doing in me pockets?");
			break;
		default:
			snprintf(return_msg, size, 
					 "What do you think you're doing?");
			break;
	}
}

int get_thieving_stall_respawn_ticks(int thieving_id)
{
	switch (thieving_id)
	{
		/* Stalls.  */
		case THIEVING_STALL_VEGETABLE:
			return 3;
		case THIEVING_STALL_BAKERY:
			return 3;
		case THIEVING_STALL_BAKERY_DWARF:
			return 15;
		case THIEVING_STALL_CRAFTING:
		case THIEVING_STALL_MONKEY_FOOD:
		case THIEVING_STALL_MONKEY_GENERAL:
		case THIEVING_STALL_TEA:
			return 12;
		case THIEVING_STALL_CRAFTING_DWARF:
			return 4;
		case THIEVING_STALL_SILK:
			return 8;
		case THIEVING_STALL_SEED:
			return 15;
		case THIEVING_STALL_FUR:
			return 17;
		case THIEVING_STALL_FISH:
			return 17;
		case THIEVING_STALL_SILVER:
			return 50;
		case THIEVING_STALL_SILVER_DWARF:
			return 15;
		case THIEVING_STALL_SPICE:
			return 134;
		case THIEVING_STALL_MAGIC:
			return 134;
		case THIEVING_STALL_SCIMITAR:
			return 100.0;
		case THIEVING_STALL_GEM:
			return 300;

		default:
			printf("Invalid thieving stall respawn id: %d\n", thieving_id);
			return -1;
	}
}

int get_thieving_stall_empty_id(int stall_id)
{
	switch (stall_id)
	{
		/* Dwarven stalls.  */
		case 6162:
		case 6163:
		case 6164:
		case 6165:
		case 6166:
			return 6984;
		/* Green stalls?  */
		case 6568:
		case 6589:
		case 6590:
		case 6591:
		case 6592:
		case 6593:
		default:
			return 4276;
	}
}

float get_thieving_chest_trap_max_damage(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_CHEST_10_COIN:
			return 0.03;
		case THIEVING_CHEST_NATURE_RUNE:
			return 0.06;
		case THIEVING_CHEST_50_COIN:
			return 0.10;
		case THIEVING_CHEST_STEEL_ARROWTIPS:
			return 0.13;
		case THIEVING_CHEST_BLOOD_RUNE:
			return 0.18;
		case THIEVING_CHEST_KING_LATHAS:
			return 0.20;

		default:
			printf("ID: %d not handled! Thieving trap dmg unknown!\n", 
				   thieving_id);
			return 0.0;
	}
}

int get_thieving_chest_respawn_ticks(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_CHEST_10_COIN:
			return 12;
		case THIEVING_CHEST_NATURE_RUNE:
			return 25;
		case THIEVING_CHEST_50_COIN:
			return 83;
		case THIEVING_CHEST_STEEL_ARROWTIPS:
			return 350;
		case THIEVING_CHEST_BLOOD_RUNE:
			return 225;
		case THIEVING_CHEST_KING_LATHAS:
			return 667;

		default:
			printf("ID: %d not handled! Thieving respawn ticks unknown!\n", 
				   thieving_id);
			return 0.0;
	}
}

int get_thieving_lockpick_needed(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_DOOR_10_COIN:
		case THIEVING_DOOR_NATURE_RUNE:
		case THIEVING_DOOR_ARDOUGNE_STOVE:
		case THIEVING_DOOR_HAM:
			return 0;
		case THIEVING_DOOR_WILDERNESS_AXE:
			return 1;
		case THIEVING_DOOR_ARDOUGNE_SEWER:
			return 0;
		case THIEVING_DOOR_WILDERNESS_PIRATE:
			return 1;
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			return 0;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			return 0;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			return 1;
		default:
			printf("ID: %d not handled! Thieving lockpick chance unknown!\n", 
				   thieving_id);
			return -1;
	}
}

int get_thieving_lockpick_chance_base(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_DOOR_10_COIN:
			return 180;
		case THIEVING_DOOR_NATURE_RUNE:
			return 140;
		case THIEVING_DOOR_ARDOUGNE_STOVE:
			return 120;
		case THIEVING_DOOR_HAM:
			return 120;
		case THIEVING_DOOR_WILDERNESS_AXE:
			return 90;
		case THIEVING_DOOR_ARDOUGNE_SEWER:
			return 90;
		case THIEVING_DOOR_WILDERNESS_PIRATE:
			return 50;
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			return 40;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			return 30;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			return 1;
		default:
			printf("ID: %d not handled! Thieving lockpick base unknown!\n", 
				   thieving_id);
			return -1;
	}
}

int get_thieving_lockpick_chance_max(int thieving_id)
{
	switch (thieving_id)
	{
		case THIEVING_DOOR_10_COIN:
		case THIEVING_DOOR_NATURE_RUNE:
		case THIEVING_DOOR_ARDOUGNE_STOVE:
		case THIEVING_DOOR_HAM:
		case THIEVING_DOOR_WILDERNESS_AXE:
		case THIEVING_DOOR_ARDOUGNE_SEWER:
		case THIEVING_DOOR_WILDERNESS_PIRATE:
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			return 240;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			return 230;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			return 180;
		default:
			printf("ID: %d not handled! Thieving lockpick max unknown!\n", 
				   thieving_id);
			return -1;
	}
}


/* FIXME: Needs rework for plural items being stolen, like "raw anchovies".
 * Should use the same line as coins.  */
static void send_thieving_message(struct client *c, int item_id, int amount)
{
	char loot_msg[100];
	char gram_msg[3];
	
	if (item_id != -1)
	{
		if (amount == 1)
		{
			/* Get the correct article for the item name.  */
			get_a_or_an(ITEMS[item_id]->name, gram_msg, 3);

			snprintf(loot_msg, 100,  "You steal %s %s.", gram_msg, 
				string_to_lower(ITEMS[item_id]->name));
		}
		else if (item_id == 995)
		{
			snprintf(loot_msg, 100, "You steal some %s.",
				string_to_lower(ITEMS[item_id]->name));
		}
		else
		{
			snprintf(loot_msg, 100, "You steal %d %ss.", amount,
				string_to_lower(ITEMS[item_id]->name));
		}

		packet_send_chatbox_message(c, loot_msg);
	}
}

/* Returns 1 on door is locked side, 0 if not.  */
static int check_thieving_door_lock_side(struct client *c)
{
	switch (c->plr->action_id_type)
	{
		case THIEVING_DOOR_10_COIN:
			if (c->plr->world_y <= c->plr->action_y)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_NATURE_RUNE:
			if (c->plr->world_y >= c->plr->action_y)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_ARDOUGNE_STOVE:
			if (c->plr->world_x <= c->plr->action_x)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_HAM:
			return 1;
		case THIEVING_DOOR_WILDERNESS_AXE:
			if (c->plr->world_y <= c->plr->action_y)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_ARDOUGNE_SEWER:
			if (c->plr->world_x <= c->plr->action_x)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_WILDERNESS_PIRATE:
			switch (c->plr->action_x)
			{
				/* West door.  */
				case 3038:
					if (c->plr->world_x < c->plr->action_x)
					{
						return 1;
					}
					break;
				/* North door.  */
				case 3041:
					if (c->plr->world_y > c->plr->action_y)
					{
						return 1;
					}
					break;
				/* East door.  */
				case 3044:
					if (c->plr->world_x > c->plr->action_x)
					{
						return 1;
					}
					break;
			}
			break;
		case THIEVING_DOOR_CHAOS_DRUID_TOWER:
			if (c->plr->world_x >= c->plr->action_x)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_ARDOUGNE_PALACE:
			if (c->plr->world_y >= c->plr->action_y)
			{
				return 1;
			}
			break;
		case THIEVING_DOOR_YANILLE_DUNGEON:
			if (c->plr->world_y < c->plr->action_y)
			{
				return 1;
			}
			break;
	}
	
	return 0;
}

/**
 * When a player tries to open a chest without checking for traps, they take
 * damage from it. This handles dealing the damage.
 * @param *c Client instance.
 * @param chest_type Chest's thieving ID, used to get the right damage.
 *
 * @todo Wrong animation, needs research. Timing may be off as well.
 * @todo Damage scaling is best guess, but could not find any resources out
 * there about this.
 */
void activate_chest_trap(struct client *c, int chest_type)
{
	double max_dmg_percent = get_thieving_chest_trap_max_damage(chest_type);
	double damage_roll = 0.0;
	double damage_dealt = 0.0;

	/* Don't trigger a trap if the player already looted this chest.  */
	if (get_inst_object_index_at_position(c, c->plr->action_x, c->plr->action_y,
									 c->plr->world_z) != -1)
	{
		packet_send_chatbox_message(c, "It looks like this chest has already been"
					 " looted.");
		return;
	}

	if (is_double_about_equal(max_dmg_percent, 0.0, EPSILON))
	{
		printf("Error! Damage not set!\n");
		return;
	}

	damage_roll = rand_double(0.01, max_dmg_percent);

	damage_dealt = ((double) c->plr->level[HITPOINTS_SKILL] * damage_roll);

	player_play_animation_no_overwrite(c, THIEVING_EMOTE_CHEST, 1);
	freeze_player(c, EFFECT_FREEZE, 1);

	packet_send_chatbox_message(c, "You have activated a trap on the chest.");

	/* Scale damage based on player's hp, and chest level.  */
	deal_damage_to_player(c, (int) ceil(damage_dealt), -1, 0, -1, -1, -1); 
}

/**
 * Check all skill levels, requirements before starting the thieving process.
 * @param *c Client instance.
 */
void init_thieving(struct client *c)
{
	/* Catch any "spam" clicking from the player, prevent overwriting of the
	 * pickpocketing action.  */
	if (c->plr->action_skill_type == THIEVING_ACTION_PICKPOCKET
		&& is_player_frozen(c, 0))
	{
		return;
	}

	if (NPC_SPAWNS[c->plr->action_index]->in_combat == 1)
	{
		packet_send_chatbox_message(c, "You can't pick their pocket now!\n");
		return;
	}

	int npc_id = NPC_SPAWNS[c->plr->action_index]->npc_id;
	int level_needed = get_thieving_level_needed(c->plr->action_id_type);
	int thieving_level = get_player_skill_level(c, THIEVING_SKILL);

	c->plr->action_skill_id = THIEVING_SKILL;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_THIEVING);

	/* Quick check here for players under the thieving level passing through
	 * doors on their non-locked side.  */
	if (c->plr->action_skill_type == THIEVING_ACTION_LOCKPICK_DOOR 
		&& check_thieving_door_lock_side(c) == 0)
	{
		packet_send_chatbox_message(c, "You pass through the door.");
		handle_auto_door_at_pos(c, c->plr->action_x, c->plr->action_y,
								c->plr->action_z);
		return;
	}

	if (thieving_level < level_needed)
	{
		/* If the player's thieving level isn't high enough for the chest,
		 * they are told there isn't any traps on it, when there really is.
		 */
		if (c->plr->action_skill_type 
			== THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST)
		{

			packet_send_chatbox_message(c, "You search the chest for traps.");
			packet_send_chatbox_message(c, "You find nothing.");
		}
		else
		{
			char error_msg[150];
			player_stop_facing_npc(c);
			snprintf(error_msg, 150, "You must be at least level %d in "
					 "thieving to do that!\n", level_needed);
			packet_send_chatbox_message(c, error_msg);
			set_player_state(c, STATE_IDLE);
		}

		return;
	}

	if (c->plr->action_skill_type == THIEVING_ACTION_PICKPOCKET)
	{
		player_stop_facing_npc(c);
		player_play_animation(c, THIEVING_EMOTE_PICKPOCKET);
		c->plr->frozen_ticks = 1;
		c->plr->frozen_type = EFFECT_FREEZE;

		if (NPCS[npc_id]->sex == 0)
		{
			packet_send_chatbox_message(c, "You attempt to pick the man's pocket.");
		}
		else
		{
			packet_send_chatbox_message(c, "You attempt to pick the woman's pocket.");
		}
		c->plr->action_ticks = 0;
	}
	else if (c->plr->action_skill_type == THIEVING_ACTION_STALL)
	{
		player_play_animation(c, THIEVING_EMOTE_STALL);
		c->plr->frozen_ticks = 1;
		c->plr->frozen_type = EFFECT_FREEZE;

		/** FIXME This is not an accurate message. There is a different message
		 * for each of the stalls.  */
		packet_send_chatbox_message(c, "You attempt to steal from the stall.");
		c->plr->action_ticks = 2;
	}
	else if (c->plr->action_skill_type 
			 == THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST)
	{
		if (get_inst_object_index_at_position(c, c->plr->action_x, c->plr->action_y,
										 c->plr->world_z) != -1)
		{
			packet_send_chatbox_message(c, "It looks like this chest has already been"
						 " looted.");
			c->plr->action_skill_id = -1;
			c->plr->action_skill_type = -1;
		}
		else
		{
			player_play_animation_no_overwrite(c, THIEVING_EMOTE_CHEST, 1);
			freeze_player(c, EFFECT_FREEZE, 1);
			packet_send_chatbox_message(c, "You search the chest for traps.");
			packet_send_chatbox_message(c, "You find a trap on the chest.");
			c->plr->action_total_time_acted = 1;
		}
	}
	else if (c->plr->action_skill_type == THIEVING_ACTION_LOCKPICK_DOOR)
	{
		/* Doors only are locked on one side, so check which side they are on
		 * before picking the lock. If they are on the side of the door that
		 * doesn't have the lock, they just walk through the door.  */
		if (check_thieving_door_lock_side(c) == 1)
		{
			double bonus = 1.0;

			int lockpick_needed 
				= get_thieving_lockpick_needed(c->plr->action_id_type);
			int lockpick_slot = player_has_item(c, THIEVING_ITEM_LOCKPICK, 1);

			/* If player doesn't have a lockpick...  */
			if (lockpick_slot == -1)
			{
				if (lockpick_needed == 1)
				{
					packet_send_chatbox_message(c, "This lock is to complex to pick without a"
								 " lockpick.");
					return;
				}
			}
			else
			{
				/* Apply a bonus if the player has a lockpick and it isn't
				 * required for this lock.  */
				if (lockpick_needed == 0)
				{
					bonus = 1.05;
				}
			}

			packet_send_chatbox_message(c, "You attempt to pick the lock.");

			/* NOTE: This formula is based off of best guesses, not official
			 * data. If official data ever becomes available, update this.
			 * May need some tweaking down the line.  */
			if (is_skill_check_success(c, THIEVING_SKILL, 
					get_thieving_lockpick_chance_base(c->plr->action_id_type),
					get_thieving_lockpick_chance_max(c->plr->action_id_type),
					bonus) == 1)
			{
				packet_send_chatbox_message(c, "You manage to pick the lock");
				handle_auto_door_at_pos(c, c->plr->action_x, c->plr->action_y,
										c->plr->action_z);
				add_skill_xp(c, THIEVING_SKILL, 
							 get_thieving_xp(c->plr->action_id_type));
			}
			else
			{
				packet_send_chatbox_message(c, "You fail to pick the lock");
			}
		}
		else
		{
			packet_send_chatbox_message(c, "You pass through the door.");
			handle_auto_door_at_pos(c, c->plr->action_x, c->plr->action_y,
									c->plr->action_z);
		}
	}
}


/**
 * Process thieving actions.
 * @param *c Client instance.
 * @todo Remove the timed action handling stuff from here, just handle it
 * through this method.
 */
void handle_thieving(struct client *c)
{
	if (c->plr->action_skill_type == THIEVING_ACTION_PICKPOCKET)
	{
		/* Calculate the chance for the player to succeed.  */
		int npc_id = NPC_SPAWNS[c->plr->action_index]->npc_id;

		/* If the player succeeded in pickpocketing.  */
		if (is_skill_check_success(c, THIEVING_SKILL, 
				get_thieving_pickpocket_chance_base(c->plr->action_id_type),
				get_thieving_pickpocket_chance_max(c->plr->action_id_type),
				1.0) == 1)
		{
			int x;
			int loot[MAX_NPCS_THIEVING_TABLES] = {0};
			int amount[MAX_NPCS_THIEVING_TABLES] = {0};

			/* Pickpocket won.  */
			if (NPCS[npc_id]->sex == 0)
			{
				packet_send_chatbox_message(c, "You pick the man's pocket.");
			}
			else
			{
				packet_send_chatbox_message(c, "You pick the woman's pocket.");
			}

			/* Get loot from each of the NPC's tables.  */
			for (x = 0; x < MAX_NPCS_THIEVING_TABLES; x++)
			{
				loot[x] = get_loot_table_item(c, 
					NPCS[npc_id]->thieving_loot_indexes[x], &amount[x]);
				
				send_thieving_message(c, loot[x], amount[x]);
			}

			/* Give the player the loot.  */
			for (x = 0; x < MAX_NPCS_THIEVING_TABLES; x++)
			{
				if (loot[x] != -1)
				{
					add_item_to_inv_drop_if_no_space(c, loot[x], amount[x], 
													 -1);
				}
			}

			add_skill_xp(c, THIEVING_SKILL, 
						 get_thieving_xp(c->plr->action_id_type));
			set_player_state(c, STATE_IDLE);
			return;
		}
		else
		{
			struct npc_spawn *npc = NULL;
			char return_msg[100];

			if (NPCS[npc_id]->sex == 0)
			{
				packet_send_chatbox_message(c, "You fail to pick the man's pocket.");
			}
			else
			{
				packet_send_chatbox_message(c, "You fail to pick the woman's pocket.");
			}

			npc = NPC_SPAWNS[c->plr->action_index];
			/*
			clear_npc_walking_queue(npc);
			npc->walks = 0;
			npc->walks_stop_ticks = 3;
			*/

			get_thieving_fail_text(c->plr->action_id_type, c->plr->name, 
								   return_msg, 100);
			/* FIXME: pickpocketing H.A.M members may also print this string
			 * to the player's chatbox. Investigate.  */
			set_npc_overhead_text(npc, return_msg);

			turn_npc_to_coord(npc, c->plr->world_x, c->plr->world_y);
			set_npc_animation(npc, THIEVING_EMOTE_PUNCH);
			player_stop_facing_npc(c);
			player_play_animation(c, THIEVING_EMOTE_BLOCK);
			freeze_player(c, EFFECT_FREEZE_STUN, 
				get_thieving_pickpocket_stun_duration( c->plr->action_id_type));
			deal_damage_to_player(c, 
				get_thieving_pickpocket_damage(c->plr->action_id_type),
				-1, 0, -1, -1, -1);

			set_player_state(c, STATE_IDLE);

			return;
		}
	}
	else if (c->plr->action_skill_type == THIEVING_ACTION_STALL)
	{
		int amount = 0;
		int rv = 0;
		int x = 0, y = 0;
		int spotted_id = -1;
		int spotted_index = 0;
		int spotted_by_owner = 0;

		/* Loop through all the local NPCs to see if they can see the player.
		 */
		for (x = 0; x < c->plr->local_npcs_num; x++)
		{
			/* Someone has spotted the player, exit loop.  */
			if (spotted_id != -1)
			{
				break;
			}

			/* If this NPC is set to guard stalls, check their LOS.  */
			if (NPCS[c->plr->local_npcs[x]->npc_id]->thieving_guard == 1)
			{
				if (check_line_of_sight(c, c->plr->local_npcs[x], 
										THIEVING_LOS_DISTANCE) == 1)
				{
					spotted_id = c->plr->local_npcs[x]->npc_id;
					spotted_index = x;
					break;
				}
			}

			/* If this NPC is the owner of the stall the player is stealing
			 * from, check their LOS.  */
			if (NPCS[c->plr->local_npcs[x]->npc_id]->objects_protected[0] 
				!= -1)
			{
				for (y = 0; y < MAX_NPCS_OBJECTS_PROTECTED; y++)
				{
					if (NPCS[c->plr->local_npcs[x]->npc_id]
							->objects_protected[y] 
						== c->plr->action_id_type2)
					{
						if (check_line_of_sight(c, c->plr->local_npcs[x], 
												THIEVING_LOS_DISTANCE) == 1)
						{
							spotted_id = c->plr->local_npcs[x]->npc_id;
							spotted_index = x;
							spotted_by_owner = 1;
							break;
						}
					}
				}
			}
		}

		/* Success.  */
		if (spotted_id == -1)
		{
			int item_id = get_loot_table_item(c, 
				OBJECTS[c->plr->action_id_type2]->loot_table_indexes[0], &amount);
			
			send_thieving_message(c, item_id, amount);

			if (item_id != -1)
			{
				char loot_msg[100];
				rv = add_item_to_inv_silent(c, item_id, amount, -1);

				/* Could not add item to inventory, full.  */
				if (rv == 0)
				{
					add_to_item_drops_exclusive(c, item_id, amount, 
												get_item_default_charges(item_id),
												c->plr->world_x, 
												c->plr->world_y,
												c->plr->world_z,
												1);
					snprintf(loot_msg, 100, 
							 "You drop the %s on the ground.",
							 string_to_lower(ITEMS[item_id]->name));
					packet_send_chatbox_message(c, loot_msg);
				}

				/* Spawn the empty stall.  */
				rv = get_inst_object_index_at_position(c, c->plr->action_x, 
										  c->plr->action_y, 
										  c->plr->action_z);

				/* If there is not an instanced object here, create it.  */
				if (rv == -1)
				{
					int delay 
						= get_thieving_stall_respawn_ticks(
							c->plr->action_id_type);
					/* Retrieve object information stuff.
					 * action_store_one = obj_id
					 * action_store_two = obj_type
					 * action_store_three = obj_face
					 */
					rv = create_inst_object(c, c->plr->action_store_one, 
										    c->plr->action_store_two, 
										    c->plr->action_x, 
										    c->plr->action_y,
										    c->plr->action_z, 
											c->plr->action_store_three, delay);
					handle_transforming_inst_object(c, rv, 
						get_thieving_stall_empty_id(c->plr->action_store_one),
						delay);
				}

				add_skill_xp(c, THIEVING_SKILL, 
							 get_thieving_xp(c->plr->action_id_type));

				c->plr->action_skill_type = -1;
			}
		}
		else
		{
			/* Failure.  */
			if (spotted_by_owner == 1)
			{
				int nearest_idx = -1;
				int nearest_dist = 99;
				int test_dist = -1;

				set_npc_overhead_text(c->plr->local_npcs[spotted_index], 
									"Guards, Guards!");
				c->plr->action_skill_type = -1;

				/* Find the closest guard around that isn't occupied.  */
				for (x = 0; x < c->plr->local_npcs_num; x++)
				{
					if (NPCS[c->plr->local_npcs[x]->npc_id]->thieving_guard 
						== 1)
					{
						/* We cannot call over any guards that are already
						 * fighting, so ignore them.  */
						if (c->plr->local_npcs[x]->in_combat != 0)
						{
							continue;
						}

						/* Get distance away, find the closest guard.  */
						if (check_good_distance(
								c->plr->local_npcs[spotted_index]->world_x, 
								c->plr->local_npcs[spotted_index]->world_y, 
								c->plr->local_npcs[x]->world_x, 
								c->plr->local_npcs[x]->world_y,
								THIEVING_GUARD_HEARING_DISTANCE) 
								== 1)
						{
							test_dist = get_distance_from_point(
								c->plr->local_npcs[spotted_index]->world_x, 
								c->plr->local_npcs[spotted_index]->world_y, 
								c->plr->local_npcs[x]->world_x, 
								c->plr->local_npcs[x]->world_y);

							if (test_dist < nearest_dist)
							{
								nearest_idx = x;
								nearest_dist = test_dist;
							}
						}
					}
				}

				/* If we found a guard to call over, bring them over.  */
				if (nearest_idx != -1)
				{
					set_npc_combat_target(
						c->plr->local_npcs[nearest_idx], -1, 
						c->plr->index);
					set_npc_overhead_text(
						c->plr->local_npcs[nearest_idx], 
						"Hey! Get your hands off there!");
					c->plr->action_skill_type = -1;
				}
			}
			else
			{
				/* Get the guard over here.  */
				set_npc_combat_target(c->plr->local_npcs[spotted_index], -1, 
									  c->plr->index);
				set_npc_overhead_text(c->plr->local_npcs[spotted_index], 
									"Hey! Get your hands off there!");
				c->plr->action_skill_type = -1;
			}
		}
	}
	else if (c->plr->action_skill_type 
			 == THIEVING_ACTION_SEARCH_FOR_TRAPS_CHEST)
	{
		int item_id = -1;
		int amount = 0;
		int rv = 0;
		int x = 0;
		char loot_msg[100];

		freeze_player(c, EFFECT_FREEZE, 0);

		switch( c->plr->action_total_time_acted)
		{
			case 3:
				packet_send_chatbox_message(c, "You disable the trap.");
				break;
			case 5:
				/* Open chest.  */
				c->plr->action_store_two = 
					create_inst_object(c, c->plr->action_id, 10, 
									   c->plr->action_x, 
									   c->plr->action_y, c->plr->world_z,
									   c->plr->action_store_one,
									   get_thieving_chest_respawn_ticks(
											c->plr->action_id_type));
				handle_transforming_inst_object(c, c->plr->action_store_two,
												THIEVING_OBJECT_OPEN_CHEST, 6);
				packet_send_chatbox_message(c, "You open the chest.");
				break;
			case 8:
				packet_send_chatbox_message(c, "You find treasure inside!");
				player_play_animation_no_overwrite(c, THIEVING_EMOTE_CHEST, 1);

				for (x = 0; x < MAX_OBJECTS_LOOT_TABLES; x++)
				{
					item_id = get_loot_table_item(c, 
							OBJECTS[c->plr->action_id]->loot_table_indexes[x], 
							&amount);
				
					if (item_id != -1)
					{
						rv = add_item_to_inv_silent(c, item_id, amount, -1);

						/* Could not add item to inventory, full.  */
						if (rv == 0)
						{
							add_to_item_drops_exclusive(c, item_id, amount, 
														get_item_default_charges(item_id),
														c->plr->world_x, 
														c->plr->world_y,
														c->plr->world_z,
														1);
							snprintf(loot_msg, 100, 
									 "You drop the %s on the ground.",
									 string_to_lower(ITEMS[item_id]->name));
							packet_send_chatbox_message(c, loot_msg);
						}
					}
				}
				break;
			case 10:
				/* close chest.  */
				c->plr->action_skill_type = -1;
				c->plr->action_skill_id = -1;
				c->plr->action_total_time_acted = -1;
				return;
		}

		c->plr->action_total_time_acted++;
	}
}

