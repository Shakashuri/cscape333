#ifndef _CSV_H_
#define _CSV_H_

struct csv_line
{
	int num_tokens;
	char **tokens;
};

struct csv_file
{
	int num_lines;
	struct csv_line **data;
};

int free_csv_line(struct csv_line *line);
int free_csv_file(struct csv_file *file);

struct csv_line *new_csv_line(void);
struct csv_file *new_csv_file(void);

void print_csv_line(struct csv_line *line);
void print_csv_line_horizontal(struct csv_line *line);
void print_csv_file(struct csv_file *file);
void print_csv_file_horizontal(struct csv_file *file);

int get_num_bytes_in_file(FILE *fp);
int get_num_lines_in_file(FILE *fp);
int get_num_delimiters_in_line(FILE *fp, char delimiter);

int add_line_to_csv_file(struct csv_file *file, struct csv_line *line);
int add_token_to_csv_line(struct csv_line *line, char *token);

struct csv_line *read_tokens_from_string(char *buffer, char *delimiters);
char *read_line_from_file(FILE *fp);

struct csv_file *read_csv_file(FILE *fp, char *delimiters);

char *get_csv_token(struct csv_file *csv, int line, int token);

#endif
