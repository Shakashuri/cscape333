#ifndef _SKILL_FLETCHING_H_
#define _SKILL_FLETCHING_H_

int get_fletching_level_needed(int fletching_id);
float get_fletching_xp(int fletching_id);

void cut_bow(struct client *c, int log_id, int bow_id, float xp, int is_long);
void string_bow(struct client *c, int ubow_id, int bow_id, int fletching_id);
void add_tips_to_bolts(struct client *c, int tips_id, int fbolts_id, 
					   int fletching_id);
void init_fletching(struct client *c);
void handle_fletching(struct client *c);

#endif
