/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"

/* Runecrafting - ID 20.  */
int get_runecrafting_level_needed(int runecrafting_id)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_RUNE_AIR:
			return 1;
		case RUNECRAFTING_RUNE_MIND:
			return 2;
		case RUNECRAFTING_RUNE_WATER:
			return 5;
		case RUNECRAFTING_RUNE_EARTH:
			return 9;
		case RUNECRAFTING_RUNE_FIRE:
			return 14;
		case RUNECRAFTING_RUNE_BODY:
			return 20;
		case RUNECRAFTING_RUNE_COSMIC:
			return 27;
		case RUNECRAFTING_RUNE_CHAOS:
			return 35;
		case RUNECRAFTING_RUNE_NATURE:
			return 44;
		case RUNECRAFTING_RUNE_LAW:
			return 54;

		/* Combo runes.  */
		case RUNECRAFTING_RUNE_MIST:
			return 6;
		case RUNECRAFTING_RUNE_DUST:
			return 10;
		case RUNECRAFTING_RUNE_MUD:
			return 13;
		case RUNECRAFTING_RUNE_SMOKE:
			return 15;
		case RUNECRAFTING_RUNE_STEAM:
			return 19;
		case RUNECRAFTING_RUNE_LAVA:
			return 23;
		
		/* Note that tiaras have no level requirement.  */
		default:
			return -1;
	}
}

float get_runecrafting_xp(int runecrafting_id)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_RUNE_AIR:
			return 5.0;
		case RUNECRAFTING_RUNE_MIND:
			return 5.5;
		case RUNECRAFTING_RUNE_WATER:
			return 6.0;
		case RUNECRAFTING_RUNE_EARTH:
			return 6.5;
		case RUNECRAFTING_RUNE_FIRE:
			return 7.0;
		case RUNECRAFTING_RUNE_BODY:
			return 7.5;
		case RUNECRAFTING_RUNE_COSMIC:
			return 8.0;
		case RUNECRAFTING_RUNE_CHAOS:
			return 8.5;
		case RUNECRAFTING_RUNE_NATURE:
			return 9.0;
		case RUNECRAFTING_RUNE_LAW:
			return 9.5;

		case RUNECRAFTING_TIARA_AIR:
			return 25.0;
		case RUNECRAFTING_TIARA_MIND:
			return 27.5;
		case RUNECRAFTING_TIARA_WATER:
			return 30.0;
		case RUNECRAFTING_TIARA_EARTH:
			return 32.5;
		case RUNECRAFTING_TIARA_FIRE:
			return 35.0;
		case RUNECRAFTING_TIARA_BODY:
			return 37.5;
		case RUNECRAFTING_TIARA_COSMIC:
			return 40.0;
		case RUNECRAFTING_TIARA_CHAOS:
			return 42.5;
		case RUNECRAFTING_TIARA_NATURE:
			return 45.0;
		case RUNECRAFTING_TIARA_LAW:
			return 47.5;
	}

	return 0.0;
}

float get_runecrafting_combo_xp(int runecrafting_id, int is_higher_altar)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_RUNE_MIST:
			if (is_higher_altar == 0)
			{
				return 8.0;
			}
			else
			{
				return 8.5;
			}
		case RUNECRAFTING_RUNE_DUST:
			if (is_higher_altar == 0)
			{
				return 8.3;
			}
			else
			{
				return 9.0;
			}
		case RUNECRAFTING_RUNE_MUD:
			if (is_higher_altar == 0)
			{
				return 9.3;
			}
			else
			{
				return 9.5;
			}
		case RUNECRAFTING_RUNE_SMOKE:
			if (is_higher_altar == 0)
			{
				return 8.5;
			}
			else
			{
				return 9.5;
			}
		case RUNECRAFTING_RUNE_STEAM:
			if (is_higher_altar == 0)
			{
				return 9.3;
			}
			else
			{
				return 10.0;
			}
		case RUNECRAFTING_RUNE_LAVA:
			if (is_higher_altar == 0)
			{
				return 10.0;
			}
			else
			{
				return 10.5;
			}
		
		default:
			return 0.0;
	}
}

int get_runecrafting_multiplier(int runecrafting_id)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_RUNE_AIR:
			return 11;
		case RUNECRAFTING_RUNE_MIND:
			return 14;
		case RUNECRAFTING_RUNE_WATER:
			return 19;
		case RUNECRAFTING_RUNE_EARTH:
			return 26;
		case RUNECRAFTING_RUNE_FIRE:
			return 35;
		case RUNECRAFTING_RUNE_BODY:
			return 46;
		case RUNECRAFTING_RUNE_COSMIC:
			return 59;
		case RUNECRAFTING_RUNE_CHAOS:
			return 74;
		case RUNECRAFTING_RUNE_NATURE:
			return 91;
		case RUNECRAFTING_RUNE_LAW:
			return 95;
		
		default:
			return 1;
	}
}

int get_runecrafting_talisman_id(int runecrafting_id)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_TIARA_AIR:
		case RUNECRAFTING_RUNE_AIR:
		case AIR_RUNE:
			return 1438;
		case RUNECRAFTING_TIARA_MIND:
		case RUNECRAFTING_RUNE_MIND:
		case MIND_RUNE:
			return 1448;
		case RUNECRAFTING_TIARA_WATER:
		case RUNECRAFTING_RUNE_WATER:
		case WATER_RUNE:
			return 1444;
		case RUNECRAFTING_TIARA_EARTH:
		case RUNECRAFTING_RUNE_EARTH:
		case EARTH_RUNE:
			return 1440;
		case RUNECRAFTING_TIARA_FIRE:
		case RUNECRAFTING_RUNE_FIRE:
		case FIRE_RUNE:
			return 1442;
		case RUNECRAFTING_TIARA_BODY:
		case RUNECRAFTING_RUNE_BODY:
		case BODY_RUNE:
			return 1446;
		case RUNECRAFTING_TIARA_COSMIC:
		case RUNECRAFTING_RUNE_COSMIC:
		case COSMIC_RUNE:
			return 1454;
		case RUNECRAFTING_TIARA_CHAOS:
		case RUNECRAFTING_RUNE_CHAOS:
		case CHAOS_RUNE:
			return 1452;
		case RUNECRAFTING_TIARA_NATURE:
		case RUNECRAFTING_RUNE_NATURE:
		case NATURE_RUNE:
			return 1462;
		case RUNECRAFTING_TIARA_LAW:
		case RUNECRAFTING_RUNE_LAW:
		case LAW_RUNE:
			return 1458;
		
		default:
			return -1;
	}
}

int get_runecrafting_infused_tiara_id(int runecrafting_id)
{
	switch (runecrafting_id)
	{
		case RUNECRAFTING_TIARA_AIR:
			return 5527;
		case RUNECRAFTING_TIARA_MIND:
			return 5529;
		case RUNECRAFTING_TIARA_WATER:
			return 5531;
		case RUNECRAFTING_TIARA_EARTH:
			return 5535;
		case RUNECRAFTING_TIARA_FIRE:
			return 5537;
		case RUNECRAFTING_TIARA_BODY:
			return 5533;
		case RUNECRAFTING_TIARA_COSMIC:
			return 5539;
		case RUNECRAFTING_TIARA_CHAOS:
			return 5543;
		case RUNECRAFTING_TIARA_NATURE:
			return 5541;
		case RUNECRAFTING_TIARA_LAW:
			return 5545;
		
		default:
			return -1;
	}
}

/**
 * Check all variables are what they need to be, needed items are held before
 * runecrafting begins.
 * @param *c Client instance.
 */
void init_runecrafting(struct client *c)
{
	int level_needed = get_runecrafting_level_needed(c->plr->action_id_type);
	int reg_essence_held = player_has_item(c, RUNECRAFTING_ESSENCE_ID, 1);
	int runecrafting_level = get_player_skill_level(c, RUNECRAFTING_SKILL);

	c->plr->action_skill_id = RUNECRAFTING_SKILL;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_RUNECRAFTING);
	
	/* Check for required level.  */
	if (runecrafting_level < level_needed)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "runecrafting to do that!\n", level_needed);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Make sure the player has some essence in their inventory.  */
	if (reg_essence_held == -1)
	{
		packet_send_chatbox_message(c, "You need rune essence to make runes out of!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Play proper animation and graphic.  */
	player_play_animation(c, RUNECRAFTING_EMOTE_RUNECRAFT);
	player_draw_gfx_at_height(c, RUNECRAFTING_GFX_RUNECRAFT, 
					RUNECRAFTING_GFX_HEIGHT_RUNECRAFT);
	c->plr->action_ticks = 4;
	return;
}

/**
 * Process creation of normal, non-combination runes.
 * @param *c Client instance.
 */
void handle_runecrafting(struct client *c)
{
	int base_mult = get_runecrafting_multiplier(c->plr->action_id_type);
	float base_xp = get_runecrafting_xp(c->plr->action_id_type);
	int reg_essence_held 
		= player_has_num_items_non_stack(c, RUNECRAFTING_ESSENCE_ID);
	int multiplier = (c->plr->level[RUNECRAFTING_SKILL] / base_mult) + 1;

	delete_item_from_inv_nonstack(c, RUNECRAFTING_ESSENCE_ID, 
								  reg_essence_held);

	if (multiplier == 0)
	{
		multiplier = 1;
	}

	switch (c->plr->action_id_type)
	{
		case RUNECRAFTING_RUNE_AIR:
			add_item_to_inv(c, AIR_RUNE, (multiplier * reg_essence_held), -1);
			packet_send_chatbox_message(c, "You bind the temple's power into air runes.");
			break;
		case RUNECRAFTING_RUNE_MIND:
			add_item_to_inv(c, MIND_RUNE, (multiplier * reg_essence_held), -1);
			packet_send_chatbox_message(c, "You bind the temple's power into mind runes.");
			break;
		case RUNECRAFTING_RUNE_WATER:
			add_item_to_inv(c, WATER_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into water runes.");
			break;
		case RUNECRAFTING_RUNE_EARTH:
			add_item_to_inv(c, EARTH_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into earth runes.");
			break;
		case RUNECRAFTING_RUNE_FIRE:
			add_item_to_inv(c, FIRE_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into fire runes.");
			break;
		case RUNECRAFTING_RUNE_BODY:
			add_item_to_inv(c, BODY_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into body runes.");
			break;
		case RUNECRAFTING_RUNE_COSMIC:
			add_item_to_inv(c, COSMIC_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into cosmic runes.");
			break;
		case RUNECRAFTING_RUNE_CHAOS:
			add_item_to_inv(c, CHAOS_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into chaos runes.");
			break;
		case RUNECRAFTING_RUNE_NATURE:
			add_item_to_inv(c, NATURE_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into nature runes.");
			break;
		case RUNECRAFTING_RUNE_LAW:
			add_item_to_inv(c, LAW_RUNE, (multiplier * reg_essence_held), 
							-1);
			packet_send_chatbox_message(c, "You bind the temple's power into law runes.");
			break;

	}

	add_skill_xp(c, RUNECRAFTING_SKILL, (float) reg_essence_held * base_xp);

	set_player_state(c, STATE_IDLE);
	return;
}

/**
 * Process creating combination runes.
 * @param *c Client instance.
 * @param runecrafting_id ID representing the runes being made.
 * @param item_id ID of the item clicked, typically the talisman.
 * @param item_slot Slot where the item is located.
 */
void create_combination_rune(struct client *c, int runecrafting_id, 
							 int item_id, int item_slot)
{
	int x;
	float xp = 0.0;
	int combo_id = 0;
	int num_to_make = 0, num_made = 0;
	int rune_id = -1, rune_slot = -1, rune_num = -1;
	int talisman_id, talisman_slot;
	int reg_essence_held 
		= player_has_num_items_non_stack(c, RUNECRAFTING_ESSENCE_ID);
	char return_msg[100];
	char binding_amulet_used = 0;

	if (reg_essence_held == 0)
	{
		packet_send_chatbox_message(c, "You don't have any rune essence to make those.");
		return;
	}

	/* What is the base altar that the player is using.  */
	if (runecrafting_id == RUNECRAFTING_RUNE_AIR)
	{
		/* Water talisman.  */
		if (item_id == 1444)
		{
			printf("Making mist runes.\n");
			combo_id = MIST_RUNE; 
			rune_id = WATER_RUNE;
		}
		/* Earth talisman.  */
		if (item_id == 1440)
		{
			printf("Making dust runes.\n");
			combo_id = DUST_RUNE;
			rune_id = EARTH_RUNE;
		}
		/* Fire talisman.  */
		if (item_id == 1442)
		{
			printf("Making smoke runes.\n");
			combo_id = SMOKE_RUNE;
			rune_id = FIRE_RUNE;
		}
		/* XP for all of these is on the lower xp, as there is no lower altar
		 * than air.  */
		xp = get_runecrafting_combo_xp(combo_id, 0);
	}
	else if (runecrafting_id == RUNECRAFTING_RUNE_FIRE)
	{
		/* Water talisman.  */
		if (item_id == 1444)
		{
			printf("Making steam runes.\n");
			combo_id = STEAM_RUNE;
			rune_id = WATER_RUNE;
		}
		/* Earth talisman.  */
		if (item_id == 1440)
		{
			printf("Making lava runes.\n");
			combo_id = LAVA_RUNE;
			rune_id = EARTH_RUNE;
		}
		/* Air talisman.  */
		if (item_id == 1438)
		{
			printf("Making smoke runes.\n");
			combo_id = SMOKE_RUNE;
			rune_id = AIR_RUNE;
		}
		/* XP for all of these is on the higher xp, as there is no higher
		 * elemental altar than fire.  */
		xp = get_runecrafting_combo_xp(combo_id, 1);
	}
	else if (runecrafting_id == RUNECRAFTING_RUNE_WATER)
	{
		/* Air talisman.  */
		if (item_id == 1438)
		{
			printf("Making mist runes.\n");
			combo_id = MIST_RUNE;
			rune_id = AIR_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 1);
		}
		/* Earth talisman.  */
		if (item_id == 1440)
		{
			printf("Making mud runes.\n");
			combo_id = MUD_RUNE;
			rune_id = EARTH_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 0);
		}
		/* Fire talisman.  */
		if (item_id == 1442)
		{
			printf("Making steam runes.\n");
			combo_id = STEAM_RUNE;
			rune_id = FIRE_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 0);
		}
	}
	else if (runecrafting_id == RUNECRAFTING_RUNE_EARTH)
	{
		/* Air talisman.  */
		if (item_id == 1438)
		{
			printf("Making dust runes.\n");
			combo_id = DUST_RUNE;
			rune_id = AIR_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 1);
		}
		/* Water talisman.  */
		if (item_id == 1444)
		{
			printf("Making mud runes.\n");
			combo_id = MUD_RUNE;
			rune_id = WATER_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 1);
		}
		/* Fire talisman.  */
		if (item_id == 1442)
		{
			printf("Making lava runes.\n");
			combo_id = LAVA_RUNE;
			rune_id = FIRE_RUNE;
			xp = get_runecrafting_combo_xp(combo_id, 0);
		}
	}

	/* Ensure player has everything they need.  */
	talisman_id = get_runecrafting_talisman_id(rune_id);

	/* If the item the player clicked was a talisman, use that.  */
	if (item_id == talisman_id)
	{
		talisman_slot = item_slot;
	}
	else
	{
		talisman_slot = player_has_item(c, talisman_id, 1);
	}
	
	/* If the item the player clicked was the runes, use those.  */
	if (item_id == rune_id)
	{
		rune_slot = item_slot;
	}
	else
	{
		rune_slot = player_has_item(c, rune_id, 1);
	}
	rune_num = c->plr->items_n[rune_slot];

	/* Error checking.  */
	if (rune_slot == -1 || rune_id == -1)
	{
		packet_send_chatbox_message(c, "You don't have any runes to combine.");
		return;
	}
	if (talisman_id == -1 || talisman_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have a talisman to use.");
		return;
	}
	
	/* Find how many they can make.  */
	if (rune_num < reg_essence_held)
	{
		num_to_make = rune_num;
	}
	else
	{
		num_to_make = reg_essence_held;
	}

	delete_item_from_inv(c, talisman_id, 1);

	/* Find how many runes the player made successfully.  */
	for (x = 0; x < num_to_make; x++)
	{
		/* If player is wearing a binding amulet with charges, make all runes.
		 * Otherwise, each rune has a 50/50 chance to be made.  */
		if (c->plr->equipment[AMULET_SLOT] == 5521 
			&& c->plr->equipment_charges[AMULET_SLOT] > 0)
		{
			num_made++;
			add_skill_xp(c, RUNECRAFTING_SKILL, xp);
			binding_amulet_used = 1;
		}
		else
		{
			switch (rand_int(0, 1))
			{
				case 0:
					num_made++;
					add_skill_xp(c, RUNECRAFTING_SKILL, xp);
					break;
			}
		}
	}

	/* Create the runes.  */
	add_item_to_inv(c, combo_id, num_made, -1);
	delete_item_from_inv_nonstack(c, RUNECRAFTING_ESSENCE_ID, 
								  reg_essence_held);
	delete_item_from_inv(c, rune_id, num_made);

	/* If the binding amulet was used, remove a charge.  */
	if (binding_amulet_used == 1)
	{
		remove_equipment_item_charges(c, AMULET_SLOT, 1);
	}

	snprintf(return_msg, 100, "You bind the Temple's power into %ss.", 
			 ITEMS[combo_id]->name);
	packet_send_chatbox_message(c, return_msg);
}

/**
 * Creates an elemental tiara for the specified element.
 * @param *c Client instance.
 * @param runecrafting_id ID representing the tiara being made.
 * @param item_id Tiara id.
 */
void infuse_tiara(struct client *c, int runecrafting_id, int item_id)
{
	int tiara_slot = -1;
	int talisman_id = -1;
	int talisman_slot = -1;
	int finished_id = -1;
	float xp = -1.0;

	/* If items are not a talisman, or a blank tiara, return.  */
	if (item_id != talisman_id && item_id != RUNECRAFTING_TIARA_BLANK_ID)
	{
		packet_send_chatbox_message(c, "Nothing interesting happens.");
		return;
	}

	/* Get data needed.  */
	tiara_slot = player_has_item(c, RUNECRAFTING_TIARA_BLANK_ID, 1);
	talisman_id = get_runecrafting_talisman_id(runecrafting_id);
	talisman_slot = player_has_item(c, talisman_id, 1);
	finished_id = get_runecrafting_infused_tiara_id(runecrafting_id);
	xp = get_runecrafting_xp(runecrafting_id);

	/* If everything needed is present, infuse the tiara.  */
	if (tiara_slot != -1 && talisman_slot != -1)
	{
		packet_send_chatbox_message(c, "You bind the power of the talisman into your Tiara.");
		delete_item_from_inv_slot(c, tiara_slot, 1);
		transform_item(c, talisman_slot, finished_id);
		add_skill_xp(c, RUNECRAFTING_SKILL, xp);
	}
}
