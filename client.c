/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file client.c
 * @brief Contains functions related to client connection handling, 
 * initialization.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include "constants.h"
#include "action_queue.h"
#include "audio.h"
#include "client.h"
#include "fileio.h"
#include "interface.h"
#include "isaac.h"
#include "item.h"
#include "stream.h"
#include "misc.h"
#include "packet.h"
#include "quests.h"
#include "player.h"
#include "regions.h"
#include "skills.h"
#include "skill_cooking.h"

/**
 * @brief Handles initial setup of player data when a new connection is found.
 *
 * @param socket_fd Socket to load the connection from.
 * @return Returns the created player struct on success.
 */
struct client *load_connection(int socket_fd)
{
	/* Allocates memory for the needed structs for each
	   connection, and puts what socket file descriptor
	   it is associated with.  */
	struct client c_empty = {0};
	struct player p_empty = {0};
	struct isaac i_empty = {0};

	struct player *plr_addr = malloc(sizeof(struct player));
	struct client *clnt_addr = malloc(sizeof(struct client));
	struct isaac *enc_addr = malloc(sizeof(struct isaac));
	struct isaac *dec_addr = malloc(sizeof(struct isaac));

	struct sockaddr_in addr;
	socklen_t addr_size = sizeof(struct sockaddr_in);

	int i;
	
	/* Ensures some sort of timeout for hung connections.  */
    struct timeval tv = {0, 10000};

	/* Zero the structs out.  */
	*clnt_addr = c_empty;
	*plr_addr = p_empty;
	*enc_addr = i_empty;
	*dec_addr = i_empty;

	clnt_addr->plr = plr_addr;
	clnt_addr->encrypt = enc_addr;
	clnt_addr->decrypt = dec_addr;
	clnt_addr->socket = socket_fd;

	clnt_addr->current_packet = 0;
	for (i = 0; i < LAST_PACKETS_SIZE; i++)
	{
		clnt_addr->last_packets[i] = 0;
	}

	/* Applies settings to the client's socket so that it uses the timeout
	   timer.  */
	setsockopt(clnt_addr->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, 
			   sizeof(struct timeval));
    
	getpeername(clnt_addr->socket, (struct sockaddr *)&addr, &addr_size);
    strncpy(clnt_addr->ip_addr, inet_ntoa(addr.sin_addr), 20);
	
	/* Set to 1 if client properly logged out.  */
	clnt_addr->plr->logged_out = 0;
	
	/* disconnect timer is set to -1 if player hasn't timed out, anything
	   else means they are timing out, and once the timer hits 0, they are
	   kicked.  */
	clnt_addr->plr->disconnect_timer = -1;

	clnt_addr->plr->name[0] = '\0';

	clnt_addr->initialized = 1;
	return clnt_addr;
}

/**
 * @brief Handles initial setup of a music client connection.
 * @param socket_fd Socket to get the connection from.
 *
 * @return Returns created music_client struct.
 */
struct music_client *load_music_connection(int socket_fd)
{
	/* Allocates memory for the needed structs for each
	   connection, and puts what socket file descriptor
	   it is associated with.  */
	struct music_client c_empty = {0};
	struct music_client *clnt_addr = malloc(sizeof(struct music_client));
	struct sockaddr_in addr;
	
	socklen_t addr_size = sizeof(struct sockaddr_in);
	
	/* Ensures some sort of timeout for hung connections.  */
    struct timeval tv = {0, 10000};

	/* Zero the structs out.  */
	*clnt_addr = c_empty;

	clnt_addr->socket = socket_fd;

	/* Applies settings to the client's socket so that it uses the timeout
	   timer.  */
	setsockopt(clnt_addr->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, 
			   sizeof(struct timeval));
    
	getpeername(clnt_addr->socket, (struct sockaddr *)&addr, &addr_size);
    strncpy(clnt_addr->ip_addr, inet_ntoa(addr.sin_addr), 20);
	
	clnt_addr->initialized = 1;
	return clnt_addr;
}

/**
 * @brief Goes through all the clients currently connected 
 * to see which matches the socket used outside of the
 * struct.
 *
 * @param socket_fd Socket id to look for a match.
 * @return Returns index of CONNECTED_CLIENTS that matches, -1 if no match was 
 * found.
 */
int get_client_socket(int socket_fd)
{
	int x;
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] == NULL)
		{
			continue;
		}
		else
		{
			if (CONNECTED_CLIENTS[x]->socket == socket_fd)
			{
				return x;
			}
		}
	}
	return -1;
}

/**
 * @brief Goes through all the ondemand clients currently connected to see 
 * which matches the socket used outside of the struct.
 *
 * @param socket_fd Socket id to look for a match.
 * @return Returns index of CONNECTED_ONDEMAND_CLIENTS that matches, -1 if no 
 * match was found.
 */
int get_music_client_socket(int socket_fd)
{
	int x;
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_MUSIC_CLIENTS[x] == NULL)
		{
			continue;
		}
		else
		{
			if (CONNECTED_MUSIC_CLIENTS[x]->socket == socket_fd)
			{
				return x;
			}
		}
	}
	return -1;
}

/**
 * Sets up player data on login, assigns default values or the last saved 
 * values for that character.
 *
 * @param *c client instance
 */
void initialize_login_stats(struct client *c)
{
	int x;

	c->frame_stack_ptr = -1;
	clear_player_update_flags(c);
	c->plr->animation_request = -1;
	c->plr->animation_request_last = -1;
	c->plr->update_required = 1;
	
	if (c->plr->new_player == 1)
	{
		for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
		{
			if (x == HITPOINTS_SKILL)
			{
				c->plr->level[x] = 10;
				c->plr->xp[x] = 1154.0;
			}
			else
			{
				c->plr->level[x] = 1;
				c->plr->xp[x] = 0;
			}

			/* Zero all temporary bonuses.  */
			c->plr->temp_bonuses[x] = 0;
		}
		
		/* TODO: Replace these with a function call.  */
		for (x = 0; x < INV_SIZE; x++)
		{
			c->plr->items[x] = -1;
			c->plr->items_n[x] = 0;
			c->plr->items_charges[x] = -1;
		}
		
		for (x = 0; x < EQUIP_SIZE; x++)
		{
			c->plr->equipment[x] = -1;
			c->plr->equipment_n[x] = 0;
			c->plr->equipment_charges[x] = -1;
		}

		for (x = 0; x < PLAYER_BANK_SIZE; x++)
		{
			c->plr->bank_items[x] = -1;
			c->plr->bank_items_n[x] = 0;
			c->plr->bank_items_charges[x] = -1;
		}

		default_look_male(c);
		c->plr->can_packet_player_change_appearance = 1;
		c->plr->running_energy = 100;
		
		/* Set player to Lumbridge, and to respawn there on death.  */
		c->plr->tele_x = 3222;
		c->plr->tele_y = 3218;
		c->plr->tele_z = 0;

		c->plr->home_x = 3222;
		c->plr->home_y = 3218;
		c->plr->home_z = 0;

		/* Init music unlocks  */
		for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
		{
			if (MUSIC_DATA[x]->unlocked_by_default == 1)
			{
				c->plr->unlocked_music[x] = 1;
			}
		}

		c->plr->old_region_id = 0;
		c->plr->region_id = get_region_id(c->plr->tele_x, c->plr->tele_y);
		c->plr->world_z = 0;
	}
	
	/* Makes sure that the client shows up to date skill data.  */
	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		packet_send_updated_skill_stats(c, x);
	}

	for (x = 0; x < HIT_QUEUE_SIZE; x++)
	{
		c->plr->hit_queued[x] = -1;
		c->plr->hit_delay[x] = -1;
		c->plr->hit_effect[x] = -1;
		c->plr->hit_from[x] = -1;
		c->plr->hit_index[x] = -1;
	}
	
	player_update_world_location(c);

	c->plr->map_region_changed = 1;
	
	c->plr->local_players_num = 0;
	c->plr->local_npcs_num = 0;
	
	c->is_active = 1;

	c->plr->fighting_style = ACCURATE_STYLE; 
	/* Combat related ints.  */
	c->plr->hit_gfx_delay = -1;
	c->plr->highest_hit_offset = -1;
	c->plr->projectile_delay = -1;
	c->plr->teleport_timer = -1;
	c->plr->poison_damage = -1;
	c->plr->player_npc_id = -1;
	c->plr->cast_spell_id = 0;
	c->plr->cast_spell_index = -1;
	c->plr->skulled_ticks = -1;
	/* Action/skill handling.  */
	c->plr->action_ticks = -1;
	c->plr->is_casting_ticks = -1;
	c->plr->interface_open = -1;
	c->plr->interface_open_sub = -1;
	c->plr->action_skill_id = -1;
	c->plr->action_spell_id = -1;
	c->plr->action_skill_type = -1;
	c->plr->action_skill_waiting_for_resource = -1;
	c->plr->action_menu_id = -1;
	c->plr->action_menu_skill_id = -1;
	c->plr->item_add_timer = -1;
	c->plr->item_add_type = -1;
	c->plr->item_add_idx = -1;
	c->plr->item_add_message = -1;
	c->plr->item_add_uninterruptable = 0;

	c->plr->destination_x = c->plr->destination_y = -1;
	c->music_idx = -1;

	c->plr->agility_course_idx = -1;
	
	c->plr->last_attack = 0;
	c->plr->retaliation_delay = -1;
	c->plr->last_action = 0;
	c->plr->targeted_player = -1;
	c->plr->targeted_npc = -1;

	/* Clear animation stuff.  */
	c->plr->animation_play_type = -1;
	c->plr->animation_stored_emote = -1;
	c->plr->animation_stored_ticks = -1;
	c->plr->animation_stored_ticks_start = -1;

	/* Clear timed action stuff.  */
	c->plr->timed_action_type = -1;
	c->plr->timed_action_ticks = -1;
	c->plr->timed_action_ticks_start = -1;
	c->plr->timed_action_store = 0;

	c->plr->frozen_ticks = 0;
	c->plr->frozen_type = EFFECT_CLEAR;
	c->plr->health_regen = 100;
	c->plr->stat_regen = 100;
	c->plr->stat_decay = 100;
	c->plr->highest_hit_offset = -1;
	c->plr->poison_immunity_timer = 0;

	/* These are set to 1 so that when they are used in combat formulas, they
	   wont set everything to 0 when multiplied.  */
	c->plr->prayer_attack_bonus = 1.0;
	c->plr->prayer_strength_bonus = 1.0;
	c->plr->prayer_defence_bonus = 1.0;
	c->plr->prayer_ranged_bonus = 1.0;
	c->plr->prayer_magic_bonus = 1.0;

	/* Set overhead icons to none, -1.  */
	c->plr->prayer_icon = -1;
	c->plr->pk_icon = -1;

	c->plr->walking_dir = -1;
	c->plr->running_dir = -1;
	/* Default when logged in.  */
	c->plr->last_walked_direction = SOUTH;

	/* Isn't this set through the equipment data? Replace this with that
	 * function call. FIXME.  */
	c->plr->standing_emote = 808;
	c->plr->turn_emote = 823;
	c->plr->turn_about_emote = 820;
	c->plr->turn_cw_emote = 821;
	c->plr->turn_ccw_emote = 822;
	c->plr->walking_emote = 819;
	c->plr->running_emote = 824;

	set_player_initial_state(c);

	reset_skilling(c);
}

/**
 * @brief Sets up the default client configuration, loads all needed 
 * interfaces.
 *
 * Used when the client passes all the login steps, and is loading into the
 * game world. Sets default variables related to interfaces, and sends 
 * interface state.
 * 
 * @param *c client instance
 */
void initialize_new_login(struct client *c)
{
	int i = 0;
	char temp[500] = {0};

	/* Sets pm chat split.  */
	packet_set_client_config_value(c, PRIVATE_MESSAGE_SPLIT, 1);
	
	/* Sets autoretaliate, needs to be inverted here for the proper 
	 * handling.  */
	packet_set_client_config_value(c, COMBAT_AUTORETAL_MODE, !c->plr->will_retaliate);
	/* Sends fighting type.  */
	packet_set_client_config_value(c, COMBAT_ATTACK_MODE, c->plr->fighting_style);
	/* Send accepts aid.  */
	packet_set_client_config_value(c, CLIENT_ACCEPT_AID, c->plr->accepts_aid);

	/* Sets client brightness to normal.  */
	packet_set_client_config_value(c, BRIGHTNESS_LEVEL, 2);
	
	/* Sets client to walking.  */
	packet_set_client_config_value(c, WALK_OR_RUN_MODE, 0);
	
	/* Reset client autocast.  */
	packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 0);

	/* Set bank to swap mode.  */
	packet_set_client_config_value(c, BANK_REARRANGE_MODE, 0);
	/* Set bank to withdraw as item mode.  */
	packet_set_client_config_value(c, BANK_WITHDRAW_MODE, 0);

	/* Setup special configuration for equipment worn.  */
	set_special_equipment_configs(c);
	
	/* Clear all prayer ids.  */
	/* @todo Move this to its own function.  */
	for (i = 83; i < (83 + PROTOCOL_NUM_PRAYERS); i++)
	{
		packet_set_client_config_value(c, i, 0);
	}

	c->plr->autocast_spell_id = 0;
	c->plr->autocasting = 0;

	c->plr->stat_regen = 100;
	c->plr->stat_decay = 100;

	packet_send_membership_status(c, 1);

	packet_set_chat_options(c, 0, 0, 0);
	c->plr->chat_type = -1;
	
	/* Set the player's skill levels and xp.  */
	for (i = 0; i < PROTOCOL_NUM_SKILLS; i++) 
	{
		packet_send_player_skill_info(c, i, c->plr->level[i], c->plr->xp[i]);
	}

	/* Reset camera.  */
	create_frame(c, SERVER_PKT_RESET_CAMERA);

	/* Set all interface tabs up.  */
	packet_show_sidebar_interface(c, 0,  2423); /* attack tab */
	packet_show_sidebar_interface(c, 1,  3917); /* skills tab */
	packet_show_sidebar_interface(c, 2,  638); /* quest tab */
	packet_show_sidebar_interface(c, 3,  3213); /* backpack tab */
	packet_show_sidebar_interface(c, 4,  1644); /* items wearing tab */
	packet_show_sidebar_interface(c, 5,  5608); /* prayer tab */
	
	/* Modern magic menu.  */
	if (c->plr->spellbook == 0)
	{
		packet_show_sidebar_interface(c, 6, 1151);
	}
	/* Ancient magic menu.  */
	else if (c->plr->spellbook == 1)
	{
		packet_show_sidebar_interface(c, 6, 12855);
	}

	packet_show_sidebar_interface(c, 7,  -1); /* clan chat */
	packet_show_sidebar_interface(c, 8,  5065); /* friend */
	packet_show_sidebar_interface(c, 9,  5715); /* ignore */
	packet_show_sidebar_interface(c, 10, 2449); /* logout tab */
	packet_show_sidebar_interface(c, 11, 4445); /* wrench tab */
	packet_show_sidebar_interface(c, 12, 147); /* run tab */
	packet_show_sidebar_interface(c, 13, 962); /* harp tab */
	
	/* Add various right-click menu stuff.  */
	if (combat_pvp_everywhere == 1 || is_player_in_wilderness(c))
	{
		packet_set_right_click_menu_options(c, 3, 1, "Attack");
	}
	
	packet_set_right_click_menu_options(c, 4, 0, "Follow");
	packet_set_right_click_menu_options(c, 5, 0, "Trade with");

	/* Welcome message.  */
	snprintf(temp, 100, "Welcome to %s.", server_name);
	packet_send_chatbox_message(c, temp);
	snprintf(temp, 100, "This server runs on CScape: https://gitlab.com/Shakashuri/cscape333");
	packet_send_chatbox_message(c, temp);
	
	/* Show character creation menu.  */
	if (c->plr->new_player == 1)
	{
		packet_show_interface(c, 3559);
	}

	packet_set_interface_text(c, "", 6067);
	packet_set_interface_text(c, "", 6071);
	
	packet_show_sidebar_interface(c, 0, 5855); /* Punch, kick, block.  */
	packet_set_interface_text(c, "Unarmed", 5857);
	
	for (i = 0; i < EQUIP_SIZE; i++)
	{
		apply_equipment_info(c, c->plr->equipment[i], i, 
							 c->plr->equipment_n[i], 
							 c->plr->equipment_charges[i]);
	}
	
	packet_send_run_energy(c);
	
	packet_reload_items_in_inventory_interface(c);
	packet_reload_items_in_equipment_interface(c);
	packet_reload_items_in_bank_interface(c);

	packet_player_update_viewport_equipment_bonuses(c);

	/* Friend list load status packet sent here.  */
	create_frame(c, SERVER_PKT_SET_FRIEND_LIST_STATUS);
	write_byte(c, 2);
	
	/* Sends some strings the client needs at login.  */
	update_quest_points(c);
	update_quest_log(c);

	/* Send music unlock status.  */
	update_unlocked_music_list(c);
	
	/* Setup bank interface text.  */
	snprintf(temp, 100, "The Bank of %s", server_name);
	packet_set_interface_text(c, temp, 5383);
	snprintf(temp, 100, "The Bank of %s - Deposit Box", server_name);
	packet_set_interface_text(c, temp, 7421);

	packet_send_total_carried_weight(c);

	send_out_stream(c);

	/* Set collision on login.  */
	add_collision(c->plr->world_x, c->plr->world_y, c->plr->world_z,
				  TAKEN_MASK);

	/* Misc functions.  */
	spawn_flour_bins(c);
	
	/* Clear action queue.  */
	action_queue_clear(c);

	c->plr->logged_in = 1;
}

/**
 * Frees all allocated data for a client.
 * @param client_index Index of the client to free in the CONNECTED_CLIENTS
 * list.
 *
 * @return Returns 0 on success.
 */
int free_player_data(int client_index)
{
	int x;

	if (CONNECTED_CLIENTS[client_index]->login_state == -1)
	{
		/* Move player to 0,0. Removes them from other player's screen.  */
		CONNECTED_CLIENTS[client_index]->plr->world_x = 0;
		CONNECTED_CLIENTS[client_index]->plr->world_y = 0;
		
		if (CONNECTED_CLIENTS[client_index]->plr->inst_objects != NULL)
		{
			for (x = 0; 
				 x < CONNECTED_CLIENTS[client_index]->plr->inst_objects_size;
				 x++)
			{
				if (CONNECTED_CLIENTS[client_index]->plr->inst_objects[x] 
					!= NULL)
				{
					free(CONNECTED_CLIENTS[client_index]
						 ->plr->inst_objects[x]);
				}
			}

			free(CONNECTED_CLIENTS[client_index]->plr->inst_objects);
			free(CONNECTED_CLIENTS[client_index]->plr->inst_objects_on_ground);
		}

		free(CONNECTED_CLIENTS[client_index]->plr);
		free(CONNECTED_CLIENTS[client_index]->encrypt);
		free(CONNECTED_CLIENTS[client_index]->decrypt);
		free(CONNECTED_CLIENTS[client_index]);
		CONNECTED_CLIENTS[client_index] = NULL;
		CURRENT_PLAYERS -= 1;
	}
	else
	{
		if (CONNECTED_CLIENTS[client_index]->plr != NULL)
		{
			free(CONNECTED_CLIENTS[client_index]->plr);
			CURRENT_PLAYERS -= 1;
		}
		if (CONNECTED_CLIENTS[client_index]->encrypt != NULL)
		{
			free(CONNECTED_CLIENTS[client_index]->encrypt);
		}
		if (CONNECTED_CLIENTS[client_index]->decrypt != NULL)
		{
			free(CONNECTED_CLIENTS[client_index]->decrypt);
		}
		free(CONNECTED_CLIENTS[client_index]);
		CONNECTED_CLIENTS[client_index] = NULL;
	}

	return 0;
}

/**
 * @brief Checks if a connection needs to be closed.
 * @param index Index of the client in the CONNECTED_CLIENTS array to test.
 *
 * @return Returns 1 if a connection was closed, 0 if not.
 * @todo Add a function to clean up loose ends when logging out.
 */
int clean_up_connection(int index)
{
	/* Checks the status of a client, if they logged out properly or if they
	   just closed the game without logout.  */
	if (CONNECTED_CLIENTS[index]->plr->logged_out == 1)
	{
		printf("Player %s has logged out.\n", 
			   CONNECTED_CLIENTS[index]->plr->name);
		save_player_data(CONNECTED_CLIENTS[index]);
		close(CONNECTED_CLIENTS[index]->socket);
		free_player_data(index);
		return 1;	
	}
	else if (CONNECTED_CLIENTS[index]->plr->unclean_logout == 1)
	{
		if (CONNECTED_CLIENTS[index]->plr->disconnect_timer == -1)
		{
			printf("Player %s has disconnected (unclean)!\n", 
				   CONNECTED_CLIENTS[index]->plr->name);
			save_player_data(CONNECTED_CLIENTS[index]);
			CONNECTED_CLIENTS[index]->plr->disconnect_timer = TIMEOUT_TICKS;
		}
		else if (CONNECTED_CLIENTS[index]->plr->disconnect_timer == 0)
		{
			save_player_data(CONNECTED_CLIENTS[index]);
			printf("Kicked disconnected player %s.\n", 
				   CONNECTED_CLIENTS[index]->plr->name);
			close(CONNECTED_CLIENTS[index]->socket);
			free_player_data(index);
			return 1;	
		}
		else
		{
			/* Decrease disconnect timer if not in combat, 
			   but reset it if so.  */
			if (CONNECTED_CLIENTS[index]->plr->last_combat_time > 0)
			{
				CONNECTED_CLIENTS[index]->plr->disconnect_timer--;
			}
			else
			{
				CONNECTED_CLIENTS[index]->plr->disconnect_timer 
					= TIMEOUT_TICKS;
			}
		}
	}
	return 0;
}
