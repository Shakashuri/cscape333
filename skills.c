/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file skills.c
 * @brief Contains generic skill handing related functions.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <math.h>

#include "constants.h"

#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "dialogue.h"
#include "dialogue_handler.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "npc.h"
#include "object.h"
#include "object_handler.h"
#include "packet.h"
#include "pathfinding.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "skill_agility.h"
#include "skill_cooking.h"
#include "skill_crafting.h"
#include "skill_firemaking.h"
#include "skill_fishing.h"
#include "skill_fletching.h"
#include "skill_herblore.h"
#include "skill_magic.h"
#include "skill_mining.h"
#include "skill_prayer.h"
#include "skill_runecrafting.h"
#include "skill_smithing.h"
#include "skill_thieving.h"
#include "skill_woodcutting.h"
#include "stream.h"

/**
 * Interface slots for each skill in the stats menu, level text interface.
 * XP slot is just skill_lvl_stats_id + 1.
 */
int skill_lvl_stats_id[PROTOCOL_NUM_SKILLS] = {4004, 4008, 4006, 4016, 4010,
	4012, 4014, 4034, 4038, 4026, 4032, 4036, 4024, 4030, 4028, 4020, 4018,
	4022, 12166, 13926, 4152};
/**
 * Interface slots for each skill in the stats menu, total xp interface.
 * XP needed slot is just skill_total_xp_id + 1.
 */
int skill_total_xp_id[PROTOCOL_NUM_SKILLS] = {4044, 4056, 4050, 4080, 4062,
	4068, 4074, 4134, 4146, 4110, 4128, 4140, 4104, 4122, 4116, 4092, 4086,
	4098, 12171, 13921, 4157};

/**
 * Adds xp gained in a skill to a client, shows the level up dialogue if a new
 * level in the skill has been reached.
 * @param *c Client instance.
 * @param skill_id Skill to add the xp to.
 * @param xp How much xp to add.
 */
void add_skill_xp(struct client *c, int skill_id, float xp)
{
	if (skill_id < 0 || skill_id >= PROTOCOL_NUM_SKILLS)
	{
		return;
	}

	int old_level;

	if ((c->plr->xp[skill_id] + xp) > 200000000.0
	    || (c->plr->xp[skill_id] + xp < 0.0))
	{
		return;
	}
	
	/* server_xp_multiplier can be changed in data/server.conf to affect 
	   the xp rate.  */
	old_level = get_level_for_xp(c->plr->xp[skill_id]);
	c->plr->xp[skill_id] += (xp * server_xp_multiplier);

	packet_send_player_skill_info(c, skill_id, c->plr->level[skill_id], 
					  c->plr->xp[skill_id]);
	
	/* Check for a level up.  */
	if (get_level_for_xp(c->plr->xp[skill_id]) > old_level)
	{
		/** @bug I don't think hitpoints restore when you level, but need to
		 * double check anyways.  */
		if (skill_id != HITPOINTS_SKILL)
		{
			c->plr->base_level[skill_id] 
				= get_level_for_xp(c->plr->xp[skill_id]);
		}

		if (skill_id != HITPOINTS_SKILL && skill_id != PRAYER_SKILL)
		{
			c->plr->level[skill_id] = c->plr->base_level[skill_id];
		}
		
		c->plr->needs_level_up_dialogue[skill_id] = 1;

		/* If an interface is open right now, queue up the stuff.  */	
		if (c->plr->interface_open == -1)
		{
			c->plr->chat_type = LEVEL_UP_CHAT;
			c->plr->next_chat_id = 0;
			handle_dialogue(c);
		}
	}

	packet_send_updated_skill_stats(c, skill_id);	
}

/**
 * Checks to see if there is another level up event that needs to be shown to
 * the client.
 * @param *c Client instance to check.
 * @return Returns -1 if there are no level up events left, anything else is
 * the skill ID of a skill that needs to be handled.
 */
int get_next_level_up_to_handle(struct client *c)
{
	int x = 0;

	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		if (c->plr->needs_level_up_dialogue[x] == 1)
		{
			return x;
		}
	}

	return -1;
}

/**
 * Sets a player's skill interface in the client.
 * @param *c Client instance.
 * @param skill_id Skill id to send the information for.
 * @param current_level Level of the skill.
 * @param xp Total xp of the skill.
 */
void packet_send_player_skill_info(struct client *c, int skill_id, 
		int current_level, float xp)
{
	create_frame(c, SERVER_PKT_SET_PLAYER_SKILLS);
	write_dword_v1(c, (int) xp);
	write_byte_c(c, skill_id);
	write_byte_c(c, current_level);
}

/**
 * Adds up a player's total levels across all skills and return the total.
 * @param *c Client instance.
 * @return Player's total level.
 */
int get_total_levels(struct client *c)
{
	int x, total_levels = 0;

	for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
	{
		total_levels += get_level_for_xp(c->plr->xp[x]);
	}

	return total_levels;
}

/**
 * Sets up the xp table with all the values it needs. Uses the same formula
 * that the original game uses.
 */
void init_xp_table(void)
{
	int lvl;
	int points = 0;
	int output = 0;
	
	/* Original XP scaling formula.  */
	for (lvl = 1; lvl <= 99; lvl++) 
	{
		points += floor(lvl + 300.0 * pow(2.0, lvl / 7.0));
		output = (int) floor(points / 4.0);
		
		XP_FOR_LEVEL[lvl] = output;
	}
}

/**
 * Returns the level of a skill based on xp.
 * @param xp Experience value to check level for.
 * @return Level for the xp passed.
 */
int get_level_for_xp(float xp) 
{
	int lvl;

	if (xp > 13034430.0)
	{
		return 99;
	}

	for (lvl = 1; lvl <= 99; lvl++) 
	{
		if (XP_FOR_LEVEL[lvl] > ((int) xp)) 
		{
			return lvl;
		}
	}

	return 1;
}

/**
 * Returns the xp for a level.
 * @param level Level to check experience for.
 * @return XP amount for a level.
 */
int get_xp_needed_for_level(int level)
{
	if (level > 99 || level < 0)
	{
		return -1;
	}
	else
	{
		return XP_FOR_LEVEL[level];
	}
}

/**
 * This sends to the client what each skill is at, xp to next level, etc.
 * @param *c Client instance.
 * @param skill_id Skill to update the data for.
 */
void packet_send_updated_skill_stats(struct client *c, int skill_id) 
{
	if (skill_id < 0 || skill_id >= PROTOCOL_NUM_SKILLS)
	{
		return;
	}

	/* Used for prayer point levels.  */
	char skill_string[15];

	int skill_lvl = (c->plr->level[skill_id] + c->plr->temp_bonuses[skill_id]);
	int skill_xp = (int) c->plr->xp[skill_id];
	int skill_lvl_from_xp = get_level_for_xp((float) skill_xp);
	int skill_xp_from_lvl = get_xp_needed_for_level(skill_lvl_from_xp);

	/* Send all the skill update info.  */
	packet_set_interface_text_int(c, skill_lvl, skill_lvl_stats_id[skill_id]);
	packet_set_interface_text_int(c, skill_lvl_from_xp, 
						   (skill_lvl_stats_id[skill_id] + 1));
	packet_set_interface_text_int(c, skill_xp, skill_total_xp_id[skill_id]);
	packet_set_interface_text_int(c, skill_xp_from_lvl, 
						   (skill_total_xp_id[skill_id] + 1));

	/* Update specific prayer information, prayer point string.  */
	if (skill_id == PRAYER_SKILL)
	{
		snprintf(skill_string, 15, "%d/%d", skill_lvl, 
				 skill_lvl_from_xp);
		packet_set_interface_text(c, skill_string, 687);
	}

	packet_send_player_skill_info(c, skill_id, skill_lvl, skill_xp);
}

static void clear_skilling_variables(struct client *c)
{
	c->plr->action_index = -1;
	c->plr->action_item_id = -1;
	c->plr->action_item_id_2 = -1;
	c->plr->action_item_slot = -1;
	c->plr->action_item_slot_2 = -1;
	c->plr->action_item_space_needed = -1;
	c->plr->action_id_type = -1;
	c->plr->action_id_type2 = -1;
	c->plr->action_menu_id = -1;
	c->plr->action_menu_skill_id = -1;
	c->plr->chat_type = -1;
	c->plr->action_ticks = -1;
	c->plr->action_skill_type = -1;
	c->plr->action_skill_result_item = -1;
	c->plr->action_skill_remaining_actions = -1;
	c->plr->action_skill_waiting_for_resource = -1;

	/* Reset skill items.  */
	c->plr->skill_tool_id = -1;
	c->plr->skill_tool_slot = -1;
	c->plr->skill_tool_equip = -1;
	c->plr->skill_req_one_id = -1;
	c->plr->skill_req_two_id = -1;
	c->plr->skill_req_three_id = -1;
	c->plr->skill_req_four_id = -1;

	c->plr->skill_tool_emote = -1;
	c->plr->skill_tool_bonus = -1;
	c->plr->skill_tool_time = -1;
}

/**
 * Resets all skill related variables for a client, and resets any animations
 * that may be going on.
 * @param *c Client to act on.
 * @see reset_skilling_no_animation_reset
 */
void reset_skilling(struct client *c)
{
	clear_skilling_variables(c);	
	player_replay_animation(c);
}

/**
 * Resets all skill related variables for a client, but doesn't reset any
 * animations.
 * @param *c Client to act on.
 * @see reset_skilling
 */
void reset_skilling_no_animation_reset(struct client *c)
{
	clear_skilling_variables(c);	
}

/**
 * Stops all clients in the area of a client that are action upon the same
 * object. This is used for when someone chops down a tree, and everyone
 * else is stopped.
 * @param *c Client instance used.
 * @param index Skill index to reset based on.
 */
void reset_others_skilling(struct client *c, int index)
{
	int x;

	for (x = 0; x < MAX_LOCAL_PLAYERS; x++)
	{
		if (c->plr->local_players[x] != NULL)
		{
			if (c->plr->local_players[x]->plr->action_index == index)
			{
				player_reset_animation(c);
				set_player_state(c, STATE_IDLE);
				/*
				reset_skilling(c->plr->local_players[x]);
				*/
			}
		}
	}
}

int is_player_state_type_skilling(struct client *c)
{
	if ((c->plr->state_type & ACTION_TYPE_SKILLING_BIT) != 0)
	{
		return 1;
	}

	return 0;
}

/**
 * Allows for temporary skill bonuses to be added, used for potions and certain
 * foods.
 * @param *c Client instance.
 * @param skill_id Skill to add the temp bonus to.
 * @param amount Amount to add to the skill, can be positive or negative.
 */
void add_temp_skill_modifier(struct client *c, int skill_id, int amount)
{
	if (skill_id < 0 || skill_id >= PROTOCOL_NUM_SKILLS)
	{
		return;
	}

	/* Set the temp bonus amount to the new number if it is higher than the
	 * old.  */
	if ((amount > 0) && (amount > c->plr->temp_bonuses[skill_id]))
	{
		c->plr->temp_bonuses[skill_id] = amount;
		packet_send_updated_skill_stats(c, skill_id);
	}

	/* Set the temp bonus amount to the new number if it is lower than the
	 * old, negative "bonus".  */
	if ((amount < 0) && (amount < c->plr->temp_bonuses[skill_id]))
	{
		if (c->plr->level[skill_id] + amount < 0)
		{
			amount = (-1 * c->plr->level[skill_id]);
		}

		c->plr->temp_bonuses[skill_id] = amount;
		packet_send_updated_skill_stats(c, skill_id);
	}
}

/**
 * Allows for temporary skill bonuses to be added, used for potions and certain
 * foods. Allows for stacking the bonus, used for certain drinks.
 * @param *c Client instance.
 * @param skill_id Skill to add the temp bonus to.
 * @param amount Amount to add to the skill, can be positive or negative.
 */
void add_temp_skill_modifier_stacking(struct client *c, int skill_id, 
									  int amount)
{
	if (skill_id < 0 || skill_id >= PROTOCOL_NUM_SKILLS)
	{
		return;
	}

	/* Don't allow the skill to be brought down below 0.  */
	if (c->plr->level[skill_id] + amount < 0)
	{
		amount = (-1 * c->plr->level[skill_id]);
	}

	c->plr->temp_bonuses[skill_id] += amount;
	packet_send_updated_skill_stats(c, skill_id);
}

/**
 * Returns the level a player has in a skill with the bonuses factored in.
 * @param *c Client instance.
 * @param skill_id Skill to get the level for.
 * 
 * @return Returns the effective level a player has in a skill, -1 if the skill
 * id was invalid.
 */
int get_player_skill_level(struct client *c, int skill_id)
{
	if (skill_id < 0 || skill_id >= PROTOCOL_NUM_SKILLS)
	{
		return -1;
	}

	return (c->plr->level[skill_id] + c->plr->temp_bonuses[skill_id]);
}

/**
 * Returns if the player succeeded a skill check when the chances are based
 * around numerators over 255 (unsigned char).
 * @param *c Client doing the skill roll.
 * @param skill_id ID of the skill being tested.
 * @param base Chance out of 255 for success when the player is at level 1 in
 * the tested skill.
 * @param max Chance out of 255 for success when the player is at level 99 in
 * the tested skill.
 * @param bonus_amount Amount to multiply the base and max chance by. 1.0 if 
 * no change.
 * @return Returns 1 if the player succeeded the skill roll, 0 if not.
 */
int is_skill_check_success(struct client *c, int skill_id, int base, 
									 int max, double bonus_amount)
{
	int skill_level = get_player_skill_level(c, skill_id);

	if (skill_level == -1)
	{
		printf("Invalid skill id! %d\n", skill_id);
		return 0;
	}

	double base_chance = (((double) base * bonus_amount) / 255.0);
	double max_chance = (((double) max * bonus_amount) / 255.0);

	double each_level_inc = ((max_chance - base_chance) / 99.0);
	double roll = rand_double(0.0, 1.0);

	double final_chance = (base_chance + (each_level_inc * skill_level));

	printf("roll: %2.4f chance: %2.4f bonus: %2.2f\n", roll, final_chance, 
		   bonus_amount);

	if (roll <= final_chance)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
