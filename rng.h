#ifndef _RNG_H_
#define _RNG_H_

uint64_t xoroshiro128plus(void);
uint64_t splitmix64(int seed);

void initialize_rng(int seed);
int rand_int(int min, int max);
double rand_double(double min, double max);

#endif
