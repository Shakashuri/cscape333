#ifndef _SKILL_COOKING_H_
#define _SKILL_COOKING_H_

int get_cooking_level_needed(int cooking_id);
float get_cooking_xp(int cooking_id);
int get_cooking_valid_raw_meats(int food_id);
int get_cooking_raw_id(int cooking_id);
int get_cooking_cooked_id(int cooking_id);
int get_cooking_burnt_id(int cooking_id);
int get_cooking_level_stop_burn(int cooking_id, int has_cooking_gauntlets);
int get_cooking_fire_type(int object_id);
int get_cooking_needs_range(int cooking_id);

int calculate_cooking_burn_chance(struct client *c, int cooking_id, int bonus);
int milk_dairy_cow(struct client *c);
void spawn_flour_bins(struct client *c);
void hopper_controls(struct client *c);
void handle_flour_bins(struct client *c);
void create_dough(struct client *c, int choice);
void init_cooking(struct client *c);
void handle_cooking(struct client *c);
#endif
