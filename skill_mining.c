/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
/* MINING - ID 14.  */
int get_mining_level_needed(int mining_id)
{
	switch (mining_id)
	{
		case MINING_RUNE_ESSENCE:
		case MINING_CLAY:
		case MINING_COPPER:
		case MINING_TIN:
			return 1;
		case MINING_BLURITE:
			return 10;
		case MINING_IRON:
			return 15;
		case MINING_SILVER:
			return 20;
		case MINING_COAL:
			return 30;
		case MINING_GOLD:
		case MINING_GEM:
			return 40;
		case MINING_MITHRIL:
			return 55;
		case MINING_ADAMANT:
			return 70;
		case MINING_RUNE:
			return 85;
		default:
			printf("ID: %d not handled! Mining lvl unknown!\n", mining_id);
			return -1;
	}
}

float get_mining_xp(int mining_id)
{
	switch (mining_id)
	{
		case MINING_RUNE_ESSENCE:
		case MINING_CLAY:
			return 5.0;
		case MINING_TIN:
		case MINING_COPPER:
		case MINING_BLURITE:
			return 17.5;
		case MINING_IRON:
			return 35.0;
		case MINING_SILVER:
			return 40.0;
		case MINING_COAL:
			return 50.0;
		case MINING_GOLD:
			return 65.0;
		case MINING_GEM:
			return 65.0;
		case MINING_MITHRIL:
			return 80.0;
		case MINING_ADAMANT:
			return 95.0;
		case MINING_RUNE:
			return 125.0;
		default:
			printf("ID: %d not handled! Mining xp unknown!\n", mining_id);
			return 0.0;
	}
}

int get_mining_ore_id(int mining_id)
{
	switch (mining_id)
	{
		case MINING_RUNE_ESSENCE:
			return 1436;
		case MINING_CLAY:
			return 434;
		case MINING_COPPER:
			return 436;
		case MINING_TIN:
			return 438;
		case MINING_BLURITE:
			return 668;
		case MINING_IRON:
			return 440;
		case MINING_SILVER:
			return 442;
		case MINING_COAL:
			return 453;
		/* Note that gem rocks is unlisted here, that is handled through a loot
		 * table instead.  */
		case MINING_GOLD:
			return 444;
		case MINING_MITHRIL:
			return 447;
		case MINING_ADAMANT:
			return 449;
		case MINING_RUNE:
			return 451;
		default:
			printf("ID: %d not handled! Mining ore id unknown!\n", mining_id);
			return -1;
	}
}

char *get_mining_ore_name(int mining_id, int ore_id)
{
	switch (mining_id)
	{
		case MINING_RUNE_ESSENCE:
			return "essence";
		case MINING_CLAY:
			return "clay";
		case MINING_TIN:
			return "tin";
		case MINING_COPPER:
			return "copper";
		case MINING_BLURITE:
			return "blurite";
		case MINING_IRON:
			return "iron";
		case MINING_SILVER:
			return "silver";
		case MINING_COAL:
			return "coal";
		case MINING_GOLD:
			return "gold";
		case MINING_GEM:
			/* For some reason, gem rocks are handled differently.  */
			switch (ore_id)
			{
				case 1617:
					return "a Diamond";
				case 1619:
					return "a Ruby";
				case 1621:
					return "an Emerald";
				case 1623:
					return "a Sapphire";
				case 1625:
					return "an Opal";
				case 1627:
					return "a Jade";
				case 1629:
					return "a Red topaz";
				default:
					printf("ID: %d not handled! Mining ore name unknown!\n", 
						   mining_id);
					printf("Ore id: %d not handled!\n", ore_id);
					return "null";
			}
			break;
		case MINING_MITHRIL:
			return "mithril";
		case MINING_ADAMANT:
			return "adamantite";
		case MINING_RUNE:
			return "runite";

		default:
			printf("ID: %d not handled! Mining ore name unknown!\n", 
				   mining_id);
			return "null";
	}
}

float get_mining_chance(int mining_id, int mining_level)
{
	switch (mining_id)
	{
		/* Rune essence, clay, tin, copper, bluerite, and iron supposedly
		 * all use the same rate.  */
		case MINING_RUNE_ESSENCE:
		case MINING_CLAY:
		case MINING_TIN:
		case MINING_COPPER:
		case MINING_BLURITE:
		case MINING_IRON:
			return (mining_level * 0.0085) + 0.45;
		case MINING_SILVER:
			return (mining_level * 0.0085);
		case MINING_COAL:
			return (mining_level * 0.004);
		case MINING_GOLD:
		/* Not sure if it really goes here, but the level req fits.  */
		case MINING_GEM:
			return (mining_level * 0.003);
		case MINING_MITHRIL:
			return (mining_level * 0.002);
		case MINING_ADAMANT:
			return (mining_level * 0.001);
		case MINING_RUNE:
			return (mining_level * 0.0008);
		default:
			printf("ID: %d not handled! Mining chance unknown!\n", mining_id);
			return -1;
	}
}

int get_mining_respawn_ticks(int mining_id)
{
	switch (mining_id)
	{
		/* Rune essence never runs out.  */
		case MINING_RUNE_ESSENCE:
			return -1;
		case MINING_CLAY:
			return 2;
		case MINING_TIN:
		case MINING_COPPER:
			return 4;
		case MINING_BLURITE:
			return 33;
		case MINING_IRON:
			return 10;
		case MINING_SILVER:
			return 123;
		case MINING_COAL:
			return 53;
		case MINING_GOLD:
			return 123;
		case MINING_GEM:
			return 175;
		case MINING_MITHRIL:
			return 246;
		case MINING_ADAMANT:
			return 500;
		case MINING_RUNE:
			return 1500;
		default:
			printf("ID: %d not handled! Mining respawn ticks unknown!\n",
				   mining_id);
			return -1;
	}
}


/**
 * Sets up all the needed variables for mining, bonuses for player's
 * pickaxe used, checking skill requirements, setting animation.
 * @param *c Client instance.
 */
void init_mining(struct client *c)
{
	int mining_level = get_player_skill_level(c, MINING_SKILL);

	/* Set action related ids.  */
	c->plr->action_skill_id = MINING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_MINING);
	
	/* Check inventory space.  */
	if (get_free_inv_slots(c) == 0)
	{
		packet_send_chatbox_message(c, "You don't have any room for ore!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	c->plr->action_skill_lvl_req 
		= get_mining_level_needed(c->plr->action_id_type);

	/* Check if level requirements are met.  */
	if (mining_level < c->plr->action_skill_lvl_req)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "mining to mine that!\n", 
				 c->plr->action_skill_lvl_req);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Now we check if they have the needed equipment, and if they are the 
	   level to use it.  */
	if (c->plr->equipment[WEAPON_SLOT] <= 1275
		&& c->plr->equipment[WEAPON_SLOT] >= 1265)
	{
		/* Player has a mining pick equiped, use that.  */
		c->plr->skill_tool_id = c->plr->equipment[WEAPON_SLOT];
		c->plr->skill_tool_slot = WEAPON_SLOT;
		c->plr->skill_tool_equip = 1;
	}
	else
	{
		int x;
		/* Loops through the player's inventory to see if they have an axe to
		   use.  */
		for (x = 0; x < INV_SIZE; x++)
		{
			if (c->plr->items[x] <= 1275 && c->plr->items[x] >= 1265
				&& ITEMS[c->plr->items[x]]->noted != 1)
			{
				c->plr->skill_tool_id = c->plr->items[x];
				c->plr->skill_tool_slot = x;
				c->plr->skill_tool_equip = 0;
				break;
			}
		}
	}

	if (c->plr->skill_tool_id != -1)
	{
		switch (c->plr->skill_tool_id)
		{
			/* Bronze.  */
			case 1265:
				c->plr->skill_tool_emote = 625;
				c->plr->skill_tool_time = 8;
				break;
			/* Iron.  */
			case 1267:
				c->plr->skill_tool_emote = 626;
				c->plr->skill_tool_time = 7;
				break;
			/* Steel.  */
			case 1269:
				c->plr->skill_tool_emote = 627;
				c->plr->skill_tool_time = 6;
				break;
			/* Mithril.  */
			case 1273:
				c->plr->skill_tool_emote = 629;
				c->plr->skill_tool_time = 5;
				break;
			/* Adamant.  */
			case 1271:
				c->plr->skill_tool_emote = 628;
				c->plr->skill_tool_time = 4;
				break;
			/* Rune.  */
			case 1275:
				c->plr->skill_tool_emote = 624;
				c->plr->skill_tool_time = 3;
				break;
		}

		if (mining_level 
			< ITEMS[c->plr->skill_tool_id]->skill_use_req
			&& c->plr->skill_tool_emote != -1)
		{
			char error_msg[150];
			snprintf(error_msg, 150, "You must be at least level %d in "
					 "mining to use that pick!\n", 
					 ITEMS[c->plr->skill_tool_id]->skill_use_req);
			packet_send_chatbox_message(c, error_msg);
			set_player_state(c, STATE_IDLE);
			return;
		}
		else
		{
			if (c->plr->action_id_type == MINING_RUNE_ESSENCE)
			{
				c->plr->action_ticks = 0;
			}
			else
			{
				c->plr->action_ticks = c->plr->skill_tool_time;
			}

			/* It seems like different picks have different animation lengths.
			 * This should adjust for it.  */
			player_play_animation_repeats(c, c->plr->skill_tool_emote,
								  c->plr->skill_tool_time);
			
			packet_send_chatbox_message(c, "You swing your pick at the rock.");
		}
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have a pick to use!");
		set_player_state(c, STATE_IDLE);
		return;
	}
}

/**
 * Handles chance for mining ore.
 * @param *c Client instance.
 */
void handle_mining_ore(struct client *c)
{
	int effective_mn_level = get_player_skill_level(c, MINING_SKILL);
	int ore_respawn_ticks 
		= get_mining_respawn_ticks(c->plr->action_id_type);

	/* Rune essence mining is handled differently.  */
	if (c->plr->action_id_type == MINING_RUNE_ESSENCE)
	{
		/** Checks if the player gets rune essence this tick or not.  */
		int rune_essence_tracker = 0;

		/* Mining essence is based on the pick used. Amount of essence mined
		 * is based off of each pick's interval, and with one tick delay. If
		 * the player is using a bronze pickaxe, that has a time interval of 7.
		 * For essence, it means they will get essence 7 times, and pause on 8.
		 * Rune has an interval of 2 ticks, which means they get 2 essence per
		 * three game ticks.  */

		/* If the server has the default, jagex like method.  */
		if (skills_mining_rune_essence_method == 0)
		{
			rune_essence_tracker = c->plr->action_total_time_acted 
										% c->plr->skill_tool_time;
		}
		else
		{
			rune_essence_tracker = c->plr->action_total_time_acted 
										% (11 - c->plr->skill_tool_time);
		}

		if (rune_essence_tracker != 0)
		{
			/* Add ore to inv.  */
			if (add_item_to_inv(c, get_mining_ore_id(c->plr->action_id_type),
								1, -1) == 1)
			{
				packet_send_chatbox_message(c, "You get some rune essence.");
				/* Gives the player xp proper to the ore.  */
				add_skill_xp(c, MINING_SKILL, 
							 get_mining_xp(c->plr->action_id_type));
			}
			else
			{
				c->plr->action_skill_type = -1;
				set_player_state(c, STATE_IDLE);
				return;
			}
		}

		/* We want to get right back to here, no delay when mining essence.  */
		c->plr->action_ticks = 0;
	}
	else
	{
		/* Roll for mine.  */
		double roll = rand_double(0.0, 1.0);
		double mine_chance = get_mining_chance(c->plr->action_id_type, 
											   effective_mn_level);
		/* If mining has succeeded.  */
		if (roll < mine_chance)
		{
			int obj_index, ore_drained_obj_id;

			/* Check if there already is a depleted rock at the location.  */
			obj_index = get_object_spawn_index(c->plr->action_x, 
						c->plr->action_y, c->plr->world_z);

			/* If rock has been depleted, stop mining.  */
			if (obj_index != -1)
			{
				set_player_state(c, STATE_IDLE);
				return;
			}

			/* Whenever ore is mined, there is a chance to get an uncut 
			 * gem.  */
			if (rand_int(0, 255) == 0)
			{
				int gem_amount = 0;
				int gem_id = get_loot_table_item(c, MINING_PRECIOUS_GEM_TABLE,
												 &gem_amount);
				add_item_to_inv_drop_if_no_space(c, gem_id, gem_amount, -1);

				/* Gem xp is 65.0.  */
				add_skill_xp(c, MINING_SKILL, 65.0);
				packet_send_chatbox_message(c, "You find a gem in the rock!");
			}
			else
			{
				int ore_id = 0;
				int ore_count = 0;

				if (c->plr->action_id_type == MINING_GEM)
				{
					ore_id = get_loot_table_item(c, MINING_GEM_ROCK_TABLE,
												 &ore_count);
				}
				else
				{
					ore_id = get_mining_ore_id(c->plr->action_id_type);
					ore_count = 1;
				}

				/* Add ore to inv.  */
				if (add_item_to_inv(c, ore_id, ore_count, -1) == 1)
				{
					char return_msg[100];

					if (c->plr->action_id_type != MINING_GEM)
					{
						snprintf(return_msg, 100, "You manage to mine some %s.", 
							get_mining_ore_name(c->plr->action_id_type, ore_id)
							);
					}
					else
					{
						/* The get_mining_ore_name handles the grammatical 
						 * article needed.  */
						snprintf(return_msg, 100, "You just mined %s!", 
							get_mining_ore_name(c->plr->action_id_type, ore_id)
							);
					}

					/* Send the mining message.  */
					packet_send_chatbox_message(c, return_msg);

					/* Stop playing the mining emote once we get ore.  */
					player_reset_animation(c);
					/* Gives the player xp proper to the ore.  */
					add_skill_xp(c, MINING_SKILL, 
								 get_mining_xp(c->plr->action_id_type));
				}
				else
				{
					c->plr->action_skill_type = -1;
					set_player_state(c, STATE_IDLE);
					return;
				}
			}

			/* Roll to see if the ore turns to its depleated state, only for ores
			 * other than rune essence.  */
			if (((rand_int(1, 8) == 8 && skills_mining_ore_depletion_method == 1) 
				  || skills_mining_ore_depletion_method == 0)
				  && c->plr->action_id_type != MINING_RUNE_ESSENCE)
			{
				
				ore_drained_obj_id = add_to_object_list(c->plr->action_id, 
								   10, 
								   c->plr->action_x,
								   c->plr->action_y, 
								   c->plr->world_z, 0, 
								   (ore_respawn_ticks + 1),
								   0);
				set_object_transforms(ore_drained_obj_id, 
										   452, ore_respawn_ticks);
				spawn_object_globally(ore_drained_obj_id);
				
				set_player_state(c, STATE_IDLE);
				return;
			}
		}

		c->plr->action_ticks = c->plr->skill_tool_time;
	}

	c->plr->action_total_time_acted++;
}
