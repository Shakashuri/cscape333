#ifndef _SKILL_MINING_H_
#define _SKILL_MINING_H_

int get_mining_level_needed(int mining_id);
float get_mining_xp(int mining_id);
int get_mining_ore_id(int mining_id);
char *get_mining_ore_name(int mining_id, int ore_id);
float get_mining_chance(int mining_id, int mining_level);
int get_mining_respawn_ticks(int mining_id);

void init_mining(struct client *c);
void handle_mining_ore(struct client *c);

#endif
