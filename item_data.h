#ifndef _ITEM_DATA_H_
#define _ITEM_DATA_H_

int get_container_water_id(int container_id);
int check_for_water_container(int item_id);
int get_food_healing_amount(int food_id);
void get_food_consume_message(int food_id, char *food_name, char *eat_msg, 
							  int size);
int get_item_uses(int item_id);
#endif
