/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** 
 * @file pathfinding.c
 * @brief Contains Astar pathfinding functions, data structs.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "fileio.h"
#include "misc.h"
#include "packet.h"
#include "pathfinding.h"
#include "player.h"
#include "regions.h"

/** Maximum size of nodes to try.  */
#define LIST_SIZE 800
/** Max size of path variables.  */
#define PATH_SIZE 200
/** Maximum size of results array.  */
#define RESULTS_SIZE 200 

/* Define everything we're going to use here.  */

/** X coordinate offsets of every square around one.  */
int dir_x[8] = {0, 0, 1,-1, 1,-1, 1,-1};
/** Y coordinate offsets of every square around one.  */
int dir_y[8] = {1,-1, 0, 0,-1,-1, 1, 1};

/** 
 * Node struct for pathfinding.
 */
struct node
{
	/** Position of node, x */
	int x;
	/** Position of node, y */
	int y;
	/** "parent's" position in map, x position.  */
	int p_x;
	/** "parent's" position in map, y position.  */
	int p_y;
	/** g - movement cost from start to this point. */
	int g;
	/** estimated movement cost from here to the end of the path.  */
	int h;
	/** f - (g + h) */
	int f;
};

/**
 * Checks if a node with a certain position exists in the list.
 *
 * @param *node[] Node list to check.
 * @param size Size of the node list.
 * @param c_x Node to check for's x coordinate.
 * @param c_y Node to check for's y coordinate.
 *
 * @return -1 if failed to find node, anything else for its position in the 
 * node list.
 */
static int is_in_list(struct node *node[], int size, int c_x, int c_y)
{
	int i;

	for (i = 0; i < size; i++)
	{
		if (node[i] != NULL)
		{
			if ((node[i]->x == c_x) && (node[i]->y == c_y))
			{
				return i;
			}
		}
	}
	return -1;
}

/**
 * Returns the number of non NULL objects in a list.
 *
 * @param *node[] Node list to check.
 * @param size Size of the node list.
 *
 * @return Returns the number of nodes in the list.
 */
static int num_entries_in_list(struct node *node[], int size)
{
	int x;
	int found = 0;
	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
		{
			found++;
		}
	}
	return found;
}

/** 
 * Returns the list address of the first NULL object in a list.
 *
 * @param *node[] Node list to check.
 * @param size Size of the node list.
 *
 * @return Returns the index of the first free slot.
 */
static int get_free_slot(struct node *node[], int size)
{
	int x;
	for (x = 0; x < size; x++)
	{
		if (node[x] == NULL)
		{
			return x;
		}
	}
	return -1;
}

/** 
 * Returns the list address of the first non NULL object in a list.
 * 
 * @param *node[] Node list to check.
 * @param size Size of the node list.
 *
 * @return Returns the index of the first non NULL object, -1 if it can't find 
 * one.
 */
static int get_first(struct node *node[], int size)
{
	int x;
	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
		{
			return x;
		}
	}
	return -1;
}

/** 
 * Returns the list address of the object with the lowest cost value in a list.
 * 
 * @param *node[] Node list to check.
 * @param size Size of the node list.
 *
 * @return Returns the index of the object in the node with the lowest cost.
 */
static int get_lowest_cost(struct node *node[], int size)
{
	int current_low = get_first(node, size);
	int x;

	/* If current_low is -1, there are no nodes to check.  */
	if (current_low == -1)
	{
		return current_low;
	}

	for (x = 0; x < size; x++)
	{
		if (node[x] != NULL)
		{
			if (node[x]->f < node[current_low]->f)
			{
				current_low = x;
			}
		}
	}
	return current_low;
}

/**
 * Manhattan heuristic used for Astar calculation.
 *
 * @param c_x Start x coord.
 * @param e_x End x coord.
 * @param c_y Start y coord.
 * @param e_y End y coord.
 *
 * @return Returns abs(c_x - e_x) + abs(c_y - e_y), the distance from the 
 * start point to the end.
 */
static int manhattan(int c_x, int e_x, int c_y, int e_y)
{
	return (abs(c_x - e_x) + abs(c_y - e_y));
}

int get_perpendicular_square_coords_offsets_around_entity(int size_x, 
		int size_y, int **offsets_x, int **offsets_y)
{
	if (size_x == 0 || size_y == 0)
	{
		printf("can't use zero size.\n");
		return 0;
	}

	if (*offsets_x != NULL)
	{
		printf("offset_x is not null!\n");
		return 0;
	}
	if (*offsets_y != NULL)
	{
		printf("offset_y is not null!\n");
		return 0;
	}

	int array_size = ((size_x * 2) + (size_y * 2));
	int array_ptr = 0;
	int x = 0, y = 0;

	int sw_corner_x = -1;
	int sw_corner_y = -1;
	int ne_corner_x = size_x;
	int ne_corner_y = size_y;

	*offsets_x = calloc(array_size, sizeof(int));
	*offsets_y = calloc(array_size, sizeof(int));

	if (*offsets_x == NULL)
	{
		printf("offset_x is null after calloc!\n");
		return 0;
	}
	if (*offsets_y == NULL)
	{
		printf("offset_y is null after calloc!\n");
		return 0;
	}

	for (x = sw_corner_x; x <= ne_corner_x; x++)
	{
		for (y = sw_corner_y; y <= ne_corner_y; y++)
		{
			/* If the square is a corner, skip.  */
			if ((x == sw_corner_x && y == sw_corner_y)
				|| (x == sw_corner_x && y == ne_corner_y)
				|| (x == ne_corner_x && y == ne_corner_y)
				|| (x == ne_corner_x && y == sw_corner_y))
			{
				continue;
			}

			/* If the square is part of the interior, skip it.  */
			if ((x > sw_corner_x && x < ne_corner_x)
				&& (y > sw_corner_y && y < ne_corner_y))
			{
				continue;
			}

			if (array_ptr > array_size)
			{
				printf("Should not have happened!\n");
				free(offsets_x);
				free(offsets_y);
				return 0;
			}

			(*offsets_x)[array_ptr] = x;
			(*offsets_y)[array_ptr] = y;

			array_ptr++;
		}
	}

	return array_ptr;
}

/**
 * Find what the closest square around an entity is.
 * @param entity_world_x, entity_world_y X and Y world coordinates of the
 * entity.
 * @param size_x, size_y Size in squares of the entity.
 * @param *result_x, *results_y Pointer to where the best square coordinates
 * should be writen to. Also is what coord should be compared to to find the
 * closest path to.
 * @param height Where the entity is located on the world Z level.
 * @returns -1 on error, number of squares of the path on success.
 */
int get_closest_perpendicular_square_around_entity(int entity_world_x, 
		int entity_world_y, int size_x, int size_y, int *result_x, 
		int *result_y, int height)
{
	int *entity_world_x_offsets = NULL;
	int *entity_world_y_offsets = NULL;
	int *is_valid = NULL;
	int *score = NULL;
	int collision = 0;
	int lowest_index = 0;
	int x = 0;
	int offset_size 
		= get_perpendicular_square_coords_offsets_around_entity(size_x, 
				size_y, &entity_world_x_offsets, &entity_world_y_offsets);
	
	int result_score = 0;

	if (offset_size == 0)
	{
		printf("Offset size is 0\n");
		return -1;
	}

	if (size_x == 0 || size_y == 0)
	{
		printf("Size is zero.\n");
		return -1;
	}

	is_valid = calloc(offset_size, sizeof(int));
	score = calloc(offset_size, sizeof(int));

	for (x = 0; x < offset_size; x++)
	{
		score[x] = 9999;
	}

	printf("PERP FUNC\n");
	for (x = 0; x < offset_size; x++)
	{
		printf("X: %d Y: %d\n", entity_world_x + entity_world_x_offsets[x], 
								entity_world_y + entity_world_y_offsets[x]);

		collision = get_collision(entity_world_x + entity_world_x_offsets[x], 
								  entity_world_y + entity_world_y_offsets[x], 
								  height);

		//if ((collision & OCCUPIED_MASK) || (collision & TAKEN_MASK))
		if ((collision & BLOCKED_MASK))
		{
			printf("\tBLOCKED!\n");
			is_valid[x] = 0;
			score[x] = -1;
			continue;
		}

		/* If this is the same square as the player is on, use it.  */
		if (((entity_world_x + entity_world_x_offsets[x]) == *result_x) 
			&& ((entity_world_y + entity_world_y_offsets[x]) == *result_y))
		{
			printf("\tSAME AS PLAYER SQUARE!\n");
			is_valid[x] = 1;
			score[x] = 0;
			continue;
		}
		
		score[x] 
			= get_num_walking_spaces_to_point(
				*result_x, *result_y,
				entity_world_x + entity_world_x_offsets[x], 
				entity_world_y + entity_world_y_offsets[x], 
				height);

		if (score[x] >= 0)
		{
			printf("\tGOOD! SC %d\n", score[x]);
			is_valid[x] = 1;
		}
		else
		{
			printf("\t\tBAD SC %d\n", score[x]);
			is_valid[x] = 0;
		}
	}

	/* Find lowest index as starting point.  */
	for (x = 0; x < offset_size; x++)
	{
		if (score[x] >= 0 && is_valid[x] == 1)
		{
			lowest_index = x;
			break;
		}
	}

	/* Search for lowest.  */
	for (x = 0; x < offset_size; x++)
	{
		if (is_valid[x])
		{
			if (score[x] < score[lowest_index] && score[x] >= 0)
			{
				lowest_index = x;
				result_score = score[x];
			}
		}
	}

	*result_x = (entity_world_x + entity_world_x_offsets[lowest_index]);
	*result_y = (entity_world_y + entity_world_y_offsets[lowest_index]);

	free(entity_world_x_offsets);
	free(entity_world_y_offsets);
	free(score);
	free(is_valid);

	printf("DONE WITH PERP FUNC\n");
	
	return result_score;
}

void get_best_object_usage_square(int entity_world_x, 
		int entity_world_y, int size_x, int size_y, int *result_x, 
		int *result_y, int height)
{
	int start_x = *result_x;
	int start_y = *result_y;

	int perp_x = start_x;
	int perp_y = start_y;

	int perp_dist = 0;
	int obj_square_dist = 0;

	perp_dist = get_closest_perpendicular_square_around_entity( 
			entity_world_x, 
			entity_world_y, 
			size_x, 
			size_y, 
			&perp_x, 
			&perp_y, 
			height);

	obj_square_dist = get_num_walking_spaces_to_point(start_x, start_y, 
			entity_world_x, entity_world_y, height);

	/*
	printf("Start X: %d Y: %d\n", start_x, start_y);
	printf("Obj X: %d Y: %d\n", entity_world_x, entity_world_y);
	printf("Perp X: %d Y: %d\n", perp_x, perp_y);
	printf("Perp dist: %d Obj dist: %d\n", perp_dist, obj_square_dist);
	*/

	/* Offset perp_dist by 2 to make sure we prefer the object square
	 * when there is only a small difference between the two. This should make
	 * using doors a little more natural.  */
	if (obj_square_dist <= (perp_dist + 2) && obj_square_dist >= 0)
	{
		*result_x = entity_world_x;
		*result_y = entity_world_y;
	}
	else
	{
		*result_x = perp_x;
		*result_y = perp_y;
	}

	return;
}

/**
 * Calculates the best path using the Astar algorithm. Uses manhattan 
 * calculation for square distance.
 *
 * @param start_x Starting x coord.
 * @param start_y Starting y coord.
 * @param end_x Ending x coord.
 * @param end_y Ending y coord.
 * @param height What height level are the coordinates on.
 * @param results_x[] X coord results of astar.
 * @param results_y[] Y coord results of astar.
 * @param results_size Size of the results arrays.
 *
 * @return Returns a negative on failure, number of squares in the path
 * to the position on success.
 */
static int astar(int start_x, int start_y, int end_x, int end_y, int height, 
				 int results_x[], int results_y[], int results_size)
{
	int x, z;
	int lowest_open_node = 0;

	int num_open_nodes = 0;

	int path_x[PATH_SIZE] = {-1};
	int path_y[PATH_SIZE] = {-1};
	int path_last = 0;

	/* Define the lists to hold our data.  */
	struct node *open_list[LIST_SIZE];
	struct node *closed_list[LIST_SIZE];
	/* Create the starting node.  */
	struct node *start = malloc(sizeof(struct node));

	if (start_x == end_x && start_y == end_y)
	{
		return 0;
	}
	
	/* Zero out the lists before we use them.  */
	for (x = 0; x < LIST_SIZE; x++)
	{
		open_list[x] = closed_list[x] = NULL;
	}

	/* Setup the starting node.  */
	start->x = start_x;
	start->y = start_y;
	start->p_x = start->p_y = 0;
	start->g = 0;
	start->h = manhattan(start->x, end_x, start->y, end_y);
	start->f = (start->h + start->g);

	/* Assign start to open_list.  */
	open_list[0] = start;
	
	/* While there are still entries in the open list, continue.  */
	while ((num_open_nodes = num_entries_in_list(open_list, LIST_SIZE)) != 0)
	{
		/* Get the lowest cost node in the open_list, transfer it to closed,
		   then create its neighbors.  */
		lowest_open_node = get_lowest_cost(open_list, LIST_SIZE);

		z = get_free_slot(closed_list, LIST_SIZE);

		if (z == -1)
		{
			/* packet_send_chatbox_message(c, "I can't go there."); */
			return -2;
		}

		closed_list[z] = open_list[lowest_open_node];
		open_list[lowest_open_node] = NULL;

		/* If the open node is the endpoint, end the loop and print results.  */
		if (closed_list[z]->x == end_x 
			&& closed_list[z]->y == end_y)
		{
			int hold, track;
			/* Free everything in the open_list. We have the path and this
			   isn't needed anymore.  */
			for (x = 0; x < LIST_SIZE; x++)
			{
				if (open_list[x] != NULL)
				{
					free(open_list[x]);
					open_list[x] = NULL;
				}
			}
			
			path_x[0] = end_x;
			path_y[0] = end_y;
			path_x[1] = closed_list[z]->p_x;
			path_y[1] = closed_list[z]->p_y;

			track = 2;
			x = is_in_list(closed_list, LIST_SIZE, closed_list[z]->p_x, 
							closed_list[z]->p_y);
			free(closed_list[z]);
			closed_list[z] = NULL;
			/* Hold stores position data so that when things are not needed,
			   they can be freed.  */
			hold = x;
		
			while (x != -1)
			{
				x = is_in_list(closed_list, LIST_SIZE, closed_list[hold]->p_x, 
								closed_list[hold]->p_y);
				free(closed_list[hold]);
				closed_list[hold] = NULL;
				
				if (x != -1)
				{
					path_x[track] = closed_list[x]->x;
					path_y[track] = closed_list[x]->y;
					track++;
					hold = x;
				}
				else
				{
					/* Path is printed out, free everything in the 
					   closed list.  */
					for (z = 0; z < LIST_SIZE; z++)
					{
						if (closed_list[z] != NULL)
						{
							free(closed_list[z]);
							closed_list[z] = NULL;
						}
					}
					path_last = track;
				}
			}	
			break;
		}
		
		/* Go through each adjacent tile to the one picked.  */
		for (x = 0; x < 8; x++)
		{
			int return_v;
			/* Create struct for the neighbor node.  */
			struct node *next = malloc(sizeof(struct node));
			/* Connect the node to its parent.  */
			next->p_x = closed_list[z]->x;
			next->p_y = closed_list[z]->y;
			next->x = (next->p_x + dir_x[x]);
			next->y = (next->p_y + dir_y[x]);
			
			/* The node's cost is one plus its parent's cost, unless it is
			   a diagonal movement, then add two. This helps ensure the
			   smoothest movement.  */
			if (x < 4)
			{
				next->g = (closed_list[z]->g + 1);
			}
			else
			{
				next->g = (closed_list[z]->g + 2);
			}
			next->h = manhattan(next->x, end_x, next->y, end_y);
			next->f = (next->g + next->h);
			
			/* If node is in a out of bounds location, or blocked, free it and
			   continue to the next neighbor.  */
			if (can_walk_in_direction(next->p_x, next->p_y, height, next->x, next->y, 0) != 1)
			{
				free(next);
				continue;
			}
			
			/* If next already exists in the open list...  */
			return_v = is_in_list(open_list, LIST_SIZE, next->x, next->y);
			if (return_v != -1)
			{
				/* If the cost of this one is lower, overwite the old one.
				   Otherwise, free it and continue.  */
				if (open_list[return_v]->f > next->f)
				{
					free(open_list[return_v]);
					open_list[return_v] = next;
					continue;
				}
				else
				{
					free(next);
					continue;
				}
			}
			/* If next is in the closed list, free it and continue.  */
			return_v = is_in_list(closed_list, LIST_SIZE, next->x, next->y);
			if (return_v != -1)
			{
				free(next);
				continue;
			}
			/* If we got here, that means that this node is a newly discovered
			   one, so add it to the open_list.  */
			open_list[get_free_slot(open_list, LIST_SIZE)] = next;
		}
	}
	
	if (num_open_nodes != 0)
	{
		x = (path_last - 1);

		if (x >= results_size)
		{
			return (path_last - 1);
		}

		for (; x >= 0; x--)
		{
			results_x[x] = path_x[x];
			results_y[x] = path_y[x];
		}

		return (path_last - 1);
	}
	else
	{
		return -1;
	}
}

int is_valid_path_to_point(int start_x, int start_y, int end_x, int end_y, int height)
{
	int rv = astar(start_x, start_y, end_x, end_y, height, NULL, NULL, 0);

	printf("is_valid_path_to_point X: %d Y: %d eX: %d eY: %d RV: %d\n", 
			start_x, start_y, end_x, end_y, rv);

	if (rv < 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int get_num_walking_spaces_to_point(int start_x, int start_y, int end_x, 
		int end_y, int height)
{
	int rv = astar(start_x, start_y, end_x, end_y, height, NULL, NULL, 0);

	printf("get_num_walking_spaces_to_point "
			"X: %d Y: %d eX: %d eY: %d RV: %d\n", start_x, start_y, end_x, 
			end_y, rv);

	if (rv < 0)
	{
		return -1;
	}
	else
	{
		return rv;
	}
}

/**
 * Calculates the best path using Astar, moves player along the path.
 *
 * @param *c Client instance.
 * @param start_x Starting x coord.
 * @param start_y Starting y coord.
 * @param end_x Ending x coord.
 * @param end_y Ending y coord.
 * @param height What height level are the coordinates on.
 *
 * @return Returns a 1 on failure, zero on success.
 */
int player_move_to_location(struct client *c, int start_x, int start_y, 
		int end_x, int end_y, int height)
{
	/* If player is frozen, prevent movement.  */
	if (is_player_dead_or_frozen(c, 0))
	{
		return 1;
	}

	/* If dest and start are the same.  */
	if (start_x == end_x && start_y == end_y)
	{
		return 0;
	}

	int results_x[RESULTS_SIZE] = {-1};
	int results_y[RESULTS_SIZE] = {-1};
	int x;
	int rv = 0;

	player_clear_walking_queue(c);
	rv = astar(start_x, start_y, end_x, end_y, height, results_x, results_y, 
			RESULTS_SIZE);

	if (rv == -2)
	{
		packet_send_chatbox_message(c, "I can't go there."); 
		return 1;
	}
	else if (rv == -1)
	{
		return 1;
	}
	else
	{
		for (x = rv; x >= 0; x--)
		{
			player_move_one_square(c, results_x[x], results_y[x]);
		}

		return 0;
	}
}

/**
 * Uses Astar to see what how many spaces the player has to move to be within
 * attack range of an opponent, and moves them there.
 *
 * @param *c Client instance.
 * @param start_x Starting x coord.
 * @param start_y Starting y coord.
 * @param end_x Ending x coord.
 * @param end_y Ending y coord.
 * @param height What height level are the coordinates on.
 * @param npc_size Size of the NPC the player is attacking.
 * @param attack_range Range the player needs to be within in order to attack.
 *
 * @return Returns a 1 on failure, zero on success.
 */
int player_move_is_within_combat_range(struct client *c, int start_x, int start_y,
									int end_x, int end_y, int height, 
									int npc_size, int attack_range)
{
	/* If player is frozen, prevent movement.  */
	if (is_player_dead_or_frozen(c, 0))
	{
		return 1;
	}

	int target_x = start_x, target_y = start_y, perpen = 0;

	/* Check if using melee, then move to a perpendicular square.  */
	if (attack_range < 2)
	{
		perpen = 1;
		get_closest_perpendicular_square_around_entity(end_x, end_y, 
										   npc_size,
										   npc_size,
										   &target_x, &target_y,
										   c->plr->world_z);
		/* Change the ending point to the perpendicular square we found.  */
		end_x = target_x;
		end_y = target_y;
	}

	/* Do a sanity check, don't do anything if the player is within range and
	 * their attack isn't blocked.  */
	int within_range = is_within_combat_range(end_x, end_y, npc_size, start_x, start_y, attack_range);
	int can_range = can_range_in_direction(start_x, start_y, end_x, end_y, height);

	printf("within %d can %d\n", within_range, can_range);

	if (within_range == 1 && can_range == 1)
	{
		/* If melee, make sure the player is perpendicular to the target.  */
		if (perpen == 1)
		{
			if (c->plr->world_x == target_x && c->plr->world_y == target_y)
			{
				printf("within range\n");
				return 0;
			}
		}
		else
		{
			printf("within range\n");
			return 0;
		}
	}


	int results_x[RESULTS_SIZE] = {-1};
	int results_y[RESULTS_SIZE] = {-1};
	int x, y;
	int rv = 0;

	int is_within_range = -1;
	int is_ranged_clear = 0;

	player_clear_walking_queue(c);
	rv = astar(start_x, start_y, end_x, end_y, height, results_x, 
			results_y, RESULTS_SIZE);

	if (rv == -2)
	{
		if (attack_range > 1)
		{
			packet_send_chatbox_message(c, "I can't hit that from here.");
		}
		else
		{
			packet_send_chatbox_message(c, "I can't go there."); 
		}
		c->plr->targeted_npc = -1;
		return 1;
	}
	else if (rv == -1)
	{
		return 1;
	}
	else
	{
		printf("**********************\n");
		/* Loop through the path until we find a square that is within range 
		 * so the player can attack.  */
		for (x = rv; x >= 0; x--)
		{
			is_within_range = is_within_combat_range(end_x, end_y, npc_size, 
											   results_x[x], results_y[x], 
											   attack_range);

			is_ranged_clear = can_range_in_direction(results_x[x], results_y[x], end_x, end_y, height);

			if (is_within_range == 1)
			{
				if (is_ranged_clear == 0)
				{
					printf("Within range, but has a blockage. Continue.\n");
				}
				else if (is_ranged_clear == 1)
				{
					printf("Within range, and has a clear shot. Move here.\n");

					/* Move the player to that square.  */
					if (perpen == 1)
					{
						for (y = rv; y >= 0; y--)
						{
							player_move_one_square(c, results_x[y], results_y[y]);
						}
					}
					else
					{
						for (y = rv; y >= x; y--)
						{
							player_move_one_square(c, results_x[y], results_y[y]);
						}
					}
					return 0;
				}
			}
			else
			{
				printf("Not within range, keep moving.\n");
			}
		}

		/* If we got here, there was no square that was within range, and had
		 * a clear shot.  */
		packet_send_chatbox_message(c, "I can't go there."); 
		return 1;
	}
}

