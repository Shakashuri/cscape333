/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file shops.c
 * @brief Functions relating to handling shops, buying and selling items.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "constants.h"

#include "item.h"
#include "client.h"
#include "shops.h"
#include "misc.h"
#include "packet.h"
#include "player.h"

/**
 * Returns the index of a specific shop in the SHOP_DATA array.
 * @param shop_id ID of the shop to look for.
 * @return Returns the index of the shop in the SHOP_DATA array if found,
 * -1 if it wasn't found.
 */
int get_shop_index(int shop_id)
{
	int x;

	for (x = 0; x < NUM_SHOPS; x++)
	{
		if (shop_id == SHOP_DATA[x]->id)
		{
			/* Found matching shop, skip.  */
			return x;
		}
	}

	return -1;
}

/**
 * Sees how much a shop will sell an item for, based off of their current
 * stock.
 * @param shop_idx Index of the store.
 * @param item_id Item in the store to evaluate.
 * @param stock_override Override the stock number with this, -1 if no.
 * @return Returns -1 on fail, anything else is the price.
 */
int get_shop_selling_price(int shop_idx, int item_id, int stock_override)
{
	int item_slot = -1, num_in_stock = 0;
	int max_price = 0, min_price = 0;
	float change_price = 0.0;
	int x;

	if (shop_idx < 0 || shop_idx >= NUM_SHOPS)
	{
		return -1;
	}

	for (x = 0; x < SHOP_SIZE; x++)
	{
		if (item_id == SHOP_DATA[shop_idx]->items[x])
		{
			item_slot = x;
		}
	}

	if (item_slot != -1)
	{
		if (stock_override == -1)
		{
			num_in_stock = SHOP_DATA[shop_idx]->items_n[item_slot];
		}
		else
		{
			num_in_stock = stock_override;
		}

		max_price = ITEMS[item_id]->spec_price;
		min_price = ITEMS[item_id]->spec_price * .25;

		/* Ensure the store doesn't give it away for free.  */
		if (min_price <= 0)
		{
			min_price = 1;
		}

		change_price = (((float) max_price - (float) min_price) / 10.0);

		printf("MAX: %d MIN: %d CHANGE: %.2f\n", max_price, min_price, 
			   change_price);

		if (num_in_stock <= 0)
		{
			return max_price;
		}
		else if (num_in_stock == 1)
		{
			return max_price;
		}
		else if (num_in_stock <= 10)
		{
			return (int) ((float) max_price - (change_price * (float) num_in_stock));
		}
		else
		{
			return min_price;
		}
	}

	return -1;
}

/**
 * Sees how much a shop will pay for an item, based off of their current
 * stock.
 * @param shop_idx Index of the store.
 * @param item_id Item in the inventory store to evaluate.
 * @param stock_override Override the stock number with this, -1 if no.
 * @return Returns -1 on fail, anything else is the price.
 */
int get_shop_buying_price(int shop_idx, int item_id, int stock_override)
{
	int item_slot = -1, num_in_stock = 0;
	int max_price = 0, min_price = 0, change_price = 0;
	int x;

	if (shop_idx < 0 || shop_idx >= NUM_SHOPS)
	{
		return -1;
	}

	for (x = 0; x < SHOP_SIZE; x++)
	{
		if (item_id == SHOP_DATA[shop_idx]->items[x])
		{
			item_slot = x;
		}
	}

	max_price = ITEMS[item_id]->low_alch;
	min_price = ITEMS[item_id]->low_alch * .25;

	/* Ensure we don't sell for free.  */
	if (min_price <= 0)
	{
		min_price = 1;
	}

	change_price = ((max_price - min_price) / 10);

	if (stock_override == -1)
	{
		if (item_slot == -1)
		{
			num_in_stock = 0;
		}
		else
		{
			num_in_stock = SHOP_DATA[shop_idx]->items_n[item_slot];
		}
	}
	else
	{
		num_in_stock = stock_override;
	}

	if (num_in_stock <= 0)
	{
		return max_price;
	}
	else if (num_in_stock <= 10)
	{
		return (max_price - (change_price * num_in_stock));
	}
	else
	{
		return min_price;
	}

	return -1;
}

/**
 * Handles buying an item from a shop.
 * @param *c Client instance.
 * @param shop_idx Index of the shop to sell to.
 * @param item_id Item ID of the item being purchased.
 * @param amount Number of the item being purchased.
 * @return 1 on success, anything else on failure.
 */
int buy_item_from_shop(struct client *c, int shop_idx, int item_id, int amount)
{
	if (shop_idx < 0 || shop_idx >= NUM_SHOPS)
	{
		return -1;
	}

	if (is_item_id_invalid(item_id))
	{
		return -1;
	}

	if (amount < 0 || amount > MAX_ITEM_STACK)
	{
		return -1;
	}

	int x;
	int rv;
	int item_slot = -1;
	int total_cost = 0;
	int item_cost = 0;
	int num_in_stock = -1;
	int gold_slot = -1;
	int free_inv_slots = get_free_inv_slots(c);
	int is_item_stacking = ITEMS[item_id]->stacking;
	char return_msg[200];

	for (x = 0; x < SHOP_SIZE; x++)
	{
		if (item_id == SHOP_DATA[shop_idx]->items[x])
		{
			item_slot = x;
		}
	}

	/* If player doesn't have any free space, allow it if the item is stacking
	 * and they already have on in their inventory.  */
	if (free_inv_slots >= 0 
		|| (player_has_item(c, item_id, 1) && is_item_stacking))
	{
		printf("inv space good.\n");
	}

	num_in_stock = SHOP_DATA[shop_idx]->items_n[item_slot];

	if (num_in_stock == -1)
	{
		num_in_stock = MAX_ITEM_STACK;
	}

	if (num_in_stock == 0)
	{
		packet_send_chatbox_message(c, "They don't have any of that in stock right now.");
		return -1;
	}

	if (num_in_stock < amount)
	{
		amount = num_in_stock;
	}

	/* Only buy as much as we have free inventory slots for.  */
	if (is_item_stacking != 1 && free_inv_slots < amount)
	{
		if (free_inv_slots <= 0)
		{
			packet_send_chatbox_message(c, "You don't have any space in your inventory to "
						 "buy that.");
			return -1;
		}
		amount = free_inv_slots;
	}

	if (amount > 1)
	{
		for (x = num_in_stock; x > (num_in_stock - amount); x--)
		{
			item_cost = get_shop_selling_price(shop_idx, item_id, x);
			printf("Amount: %d item cost: %d\n", x, item_cost);
			total_cost += item_cost;
		}
	}
	else
	{
		total_cost = get_shop_selling_price(shop_idx, item_id, -1);
	}

	printf("Total cost of %d items: %d\n", amount, total_cost);

	if ((gold_slot = player_has_item(c, 995, total_cost)) != -1)
	{
		printf("Has money.\n");
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have enough gold to buy that!");
		return -1;
	}

	/* If item is stacking, then there we don't need to handle anything
	 * specially. Let the add_item_to_inv function handle it.  */
	if (is_item_stacking == 1)
	{
		rv = add_item_to_inv(c, item_id, amount, get_item_default_charges(item_id));
	}
	else
	{
		/* All all non-stacking items.  */
		for (x = 0; x < amount; x++)
		{
			rv = add_item_to_inv(c, item_id, amount, 
								 get_item_default_charges(item_id));
			
			if (rv != 1)
			{
				printf("FAILURE FAILURE BUY FROM STORE!\n");
				return -1;
			}
		}
	}

	/* Handle removing items from the shop.  */
	if (rv == 1)
	{
		if (SHOP_DATA[shop_idx]->items_n[item_slot] != -1)
		{
			SHOP_DATA[shop_idx]->items_n[item_slot] -= amount;

			if (SHOP_DATA[shop_idx]->items_n[item_slot] == 0)
			{
				/* Only fully remove the item from the shop if it isn't one that
				 * will get restocked.  */
				if (SHOP_DATA[shop_idx]->orig_items[item_slot] == -1)
				{
					SHOP_DATA[shop_idx]->items[item_slot] = -1;
				}
			}
		}
	}

	/* Take money for the payment.  */
	c->plr->items_n[gold_slot] -= total_cost;

	snprintf(return_msg, 200, "You bought %d %s for %d coin(s).", amount, 
			 ITEMS[item_id]->name, total_cost);
	packet_send_chatbox_message(c, return_msg);

	/* Refresh all needed interfaces.  */
	packet_reload_items_in_shop_interface_globally(c, shop_idx);
	packet_reload_items_in_inventory_interface(c);
	packet_reload_items_in_shop_sidebar_interface(c);
	
	return 1;
}

/**
 * Handles selling an item from a player's inventory to the selected shop.
 * @param *c Client instance.
 * @param shop_idx Index of the shop to sell to.
 * @param item_id Item ID of the item being sold.
 * @return 1 on success, anything else on failure.
 */
int sell_item_to_shop(struct client *c, int shop_idx, int item_id)
{
	if (shop_idx < 0 || shop_idx >= NUM_SHOPS)
	{
		return -1;
	}

	if (is_item_id_invalid(item_id))
	{
		return -1;
	}
	/* Make sure this item can even be sold.  */
	/* TODO: Add other currencies to this list later.  */
	if (ITEMS[item_id]->tradable == 0 || item_id == 995)
	{
		packet_send_chatbox_message(c, "You can't sell that.");
		return -1;
	}


	int x;
	int item_inv_slot = -1;
	int item_store_slot = -1;
	int item_cost = 0;
	int gold_slot = player_has_item(c, 995, 1);
	int free_inv_slots = get_free_inv_slots(c);
	int free_store_slots = 0;
	char return_msg[200];

	item_inv_slot = player_has_item(c, item_id, 1);

	if (item_inv_slot == -1)
	{
		packet_send_chatbox_message(c, "You can't sell what you don't have!");
		printf("Cheating from player %s\n", c->plr->name);
		return -1;
	}

	/* See if the store has the item being sold in stock.  */
	for (x = 0; x < SHOP_SIZE; x++)
	{
		if (item_id == SHOP_DATA[shop_idx]->items[x])
		{
			item_store_slot = x;
		}
	}

	if (item_store_slot == -1 && SHOP_DATA[shop_idx]->is_specialty == 1)
	{
		packet_send_chatbox_message(c, "This store won't buy that.");
		return -1;
	}

	/* Check if the player has gold already, or has a free inventory slot.  */
	if (free_inv_slots <= 0 && gold_slot == -1)
	{
		packet_send_chatbox_message(c, "You don't have enough space to do that.");
		return -1;
	}

	/* If the item is not in the store, find the first empty slot to put the
	 * item in.  */
	if (item_store_slot == -1 && SHOP_DATA[shop_idx]->is_specialty == 0)
	{
		printf("Item not in shop, find first empty slot for it.\n");
		for (x = 0; x < SHOP_SIZE; x++)
		{
			if (SHOP_DATA[shop_idx]->items[x] == -1 
				&& SHOP_DATA[shop_idx]->orig_items[x] == -1)
			{
				free_store_slots++;

				if (item_store_slot == -1)
				{
					item_store_slot = x;
				}
			}
		}
	}

	item_cost = get_shop_buying_price(shop_idx, item_id, -1);

	/* Add item to the store.  */
	SHOP_DATA[shop_idx]->items[item_store_slot] = item_id;
	/* Don't add if the shop has infinte stock.  */
	if (SHOP_DATA[shop_idx]->items_n[item_store_slot] != -1)
	{
		SHOP_DATA[shop_idx]->items_n[item_store_slot] += 1;
	}

	/* Take money for the payment.  */
	if (gold_slot != -1)
	{
		c->plr->items_n[gold_slot] += item_cost;
	}
	else
	{
		add_item_to_inv(c, 995, item_cost, get_item_default_charges(995));
	}

	delete_item_from_inv(c, item_id, 1);

	snprintf(return_msg, 200, "You sold 1 %s for %d coin(s).",
			 ITEMS[item_id]->name, item_cost);
	packet_send_chatbox_message(c, return_msg);

	/* Refresh all needed interfaces.  */
	packet_reload_items_in_shop_interface_globally(c, shop_idx);
	packet_reload_items_in_inventory_interface(c);
	packet_reload_items_in_shop_sidebar_interface(c);
	return 1;
}

