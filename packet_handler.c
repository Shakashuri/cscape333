/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file packet_handler.c
 * @brief Contains functions for data packet handling, command handling.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"

#include "action_queue.h"
#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "debug.h"
#include "dialogue_handler.h"
#include "fileio.h"
#include "interface.h"
#include "interface_handler.h"
#include "item.h"
#include "item_handler.h"
#include "misc.h"
#include "npc.h"
#include "server.h"
#include "stream.h"
#include "skills.h"
#include "skill_magic.h"
#include "object.h"
#include "object_handler.h"
#include "packet.h"
#include "packet_handler.h"
#include "pathfinding.h"
#include "player.h"
#include "quests.h"
#include "regions.h"

/** Handles all incoming data packets from a client.
 * @param *c Client instance.
 */
void handle_packets(struct client *c)
{
	int opcode = 0;

	/* Check if there are any packets to read, if so continue.  */
	if (c->bytes_read == 0)
	{
		return;
	}
	else
	{
		c->instream_offset = 0;
		opcode = read_opcode(c);
	}

	while (c->instream_offset <= c->bytes_read)
	{
		/* read_opcode refused to read more than it should.  */
		if (opcode == -1)
		{
			break;
		}
		else if (opcode == CLIENT_PKT_IDLE)
		{
			/* Idle packet. Ignored.  */
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_FINISHED_REGION)
		{
			/* Client finished loading a region, ignored.  */
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_FINISHED_REGION_JUNK)
		{
			/* Client sent that they have loaded a new region.  */
			/* Unsure what this is for, just junk bytes?  */
			read_dword(c);
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_IDLE_LOGOUT)
		{
			/* Client has sent that the player has been idle for a while
			   and should be logged out. TODO Implement later.  */
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CHOSE_AMOUNT)
		{
			/* Client has sent an amount from the "choose an amount" menu.  */

			if (c->plr->interface_open == INTR_BANK
				|| c->plr->interface_open == INTR_BANK_DEPOSIT_BOX)
			{
				c->plr->action_skill_remaining_actions = read_dword(c);
				printf("Num received for bank: %d\n", 
					   c->plr->action_skill_remaining_actions);
				interface_item_handler(c, c->plr->action_id_type, 
									   c->plr->action_item_slot, 
									   c->plr->action_item_id, 
									   CLIENT_PKT_SELECT_X_ITEMS);
			}
			else
			{
				c->plr->action_skill_remaining_actions = read_dword(c);
				printf("Num received: %d\n", 
					c->plr->action_skill_remaining_actions);
				handle_dialogue(c);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MOUSE_CLICKED)
		{
			/* Mouse clicked somewhere */
			read_dword(c);
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_WALKING_ANTICHEAT)
		{
			/* Some sort of anit-cheat, discard.  */
			read_dword(c);
			opcode = read_opcode(c);
		}	
		else if (opcode == CLIENT_PKT_WINDOW_FOCUS)
		{
			/* Focus of client changed */
			read_unsigned_byte(c);
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CAMERA_MOVED)
		{
			/* Camera movement */
			read_unsigned_word(c);
			read_unsigned_word_a(c);
			opcode = read_opcode(c);
		}
		else if (opcode == 129)
		{
			/* Junk packet.  */
			read_unsigned_word(c);
			opcode = read_opcode(c);
		}
		else if (opcode == 191)
		{
			/* Junk packet.  */
			int length = 0;
			/* read junk data.  */
			length = read_unsigned_byte(c);

			while (length != 0)
			{
				read_unsigned_byte(c);
				length--;
			}

			opcode = read_opcode(c);
		}
		else if (opcode == 248)
		{
			/* Junk packet.  */
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_WALKING_MINIMAP 
				 || opcode == CLIENT_PKT_WALKING_NORMAL 
				 || opcode == CLIENT_PKT_WALKING_COMMAND)
		{
			/* Walking packets are handled here.  */
			int x;

			packet_player_walking(c, opcode);
			
			/* If the packet is for walking using the map, remove the 14
			   junk bytes at the end of the packet.  */
			if (opcode == CLIENT_PKT_WALKING_MINIMAP)
			{
				for (x = 0; x < 14; x++)
				{
					read_unsigned_byte(c);
				}
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CHAT_MESSAGE)
		{
			/* Player has said something in chat.  */
			c->plr->chat_msg_size = read_unsigned_byte(c);
			
			c->plr->chat_msg_size -= 2;
			c->plr->chat_text_effect = read_unsigned_byte_a(c);

			read_bytes_a(c, c->plr->chat_text, c->plr->chat_msg_size, 0);
			c->plr->chat_text_color = read_unsigned_byte_s(c);

			c->plr->update_masks[CHAT_TEXT] = 1;
			c->plr->update_required = 1;

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_PLAYER_APPEARANCE)
		{
			/* Closed Player Appearance Window */
			packet_player_change_appearance(c);
			packet_close_all_open_interfaces(c);
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ENTERED_COMMAND)
		{
			/* Player entered a command */	
			char command[100] = {0};
			read_string(c, command, 100);
			/* Turn the command fully lowercase before parsing.  */
			handle_commands(c, string_to_lower(command));
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CLICKED_INTERFACE)
		{
			/* Clicked an Interface, word is interface ID.  */
			int interface_id;
			interface_id = read_unsigned_word(c);
			printf("	CLICKED ID: %d\n", interface_id);

			if (is_player_dead(c))
			{
				printf("dead!\n");
			}
			else
			{
				handle_interface_clicked(c, interface_id);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CLICKED_INTERFACE_CLOSE)
		{
			/* Clicked the close button on an interface.  */
			packet_close_all_open_interfaces(c);
			/* Handle showing level up dialogues when interfaces are closed,
			 * like for quest rewards.  */
			if (get_next_level_up_to_handle(c) != -1)
			{
				c->plr->chat_type = LEVEL_UP_CHAT;
				c->plr->next_chat_id = 0;
				handle_dialogue(c);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_EQUIP_ITEM)
		{
			/* Equip item.  */
			/* Interface id, unused so far.  */
			int frame_id = read_unsigned_word(c);
			int wear_id = read_unsigned_word_a(c);
			int wear_slot = read_unsigned_word_bigendian_a(c);

			if (is_slot_num_invalid(wear_slot, frame_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(wear_id))
			{
				printf("Item ID invalid!");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				equip_item(c, wear_id, wear_slot);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_ON_ANOTHER)
		{
			/* Used item on another.  */
			int item_1_slot = read_unsigned_word(c);
			int item_1_id = read_unsigned_word_bigendian_a(c);
			int action_info_1 = read_signed_word_a(c);
			int last_item_interface = read_unsigned_word_bigendian_a(c);
			int item_2_id = read_signed_word_a(c);
			int item_2_slot = read_unsigned_word(c);

			printf("Item %d:%d used on %d:%d\n", item_1_slot, item_1_id, 
					item_2_slot, item_2_id);
			/* FIXME: Test later to see what these two data points are.
			   I am pretty sure they are interface ids, but which one is for
			   which? Do they need to be switched?  */
			printf("Last item interface: %d\n", last_item_interface);
			printf("Action info 1: %d\n", action_info_1);

			if (is_slot_num_invalid(item_1_slot, action_info_1))
			{
				printf("Slot invalid!\n");
			}
			else if (is_slot_num_invalid(item_2_slot, last_item_interface))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(item_1_id))
			{
				printf("Item 1 ID invalid!\n");
			}
			else if (is_item_id_invalid(item_2_id))
			{
				printf("Item 2 ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead or frozen!\n");
			}
			else
			{
				handle_use_item_on_item(c, item_1_id, item_1_slot, item_2_id, 
							 item_2_slot);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_REMOVE_ITEM_FROM_INTERFACE)
		{
			/* Remove item from an interface (shop, bank, equipment, etc)  */
			int item_id = read_unsigned_word_bigendian_a(c);
			int interface_id = read_unsigned_word_bigendian(c);
			int slot_id = read_unsigned_word_bigendian(c);
		
			printf("RM ITEM FROM INTR: Intr id: %d Slot: %d Item: %d\n", 
				   interface_id, slot_id, item_id);

			if (is_slot_num_invalid(slot_id, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				interface_item_handler(c, interface_id, slot_id, item_id, 
									   opcode);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_USE_GROUND_ITEM)
		{
			/* Used the click option on an item on the ground.  */
			
			c->plr->action_item_id = read_unsigned_word(c);
			c->plr->action_y = read_unsigned_word(c);
			c->plr->action_x = read_unsigned_word(c);
			c->plr->action_z = c->plr->world_z;
	
			if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
								  VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else if (is_item_id_invalid(c->plr->action_item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_GROUND_ITEM_ACTIVATE);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_SELECT_FIVE_ITEMS)
		{
			/* Select 5 items.  */
			int item_id = read_unsigned_word_bigendian_a(c);
			int slot_id = read_unsigned_word_a(c);
			int interface_id = read_unsigned_word_bigendian_a(c);
			
			printf("Select 5: Intr id: %d Slot: %d Item: %d\n", interface_id, 
													  slot_id, item_id);
			
			if (is_slot_num_invalid(slot_id, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				interface_item_handler(c, interface_id, slot_id, item_id,
									   opcode);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_SELECT_TEN_ITEMS)
		{
			/* Select 10 items.  */
			int item_id = read_unsigned_word_a(c);
			int interface_id = read_unsigned_word_bigendian_a(c);
			int slot_id = read_unsigned_word_bigendian(c);
			
			printf("Select 10: Intr id: %d Slot: %d Item: %d\n", interface_id, 
													  slot_id, item_id);
			
			if (is_slot_num_invalid(slot_id, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				interface_item_handler(c, interface_id, slot_id, item_id, opcode);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_SELECT_ALL_ITEMS)
		{
			/* Select all items.  */
			int item_id = read_unsigned_word_bigendian_a(c);
			int interface_id = read_unsigned_word_a(c);
			int slot_id = read_unsigned_word_bigendian(c);
			
			printf("Select All: Intr id: %d Slot: %d Item: %d\n", interface_id, 
													  slot_id, item_id);
			
			if (is_slot_num_invalid(slot_id, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				if (c->plr->interface_open == INTR_BANK_DEPOSIT_BOX)
				{
					printf(" Bank deposit box, send choice.\n");
					c->plr->action_item_slot = slot_id;
					c->plr->action_id_type = interface_id;
					c->plr->action_item_id = item_id;
					packet_send_choose_amount_chatbox_interface(c, 0);
				}
				else
				{
					interface_item_handler(c, interface_id, slot_id, item_id, 
										   opcode);
				}
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_SELECT_X_ITEMS)
		{
			/* Select X items.  */
			int interface_id = 0, slot_id = 0, item_id = 0;

			c->plr->action_item_slot = slot_id 
				= read_unsigned_word(c);
			c->plr->action_id_type = interface_id 
				= read_unsigned_word_bigendian_a(c);
			c->plr->action_item_id = item_id 
				= read_unsigned_word_bigendian(c);
			
			if (is_slot_num_invalid(slot_id, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else if (is_item_id_invalid(c->plr->action_item_id))
			{
				printf("Item ID invalid!\n");
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else
			{
				if (c->plr->interface_open == INTR_BANK_DEPOSIT_BOX)
				{
					printf("Deposit all, deposit box uses this packet for\n");
					printf("deposit all.\n");
					interface_item_handler(c, interface_id, slot_id, item_id,
										   129);
				}
				else
				{
					printf("Select X: Intr id: %d Slot: %d Item: %d\n", 
						interface_id, slot_id, item_id);
					packet_send_choose_amount_chatbox_interface(c, 0);
				}
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_OBJECT_FIRST_ACTION)
		{
			/* When player clicks the first action of an object. 
			   Ex: cut tree.  */
			c->plr->action_y = read_unsigned_word(c);
			c->plr->action_id = read_unsigned_word(c);
			c->plr->action_x = read_unsigned_word_a(c);
			c->plr->action_z = c->plr->world_z;

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);
				
				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_OBJECT_ACTIVATE);

				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 1;

				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;

				printf("Object clicked, 1st action.\n");
				printf("	X: %d Y: %d ID: %d\n", c->plr->action_x, 
												   c->plr->action_y, 
												   c->plr->action_id);
				printf("	RX: %d RY: %d\n", c->plr->action_range_x, 
											  c->plr->action_range_y);
			}
			
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_OBJECT_SECOND_ACTION)
		{
			/* When player clicks the second action of an object.  */
			c->plr->action_x = read_unsigned_word_bigendian_a(c);
			c->plr->action_id = read_unsigned_word_bigendian_a(c);
			c->plr->action_y = read_unsigned_word_a(c);
			c->plr->action_z = c->plr->world_z;

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_OBJECT_ACTIVATE);

				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 2;

				c->plr->action_mask = WILL_OPERATE;

				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;

				printf("Object clicked, 2nd action.\n");
				printf("	X: %d Y: %d ID: %d\n", c->plr->action_x, 
												   c->plr->action_y, 
												   c->plr->action_id);
				printf("	RX: %d RY: %d\n", c->plr->action_range_x, 
											  c->plr->action_range_y);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_OBJECT_THIRD_ACTION)
		{
			/* When player clicks the third action of an object.  */
			c->plr->action_y = read_unsigned_word_a(c);
			c->plr->action_id = read_unsigned_word_bigendian_a(c);
			c->plr->action_x = read_unsigned_word_a(c);

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_OBJECT_ACTIVATE);

				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 3;

				c->plr->action_mask = WILL_OPERATE;

				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;

				printf("Object clicked, 3rd action.\n");
				printf("	X: %d Y: %d ID: %d\n", c->plr->action_x, 
												   c->plr->action_y, 
												   c->plr->action_id);
				printf("	RX: %d RY: %d\n", c->plr->action_range_x, 
											  c->plr->action_range_y);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_ON_OBJECT)
		{
			/* When player uses an item on an object.  */
			int frame = 0;

			c->plr->action_item_slot = read_unsigned_word_a(c);
			c->plr->action_id = read_unsigned_word_bigendian(c);
			frame = read_unsigned_word(c);
			c->plr->action_y = read_unsigned_word_a(c);
			c->plr->action_item_id = read_unsigned_word_bigendian_a(c);	
			c->plr->action_x = read_unsigned_word(c);

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (is_item_id_invalid(c->plr->action_item_id))
			{
				printf("Invalid item id!\n");
			}
			else if (is_slot_num_invalid(c->plr->action_item_slot, frame))
			{
				printf("Invalid slot num!\n");
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				printf("OBJ ID: %d ITEM ID: %d\n", c->plr->action_id, 
					   c->plr->action_item_id);
				
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 4;

				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_USE_ITEM_ON_OBJECT);

				printf("Item on object.\n");
				printf("Frame: %d\n", frame);
				printf("Action id: %d X: %d Y: %d\n", c->plr->action_id, 
						c->plr->action_x, c->plr->action_y);
				printf("Item id: %d slot: %d\n", c->plr->action_item_id, 
					   c->plr->action_item_slot); 
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_NPC_FIRST_ACTION)
		{
			/* When player clicks on the first option for an npc 
			 * (ex: talk).  */
			int npc_index;
			npc_index = read_unsigned_word_bigendian(c);

			printf("1st action\n");

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x,
					 NPC_SPAWNS[npc_index]->world_y, VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				//reset_player_combat_target(c);
			
				/* Astar pathing, does talking to npcs require perpendicular
				   movements, like melee combat? FIXME  */
				player_face_npc(c, npc_index);

				c->plr->action_x = NPC_SPAWNS[npc_index]->world_x;
				c->plr->action_y = NPC_SPAWNS[npc_index]->world_y;
				/* Sets variables needed for following npc, ensuring a 
				   good path.  */
				c->plr->target_x = c->plr->action_x;
				c->plr->target_y = c->plr->action_y;
				c->plr->targeted_npc = npc_index;
				c->plr->target_is_npc = 1;
				c->plr->is_pursuing_target = 1;

				c->plr->action_id =  NPC_SPAWNS[npc_index]->npc_id;
				c->plr->action_index = npc_index;
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = c->plr->action_range_y
					= NPCS[NPC_SPAWNS[npc_index]->npc_id]->size;
			
				c->plr->action_click_num = 1;

				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_NPC_ACTIVATE);
			}
			
			opcode = read_opcode(c);	
		}
		else if (opcode == CLIENT_PKT_NPC_SECOND_ACTION)
		{
			/* When player clicks on the second option for an npc.  */	
			int npc_index = read_unsigned_word(c);
			printf("2nd action\n");

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x,
					 NPC_SPAWNS[npc_index]->world_y, VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				player_face_npc(c, npc_index);

				/* Sets variables needed for following npc, ensuring a 
				   good path.  */
				c->plr->action_x = NPC_SPAWNS[npc_index]->world_x;
				c->plr->action_y = NPC_SPAWNS[npc_index]->world_y;
				c->plr->target_x = c->plr->action_x;
				c->plr->target_y = c->plr->action_y;
				c->plr->targeted_npc = npc_index;
				c->plr->target_is_npc = 1;
				c->plr->is_pursuing_target = 1;

				c->plr->action_id =  NPC_SPAWNS[npc_index]->npc_id;
				c->plr->action_index = npc_index;
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = c->plr->action_range_y
					= NPCS[NPC_SPAWNS[npc_index]->npc_id]->size;
				c->plr->action_click_num = 2;
				c->plr->action_mask = WILL_TALK;
				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;
				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_NPC_ACTIVATE);
			}
		
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_NPC_THIRD_ACTION)
		{
			/* When player clicks on the third option for an npc.  */	
			int npc_index;
			npc_index = read_unsigned_word_bigendian(c);

			printf("3rd action\n");

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x,
					 NPC_SPAWNS[npc_index]->world_y, VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				player_face_npc(c, npc_index);

				c->plr->action_x = NPC_SPAWNS[npc_index]->world_x;
				c->plr->action_y = NPC_SPAWNS[npc_index]->world_y;
				/* Sets variables needed for following npc, ensuring a 
				   good path.  */
				c->plr->target_x = c->plr->action_x;
				c->plr->target_y = c->plr->action_y;
				c->plr->targeted_npc = npc_index;
				c->plr->target_is_npc = 1;
				c->plr->is_pursuing_target = 1;

				c->plr->action_id =  NPC_SPAWNS[npc_index]->npc_id;
				c->plr->action_index = npc_index;
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = c->plr->action_range_y
					= NPCS[NPC_SPAWNS[npc_index]->npc_id]->size;
				c->plr->action_click_num = 3;
				c->plr->action_mask = WILL_TALK;
				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;
				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_NPC_ACTIVATE);
			}
			
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_ON_NPC)
		{
			/* Frame id, not really used as far as I can tell.  */
			int frame = read_unsigned_word_bigendian_a(c); 
			int item_slot = read_unsigned_word_a(c);
			int item_id = read_unsigned_word(c);
			int npc_index = read_unsigned_word_bigendian_a(c);

			/*
			printf("Item %d at slot %d used on npc named: %s\n", item_id, 
				   item_slot, NPCS[NPC_SPAWNS[npc_index]->npc_id]->name);
			*/

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_slot_num_invalid(item_slot, frame))
			{
				printf("Slot invalid!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x,
					 NPC_SPAWNS[npc_index]->world_y, VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				c->plr->action_id = NPC_SPAWNS[npc_index]->npc_id;
				c->plr->action_index = npc_index;
				c->plr->action_x = NPC_SPAWNS[npc_index]->world_x;
				c->plr->action_y = NPC_SPAWNS[npc_index]->world_y;
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = c->plr->action_range_y
					= NPCS[NPC_SPAWNS[npc_index]->npc_id]->size;


				reset_player_combat_target(c);

				/* Following npc.  */
				c->plr->target_x = c->plr->action_x;
				c->plr->target_y = c->plr->action_y;
				c->plr->targeted_npc = npc_index;
				c->plr->target_is_npc = 1;
				c->plr->is_pursuing_target = 1;

				player_face_npc(c, npc_index);

				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;

				c->plr->action_item_slot = item_slot;
				c->plr->action_item_id = item_id;

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_USE_ITEM_ON_NPC);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_PLR_TRADE)
		{
			int player_index = read_unsigned_word_bigendian_a(c); 

			player_face_player(c, player_index);

			/* Have to subtract one from this because the id sent to clients
			   for each player is their index + 1. This is because clients
			   don't do well with players when their id is 0.  */
			player_index--;

			/* Astar pathing, does talking to npcs require perpendicular
			   movements, like melee combat? FIXME  */
			if (player_move_to_location(c, c->plr->world_x, c->plr->world_y, 
				  CONNECTED_CLIENTS[player_index]->plr->world_x, 
				  CONNECTED_CLIENTS[player_index]->plr->world_y,
				  c->plr->world_z) >= 0)
			{
				reset_player_combat_target(c);

				c->plr->action_x 
					= CONNECTED_CLIENTS[player_index]->plr->world_x;
				c->plr->action_y 
					= CONNECTED_CLIENTS[player_index]->plr->world_y;
				/* Sets variables needed for following npc, ensuring a 
				   good path.  */
				c->plr->target_x = c->plr->action_x;
				c->plr->target_y = c->plr->action_y;
				c->plr->targeted_player = player_index;
				c->plr->target_is_npc = 0;
				c->plr->is_pursuing_target = 1;

				c->plr->action_index = player_index;
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = c->plr->action_range_y
					= 1;
				c->plr->action_mask = WILL_TRADE;
				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = c->plr->action_x;
				c->plr->destination_y = c->plr->action_y;
			}
			else
			{
				packet_send_chatbox_message(c, "I can't go there!");
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_CONTINUE_DIALOGUE)
		{
			/* Handle continue dialogue.  */
			handle_dialogue(c);
			/* Unknown.  */
			read_unsigned_word(c);
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_DROP_ITEM)
		{
			/* Drop item onto the ground.  */
			int frame = read_unsigned_word_a(c);
			int slot_id = read_unsigned_word(c);
			int item_id = read_unsigned_word_bigendian(c);
			
			if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid! %d", item_id);
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else if (is_slot_num_invalid(slot_id, frame))
			{
				printf("Slot invalid!\n");
			}
			else
			{
				/* Check if player has that item, if so, drop it.  */
				if (player_has_item_slot(c, item_id, 1, slot_id) != -1)
				{
					/* Inventory  */
					if (frame == INTR_INV_PLR_ITEMS)
					{
						packet_player_drop_item_on_ground(c, item_id, frame, slot_id);
					}
				}
				else
				{
					printf("Player %s tried to drop an item they don't "
						   "have!\n", c->plr->name);
				}
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_FIRST_ACTION)
		{
			/* Player used first option on an item.  */
			int slot_id = read_unsigned_word_bigendian_a(c);
			int item_id = read_unsigned_word_bigendian_a(c);
			int frame_id = read_unsigned_word_bigendian(c);

			printf("slot: %d item: %d frame: %d\n", slot_id, item_id, frame_id);
			
			if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid! %d", item_id);
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else if (is_slot_num_invalid(slot_id, frame_id))
			{
				printf("Slot invalid!\n");
			}
			else
			{
				handle_item_first_action(c, frame_id, item_id, slot_id);
			}
			
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_SECOND_ACTION)
		{
			/* Player used second option on an item.  */
			int slot_id = read_unsigned_word(c);
			int item_id = read_unsigned_word_a(c);
			int frame_id = read_unsigned_word(c);
			
			if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid! %d", item_id);
			}
			else if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else if (is_slot_num_invalid(slot_id, frame_id))
			{
				printf("Slot invalid!\n");
			}
			else
			{
				handle_item_second_action(c, frame_id, item_id, slot_id);
			}
			
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MOVE_ITEM)
		{
			/* Move item to another place in an interface.  */
			int item_slot_to = read_unsigned_word_a(c);
			int interface_id = read_unsigned_word_bigendian_a(c);
			int item_slot_from = read_unsigned_word(c);
			/* Insert mode, not sure if it is even used for anything.  */
			read_unsigned_byte_s(c);

			if (is_player_dead(c))
			{
				printf("Player dead!\n");
			}
			else if (is_slot_num_invalid(item_slot_to, interface_id) 
					 || is_slot_num_invalid(item_slot_from, interface_id))
			{
				printf("Slot invalid!\n");
			}
			else
			{
				printf("intr id: %d from: %d to: %d\n", interface_id, 
					   item_slot_from, item_slot_to);

				move_item(c, item_slot_from, item_slot_to, interface_id);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_PICKUP_ITEM)
		{
			c->plr->action_id = read_unsigned_word_bigendian(c);
			c->plr->action_y = read_unsigned_word_a(c);
			c->plr->action_x = read_unsigned_word_bigendian(c);
			c->plr->action_z = c->plr->world_z;

			if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_PICKUP_ITEM);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ATTACK_NPC)
		{
			int npc_index = read_unsigned_word_a(c);

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x, 
									   NPC_SPAWNS[npc_index]->world_y,
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				reset_player_combat_target(c);
				set_player_state(c, STATE_ATTACK);

				player_face_npc(c, npc_index);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);
				
				/* Check if everything is in order for combat.  */
				player_init_combat(c, npc_index, -1, -1);
				c->plr->destination_movement_needs_updating = 1;
				c->plr->destination_x = NPC_SPAWNS[npc_index]->world_x;
				c->plr->destination_y = NPC_SPAWNS[npc_index]->world_y;
				printf("target: %d\n", c->plr->targeted_npc);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MAGIC_ON_NPC)
		{
			/* Cast magic on an npc.  */
			int npc_index = read_signed_word(c);
			int spell_id = read_signed_word(c);
			int spell_idx = get_spell_index(spell_id);

			if (is_npc_index_invalid(npc_index))
			{
				printf("NPC index invalid! %d", npc_index);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead!\n");
			}
			else if (is_player_too_far(c, NPC_SPAWNS[npc_index]->world_x, 
									   NPC_SPAWNS[npc_index]->world_y,
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else if (spell_idx == -1)
			{
				printf("Spell not found in server database!\n");
				printf("ID: %d\n", spell_id);
			}
			else
			{
				set_player_state(c, STATE_ATTACK);
				reset_player_combat_target(c);

				printf("INDX: %d SPELL: %d\n", npc_index, spell_id);

				printf("Spell idx: %d Name: %s\n", spell_idx,
					   SPELLS[spell_idx]->name);

				c->plr->cast_spell_id = spell_id;
				player_face_npc(c, npc_index);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				/* Check if everything is in order for combat.  */
				player_init_combat(c, npc_index, -1, spell_id);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MAGIC_ON_PLAYER)
		{
			/* Cast magic on a player.  */
			int player_index = read_signed_word(c);
			int spell_id = read_signed_word_a(c);
	
			printf("PACKET IS CURRENTLY UNHANDLED, FIX\n");
			/* Have to subtract one from this because the id sent to clients
			   for each player is their index + 1. This is because clients
			   don't do well with players when their id is 0.  */
			player_index--;
			
			player_stop_all_movement(c);
			packet_reset_minimap_flag(c);
			player_update_world_location(c);
			
			player_turn_to_world_coord(c, 
				CONNECTED_CLIENTS[player_index]->plr->world_x, 
				CONNECTED_CLIENTS[player_index]->plr->world_y);
			
			/* If spell is a teleother.  */
			if (spell_id == 12425 || spell_id == 12435 || spell_id == 12455)
			{
				packet_send_chatbox_message(c, "You can't cast that spell on that!");
				c->plr->cast_spell_id = spell_id;
			}
			else
			{
				/* This is where combat handling will go for pvp when done.  */
				/*
				c->plr->cast_spell_id = spell_id;
				if (can_range_in_direction(c->plr->world_x, c->plr->world_y, 
					 NPC_SPAWNS[npc_spawn_index]->world_x, NPC_SPAWNS[npc_spawn_index]->world_y,
					 c->plr->world_z) != 1)
				{
					packet_send_chatbox_message(c, "I can't hit that from here.");
				}
				else if (NPC_SPAWNS[npc_spawn_index]->attackable == 0)
				{
					packet_send_chatbox_message(c, "I can't attack this person!");
				}
				else
				{
					c->plr->in_combat = 1;
					c->plr->attacking_npc = 1;
					c->plr->cast_spell_id = spell_id;
					c->plr->targeted_npc = npc_spawn_index;
				}
				*/
			}
			
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MAGIC_ON_ITEM)
		{
			/* Magic cast on item.  */
			int item_slot = read_signed_word_bigendian_a(c);
			int frame_id = read_signed_word(c);
			int item_id = read_signed_word(c);
			int spell_id = read_signed_word_bigendian(c);
			int spell_idx = get_spell_index(spell_id);

			if (is_item_id_invalid(item_id))
			{
				printf("Item ID invalid! %d", item_id);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_slot_num_invalid(item_slot, frame_id))
			{
				printf("Slot invalid!\n");
			}
			else if (spell_idx == -1)
			{
				printf("Spell not found in server database!\n");
				printf("ID: %d\n", spell_id);
			}
			else if (is_player_casting_a_spell(c))
			{
				printf("Currently casting something!\n");
			}
			else
			{
				printf("Magic on item.\n");

				printf("Spell %s cast on %d at slot %d in interface %d\n", 
						SPELLS[spell_idx]->name,
						item_id, item_slot, frame_id);
				handle_magic_spell_on_item(c, spell_id, item_id, item_slot);
			}
			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_MAGIC_ON_OBJECT)
		{
			c->plr->action_id = read_unsigned_word_a(c);
			/* Spell cast, rename this or create a new var for it.  */
			c->plr->action_spell_id = read_unsigned_word_bigendian_a(c);
			c->plr->action_y = read_unsigned_word_bigendian(c);
			c->plr->action_x = read_unsigned_word_bigendian_a(c);

			int spell_idx = get_spell_index(c->plr->action_spell_id);

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (spell_idx == -1)
			{
				printf("Spell not found in server database!\n");
				printf("ID: %d\n", c->plr->action_spell_id);
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				printf("Player cast spell id %d on object %d at coords %d %d.\n",
					   c->plr->action_spell_id, c->plr->action_id, 
					   c->plr->action_x, c->plr->action_y);

				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 5;

				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_USE_SPELL_ON_OBJECT);
			}

			opcode = read_opcode(c);
		}
		else if (opcode == CLIENT_PKT_ITEM_ON_OBJECT)
		{
			/* When player uses an item on an object.  */
			int frame = 0;

			c->plr->action_item_slot = read_unsigned_word_a(c);
			c->plr->action_id = read_unsigned_word_bigendian(c);
			frame = read_unsigned_word(c);
			c->plr->action_y = read_unsigned_word_a(c);
			c->plr->action_item_id = read_unsigned_word_bigendian_a(c);	
			c->plr->action_x = read_unsigned_word(c);

			printf("PKT ON OBJ\n");

			printf("Item on object.\n");
			printf("Frame: %d\n", frame);
			printf("Action id: %d X: %d Y: %d\n", c->plr->action_id, 
					c->plr->action_x, c->plr->action_y);
			printf("Item id: %d slot: %d\n", c->plr->action_item_id, 
				   c->plr->action_item_slot); 

			if (is_object_id_invalid(c->plr->action_id))
			{
				printf("invalid action id!\n");
			}
			else if (is_item_id_invalid(c->plr->action_item_id))
			{
				printf("Invalid item id!\n");
			}
			else if (is_slot_num_invalid(c->plr->action_item_slot, frame))
			{
				printf("Invalid slot num!\n");
			}
			else if (is_player_dead_or_frozen(c, 1))
			{
				printf("Player dead or frozen!\n");
			}
			else if (is_player_too_far(c, c->plr->action_x, c->plr->action_y, 
									   VIEWPORT_DISTANCE))
			{
				printf("Player too far!\n");
			}
			else
			{
				printf("OBJ ID: %d ITEM ID: %d\n", c->plr->action_id, 
					   c->plr->action_item_id);
				
				c->plr->action_z = c->plr->world_z;
				c->plr->action_range_x = get_object_size_x(c->plr->action_id);
				c->plr->action_range_y = get_object_size_y(c->plr->action_id);
				c->plr->action_total_time_acted = 0;
				c->plr->action_click_num = 4;

				reset_player_combat_target(c);
				player_clear_walking_queue(c);
				packet_reset_minimap_flag(c);

				set_player_state(c, STATE_ACTION);
				set_player_state_type(c, STATE_USE_ITEM_ON_OBJECT);

				printf("Item on object.\n");
				printf("Frame: %d\n", frame);
				printf("Action id: %d X: %d Y: %d\n", c->plr->action_id, 
						c->plr->action_x, c->plr->action_y);
				printf("Item id: %d slot: %d\n", c->plr->action_item_id, 
					   c->plr->action_item_slot); 
			}

			opcode = read_opcode(c);
		}
		else
		{
			printf("	Unhandled packet: %d\n", opcode);
			printf("INSTREAM\n");
			print_instream(c, 40);
			printf("	Last packets:\n");
			print_player_last_packets(c);
			break;
		}
	}
	c->bytes_read = 0;
}

/** Handles all commands sent from the client to the server.
 * @param *c Client instance.
 * @param string[] Command string entered by the user.
 */
void handle_commands(struct client *c, char string[])
{
	/* String buffer for output to user.  */
	char return_msg[200];
	
	/* Commands that normal players can use as well.
	   player_rights - 0 == normal player.  */
	if (c->plr->player_rights >= 0)
	{
		if (strcmp(string, "posv") == 0)
		{
			int pos = get_collision(c->plr->world_x, c->plr->world_y, 
									c->plr->world_z);
			snprintf(return_msg, 100, "X:%d Y:%d Z:%d (%d)", 
					c->plr->world_x, c->plr->world_y, c->plr->world_z,
					(pos & ~(TAKEN_MASK)));
			packet_send_chatbox_message(c, return_msg);
			snprintf(return_msg, 100, "RX:%d RY:%d", c->plr->world_region_x,
					 c->plr->world_region_y);
			packet_send_chatbox_message(c, return_msg);
			snprintf(return_msg, 100, "In RX:%d In RY:%d", 
					 c->plr->in_region_x,
					 c->plr->in_region_y);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strcmp(string, "pos") == 0)
		{
			/* Print position, collision data without the taken mask.  */
			int pos = get_collision(c->plr->world_x, c->plr->world_y, 
									c->plr->world_z);
			int reg_id = get_region_id(c->plr->world_x, c->plr->world_y);
			snprintf(return_msg, 100, "X:%d Y:%d Z:%d (%d)", 
					c->plr->world_x, c->plr->world_y, c->plr->world_z,
					(pos & ~(TAKEN_MASK)));
			packet_send_chatbox_message(c, return_msg);
			snprintf(return_msg, 100, "Reg ID: %d In Reg X:%d In Reg Y:%d", 
					 reg_id, c->plr->in_region_x, c->plr->in_region_y);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strcmp(string, "rid") == 0)
		{
			int reg_id = get_region_id(c->plr->world_x, c->plr->world_y);

			snprintf(return_msg, 100, "Region id: %d\n", reg_id);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strcmp(string, "opos") == 0)
		{
			int reg_id = get_region_id(c->plr->world_x, c->plr->world_y);
			int reg_idx = get_region_index(reg_id);
			int id, face, type;

			if (reg_id == -1 || reg_idx == -1)
			{

			}
			else
			{
				if (get_region_object_data(reg_idx, c->plr->world_x, 
					c->plr->world_y, c->plr->world_z, &id, &face, &type) != -1)
				{
					printf("ID: %d face %d type %d\n", id, face, type);
					
					printf("name: %s\n", OBJECTS[id]->name);
					printf("desc: %s\n", OBJECTS[id]->desc);
				}
				else
				{
					printf("none here.\n");
				}
			}
	
		}
		else if (strcmp(string, "save") == 0)
		{
			save_player_data(c);
			packet_send_chatbox_message(c, "Saved player data.");
		}
		else if (strcmp(string, "save_new") == 0)
		{
			save_player_data_new(c);
			packet_send_chatbox_message(c, "Saved player data.");
		}
		else if (strcmp(string, "uptime") == 0)
		{
			int uptime_seconds = get_uptime_seconds();
			int uptime_minutes = 0;
			int uptime_hours = 0;
			if (uptime_seconds > 60 && uptime_seconds < 3600)
			{
				uptime_minutes = uptime_seconds / 60;
				snprintf(return_msg, 100, 
						 "Server has been up for %d minute(s)"
						 " and %d second(s).\n",
						 uptime_minutes, 
						 (uptime_seconds - (uptime_minutes * 60))); 
			}
			else if (uptime_seconds > 3600)
			{
				uptime_minutes = uptime_seconds / 60;
				uptime_hours = uptime_minutes / 60;
				snprintf(return_msg, 100, 
						 "Server has been up for %d hour(s)"
						 " and %d minute(s).\n",
						 uptime_hours, (uptime_minutes - (uptime_hours * 60))); 
			}
			else
			{
				snprintf(return_msg, 100, 
						 "Server has been up for %d second(s).\n", 
						 uptime_seconds);
			}
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strcmp(string, "maxhit") == 0)
		{
			char max_hit[100];
			calculate_max_hit(c);
			snprintf(max_hit, 100, "Your max hit with your weapon is: %d\n", 
					 c->plr->max_hit);
			packet_send_chatbox_message(c, max_hit);
		}
		else if (strcmp(string, "rstr") == 0)
		{
			char r_str[100];
			snprintf(r_str, 100, "Your ranged strength is: %d\n", 
					 c->plr->ranged_strength);
			packet_send_chatbox_message(c, r_str);
		}
		/* Send what items will be kept on death to the player's chat.  */
		/* TODO: Fix this up so that it will handle if player is not in an
		 * unsafe area, and tell them they will keep everything, and will
		 * be affected by prayers.  */
		else if (strncmp(string, "items_kept ", 11) == 0)
		{
			int num = 3;
			int ret = sscanf(string, "items_kept %d", &num);

			if (ret == 1)
			{
				int *items = calloc(num, sizeof(int));
				int *location = calloc(num, sizeof(int));

				get_most_valuable_items(c, items, location, num);
				print_valueable_items(c, items, location, num);

				free(items);
				free(location);
			}
			else
			{
				int *items = calloc(3, sizeof(int));
				int *location = calloc(3, sizeof(int));

				get_most_valuable_items(c, items, location, 3);
				print_valueable_items(c, items, location, 3);

				free(items);
				free(location);
			}
		}
		else if (strcmp(string, "charges") == 0)
		{
			int x;

			for (x = 0; x < INV_SIZE; x++)
			{
				if (c->plr->items_charges[x] > 0)
				{
					snprintf(return_msg, 100, "Your %s in your inventory has "
							 "%d charge(s) left.", 
							 ITEMS[c->plr->items[x]]->name,
							 c->plr->items_charges[x]);
					packet_send_chatbox_message(c, return_msg);
				}
			}
			for (x = 0; x < EQUIP_SIZE; x++)
			{
				if (c->plr->equipment_charges[x] > 0)
				{
					snprintf(return_msg, 100, "Your equipped %s has "
							 "%d charge(s) left.", 
							 ITEMS[c->plr->equipment[x]]->name,
							 c->plr->equipment_charges[x]);
					packet_send_chatbox_message(c, return_msg);
				}
			}
		}
		else if (strncmp(string, "sfx ", 4) == 0)
		{
			int sfx_id, sfx_delay = 100, sfx_volume = 64;
			int ret = sscanf(string, "sfx %d %d %d", 
							 &sfx_id, &sfx_delay, &sfx_volume);
			if (ret == 3)
			{
				packet_play_sfx(c, sfx_id, sfx_delay, sfx_volume);
			}
			else if (ret == 2)
			{
				packet_play_sfx(c, sfx_id, sfx_delay, -1);
			}
			else if (ret == 1)
			{
				packet_play_sfx(c, sfx_id, 0, -1);
			}
			else
			{
				packet_send_chatbox_message(c, "sfx: id [delay] [vol]");
			}
		}
	}
	/* Commands that can be used by player mods and above.  */
	if (c->plr->player_rights >= 1)
	{
		if (strcmp(string, "to_spawn") == 0
			|| strcmp(string, "home") == 0
			|| strcmp(string, "h") == 0)
		{
			packet_send_chatbox_message(c, "Going to spawn point...");
			c->plr->tele_x = c->plr->home_x;
			c->plr->tele_y = c->plr->home_y;
			c->plr->tele_z = c->plr->home_z;
		}
	}
	/* Commands that can only be used by admins.  */
	if (c->plr->player_rights == 2)
	{
		if (strcmp(string, "set_spawn") == 0
			|| strcmp(string, "set_home") == 0)
		{
			packet_send_chatbox_message(c, "New spawn set here!");
			c->plr->home_x = c->plr->world_x;
			c->plr->home_y = c->plr->world_y;
			c->plr->home_z = c->plr->world_z;
		}
		if (strcmp(string, "test") == 0)
		{
			/*
			snprintf(return_msg, 100, "%s %s", 
					 get_player_state_text(c->plr->state), 
					 get_player_state_text(c->plr->movement_state));

			printf("%s\n", return_msg);
			packet_send_chatbox_message(c, return_msg);
			*/

			player_add_straight_line_path_to_walking_queue(c, 
				c->plr->world_x + 5, c->plr->world_y + 5, 1, 1);
			/*
			update_unlocked_music_list(c);
			*/
			//packet_play_song(c, 22);
			/*
			c->plr->emote = AGILITY_EMOTE_CROSS_LOG;

			c->plr->walking_emote = c->plr->running_emote 
				= c->plr->standing_emote = c->plr->turn_about_emote 
				= c->plr->turn_ccw_emote = c->plr->turn_cw_emote 
				= AGILITY_EMOTE_CROSS_LOG;

			c->plr->running_emote = AGILITY_EMOTE_CROSS_LOG;

			c->plr->update_masks[ANIMATION_REQ] = 1;
			c->plr->update_masks[APPEARANCE_UPDATE] = 1;
			c->plr->update_required = 1;
			*/

		}
		else if (strncmp(string, "er ", 3) == 0)
		{
			int emote_num = 0, emote_time = 0, ret = 0; 
			
			ret = sscanf(string, "er %d %d", &emote_num, &emote_time);
			
			if (ret == 2 && (emote_num < PROTOCOL_MAX_EMOTES 
							 && emote_num >= 0))
			{
				player_play_animation_repeats(c, emote_num, emote_time);
				snprintf(return_msg, 100, "Playing emote repeat: %d", 
						 c->plr->animation_request);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				printf("Emote entered: %d\n", emote_num);
				snprintf(return_msg, 100,
						"Error! Emote id must be less than %d!", 
						PROTOCOL_MAX_EMOTES);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		if (strncmp(string, "ta ", 3) == 0)
		{
			int timed_action = 0, ticks = 0, ret = 0;
			
			ret = sscanf(string, "ta %d %d", &timed_action, &ticks);
			
			if (ret == 2)
			{
				set_timed_action(c, timed_action, ticks);
				snprintf(return_msg, 100, "Playing timed action: %d", 
						 timed_action);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		if (strncmp(string, "questl ", 7) == 0)
		{
			int x = 0;
			char text[100];

			for (x = 0; x < 100; x++)
			{
				snprintf(text, 99, "%d", x);	
				write_quest_log_line(c, text, x);
			}
		}
		if (strncmp(string, "questd ", 7) == 0)
		{
			int quest_num = 0, quest_data = 0, quest_state = 0, ret = 0; 
			
			ret = sscanf(string, "questd %d %d %d", &quest_num, &quest_data, 
						 &quest_state);
			
			if (ret == 3 && (quest_num < PROTOCOL_NUM_QUESTS 
							 && quest_num >= 0))
			{
				snprintf(return_msg, 100, "Changing quest %d data %d to %d", 
						 quest_num, quest_data, quest_state);
				packet_send_chatbox_message(c, return_msg);

				if (quest_data >= 0 && quest_data <= 4)
				{
					c->plr->quest_data[quest_num][quest_data] = quest_state;
				}
			}
			else
			{
				printf("Quest entered: %d\n", quest_num);
				snprintf(return_msg, 100,
						"Error! Quest id must be less than %d!", 
						PROTOCOL_NUM_QUESTS);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		if (strncmp(string, "quest ", 6) == 0)
		{
			int quest_num = 0, quest_state = 0, ret = 0; 
			
			ret = sscanf(string, "quest %d %d", &quest_num, &quest_state);
			
			if (ret == 2 && (quest_num < PROTOCOL_NUM_QUESTS 
							 && quest_num >= 0))
			{
				snprintf(return_msg, 100, "Changing quest %d state to %d", 
						 quest_num, quest_state);
				packet_send_chatbox_message(c, return_msg);
				c->plr->quests[quest_num] = quest_state;
				update_quest_log_status(c, quest_num);
			}
			else
			{
				printf("Quest entered: %d\n", quest_num);
				snprintf(return_msg, 100,
						"Error! Quest id must be less than %d!", 
						PROTOCOL_NUM_QUESTS);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		if (strncmp(string, "emote ", 6) == 0)
		{
			int emote_num = 0, ret = 0; 
			
			ret = sscanf(string, "emote %d", &emote_num);
			
			if (ret == 1 && (emote_num < PROTOCOL_MAX_EMOTES 
							 && emote_num >= 0))
			{
				player_play_animation(c, emote_num);
				snprintf(return_msg, 100, "Playing emote: %d", 
						 c->plr->animation_request);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				printf("Emote entered: %d\n", emote_num);
				snprintf(return_msg, 100,
						"Error! Emote id must be less than %d!", 
						PROTOCOL_MAX_EMOTES);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		else if (strncmp(string, "sitem ", 6) == 0)
		{
			int ret = 0; 
			char search_string[100];
			
			ret = sscanf(string, "sitem %99s", search_string);
			
			if (ret == 1)
			{
				int results_num = 0;
				int *results;
				results = search_item_names_and_desc_for_string(search_string, &results_num);

				if (results_num == 0)
				{
					packet_send_chatbox_message(c, "No matches.");
				}
				else
				{
					for (int x = 0; x < results_num; x++)
					{
						if (x == 0)
						{
							packet_send_chatbox_message(c, "========");
						}
						snprintf(return_msg, 200,
								"%4d: %s %s", results[x], 
								ITEMS[results[x]]->name, 
								ITEMS[results[x]]->desc);
						packet_send_chatbox_message(c, return_msg);
					}
				}

				free(results);
			}
		}
		else if (strncmp(string, "teler ", 6) == 0)
		{
			int pos_x, pos_y, pos_z;
			int ret = sscanf(string, "teler %d %d %d", &pos_x, &pos_y, &pos_z);

			if (ret == 1)
			{
				char tele_str[50];
				snprintf(tele_str, 50, "Teleport relative: x:%d", 
						 pos_x);
				packet_send_chatbox_message(c, tele_str);
				c->plr->tele_x = (c->plr->world_x + pos_x);
				c->plr->tele_y = c->plr->world_y;
				c->plr->tele_z = c->plr->world_z;
			}
			else if (ret == 2)
			{
				char tele_str[50];
				snprintf(tele_str, 50, "Teleport relative: x:%d y:%d", 
						 pos_x, pos_y);
				packet_send_chatbox_message(c, tele_str);
				c->plr->tele_x = (c->plr->world_x + pos_x);
				c->plr->tele_y = (c->plr->world_y + pos_y);
				c->plr->tele_z = c->plr->world_z;
			}
			else if (ret == 3)
			{
				char tele_str[50];
				int pos_z_diff = (c->plr->world_z + pos_z);
				snprintf(tele_str, 50, "Teleport relative: x:%d y:%d z:%d", 
						 pos_x, pos_y, pos_z);
				packet_send_chatbox_message(c, tele_str);
				c->plr->tele_x = (c->plr->world_x + pos_x);
				c->plr->tele_y = (c->plr->world_y + pos_y);

				/* Check z is within proper range.  */
				if (pos_z_diff > 0 && pos_z_diff < 4)
				{
					c->plr->tele_z = (c->plr->world_z + pos_z);
				}
				else
				{
					packet_send_chatbox_message(c, "Invalid z coord, must be from 0 to 3!");
				}
			}
			else
			{
				packet_send_chatbox_message(c, "Usage: teler [pos_x] [pos_y] [pos_z]");
				packet_send_chatbox_message(c, "Values are relative to your position.");
			}
		}
		else if (strncmp(string, "telereg ", 8) == 0)
		{
			int region;
			int ret = sscanf(string, "telereg %d", &region);
			int valid = 0, x = 0;

			if (region < 0 || region > PROTOCOL_MAX_REGIONS)
			{
				packet_send_chatbox_message(c, "Invalid tele coords!");
			}
			else
			{
				valid = 1;
			}
			
			if (ret == 1 && valid == 1)
			{
				for (x = 0; x < REGION_LIST_NUM; x++)
				{
					if (region == REGION_LIST[x]->id)
					{
						c->plr->tele_x = REGION_LIST[x]->world_x;
						c->plr->tele_y = REGION_LIST[x]->world_y;
						c->plr->tele_z = c->plr->world_z;
					}
				}
			}
		}
		else if (strncmp(string, "tele ", 5) == 0)
		{
			int pos_x, pos_y, pos_z;
			int ret = sscanf(string, "tele %d %d %d", &pos_x, &pos_y, &pos_z);
			int valid = 0;

			if (pos_x < 0 || pos_y < 0)
			{
				packet_send_chatbox_message(c, "Invalid tele coords!");
			}
			else if (ret > 2 && (pos_z < 0 || pos_z > 3))
			{
				packet_send_chatbox_message(c, "Invalid tele height coords!");
			}
			else
			{
				valid = 1;
			}
			
			if (ret == 2 && valid == 1)
			{
				char tele_str[50];
				snprintf(tele_str, 50, "Teleport to: x:%d y:%d", pos_x, pos_y);
				packet_send_chatbox_message(c, tele_str);
				c->plr->tele_x = pos_x;
				c->plr->tele_y = pos_y;
				c->plr->tele_z = c->plr->world_z;
			}
			else if (ret == 3 && valid == 1)
			{
				char tele_str[50];
				snprintf(tele_str, 50, "Teleport to: x:%d y:%d z:%d", 
						 pos_x, pos_y, pos_z);
				packet_send_chatbox_message(c, tele_str);
				c->plr->tele_x = pos_x;
				c->plr->tele_y = pos_y;
				c->plr->tele_z = pos_z;
			}
			else
			{
				/* If number test didn't work, check if the player typed in 
				   the name of a place instead.  */
				char tele_name[50];
				ret = sscanf(string, "tele %49s", tele_name);
				tele_name[49] = '\0';

				if (ret == 1)
				{
					/* TODO: Replace this with loading from a file, maybe?  */
					if (strcmp(tele_name, "lumbridge") == 0)
					{
						c->plr->tele_x = 3222;
						c->plr->tele_y = 3222;
						c->plr->tele_z = 0;
						packet_send_chatbox_message(c, "Teleporting to Lumbridge...");
					}
					else if (strcmp(tele_name, "falador") == 0)
					{
						c->plr->tele_x = 2965;
						c->plr->tele_y = 3380;
						c->plr->tele_z = 0;
						packet_send_chatbox_message(c, "Teleporting to Falador...");
					}
					else if (strcmp(tele_name, "varrock") == 0)
					{
						c->plr->tele_x = 3210;
						c->plr->tele_y = 3424;
						c->plr->tele_z = 0;
						packet_send_chatbox_message(c, "Teleporting to Varrock...");
					}
					else
					{
						packet_send_chatbox_message(c, "Invalid location!");
					}
				}
				else
				{
					packet_send_chatbox_message(c, "Invalid command!");
				}
			}
		}
		else if (strncmp(string, "item ", 5) == 0)
		{
			int item_num = -1, amount = 0, charges = 0, x = 0, func_ret = 0;
			int ret = sscanf(string, "item %d %d %d", 
							 &item_num, &amount, &charges);

			if (item_num < 0 || item_num >= PROTOCOL_MAX_ITEMS)
			{
				packet_send_chatbox_message(c, "Item id out of range!");
				return;
			}

			if (ret == 2)
			{
				if (ITEMS[item_num]->stacking == 0)
				{
					for (x = 0; x < amount; x++)
					{
						func_ret = add_item_to_inv(c, item_num, amount, 
												   get_item_default_charges(item_num));
						if (func_ret == 0)
						{
							break;
						}
					}
				}
				else
				{
					add_item_to_inv(c, item_num, amount, 
									get_item_default_charges(item_num));
				}
			}
			else if (ret == 1 && item_num > -1)
			{
				if (add_item_to_inv(c, item_num, 1, get_item_default_charges(item_num))
					!= 0)
				{
					snprintf(return_msg, 100, "Added item %d to inv.", 
							 item_num);
					packet_send_chatbox_message(c, return_msg);
				}
			}
		}
		else if (strcmp(string, "sfxtest") == 0)
		{
			packet_play_sfx(c, 233, 200, -1);
			packet_play_sfx(c, 23, 300, -1);
			packet_play_sfx(c, 1238, 500, -1);
			packet_play_sfx(c, 1241, 400, -1);
			packet_play_sfx(c, 10, 200, -1);
			packet_play_sfx(c, 1, 300, -1);
			packet_play_sfx(c, 34, 500, -1);
			packet_play_sfx(c, 99, 400, -1);
		}
		else if (strncmp(string, "music ", 6) == 0)
		{
			int music_id;
			int ret = sscanf(string, "music %d", &music_id);
			if (ret == 1)
			{
				packet_play_song(c, music_id);
			}
		}
		/* Various equipment testing commands.  */
		else if (strcmp(string, "melee") == 0)
		{
			add_item_to_inv(c, 1203, 1, -1);
			packet_send_chatbox_message(c, "Added melee test equipment.");
		}
		else if (strcmp(string, "range") == 0)
		{
			add_item_to_inv(c, 884, 100, -1);
			add_item_to_inv(c, 839, 1, -1);
			packet_send_chatbox_message(c, "Added ranged test equipment.");
		}
		else if (strcmp(string, "magic") == 0)
		{
			add_item_to_inv(c, 1381, 1, -1);
			add_item_to_inv(c, 558, 100, -1);
			add_item_to_inv(c, 556, 100, -1);
			packet_send_chatbox_message(c, "Added magic test equipment.");
		}
		else if (strcmp(string, "firemaking") == 0)
		{
			add_item_to_inv(c, 590, 1, -1);
			add_item_to_inv(c, 1511, 27, -1);
			packet_send_chatbox_message(c, "Added firemaking test equipment.");
		}
		else if (strcmp(string, "tools") == 0)
		{
			add_item_to_inv(c, 1267, 1, -1);
			add_item_to_inv(c, 1349, 1, -1);
			add_item_to_inv(c, 590, 1, -1);
			add_item_to_inv(c, 303, 1, -1);
			packet_send_chatbox_message(c, "Added misc equipment.");
		}
		else if (strcmp(string, "craft") == 0)
		{
			add_item_to_inv(c, 1741, 5, -1);
			add_item_to_inv(c, 1733, 1, -1);
			add_item_to_inv(c, 1734, 1, 5);
			packet_send_chatbox_message(c, "Added crafting test equipment.");
		}
		else if (strcmp(string, "cooking") == 0)
		{
			add_item_to_inv(c, 317, 1, -1);
			add_item_to_inv(c, 317, 1, -1);
			add_item_to_inv(c, 317, 1, -1);
			add_item_to_inv(c, 317, 1, -1);
			add_item_to_inv(c, 2287, 1, -1);
			add_item_to_inv(c, 2287, 1, -1);
			add_item_to_inv(c, 2287, 1, -1);
			add_item_to_inv(c, 2287, 1, -1);
			packet_send_chatbox_message(c, "Added cooking test equipment.");
		}
		else if (strcmp(string, "mine") == 0)
		{
			packet_send_chatbox_message(c, "Use command: \"::tools\" if needed.\n");
			c->plr->tele_x = 3179;
			c->plr->tele_y = 3368;
			c->plr->tele_z = 0;
			packet_send_chatbox_message(c, "Teleporting to mining area.");
		}
		else if (strcmp(string, "fish") == 0)
		{
			packet_send_chatbox_message(c, "Use command: \"::tools\" if needed.\n");
			c->plr->tele_x = 3086;
			c->plr->tele_y = 3230;
			c->plr->tele_z = 0;
			packet_send_chatbox_message(c, "Teleporting to fishing area.");
		}
		else if (strcmp(string, "dmg") == 0)
		{
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			/*
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			deal_damage_to_player(c, 1, 0, 0, 0, -1, -1);
			*/
		}
		else if (strcmp(string, "allxp") == 0)
		{
			int x;

			for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
			{
				add_skill_xp(c, x, 99.0);
			}
		}
		else if (strcmp(string, "los") == 0)
		{
			int x;

			printf("$$$$$$$$$$$$$$$$$\n");
			for (x = 0; x < c->plr->local_npcs_num; x++)
			{
				printf("id: %4d in sight: %d\n", c->plr->local_npcs[x]->npc_id,
					   check_line_of_sight(c, c->plr->local_npcs[x], 5));
			}
		}
		/* XXX: Wipes player's inventory. Testing only.  */
		else if (strcmp(string, "clear_inv") == 0)
		{
			empty_player_inventory(c);
			packet_send_chatbox_message(c, "Cleared inventory.");
		}
		/* XXX: Wipes players' equipment. Testing only.  */
		else if (strcmp(string, "clear_equip") == 0)
		{
			empty_player_equipment(c);
			packet_send_chatbox_message(c, "Cleared equipment.");
		}
		else if (strcmp(string, "heal") == 0)
		{
			c->plr->level[HITPOINTS_SKILL] = 
				c->plr->base_level[HITPOINTS_SKILL];
			c->plr->update_masks[APPEARANCE_UPDATE] = 1;
			c->plr->update_required = 1;
			packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
			packet_send_chatbox_message(c, "You are healed back to full health.");
		}
		else if (strncmp(string, "gfx ", 4) == 0)
		{
			int id = 0, height = 0;
			int ret = sscanf(string, "gfx %d %d", &id, &height);

			if (id < 0 || id >= PROTOCOL_MAX_GFX)
			{
				snprintf(return_msg, 100,
						"Error! Gfx id must be less than %d!", 
						PROTOCOL_MAX_GFX);
				packet_send_chatbox_message(c, return_msg);
				return;
			}

			if (ret == 1)
			{
				player_draw_gfx(c, id);

				snprintf(return_msg, 100, "Playing gfx id: %d", id);
			}
			else if (ret == 2)
			{
				player_draw_gfx_at_height(c, id, height);

				snprintf(return_msg, 100, "Playing gfx id: %d height: %d", id,
						 height);
			}

			packet_send_chatbox_message(c, return_msg);
		}
		else if (strncmp(string, "axp ", 4) == 0)
		{
			int skill_id;
			float skill_xp;
			int ret = sscanf(string, "axp %d %f", 
							 &skill_id, &skill_xp);

			if (ret == 2)
			{
				add_skill_xp(c, skill_id, skill_xp);
			}
		}
		else if (strncmp(string, "xp ", 3) == 0)
		{
			int skill_id;
			float skill_xp;
			int ret = sscanf(string, "xp %d %f", 
							 &skill_id, &skill_xp);

			if (ret == 2)
			{
				c->plr->level[skill_id] = get_level_for_xp(skill_xp);
				c->plr->base_level[skill_id] = get_level_for_xp(skill_xp);
				c->plr->xp[skill_id] = skill_xp;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				packet_send_updated_skill_stats(c, skill_id);
				calculate_player_combat_level(c);

				snprintf(return_msg, 100, "Set skill %d xp to %.2f level %d.", 
						 skill_id, skill_xp, c->plr->level[skill_id]);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		else if (strncmp(string, "level_up ", 9) == 0)
		{
			int skill_id;
			float skill_level;
			int ret = sscanf(string, "level_up %d %f", 
							 &skill_id, &skill_level);

			if (ret == 2)
			{
				c->plr->level[skill_id] = skill_level;
				c->plr->base_level[skill_id] = skill_level;
				c->plr->xp[skill_id] = get_xp_needed_for_level(skill_level - 1);
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				packet_send_updated_skill_stats(c, skill_id);
				calculate_player_combat_level(c);

				snprintf(return_msg, 100, "Leveled skill %d to level %d.", 
						 skill_id, (int) skill_level);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		else if (strncmp(string, "set_level ", 10) == 0)
		{
			int skill_id;
			float skill_level;
			int ret = sscanf(string, "set_level %d %f", 
							 &skill_id, &skill_level);

			if (ret == 2)
			{
				c->plr->level[skill_id] = skill_level;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				packet_send_updated_skill_stats(c, skill_id);

				snprintf(return_msg, 100, "Set skill %d to level %d.", 
						 skill_id, (int) skill_level);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		/* Npc transform testing.  */
		else if (strncmp(string, "npt ", 4) == 0)
		{
			int npc_id;
			int valid = 0;
			int ret = sscanf(string, "npt %d", &npc_id);

			if (npc_id < 0 || npc_id >= PROTOCOL_MAX_NPCS || ret == 0)
			{
				/* FIXME: Might not fully reset animations.  */
				packet_send_chatbox_message(c, "NPC id not valid, clearing npc transform.");
				/* Reset emotes.  */
				apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
				player_replay_animation(c);
				c->plr->player_npc_id = -1;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
			}
			else
			{
				valid = 1;
			}

			if (ret == 1 && valid == 1)
			{
				c->plr->player_npc_id = npc_id;
				c->plr->standing_emote = NPCS[npc_id]->stand_anim;
				c->plr->walking_emote = NPCS[npc_id]->walk_anim;
				
				if (NPCS[npc_id]->turn_about_anim == 0)
				{
					c->plr->turn_about_emote = c->plr->turn_cw_emote
					= c->plr->turn_ccw_emote = c->plr->walking_emote;
				}
				else
				{
					c->plr->turn_about_emote = NPCS[npc_id]->turn_about_anim;
					c->plr->turn_cw_emote = NPCS[npc_id]->turn_cw_anim;
					c->plr->turn_ccw_emote = NPCS[npc_id]->turn_ccw_anim;
				}
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				
				snprintf(return_msg, 100, "Transforming to npc #%d.", npc_id);
				packet_send_chatbox_message(c, return_msg);
			}
		}
		else if (strcmp(string, "chat1") == 0)
		{
			c->plr->action_menu_id = 124;
			c->plr->action_menu_skill_id = CRAFTING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->next_chat_id = 0;
			handle_dialogue(c);	
		}
		else if (strcmp(string, "aqtest") == 0)
		{
			action_queue_clear(c);
			action_queue_add_action(c, AQ_PLAY_EMOTE, 0, 1110, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_PLAY_GFX, 3, 135, 100, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 3, 
						c->plr->world_x + 1, c->plr->world_y + 1, 0, 0, 0, 0);
			action_queue_start(c);
		}
		else if (strcmp(string, "aqtest1") == 0)
		{
			action_queue_clear(c);
			action_queue_add_action(c, AQ_PLAY_EMOTE, 0, 1110, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_PLAY_GFX, 0, 135, 100, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 5, 
						c->plr->world_x + 1, c->plr->world_y + 1, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_PLAY_EMOTE, 3, 799, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 7, 
						c->plr->world_x + 2, c->plr->world_y + 2, 0, 0, 0, 0);
			action_queue_start(c);
		}
		else if (strcmp(string, "aqtest2") == 0)
		{
			action_queue_clear(c);
			action_queue_add_action(c, AQ_PLAY_EMOTE, 3, 799, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_SET_WALKING_EMOTE, 3, 799, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_SET_STANDING_EMOTE, 3, 803, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_PLAY_GFX, 0, 136, 100, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 5, 
						c->plr->world_x + 1, c->plr->world_y + 1, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_PLAY_EMOTE, 3, 799, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 7, 
						c->plr->world_x + 2, c->plr->world_y + 2, 0, 0, 0, 0);
			action_queue_start(c);
		}
		else if (strcmp(string, "aqtest3") == 0)
		{
			action_queue_clear(c);
			action_queue_add_action(c, AQ_SET_WALKING_EMOTE, 0, 762, 0, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_MOVE_TO_POSITION, 0, 
						c->plr->world_x, c->plr->world_y - 7, 0, 0, 0, 0);
			action_queue_add_action(c, AQ_IDLE, 7, 
						c->plr->world_x, c->plr->world_y - 7, 0, 0, 0, 0);
			action_queue_start(c);
		}
		else if (strcmp(string, "wrx") == 0)
		{
			snprintf(return_msg, 100, "Region coords X: %d Y: %d\n", 
					get_region_coord(c->plr->world_x), 
					get_region_coord(c->plr->world_y));
			packet_send_chatbox_message(c, return_msg);
		}
		/*
		else if (strcmp(string, "hans") == 0)
		{
			int num = 1;
			printf("x: %d y: %d\n", NPC_SPAWNS[num]->world_x, 
				   NPC_SPAWNS[num]->world_y);
			set_npc_combat_target(NPC_SPAWNS[num], -1, c->plr->index);
		}
		*/
		else if (strcmp(string, "freeze") == 0)
		{
			packet_send_chatbox_message(c, "Frozen!");
			freeze_player(c, EFFECT_FREEZE_STUN, 5);
		}
		else if (strncmp(string, "proj ", 5) == 0)
		{
			int gfx_id, proj_speed, delay;
			int ret = sscanf(string, "proj %d %d %d", &gfx_id, &proj_speed, 
							 &delay);
			if (ret == 1)
			{
				player_play_animation(c, 426);
				player_draw_gfx(c, 19);
				packet_create_projectile(c, c->plr->world_x, c->plr->world_y,
								  c->plr->world_x + 5,
								  c->plr->world_y,
								  50, 60, gfx_id, 43, 31, 0, 45, 10);
			}
			else if (ret == 2)
			{
				player_play_animation(c, 426);
				player_draw_gfx(c, 19);
				packet_create_projectile(c, c->plr->world_x, c->plr->world_y,
								  c->plr->world_x + 5,
								  c->plr->world_y,
								  50, proj_speed, gfx_id, 43, 31, 0, 45, 10);
			}
			else if (ret == 3)
			{
				player_play_animation(c, 426);
				player_draw_gfx(c, 19);
				packet_create_projectile(c, c->plr->world_x, c->plr->world_y,
								  c->plr->world_x + 5,
								  c->plr->world_y,
								  50, proj_speed, gfx_id, 43, 31, 0, delay, 10);
			}
		}
		else if (strncmp(string, "projm ", 6) == 0)
		{
			int proj_speed, delay;
			int ret = sscanf(string, "projm %d %d", &proj_speed, &delay);
			if (ret == 1)
			{
				player_play_animation(c, 711);
				player_draw_gfx(c, 90);
				packet_create_projectile(c, c->plr->world_x, c->plr->world_y,
								  NPC_SPAWNS[1884]->world_x, 
								  NPC_SPAWNS[1884]->world_y,
								  50, proj_speed, 91, 43, 31, 0, 55, 10);
			}
			else if (ret == 2)
			{
				player_play_animation(c, 711);
				player_draw_gfx(c, 90);
				packet_create_projectile(c, c->plr->world_x, c->plr->world_y,
								  NPC_SPAWNS[1884]->world_x, 
								  NPC_SPAWNS[1884]->world_y,
								  50, proj_speed, 91, 43, 31, 0, delay, 10);
			}
		}
		else if (strcmp(string, "ca") == 0)
		{
			/* ca - change appearance.  */
			c->plr->can_packet_player_change_appearance = 1;
			packet_show_interface(c, 3559);
		}
		else if (strcmp(string, "bank") == 0)
		{
			send_bank_menu(c);
		}
		else if (strncmp(string, "shop", 4) == 0)
		{
			int shop_idx;
			int ret = sscanf(string, "shop %d", &shop_idx);

			if (ret == 1)
			{
				if (shop_idx < 0 || shop_idx > NUM_SHOPS)
				{

				}
				else
				{
					printf("shop %d\n", shop_idx);
					send_shop_menu(c, shop_idx);
				}

			}
			else
			{
				send_shop_menu(c, 0);
			}
		}
		else if (strcmp(string, "shop") == 0)
		{
			send_shop_menu(c, 0);
		}
		else if (strcmp(string, "dbank") == 0)
		{
			send_bank_deposit_box_menu(c);
		}
		else if (strcmp(string, "rbank") == 0)
		{
			packet_reload_items_in_bank_interface(c);
			packet_reload_items_in_bank_sidebar_interface(c);
		}
		else if (strncmp(string, "intrinv ", 8) == 0)
		{
			int intr_id, inv_intr_id;
			int ret = sscanf(string, "intrinv %d %d", &intr_id, &inv_intr_id);
			if (ret == 2)
			{
				printf("Showing interface: %d Sidebar: %d\n", intr_id, 
					inv_intr_id);
				packet_show_interface_inventory(c, intr_id, inv_intr_id);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "ih ", 3) == 0)
		{
			int npc_id, slot;
			int ret = sscanf(string, "ih %d %d", &npc_id, &slot);
			if (ret == 2)
			{
				packet_set_interface_npc_head(c, npc_id, slot);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intrm ", 6) == 0)
		{
			int interface_id;
			int ret = sscanf(string, "intrm %d", &interface_id);
			if (ret == 1)
			{
				packet_set_item_in_interface_model(c, interface_id, 200, 48);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intri ", 6) == 0)
		{
			int interface_id, slot;
			int ret = sscanf(string, "intri %d %d", &interface_id, &slot);
			if (ret == 2)
			{
				packet_set_item_in_interface(c, interface_id, slot, 1336, 1);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intrt_l ", 8) == 0)
		{
			int interface_id_from, interface_id_to;
			int ret = sscanf(string, "intrt_l %d %d", &interface_id_from, 
							 &interface_id_to);
			if (ret == 2)
			{
				int x = 0;
				char text[100];

				for (x = interface_id_from; x < interface_id_to; x++)
				{
					snprintf(text, 99, "%d", x);
					packet_set_interface_text(c, text, x);
				}
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intrt ", 6) == 0)
		{
			char text[100];
			int interface_id;
			int ret = sscanf(string, "intrt %d %99s", &interface_id, text);
			if (ret == 1)
			{
				snprintf(text, 100, "%d", interface_id);
				packet_set_interface_text(c, text, interface_id);
			}
			else if (ret == 2)
			{
				packet_set_interface_text(c, text, interface_id);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intrc ", 6) == 0)
		{
			int interface_id;
			int ret = sscanf(string, "intrc %d", &interface_id);
			if (ret == 1)
			{
				packet_show_interface_chatbox(c, interface_id);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "intropen ", 9) == 0)
		{
			snprintf(return_msg, 100, "Interface open: %d sub: %d", 
				c->plr->interface_open, c->plr->interface_open_sub);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strncmp(string, "intr ", 5) == 0)
		{
			int interface_id;
			int ret = sscanf(string, "intr %d", &interface_id);
			if (ret == 1)
			{
				packet_show_interface(c, interface_id);
				snprintf(return_msg, 100, "Showing interface: %d", 
						 interface_id);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				packet_close_all_open_interfaces(c);
			}
		}
		else if (strncmp(string, "npc_move ", 9) == 0)
		{
			int npc_index, w_x, w_y;
			int ret = sscanf(string, "npc_move %d %d %d", 
							 &npc_index, &w_x, &w_y);
			
			if (ret == 3)
			{
				if (NPC_SPAWNS[npc_index] != NULL)
				{
					NPC_SPAWNS[npc_index]->walk_to_x 
						= (NPC_SPAWNS[npc_index]->world_x + w_x);
					NPC_SPAWNS[npc_index]->walk_to_y 
						= (NPC_SPAWNS[npc_index]->world_y + w_y);
					NPC_SPAWNS[npc_index]->update_required = 1;
					get_npc_movement_direction(NPC_SPAWNS[npc_index]);
					move_npc_to_next_point(NPC_SPAWNS[npc_index]);
				}
			}
		}
		else if (strncmp(string, "npc_turn ", 9) == 0)
		{
			int npc_index, w_x, w_y;
			int ret = sscanf(string, "npc_turn %d %d %d", 
							 &npc_index, &w_x, &w_y);
			
			if (ret == 3)
			{
				if (NPC_SPAWNS[npc_index] != NULL)
				{
					turn_npc_to_coord(NPC_SPAWNS[npc_index], w_x, w_y);
				}
			}
		}
		else if (strncmp(string, "npc_can ", 8) == 0)
		{
			int npc_index;
			int x_diff = 0, y_diff = 0;
			int ret = sscanf(string, "npc_can %d %d %d", &npc_index, &x_diff, 
							 &y_diff);
			
			if (ret == 3)
			{
				if (NPC_SPAWNS[npc_index] != NULL)
				{
					int world_x = NPC_SPAWNS[npc_index]->world_x;
					int world_y = NPC_SPAWNS[npc_index]->world_y;
					int world_z = NPC_SPAWNS[npc_index]->world_z;
					int npc_size = NPCS[NPC_SPAWNS[npc_index]->npc_id]->size;
					int rv = 0;

					rv = can_walk_in_direction_big(world_x, world_y, world_z, 
											  (world_x + x_diff), 
											  (world_y + y_diff), 
											  1, npc_size);
					
					snprintf(return_msg, 100, "RV: %d xdiff: %d ydiff: %d", rv, 
							 x_diff, y_diff);
					packet_send_chatbox_message(c, return_msg);
				}
			}
		}
		else if (strncmp(string, "npc_data ", 9) == 0)
		{
			int npc_index;
			int ret = sscanf(string, "npc_data %d", &npc_index);
			
			if (ret == 1)
			{
				if (NPC_SPAWNS[npc_index] != NULL)
				{
					printf("x: %d y: %d size: %d\n", 
						   NPC_SPAWNS[npc_index]->world_x,
						   NPC_SPAWNS[npc_index]->world_y,
						   NPCS[NPC_SPAWNS[npc_index]->npc_id]->size);
				}
			}
		}
		else if (strncmp(string, "npc_trans ", 10) == 0)
		{
			int npc_index;
			int ret = sscanf(string, "npc_trans %d", &npc_index);

			if (ret == 1)
			{
				if (NPC_SPAWNS[npc_index] != NULL)
				{
					NPC_SPAWNS[npc_index]->npc_id = 90;
					NPC_SPAWNS[npc_index]->transform_timer = 10;
					NPC_SPAWNS[npc_index]->update_masks[APPEARANCE_UPDATE] = 1;
					NPC_SPAWNS[npc_index]->update_required = 1;
				}
			}
		}
		else if (strcmp(string, "npc_scan") == 0)
		{
			int loop = 0;
			for (loop = 0; loop < c->plr->local_npcs_num; loop++)
			{
				printf("idx: %d\n", c->plr->local_npcs[loop]->index);
			}
		}
		else if (strncmp(string, "spawn ", 6) == 0)
		{
			int npc_id, walks;
			int ret = sscanf(string, "spawn %d %d", &npc_id, &walks);

			if (ret > 0)
			{
				if (npc_id >= 0 && npc_id <= PROTOCOL_MAX_NPCS)
				{
					if (ret == 2)
					{
						spawn_new_npc(npc_id, c->plr->world_x, c->plr->world_y, 
									  c->plr->world_z, walks, 0, 1);
						printf("spawning npc %d at x: %d y: %d walks: %d\n", 
							   npc_id, c->plr->world_x, c->plr->world_y, 
							   walks);
					}
					else if (ret == 1)
					{
						spawn_new_npc(npc_id, c->plr->world_x, c->plr->world_y, 
									  c->plr->world_z, 1, 0, 1);
						printf("spawning npc %d at x: %d y: %d walks: %d\n", 
							   npc_id, c->plr->world_x, c->plr->world_y, 1);
					}
				}
				else
				{
					snprintf(return_msg, 100,
							"Error! NPC id must be less than %d!", 
							PROTOCOL_MAX_NPCS);
					packet_send_chatbox_message(c, return_msg);
				}
			}
		}
		else if (strcmp(string, "kill") == 0)
		{
			c->plr->level[HITPOINTS_SKILL] = 0;
			packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
		}
		else if (strcmp(string, "hard_crash") == 0)
		{
			player_draw_gfx(c, 10);
		}
		else if (strcmp(string, "crash") == 0)
		{
			create_frame(c, 99);
			write_byte(c, 99);
			write_byte(c, 99);
		}
		/* @todo Change this to send to the player who called it, maybe show
		 * on an interface?  */
		else if (strcmp(string, "who") == 0)
		{
			int x;

			for (x = 0; x < MAX_PLAYERS; x++)
			{
				if (CONNECTED_CLIENTS[x] != NULL)
				{
					printf("Name: %s\n", CONNECTED_CLIENTS[x]->plr->name);
				}
			}
		}
		else if (strcmp(string, "shutdown") == 0)
		{
			printf("Server shutdown command ran by: %s!\n", c->plr->name);
			shutdown_server(0);
		}
		else if (strncmp(string, "lvl ", 4) == 0)
		{
			int height = 0;
			int ret = sscanf(string, "lvl %d", &height);

			if (ret == 1)
			{
				if (height >= 0 && height <= 3)
				{
					c->plr->tele_z = height;
					c->plr->tele_x = c->plr->world_x;
					c->plr->tele_y = c->plr->world_y;
				}
				packet_send_chatbox_message(c, "Changing level.");
			}
			else
			{
				packet_send_chatbox_message(c, "Usage: ::lvl [world_z]");
			}
		}
		else if (strncmp(string, "400 ", 4) == 0)
		{
			int start_tick = 0, end_tick = 0, direction = 0;
			int ret = sscanf(string, "400 %d %d %d", &start_tick, &end_tick, 
							 &direction);

			if (ret == 3)
			{
				c->plr->update_masks[ANIMATION_REQ] = 1;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;

				set_phase_movement(c, c->plr->world_x, (c->plr->world_y - 3), 
								   c->plr->world_x, 
								   c->plr->world_y, 
								   start_tick, end_tick,
								   direction);
				packet_send_chatbox_message(c, "400 test...");
			}
			else
			{
				printf("wrong!\n");
			}
		}
		else if (strncmp(string, "icon ", 5) == 0)
		{
			int icon_id = 0;
			int ret = sscanf(string, "icon %d", &icon_id);

			if (ret == 1)
			{
				packet_send_chatbox_message(c, "Icon set.");
				c->plr->prayer_icon = icon_id;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
			}
		}
		else if (strncmp(string, "configb ", 8) == 0)
		{
			int id = 0, amount = 0;
			int ret = sscanf(string, "configb %d %d", &id, &amount);

			if (ret == 2)
			{
				packet_set_client_config_value_int(c, id, amount);
				snprintf(return_msg, 100, "Setting client config big %d to %d.", 
						 id, amount);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				packet_send_chatbox_message(c, "configb: Sets your client's configuration "
							 "option to a specified value.");
				packet_send_chatbox_message(c, "Usage: ::configb [config_id] [new value]");
			}
		}
		else if (strncmp(string, "configl ", 8) == 0)
		{
			int start = 0, stop = 0, amount = 0, x = 0;
			int ret = sscanf(string, "configl %d %d %d", &start, &stop, 
							 &amount);

			if (ret == 3)
			{
				for (x = start; x < stop; x++)
				{
					packet_set_client_config_value(c, x, amount);
				}
				snprintf(return_msg, 100, "Setting client configs, loop %d to "
						 "%d.", start, stop);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				packet_send_chatbox_message(c, "Usage: ::configl [start config id] "
							 "[end config id] [new value]");
			}
		}
		else if (strncmp(string, "config ", 7) == 0)
		{
			int id = 0, amount = 0;
			int ret = sscanf(string, "config %d %d", &id, &amount);

			if (ret == 2)
			{
				packet_set_client_config_value(c, id, amount);
				snprintf(return_msg, 100, "Setting client config %d to %d.", 
						 id, amount);
				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				packet_send_chatbox_message(c, "config: Sets your client's configuration "
							 "option to a specified value.");
				packet_send_chatbox_message(c, "Usage: ::config [config_id] [new value]");
			}
		}
		else if (strcmp(string, "packets") == 0)
		{
			print_player_last_packets(c);
		}
		else if (strcmp(string, "statup") == 0)
		{
			int x;
			for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
			{
				c->plr->level[x] += 5;
				packet_send_updated_skill_stats(c, x);
			}
		}
		else if (strcmp(string, "clear_temp") == 0)
		{
			int x;
			for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
			{
				c->plr->temp_bonuses[x] = 0;
				packet_send_updated_skill_stats(c, x);
			}
		}
		else if (strcmp(string, "hansd") == 0)
		{
			snprintf(return_msg, 100, "Hans facing dir: %d\n", 
				NPC_SPAWNS[0]->last_walked_direction);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strcmp(string, "myd") == 0)
		{
			snprintf(return_msg, 100, "Player facing dir: %d\n", 
				c->plr->last_walked_direction);
			packet_send_chatbox_message(c, return_msg);
		}
		else if (strncmp(string, "tune ", 5) == 0)
		{
			int config = 0;
			int ret = sscanf(string, "tune %d", &config);
	
			if (ret == 1)
			{
				if (c->plr->action_store_one != config)
				{
					printf("new config!\n");
					c->plr->action_store_one = config;
					c->plr->action_store_two = 0;
				}
			}
			else
			{
				if (c->plr->action_store_one != 0)
				{
					unsigned int res = 1 << c->plr->action_store_two;
					snprintf(return_msg, 100, "Config %d set to %d.", 
							 c->plr->action_store_one, res);
					packet_send_chatbox_message(c, return_msg);
					packet_set_client_config_value_int(c, c->plr->action_store_one, res);
					(c->plr->action_store_two)++;
				}
			}

		}
		else if (strncmp(string, "instobjr ", 9) == 0)
		{
			int index = 0, clear_client = 0;
			int ret = sscanf(string, "instobjr %d %d", &index, &clear_client);

			if (ret == 1)
			{
				remove_inst_object(c, index, 0);
			}
			else if (ret == 2)
			{
				remove_inst_object(c, index, clear_client);
			}
			else
			{
				packet_send_chatbox_message(c, "instobjr: Removes an instanced object from "
							 "the server.");
				packet_send_chatbox_message(c, "Usage: ::instobjr obj_index [clear_client]");
			}
		}
		else if (strncmp(string, "instobjt ", 9) == 0)
		{
			int index = 0, to_id = 0, timer = 10;
			int ret = sscanf(string, "instobjt %d %d %d", &index, &to_id, 
							 &timer);

			if (to_id < 0 || to_id >= PROTOCOL_MAX_OBJECTS)
			{
				snprintf(return_msg, 100,
						"Error! Object id must be less than %d!", 
						PROTOCOL_MAX_OBJECTS);
				packet_send_chatbox_message(c, return_msg);
			}

			if (ret == 2)
			{
				handle_transforming_inst_object(c, index, to_id, 10);
				c->plr->inst_objects_on_ground[index] = 0;
				spawn_inst_object_on_ground(c, index);
			}
			else if (ret == 3)
			{
				handle_transforming_inst_object(c, index, to_id, timer);
				c->plr->inst_objects_on_ground[index] = 0;
				spawn_inst_object_on_ground(c, index);
			}
			else
			{
				packet_send_chatbox_message(c, "instobjt: Tells an instanced object to "
							 "transform to another one for a specified time.");
				packet_send_chatbox_message(c, "Usage: ::instobjt obj_index to_id [time]");
			}
		}
		else if (strncmp(string, "instobj ", 8) == 0)
		{
			int x;
			int object_id, object_face;
			int ret = sscanf(string, "instobj %d %d", &object_id, 
							 &object_face);

			if (object_id < 0 || object_id >= PROTOCOL_MAX_OBJECTS)
			{
				snprintf(return_msg, 100,
						"Error! Object id must be less than %d!", 
						PROTOCOL_MAX_OBJECTS);
				packet_send_chatbox_message(c, return_msg);
			}

			if (ret == 1)
			{
				x = create_inst_object(c, object_id, 10, c->plr->world_x, 
											c->plr->world_y, 0, 0, 1000);
				spawn_inst_object_on_ground(c, x);
				printf("inst obj at index: %d\n", x);
			}
			else if (ret == 2)
			{
				x = create_inst_object(c, object_id, 10, c->plr->world_x, 
											c->plr->world_y, 0, object_face, 
											1000);
				spawn_inst_object_on_ground(c, x);
				printf("inst obj at index: %d\n", x);
			}
			else
			{
				packet_send_chatbox_message(c, "instobj: Spawns an object at your position "
							 " that only you can see.");
				packet_send_chatbox_message(c, "Usage: ::instobj obj_id [obj_face]");
			}
		}
		else if (strncmp(string, "ospawn ", 7) == 0)
		{
			int x;
			int object_id = 0, object_face = 0, object_type = 0;
			int ret = sscanf(string, "ospawn %d %d %d", &object_id, 
							 &object_face, &object_type);

			if (object_id < 0 || object_id >= PROTOCOL_MAX_OBJECTS)
			{
				snprintf(return_msg, 100,
						"Error! Object id must be less than %d!", 
						PROTOCOL_MAX_OBJECTS);
				packet_send_chatbox_message(c, return_msg);
			}

			if (ret == 1)
			{
				x = add_to_object_list(object_id, 10, c->plr->world_x, 
									   c->plr->world_y, c->plr->world_z, 0, 
									   1000, 1);
				if (x != -1)
				{
					spawn_object_globally(x);
					printf("ospawn %d facing %d\n", object_id, object_face);	
				}
			}
			else if (ret == 2)
			{
				x = add_to_object_list(object_id, 10, c->plr->world_x, 
									   c->plr->world_y, c->plr->world_z, 
									   object_face, 10, 1);
				if (x != -1)
				{
					spawn_object_globally(x);
					printf("ospawn %d facing %d\n", object_id, object_face);	
				}
			}
			else if (ret == 3)
			{
				x = add_to_object_list(object_id, object_type, c->plr->world_x, 
									   c->plr->world_y, c->plr->world_z, 
									   object_face, 1000, 1);
				if (x != -1)
				{
					spawn_object_globally(x);
					printf("ospawn %d facing %d type %d\n", object_id, 
						   object_face, object_type);	
				}
			}
			else
			{
				packet_send_chatbox_message(c, "ospawn: Spawns an object at your position.");
				packet_send_chatbox_message(c, "Usage: ::ospawn obj_id [obj_face] "
							 "[obj_type]");
			}
		}
	}
}
