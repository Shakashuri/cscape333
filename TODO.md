Todo List - Non Doxygen
=======================

## GENERAL:

* Clean up code, remove lots of globals.
* Create unit tests for functions in the server.

## SERVER:

* Implement friend/ignore list.
* Check if multiple of the same packets in one tick should be processed or 
	if they should be discarded.
	* Note: Some have a limit to how many times in one tick they can be used.
	* Should this be implemented? Equiping items is the only one known for
	  sure.
* Figure out NPC height, used for proper projectile impact height. This is
	not stored in the cache.
* Login process uses another layer of encryption that is disabled on most
	de-obfuscated clients, RSA I think. Implement.
* Log for possible cheating when a client sends an invalid value for a packet.
* Investigate if changing the storage lists for NPC_SPAWNS and ITEM_DROPS to 
	be more like collision map, where it is all stored based on the region id
	it is located in, would be more efficient.
* Add new functions to create a permanent npc or item spawn in the game world,
	and then save the new list to the file. Would make it easier to add new
	NPCs to the world without manual editing.
	* Low priority since the wiki dump, may not really be needed.
* Stop following NPC's when they don't have an action you can do with them.
* Get rid of the seperation between item stacking and non-stacking functions.
	Should just be one set of functions that work for all items.
* Need to remove the need for the region file to have the music ids, I think
	how it was implemented in order to have the checking for non-music regions
	may make it more difficult to initially setup the server.
* Add in instanced area support. Have a config value for the player if they
	are instanced or not. If so, don't show them to other players and don't
	show other players as well. Also, add a flag for NPCs that make it so
	that they only appear to specific players.

## COMBAT:

* Combat formulas need a re-work, combat isn't 100% accurate. Max damage
    calculations look like they're fine, but accuracy calculations are not.
* De-couple combat functions.
* Implement crystal items.
* Add support for PvP combat.
	* Ensure that the movement for PvP is not in the combat function, 
		uncoupled. Also make sure that pursuit is lagged like NPCs.
* NPCs prevent being attacked by a different player, if the last player that
	attacked them hasn't in the last 8 ticks. Need to implement.
* Add large npc TAKEN_MASK updating.
* NPCs don't poison players currently if they should, double check this.
* Ensure that when a player clicks on a large NPC, they do not walk through one
	of their model squares. (Might already be done, double check this)
* Also, some npcs don't allow the player to walk through them.
	Investigate.
* Possibly move NPC AI functions to their own file.
* Spell casting emotes for bind is a placeholder, also for the wave spells.
* Add special attacks.
	* Dragon Spear attack animation may be wrong.
* Find out proper gfxs for obsidian ring weapon.
* Arrow draw and projectile gfx for bolt racks are placeholders.

## SKILLS:

* General:
	* Implement skill info interfaces when their icons are clicked on.
	* Need to add showing of new things the player can do when they level up
		a skill.
	* Move a lot of the skill_data information to a file like how the agility
		info is.
	* Majority of skill chance calculations seems to use chances out of 255
		like thieving and agility do. May need to align the other skills to
		this system.
* Cooking:
	* Research more into how gnome cooking is handled in this revision, and
		implement it.
	* Implement brewing.
* Fishing:
	* Implement fishing spots moving around.
	* Implement karambwan fishing.
    * Probably missing fishing spots in areas.
* Magic:
	* Add teleport-other spell support.
	* Projectiles for combat spells are all out of wack, need to be fixed.
		* Is in progress.
	* Spells that require an item to use may not be implemented.
	* Implement the telegrab spell.
* Prayer:
	* Add support for the Smite prayer (PvP).
* Runecrafting:
	* Implement talisman locate function.
* Firemaking:
	* Need to adjust ashes spawning to be added after the fire is removed.
	* Implement lighting lanterns.
* Agility:
	* Bug where when entering the gnome stronghold pipe, the player is rendered
		above the pipe, not inside. Have seen this on videos of the offical
		game, though. Not sure.
	* Need to implement the following courses:
		* Werewolf
		* Brimhaven
		* Entry ramp to the wilderness course
	* Shortcuts are in progress.
	* Some obstacles like stepping stones need to have a larger action range
		implemented.
* Thieving:
	* Need to track stealing from a merchant and have them call the guards
		if you've stolen from them in the past 15 minutes or so.
	* Blackjacking not implemented, wait until its quest is done?
	* Toxic gas random event not implemented yet, along with the special 
		handling for King Latha's chest.
* Herblore:
	* Finish adding in Lantadyme herb, need to add the potions to it.
	* Add support for using these potions:
		* Relicym's Balm
		* All weapon poisons
		* Anti-Fire Potion
		* Various quest only Potions
		* Add ability to make the guthix rest tea.
		* Implement the Druidic Ritual quest.
* Slayer:
	* Not implemented.
* Farming:
	* Not implemented.

## NPC RELATED:

* Investigate how dragonfire damage is handled.
* Add Rams and that penguin sheep to the sheep pens in Lumbridge.
* Most NPCs have drops now, but there are a few that still need edits.
	* These monsters don't have 100% accurate drops. The items should be
		accurate, but the chance had to be "made up".
		* Possessed Priest
		* Pollnivneach Bandit
		* Guardian of Armadyl
		* Lensa
		* Jennella
		* Lanzig
		* Freidir
		* Borrokar
		* Wall Beast
		* Zombie Random Event
		* TzHaar-Hur
		* TzHaar-Mej
		* TzHaar-Xil
		* Giant Frog
	* Might have incorrect drops, need verification later
		* Soldiers
		* Tree spirit
	* Setup drops to be a fraction, do not store floats.
* Add ranged attacks to: 
	* all thrower trolls
	* Elf warriors with crystal bow
	* 2456 - Ranged Dagganoth
* Add spells to these:
	* 907 - 914: God spells
	* 1007: Flames of Zamorak
* All dragons add dragonfire:
	* Black, Blue, Green, Red dragons have a dragonfire range of 1,
		all the metal dragons can use it ranged.
* Fix dialogue movement when multiple people talk to the same NPC, if one
	client ends dialogue, the NPC will start moving again.

## QUESTS:

* Implement tutorial island.
* Implement basic free to play quests. Still to do:
	* Black Knights' Fortress
	* Cook's Assistant
	* Demon Slayer
	* Rune Mysteries Quest
	* Doric's Quest
	* The Restless Ghost
	* Goblin Diplomacy
	* Ernest the Chicken
	* Imp Catcher
	* Pirate's Treasure
	* Prince Ali Rescue
	* Romeo & Juliet
	* Shield of Arrav
	* The Knight's Sword
	* Vampire Slayer
	* Witch's Potion
* Add quest requirement handling for some of the skills. Some things require
	quest completion before they can be made/done.

## OTHER:

* Implement stores, buying and selling items (Mostly done).
	* Implement the 3 stores that will buy at higher prices than others.
	* Implement stores that use different currencies than just gold.
	* Implement items that sell for fixed prices, like dragon items.
* Implement trading with other players.
* Decouple autodoor handling from player movement. Set it up in the loop for
	handling objects.

* Add in these loot tables.
	* Drops random easy clue scroll.
	* Drops random medium clue scroll.
	* Drops random hard clue scroll.

* Implement handling of sounds and music for this revision.
	* Inprogress, need to add clicking of the song in the music player to start
		playing, investigate how the music player handles with different
		options, the "auto/man/loop" buttons in the ui.
	* Narnode's Theme and Barking Mad look like they share the same bit to
		unlock the song 346 / 262144. Is this a client bug, or something else?
* Create a simple tool to test how many players the server can handle.
* Create a tool to send packets to the server with random data to test against
	bugs.
* Some doors do not work properly, need to enter data for them manually. Maybe
	make a tool to automate this?
* The door collision function was rewritten to be a lot cleaner, but the other
	functions could use it as well.
* Move all door related functions to object, not object_handler.
* Create an FAQ towards people not familar with private server terms. 
	Explain what they can expect from the server, what's in it, from what
	time in the game's history is it.
