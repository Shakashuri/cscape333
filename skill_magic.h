#ifndef _SKILL_MAGIC_H_
#define _SKILL_MAGIC_H_

/**
 * @brief Information for a spell in the game.
 */
struct spell
{
	/** Id of the spell.  */
	int id;
	/** Name of the spell.  */
	char name[30];
	/**
	 * What spellbook is this spell from?
	 *
	 * 0 - Modern 
	 * 1 - Ancient 
	 * 2 - Lunar (For later revisions)
	 */
	int spellbook;
	/** Base xp gained from casting the spell.  */
	float xp;
	/**
	 * ID of the item that the player has to have to cast this spell, -1 if
	 * not used.
	 */
	int item_needed;
	/** Id of the object the spell is casted on, -1 if not used.  */
	int object_needed;
	/** Emote id that is played when casting the spell.  */
	int cast_emote;
	/** Graphic that is played during cast.  */
	int cast_gfx;
	/** What height the graphic is played at.  */
	int cast_gfx_height;
	/** What graphic the projectile is.  */
	int projectile_gfx;
	/** What angle the projectile is shot at.  */
	int projectile_angle;
	/** How many ticks after casting starts should the projectile spawn?  */
	int projectile_delay;
	/** At what height the projectile start at.  */
	int projectile_start_height;
	/** At what height the projectile ends at.  */
	int projectile_end_height;
	/** Projectile's slope of travel.  */
	int projectile_slope;
	/** Graphic that plays when the projectile impacts with the target.  */
	int impact_gfx;
	/** Height that the impact graphic.  */
	int impact_gfx_height;
	/** Magic level requirement to cast the spell.  */
	int level_req;
	/** Max damage the spell can do.  */
	int max_dmg;

	/** Elemental damage done when the spell impacts.
	 * @see ELEMENTAL_DAMAGE_TYPES
	 */
	int dmg_type;
	/** How many ticks it takes to cast the spell, until the player can cast
	 * another one.  */
	int cast_ticks;
	/** If the spell poisons, what damage the poison starts at.  */
	int poison_dmg;
	/** Effect that this spell inflicts, if any.  */
	int spell_effect;
	/** Strength of the effect that this spell inflicts, if any.  */
	float spell_effect_mag;
	/** How long the effect of the spell lasts.  */
	int duration;
	/** Teleport x location, if applicable.  */
	int tele_x;
	/** Teleport y location, if applicable.  */
	int tele_y;
	/** How many runes of each type that are needed for casting.  */
	int runes_needed[14];
};

int get_spell_index(int id);
int get_jewelry_enchanted_id(int item_id);
int get_jewelry_enchant_gfx(int item_id);
int get_jewelry_enchant_emote(int item_id);
int get_charged_orb_id(int object_id);

int has_needed_runes(struct client *c, int spell_index, int delete_runes);
void set_autocast_spell(struct client *c, int spell_id);
void cast_teleport_spell(struct client *c, int spell_id);
void accept_teleother(struct client *c, int spell_id);
void cast_bones_to_bananas(struct client *c, int spell_id);
void cast_superheat_on_item(struct client *c, int spell_id, int item_id);
void cast_charge_orb_on_obelisk(struct client *c, int obj_id, int spell_id);

void handle_magic_spell_on_item(struct client *c, int spell_id, int item_id,
								int item_slot);
#endif
