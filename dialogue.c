/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file dialogue.c
 * @brief Contains functions relating to dialogue windows, level up menus,
 * some skill action menus.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"

#include "client.h"
#include "interface.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
#include "skill_cooking.h"
#include "skill_crafting.h"
#include "skill_fletching.h"
#include "skill_smithing.h"

/* CHAT RELATED PACKETS.  */

static const int SKILL_LEVELUP_INTERFACE[PROTOCOL_NUM_SKILLS] 
				 = {6247, 6253, 6206, 6216, 4443, 6242, 6211, 6226, 4272, 6231, 
					6258, 4282, 6263, 6221, 4416, 6237, 4277, 4261, 12122, 
					11864, 4267};


/* Sends npc dialogue messages.  */
static void send_npc_chat1(struct client *c, char string[], int npc_id, int head_anim)
{
	set_interface_model_anim(c, 4883, head_anim);
	packet_set_interface_text(c, NPCS[npc_id]->name, 4884);
	packet_set_interface_text(c, string, 4885);
	packet_set_interface_npc_head(c, npc_id, 4883);
	packet_show_interface_chatbox(c, 4882);
}

static void send_npc_chat2(struct client *c, char string[], char string1[], 
					int npc_id, int head_anim)
{
	set_interface_model_anim(c, 4888, head_anim);
	packet_set_interface_text(c, NPCS[npc_id]->name, 4889);
	packet_set_interface_text(c, string, 4890);
	packet_set_interface_text(c, string1, 4891);
	packet_set_interface_npc_head(c, npc_id, 4888);
	packet_show_interface_chatbox(c, 4887);
}

static void send_npc_chat3(struct client *c, char string[], char string1[],
					char string2[], int npc_id, int head_anim)
{
	set_interface_model_anim(c, 4894, head_anim);
	packet_set_interface_text(c, NPCS[npc_id]->name, 4895);
	packet_set_interface_text(c, string, 4896);
	packet_set_interface_text(c, string1, 4897);
	packet_set_interface_text(c, string2, 4898);
	packet_set_interface_npc_head(c, npc_id, 4894);
	packet_show_interface_chatbox(c, 4893);
}

static void send_npc_chat4(struct client *c, char string[], char string1[],
					char string2[], char string3[], int npc_id, int head_anim)
{
	set_interface_model_anim(c, 4901, head_anim);
	packet_set_interface_text(c, NPCS[npc_id]->name, 4902);
	packet_set_interface_text(c, string, 4903);
	packet_set_interface_text(c, string1, 4904);
	packet_set_interface_text(c, string2, 4905);
	packet_set_interface_text(c, string3, 4906);
	packet_set_interface_npc_head(c, npc_id, 4901);
	packet_show_interface_chatbox(c, 4900);
}

/**
 * Sends a chat interface to a client that shows a npc and what they are
 * saying.
 * @param *c Client instance to show chat to.
 * @param string, string1, string2, string3 Chat strings to send. Size of
 * interface is determined by how many non-null strings passed.
 * @param npc_id NPC that shows up in the chat interface.
 * @param head_anim What animation id the NPC head uses, what emotion.
 */
void send_npc_chat(struct client *c, char string[], char string1[], 
				   char string2[], char string3[], int npc_id, int head_anim)
{
	if (string3[0] == '\0')
	{
		if (string2[0] == '\0')
		{
			if (string1[0] == '\0')
			{
				if (string[0] == '\0')
				{
					printf("INVAID CHAT!\n");
					return;
				}
				else
					send_npc_chat1(c, string, npc_id, head_anim);
			}
			else
				send_npc_chat2(c, string, string1, npc_id, head_anim);
		}
		else
			send_npc_chat3(c, string, string1, string2, npc_id, head_anim);
	}
	else
		send_npc_chat4(c, string, string1, string2, string3, npc_id, head_anim);
}

/**
 * Sends a npc chat window with the npc_id's head, playing the specific
 * head_anim, and saying string.
 * @param *c Client instance.
 * @param string String the NPC says in chat.
 * @param npc_id ID of the NPC speaking.
 * @param head_anim Animation the head uses, see HEAD_ANIMS
 */
void send_npc_chat_simple(struct client *c, char string[], int npc_id,
						  int head_anim)
{
	int line_max = 48;
	int len = strlen(string);
	int x = 0, y = 0;
	char out_string[4][200] = {{'\0'}};
	int split_positions[4] = {0};
	int used_string[4] = {0};
	int counted_chars = 0;
	int at_string = 0;
	int starting_pos = 0;
	int out_string_pos = 0;

	printf("String len: %d\n", len);

	if (len < line_max)
	{
		send_npc_chat(c, string, "", "", "", npc_id, head_anim);
		return;
	}
	else
	{
		for (x = starting_pos; x < len; x++, counted_chars++)
		{
			if (string[x] == ' ')
			{
				if (counted_chars < line_max)
				{
					split_positions[at_string] = x;
				}
				else
				{
					printf("next string\n");
					counted_chars = 0;

					if (len - x < line_max)
					{
						break;
					}
					at_string++;
				}
			}
		}
	}

	for (x = 0; x < 4; x++)
	{
		printf("Split %d: %d\n", x, split_positions[x]);
	}

	for (y = 0, at_string = 0, out_string_pos = 0; (y < len && at_string < 4); y++)
	{
		out_string[at_string][out_string_pos++] = string[y];

		if (used_string[at_string] == 0)
		{
			used_string[at_string] = 1;
		}
	
		if (y == split_positions[at_string])
		{
			out_string_pos = 0;
			at_string++;
		}
	}

	send_npc_chat(c, out_string[0], out_string[1], out_string[2], 
				  out_string[3], npc_id, head_anim);

	printf("1 %d: %s\n", used_string[0], out_string[0]);
	printf("2 %d: %s\n", used_string[1], out_string[1]);
	printf("3 %d: %s\n", used_string[2], out_string[2]);
	printf("4 %d: %s\n", used_string[3], out_string[3]);
}

/* Sends dialogue messages said by the player.  */
static void send_player_chat1(struct client *c, char string[], int head_anim)
{
	set_interface_model_anim(c, 969, head_anim);
	packet_set_interface_text(c, c->plr->name, 970);
	packet_set_interface_text(c, string, 971);
	packet_set_interface_player_head(c, 969);
	packet_show_interface_chatbox(c, 968);
}

static void send_player_chat2(struct client *c, char string[], char string1[], 
					   int head_anim)
{
	set_interface_model_anim(c, 974, head_anim);
	packet_set_interface_text(c, c->plr->name, 975);
	packet_set_interface_text(c, string, 976);
	packet_set_interface_text(c, string1, 977);
	packet_set_interface_player_head(c, 974);
	packet_show_interface_chatbox(c, 973);
}

static void send_player_chat3(struct client *c, char string[], char string1[], 
					   char string2[], int head_anim)
{
	set_interface_model_anim(c, 980, head_anim);
	packet_set_interface_text(c, c->plr->name, 981);
	packet_set_interface_text(c, string, 982);
	packet_set_interface_text(c, string1, 983);
	packet_set_interface_text(c, string2, 984);
	packet_set_interface_player_head(c, 980);
	packet_show_interface_chatbox(c, 979);
}

static void send_player_chat4(struct client *c, char string[], char string1[], 
					   char string2[], char string3[], int head_anim)
{
	set_interface_model_anim(c, 987, head_anim);
	packet_set_interface_text(c, c->plr->name, 988);
	packet_set_interface_text(c, string, 989);
	packet_set_interface_text(c, string1, 990);
	packet_set_interface_text(c, string2, 991);
	packet_set_interface_text(c, string3, 992);
	packet_set_interface_player_head(c, 987);
	packet_show_interface_chatbox(c, 986);
}

/**
 * Sends a chat interface to a client that shows the player and what they are
 * saying.
 * @param *c Client instance to show chat to.
 * @param string, string1, string2, string3 Chat strings to send. Size of
 * interface is determined by how many non-null strings passed.
 * @param head_anim What animation id the player's head uses, what emotion.
 */
void send_player_chat(struct client *c, char string[], char string1[], 
					  char string2[], char string3[], int head_anim)
{
	if (string3[0] == '\0')
	{
		if (string2[0] == '\0')
		{
			if (string1[0] == '\0')
			{
				if (string[0] == '\0')
				{
					printf("INVAID CHAT!\n");
					return;
				}
				else
					send_player_chat1(c, string, head_anim);
			}
			else
				send_player_chat2(c, string, string1, head_anim);
		}
		else
			send_player_chat3(c, string, string1, string2, head_anim);
	}
	else
		send_player_chat4(c, string, string1, string2, string3, head_anim);
}

/* Sends the player a choice between some options.  */
static void send_chat_choice2(struct client *c, char string[], char string1[])
{
	packet_set_interface_text(c, "Select an option", 2460);
	packet_set_interface_text(c, string, 2461);
	packet_set_interface_text(c, string1, 2462);
	packet_show_interface_chatbox(c, 2459);
}

static void send_chat_choice3(struct client *c, char string[], char string1[], 
					   char string2[])
{
	packet_set_interface_text(c, "Select an option", 2470);
	packet_set_interface_text(c, string, 2471);
	packet_set_interface_text(c, string1, 2472);
	packet_set_interface_text(c, string2, 2473);
	packet_show_interface_chatbox(c, 2469);
}

static void send_chat_choice4(struct client *c, char string[], char string1[], 
					   char string2[], char string3[])
{
	packet_set_interface_text(c, "Select an option", 2481);
	packet_set_interface_text(c, string, 2482);
	packet_set_interface_text(c, string1, 2483);
	packet_set_interface_text(c, string2, 2484);
	packet_set_interface_text(c, string3, 2485);
	packet_show_interface_chatbox(c, 2480);
}

static void send_chat_choice5(struct client *c, char string[], char string1[], 
					   char string2[], char string3[], char string4[])
{
	packet_set_interface_text(c, "Select an option", 2493);
	packet_set_interface_text(c, string, 2494);
	packet_set_interface_text(c, string1, 2495);
	packet_set_interface_text(c, string2, 2496);
	packet_set_interface_text(c, string3, 2497);
	packet_set_interface_text(c, string4, 2498);
	packet_show_interface_chatbox(c, 2492);
}

/**
 * Sends a chat interface to a client that shows a number of clickable options.
 * @param *c Client instance to show the choices to.
 * @param string, string1, string2, string3, string4 Choice strings to send.
 * Size of interface is determined by how many non-null strings passed.
 */
void send_chat_choice(struct client *c, char string[], char string1[], 
					  char string2[], char string3[], char string4[])
{
	if (string4[0] == '\0')
	{
		if (string3[0] == '\0')
		{
			if (string2[0] == '\0')
			{
				if (string1[0] == '\0')
				{
					printf("INVAID CHAT!\n");
					return;
				}
				else
					send_chat_choice2(c, string, string1);
			}
			else
				send_chat_choice3(c, string, string1, string2);
		}
		else
			send_chat_choice4(c, string, string1, string2, string3);
	}
	else
		send_chat_choice5(c, string, string1, string2, string3, string4);
}

/* Sends a statment to the player's chatbox.  */
static void send_chat_statement1(struct client *c, char string[])
{
	packet_set_interface_text(c, string, 357);
	packet_set_interface_text(c, "Click here to continue", 358);
	packet_show_interface_chatbox(c, 356);
}

static void send_chat_statement2(struct client *c, char string[], 
								 char string1[])
{
	packet_set_interface_text(c, string, 360);
	packet_set_interface_text(c, string1, 361);
	packet_set_interface_text(c, "Click here to continue", 362);
	packet_show_interface_chatbox(c, 359);
}

static void send_chat_statement3(struct client *c, char string[], 
								 char string1[], char string2[])
{
	packet_set_interface_text(c, string, 364);
	packet_set_interface_text(c, string1, 365);
	packet_set_interface_text(c, string2, 366);
	packet_set_interface_text(c, "Click here to continue", 367);
	packet_show_interface_chatbox(c, 363);
}

static void send_chat_statement4(struct client *c, char string[], 
								 char string1[], char string2[], char string3[])
{
	packet_set_interface_text(c, string, 369);
	packet_set_interface_text(c, string1, 370);
	packet_set_interface_text(c, string2, 371);
	packet_set_interface_text(c, string3, 372);
	packet_set_interface_text(c, "Click here to continue", 373);
	packet_show_interface_chatbox(c, 368);
}

static void send_chat_statement5(struct client *c, char string[], 
								 char string1[], char string2[], char string3[], 
								 char string4[])
{
	packet_set_interface_text(c, string, 375);
	packet_set_interface_text(c, string1, 376);
	packet_set_interface_text(c, string2, 377);
	packet_set_interface_text(c, string3, 378);
	packet_set_interface_text(c, string4, 379);
	packet_set_interface_text(c, "Click here to continue", 380);
	packet_show_interface_chatbox(c, 374);
}

/**
 * Sends a interface to a client that shows text strings, but not said by
 * anyone in particular. Used for actions or narration.
 * @param *c Client instance to show the text.
 * @param string, string1, string2, string3, string4 Text strings to send.
 * Size of interface is determined by how many non-null strings passed.
 */
void send_chat_statement(struct client *c, char string[], char string1[], 
						 char string2[], char string3[], char string4[])
{
	if (string4[0] == '\0')
	{
		if (string3[0] == '\0')
		{
			if (string2[0] == '\0')
			{
				if (string1[0] == '\0')
				{
					if (string[0] == '\0')
					{
						printf("INVAID CHAT!\n");
					}
					else
						send_chat_statement1(c, string);
				}
				else
					send_chat_statement2(c, string, string1);
			}
			else
				send_chat_statement3(c, string, string1, string2);
		}
		else
			send_chat_statement4(c, string, string1, string2, string3);
	}
	else
	{
		send_chat_statement5(c, string, string1, string2, string3, string4);
	}
}

/**
 * Sends the appropriate level up menu for the skill to a player, and writes
 * the "you leveled up" string to the player's chat box as well.
 * @param *c Client to send the level up screen to.
 * @param skill_id The id of the skill that was leveled.
 */
void send_level_up_dialogue(struct client *c, int skill_id)
{
	char level_up_text[2][300];
	char color_level_text[400];

	/* Setup the level up strings.  */
	if (skill_id == ATTACK_SKILL || skill_id == AGILITY_SKILL)
	{
		snprintf(level_up_text[0], 300, 
				 "Congratulations, you just advanced an %s level.", 
				 SKILL_NAMES[skill_id]);
	}
	else
	{
		snprintf(level_up_text[0], 300, 
				 "Congratulations, you just advanced a %s level.", 
				 SKILL_NAMES[skill_id]);
	}

	snprintf(color_level_text, 400, "@blu@%s", level_up_text[0]);
	snprintf(level_up_text[1], 300, "Your %s level is now %d.", 
			 SKILL_NAMES[skill_id], get_level_for_xp(c->plr->xp[skill_id]));

	switch (skill_id)
	{
		case ATTACK_SKILL:
		case AGILITY_SKILL:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_interface_text(c, color_level_text, 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 1);
			packet_set_interface_text(c, level_up_text[1], 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 2);
			break;
		
		case RANGED_SKILL:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_interface_text(c, color_level_text, 5453);
			packet_set_interface_text(c, level_up_text[1], 6114);
			break;
		
		case THIEVING_SKILL:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_interface_text(c, color_level_text, 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 2);
			packet_set_interface_text(c, level_up_text[1], 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 3);
			break;
		
		case MINING_SKILL:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_interface_text(c, color_level_text, 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 1);
			packet_set_interface_text(c, level_up_text[1], 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 22);
			break;

		case FARMING_SKILL:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_item_in_interface_model(c, SKILL_LEVELUP_INTERFACE[skill_id] + 1,
									 200, 952);
			packet_set_interface_text(c, color_level_text, 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 3);
			packet_set_interface_text(c, level_up_text[1], 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 2);
			break;

		default:
			packet_send_chatbox_message(c, level_up_text[0]);
			packet_set_interface_text(c, color_level_text,
							   SKILL_LEVELUP_INTERFACE[skill_id] + 1);
			packet_set_interface_text(c, level_up_text[1], 
							   SKILL_LEVELUP_INTERFACE[skill_id] + 2);
			break;
	}

	/*
	c->plr->next_chat_id = 0;
	*/
	packet_show_interface_chatbox(c, SKILL_LEVELUP_INTERFACE[skill_id]);
}

/* Skill specific menu functions.  */

/* Resets skill menu ids when needed.  */
static void reset_skill_menus(struct client *c)
{
	c->plr->action_menu_id = -1;
	c->plr->action_menu_skill_id = -1;
}

/**
 * Sends the appropriate fletching menu to the user.
 * @param *c Client instance.
 */
void fletching_action_menus(struct client *c)
{
	switch(c->plr->action_menu_id)
	{
		/* Fletching - Add feathers to arrow shafts.  */
		case FLETCHING_ACTION_ADD_FEATHERS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_fletching_choice_menu(c, 0, -1);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type = 0;
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_fletching(c);
					break;
			}
			break;
		
		/* Fletching - Add arrow heads.  */
		case FLETCHING_ACTION_ADD_ARROW_HEADS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_fletching_choice_menu(c, 5, c->plr->action_id_type);
					c->plr->next_chat_id++;
					break;
				case 1:
					/* action_id_type is set by the fletching_choice_menu 
					   in this case.  */
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_fletching(c);
					break;
			}
			break;
		/* Fletching - Add bolt heads.  */
		case FLETCHING_ACTION_ADD_BOLT_HEADS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_fletching_choice_menu(c, 6, c->plr->action_id_type);
					c->plr->next_chat_id++;
					break;
				case 1:
					/* action_id_type is set by the fletching_choice_menu 
					   in this case.  */
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_fletching(c);
					break;
			}
			break;

	/* BOLTS, JAVELINS, ETC GO HERE.  */
	/* Case 93, 94, 95, 96 are reserved for later.  */

		/* Fletching - Add dart heads to feathers.  */
		case FLETCHING_ACTION_ADD_DART_HEADS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_fletching_choice_menu(c, 7, c->plr->action_id_type);
					c->plr->next_chat_id++;
					break;
				case 1:
					/* action_id_type is set by the fletching_choice_menu 
					   in this case.  */
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_fletching(c);
					break;
			}
			break;

		/* Fletching - Normal logs.  */
		case FLETCHING_ACTION_USE_LOGS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					switch (c->plr->action_id_type)
					{
						/* Normal logs.  */
						case 1511:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_LOGS, -1);
							break;
						/* Oak logs.  */
						case 1521:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_OAK_LOGS, -1);
							break;
						/* Willow logs.  */
						case 1519:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_WILLOW_LOGS, -1);
							break;
						/* Maple logs.  */
						case 1517:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_MAPLE_LOGS, -1);
							break;
						/* Yew logs.  */
						case 1515:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_YEW_LOGS, -1);
							break;
						/* Magic logs.  */
						case 1513:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_MAGIC_LOGS, -1);
							break;
						/* Achey logs.  */
						case 2862:
							send_fletching_choice_menu(c, 
								FLETCHING_MENU_USE_ACHEYLOGS, -1);
							break;
					}
					c->plr->next_chat_id++;
					break;
				case 1:
					/* NOTE: Add 10 * level of log.
						Ex: chat_choice + (20) (for wood log stuff.)  
						Ex: chat_choice + (60) (for yew log stuff.)  */
					switch (c->plr->action_id_type)
					{
						/* Normal logs.  */
						case 1511:
							c->plr->action_id_type 
							= (FLETCHING_ARROW_SHAFTS + c->plr->chat_choice);
							break;
						/* Oak logs.  */
						case 1521:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_OAK_SBOW + c->plr->chat_choice);
							break;
						/* Willow logs.  */
						case 1519:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_WILLOW_SBOW 
							   + c->plr->chat_choice);
							break;
						/* Maple logs.  */
						case 1517:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_MAPLE_SBOW 
							   + c->plr->chat_choice);
							break;
						/* Yew logs.  */
						case 1515:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_YEW_SBOW 
							   + c->plr->chat_choice);
							break;
						/* Magic logs.  */
						case 1513:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_MAGIC_SBOW 
							   + c->plr->chat_choice);
							break;
						/* Achey logs.  */
						case 2862:
							c->plr->action_id_type 
							= (FLETCHING_CARVE_COMP_BOW 
							   + c->plr->chat_choice);
							break;
					}
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_fletching(c);
					break;
			}
			break;
	}
}

/**
 * Sends the appropriate crafting menu to the user.
 * @param *c Client instance.
 */
void crafting_action_menus(struct client *c)
{
	switch (c->plr->action_menu_id)
	{
		/* Crafting - Spinning wheel.  */
		case CRAFTING_ACTION_SPINNING:
			switch (c->plr->next_chat_id)
			{
				case 0:
					/* Spinning menu.  */
					send_crafting_choice_menu(c, CRAFTING_ACTION_SPINNING, 0);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type = (c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Loom.  */
		case CRAFTING_ACTION_WEAVING:
			switch (c->plr->next_chat_id)
			{
				case 0:
					/* Weaving menu.  */
					send_crafting_choice_menu(c, CRAFTING_ACTION_WEAVING, 0);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type = (CRAFTING_CLOTH_STRIP 
											  + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Pottery wheel.  */
		case CRAFTING_ACTION_POTTERY:
			switch (c->plr->next_chat_id)
			{
				case 0:
					/* Pottery wheel menu.  */
					send_crafting_choice_menu(c, CRAFTING_ACTION_POTTERY, 0);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type = (CRAFTING_SHAPING_POT 
											  + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Pottery kiln.  */
		case CRAFTING_ACTION_FIRING:
			switch (c->plr->next_chat_id)
			{
				case 0:
					/* Pottery kiln menu.  */
					send_crafting_choice_menu(c, CRAFTING_ACTION_FIRING, 1);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type = (CRAFTING_FIRING_POT 
											  + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Tan leather menu.  */
		case CRAFTING_ACTION_TANNING:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, CRAFTING_ACTION_TANNING, 0);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type 
						= (CRAFTING_TAN_SOFT_LEATHER + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Soft leather crafting menu.  */
		case CRAFTING_ACTION_LEATHER:
			switch (c->plr->next_chat_id)
			{
				case 0:
					switch (c->plr->action_id_type)
					{
						/* Soft leather.  */
						case 1741:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 0);
							break;
						/* Hard leather.  */
						case 1743:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 1);
							break;
						/* Green dragon leather.  */
						case 1745:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 2);
							break;
						/* Blue dragon leather.  */
						case 2505:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 3);
							break;
						/* Red dragon leather.  */
						case 2507:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 4);
							break;
						/* Black dragon leather.  */
						case 2509:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 5);
							break;
						/* Snakeskin.  */
						case 6289:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 6);
							break;
						/* Add studs to leather body.  */
						case 1129:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 10);
							break;
						/* Add studs to leather chaps.  */
						case 1095:
							send_crafting_choice_menu(c, 
								CRAFTING_ACTION_LEATHER, 11);
							break;
					}
					c->plr->next_chat_id++;
					break;
				case 1:
					switch (c->plr->action_id_type)
					{
						/* Soft leather.  */
						case 1741:
							c->plr->action_id_type 
							= (CRAFTING_LEATHER_BODY + c->plr->chat_choice);
							break;
						/* Hard leather.  */
						case 1743:
							c->plr->action_id_type 
							= (CRAFTING_HARD_LEATHER_BODY 
								+ c->plr->chat_choice);
							break;
						/* Green dragon leather.  */
						case 1745:
							c->plr->action_id_type 
							= (CRAFTING_GREEN_DLEATHER_BODY 
								+ c->plr->chat_choice);
							break;
						/* Blue dragon leather.  */
						case 2505:
							c->plr->action_id_type 
							= (CRAFTING_BLUE_DLEATHER_BODY 
								+ c->plr->chat_choice);
							break;
						/* Red dragon leather.  */
						case 2507:
							c->plr->action_id_type 
							= (CRAFTING_RED_DLEATHER_BODY 
								+ c->plr->chat_choice);
							break;
						/* Black dragon leather.  */
						case 2509:
							c->plr->action_id_type 
							= (CRAFTING_BLACK_DLEATHER_BODY 
								+ c->plr->chat_choice);
							break;
						/* Snakeskin.  */
						case 6289:
							c->plr->action_id_type 
							= (CRAFTING_SNAKESKIN_BODY 
								+ c->plr->chat_choice);
							break;
						/* Add studs to leather body.  */
						case 1129:
							c->plr->action_id_type 
								= CRAFTING_ADD_STUDS_LEATHER_BODY;
							break;
						/* Add studs to leather chaps.  */
						case 1095:
							c->plr->action_id_type
								= CRAFTING_ADD_STUDS_LEATHER_CHAPS;
							break;
					}
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Making molten glass.  */
		case CRAFTING_ACTION_MOLTEN_GLASS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, CRAFTING_ACTION_MOLTEN_GLASS, -1);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type 
						= (CRAFTING_MOLTEN_GLASS + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Making glass items.  */
		case CRAFTING_ACTION_GLASSBLOWING:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, CRAFTING_ACTION_GLASSBLOWING, -1);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type 
						= (CRAFTING_VIAL + c->plr->chat_choice);
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Cut gems.  */
		case CRAFTING_ACTION_CUTTING_GEMS:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, CRAFTING_ACTION_CUTTING_GEMS,
						c->plr->action_id_type);
					c->plr->next_chat_id++;
					break;
				case 1:
					/* c->plr->action_id_type is set through the menu.
					c->plr->action_id_type = (130 + c->plr->chat_choice); */
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
			}
			break;
		/* Crafting - Making jewelry.  */
		case CRAFTING_ACTION_MAKING_JEWELRY:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, 
						CRAFTING_ACTION_MAKING_JEWELRY, -1);
					c->plr->next_chat_id++;
					break;
				case 1:
					/* c->plr->action_id_type is set through the menu.
					c->plr->action_id_type = (130 + c->plr->chat_choice); */
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
					
			}
			break;
		/* Crafting - Silver.  */
		case CRAFTING_ACTION_SILVER:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_crafting_choice_menu(c, CRAFTING_ACTION_SILVER, -1);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_id_type 
						= (CRAFTING_UNSTRUNG_SYMBOL + c->plr->chat_choice);
					printf("Silver choice %d\n", c->plr->action_id_type);	
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_crafting(c);
					break;
					
			}
			break;
	}
}

/**
 * Sends the appropriate cooking menu to the user.
 * @param *c Client instance.
 */
void cooking_action_menus(struct client *c)
{
	switch (c->plr->action_menu_id)
	{
		/* Standard, how many you want to cook menu.  */
		case COOKING_ACTION_COOK:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_cooking_choice_menu(c, c->plr->action_id_type);
					c->plr->next_chat_id++;
					break;
				case 1:
					/*
					Chat choice not used for this.

					c->plr->action_id_type = (c->plr->chat_choice);
					*/
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					init_cooking(c);
					break;
			}
			break;
		/* What kind of dough interface.  */
		case COOKING_ACTION_DOUGH:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_cooking_dough_menu(c);
					c->plr->next_chat_id++;
					break;
				case 1:
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					create_dough(c, c->plr->chat_choice);
					break;
			}
			break;
	}
}

/**
 * Sends the appropriate smithing menu to the user.
 * @param *c Client instance.
 */
void smithing_action_menus(struct client *c)
{
	switch (c->plr->action_menu_id)
	{
		/* Smelting bars standard interface.  */
		case SMITHING_ACTION_SMELT:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_smelting_choice_menu(c);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_skill_type = SMITHING_ACTION_SMELT;
					c->plr->action_skill_result_item 
						= get_smithing_bar_id_rev(c->plr->chat_choice);
					c->plr->action_id_type = c->plr->chat_choice;
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					printf("NEXT STEP: %d\n", c->plr->chat_choice);
					init_smithing(c);
					break;
			}
			break;
		/* Smithing metal into objects, standard interface.  */
		case SMITHING_ACTION_SMITH:
			switch (c->plr->next_chat_id)
			{
				case 0:
					send_smithing_choice_menu(c, c->plr->action_item_id);
					c->plr->next_chat_id++;
					break;
				case 1:
					c->plr->action_skill_type = SMITHING_ACTION_SMITH;
					reset_skill_menus(c);
					packet_close_all_open_interfaces(c);
					printf("NEXT STEP!\n");
					init_smithing(c);
					break;
			}
			break;
	}
}
