/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INTERFACE_H_
#define _INTERFACE_H_

/**
 * Holds info on interfaces, what id's are a part of it, and how many id's
 * there are.
 */
struct parent_interface
{
	/** ID of the interface.  */
	int id;
	/** Array holding ID's of child interfaces to this one.  */
	int *children;
	/** Size of the children array.  */
	int size;
};

int anticheat_check_interface(struct client *c, int interface_clicked);
void send_cooking_dough_menu(struct client *c);
void send_cooking_choice_menu(struct client *c, int menu_id);
void send_fletching_choice_menu(struct client *c, int menu_id, 
								int arrow_head_id);
void send_crafting_choice_menu(struct client *c, int menu_id, int item_id);
void send_smelting_choice_menu(struct client *c);
void send_smithing_choice_menu(struct client *c, int metal_bar_id);
void send_bank_menu(struct client *c);
void send_bank_deposit_box_menu(struct client *c);
void send_shop_menu(struct client *c, int shop_id);
int get_quest_index_from_id(int id);
int get_quest_index_from_intr_id(int intr_id);
void send_quest_log(struct client *c, int intr_id);
void send_skill_guide(struct client *c, int skill, int page);
#endif
