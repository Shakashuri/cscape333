0 – Slashing	1 – Crushing	2 – Stabbing	3 – Ranged	4 – Magic
18	Temple guardian claw	0		
25	Baby dragon headbutt	1		
30	Bat swoop (bite?)	2		
41	Bear claw	0		
55	Chicken peck	2		
59	Cow headbutt	1		
64	Demon claw	0		
75	Wolf slash	0		
80	KBD claw	0		
91	Dragon head slam?	1		
94	Tree spirit wand?	2
99	Dwarf crush	1		
115	Peck	2		
123	Ghost claw	0		
128	Giant club crush	1		
138	Giant rat bite	2		
143	Giant spider bite	2		
153	Golem smash	1		
158	Hellhound bite/claw	2		
163	Hobgoblin spear stab	2		
164	Hobgoblin punch	1		
169	Imp jump punch	1		
185	Magic axe slash	0		
190	Gnome shoot bow	3		
191	Gnome punch	1		
192	Gnome slash	0		
220	Monkey jump hit	1		
227	Cave crawler tongue	1		
237	Penguin beak?	2		
240	Rat bite	2		
246	Scorpion snip	0		
255	Skavid claw	0		
260	Skeleton slash	0		
265	Ghostly skeleton slash	0		
270	Small scorpion snip	0		
275	Snake bite	2		
280	Spider bite	2		
284	Troll club	1		
289	Unicorn headbutt	2		
294	Giant smack	1		
299	Zombie smack	1		
309	Goblin smack	1		
310	Goblin spear	2		
315	Cat slash	0		
322	Terrorbird bite	2		
327	Mounted terrorbird bite	2		
328	Mounted terrorbird spear stab	2		
338	Soulless club	1		
344	Bird peck	2		
353	Strange plant bite	2		
359	Ogre overhead	1		
376	DDagger	2		
377	Ddagger	0		
393	Claws	0		
395	Axe slash	0		
401	Mace	1		
406	2h overhead	1		
412	Dagger	2		
422	Fist	1		
423	Kick	1		
426	Bow	3		
428	Spear stab	2		
429	Spear smash	1		
451	Sword	0		
1011	Chompy headbutt	1		
1019	Plague frog	1		
1029	Fire elemental smack	1		
1035	Earth elemental punch	1		
1040	Air elemental smack	1		
1044	Water elemental stab	2		
1080	Wolfman claw	0		
1087	Ghast claws	0		
1184	Kalphite bite	2		
1199	Shaikahan claw	0		
1244	Rabbit jump spin	1		
1245	Rabbit bite	2		
1277	Snail stab?	2		
1284	Fiyr shade slash	0		
1312	Rock crab smack	1
1341	Daggonoth bite	2		
1383	Monkey punch	1		
1394	Ninja monkey bow	3		
1396	Ninja monkey slash	0		
1402	Gorilla punch	1		
1495	Skeletal hellhound slash	0		
1512	Kurask slam	1		
1517	Gargoyle flap	1		
1523	Banshee screech	2		
1528	Nechryael stab	2		
1537	Abyssal demon stab	2		
1540	Death spawn slash	0		
1546	Basilisk slash	0		
1552	Bloodveld tongue	0		
1557	Dust devil inflate	0	check	
1560	Cockatrice flap	1		
1567	Rockslug poke	2		
1582	Pyrefiend smack	1		
1586	Cube touch	1		
1592	Crawling hand smack	1		
1595	Turoth scratch	0		
1612	Experiment bite (sheepdog)	2		
1616	Experiment headbutt (spider)	1		
1626	Abomination smack	1		
1750	Slaglith smack down	1		
1760	Kendall slash	0		
1775	Cave bug bite	2		
1785	Goblin claw	0		
1789	Cave slime poke	1		
1793	Frog tongue	1		
1802	Wall beast claw	0		
1840	Arzinian avatar smack	1		
1843	Arzinian avatar bow	3	check	
1844	Arzinian avatar magic	4	check	
1926	Mummy slap	1		
1948	Scarab bite	2		
2014	Locust bite	2	check	
2029	Sphinx headbutt	1		
2039	Crocodile bite	1		
2062	Verac the Defiled flail	1		
2067	Dharok the Wretched slash	0		
2068	Torag the Corrupted hammers	1		
2070	Bloodworm leap	2		
2075	Karil the Tainted shoot	3		
2078	Ahrim the Blighted slash	0		
2080	Guthan the Infested stab	2		
2102	Slash Bash’s kick	1		
2120	Goblin stab	2		
2167	Thing under the Bed’s claw	0		
2299	Evil chicken peck	2		
2365	Wallasalki bite	2		
2371	Pheasant peck	2		
2397	Bug sting	2		
2609	TzHaar-Mej punches	1		
2610	TzHaar-Ket crush	1		
2612	TzHaar-Xil stab	2		
2621	Tz-Kih bite	2		
2625	Tz-Kek smack	1		
2628	Tok-Xil punch	1		
