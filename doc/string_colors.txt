These are chat codes which go in interface text strings sent to the client.
They are used in this format.

set_interface_text(c, "@blu@Hello! @red@This is a test!", 9999);

@red@ - red
@gre@ - green
@blu@ - blue
@yel@ - yellow
@cya@ - cyan
@mag@ - magenta
@whi@ - white
@bla@ - black
@lre@ - orange
@dre@ - dark red
@dbl@ - dark blue
@or1@ - bright orange-yellow
@or2@ - bright orange
@or3@ - bright orange-red
@gr1@ - bright green-yellow
@gr2@ - bright lime green
@gr3@ - slightly greener @gr2@
@str@ - strike-through text
@end@ - end strike-through
