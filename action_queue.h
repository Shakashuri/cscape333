#ifndef _ACTION_QUEUE_H_
#define _ACTION_QUEUE_H_

enum action_queue_type
{
	AQ_IDLE = 0,
	AQ_PLAY_EMOTE,
	AQ_PLAY_GFX,
	AQ_PLAY_PHASE_MOVEMENT,
	AQ_SET_TIMED_TELEPORT,
	AQ_SET_STANDING_EMOTE,
	AQ_SET_WALKING_EMOTE,
	AQ_SET_RUNNING_EMOTE,
	AQ_MOVE_TO_POSITION,
	AQ_TURN_TO_DIRECTION,
	AQ_OPERATE_AUTO_DOORS
};

void action_queue_clear(struct client *c);
int action_queue_add_action(struct client *c, int type, int ticks, 
							int action_data_1, int action_data_2, 
							int action_data_3, int action_data_4,
							int action_data_5, int action_data_6);
void action_queue_start(struct client *c);
void action_queue_end(struct client *c);
void process_action_queue(struct client *c);

#endif
