List of files in this directory and their purpose. All files are tab delimited, 
unless otherwise stated. Any file not listed is one that exists for reference,
will be removed/replaced on a later date.

globaldrops.tsv - Contains ids of items that spawn on the ground, at what 
	location, and their respawn time.

items.tsv - Contains the definitions of every item, and the stats associated
	with it (price, weight, stat bonuses, etc).

map*.gz - Contains collision data for each level of the game world, 0 is
	ground floor, going up to 3 which is the 3rd floor.

npcs.tsv - Lists all npcs and all their stats associated with them (health,
	attacks available, size, etc).

drop_table.tsv - Lists all different loot tables and their entries. Used for
	NPC loot drops.

npcspawns.tsv - Lists npcs that spawn in the world, at what location, if they
	walk around their spawn area, the direction they face, and their respawn 
	time.

objects.tsv - Contains every object in the game, their stats, size, etc.

objectspawns.tsv - Lists all objects that the player can interact with in the
	world, their location, facing direction, and some info specific to double 
	door handling.

server.conf.default - Contains default configuration data for the server,
	should be copied to remove the ".default" part so the server will read it.
	Customize the options in it to change server operation.

spells.tsv - Contains data for each spell in the game, the effect they play,
	what they cost, etc.
