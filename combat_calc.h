/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMBAT_CALC_H_
#define _COMBAT_CALC_H_

void calculate_max_hit(struct client *c);
void calculate_npc_max_hit(struct npc_spawn *npc, int ranged_dmg);
void calculate_player_combat_level(struct client *c);
int get_npc_combat_level(struct npc *n);

int get_player_attacking_roll(struct client *c);
float calculate_pvp_guard_chance(struct client *c, struct client *attacker);

float calculate_pve_guard_chance(struct client *c, struct npc_spawn *attacker);

float calculate_npc_hit_chance(struct npc_spawn *npc);
float calculate_npc_guard_chance(struct npc_spawn *npc, int damage_type);

void apply_spell_effects_to_npc(struct npc_spawn *npc, int spell_index);

int get_ranged_hit_delay(int distance);
int get_magic_hit_delay(int distance);
int is_diagonal_from_target(int s_x, int s_y, int e_x, int e_y);
#endif
