/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file csv.c
 * @brief Custom library to handle loading of csv/tsv files.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "csv.h"

int free_csv_line(struct csv_line *line)
{
	if (line == NULL)
	{
		return 1;
	}

	int x = 0;

	if (line->tokens != NULL)
	{
		for (x = 0; x < line->num_tokens; x++)
		{
			if (line->tokens[x] != NULL)
			{
				free(line->tokens[x]);
			}
		}

		free(line->tokens);
	}

	free(line);

	return 0;
}

int free_csv_file(struct csv_file *file)
{
	if (file == NULL)
	{
		return 1;
	}

	int x = 0;
	
	if (file->data != NULL)
	{
		for (x = 0; x < file->num_lines; x++)
		{
			if (file->data[x] != NULL)
			{
				free_csv_line(file->data[x]);
			}
		}

		free(file->data);
	}

	free(file);

	return 0;
}

struct csv_line *new_csv_line(void)
{
	struct csv_line *new = calloc(1, sizeof(struct csv_line));

	if (new == NULL)
	{
		printf("ERROR: Failed to create new csv_line struct!\n");
		return NULL;
	}

	new->tokens = NULL;
	new->num_tokens = 0;

	return new;
}

struct csv_file *new_csv_file(void)
{
	struct csv_file *new = calloc(1, sizeof(struct csv_file));

	if (new == NULL)
	{
		printf("ERROR: Failed to create new csv_file struct!\n");
		return NULL;
	}

	new->data = NULL;
	new->num_lines = 0;

	return new;
}

void print_csv_line(struct csv_line *line)
{
	int x = 0;
	printf("num entries: %d\n", line->num_tokens);
	for (x = 0; x < line->num_tokens; x++)
	{
		printf("%d: %s\n", x, line->tokens[x]);
	}
	printf("done\n");
}

void print_csv_line_horizontal(struct csv_line *line)
{
	int x = 0;
	for (x = 0; x < line->num_tokens; x++)
	{
		printf("%s", line->tokens[x]);

		/* Only print a tab if we haven't reached the last item yet.  */
		if (x + 1 < line->num_tokens)
		{
			printf("\t");
		}
	}
	printf("\n");
}

void print_csv_file(struct csv_file *file)
{
	int x = 0;
	for (x = 0; x < file->num_lines; x++)
	{
		print_csv_line(file->data[x]);
	}
}

void print_csv_file_horizontal(struct csv_file *file)
{
	int x = 0;
	for (x = 0; x < file->num_lines; x++)
	{
		print_csv_line_horizontal(file->data[x]);
	}
}

int get_num_bytes_in_file(FILE *fp)
{
	rewind(fp);

	fseek(fp, 0, SEEK_END);	
	long int byte_count = ftell(fp);

	rewind(fp);

	return byte_count;
}

int get_num_lines_in_file(FILE *fp)
{
	int num_newlines = 0;
	int read_ch; 

	rewind(fp);

	while ((read_ch = fgetc(fp)) != EOF)
	{
		if (read_ch == '\n')
		{
			num_newlines++;
		}
	}

	rewind(fp);

	return num_newlines;
}

int get_num_delimiters_in_line(FILE *fp, char delimiter)
{
	long int current_loc = 0;
	int num_delimiters = 0;
	int read_ch; 

	if (fp == NULL)
	{
		return 0;
	}

	current_loc = ftell(fp);

	while ((read_ch = fgetc(fp)) != '\n')
	{
		if (read_ch == delimiter)
		{
			num_delimiters++;
		}
	}

	fseek(fp, current_loc, SEEK_SET);

	return num_delimiters;
}

int add_line_to_csv_file(struct csv_file *file, struct csv_line *line)
{
	if (line == NULL || file == NULL)
	{
		return 1;
	}

	if (file->data == NULL)
	{
		file->num_lines = 1;
		file->data = calloc(file->num_lines, sizeof(struct csv_line**));

		if (file->data == NULL)
		{
			printf("Failed to calloc lines array!\n");
			return 1;
		}
	}
	else
	{
		file->num_lines++;
		file->data = realloc(file->data, file->num_lines 
							 * (sizeof(struct csv_line**)));
	}
	

	file->data[file->num_lines - 1] = line;

	return 0;
}

int add_token_to_csv_line(struct csv_line *line, char *token)
{
	if (line == NULL || token == NULL)
	{
		return 1;
	}

	int token_size = 0;

	if (line->tokens == NULL)
	{
		line->num_tokens = 1;
		line->tokens = calloc(line->num_tokens, sizeof(char**));

		if (line->tokens == NULL)
		{
			printf("Failed to calloc tokens array!\n");
			return 1;
		}
	}
	else
	{
		line->num_tokens++;
		line->tokens = realloc(line->tokens, line->num_tokens * (sizeof(char**)));
	}

	token_size = strlen(token) + 1;
	line->tokens[line->num_tokens - 1] = calloc(token_size, sizeof(char));
	strcpy(line->tokens[line->num_tokens - 1], token);

	return 0;
}

struct csv_line *read_tokens_from_string(char *buffer, char *delimiters)
{
	if (buffer == NULL || delimiters == NULL)
	{
		return NULL;
	}

	struct csv_line *line = new_csv_line();
	char *saveptr = NULL;
	char *tok = strtok_r(buffer, delimiters, &saveptr);

	if (tok != NULL)
	{
		add_token_to_csv_line(line, tok);

		while ((tok = strtok_r(saveptr, delimiters, &saveptr)) != NULL)
		{
			add_token_to_csv_line(line, tok);
		}
	}
	else
	{
		return NULL;
	}

	return line;
}

char *read_line_from_file(FILE *fp)
{
	if (fp == NULL)
	{
		return NULL;
	}

	long int starting_pos = ftell(fp);
	int buffer_size = 1000;
	char *buffer = calloc(buffer_size, sizeof(char));
	char *rv = NULL;

	if (buffer == NULL)
	{
		printf("Failed to alloc memory for buffer!\n");
		return NULL;
	}

	rv = fgets(buffer, buffer_size, fp);

	if (rv == NULL)
	{
		free(buffer);
		return NULL;
	}

	/* Check if newline not found in the buffer, means we didn't get a full
	 * line from the file.  */
	if (strchr(buffer, '\n') == NULL)
	{
		/* Loop until we read a good newline.  */
		while (1)
		{
			fseek(fp, starting_pos, SEEK_SET);

			printf("Newline not found, expanding buffer to %d from %d.\n", buffer_size, buffer_size * 2);
			buffer = realloc(buffer, (buffer_size *= 2) * sizeof(char));

			rv = fgets(buffer, buffer_size, fp);

			if (rv == NULL)
			{
				free(buffer);
				return NULL;
			}

			if (strchr(buffer, '\n') != NULL)
			{
				printf("Found a newline, break and return.\n");
				break;
			}
			else
			{
				printf("Need to expand again, not found.\n");
			}
		}
	}

	/* Shrink string if too big.  */
	int true_size = strlen(buffer) + 1;

	if (buffer_size > true_size)
	{
		buffer = realloc(buffer, true_size * sizeof(char));
	}

	return buffer;
}

struct csv_file *read_csv_file(FILE *fp, char *delimiters)
{
	if (fp == NULL)
	{
		printf("File pointer is null, returning.\n");
		return NULL;
	}

	struct csv_file *csv = new_csv_file();

	char *buffer = NULL;

	while ((buffer = read_line_from_file(fp)) != NULL)
	{
		struct csv_line *line = NULL;
		line = read_tokens_from_string(buffer, delimiters);
		free(buffer);

		add_line_to_csv_file(csv, line);
	}

	return csv;
}

char *get_csv_token(struct csv_file *csv, int line, int token)
{
	if (csv == NULL)
	{
		//printf("CSV file is a null!\n");
		return "\0";
	}

	if (line < 0 || line >= csv->num_lines)
	{
		//printf("Line number out of bounds!\n");
		return "\0";
	}

	if (csv->data == NULL)
	{
		//printf("CSV data struct is NULL!\n");
		return "\0";
	}

	if (token < 0 || token >= csv->data[line]->num_tokens)
	{
		//printf("Token number out of bounds: %d %d\n", line, token);
		return "\0";
	}

	return csv->data[line]->tokens[token];
}
