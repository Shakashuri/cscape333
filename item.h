/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ITEM_H_
#define _ITEM_H_

/**
 * @brief Item definition information.
 */
struct item
{
	/** Item id.  */
	int id;
	/** Item name.  */
	char name[30];
	/** Item examine description.  */
	char desc[100];
	/** Equipment slot the item goes to, -1 if not able to be equipped.  */
	int slot;
	/** Is this item a bank note?  */
	int noted;
	/** Does this item stack?  */
	int stacking;
	/** Can you trade this item with other players?  */
	int tradable;
	/** Can this item be placed into a bank?  */
	int bankable;
	/** If this item is a bank note, this is its non-noted id.  */
	int unnoted_id;
	/** The id of this item when you trade it for a bank note.  */
	int note_id;
	/** Id that represents the base type of the item. Example, used for 
	 * herblore to see what kind of potion it is.  */
	int base_id;
	/** Item's price from a specialty store.  */
	int spec_price;
	/** Item's value when high alchemy is cast on it.  */
	int high_alch;
	/** Item's value when low alchemy is cast on it.  */
	int low_alch;
	/** How heavy this item is.  */
	float weight;

	/**
	 * Leads to the next item after this one has been eaten, or a dose has been
	 * drank from it.  */
	int item_consume_id;

	/** 
	 * If the item is a container for something, this is its "empty" 
	 * version.
	 */
	int item_container_id;
	
	/**
	 * Used in appearance updating to check if certain player parts need to
	 * be hidden when an item is worn. Ex: not showing player arms when wearing
	 * platebody equipment.  
	 * 1 - Hide hair
	 * 2 - Hide beard
	 * 3 - Hide whole head
	 * 4 - Hide arms
	 * 5 - Hide hands
	 * 6 - Hide arms and hands
	 */
	int display_mask;
	/** Item's stat bonuses.  */
	int bonuses[12];
	/** Ranged strength of the item, used for bows mostly.  */
	int ranged_strength;
	/** If the item needs both hands to wield.  */
	int two_handed;
	/** If the item is a ranged weapon or not.  */
	int ranged;
	/** Item's combat range.  */
	int reach;
	
	/**
	 * 0 - None
	 * 3 - Stacks in weapon slot 
	 * 4 - Charges
	 * 10-19: Arrows
	 * 	10 - max bronze
	 *  11 - max iron
	 *  12 - max steel
	 *  13 - max mithril
	 *  14 - max adamant
	 *  15 - max rune
	 *  16 - max dragon
	 * 20-29: Bolts
	 * 	like above
	 *  27 - bolt racks
	 * 30-39: Brutal arrows
	 */
	int ammo_type;

	/** What graphics to use when readying the throwing/ranged weapon.  */
	int draw_gfx;
	/** What projectile graphic the item uses when thrown.  */
	int shoot_gfx;

	/** If the item is a weapon, how fast the player can attack with this. 
	 * Stores the number of ticks between attacks.
	 */
	int atk_speed; 
	/** Level needed in a skill to equip.  */
	int skill_req[PROTOCOL_NUM_SKILLS];
	/**
	 * If the item has use in a skill, the level in that skill to use it.  
	 * Different from above, this is the level to use a pickaxe for mining,
	 * above is the level needed to equip the pickaxe.
	 */
	int skill_use_req;

	/** How many charges the item has left (crystal bow).  */
	int charges;

	/** Damage type output from weapon.  */
	int atk_type;

	/**@{*/
	/** Item animations for combat.  */
	int stand_anim;
	int walk_anim;
	int run_anim;
	int attacking_emotes[4];
	int block_anim;
	int spec_anim;
	/**@}*/

	/** Graphic played on self when using special attack.  */
	int self_spec_gfx;
	/** Graphic played on target when using special attack.  */
	int other_spec_gfx;
	/** Id of the special attack bar used.  */
	int spec_bar_id;
	/** How much the special attack bar is drained by the attack.  */
	float spec_bar_drain;
};

/**
 * @brief Information when an item is on the ground.
 */
struct item_drop
{
	/** Item dropped id.  */
	int id;
	/** Number of items that were dropped.  */
	int amount;
	/** Number of charges the item has.  */
	int charges;

	/** X coord of the item in the world.  */
	int world_x;
	/** Y coord of the item in the world.  */
	int world_y;
	/** Z coord of the item in the world.  */
	int world_z;

	/** Item respawn timer.  */
	int timer;
	/** If this item respawns after it was taken, ticks until it respawns.  */
	int tick_picked_up;
	/** If this item was dropped, ticks until it despawns.  */
	int tick_dropped;
	/** How many ticks to wait before item is spawned into the world.  */
	int tick_spawn_delay;
	/** Player index of the 'owner' of the item, who it appears for first.  */
	int player_index;

	/** Any note about the item drop.  */
	char note[200];
};

/**
 * Struct that contains all the information for a loot table.
 */
struct loot_table
{
	/** Name of the loot table.  */
	char name[100];

	/** Number of item drops in this table.  */
	int num_drops;
	/** Number of tables within this table. This allows other loot tables to be
	 * called if rolled on.  */
	int num_tables;

	/** 2d array holding all the drop info. Size is num_drops, and each entry
	 * has 4 values: item id, min count, max count, weight.  */
	double **drops;
	/** 2d array holding all the table info. Size is num_drops, and each entry
	 * has 4 values: table index, min count, max count, weight.  */
	double **tables;
	/** Holds the names of the tables within this one.  */
	char table_names[MAX_LOOT_TABLE_SUB_TABLES][100];

	/** The total weight for all of the drops in this table. Used when rolling
	 * from 0.0 to this to get a random drop.  */
	double total_weights;
};

int does_item_hide_hair(int item);
int does_item_hide_beard(int item);
int does_item_hide_hair_and_beard(int item);
int does_item_hide_arms(int item);
int does_item_hide_hands(int item);
int does_item_hide_arms_and_hands(int item);

int does_player_have_enough_inv_slots_for_item(struct client *c, int item_id,
											   int amount);

int add_item_to_inv(struct client *c, int item_id, int amount, int charges); 
int add_item_to_inv_silent(struct client *c, int item_id, int amount, 
						   int charges);
int add_item_to_inv_no_refresh(struct client *c, int item_id, int amount, 
							   int charges);
int add_item_to_inv_silent_no_refresh(struct client *c, int item_id, 
									  int amount, int charges);
void add_item_to_inv_drop_if_no_space(struct client *c, int item_id, 
									  int amount, int charges);
void add_item_to_inv_delay(struct client *c, int item_id, int num, int charges, 
						   int delay);

void delete_item_from_inv(struct client *c, int item_id, int amount);
void delete_item_from_inv_nonstack(struct client *c, int item_id, int amount);
void delete_item_from_inv_slot(struct client *c, int slot, int amount);
void delete_item_from_equipment_slot(struct client *c, int slot, int amount);

void move_item(struct client *c, int from, int to, int interface_id);

void transform_item(struct client *c, int slot, int id_to);
void transform_item_no_refresh(struct client *c, int slot, int id_to);

int fill_water_containers(struct client *c, int *pos_store);
void decant_potions(struct client *c, int first_potion, int first_potion_slot, 
					int second_potion, int second_potion_slot);

void remove_inventory_item_charges(struct client *c, int slot, int charges);
void remove_equipment_item_charges(struct client *c, int slot, int charges);
int equip_item(struct client *c, int item_id, int slot);
void unequip_item(struct client *c, int item_id, int slot);
void empty_player_inventory(struct client *c);
void empty_player_equipment(struct client *c);
void set_special_equipment_configs(struct client *c);
void apply_equipment_info(struct client *c, int item_id, int slot, int amount, 
				   int charges);

void deposit_into_bank(struct client *c, int item_id, int item_slot, 
					   int item_num);
void withdraw_from_bank(struct client *c, int item_id, int item_slot, 
						int item_num);

void apply_weapon_info(struct client *c, int item_id);

void apply_item_drop_defaults(struct item_drop *i);
void spawn_item_on_ground(struct client *c, int drop_id);
void despawn_item_on_ground(struct client *c, int drop_id);

void spawn_item_globally(int drop_id);
void spawn_item_globally_except_owner(int drop_id);
void despawn_item_globally(int drop_id);

int add_to_item_drops(int item_id, int amount, int charges, 
					  int pos_x, int pos_y, int height, int dropped);
int add_to_item_drops_exclusive(struct client *c, int item_id, int amount, 
								int charges, int pos_x, int pos_y, int height,
								int dropped);
void delete_from_item_drops(int item_index, int remove);
int is_item_drop_in_world(int item_id, int pos_x, int pos_y, int pos_z);
void scan_for_pickup(struct client *c);
float get_total_carried_weight(struct client *c);

void get_most_valuable_items(struct client *c, int items[], int location[], 
							 int num_items);
int get_num_items_kept_on_death(struct client *c);
void packet_player_drop_item_on_grounds_on_death(struct client *c, int num_to_keep);
void print_valueable_items(struct client *c, int items[], int location[], int num_items);
int player_has_item(struct client *c, int item_id, int amount);
int get_player_item_location_multiple_valid(struct client *c, int *item_ids, 
		int size);
int player_has_item_slot(struct client *c, int item_id, int amount, int slot);
int player_has_item_start(struct client *c, int item_id, int amount, int start);
int player_has_item_non_stack(struct client *c, int item_id, int amount);
int player_has_num_items_non_stack(struct client *c, int item_id);
int player_has_item_equip(struct client *c, int item_id, int amount);
int get_item_in_inventory_slot(struct client *c, int slot);
int combine_items(struct client *c, int item_1, int item_1_n, 
				  int item_2, int item_2_n, int item_3, int item_3_n);
int combine_items_permissive(struct client *c, int item_1, int item_2, 
							 int item_3, int num_to_try);
int combine_items_non_stack(struct client *c, int item_1, int item_1_n, 
				  int item_2, int item_2_n, int item_3, int item_3_n);
int look_for_rune(struct client *c, int rune_id, int num_needed);
int get_item_default_charges(int item_id);

int get_player_weapon_has_ammo(struct client *c);
int get_player_weapon_reach(struct client *c);
int is_player_weapon_ranged(struct client *c);
int get_player_weapon_projectile_id(struct client *c);
int get_player_weapon_speed(struct client *c);

int get_loot_table_item(struct client *c, int table_idx, int *amount);
void drop_npc_loot(struct npc_spawn *npc);

void player_shoot_npc(struct client *c, int distance, int size);
void npc_shoot_player(struct npc_spawn *npc, int distance);

void eat_food(struct client *c, int food_id, int food_slot);
int *search_item_names_and_desc_for_string(char *search_string, int *num_results);
#endif
