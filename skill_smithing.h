#include <stdio.h>
#include <stdint.h>

#include "constants.h"
/* SMITHING - ID 13.  */

int get_smithing_number_made(int smithing_id);
int get_smithing_bar_id(int metal_bar_id);
int get_smithing_bar_id_rev(int choice_id);
int get_smithing_level_needed(int smithing_id);
int get_smithing_bars_needed(int smithing_id);
int get_smithing_id_from_ore(int ore_id);

float get_smelting_xp(int smithing_id);
float get_smithing_xp(int smithing_id);
int get_smelting_ore_needed(int smithing_id, int secondary_ore);
int get_smelting_ore_number_needed(int smithing_id, int secondary_ore);

void smithing_return_message(struct client *c, int metal_id, int smithing_id);
void smelting_messages(struct client *c, int metal_id, int state);
void init_smithing(struct client *c);
void handle_smithing(struct client *c);
void handle_smelting(struct client *c);
