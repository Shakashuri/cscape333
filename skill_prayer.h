#ifndef _SKILL_PRAYER_H_
#define _SKILL_PRAYER_H_

int get_prayer_level_needed(int prayer_id);
float get_prayer_xp(int prayer_id);
int get_prayer_drain(int prayer_id);
int get_prayer_config_id(int prayer_id);
float get_prayer_stat_change(int prayer_id);

void bury_bones(struct client *c, int item_id, int item_slot);
void calculate_prayer_drain_resistance(struct client *c);
void enable_prayer(struct client *c, int prayer_id);
void disable_prayer(struct client *c, int prayer_id);
void handle_prayer(struct client *c, int prayer_id);

#endif
