CScape333
=========

CScape is a RuneScape server emulator completely written in C with the
goal of replicating how it was in 2005.

CScape is licensed under AGPL v3, see the LICENSE file for details.

I have not had as much time or interest to keep developing this, so updates
will be slow, and there will be bugs. I started this as my first project
in C, so expect horrible code, bugs, etc.

For information on how to setup and use CScape, see [INSTALL](INSTALL.md).
A list of features that have yet to be implemented can be found in 
[TODO](TODO.md). For a list of *known* things that may not be accurate to the
real game, check [INACCURACIES](INACCURACIES.md).

Doxygen documentation can also be generated, and is heavily used throughout
the code.

**Specifications:**

	Asynchronous client handling.  
	333 runescape version emulation.
		Dated to 26 of September, 2005.

**Current Features:**

	Login support.
	ISAAC cypher handling.
	Combat almost all done, just missing the smite prayer, pvp.
	NPC loading, dropping loot.
	Item data loading.
	Working doors/gates.
	18/21 skills working.
		All combat (except the Smite prayer).
		Magic combat works, but needs tweaking. Non-combat magic spells need
			work, too.
		Woodcutting, firemaking, mining, smithing, fishing, crafting, cooking, 
			runecrafting, prayer, herblore, thieving, and fletching all work.
		Some agility courses work, needs more work. No shortcuts implemented 
			yet.
		Major features of the listed skills are implemented, but some minor
			features or quest locked things may not be at the moment.
	1/86 quests implemented.
		Sheep Shearer

See the TODO file for more info on what needs to be done. Doxygen documentation
can also be generated for information on the server.
