/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file misc.c 
 * @brief Collection of misc functions that don't fit anywhere else.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "constants.h"
#include "client.h"
#include "fileio.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "regions.h"

/**
 * Takes how the server tracks what direction an entity is facing/moving and
 * converts it to how the client expects it to be.
 * @param direction Direction of travel in server format.
 * @return Returns direction in the client ready format.
 */
int convert_server_direction_to_client_format(int direction)
{
	switch (direction)
	{
		case SOUTH_WEST:
			return 5;
		case SOUTH:
			return 6;
		case WEST:
			return 3;
		case SOUTH_EAST:
			return 7;
		case EAST:
			return 4;
		case NORTH:
			return 1;
		case NORTH_EAST:
			return 2;
		case NORTH_WEST:
			return 0;

		default:
			return -1;
	}
}

/**
 * From the direction moved, finds out if there is any change in the x axis.
 * @param direction Direction moved.
 * @returns The change in x axis from the passed direction.
 */
int convert_server_direction_to_client_format_delta_x(int direction)
{
	switch (direction)
	{
		case NORTH_WEST:
		case WEST:
		case SOUTH_WEST:
			return -1;

		case NORTH_EAST:
		case EAST:
		case SOUTH_EAST:
			return 1;

		case NORTH:
		case SOUTH:
		default:
			return 0;
	}
}

/**
 * From the direction moved, finds out if there is any change in the y axis.
 * @param direction Direction moved.
 * @returns The change in y axis from the passed direction.
 */
int convert_server_direction_to_client_format_delta_y(int direction)
{
	switch (direction)
	{
		case NORTH_WEST:
		case NORTH:
		case NORTH_EAST:
			return 1;

		case SOUTH_WEST:
		case SOUTH_EAST:
		case SOUTH:
			return -1;

		case WEST:
		case EAST:
		default:
			return 0;
	}
}

/**
 * Print info about an npc spawn, for debugging.
 * @param npc_index Index of the npc spawn in the global array.
 */
void print_npc_spawn_data(int npc_index)
{
	printf("NPC NAME: %s ID: %d X: %d Y: %d\n", 
			NPCS[NPC_SPAWNS[npc_index]->npc_id]->name,
			NPCS[NPC_SPAWNS[npc_index]->npc_id]->id,
			NPC_SPAWNS[npc_index]->world_x,
			NPC_SPAWNS[npc_index]->world_y);
}


/**
 * @brief Checks the line between two points for ranged attack collisions, 
 * checks if a coordinate is passed in the line draw.
 *
 * Uses the Bresenham's line algorithm to find a path from one point
 * to another. This checks if each space can be ranged.  
 * This skips the first and last squares as they are not needed,
 * and returns 0 if something is in the way that can't be shot 
 * through/over.
 *
 * @param x0, y0 Starting position.
 * @param x1, y1 Ending position.
 * @param height Height level of the check, world_z.
 * @param x_check, y_check Position to check if the line draws through.
 * @return 0 if ranged line is blocked.
 * @return 1 if found the check position, and no blockage upto that point.
 */
int is_point_within_line_of_sight(int x0, int y0, int x1, int y1, int height, 
								  int x_check, int y_check)
{
	int first = 0;
	int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
	int err = (dx>dy ? dx : -dy)/2, e2;
	
	for(;;)
	{
		if (first != 0)
		{
			if (x0 == x1 && y0 == y1) 
			{
				break;
			}
			else
			{
				/* If a square has the no projectile mask or the taken flag
				 * applied, end and return 0.  */
				if ((get_collision(x0, y0, height) 
					& PROJ_BLOCKED_MASK)
					|| (get_collision(x0, y0, height) 
					& TAKEN_MASK))
				{
					return 0;
				}

				/* If we hit the coordinates without issue, return 1.  */
				if (x0 == x_check && y0 == y_check)
				{
					return 1;
				}
			}
		}
		e2 = err;

		if (e2 >-dx) 
		{ 
			err -= dy; 
			x0 += sx; 
		}
		if (e2 < dy) 
		{ 
			err += dx; 
			y0 += sy; 
		}
		first = 1;
	}

	/* Something else happened, return not within LOS.  */
	return 0;
}

static int line_of_sight_facing_check(struct client *c, struct npc_spawn *npc)
{
	/* Check basic n s e w direction first.  */
	switch (npc->last_walked_direction)
	{
		case SOUTH:
		case SOUTH_EAST:
		case SOUTH_WEST:
			if (c->plr->world_y >= npc->world_y)
			{
				return 0;
			}
			break;
		case NORTH:
		case NORTH_EAST:
		case NORTH_WEST:
			if (c->plr->world_y <= npc->world_y)
			{
				return 0;
			}
			break;
		case EAST:
			if (c->plr->world_x <= npc->world_x)
			{
				return 0;
			}
			break;
		case WEST:
			if (c->plr->world_x >= npc->world_x)
			{
				return 0;
			}
			break;
	}

	/* Check diagonal directions now.  */
	switch (npc->last_walked_direction)
	{
		case SOUTH_EAST:
		case NORTH_EAST:
			if (c->plr->world_x <= npc->world_x)
			{
				return 0;
			}
			break;

		case SOUTH_WEST:
		case NORTH_WEST:
			if (c->plr->world_x >= npc->world_x)
			{
				return 0;
			}
			break;
	}

	/* If we made it this far, there isn't any issues. NPC could see the 
	 * player based on their direction.  */
	return 1;
}

/**
 * Checks if a player is within an NPC's line of sight.
 * @param *c Client to check.
 * @param *npc NPC spawn to draw line of sight from.
 * @param distance How many spaces from the NPC can they see.
 * @return Returns 0 if not, 1 if client is in the NPC's line of sight.
 */
int check_line_of_sight(struct client *c, struct npc_spawn *npc, int distance)
{
	/* Check if the player could even been seen based on the NPC's facing 
	 * direction. If not, don't waste time doing any more calculations.  */
	if (!line_of_sight_facing_check(c, npc))
	{
		return 0;
	}

	int npc_x[3] = {0};
	int npc_y[3] = {0};
	int distance_x = distance;
	int distance_y = distance;
	int x;

	npc_x[0] = npc->world_x;
	npc_y[0] = npc->world_y;

	/* Check line of sight by casting a ray of 'distance' size from the NPC
	 * straight ahead. For thieving, it appears that the NPC can see at least
	 * one space over to each side. Three rays will be cast. One a space above
	 * the NPC's position, one below, and one on.  */
	switch (npc->last_walked_direction)
	{
		default:
		case SOUTH:
			distance_x = 0;
			distance_y = -distance_y;
			npc_x[1] = npc->world_x + 1;
			npc_y[1] = npc->world_y;
			npc_x[2] = npc->world_x - 1;
			npc_y[2] = npc->world_y;
			break;
		case NORTH:
			distance_x = 0;
			npc_x[1] = npc->world_x + 1;
			npc_y[1] = npc->world_y;
			npc_x[2] = npc->world_x - 1;
			npc_y[2] = npc->world_y;
			break;
		case WEST:
			distance_y = 0;
			distance_x = -distance_x;
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y + 1;
			npc_x[2] = npc->world_x;
			npc_y[2] = npc->world_y - 1;
			break;
		case EAST:
			distance_y = 0;
			distance_x = distance_x;
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y + 1;
			npc_x[2] = npc->world_x;
			npc_y[2] = npc->world_y - 1;
			break;
		/* Diagonal directions are a little more difficult, cannot easily do a 
		 * precheck like the non-diagonal directions. Just draw the lines and
		 * check if the player is within the line.  */
		case SOUTH_EAST:
			distance_y = -distance_y;
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y + 1;
			npc_x[2] = npc->world_x - 1;
			npc_y[2] = npc->world_y;
			break;
		case SOUTH_WEST:
			distance_x = -distance_x;
			distance_y = -distance_y;
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y + 1;
			npc_x[2] = npc->world_x + 1;
			npc_y[2] = npc->world_y;
			break;
		case NORTH_WEST:
			distance_x = -distance_x;
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y - 1;
			npc_x[2] = npc->world_x + 1;
			npc_y[2] = npc->world_y;
			break;
		case NORTH_EAST:
			npc_x[1] = npc->world_x;
			npc_y[1] = npc->world_y - 1;
			npc_x[2] = npc->world_x - 1;
			npc_y[2] = npc->world_y;
			break;

	}

	/* Loop through and see if we've got a match.  */
	for (x = 0; x < 3; x++)
	{
		if (is_point_within_line_of_sight(npc_x[x], npc_y[x], 
									 npc_x[x] + distance_x, 
									 npc_y[x] + distance_y, 
									 c->plr->world_z, 
									 c->plr->world_x, c->plr->world_y))
		{
			/* If no blockage detected, return found in los.  */
			return 1;
		}
	}

	/* If we reached the end, the player was not found in the LOS.  */
	return 0;
}

/**
 * Applies the default male look to a player's character.
 * @param *c Client instance.
 */
void default_look_male(struct client *c)
{
	int x;
	c->plr->sex = 0;
	
	for (x = 0; x < 12; x++)
	{
		if (x == BEARD_LOOK)
		{
			c->plr->player_look[x] = 10;
		}
		else if (x == TORSO_LOOK)
		{
			c->plr->player_look[x] = 18;
		}		
		else if (x == ARMS_LOOK)
		{
			c->plr->player_look[x] = 26;
		}
		else if (x == HANDS_LOOK)
		{
			c->plr->player_look[x] = 33;
		}
		else if (x == LEGS_LOOK)
		{
			c->plr->player_look[x] = 36;
		}
		else if (x == FEET_LOOK)
		{
			c->plr->player_look[x] = 42;
		}
		else
		{
			c->plr->player_look[x] = 0;
		}		
	}
}

/**
 * Applies the default female look to a player's character.
 * @param *c Client instance.
 */
void default_look_female(struct client *c)
{
	int x;
	c->plr->sex = 1;
	
	for (x = 0; x < 12; x++)
	{
		if (x == HAIR_LOOK)
		{
			c->plr->player_look[x] = 45;
		}
		else if (x == BEARD_LOOK)
		{
			c->plr->player_look[x] = 256;
		}
		else if (x == TORSO_LOOK)
		{
			c->plr->player_look[x] = 56;
		}		
		else if (x == ARMS_LOOK)
		{
			c->plr->player_look[x] = 61;
		}
		else if (x == HANDS_LOOK)
		{
			c->plr->player_look[x] = 67;
		}
		else if (x == LEGS_LOOK)
		{
			c->plr->player_look[x] = 70;
		}
		else if (x == FEET_LOOK)
		{
			c->plr->player_look[x] = 79;
		}
		else
		{
			c->plr->player_look[x] = 0;
		}		
	}
}

/**
 * Checks if a player is located within the wilderness.
 * @param *c Client instance.
 * @return 1 if the player is within the wilderness's boundaries,
 * @return 0 if player is not in the wilderness. 
 */
int is_player_in_wilderness(struct client *c)
{
	if ((c->plr->world_x > 2941 && c->plr->world_x < 3392 
		&& c->plr->world_y > 3518 && c->plr->world_y < 3966) 
		|| (c->plr->world_x > 2941 && c->plr->world_x < 3392 
		&& c->plr->world_y > 9918 && c->plr->world_y < 10366)) 
	{
		return 1;
	}
	return 0;
}

/**
 * Using the starting and destination points, returns an id representation of
 * the direction the player is moving. Look at your numpad for reference.
 * @param orig_x, orig_y Starting coordinates.
 * @param dest_x, dest_y Destination coordinates.
 * @return Returns number representing movement direction.
 */
int get_walking_direction(int orig_x, int orig_y, int dest_x, int dest_y)
{
	/* Did not move left or right.  */
	if (orig_x == dest_x)
	{
		/* Moved north.  */
		if (orig_y < dest_y)
		{
			return 8;	
		}
		/* Moved south.  */
		else if (orig_y > dest_y)
		{
			return 2;
		}
	}
	/* Moved to the right.  */
	else if (orig_x < dest_x)
	{
		/* Moved NE.  */
		if (orig_y < dest_y)
		{
			return 9;	
		}
		/* Moved SE.  */
		else if (orig_y > dest_y)
		{
			return 3;
		}
		/* Moved east.  */
		else if (orig_y == dest_y)
		{
			return 6;
		}
	}
	/* Moved to the left.  */
	else if (orig_x > dest_x)
	{
		/* Moved NW.  */
		if (orig_y < dest_y)
		{
			return 7;
		}
		/* Moved SW.  */
		else if (orig_y > dest_y)
		{
			return 1;
		}
		/* Moved west.  */
		else if (orig_y == dest_y)
		{
			return 4;
		}
	}
	/* Did not move at all, should never get here.  */
	return 5;
}

/* Returns false if not blocked, true if is.  */
static int is_blocked(int collision)
{
	if ((collision & OCCUPIED_MASK) == 0)
	{
		return 0; 
	}
	return 1;
}

/* These functions are for npcs/players with a size of 1.  */
static int can_walk_sw(int orig, int dest, int south, int west)
{
	if (((orig & SW_WALL_MASK) == 0) 
		&& ((orig & S_WALL_MASK) == 0)
		&& ((orig & W_WALL_MASK) == 0))
	{
		if (((dest & NE_WALL_MASK) == 0)
			&& ((dest & N_WALL_MASK) == 0)
			&& ((dest & E_WALL_MASK) == 0))
		{
			if (((south & N_WALL_MASK) == 0)
				&& ((south & W_WALL_MASK) == 0)
				&& ((is_blocked(south) == 0)))
			{
				if (((west & S_WALL_MASK) == 0)
					&& ((west & E_WALL_MASK) == 0)
					&& ((is_blocked(west) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_walk_se(int orig, int dest, int south, int east)
{
	if (((orig & SE_WALL_MASK) == 0) 
		&& ((orig & S_WALL_MASK) == 0)
		&& ((orig & E_WALL_MASK) == 0))
	{
		if (((dest & NW_WALL_MASK) == 0)
			&& ((dest & N_WALL_MASK) == 0)
			&& ((dest & W_WALL_MASK) == 0))
		{
			if (((south & N_WALL_MASK) == 0)
				&& ((south & E_WALL_MASK) == 0)
				&& ((is_blocked(south) == 0)))
			{
				if (((east & S_WALL_MASK) == 0)
					&& ((east & W_WALL_MASK) == 0)
					&& ((is_blocked(east) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_walk_ne(int orig, int dest, int north, int east)
{
	if (((orig & NE_WALL_MASK) == 0) 
		&& ((orig & N_WALL_MASK) == 0)
		&& ((orig & E_WALL_MASK) == 0))
	{
		if (((dest & SW_WALL_MASK) == 0)
			&& ((dest & S_WALL_MASK) == 0)
			&& ((dest & W_WALL_MASK) == 0))
		{
			if (((north & S_WALL_MASK) == 0)
				&& ((north & E_WALL_MASK) == 0)
				&& ((is_blocked(north) == 0)))
			{
				if (((east & N_WALL_MASK) == 0)
					&& ((east & W_WALL_MASK) == 0)
					&& ((is_blocked(east) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_walk_nw(int orig, int dest, int north, int west)
{
	if (((orig & NW_WALL_MASK) == 0) 
		&& ((orig & N_WALL_MASK) == 0)
		&& ((orig & W_WALL_MASK) == 0))
	{
		if (((dest & SE_WALL_MASK) == 0)
			&& ((dest & S_WALL_MASK) == 0)
			&& ((dest & E_WALL_MASK) == 0))
		{
			if (((north & S_WALL_MASK) == 0)
				&& ((north & W_WALL_MASK) == 0)
				&& ((is_blocked(north) == 0)))
			{
				if (((west & N_WALL_MASK) == 0)
					&& ((west & E_WALL_MASK) == 0)
					&& ((is_blocked(west) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

/* These functions are used only when checking movement for larger sized 
   npcs or players.  */
static int can_walk_sw_big(int orig, int dest)
{
	if (((orig & SW_WALL_MASK) == 0) 
		&& ((orig & S_WALL_MASK) == 0)
		&& ((orig & W_WALL_MASK) == 0))
	{
		if (((dest & NE_WALL_MASK) == 0)
			&& ((dest & N_WALL_MASK) == 0)
			&& ((dest & E_WALL_MASK) == 0))
		{
			return 1;
		}
	}
	return 0;
}

static int can_walk_se_big(int orig, int dest)
{
	if (((orig & SE_WALL_MASK) == 0) 
		&& ((orig & S_WALL_MASK) == 0)
		&& ((orig & E_WALL_MASK) == 0))
	{
		if (((dest & NW_WALL_MASK) == 0)
			&& ((dest & N_WALL_MASK) == 0)
			&& ((dest & W_WALL_MASK) == 0))
		{
			return 1;
		}
	}
	return 0;
}

static int can_walk_ne_big(int orig, int dest)
{
	if (((orig & NE_WALL_MASK) == 0) 
		&& ((orig & N_WALL_MASK) == 0)
		&& ((orig & E_WALL_MASK) == 0))
	{
		if (((dest & SW_WALL_MASK) == 0)
			&& ((dest & S_WALL_MASK) == 0)
			&& ((dest & W_WALL_MASK) == 0))
		{
			return 1;
		}
	}
	return 0;
}

static int can_walk_nw_big(int orig, int dest)
{
	if (((orig & NW_WALL_MASK) == 0) 
		&& ((orig & N_WALL_MASK) == 0)
		&& ((orig & W_WALL_MASK) == 0))
	{
		if (((dest & SE_WALL_MASK) == 0)
			&& ((dest & S_WALL_MASK) == 0)
			&& ((dest & E_WALL_MASK) == 0))
		{
			return 1;
		}
	}
	return 0;
}

/**
 * @brief Checks if a movement can be done from the orig points to the dest.
 * Checks if something can walk to a square from a direction traveling.  
 * X and Y are where the object wants to move to, direction is from where
 * the object will be traveling.
 * @param orig_x, orig_y, orig_z Position of the starting point.
 * @param dest_x, dest_y Position of the destination. Shares z pos with orig.
 * @param is_npc 1 if we are checking for a npc's movement, otherwise for a
 * player.
 * @return 0 if way is blocked.
 * @return 1 if can move.
 * @see can_walk_in_direction_big Variable entity size version.
 */
int can_walk_in_direction(int orig_x, int orig_y, int orig_z,
						  int dest_x, int dest_y, int is_npc) 
{
	/* Quick check to see if the destination is completely unwalkable. If so,
	   return 0.  */
	int orig_collision = get_collision(orig_x, orig_y, orig_z);
	int dest_collision = get_collision(dest_x, dest_y, orig_z);

	int direction = get_walking_direction(orig_x, orig_y, dest_x, dest_y);

	/* NPCs cannot go onto spaces where the TAKEN_MASK is set, but players 
	   can.  */
	if (is_npc == 1)
	{
		if (is_blocked(dest_collision) == 1
			|| (dest_collision & TAKEN_MASK) != 0)
		{
			return 0;
		}
	}
	else
	{
		if (is_blocked(dest_collision) == 1)
		{
			return 0;
		}
	}

	/* Note that diagonal directions also check the four-directional 
	   equivilent. This is because some things only have collision for the four
	   directions, but are missing diagonal checking. This means a wall that is
	   blocking west can be moved through by going nw or sw, as there is 
	   nothing saying that specifically those ways are blocked.  */

	if (direction == SOUTH_WEST)
	{
		int south_collision = get_collision(orig_x, orig_y - 1, orig_z);
		int west_collision = get_collision(orig_x - 1, orig_y, orig_z);

		if (can_walk_sw(orig_collision, dest_collision, 
						south_collision, west_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == SOUTH)
	{
		if ((orig_collision & S_WALL_MASK) == 0)
		{
			if ((dest_collision & N_WALL_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == SOUTH_EAST)
	{
		int south_collision = get_collision(orig_x, orig_y - 1, orig_z);
		int east_collision = get_collision(orig_x + 1, orig_y, orig_z);

		if (can_walk_se(orig_collision, dest_collision, 
						south_collision, east_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == WEST)
	{
		if ((orig_collision & W_WALL_MASK) == 0)
		{
			if ((dest_collision & E_WALL_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == EAST)
	{
		if ((orig_collision & E_WALL_MASK) == 0)
		{
			if ((dest_collision & W_WALL_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == NORTH_WEST)
	{
		int north_collision = get_collision(orig_x, orig_y + 1, orig_z);
		int west_collision = get_collision(orig_x - 1, orig_y, orig_z);

		if (can_walk_nw(orig_collision, dest_collision, 
						north_collision, west_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == NORTH)
	{
		if ((orig_collision & N_WALL_MASK) == 0)
		{
			if ((dest_collision & S_WALL_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == NORTH_EAST)
	{
		int north_collision = get_collision(orig_x, orig_y + 1, orig_z);
		int east_collision = get_collision(orig_x + 1, orig_y, orig_z);

		if (can_walk_ne(orig_collision, dest_collision, 
						north_collision, east_collision) == 1)
		{
			return 1;
		}
	}
	return 0;
}

/**
 * @brief Checks if a movement can be done from the orig points to the dest.
 * This one takes into account entities larger than 1x1 square.
 *
 * Checks if something can walk to a square from a certain starting point.  
 * X and Y are where the object wants to move to, direction is from where
 * the object will be traveling. Checks collision, and size of entity.
 * @param orig_x, orig_y, orig_z Position of the starting point.
 * @param dest_x, dest_y Position of the destination. Shares z pos with orig.
 * @param is_npc 1 if we are checking for a npc's movement, otherwise for a
 * player.
 * @param size Size of the entity in squares. This is for both x and y size.
 * @return 0 if way is blocked.
 * @return 1 if can move.
 * @see can_walk_in_direction 1x1 Size version.
 */
int can_walk_in_direction_big(int orig_x, int orig_y, int orig_z, 
							  int dest_x, int dest_y, int is_npc, int size) 
{
	/* Need to store at least two sides of npc collision for diagonal movement.
	   For example, store data on the collision for the North and West side
	   if they are moving North West.  */
	int store_size = ((size * 2) - 1);
	int interior_size = size - 1;
	/* Arrays holding all the collision.  */
	int *start_collision = malloc(store_size * sizeof(int));
	int *end_collision = malloc(store_size * sizeof(int));
	/* Store how much of the collision arrays are actually used.  */
	int start_collision_index = 0;
	int end_collision_index = 0;
	/* Used in the final check to reduce code reuse.  */
	int good_value = 1;
	
	/* In case special handling needs to be done, use these.  */
	int adjusted_orig_x = 0;
	int adjusted_orig_y = 0;
	int adjusted_dest_x = 0;
	int adjusted_dest_y = 0;

	/* For loop.  */
	int x = 0, y = 0;

	int direction = get_walking_direction(orig_x, orig_y, dest_x, dest_y);

	switch (direction)
	{
		case SOUTH_WEST:
		case SOUTH:
		case WEST:
			adjusted_orig_x = orig_x;
			adjusted_orig_y = orig_y;
			adjusted_dest_x = dest_x;
			adjusted_dest_y = dest_y;
			break;
		case SOUTH_EAST:
		case EAST:
			adjusted_orig_x = (orig_x + interior_size);
			adjusted_orig_y = orig_y;
			adjusted_dest_x = (dest_x + interior_size);
			adjusted_dest_y = dest_y;
			break;
		case NORTH:
		case NORTH_EAST:
			adjusted_orig_x = (orig_x + interior_size);
			adjusted_orig_y = (orig_y + interior_size);
			adjusted_dest_x = (dest_x + interior_size);
			adjusted_dest_y = (dest_y + interior_size);
			break;
		case NORTH_WEST:
			adjusted_orig_x = orig_x;
			adjusted_orig_y = (orig_y + interior_size);
			adjusted_dest_x = dest_x;
			adjusted_dest_y = (dest_y + interior_size);
			break;
	}

	/* Debug check.  */
	/*
	printf("direction: %d\n", direction);
	printf("orig_x: %d adj_x: %d\n", orig_x, adjusted_orig_x);
	printf("orig_y: %d adj_y: %d\n", orig_y, adjusted_orig_y);
	printf("dest_x: %d adj_x: %d\n", dest_x, adjusted_dest_x);
	printf("dest_y: %d adj_y: %d\n", dest_y, adjusted_dest_y);
	*/

	/* Get collision of spaces we are moving from.  */
	for (x = orig_x; x < (orig_x + size); x++)
	{
		for (y = orig_y; y < (orig_y + size); y++)
		{
			if (direction == SOUTH_WEST || direction == SOUTH_EAST 
				|| direction == NORTH_WEST || direction == NORTH_EAST)
			{
				if (y == adjusted_orig_y || x == adjusted_orig_x)
				{
					start_collision[start_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
			else if (direction == SOUTH || direction == NORTH)
			{
				if (y == adjusted_orig_y)
				{
					start_collision[start_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
			else if (direction == WEST || direction == EAST)
			{
				if (x == adjusted_orig_x)
				{
					start_collision[start_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
		}
	}

	/* Get collision of spaces we will move to.  */
	for (x = dest_x; x < (dest_x + size); x++)
	{
		for (y = dest_y; y < (dest_y + size); y++)
		{
			if (direction == SOUTH_WEST || direction == SOUTH_EAST 
				|| direction == NORTH_WEST || direction == NORTH_EAST)
			{
				if (y == adjusted_dest_y || x == adjusted_dest_x)
				{
					end_collision[end_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
			else if (direction == SOUTH || direction == NORTH)
			{
				if (y == adjusted_dest_y)
				{
					end_collision[end_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
			else if (direction == WEST || direction == EAST)
			{
				if (x == adjusted_dest_x)
				{
					end_collision[end_collision_index++] 
						= get_collision(x, y, orig_z);
				}
			}
		}
	}

	/* Check for any errors in array size.  */
	if (start_collision_index != end_collision_index)
	{
		printf("Collision index mismatch!\n");
		printf("\tStart: %d End: %d\n", start_collision_index, 
			   end_collision_index);
		
		free(end_collision);
		free(start_collision);
		return 0;
	}


	/* Debug printing of collision.  */
	/*
	for (x = 0; x < start_collision_index; x++)
	{
		printf("Start: %d Index: %d\n", start_collision[x], x);
	}

	for (x = 0; x < end_collision_index; x++)
	{
		printf("End: %d Index: %d\n", end_collision[x], x);
	}
	*/

	/* Check if any of the destination squares are blocked, if so, return.  */
	if (is_npc == 1)
	{
		for (x = 0; x < end_collision_index; x++)
		{
			if (is_blocked(end_collision[x]) == 1
				|| (end_collision[x] & TAKEN_MASK) != 0)
			{
				free(end_collision);
				free(start_collision);
				return 0;
			}
		}
	}
	else
	{
		for (x = 0; x < end_collision_index; x++)
		{
			if (is_blocked(end_collision[x]) == 1)
			{
				free(end_collision);
				free(start_collision);
				return 0;
			}
		}
	}

	/* Now, check if the npc can walk that direction.  */
	switch (direction)
	{
		case SOUTH_WEST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (can_walk_sw_big(start_collision[x], end_collision[x]) != 1)
				{
					good_value = 0;
					break;
				}
			}
			break;
		case SOUTH:
			for (x = 0; x < start_collision_index; x++)
			{
				if (((start_collision[x] & S_WALL_MASK) != 0) 
					|| ((end_collision[x] & N_WALL_MASK) != 0))
				{
					good_value = 0;
					break;
				}
			}
			break;
		case SOUTH_EAST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (can_walk_se_big(start_collision[x], end_collision[x]) != 1)
				{
					good_value = 0;
					break;
				}
			}
			break;
		case WEST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (((start_collision[x] & W_WALL_MASK) != 0) 
					|| ((end_collision[x] & E_WALL_MASK) != 0))
				{
					good_value = 0;
					break;
				}
			}
			break;
		case EAST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (((start_collision[x] & E_WALL_MASK) != 0) 
					|| ((end_collision[x] & W_WALL_MASK) != 0))
				{
					good_value = 0;
					break;
				}
			}
			break;
		case NORTH_WEST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (can_walk_nw_big(start_collision[x], end_collision[x]) != 1)
				{
					good_value = 0;
					break;
				}
			}
			break;
		case NORTH:
			for (x = 0; x < start_collision_index; x++)
			{
				if (((start_collision[x] & N_WALL_MASK) != 0) 
					|| ((end_collision[x] & S_WALL_MASK) != 0))
				{
					good_value = 0;
					break;
				}
			}
			break;
		case NORTH_EAST:
			for (x = 0; x < start_collision_index; x++)
			{
				if (can_walk_ne_big(start_collision[x], end_collision[x]) != 1)
				{
					good_value = 0;
					break;
				}
			}
			break;
	}

	free(end_collision);
	free(start_collision);

	if (good_value == 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

/* Returns false if not blocked, true if is.  */
static int is_ranged_blocked(int collision)
{
	if ((collision & PROJ_BLOCKED_MASK) == 0)
	{
		return 0; 
	}
	return 1;
}

/* These functions are for npcs/players with a size of 1.  */
static int can_range_sw(int orig, int dest, int south, int west)
{
	if (((orig & SW_BLOCKED_MASK) == 0) 
		&& ((orig & S_BLOCKED_MASK) == 0)
		&& ((orig & W_BLOCKED_MASK) == 0))
	{
		if (((dest & NE_BLOCKED_MASK) == 0)
			&& ((dest & N_BLOCKED_MASK) == 0)
			&& ((dest & E_BLOCKED_MASK) == 0))
		{
			if (((south & N_BLOCKED_MASK) == 0)
				&& ((south & W_BLOCKED_MASK) == 0)
				&& ((is_blocked(south) == 0)))
			{
				if (((west & S_BLOCKED_MASK) == 0)
					&& ((west & E_BLOCKED_MASK) == 0)
					&& ((is_blocked(west) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_range_se(int orig, int dest, int south, int east)
{
	if (((orig & SE_BLOCKED_MASK) == 0) 
		&& ((orig & S_BLOCKED_MASK) == 0)
		&& ((orig & E_BLOCKED_MASK) == 0))
	{
		if (((dest & NW_BLOCKED_MASK) == 0)
			&& ((dest & N_BLOCKED_MASK) == 0)
			&& ((dest & W_BLOCKED_MASK) == 0))
		{
			if (((south & N_BLOCKED_MASK) == 0)
				&& ((south & E_BLOCKED_MASK) == 0)
				&& ((is_blocked(south) == 0)))
			{
				if (((east & S_BLOCKED_MASK) == 0)
					&& ((east & W_BLOCKED_MASK) == 0)
					&& ((is_blocked(east) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_range_ne(int orig, int dest, int north, int east)
{
	if (((orig & NE_BLOCKED_MASK) == 0) 
		&& ((orig & N_BLOCKED_MASK) == 0)
		&& ((orig & E_BLOCKED_MASK) == 0))
	{
		if (((dest & SW_BLOCKED_MASK) == 0)
			&& ((dest & S_BLOCKED_MASK) == 0)
			&& ((dest & W_BLOCKED_MASK) == 0))
		{
			if (((north & S_BLOCKED_MASK) == 0)
				&& ((north & E_BLOCKED_MASK) == 0)
				&& ((is_blocked(north) == 0)))
			{
				if (((east & N_BLOCKED_MASK) == 0)
					&& ((east & W_BLOCKED_MASK) == 0)
					&& ((is_blocked(east) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

static int can_range_nw(int orig, int dest, int north, int west)
{
	if (((orig & NW_BLOCKED_MASK) == 0) 
		&& ((orig & N_BLOCKED_MASK) == 0)
		&& ((orig & W_BLOCKED_MASK) == 0))
	{
		if (((dest & SE_BLOCKED_MASK) == 0)
			&& ((dest & S_BLOCKED_MASK) == 0)
			&& ((dest & E_BLOCKED_MASK) == 0))
		{
			if (((north & S_BLOCKED_MASK) == 0)
				&& ((north & W_BLOCKED_MASK) == 0)
				&& ((is_blocked(north) == 0)))
			{
				if (((west & N_BLOCKED_MASK) == 0)
					&& ((west & E_BLOCKED_MASK) == 0)
					&& ((is_blocked(west) == 0)))
				{
					return 1;
				}
			}
		}
	}
	return 0;
}

/**
 * @brief Checks if a ranged attack can be done from two points.
 * Checks if a projectile can travel to a square from a direction.  
 * X and Y are where the projectile wants to move to, direction is from where
 * the projectile will be traveling.
 * @param orig_x, orig_y, orig_z Position of the starting point.
 * @param dest_x, dest_y Position of the destination. Shares z pos with orig.
 * @return 0 if way is blocked.
 * @return 1 if can shoot.
 * @bug May not be accurate, think only the ranged block flag matters,
 * but the can_range_x functions check other masks.
 */
int can_range_in_direction(int orig_x, int orig_y,
						   int dest_x, int dest_y, int orig_z)
{
	/* Quick check to see if the destination is completely unwalkable. If so,
	   return 0.  */
	int orig_collision = get_collision(orig_x, orig_y, orig_z);
	int dest_collision = get_collision(dest_x, dest_y, orig_z);

	/* Handle edge cases where the dest and orig are the same. Shouldn't
	 * normally happen, but does when an NPC is attacking a player on their
	 * square.  This ensures that NPC's can continue to attack the player if
	 * this is the case.  */ 
	if (orig_x == dest_x && orig_y == dest_y)
	{
		if (is_ranged_blocked(dest_collision))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
		
	int direction = get_walking_direction(orig_x, orig_y, dest_x, dest_y);

	if (is_ranged_blocked(dest_collision))
	{
		return 0;
	}

	/* Note that diagonal directions also check the four-directional 
	   equivilent. This is because some things only have collision for the four
	   directions, but are missing diagonal checking. This means a wall that is
	   blocking west can be moved through by going nw or sw, as there is 
	   nothing saying that specifically those ways are blocked.  */

	if (direction == SOUTH_WEST)
	{
		int south_collision = get_collision(orig_x, orig_y - 1, orig_z);
		int west_collision = get_collision(orig_x - 1, orig_y, orig_z);

		if (can_range_sw(orig_collision, dest_collision, 
						south_collision, west_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == SOUTH)
	{
		if ((orig_collision & S_BLOCKED_MASK) == 0)
		{
			if ((dest_collision & N_BLOCKED_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == SOUTH_EAST)
	{
		int south_collision = get_collision(orig_x, orig_y - 1, orig_z);
		int east_collision = get_collision(orig_x + 1, orig_y, orig_z);

		if (can_range_se(orig_collision, dest_collision, 
						south_collision, east_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == WEST)
	{
		if ((orig_collision & W_BLOCKED_MASK) == 0)
		{
			if ((dest_collision & E_BLOCKED_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == EAST)
	{
		if ((orig_collision & E_BLOCKED_MASK) == 0)
		{
			if ((dest_collision & W_BLOCKED_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == NORTH_WEST)
	{
		int north_collision = get_collision(orig_x, orig_y + 1, orig_z);
		int west_collision = get_collision(orig_x - 1, orig_y, orig_z);

		if (can_range_nw(orig_collision, dest_collision, 
						north_collision, west_collision) == 1)
		{
			return 1;
		}
	}
	else if (direction == NORTH)
	{
		if ((orig_collision & N_BLOCKED_MASK) == 0)
		{
			if ((dest_collision & S_BLOCKED_MASK) == 0)
			{
				return 1;
			}
		}
	}
	else if (direction == NORTH_EAST)
	{
		int north_collision = get_collision(orig_x, orig_y + 1, orig_z);
		int east_collision = get_collision(orig_x + 1, orig_y, orig_z);

		if (can_range_ne(orig_collision, dest_collision, 
						north_collision, east_collision) == 1)
		{
			return 1;
		}
	}
	return 0;
}

/**
 * Gets all the squares in a straight line that the player will have to walk
 * through in order to reach a destination. Does only a straight line,
 * does not do astar or any more advanced pathfinding.
 * @param start_x, start_y Starting coordinates.
 * @param end_x, end_y Ending coordinates.
 * @param height Height level of the character.
 * @param results_x[], results_y[] Arrays to store the results in.
 * @param results_size Size of the results_x and results_y arrays.
 * @param ignore_collision 1 to ignore collision, 0 to test for it.
 * @param include_last 1 to include the destination square in the results, 0
 * to exclude it.
 * @return Returns number of results added.
 */
int get_straight_line_path_to_point(int start_x, int start_y,
									int end_x, int end_y, int height,
									int results_x[], int results_y[], 
									int results_size,
									int ignore_collision, int include_last)
{
	int results_index = 0;
	int dx = abs(end_x - start_x);
	int sx = start_x < end_x ? 1 : -1;
	int dy = abs(end_y - start_y);
	int sy = start_y < end_y ? 1 : -1; 

	int err = (dx > dy ? dx : -dy) / 2;
	int e2;
	int first = 0;
	int z;

	for (z = 0; ; z++)
	{
		/* This is so that the square the npc is currently on is skipped.  */
		if (first != 0)
		{
			/* If we are at the destination square, do we include it into the
			 * results arrays?  */
			if (start_x == end_x && start_y == end_y) 
			{
				if (include_last == 1)
				{
					if (((get_collision(start_x, start_y, height) &OCCUPIED_MASK) != 0
						|| (get_collision(start_x, start_y, height) & OCCUPIED_MASK) != 0
						|| (get_collision(start_x, start_y, height) & TAKEN_MASK) != 0)
						&& ignore_collision == 0)
					{
						return results_index;
					}

					if (results_index < results_size)
					{
						results_x[results_index] = start_x;
						results_y[results_index] = start_y;
						results_index++;
					}
					else
					{
						printf("Results too large for array!\n");
						printf("Given size: %d Index: %d\n", results_size, 
							results_index);
						return results_index;
					}
				}
				break;
			}
			else
			{
				/* If a square in the npc's path is blocked, or closed,
				   end and return 1.  */
				if (((get_collision(start_x, start_y, height) &OCCUPIED_MASK) != 0
					|| (get_collision(start_x, start_y, height) & OCCUPIED_MASK) != 0
					|| (get_collision(start_x, start_y, height) & TAKEN_MASK) != 0)
					&& ignore_collision == 0)
				{
					return results_index;
				}
			}

			if (results_index < results_size)
			{
				results_x[results_index] = start_x;
				results_y[results_index] = start_y;
				results_index++;
			}
			else
			{
				printf("Results too large for array!\n");
				printf("Given size: %d Index: %d\n", results_size, 
					results_index);
				return results_index;
			}
		}
		e2 = err;

		if (e2 > -dx) 
		{
			err -= dy;
			start_x += sx;
		}

		if (e2 < dy) 
		{
			err += dx;
			start_y += sy;
		}
		first = 1;
	}

	return results_index;
}

/**
 * Checks char to see if it is an alphanumeric character.
 * @param letter Char to check.
 * @return Returns 1 if a char is an alphanumeric character.
 */
int is_alphanum(char letter)
{
	if ((letter < 123 && letter > 96) || 
	    (letter < 91 && letter > 64) ||
		(letter < 58 && letter > 47))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/**
 * Gets total server uptime.
 * @return Returns total uptime.
 */
int get_uptime_seconds(void)
{
	return (int) ((float) CURRENT_TICK * ((float) TICKRATE / 1000.0));
}

/** 
 * Converts a char to its lowercase version.
 * @param letter Char to change case of.
 * @return Returns the lowercase version of a letter.
 */
char to_lower(char letter)
{
	if (letter < 91 && letter > 64)
	{
		letter += 32;
	}
	return letter;
}

/**
 * Takes a string and converts all characters in it to their lowercase version.
 * @param *string Pointer to the string.
 * @return Returns the string pointer.
 */
char *string_to_lower(char *string)
{
	int x;

	for (x = 0; string[x] != '\0'; x++)
	{
		string[x] = to_lower(string[x]);
	}
	return string;
}

char *return_lowercase_string(char *string)
{
	char *return_string = NULL;
	int string_size = strlen(string) + 1;

	return_string = calloc(string_size, sizeof(char));

	return_string = strcpy(return_string, string);

	return string_to_lower(return_string);
}

/**
 * Takes a string and converts all characters in it to their lowercase version,
 * but will only go up to limit characters in the string.
 * @param *string Pointer to the string.
 * @param limit Max number of characters to convert.
 * @return Returns the string pointer.
 */
char *string_to_lower_num(char *string, int limit)
{
	int x;

	for (x = 0; x < limit; x++)
	{
		string[x] = to_lower(string[x]);
	}
	return string;
}

/**
 * Returns the string version of a direction.
 * @param direction Direction to convert.
 * @return Returns the 2 letter version of the direction.
 */
char *get_direction_to_string(int direction)
{
	switch (direction)
	{
		default:
		case NORTH:
			return "N";
		case SOUTH:
			return "S";
		case EAST:
			return "E";
		case WEST:
			return "W";
		case NORTH_EAST:
			return "NE";
		case SOUTH_EAST:
			return "SE";
		case NORTH_WEST:
			return "NW";
		case SOUTH_WEST:
			return "SW";
	}
}

/**
 * Takes a name of something as the first argument, and returns the proper
 * article for it. Checks if the name starts with a vowel or not.
 * @param *to_check Name of the thing to check.
 * @param *article String to place the correct article in.
 * @param size Size of the *article string.
 */
void get_a_or_an(char *to_check, char *article, int size)
{
	if (size < 3)
	{
		return;
	}

	switch (to_check[0])
	{
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
		case 'A':
		case 'E':
		case 'I':
		case 'O':
		case 'U':
			strncpy(article, "an", 3);
			break;
		default:
			strncpy(article, "a", 3);
			break;
	}
}

/** 
 * Converts a char to its uppercase version.
 * @param letter Char to change case of.
 * @return Returns the uppercase version of a letter.
 */
char to_upper(char letter)
{
	if (letter < 123 && letter > 96)
	{
		letter -= 32;
	}
	return letter;
}

/**
 * Checks if two doubles are about equal, using the epsilon passed.
 * @param v1 Double to check
 * @param v2 Other double to check
 * @param epsilon Error margin to use for the check
 * @return Returns 0 if false, 1 if true.
 */
int is_double_about_equal(double v1, double v2, double epsilon)
{
	return fabs(v1 - v2) < epsilon;
}

/**
 * Checks if the attacker is within combat distance from a target of varying 
 * size.
 * @param t_x, t_y Target coordinates.
 * @param t_size Target's size.
 * @param a_x, a_y Attacker's coordinates.
 * @param a_distance Attacker's combat range.
 *
 * @return 0 when not within range.
 * @return 1 when within range.
 * @return 2 when attacker is INSIDE the target's spaces.
 *
 * @todo Allow for both the attacker and defender to have sizes that are 
 * non-zero, and for that to be accounted for.
 */
int is_within_combat_range(int t_x, int t_y, int t_size, int a_x, int a_y,
						int a_distance)
{
	int internal_size = (t_size - 1);

	int npc_internal_x_bound = (t_x + internal_size);
	int npc_internal_y_bound = (t_y + internal_size);

	int range_sw_x = (t_x - a_distance);
	int range_sw_y = (t_y - a_distance);

	int range_ne_x = (t_x + internal_size + a_distance);
	int range_ne_y = (t_y + internal_size + a_distance);


	/* Check if attacker is inside the target squares.  */
	if (a_x >= t_x && a_x <= npc_internal_x_bound)
	{
		if (a_y >= t_y && a_y <= npc_internal_y_bound)
		{
			return 2;	
		}
	}

	/* Check if the attacker is within the range.  */
	if (a_x >= range_sw_x && a_x <= range_ne_x)
	{
		if (a_y >= range_sw_y && a_y <= range_ne_y)
		{
			/* Everything is good, return 1.  */
			return 1;
		}
	}

	/* Not within range.  */
	return 0;
}

/**
 * Using the distance formula, returns the distance between two points.
 * @param start_x, start_y Starting positions.
 * @param end_x, end_y Ending positions.
 * @return Distance between the two points.
 */
int get_distance_from_point(int start_x, int start_y, int end_x, int end_y)
{
	/* Is there a better way to do this without sqrt?  */
	return (int) sqrt(pow((start_x - end_x), 2)
			  + pow((start_y - end_y), 2));
}

/**
 * Checks if the two points given are within the distance given.
 * @param start_x, start_y Starting point.
 * @param end_x, end_y Ending point.
 * @param distance Max distance allowed between the two points.
 * @return Returns a 1 if two points are within distance.
 */
int check_good_distance(int start_x, int start_y, int end_x, int end_y, 
						int distance) 
{
	return ((start_x - end_x <= distance && start_x - end_x >= -distance) 
			&& (start_y - end_y <= distance && start_y - end_y >= -distance));
}

/**
 * Checks if the player is within the squares for using a object.
 *
 * Due to how objects are in the game, their size needs to be accounted for in
 * order to have accurate distance checking. This function checks distance,
 * and what direction the object is facing.
 *
 * @param player_x, player_y Player position.
 * @param obj_x, obj_y Object Position.
 * @param obj_size_x, obj_size_y Size of the object.
 * @param index If object is a specifically spawned one, or not.
 * @param face What direction the object is facing.
 * @return 1 if player is within range.
 * @return 0 if player is outside range.
 *
 * @bug May still have some issues, double check.
 */
int check_within_object_range(int player_x, int player_y, int obj_x, int obj_y,
							  int obj_size_x, int obj_size_y, int index, 
							  int face)
{
	int calc_x_start = -1, calc_y_start = -1;
	int calc_x_size = -1, calc_y_size = -1;
	int temp_x = -1, temp_y = -1;

	/* If index doesn't exist or the object is square in size, treat object 
	 * facing as default.  */
	if (index == -1 || (obj_size_x == obj_size_y))
	{
		calc_x_size = (obj_x + obj_size_x);
		calc_y_size = (obj_y + obj_size_y);
		calc_x_start = (obj_x - 1);
		calc_y_start = (obj_y - 1);
	}
	else
	{
		/* Face 0, 2 are essentially treated the same as square objects for
		 * their position adjustment. Faces 1 and 3 have the object's x and y
		 * sizes swapped.  */
		switch (face)
		{
			case OBJECT_FACE_EAST:
			case OBJECT_FACE_WEST:
				calc_x_size = (obj_x + obj_size_x);
				calc_y_size = (obj_y + obj_size_y);
				calc_x_start = (obj_x - 1);
				calc_y_start = (obj_y - 1);
				break;
			case OBJECT_FACE_NORTH:
			case OBJECT_FACE_SOUTH:
				calc_x_size = (obj_x + obj_size_y);
				calc_y_size = (obj_y + obj_size_x);
				calc_x_start = (obj_x - 1);
				calc_y_start = (obj_y - 1);
				break;
		}
	}

	/* Ensure proper handling if obj_size variables are negative.  */
	if (calc_x_start > calc_x_size)
	{
		temp_x = calc_x_start;
		calc_x_start = calc_x_size;
		calc_x_size = temp_x;
	}
	if (calc_y_start > calc_y_size)
	{
		temp_y = calc_y_start;
		calc_y_start = calc_y_size;
		calc_y_size = temp_y;
	}

	if ((player_x >= calc_x_start) && (player_x <= calc_x_size))
	{
		if ((player_y >= calc_y_start) && (player_y <= calc_y_size))
		{
			return 1;
		}
	}

	return 0;
}

/**
 * Checks which double is larger.
 * @param one, two Doubles to compare.
 * @return Returns the higher of the two doubles passed.
 */
double f_max(double one, double two)
{
	if (one > two)
	{
		return one;
	}
	return two;
}

/**
 * Checks which int is larger.
 * @param x, y Integers to compare.
 * @return Returns the higher of the two integers passed.
 */
int get_higher(int x, int y)
{
	if (x > y)
	{
		return x;
	}
	return y;
}

/**
 * Checks which int is smaller.
 * @param x, y Integers to compare.
 * @return Returns the smaller of the two integers passed.
 */
int get_smaller(int x, int y)
{
	if (x > y)
	{
		return y;
	}
	return x;
}

static void free_objects(void)
{
	int x;
	
	for (x = 0; x < PROTOCOL_MAX_OBJECTS; x++)
	{
		if (OBJECTS[x] != NULL)
		{
			free(OBJECTS[x]);
		}
	}

	for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
	{
		if (OBJECT_SPAWNS[x] != NULL)
		{
			free(OBJECT_SPAWNS[x]);
		}
	}
}

static void free_npcs(void)
{
	int x;

	for (x = 0; x < PROTOCOL_MAX_NPCS; x++)
	{
		if (NPCS[x] != NULL)
		{
			free(NPCS[x]);
		}
	}
	for (x = 0; x < NUM_NPC_SPAWNS; x++)
	{
		if (NPC_SPAWNS[x] != NULL)
		{
			free(NPC_SPAWNS[x]);
		}
	}

	free(NPC_SPAWNS);
}

static void free_spell_data(void)
{
	int x;

	for (x = 0; x < NUM_SPELLS; x++)
	{
		if (SPELLS[x] != NULL)
		{
			free(SPELLS[x]);
		}
	}

	free(SPELLS);
}

static void free_interface_data(void)
{
	int x = 0;

	if (INTERFACES == NULL)
	{
		return;
	}

	for (x = 0; x < INTERFACES_SIZE; x++)
	{
		if (INTERFACES[x].children != NULL)
		{
			free(INTERFACES[x].children);
		}
	}

	free(INTERFACES);
}

static void free_loot_tables(void)
{
	int x = 0, y = 0;

	for (x = 0; x < NUM_LOOT_TABLES; x++)
	{
		/* Free all the drops.  */
		for (y = 0; y < LOOT_TABLES[x]->num_drops; y++)
		{
			free(LOOT_TABLES[x]->drops[y]);
		}

		free(LOOT_TABLES[x]->drops);

		/* Free all the tables.  */
		for (y = 0; y < LOOT_TABLES[x]->num_tables; y++)
		{
			free(LOOT_TABLES[x]->tables[y]);
		}

		free(LOOT_TABLES[x]->tables);

		free(LOOT_TABLES[x]);
	}

	free(LOOT_TABLES);
}

static void free_shop_data(void)
{
	int x = 0;

	for (x = 0; x < NUM_SHOPS; x++)
	{
		free(SHOP_DATA[x]);
	}

	free(SHOP_DATA);
}

void free_agility_data(void)
{
	int x = 0;

	for (x = 0; x < NUM_AGILITY_OBSTACLES; x++)
	{
		free(AGILITY_DATA[x]);
	}

	free(AGILITY_DATA);
}

static void free_quest_data(void)
{
	int x = 0;

	for (x = 0; x < PROTOCOL_NUM_QUESTS; x++)
	{
		free(QUEST_DATA[x]);
	}

	free(QUEST_DATA);
}

static void free_music_data(void)
{
	int x = 0;

	for (x = 0; x < PROTOCOL_NUM_SONGS; x++)
	{
		free(MUSIC_DATA[x]);
	}

	free(MUSIC_DATA);
}

static void free_items(void)
{
	int x;

	for (x = 0; x < PROTOCOL_MAX_ITEMS; x++)
	{
		if (ITEMS[x] != NULL)
		{
			free(ITEMS[x]);
		}
	}
	
	for (x = 0; x < MAX_ITEM_DROPS; x++)
	{
		if (ITEM_DROPS[x] != NULL)
		{
			free(ITEM_DROPS[x]);
		}
	}
}


/**
 * Free all server memory allocated for game data, not including player data.
 */
void free_memory(void)
{
	printf("Freeing update block...\n");
	free(UPDATE_BLOCK);
	printf("Freeing map data...\n");
	free_map_data(REGION_LIST, REGION_MAP);
	printf("Freeing item data...\n");
	free_items();
	printf("Freeing object data...\n");
	free_objects();
	printf("Freeing npc data...\n");
	free_npcs();
	printf("Freeing loot tables...\n");
	free_loot_tables();
	printf("Freeing spell data...\n");
	free_spell_data();
	printf("Freeing interface data...\n");
	free_interface_data();
	printf("Freeing shop data...\n");
	free_shop_data();
	printf("Freeing quest data...\n");
	free_quest_data();
	printf("Freeing music data...\n");
	free_music_data();
	printf("Freeing agility skill data...\n");
	free_agility_data();
	printf("All memory freed.\n");
}

/**
 * Checks if an NPC id is valid.
 * @param npc_id ID to check.
 * @return Returns 1 if invalid ID, 0 if valid.
 */
int is_npc_id_invalid(int npc_id)
{
	if (npc_id < 0 || npc_id >= PROTOCOL_MAX_NPCS)
	{
		return 1;
	}
	
	return 0;
}

/**
 * Checks if an NPC index is valid.
 * @param npc_index Index to check.
 * @return Returns 1 if invalid index, 0 if valid.
 */
int is_npc_index_invalid(int npc_index)
{
	if (npc_index < 0 || npc_index > NUM_NPC_SPAWNS)
	{
		return 1;
	}
	
	return 0;
}

/**
 * Checks if an object id is valid.
 * @param object_id ID to check.
 * @return Returns 1 if invalid ID, 0 if valid.
 */
int is_object_id_invalid(int object_id)
{
	if (object_id < 0 || object_id >= PROTOCOL_MAX_OBJECTS)
	{
		return 1;
	}
	
	return 0;
}

/**
 * Checks if an item id is valid.
 * @param item_id ID to check.
 * @return Returns 1 if invalid ID, 0 if valid.
 */
int is_item_id_invalid(int item_id)
{
	if (item_id < 0 || item_id >= PROTOCOL_MAX_ITEMS)
	{
		return 1;
	}

	return 0;
}

/**
 * Checks if a spell index is valid.
 * @param spell_index Index to check.
 * @return Returns 1 if invalid index, 0 if valid.
 */
int is_spell_index_invalid(int spell_index)
{
	if (spell_index < 0 || spell_index >= NUM_SPELLS)
	{
		return 1;
	}

	return 0;
}

/**
 * Checks if a slot number is valid for the interface.
 * @param slot Slot number.
 * @param interface_id Interface id.
 * @return Returns 1 if invalid, 0 if valid.
 */
int is_slot_num_invalid(int slot, int interface_id)
{
	if (interface_id == INTR_INV_PLR_ITEMS 
		|| interface_id == INTR_BANK_PLR_ITEMS
		|| interface_id == INTR_BANK_DEPOSIT_BOX_ITEMS
		|| interface_id == INTR_SHOP_PLR_ITEMS)
	{
		if (slot < 0 || slot >= INV_SIZE)
		{
			return 1;
		}

		return 0;
	}
	else if (interface_id == INTR_EQUIP_PLR_ITEMS)
	{
		if (slot < 0 || slot >= EQUIP_SIZE)
		{
			return 1;
		}

		return 0;
	}
	else if (interface_id == INTR_BANK_ITEMS)
	{
		if (slot < 0 || slot >= PLAYER_BANK_SIZE)
		{
			return 1;
		}

		return 0;
	}
	else if (interface_id == INTR_SHOP_ITEMS)
	{
		if (slot < 0 || slot >= SHOP_SIZE)
		{
			return 1;
		}

		return 0;
	}

	printf("Not handled interface_id! %d\n", interface_id);
	return 1;
}

/**
 * Checks if the player is unable to move currently.
 * @param *c Client to check.
 * @param print_stun_msg 0 - Don't send stun message, 1 do so.
 * @return Returns 1 if frozen, 0 if not.
 */
int is_player_frozen(struct client *c, int print_stun_msg)
{
	if (c->plr->frozen_ticks > 0)
	{
		if (c->plr->frozen_type == EFFECT_FREEZE_STUN)
		{
			if (print_stun_msg)
			{
				packet_send_chatbox_message(c, "You're stunned!\n");
			}
		}
		else if (c->plr->frozen_type == EFFECT_FREEZE_MAGICAL)
		{
			if (print_stun_msg)
			{
				packet_send_chatbox_message(c, "A magical force stops you from moving.");
			}
		}

		return 1;
	}

	return 0;
}

/**
 * Checks if the player is currently casting a spell or not.
 * @param *c Client to check.
 * @return Returns 1 if casting, 0 if not.
 */
int is_player_casting_a_spell(struct client *c)
{
	if (c->plr->is_casting_ticks > 0)
	{
		return 1;
	}

	return 0;
}

/**
 * Checks if the player is unable to move currently, from death or frozen.
 * @param *c Client to check.
 * @param print_stun_msg 0 - Don't send stun message, 1 do so.
 * @return Returns 1 if frozen or dead, 0 if not dead or frozen.
 */
int is_player_dead_or_frozen(struct client *c, int print_stun_msg)
{
	return (is_player_frozen(c, print_stun_msg) || is_player_dead(c)) ? 1: 0;
}

/**
 * Checks if a player is too far from something or not.
 * @param *c Client to check.
 * @param entity_x Item's x coordinate.
 * @param entity_y Item's y coordinate.
 * @param dist Distance away to check.
 * @return Returns 0 if within distance, 1 if not.
 */
int is_player_too_far(struct client *c, int entity_x, int entity_y, int dist)
{
	/* If within distance.  */
	if (check_good_distance(c->plr->world_x, c->plr->world_y, entity_x, 
							entity_y, dist) == 1)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
