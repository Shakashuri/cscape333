/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file rng.c
 * @brief Contains the random number generation routines used by the server.
 */

#include <stdint.h>

/** Holds the seeds used for the random number generators.  */
uint64_t RAND_SEED[4];

/**
 * Returns a random uint64_t using the xorshift128 method.
 * @return Random uint64_t value.
 */
uint64_t xorshift128(void)
{
	/* Algorithm "xor128" from p. 5 of Marsaglia, "Xorshift RNGs" */
	uint32_t s, t = RAND_SEED[3];

	t ^= t << 11;
	t ^= t >> 8;

	RAND_SEED[3] = RAND_SEED[2];
	RAND_SEED[2] = RAND_SEED[1];
	RAND_SEED[1] = s = RAND_SEED[0];

	t ^= s;
	t ^= s >> 19;
	RAND_SEED[0] = t;

	return t;
}

/**
 * Returns a random uint64_t using the xorshift64star method.
 * @return Random uint64_t value.
 */
uint64_t xorshift64star(void)
{
	uint64_t result = RAND_SEED[0];

	result ^= result >> 12; // a
	result ^= result << 25; // b
	result ^= result >> 27; // c
	RAND_SEED[0] = result;

	return result * UINT64_C(0x2545F4914F6CDD1D);
}

static inline uint64_t rotl(const uint64_t x, int k) 
{
	return (x << k) | (x >> (64 - k));
}

/**
 * Returns a random uint64_t using the xoroshiro128plus method.
 * @return Random uint64_t value.
 * @note This is the current PRNG used.
 */
uint64_t xoroshiro128plus(void) 
{
	const uint64_t s0 = RAND_SEED[0];
	uint64_t s1 = RAND_SEED[1];
	const uint64_t result = s0 + s1;

	s1 ^= s0;
	RAND_SEED[0] = rotl(s0, 55) ^ s1 ^ (s1 << 14); /* a, b  */
	RAND_SEED[1] = rotl(s1, 36); /* c  */

	return result;
}

/**
 * Used to seed the values for the main rng we use, above.
 * @param seed Initial seed value, taken from system time.
 * @return Random uint64_t value.
 */
uint64_t splitmix64(int seed) 
{
	uint64_t z = (seed + UINT64_C(0x9E3779B97F4A7C15));
	z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
	z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
	return z ^ (z >> 31);
}

/**
 * Sets up the seeds for the random number generators.
 * @param seed Normally a call to time(NULL) is used as its parameter, but
 * can use anything you'd like.
 */
void initialize_rng(int seed)
{
	/* Seed random number generator.  */
	RAND_SEED[0] = splitmix64(seed);
	RAND_SEED[1] = splitmix64(RAND_SEED[0]);
	RAND_SEED[2] = splitmix64(RAND_SEED[1]);
	RAND_SEED[3] = splitmix64(RAND_SEED[2]);
}

/**
 * Returns a random integer between min and max, including min and max.
 * @param min Minimum value range, lowest "roll".
 * @param max Maximum value range, highest "roll".
 * @return Random number from min to max.
 */
int rand_int(int min, int max)
{
	return (min + xoroshiro128plus() / (UINT64_MAX / (max - min + 1.0) + 1.0));
}

/**
 * Returns a random double between min and max, including min and max.
 * @param min Minimum value range, lowest "roll".
 * @param max Maximum value range, highest "roll".
 * @return Random number from min to max.
 */
double rand_double(double min, double max)
{
	return (min + (xoroshiro128plus() / (UINT64_MAX / (max - min))));
}
