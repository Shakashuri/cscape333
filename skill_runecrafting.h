#ifndef _SKILL_RUNECRAFTING_H_
#define _SKILL_RUNECRAFTING_H_

int get_runecrafting_level_needed(int runecrafting_id);
float get_runecrafting_xp(int runecrafting_id);
float get_runecrafting_combo_xp(int runecrafting_id, int is_higher_altar);
int get_runecrafting_multiplier(int runecrafting_id);
int get_runecrafting_talisman_id(int runecrafting_id);
int get_runecrafting_infused_tiara_id(int runecrafting_id);

void init_runecrafting(struct client *c);
void handle_runecrafting(struct client *c);
void create_combination_rune(struct client *c, int runecrafting_id, 
							 int item_id, int item_slot);
void infuse_tiara(struct client *c, int runecrafting_id, int item_id);

#endif
