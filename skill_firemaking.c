/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
/* FIREMAKING - ID 11.  */

int get_firemaking_level_needed(int firemaking_id)
{
	switch (firemaking_id)
	{
	/* Lighting normal logs.  */
		case FIREMAKING_REGULAR:
			return 1;
		case FIREMAKING_ACHEY:
			return 1;
		case FIREMAKING_OAK:
			return 15;
		case FIREMAKING_WILLOW:
			return 30;
		case FIREMAKING_TEAK:
			return 35;
		case FIREMAKING_MAPLE:
			return 45;
		case FIREMAKING_MAHOGANY:
			return 50;
		case FIREMAKING_YEW:
			return 60;
		case FIREMAKING_MAGIC:
			return 75;
	/* Pyre log - annointing.  */
		/* As far as I can tell, annointing has no level requirements.  */
		case FIREMAKING_ANNOINT_REGULAR:
		case FIREMAKING_ANNOINT_OAK:
		case FIREMAKING_ANNOINT_WILLOW:
		case FIREMAKING_ANNOINT_TEAK:
		case FIREMAKING_ANNOINT_MAPLE:
		case FIREMAKING_ANNOINT_MAHOGANY:
		case FIREMAKING_ANNOINT_YEW:
		case FIREMAKING_ANNOINT_MAGIC:
			return 1;
	/* Pyre log - burning.  */
		case FIREMAKING_PYRE_REGULAR:
			return 5;
		case FIREMAKING_PYRE_OAK:
			return 20;
		case FIREMAKING_PYRE_WILLOW:
			return 35;
		case FIREMAKING_PYRE_TEAK:
			return 40;
		case FIREMAKING_PYRE_MAPLE:
			return 50;
		case FIREMAKING_PYRE_MAHOGANY:
			return 55;
		case FIREMAKING_PYRE_YEW:
			return 65;
		case FIREMAKING_PYRE_MAGIC:
			return 80;
	/* Light sources.  */
		case FIREMAKING_TORCHES:
			return 1;
		case FIREMAKING_CANDLE_LANTERN:
			return 4;
		case FIREMAKING_OIL_LAMP:
			return 12;
		case FIREMAKING_OIL_LANTERN:
			return 26;
		case FIREMAKING_BULLSEYE_LANTERN:
			return 49;
		case FIREMAKING_SAPPHIRE_LANTERN:
			return 49;
		case FIREMAKING_EMERALD_LANTERN:
			return 49;
		case FIREMAKING_MINING_HELMET:
			return 65;
		default:
			printf("Firemaking level for %d not found!\n", firemaking_id);
			return -1;
	}
}

/* There actually isn't any change in firemaking chance based on the logs.
 * Change is based solely on level. 25% chance at level 1, then 100% at 43.
 */
int get_firemaking_chance_base()
{
	return 65;
}

/* There actually isn't any change in firemaking chance based on the logs.
 * Change is based solely on level. 25% chance at level 1, then 100% at 43.
 */
int get_firemaking_chance_max()
{
	return 513;
}

float get_firemaking_xp(int firemaking_id)
{
	switch (firemaking_id)
	{
	/* Lighting normal logs.  */
		case FIREMAKING_REGULAR:
			return 40.0;
		case FIREMAKING_ACHEY:
			return 40.0;
		case FIREMAKING_OAK:
			return 60.0;
		case FIREMAKING_WILLOW:
			return 90.0;
		case FIREMAKING_TEAK:
			return 105.0;
		case FIREMAKING_MAPLE:
			return 135.0;
		case FIREMAKING_MAHOGANY:
			return 157.5;
		case FIREMAKING_YEW:
			return 202.5;
		case FIREMAKING_MAGIC:
			return 303.8;
	/* Pyre log - annointing.  */
		/* As far as I can tell, annointing has no level requirements.  */
		case FIREMAKING_ANNOINT_REGULAR:
		case FIREMAKING_ANNOINT_OAK:
		case FIREMAKING_ANNOINT_WILLOW:
			return 12.0;
		case FIREMAKING_ANNOINT_TEAK:
		case FIREMAKING_ANNOINT_MAPLE:
		case FIREMAKING_ANNOINT_MAHOGANY:
			return 14.0;
		case FIREMAKING_ANNOINT_YEW:
		case FIREMAKING_ANNOINT_MAGIC:
			return 16.0;
	/* Pyre log - burning.  */
		case FIREMAKING_PYRE_REGULAR:
			return 50.0;
		case FIREMAKING_PYRE_OAK:
			return 70.0;
		case FIREMAKING_PYRE_WILLOW:
			return 100.0;
		case FIREMAKING_PYRE_TEAK:
			return 120.0;
		case FIREMAKING_PYRE_MAPLE:
			return 175.0;
		case FIREMAKING_PYRE_MAHOGANY:
			return 210.0;
		case FIREMAKING_PYRE_YEW:
			return 225.0;
		case FIREMAKING_PYRE_MAGIC:
			return 404.5;
	/* Light sources.  */
		case FIREMAKING_TORCHES:
		case FIREMAKING_CANDLE_LANTERN:
		case FIREMAKING_OIL_LAMP:
		case FIREMAKING_OIL_LANTERN:
		case FIREMAKING_BULLSEYE_LANTERN:
		case FIREMAKING_SAPPHIRE_LANTERN:
		case FIREMAKING_EMERALD_LANTERN:
		case FIREMAKING_MINING_HELMET:
			return 0.0;
		default:
			printf("Firemaking xp for %d not found!\n", firemaking_id);
			return 0.0;
	}
}

int get_sacred_oil_by_dose(int doses)
{
	switch (doses)
	{
		case 1:
			return 3436;
		case 2:
			return 3434;
		case 3:
			return 3432;
		case 4:
			return 3430;
		default:
			printf("Invalid doses %d!\n", doses);
			return -1;
	}
}

int get_annointed_log_id(int log_id)
{
	switch (log_id)
	{
		case 1511:
			return 3438;
		case 1521:
			return 3440;
		case 1519:
			return 3442;
		case 6333:
			return 6211;
		case 6332:
			return 6213;
		case 1517:
			return 3444;
		case 1515:
			return 3446;
		case 1513:
			return 3448;
		default:
			printf("Log id %d not found!\n", log_id);
			return -1;
	}
}

/* Gets the id that will be used for funeral pyres for xp, loot handling
   later.  */
int get_pyre_log_id(int log_id)
{
	switch (log_id)
	{
		/* Regular.  */
		case 3438:
			return FIREMAKING_PYRE_REGULAR;
		/* Oak.  */
		case 3440:
			return FIREMAKING_PYRE_OAK;
		/* Willow.  */
		case 3442:
			return FIREMAKING_PYRE_WILLOW;
		/* Teak.  */
		case 6211:
			return FIREMAKING_PYRE_TEAK;
		/* Maple.  */
		case 3444:
			return FIREMAKING_PYRE_MAPLE;
		/* Mahogany.  */
		case 6213:
			return FIREMAKING_PYRE_MAHOGANY;
		/* Yew.  */
		case 3446:
			return FIREMAKING_PYRE_YEW;
		/* Magic.  */
		case 3448:
			return FIREMAKING_PYRE_MAGIC;
		default:
			printf("Log id %d not found!\n", log_id);
			return -1;
	}
}

/* Gets the id that will be used for funeral pyres for xp, loot handling
   later.  */
int get_pyre_remains_id(int remains_id)
{
	switch (remains_id)
	{
		case 3396:
			return FIREMAKING_LOAR_REMAINS;
		case 3398:
			return FIREMAKING_PHRIN_REMAINS;
		case 3400:
			return FIREMAKING_RIYL_REMAINS;
		case 3402:
			return FIREMAKING_ASYN_REMAINS;
		case 3404:
			return FIREMAKING_FIYR_REMAINS;
		default:
			printf("Remains id %d not found!\n", remains_id);
			return -1;
	}
}

static int get_firemaking_movement(struct client *c)
{
	int pos_x[4] = {-1, 1, 0, 0};
	int pos_y[4]= {0, 0, -1, 1};
	int x = 0;

	for (x = 0; x < 4; x++)
	{
		if (can_walk_in_direction(c->plr->world_x, c->plr->world_y, 
								  c->plr->world_z, 
								  c->plr->world_x + pos_x[x], 
								  c->plr->world_y + pos_y[x],
								  0) == 1
			&& get_object_spawn_index(c->plr->world_x + pos_x[x], 
									c->plr->world_y + pos_y[x], 
									c->plr->world_z) == -1)
		{
			return x;
		}
	}
	return -1;
}

static void set_firemaking_movement(struct client *c, int dir)
{
	/* Note that dir here is based off of the function above, 
	 * get_firemaking_movement, not on any standard enum.  */
	switch (dir)
	{
		/* West.  */
		case 0:
			player_move_one_square(c, c->plr->world_x - 1, c->plr->world_y);
			player_turn_to_world_coord(c, c->plr->world_x, c->plr->world_y);
			break;	
		/* East.  */
		case 1:
			player_move_one_square(c, c->plr->world_x + 1, c->plr->world_y);
			player_turn_to_world_coord(c, c->plr->world_x, c->plr->world_y);
			break;	
		/* South.  */
		case 2:
			player_move_one_square(c, c->plr->world_x, c->plr->world_y - 1);
			player_turn_to_world_coord(c, c->plr->world_x, c->plr->world_y);
			break;	
		/* North.  */
		case 3:
			player_move_one_square(c, c->plr->world_x, c->plr->world_y + 1);
			player_turn_to_world_coord(c, c->plr->world_x, c->plr->world_y);
			break;
	}
}

/**
 * Handle using sacred oil on logs to create pyre logs.
 * @param *c Client instance.
 * @param oil_id ID of the oil used.
 * @param oil_slot Slot where the oil vial is in the inventory.
 * @param logs_id ID of the logs used.
 * @param logs_slot Slot where the logs are in the inventory.
 * @param firemaking_id ID of the task being done.
 */
void anoint_logs(struct client *c, int oil_id, int oil_slot, int logs_id,
				  int logs_slot, int firemaking_id)
{
	int required_level = get_firemaking_level_needed(firemaking_id);
	int firemaking_level = get_player_skill_level(c, FIREMAKING_SKILL);
	int doses, needed_doses, doses_left = 0;
	char msg[100] = {0};

	/* Pretty sure that there is no level requirement for anointing logs,
	   but keep this here for consistancy and in case they do need it in later
	   revisions.  */
	if (firemaking_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "firemaking to do that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Oil id, how many doses it has.  */
	switch (oil_id)
	{
		case 3430:
			doses = 4;
			break;
		case 3432:
			doses = 3;
			break;
		case 3434:
			doses = 2;
			break;
		case 3436:
			doses = 1;
			break;
		default:
			doses = -1;
			break;
	}

	/* Log ids, and how many doses of sacred oil they require.  */
	switch (logs_id)
	{
		/* Regular.  */
		case 1511:
		/* Oak.  */
		case 1521:
			needed_doses = 2;
			break;
		/* Willow.  */
		case 1519:
		/* Teak.  */
		case 6333:
		/* Mahogany.  */
		case 6332:
		/* Maple.  */
		case 1517:
			needed_doses = 3;
			break;
		/* Yew.  */
		case 1515:
		/* Magic.  */
		case 1513:
			needed_doses = 4;
			break;
		default:
			needed_doses = -1;
			break;
	}

	/* Error checking.  */
	if (doses == -1 || needed_doses == -1)
	{
		printf("Invalid ids for log annointing!\n");
		printf("Oil id: %d doses: %d\n", oil_id, doses);
		printf("Log id: %d doses needed: %d\n", logs_id, needed_doses);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Check if player has the needed doses.  */
	if (doses < needed_doses)
	{
		snprintf(msg, 100, "You need at least %d doses for these logs.", 
				 needed_doses);
		packet_send_chatbox_message(c, msg);
	}
	else
	{
		int annointed_id = get_annointed_log_id(logs_id);
		doses_left = (doses - needed_doses);
		
		transform_item(c, logs_slot, annointed_id); 
		
		/* If there are still doses remaining, don't delete the oil, but change
		   it to a lower dose amount.  */
		if (doses_left > 0)
		{
			transform_item(c, oil_slot, get_sacred_oil_by_dose(doses_left));
		}
		else
		{
			delete_item_from_inv_slot(c, oil_slot, 1);
		}


		snprintf(msg, 100, "You use the sacred oil on the %s and get %s.", 
				 string_to_lower_num(ITEMS[logs_id]->name, 1), 
				 string_to_lower_num(ITEMS[annointed_id]->name, 1));
		packet_send_chatbox_message(c, msg);
		add_skill_xp(c, FIREMAKING_SKILL, get_firemaking_xp(firemaking_id));
	}
}

/**
 * Handle adding of logs to the funeral pyre, tracking of what logs
 * are on it.
 * @param *c Client instance.
 * @param logs_id ID of the logs used.
 * @param logs_slot Slot where the logs are in the inventory.
 * @param firemaking_id ID for the firemaking task being performed.
 */
void add_logs_to_pyre(struct client *c, int logs_id, int logs_slot, 
					  int firemaking_id)
{
	int required_level = get_firemaking_level_needed(firemaking_id);
	int firemaking_level = get_player_skill_level(c, FIREMAKING_SKILL);
	int obj_index;

	if (firemaking_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "firemaking to do that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	obj_index = get_object_spawn_index(c->plr->action_x, c->plr->action_y, 
										  c->plr->world_z);

	OBJECT_SPAWNS[obj_index]->internal_counter = get_pyre_log_id(logs_id);
	set_object_transforms(obj_index, 4094, 50);

	delete_item_from_inv_slot(c, logs_slot, 1);

	reset_object_globally(obj_index);
	spawn_object_globally(obj_index);
	/* TODO: Find out what the place onto pyre animation is, and add 
	   it here.  */
}

/**
 * Handle adding remains to the pyre, checking if the logs are the right kind
 * for the remains used.
 * @param *c Client instance.
 * @param remains_id ID of the remains to use.
 * @param remains_slot Slot in the inventory where the remains are.
 */
void add_remains_to_pyre(struct client *c, int remains_id, int remains_slot)
{
	int obj_index;
	int remains_pyre_id;
	int failed_remains_type = 0;

	/* Don't think we need to check level here, just remains id.  */
	/*
	if (c->plr->level[FIREMAKING_SKILL] < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "firemaking to do that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	*/

	obj_index = get_object_spawn_index(c->plr->action_x, 
					c->plr->action_y, c->plr->world_z);

	remains_pyre_id = get_pyre_remains_id(remains_id);
	
	/* Checks if the logs are good enough quality to burn the kind of
	   remains the player is trying to put on the pyre.  */
	switch (OBJECT_SPAWNS[obj_index]->internal_counter)
	{
		/* Regular pyre logs, oak.  */
		case FIREMAKING_PYRE_REGULAR:
		case FIREMAKING_PYRE_OAK:
			if (remains_pyre_id > FIREMAKING_PHRIN_REMAINS)
			{
				failed_remains_type = 1;
			}
			break;
		case FIREMAKING_PYRE_WILLOW:
		case FIREMAKING_PYRE_TEAK:
		case FIREMAKING_PYRE_MAPLE:
		case FIREMAKING_PYRE_MAHOGANY:
			if (remains_pyre_id > FIREMAKING_RIYL_REMAINS)
			{
				failed_remains_type = 1;
			}
			break;
		case FIREMAKING_PYRE_YEW:
			if (remains_pyre_id > FIREMAKING_ASYN_REMAINS)
			{
				failed_remains_type = 1;
			}
			break;
		case FIREMAKING_PYRE_MAGIC:
			if (remains_pyre_id > FIREMAKING_FIYR_REMAINS)
			{
				failed_remains_type = 1;
			}
			break;
	}
	
	/* If the logs weren't good enough to burn that type of remains, let the
	 * player know.  */
	if (failed_remains_type == 1)
	{
		packet_send_chatbox_message(c, "You can't burn those remains with these "
						"logs!");
		OBJECT_SPAWNS[obj_index]->transform_ticks = 50;
		return;
	}

	OBJECT_SPAWNS[obj_index]->internal_counter2 
		= get_pyre_remains_id(remains_id);
	set_object_transforms(obj_index, 4100, 50);
	delete_item_from_inv_slot(c, remains_slot, 1);
	reset_object_globally(obj_index);
	spawn_object_globally(obj_index);
}

/**
 * Check if the player meets all level requirements before lighting pyre,
 * sets up the lighting animation.
 * @param *c Client instance.
 */
void init_light_pyre_remains(struct client *c)
{
	int obj_index;
	int log_id, remains_id, required_level;
	int firemaking_level = get_player_skill_level(c, FIREMAKING_SKILL);

	obj_index = get_object_spawn_index(c->plr->action_x, 
					c->plr->action_y, c->plr->world_z);
	
	if (obj_index == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	log_id = OBJECT_SPAWNS[obj_index]->internal_counter;
	remains_id = OBJECT_SPAWNS[obj_index]->internal_counter2;

	required_level = get_firemaking_level_needed((40 + log_id));

	/* If logs or remains were not properly set, return.  */
	if (log_id == -1 || remains_id == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (firemaking_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "firemaking to do that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	c->plr->action_ticks = 4;
	c->plr->action_skill_id = FIREMAKING_SKILL;
	c->plr->action_skill_type = 1;
	player_play_animation(c, 733);

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_FIREMAKING);
	
	/* Should prevent pyre despawning when a player is trying to use it.  */
	if (OBJECT_SPAWNS[obj_index]->transform_ticks < 5)
	{
		OBJECT_SPAWNS[obj_index]->transform_ticks = 5;
	}
}

/**
 * Handle lighting of the funeral pyre, add loot, play animations.
 * @param *c Client instance.
 */
void light_pyre_remains(struct client *c)
{
	int obj_index;
	int remains_id, log_id;
	/* Base pyre coordinates.  */
	int pyre_x = 0;
	int pyre_y = 0;
	/* Coordinates to spawn the graphics over the pyre.  */
	int pyre_gfx_x = 0;
	int pyre_gfx_y = 0;
	/* Coordinates to spawn items on the stand next to the pyre.  */
	int key_x = 0;
	int key_y = 0;
	
	int item_index = 0;


	obj_index = get_object_spawn_index(c->plr->action_x, 
					c->plr->action_y, c->plr->world_z);
	
	if (obj_index == -1)
	{
		return;
	}

	pyre_x = (OBJECT_SPAWNS[obj_index]->world_x);
	pyre_y = (OBJECT_SPAWNS[obj_index]->world_y);

	log_id = OBJECT_SPAWNS[obj_index]->internal_counter;
	remains_id = OBJECT_SPAWNS[obj_index]->internal_counter2;

	/* If logs or remains were not properly set, return.  */
	if (log_id == -1 || remains_id == -1)
	{
		return;
	}
	
	OBJECT_SPAWNS[obj_index]->transform_ticks = 3;
	OBJECT_SPAWNS[obj_index]->cooldown_ticks = 6;

	switch (OBJECT_SPAWNS[obj_index]->internal_face)
	{
		case 0:
			pyre_gfx_x = (pyre_x);
			pyre_gfx_y = (pyre_y + 1);
			key_x = (pyre_x);
			key_y = (pyre_y + 3);
			break;
		case 2:
			pyre_gfx_x = (pyre_x);
			pyre_gfx_y = (pyre_y + 1);
			key_x = (pyre_x);
			key_y = (pyre_y - 1);
			break;
		case 1:
			pyre_gfx_x = (pyre_x + 1);
			pyre_gfx_y = (pyre_y);
			key_x = (pyre_x + 3);
			key_y = (pyre_y);
			break;
		case 3:
			pyre_gfx_x = (pyre_x + 1);
			pyre_gfx_y = (pyre_y);
			key_x = (pyre_x - 1);
			key_y = (pyre_y);
			break;
		default:
			printf("NON VALID FACE!\n");
			break;
	}

	player_replay_animation(c);
	packet_draw_gfx_in_world_globally(c, 357, pyre_gfx_x, pyre_gfx_y, 125, 1);
	packet_draw_gfx_in_world_globally(c, 293, pyre_gfx_x, pyre_gfx_y, 100, 35);
	packet_draw_gfx_in_world_globally(c, 86, key_x, key_y, 100, 70);
	
	/* TODO: Replace this with actual, proper loot drops.  */
	item_index = add_to_item_drops(1337, 1, -1, key_x, key_y, 0, 1);

	if (item_index != -1)
	{
		ITEM_DROPS[item_index]->player_index = c->plr->index;
		ITEM_DROPS[item_index]->tick_spawn_delay = 2;
		ITEM_DROPS[item_index]->tick_dropped = 52;
	}

	/* Give xp to the player for burning the pyre.  */
	add_skill_xp(c, FIREMAKING_SKILL, get_firemaking_xp((log_id + 40)));
	/* TODO: Add prayer xp gained.  */
	c->plr->action_skill_type = c->plr->action_skill_id = -1;
}

/**
 * Check needed variables for starting firemaking.
 * @param *c Client instance.
 */
void init_firemaking(struct client *c)
{
	int required_level = get_firemaking_level_needed(c->plr->action_id_type);
	int has_tinderbox = player_has_item(c, 590, 1);
	int firemaking_level = get_player_skill_level(c, FIREMAKING_SKILL);

	/* Set action related ids.  */
	c->plr->action_skill_id = FIREMAKING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_FIREMAKING);

	if (firemaking_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "firemaking to do that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (has_tinderbox == -1)
	{
		packet_send_chatbox_message(c, "You need a tinderbox to light logs!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* If we're doing regular firemaking, setup.  */
	if (c->plr->action_id_type >= FIREMAKING_REGULAR 
		&& c->plr->action_id_type <= FIREMAKING_MAGIC)
	{
		if (get_object_spawn_index(c->plr->world_x, c->plr->world_y, 
								   c->plr->world_z) != -1)
		{
			set_player_state(c, STATE_IDLE);
			packet_send_chatbox_message(c, "You can't light a fire here!");
			return;	
		}

		/* If item slot is set, then not using item on ground.  */
		if (c->plr->action_item_slot != -1)
		{
			c->plr->action_store_two = packet_player_drop_item_on_ground(c, c->plr->action_item_id, 
												 INTR_INV_PLR_ITEMS, 
												 c->plr->action_item_slot);

			if (c->plr->action_store_two == -1)
			{
				return;
			}
		}
		else
		{
			c->plr->action_store_two 
			= is_item_drop_in_world(c->plr->action_item_id, 
										c->plr->action_x, 
										c->plr->action_y,
										c->plr->action_z);

			if (c->plr->action_store_two == -1)
			{
				return;
			}
		}

		packet_send_chatbox_message(c, "You attempt to light the logs.");
		/* Add a slight delay before starting the fire.  */
		c->plr->action_ticks = 2;
		packet_play_sfx(c, 375, 0, -1);
	}


	c->plr->skill_tool_equip = 0;
	c->plr->skill_tool_id = 590; 
	c->plr->skill_tool_slot = has_tinderbox;

	printf("Firemaking setup. No errors.\n");
}

/**
 * Handle creating the fire, adding xp and removing the items.
 * @param *c Client instance.
 * @todo Lantern lighting is not handled yet, add that soon.
 */
void handle_firemaking(struct client *c)
{
	float xp_gain = get_firemaking_xp(c->plr->action_id_type);
	int obj_index = 0;
	int fire_time = 0;
	int success = is_skill_check_success(c, FIREMAKING_SKILL, 
				get_firemaking_chance_base(c->plr->action_id_type),
				get_firemaking_chance_max(c->plr->action_id_type),
				1.0);

	player_clear_walking_queue(c);

	/* If woodcutting has suceeded.  */
	if (success)
	{
		/* TODO: Find more accurate burning times than this.  */
		fire_time = rand_int(60, 240);

		obj_index 
		= add_to_object_list(2732, 10, c->plr->world_x, c->plr->world_y, 
									   c->plr->world_z, 0, fire_time, 1);
		set_object_spawning_time(obj_index, 0);

		/* TODO: Can I combine these into one?  */
		set_firemaking_movement(c, get_firemaking_movement(c));
		delete_from_item_drops(c->plr->action_store_two, 1);
		add_to_item_drops_exclusive(c, 592, 1, -1, c->plr->world_x, 
									c->plr->world_y, c->plr->world_z, 1);
	
		add_skill_xp(c, FIREMAKING_SKILL, xp_gain);
		packet_send_chatbox_message(c, "The fire catches and the logs begin to burn.");
		set_player_state(c, STATE_IDLE);
		return;
	}

	player_play_animation(c, 733);
	c->plr->action_ticks = 4;
}
