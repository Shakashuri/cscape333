/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "constants.h"
#include "client.h"
#include "combat.h"
#include "interface.h"
#include "item.h"
#include "misc.h"
#include "packet.h"
#include "player.h"
#include "skills.h"
#include "skill_magic.h"
#include "skill_smithing.h"

/**
 * @brief Gets where in the list of spells the spell with the passed id is.
 *
 * @param id Spell id to search for.
 * @return Returns index of spell if found, -1 if no spell matching the passed
 * id was found.
 */
int get_spell_index(int id)
{
	int x;

	for (x = 0; x < NUM_SPELLS; x++)
	{
		if (SPELLS[x] == NULL)
		{
			continue;
		}
		if (SPELLS[x]->id == id)
		{
			return x;
		}
	}

	printf("\tSPELL ID %d not supported yet!\n", id);
	return -1;
}

int get_jewelry_enchanted_id(int item_id)
{
	switch (item_id)
	{
		/* Sapphire ring.  */
		case 1637:
			return 2550;	
		/* Sapphire necklace.  */
		case 1656:
			return 3853;
		/* Sapphire amulet.  */
		case 1694:
			return 1727;
		/* Emerald ring.  */
		case 1650:
			return 2552;	
		/* Emerald necklace.  */
		case 1658:
			return 5521;
		/* Emerald amulet.  */
		case 1696:
			return 1729;
		/* Ruby ring.  */
		case 1641:
			return 2568;	
		/* Ruby necklace.  */
		/* Not in 333.
		case 1660:
			return 5521;
			*/
		/* Ruby amulet.  */
		case 1698:
			return 1725;
		/* Diamond ring.  */
		case 1643:
			return 2570;	
		/* Diamond necklace.  */
		/* Not in 333. 
		case 1662:
			return 5521; */
		/* Diamond amulet.  */
		case 1700:
			return 1731;
		/* Dragonstone ring.  */
		case 1645:
			return 2572;	
		/* Dragonstone necklace.  */
		/* Not in 333.
		case 1664:
			return 5521; */
		/* Dragonstone amulet.  */
		case 1702:
			return 1712;
		default:
			return -1;
	}
}

int get_jewelry_enchant_gfx(int item_id)
{
	switch (item_id)
	{
		/* Rings.  */
		case 1637:
		case 1650:
		case 1641:
		case 1643:
		case 1645:
			return MAGIC_GFX_ENCHANT_RING;	
		/* Sapphire necklace.  */
		case 1656:
		/* Sapphire amulet.  */
		case 1694:
			return MAGIC_GFX_ENCHANT_1_NECKLACE;	
		/* Emerald necklace.  */
		case 1658:
		/* Emerald amulet.  */
		case 1696:
			return MAGIC_GFX_ENCHANT_1_NECKLACE;	
		/* Ruby amulet.  */
		case 1698:
			return MAGIC_GFX_ENCHANT_2_NECKLACE;	
		/* Diamond amulet.  */
		case 1700:
			return MAGIC_GFX_ENCHANT_2_NECKLACE;	
		/* Dragonstone amulet.  */
		case 1702:
			return MAGIC_GFX_ENCHANT_3_NECKLACE;	
		default:
			return -1;
	}
}

int get_jewelry_enchant_emote(int item_id)
{
	switch (item_id)
	{
		/* Rings.  */
		case 1637:
		case 1650:
		case 1641:
		case 1643:
		case 1645:
			return MAGIC_EMOTE_ENCHANT_RING;	
		/* Sapphire necklace.  */
		case 1656:
		/* Sapphire amulet.  */
		case 1694:
			return MAGIC_EMOTE_ENCHANT_1_NECKLACE;	
		/* Emerald necklace.  */
		case 1658:
		/* Emerald amulet.  */
		case 1696:
			return MAGIC_EMOTE_ENCHANT_1_NECKLACE;	
		/* Ruby amulet.  */
		case 1698:
			return MAGIC_EMOTE_ENCHANT_2_NECKLACE;	
		/* Diamond amulet.  */
		case 1700:
			return MAGIC_EMOTE_ENCHANT_2_NECKLACE;	
		/* Dragonstone amulet.  */
		case 1702:
			return MAGIC_EMOTE_ENCHANT_3_NECKLACE;	
		default:
			return -1;
	}
}

int get_charged_orb_id(int object_id)
{
	switch (object_id)
	{
		/* Earth.  */
		case 2150:
			return 575;
		/* Water.  */
		case 2151:
			return 571;
		/* Air.  */
		case 2152:
			return 573;
		/* Fire.  */
		case 2153:
			return 569;

		default:
			printf("Unknown obelisk id %d!\n", object_id);
			return -1;
	}
}

/**
 * Checks if the player has needed runes for casting a spell.
 * @param *c Client instance.
 * @param spell_index Index of the spell to check runes for.
 * @param delete_runes If runes are removed from the inventory or not.
 * @return 1 on requirements met.
 * @return 0 on requirements not met.
 */
int has_needed_runes(struct client *c, int spell_index, int delete_runes)
{
	int x; 
	int y;
	int found_duplicate_rune = 0;
	int store_temp = -1;
	char return_msg[100];

	/* For checking if all needed runes are there.  */
	int *rune_slots = calloc(PROTOCOL_NUM_RUNES, sizeof(int));
	int *rune_num_has = calloc(PROTOCOL_NUM_RUNES, sizeof(int));

	/* Zero out the rune_slots array, need to be filled with -1.  */
	for (x = 0; x < PROTOCOL_NUM_RUNES; x++)
	{
		rune_slots[x] = -1;
	}

	/* Checks if player is high enough of a level to cast the 
	   selected spell. NOTE: May need to change this to check based on xp
	   instead to prevent using potions to cast spells you otherwise 
	   couldn't.  */
	if (c->plr->level[MAGIC_SKILL] < SPELLS[spell_index]->level_req)
	{
		packet_send_chatbox_message(c, "You are not a high enough level to cast that spell!");
		snprintf(return_msg, 100, "You must be at least a level %d in Magic"
								  " to cast that!", SPELLS[spell_index]->level_req);	
		packet_send_chatbox_message(c, return_msg);
		free(rune_slots);
		free(rune_num_has);
		return 0;
	}
	
	for (x = 0; x < PROTOCOL_NUM_RUNES; x++)
	{
		found_duplicate_rune = 0;

		if (SPELLS[spell_index]->runes_needed[x] != 0)
		{
			printf("\tSPELL NAME: %s\n", SPELLS[spell_index]->name);
			printf("Needs runes! RUNE ID: %d NUM: %d\n", 
					x, SPELLS[spell_index]->runes_needed[x]);
			
			/* Air rune.  */
			if (RUNE_IDS[x] == 556
				&& (c->plr->equipment[WEAPON_SLOT] == 1405
				|| c->plr->equipment[WEAPON_SLOT] == 1397
				|| c->plr->equipment[WEAPON_SLOT] == 1381))
			{
				printf("Air Staff equipped.\n");
				rune_num_has[x] = 99;
				rune_slots[x] = -1;
				continue;
			}
			/* Water rune.  */
			else if (RUNE_IDS[x] == 555
				&& (c->plr->equipment[WEAPON_SLOT] == 1403
				|| c->plr->equipment[WEAPON_SLOT] == 1395
				|| c->plr->equipment[WEAPON_SLOT] == 1383))
			{
				printf("Water Staff equipped.\n");
				rune_num_has[x] = 99;
				rune_slots[x] = -1;
				continue;
			}
			/* Fire rune.  */
			else if (RUNE_IDS[x] == 554
				&& (c->plr->equipment[WEAPON_SLOT] == 1401
				|| c->plr->equipment[WEAPON_SLOT] == 1393
				|| c->plr->equipment[WEAPON_SLOT] == 1387
				|| c->plr->equipment[WEAPON_SLOT] == 3053
				|| c->plr->equipment[WEAPON_SLOT] == 3054))
			{
				printf("Fire Staff equipped.\n");
				rune_num_has[x] = 99;
				rune_slots[x] = -1;
				continue;
			}
			/* Earth rune.  */
			else if (RUNE_IDS[x] == 557
				&& (c->plr->equipment[WEAPON_SLOT] == 1407
				|| c->plr->equipment[WEAPON_SLOT] == 1399
				|| c->plr->equipment[WEAPON_SLOT] == 1385
				|| c->plr->equipment[WEAPON_SLOT] == 3053
				|| c->plr->equipment[WEAPON_SLOT] == 3054))
			{
				printf("Earth Staff equipped.\n");
				rune_num_has[x] = 99;
				rune_slots[x] = -1;
				continue;
			}
			else
			{
				/* Store the rune slot temporarily.  */
				store_temp
					= look_for_rune(c, RUNE_IDS[x], 
									SPELLS[spell_index]->runes_needed[x]);

				if (store_temp != -1)
				{
					/* Check that we haven't already taken runes from a slot.
					 * This prevents taking double the amount of combination
					 * runes when casting a spell.  */
					for (y = 0; y < PROTOCOL_NUM_RUNES; y++)
					{
						/* If we're already removing runes from this slot.  */
						if (rune_slots[y] == store_temp)
						{
							/* If a later rune requirement also uses a combo
							 * rune, check if there is a higher number needed.
							 * If so, change the rune requirement to use the
							 * higher amount.  */
							if (SPELLS[spell_index]->runes_needed[x]
								> SPELLS[y]->runes_needed[x])
							{
								printf("larger rune need.\n");
								printf("****swapping to the larger number.\n");
								rune_slots[y] = -1;
								break;
							}
							else
							{
								printf("trying to add duplicate rune, "
									   "discard.\n");
								found_duplicate_rune = 1;
								break;
							}
						}
					}

					if (found_duplicate_rune == 0)
					{
						rune_slots[x] = store_temp;
						rune_num_has[x] = c->plr->items_n[rune_slots[x]];
						continue;
					}
					else
					{
						continue;
					}
				}
				else
				{
					printf("Requirements not met!\n");
					printf("Needed %d of rune %d, have %d\n", 
						   SPELLS[spell_index]->runes_needed[x], RUNE_IDS[x], 
						   rune_num_has[x]);
					free(rune_slots);
					free(rune_num_has);
					return 0;
				}
			}
		}
	}

	/* Delete runes from inventory if needed.  */
	if (delete_runes == 1)
	{
		for (x = 0; x < PROTOCOL_NUM_RUNES; x++)
		{
			if (rune_slots[x] != -1)
			{
				delete_item_from_inv_slot(c, rune_slots[x], 
					SPELLS[spell_index]->runes_needed[x]);
			}
		}
	}

	printf("Has everything, cast spell!\n");
	free(rune_slots);
	free(rune_num_has);
	return 1;
}
/**
 * Sets up client and server for player autocasting.
 * @param *c Client instance.
 * @param spell_id Spell to start autocasting.
 */
void set_autocast_spell(struct client *c, int spell_id)
{
	int index = get_spell_index(spell_id);

	if (has_needed_runes(c, index, 0) == 1)
	{
		c->plr->autocast_spell_id = spell_id;
		c->plr->cast_spell_id = 0;
		apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
		packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 2);
		packet_set_interface_text(c, SPELLS[index]->name, 352);
	}
	else
	{
		c->plr->autocast_spell_id = 0;
	}
}

/**
 * Handles casting of teleportation spells, moving of player.
 * @param *c Client instance.
 * @param spell_id ID of the spell being cast.
 */
void cast_teleport_spell(struct client *c, int spell_id)
{
	int index = get_spell_index(spell_id);
	c->plr->cast_spell_id = spell_id;

	if (has_needed_runes(c, index, 1) == 1)
	{
		reset_player_combat(c);
		player_play_animation(c, SPELLS[index]->cast_emote);
		packet_draw_gfx_in_world_globally(c, 
							SPELLS[index]->cast_gfx, c->plr->world_x, 
							c->plr->world_y, SPELLS[index]->cast_gfx_height, 
							50);

		c->plr->stored_tele_x = SPELLS[index]->tele_x;
		c->plr->stored_tele_y = SPELLS[index]->tele_y;
		c->plr->stored_tele_z = 0;
		c->plr->teleport_timer = SPELLS[index]->duration;
		c->plr->teleport_type = SPELLS[index]->spellbook;
	}
}

/**
 * Handles when a teleother spell is cast on someone, and that person accepts.
 * @param *c Client instance.
 * @param spell_id ID of the teleother spell.
 * @bug Still needs to be better handled.
 */
void accept_teleother(struct client *c, int spell_id)
{
	int index = get_spell_index(spell_id);

	reset_player_combat(c);
	player_play_animation(c, SPELLS[index]->cast_emote);
	packet_draw_gfx_in_world_globally(c, SPELLS[index]->cast_gfx, c->plr->world_x, 
						c->plr->world_y, SPELLS[index]->cast_gfx_height, 
						50);
	c->plr->stored_tele_x = SPELLS[index]->tele_x;
	c->plr->stored_tele_y = SPELLS[index]->tele_y;
	c->plr->teleport_timer = SPELLS[index]->duration;
	c->plr->teleport_type = SPELLS[index]->spellbook;
}

static void cast_enchant_on_jewelry(struct client *c, int spell_id,
									int item_id, int item_slot)
{
	char return_msg[100];
	int index = get_spell_index(spell_id);
	int enchanted_item_id = get_jewelry_enchanted_id(item_id);
	int enchant_gfx = -1;
	int enchant_emote = -1;

	if (enchanted_item_id == -1)
	{
		packet_send_chatbox_message(c, "You cannot enchant that with this spell.");
		return;
	}

	if (has_needed_runes(c, index, 1) == 1)
	{
		enchant_gfx = get_jewelry_enchant_gfx(item_id);
		enchant_emote = get_jewelry_enchant_emote(item_id);

		/* Double check that we got an emote and graphic to show. If one 
		 * doesn't have an animation, use the default.  */
		if (enchant_gfx == -1)
		{
			printf("Enchant gfx for %d not found!\n", item_id);
			enchant_gfx = MAGIC_GFX_ENCHANT_RING;
		}

		if (enchant_emote == -1)
		{
			printf("Enchant emote for %d not found!\n", item_id);
			enchant_emote = MAGIC_EMOTE_ENCHANT_RING;
		}

		freeze_player(c, EFFECT_FREEZE, SPELLS[index]->cast_ticks);

		player_play_animation(c, enchant_emote);
		player_draw_gfx(c, enchant_gfx);
		
		transform_item(c, item_slot, enchanted_item_id);
		add_skill_xp(c, MAGIC_SKILL, SPELLS[index]->xp);

		/* FIXME: I have not seen any return messages for enchanting jewelry
		 * yet, is this how the original game does it?  */
		snprintf(return_msg, 100, "You enchant the %s.",
				 string_to_lower(ITEMS[item_id]->name));
		packet_send_chatbox_message(c, return_msg);
	}
}

static void cast_alchemy_on_item(struct client *c, int spell_id, int item_id,
								 int item_slot, int is_high_alch)
{
	char return_msg[100];
	int index = get_spell_index(spell_id);

	int item_value = -1;
	int cast_gfx = -1;
	int cast_emote = -1;

	/* Set up the alchemy spell.  */
	if (is_high_alch)
	{
		item_value = ITEMS[item_id]->high_alch;

		/* If this item is noted, fetch the value from the unnoted version.  */
		if (item_value == 0 && ITEMS[item_id]->noted == 1)
		{
			item_value = ITEMS[ITEMS[item_id]->unnoted_id]->high_alch;
		}
		cast_emote = MAGIC_EMOTE_HIGH_ALCHEMY;
		cast_gfx = MAGIC_GFX_HIGH_ALCHEMY;
	}
	else
	{
		item_value = ITEMS[item_id]->low_alch;

		/* If this item is noted, fetch the value from the unnoted version.  */
		if (item_value == 0 && ITEMS[item_id]->noted == 1)
		{
			item_value = ITEMS[ITEMS[item_id]->unnoted_id]->low_alch;
		}
		cast_emote = MAGIC_EMOTE_LOW_ALCHEMY;
		cast_gfx = MAGIC_GFX_LOW_ALCHEMY;
	}

	if (item_value == 0 || ITEMS[item_id]->tradable == 0)
	{
		packet_send_chatbox_message(c, "You cannot use alchemy on this item.");
		return;
	}

	/* Make sure the player has space for this action.  */
	if (get_free_inv_slots(c) > 0 || (player_has_item(c, 995, 1) != -1))
	{
		if (has_needed_runes(c, index, 1) == 1)
		{
			freeze_player(c, EFFECT_FREEZE, SPELLS[index]->cast_ticks);

			player_play_animation(c, cast_emote);
			player_draw_gfx(c, cast_gfx);

			delete_item_from_inv_slot(c, item_slot, 1); 
			add_item_to_inv(c, 995, item_value, -1);
			add_skill_xp(c, MAGIC_SKILL, SPELLS[index]->xp);

			/* FIXME: I have not seen any return messages for enchanting
			 * jewelry yet. Is this how the original game does it?  */
			snprintf(return_msg, 100, "You turn the %s into gold.",
					 string_to_lower(ITEMS[item_id]->name));
			packet_send_chatbox_message(c, return_msg);
		}
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have enough space to do that!");
	}
}

/**
 * Handles casting of the bones to bananas spell. All of the normal bones
 * the player has in their inventory are changed into bananas.
 * @param *c Client instance.
 * @param spell_id ID of the spell.
 */
void cast_bones_to_bananas(struct client *c, int spell_id)
{
	int bone_slot = 0;
	int spell_index = get_spell_index(spell_id);
	int level_needed;
	int magic_level;
	int num_bones_converted = 0;

	if (spell_index == -1)
	{
		return;
	}

	level_needed = SPELLS[spell_index]->level_req;
	magic_level = get_player_skill_level(c, MAGIC_SKILL);

	if (magic_level < level_needed)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "magic to do that!\n", level_needed);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (has_needed_runes(c, spell_index, 1) != 1)
	{
		packet_send_chatbox_message(c, "You don't have the runes to cast that!");
		return;
	}

	/* Bones.  */
	while ((bone_slot = player_has_item(c, 526, 1)) != -1)
	{
		/* Don't spam with refresh if they have more than one.  */
		transform_item_no_refresh(c, bone_slot, 1963);
		num_bones_converted++;
	}

	if (num_bones_converted > 0)
	{
		add_skill_xp(c, MAGIC_SKILL, SPELLS[spell_index]->xp);
		player_play_animation(c, SPELLS[spell_index]->cast_emote);
		player_draw_gfx(c, SPELLS[spell_index]->cast_gfx);
		freeze_player(c, EFFECT_FREEZE, 1);
		packet_send_chatbox_message(c, "You turn all the bones in your inventory to "
					 "bananas.");
		packet_reload_items_in_inventory_interface(c);
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have any bones to convert!");
	}
}

/**
 * Handles when a player casts superheat on an item.
 * @param *c Client instance.
 * @param spell_id ID of the spell.
 * @param item_id ID of the item the player cast the spell on.
 */
void cast_superheat_on_item(struct client *c, int spell_id, int item_id)
{
	int spell_index = get_spell_index(spell_id);
	int magic_level = get_player_skill_level(c, MAGIC_SKILL);
	int smithing_level = get_player_skill_level(c, SMITHING_SKILL);

	int magic_level_needed = -1;
	int smithing_id = get_smithing_id_from_ore(item_id);
	int smithing_level_needed = get_smithing_level_needed(smithing_id);

	int ore_type_needed = -1;
	int ore_type_needed2 = -1;
	int ore_type_num_needed = 0;
	int ore_type_num_needed2 = 0;

	int primary_ore_has = 0;
	int secondary_ore_has = 0;

	float magic_xp;
	float smithing_xp;

	int item_to_make;
	int x;

	if (spell_index == -1)
	{
		return;
	}

	if (smithing_id == -1)
	{
		packet_send_chatbox_message(c, "You need to cast superheat item on ore.");
		player_draw_gfx(c, 85);
		return;
	}

	/* Level requirement checks.  */
	if (magic_level < magic_level_needed)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "magic to do that!\n", magic_level_needed);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (smithing_level < smithing_level_needed)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "smithing to do that!\n", smithing_level_needed);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	/* Iron and steel both use iron ore, so check if the player has enough
	 * coal to make an iron bar. If so, swap over to making steel.  */
	if (smithing_id == SMITHING_BAR_IRON)
	{
		/* Only swap over if the player has coal in their inventory, and meets
		 * the required level to do it.  */
		if ((player_has_item_non_stack(c, 453, 2) != -1)
			&& (smithing_level 
				>= get_smithing_level_needed(SMITHING_BAR_STEEL)))
		{
			smithing_id = SMITHING_BAR_STEEL;
		}
	}

	/* Get the values for all of these after the steel check, otherwise they
	 * will get an iron bar or xp.  */
	item_to_make = get_smithing_bar_id_rev(smithing_id);
	magic_xp = SPELLS[spell_index]->xp;
	smithing_xp = get_smithing_xp(smithing_id);

	/* Make sure the player has all of the ore they need to make the bar.  */
	ore_type_needed = get_smelting_ore_needed(smithing_id, 0);
	ore_type_num_needed 
		= get_smelting_ore_number_needed(smithing_id, 0);

	ore_type_needed2 = get_smelting_ore_needed(smithing_id, 1);
	ore_type_num_needed2 
		= get_smelting_ore_number_needed(smithing_id, 1);

	primary_ore_has = player_has_item_non_stack(c, ore_type_needed, 
		ore_type_num_needed);		

	if (ore_type_needed2 != -1)
	{
		secondary_ore_has = player_has_item_non_stack(c, ore_type_needed2, 
			ore_type_num_needed2);
		
		if ((primary_ore_has < ore_type_num_needed) 
			|| (secondary_ore_has < ore_type_num_needed2))
		{
			packet_send_chatbox_message(c, "You don't have enough ore to make that!");	
			set_player_state(c, STATE_IDLE);
			return;
		}
	}
	else
	{
		if ((primary_ore_has < ore_type_num_needed))
		{
			packet_send_chatbox_message(c, "You don't have enough ore to make that!");	
			set_player_state(c, STATE_IDLE);
			return;
		}
	}

	/* Check if they have the runes to cast the spell.  */
	if (has_needed_runes(c, spell_index, 1) != 1)
	{
		packet_send_chatbox_message(c, "You don't have the runes to cast that!");
		return;
	}

	/* Remove all ores needed to make the metal bar.  */
	for (x = 0; x < ore_type_num_needed; x++)
	{
		delete_item_from_inv(c, ore_type_needed, 1);
	}

	if (ore_type_num_needed2 != -1 && ore_type_needed2 != -1)
	{
		for (x = 0; x < ore_type_num_needed2; x++)
		{
			delete_item_from_inv(c, ore_type_needed2, 1);
		}
	}
	

	/* Add the bar to their inventory, give the xp, and play the 
	 * animations.  */
	add_item_to_inv(c, item_to_make, 1, -1);	
	add_skill_xp(c, SMITHING_SKILL, smithing_xp);
	add_skill_xp(c, MAGIC_SKILL, magic_xp);

	player_play_animation(c, SPELLS[spell_index]->cast_emote);
	player_draw_gfx(c, SPELLS[spell_index]->cast_gfx);
	freeze_player(c, EFFECT_FREEZE, 3);

	/** FIXME: Is this an accurate message?  */
	packet_send_chatbox_message(c, "You superheat the ore to make a metal bar.");
}

/**
 * Handles charging an orb on an obelisk. Checks to make sure the player has
 * all the requirements, right spell used on the right obelisk.
 * @param *c Client instance.
 * @param obj_id Obelisk ID the spell was cast on.
 * @param spell_id ID of the spell used.
 */
void cast_charge_orb_on_obelisk(struct client *c, int obj_id, int spell_id)
{
	int spell_index = get_spell_index(spell_id);
	int uncharged_orb_slot = player_has_item(c, 567, 1); 
	int charged_orb_id;

	if (c->plr->action_id != SPELLS[spell_index]->object_needed)
	{
		packet_send_chatbox_message(c, "You cannot use this spell on that!");
		return;
	}

	if (uncharged_orb_slot == -1)
	{
		packet_send_chatbox_message(c, "You need an unpowered orb to charge.");
		return;
	}

	charged_orb_id = get_charged_orb_id(obj_id);

	if (has_needed_runes(c, spell_index, 1) == 1)
	{
		freeze_player(c, EFFECT_FREEZE, SPELLS[spell_index]->cast_ticks);

		player_play_animation(c, SPELLS[spell_index]->cast_emote);
		player_draw_gfx(c, SPELLS[spell_index]->cast_gfx);

		transform_item(c, uncharged_orb_slot, charged_orb_id);
		add_skill_xp(c, MAGIC_SKILL, SPELLS[spell_index]->xp);

		/* FIXME: I have not seen any return messages for enchanting
		 * jewelry yet. Is this how the original game does it?  */
		packet_send_chatbox_message(c, "You charge the orb with magical power.");
		/*
		snprintf(return_msg, 100, "You turn the %s into gold.",
				 string_to_lower(ITEMS[item_id]->name));
		packet_send_chatbox_message(c, return_msg);
		*/
	}
	else
	{
		packet_send_chatbox_message(c, "You don't have the required runes for this spell!");
	}
}

/**
 * Determines what function needs to be called to properly handle when the
 * player tries to cast a spell on an item.
 * @param *c Client instance.
 * @param spell_id Spell that was cast.
 * @param item_id ID of the item the spell was cast on.
 * @param item_slot Location of the item in inventory.
 */
void handle_magic_spell_on_item(struct client *c, int spell_id, int item_id,
								int item_slot)
{
	/* Figure out what function handles the spell being cast.  */
	switch (spell_id)
	{
		/* Enchant jewelry.  */
		case 1155:
		case 1165:
		case 1176:
		case 1180:
		case 1187:
			cast_enchant_on_jewelry(c, spell_id, item_id, item_slot);
			break;
		/* Low alch.  */
		case 1162:
			cast_alchemy_on_item(c, spell_id, item_id, item_slot, 0);
			break;
		/* High alch.  */
		case 1178:
			cast_alchemy_on_item(c, spell_id, item_id, item_slot, 1);
			break;
		/* Superheat item.  */
		case 1173:
			cast_superheat_on_item(c, spell_id, item_id);
			break;
			
		default:
			printf("Spell %d is not currently handled yet!\n", spell_id);
			break;
	}
}
