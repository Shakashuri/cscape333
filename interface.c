/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** 
 * @file interface.c
 * @brief Any special interfaces that would better be put into a 
 * function to organize their data go here.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"
#include "client.h"
#include "interface.h"
#include "item.h"
#include "packet.h"
#include "player.h"

#include "shops.h"
#include "skill_smithing.h"

/**
 * Checks if the player is able to click on the interface they did, or if they
 * are hacking it in somehow.
 * @param *c Client instance.
 * @param interface_clicked ID sent from the client as to what was selected.
 * @return 0 on valid found, 1 on possible cheating.
 */
int anticheat_check_interface(struct client *c, int interface_clicked)
{
	int x = 0, y = 0;

	for (x = 0; x < INTERFACES_SIZE; x++)
	{
		if (INTERFACES[x].id == c->plr->interface_open)
		{
			for (y = 0; y < INTERFACES[x].size; y++)
			{
				if (INTERFACES[x].children != NULL)
				{
					if (INTERFACES[x].children[y] == interface_clicked)
					{
						return 0;
					}
				}
			}
			break;
		}
	}

	printf("\t### Invalid interface found, client may be cheating.\n");
	return 1;
}

/**
 * @brief Displays the jewelry menu for crafting rings, amulets, necklaces.
 * @param *c Client instance doing the crafting.
 */
static void show_jewelry_menu(struct client *c)
{
	int x;

	/* Ring mould.  */
	if (player_has_item(c, 1592, 1) != -1)
	{
		packet_set_item_in_interface(c, 4233, 0, 1635, 1);
		packet_set_item_in_interface(c, 4233, 1, 1637, 1);
		packet_set_item_in_interface(c, 4233, 2, 1639, 1);
		packet_set_item_in_interface(c, 4233, 3, 1641, 1);
		packet_set_item_in_interface(c, 4233, 4, 1643, 1);
		packet_set_item_in_interface(c, 4233, 5, 1645, 1);
		packet_set_item_in_interface_model(c, 4229, 0, -1); 
		packet_set_interface_text(c, "", 4230);
	}
	else
	{
		for (x = 0; x < 6; x++)
		{
			packet_set_item_in_interface(c, 4233, x, -1, 1);
		}	
		packet_set_item_in_interface_model(c, 4229, 120, 1592); 
		packet_set_interface_text(c, "You need a ring mould to craft rings.", 4230);
	}

	/* Necklace mould.  */
	if (player_has_item(c, 1597, 1) != -1)
	{
		packet_set_item_in_interface(c, 4239, 0, 1654, 1);
		packet_set_item_in_interface(c, 4239, 1, 1656, 1);
		packet_set_item_in_interface(c, 4239, 2, 1658, 1);
		packet_set_item_in_interface(c, 4239, 3, 1660, 1);
		packet_set_item_in_interface(c, 4239, 4, 1662, 1);
		packet_set_item_in_interface(c, 4239, 5, 1664, 1);
		packet_set_item_in_interface_model(c, 4235, 0, -1); 
		packet_set_interface_text(c, "", 4236);
	}
	else
	{
		for (x = 0; x < 6; x++)
		{
			packet_set_item_in_interface(c, 4239, x, -1, 1);
		}	
		packet_set_item_in_interface_model(c, 4235, 120, 1597); 
		packet_set_interface_text(c, "You need a necklace mould to craft necklaces.", 
							4236);
	}
	
	/* Amulet mould.  */
	if (player_has_item(c, 1595, 1) != -1)
	{
		packet_set_item_in_interface(c, 4245, 0, 1673, 1);
		packet_set_item_in_interface(c, 4245, 1, 1675, 1);
		packet_set_item_in_interface(c, 4245, 2, 1677, 1);
		packet_set_item_in_interface(c, 4245, 3, 1679, 1);
		packet_set_item_in_interface(c, 4245, 4, 1681, 1);
		packet_set_item_in_interface(c, 4245, 5, 1683, 1);
		packet_set_item_in_interface_model(c, 4241, 0, -1); 
		packet_set_interface_text(c, "", 4242);
	}
	else
	{
		for (x = 0; x < 6; x++)
		{
			packet_set_item_in_interface(c, 4245, x, -1, 1);
		}	
		packet_set_item_in_interface_model(c, 4241, 120, 1597); 
		packet_set_interface_text(c, "You need an amulet mould to craft amulets.", 
							4242);
	}

	packet_show_interface(c, 4161);	
}

/**
 * @brief Shows the tanning menu along with prices and images of the materials
 * needed. Handles text changing color if requirements are not met.
 *
 * @param *c Client instance doing the crafting.
 */
static void show_tanning_menu(struct client *c)
{
	int cash_slot = 0;
	int num_gold = 0;

	/* Send all the model ids, these don't change.  */
	packet_set_item_in_interface_model(c, 14769, 200, 1739);
	packet_set_item_in_interface_model(c, 14770, 200, 1739);
	packet_set_item_in_interface_model(c, 14771, 200, 6287);
	/* Swamp snake hide, later revision.
	packet_set_item_in_interface_model(c, 14772, 200, 6289); */
	packet_set_item_in_interface_model(c, 14773, 200, 1753);
	packet_set_item_in_interface_model(c, 14774, 200, 1751);
	packet_set_item_in_interface_model(c, 14775, 200, 1749);
	packet_set_item_in_interface_model(c, 14776, 200, 1747);
	
	if (player_has_item(c, 1739, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Soft leather", 14777);
		packet_set_interface_text(c, "@gre@Hard leather", 14778);
	}
	else
	{
		packet_set_interface_text(c, "@red@Soft leather", 14777);
		packet_set_interface_text(c, "@red@Hard leather", 14778);
	}

	if (player_has_item(c, 6287, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Snakeskin", 14779);
	}
	else
	{
		packet_set_interface_text(c, "@red@Snakeskin", 14779);
	}

	if (player_has_item(c, 1753, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Green d'hide", 14781);
	}
	else
	{
		packet_set_interface_text(c, "@red@Green d'hide", 14781);
	}
	
	if (player_has_item(c, 1751, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Blue d'hide", 14782);
	}
	else
	{
		packet_set_interface_text(c, "@red@Blue d'hide", 14782);
	}

	if (player_has_item(c, 1749, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Red d'hide", 14783);
	}
	else
	{
		packet_set_interface_text(c, "@red@Red d'hide", 14783);
	}

	if (player_has_item(c, 1747, 1) != -1)
	{
		packet_set_interface_text(c, "@gre@Black d'hide", 14784);
	}
	else
	{
		packet_set_interface_text(c, "@red@Black d'hide", 14784);
	}

	
	
	cash_slot = player_has_item(c, 995, 1);
	
	if (cash_slot == -1)
	{
		num_gold = 0;	
	}
	else
	{
		num_gold = c->plr->items_n[cash_slot];
	}

	/* Checks if the player has the gold needed for tanning, sets the cost
	   text color as needed.  */
	if (num_gold >= 1)
	{
		packet_set_interface_text(c, "@gre@1 coin", 14785);
		
		if (num_gold >= 3)
		{
			packet_set_interface_text(c, "@gre@3 coins", 14786);
			if (num_gold >= 15)
			{
				packet_set_interface_text(c, "@gre@15 coins", 14787);
				if (num_gold >= 20)
				{
					packet_set_interface_text(c, "@gre@20 coins", 14789);
					packet_set_interface_text(c, "@gre@20 coins", 14790);
					packet_set_interface_text(c, "@gre@20 coins", 14791);
					packet_set_interface_text(c, "@gre@20 coins", 14792);
				}
				else
				{
					packet_set_interface_text(c, "@red@20 coins", 14789);
					packet_set_interface_text(c, "@red@20 coins", 14790);
					packet_set_interface_text(c, "@red@20 coins", 14791);
					packet_set_interface_text(c, "@red@20 coins", 14792);
				}
			}
			else
			{
				packet_set_interface_text(c, "@red@15 coins", 14787);
			}
		}
		else
		{
			packet_set_interface_text(c, "@red@3 coins", 14786);
		}
	}
	else
	{
		packet_set_interface_text(c, "@red@1 coin", 14785);
		packet_set_interface_text(c, "@red@3 coins", 14786);
		packet_set_interface_text(c, "@red@15 coins", 14787);
		packet_set_interface_text(c, "@red@20 coins", 14789);
		packet_set_interface_text(c, "@red@20 coins", 14790);
		packet_set_interface_text(c, "@red@20 coins", 14791);
		packet_set_interface_text(c, "@red@20 coins", 14792);
	}

	/* Swamp snake hide goes here, later revision.  
	packet_set_interface_text(c, "Snakeskin", 14780);
	packet_set_interface_text(c, "20 coins", 14788);
	*/
	
	packet_set_interface_text(c, "", 14780);
	packet_set_interface_text(c, "", 14788);

	packet_show_interface(c, 14670);
}

/**
 * Shows the menu for choosing what kind of dough the player wants to make. 
 *
 * @param *c Client instance doing the crafting.
 */
void send_cooking_dough_menu(struct client *c)
{
	/* FIXME: Find a choice interface that doesn't have the swords, or remove
	   them. Don't think the usual one 
	packet_set_interface_text(c, "What sort of dough do you wish to make?", 2481);
	*/
	packet_set_interface_text(c, "Select an option", 2481);
	packet_set_interface_text(c, "Bread dough.", 2482);
	packet_set_interface_text(c, "Pastry dough.", 2483);
	packet_set_interface_text(c, "Pizza dough.", 2484);
	packet_set_interface_text(c, "Pitta dough.", 2485);
	packet_show_interface_chatbox(c, 2480);
}

/**
 * Handles sending of all cooking menus.
 *
 * @param *c Client instance doing the crafting.
 * @param menu_id ID number of the menu, food being cooked.
 */
void send_cooking_choice_menu(struct client *c, int menu_id)
{
	int model_id = 0;
	/* Keep this the same as food_name.  */
	int food_name_size = 40;
	char food_name[40];
	int good = 1;

	packet_set_interface_text(c, "What would you like to cook?", 13721);

	switch (menu_id)
	{
		case COOKING_MEAT:
			model_id = 2142;
			strncpy(food_name, "Cooked meat", food_name_size);
			break;
		case COOKING_CHICKEN:
			model_id = 2140;
			strncpy(food_name, "Cooked chicken", food_name_size);
			break;
		case COOKING_RABBIT:
			model_id = 3228;
			strncpy(food_name, "Cooked rabbit", food_name_size);
			break;
		case COOKING_BREAD:
			model_id = 2309;
			strncpy(food_name, "Cooked bread", food_name_size);
			break;
		case COOKING_PITTA_BREAD:
			model_id = 1865;
			strncpy(food_name, "Cooked pitta bread", food_name_size);
			break;
		case COOKING_SHRIMP:
			model_id = 315;
			strncpy(food_name, "Cooked shrimp", food_name_size);
			break;
		case COOKING_KARAMBWANJI:
			model_id = 3151;
			strncpy(food_name, "Cooked karambwanji", food_name_size);
			break;
		case COOKING_SARDINE:
			model_id = 325;
			strncpy(food_name, "Cooked sardine", food_name_size);
			break;
		case COOKING_ANCHOVIES:
			model_id = 319;
			strncpy(food_name, "Cooked anchovies", food_name_size);
			break;
		case COOKING_KARAMBWAN:
			model_id = 3144;
			strncpy(food_name, "Cooked karambwan", food_name_size);
			break;
		case COOKING_HERRING:
			model_id = 347;
			strncpy(food_name, "Cooked herring", food_name_size);
			break;
		case COOKING_MACKEREL:
			model_id = 355;
			strncpy(food_name, "Cooked mackerel", food_name_size);
			break;
		case COOKING_TROUT:
			model_id = 333;
			strncpy(food_name, "Cooked trout", food_name_size);
			break;
		case COOKING_COD:
			model_id = 339;
			strncpy(food_name, "Cooked cod", food_name_size);
			break;
		case COOKING_PIKE:
			model_id = 351;
			strncpy(food_name, "Cooked pike", food_name_size);
			break;
		case COOKING_SALMON:
			model_id = 329;
			strncpy(food_name, "Cooked salmon", food_name_size);
			break;
		case COOKING_SLIMEY_EEL:
			model_id = 3381;
			strncpy(food_name, "Cooked slimey eel", food_name_size);
			break;
		case COOKING_TUNA:
			model_id = 361;
			strncpy(food_name, "Cooked tuna", food_name_size);
			break;
		case COOKING_CAVE_EEL:
			model_id = 5003;
			strncpy(food_name, "Cooked cave eel", food_name_size);
			break;
		case COOKING_LOBSTER:
			model_id = 377;
			strncpy(food_name, "Cooked lobster", food_name_size);
			break;
		case COOKING_BASS:
			model_id = 365;
			strncpy(food_name, "Cooked bass", food_name_size);
			break;
		case COOKING_SWORDFISH:
			model_id = 373;
			strncpy(food_name, "Cooked swordfish", food_name_size);
			break;
		case COOKING_LAVA_EEL:
			model_id = 2149;
			strncpy(food_name, "Cooked lava eel", food_name_size);
			break;
		case COOKING_SHARK:
			model_id = 385;
			strncpy(food_name, "Cooked shark", food_name_size);
			break;
		case COOKING_SEA_TURTLE:
			model_id = 397;
			strncpy(food_name, "Cooked sea turtle", food_name_size);
			break;
		case COOKING_MANTA_RAY:
			model_id = 391;
			strncpy(food_name, "Cooked manta ray", food_name_size);
			break;
		case COOKING_REDBERRY_PIE:
			model_id = 2325;
			strncpy(food_name, "Cooked redberry pie", food_name_size);
			break;
		case COOKING_MEAT_PIE:
			model_id = 2327;
			strncpy(food_name, "Cooked meat pie", food_name_size);
			break;
		case COOKING_APPLE_PIE:
			model_id = 2323;
			strncpy(food_name, "Cooked apple pie", food_name_size);
			break;
		case COOKING_PLAIN_PIZZA:
			model_id = 2289;
			strncpy(food_name, "Cooked plain pizza", food_name_size);
			break;
		case COOKING_CAKE:
			model_id = 1891;
			strncpy(food_name, "Cooked cake", food_name_size);
			break;
		case COOKING_SWEETCORN:
			model_id = 5986;
			strncpy(food_name, "Cooked sweetcorn", food_name_size);
			break;
		default:
			good = 0;
			break;
	}

	if (good == 1)
	{
		packet_set_item_in_interface_model(c, 13716, 200, model_id);
		packet_set_interface_text(c, food_name, 13720);
		packet_show_interface_chatbox(c, 1743);
	}
	else
	{
		printf("No good! Invalid food id: %d\n", menu_id);
		return;
	}
}

/**
 * @brief Handles sending of fletching menus.
 *
 * @param *c Client instance doing the crafting.
 * @param menu_id ID number of the menu, fletching being done.
 * @param arrow_head_id If attaching arrow heads to headless arrows, what
 * id are they.
 */
void send_fletching_choice_menu(struct client *c, int menu_id, 
								int arrow_head_id)
{
	switch (menu_id)
	{
		case FLETCHING_MENU_ADD_FEATHERS:
			/* How many shafts to feather interface.  */
			packet_set_item_in_interface_model(c, 1746, 200, 53);
			packet_set_interface_text(c, "Headless arrow", 2799);
			packet_show_interface_chatbox(c, 4429);
			break;
		case FLETCHING_MENU_ADD_ARROW_HEADS:
			/* How many feathered shafts to add heads interface.  */
			printf("Arrow head id: %d\n", arrow_head_id);

			switch (arrow_head_id)
			{
				/* Bronze arrow heads.  */
				case 39:
					packet_set_item_in_interface_model(c, 1746, 200, 882);
					packet_set_interface_text(c, "Bronze arrow", 2799);
					c->plr->action_id_type = 10;
					break;
				/* Iron arrow heads.  */
				case 40:
					packet_set_item_in_interface_model(c, 1746, 200, 884);
					packet_set_interface_text(c, "Iron arrow", 2799);
					c->plr->action_id_type = 11;
					break;
				/* Steel arrow heads.  */
				case 41:
					packet_set_item_in_interface_model(c, 1746, 200, 886);
					packet_set_interface_text(c, "Steel arrow", 2799);
					c->plr->action_id_type = 12;
					break;
				/* Mithril arrow heads.  */
				case 42:
					packet_set_item_in_interface_model(c, 1746, 200, 888);
					packet_set_interface_text(c, "Mithril arrow", 2799);
					c->plr->action_id_type = 13;
					break;
				/* Adamant arrow heads.  */
				case 43:
					packet_set_item_in_interface_model(c, 1746, 200, 890);
					packet_set_interface_text(c, "Adamant arrow", 2799);
					c->plr->action_id_type = 14;
					break;
				/* Rune arrow heads.  */
				case 44:
					packet_set_item_in_interface_model(c, 1746, 200, 892);
					packet_set_interface_text(c, "Rune arrow", 2799);
					c->plr->action_id_type = 15;
					break;
				/* Wolfbone arrow tips (ogre arrows).  */
				case 2861:
					packet_set_item_in_interface_model(c, 1746, 200, 2866);
					packet_set_interface_text(c, "Ogre arrow", 2799);
					c->plr->action_id_type = 16;
					break;
			}
			packet_show_interface_chatbox(c, 4429);
			break;
		case FLETCHING_MENU_ADD_BOLT_HEADS:
			/* How many gem tips to apply to bolts.  */
			switch (arrow_head_id)
			{
				/* Opal tips.  */
				case 45:
					packet_set_item_in_interface_model(c, 1746, 200, 879);
					packet_set_interface_text(c, "Opal bolt", 2799);
					c->plr->action_id_type = 20;
					break;
				/* Pearl tips.  */
				case 46:
					packet_set_item_in_interface_model(c, 1746, 200, 880);
					packet_set_interface_text(c, "Pearl bolt", 2799);
					c->plr->action_id_type = 21;
					break;
				/* Barbed tips.  */
				case 47:
					packet_set_item_in_interface_model(c, 1746, 200, 881);
					packet_set_interface_text(c, "Barbed bolt", 2799);
					c->plr->action_id_type = 22;
					break;
			}
			packet_show_interface_chatbox(c, 4429);
			break;
		case FLETCHING_MENU_ADD_DART_HEADS:
			/* How many feathers to add to darts interface.  */
			printf("Dart tip id: %d\n", arrow_head_id);
			switch (arrow_head_id)
			{
				/* Bronze arrow heads.  */
				case 819:
					packet_set_item_in_interface_model(c, 1746, 200, 806);
					packet_set_interface_text(c, "Bronze dart", 2799);
					c->plr->action_id_type = 50;
					break;
				/* Iron arrow heads.  */
				case 820:
					packet_set_item_in_interface_model(c, 1746, 200, 807);
					packet_set_interface_text(c, "Iron dart", 2799);
					c->plr->action_id_type = 51;
					break;
				/* Steel arrow heads.  */
				case 821:
					packet_set_item_in_interface_model(c, 1746, 200, 808);
					packet_set_interface_text(c, "Steel dart", 2799);
					c->plr->action_id_type = 52;
					break;
				/* Mithril arrow heads.  */
				case 822:
					packet_set_item_in_interface_model(c, 1746, 200, 809);
					packet_set_interface_text(c, "Mithril dart", 2799);
					c->plr->action_id_type = 53;
					break;
				/* Adamant arrow heads.  */
				case 823:
					packet_set_item_in_interface_model(c, 1746, 200, 810);
					packet_set_interface_text(c, "Adamant dart", 2799);
					c->plr->action_id_type = 54;
					break;
				/* Rune arrow heads.  */
				case 824:
					packet_set_item_in_interface_model(c, 1746, 200, 811);
					packet_set_interface_text(c, "Rune dart", 2799);
					c->plr->action_id_type = 55;
					break;
			}
			packet_show_interface_chatbox(c, 4429);
			break;
		case FLETCHING_MENU_USE_LOGS:
			packet_set_item_in_interface_model(c, 8883, 200, 52);
			packet_set_interface_text(c, "15 arrow shafts", 8889);
			packet_set_item_in_interface_model(c, 8884, 200, 48);
			packet_set_interface_text(c, "Shortbow", 8893);
			packet_set_item_in_interface_model(c, 8885, 200, 50);
			packet_set_interface_text(c, "Longbow", 8897);
			packet_show_interface_chatbox(c, 8880);
			break;
		case FLETCHING_MENU_USE_OAK_LOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 54);
			packet_set_interface_text(c, "Oak Shortbow", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 48);
			packet_set_interface_text(c, "Oak Longbow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;
		case FLETCHING_MENU_USE_WILLOW_LOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 60);
			packet_set_interface_text(c, "Willow Shortbow", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 58);
			packet_set_interface_text(c, "Willow Longbow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;
		case FLETCHING_MENU_USE_MAPLE_LOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 64);
			packet_set_interface_text(c, "Maple Shortbow", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 62);
			packet_set_interface_text(c, "Maple Longbow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;
		case FLETCHING_MENU_USE_YEW_LOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 68);
			packet_set_interface_text(c, "Yew Shortbow", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 66);
			packet_set_interface_text(c, "Yew Longbow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;
		case FLETCHING_MENU_USE_MAGIC_LOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 72);
			packet_set_interface_text(c, "Magic Shortbow", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 70);
			packet_set_interface_text(c, "Magic Longbow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;	
		case FLETCHING_MENU_USE_ACHEYLOGS:
			packet_set_item_in_interface_model(c, 8869, 200, 2864);
			packet_set_interface_text(c, "15 Ogre arrow shafts", 8871);
			packet_set_item_in_interface_model(c, 8870, 200, 4825);
			packet_set_interface_text(c, "Unstrung comp bow", 8875);
			packet_show_interface_chatbox(c, 8866);
			break;	
	}
}

/**
 * @brief Handles sending of crafting menus.
 *
 * @param *c Client instance doing the crafting.
 * @param menu_id ID number of the menu, crafting being done.
 * @param item_id ID of submenus for categories of an item.
 */
void send_crafting_choice_menu(struct client *c, int menu_id, int item_id)
{
	switch (menu_id)
	{
		/* Spinning.  */
		case CRAFTING_ACTION_SPINNING:
			packet_set_item_in_interface_model(c, 8883, 200, 1759);
			packet_set_interface_text(c, "Ball of wool", 8889);
			packet_set_item_in_interface_model(c, 8884, 200, 1777);
			packet_set_interface_text(c, "Bow string", 8893);
			packet_set_item_in_interface_model(c, 8885, 200, 6038);
			packet_set_interface_text(c, "Magic string", 8897);
			packet_show_interface_chatbox(c, 8880);
			break;
		/* Weaving.  */
		case CRAFTING_ACTION_WEAVING:
			packet_set_item_in_interface_model(c, 8883, 200, 3224);
			packet_set_interface_text(c, "Strip of cloth", 8889);
			packet_set_item_in_interface_model(c, 8884, 200, 5418);
			packet_set_interface_text(c, "Empty sack", 8893);
			packet_set_item_in_interface_model(c, 8885, 200, 5376);
			packet_set_interface_text(c, "Basket", 8897);
			packet_show_interface_chatbox(c, 8880);
			break;
		/* Clay. */
		case CRAFTING_ACTION_POTTERY:
			switch (item_id)
			{
				/* FIXME: Note, pot lid may not appear here unless
				   the quest one small favour is done/in progress.  */
				/* Shaping clay.  */
				case 0:
					packet_set_item_in_interface_model(c, 8902, 120, 1787);
					packet_set_interface_text(c, "Pot", 8909);
					packet_set_item_in_interface_model(c, 8903, 150, 1789);
					packet_set_interface_text(c, "Pie dish", 8913);
					packet_set_item_in_interface_model(c, 8904, 120, 1791);
					packet_set_interface_text(c, "Bowl", 8917);
					packet_set_item_in_interface_model(c, 8905, 120, 5352);
					packet_set_interface_text(c, "Plant pot", 8921);
					packet_show_interface_chatbox(c, 8899);
					break;
				/* Firing clay.  */
				case 1:
					packet_set_item_in_interface_model(c, 8902, 120, 1931);
					packet_set_interface_text(c, "Pot", 8909);
					packet_set_item_in_interface_model(c, 8903, 150, 2313);
					packet_set_interface_text(c, "Pie dish", 8913);
					packet_set_item_in_interface_model(c, 8904, 120, 1923);
					packet_set_interface_text(c, "Bowl", 8917);
					packet_set_item_in_interface_model(c, 8905, 120, 5350);
					packet_set_interface_text(c, "Plant pot", 8921);
					packet_show_interface_chatbox(c, 8899);
					break;
			}
			break;	
		/* Tanning leather.  */
		case CRAFTING_ACTION_TANNING:
			show_tanning_menu(c);
			break;	
		/* Leather goods.  */
		case CRAFTING_ACTION_LEATHER:
			switch (item_id)
			{
				/* Soft leather.  */
				case 0:
					packet_show_interface(c, 2311);
					break;
				/* Hard leather.  */
				case 1:
					packet_set_item_in_interface_model(c, 1746, 200, 1131);
					packet_set_interface_text(c, "Hardleather body", 2799);
					packet_show_interface_chatbox(c, 4429);
					break;
				/* Green dragon leather.  */
				case 2:
					packet_set_item_in_interface_model(c, 8883, 200, 1135);
					packet_set_interface_text(c, "Green body", 8889);
					packet_set_item_in_interface_model(c, 8884, 200, 1065);
					packet_set_interface_text(c, "Green vambraces", 8893);
					packet_set_item_in_interface_model(c, 8885, 200, 1099);
					packet_set_interface_text(c, "Green chaps", 8897);
					packet_show_interface_chatbox(c, 8880);
					break;
				/* Blue dragon leather.  */
				case 3:
					packet_set_item_in_interface_model(c, 8883, 200, 2499);
					packet_set_interface_text(c, "Blue body", 8889);
					packet_set_item_in_interface_model(c, 8884, 200, 2487);
					packet_set_interface_text(c, "Blue vambraces", 8893);
					packet_set_item_in_interface_model(c, 8885, 200, 2493);
					packet_set_interface_text(c, "Blue chaps", 8897);
					packet_show_interface_chatbox(c, 8880);
					break;	
				/* Red dragon leather.  */
				case 4:
					packet_set_item_in_interface_model(c, 8883, 200, 2501);
					packet_set_interface_text(c, "Red body", 8889);
					packet_set_item_in_interface_model(c, 8884, 200, 2489);
					packet_set_interface_text(c, "Red vambraces", 8893);
					packet_set_item_in_interface_model(c, 8885, 200, 2495);
					packet_set_interface_text(c, "Red chaps", 8897);
					packet_show_interface_chatbox(c, 8880);
					break;
				/* Black dragon leather.  */
				case 5:
					packet_set_item_in_interface_model(c, 8883, 200, 2503);
					packet_set_interface_text(c, "Black body", 8889);
					packet_set_item_in_interface_model(c, 8884, 200, 2491);
					packet_set_interface_text(c, "Black vambraces", 8893);
					packet_set_item_in_interface_model(c, 8885, 200, 2497);
					packet_set_interface_text(c, "Black chaps", 8897);
					packet_show_interface_chatbox(c, 8880);
					break;
				/* Snakeskin.  */
				case 6:
					packet_set_interface_text(c, "Which snakeskin item would "
									   "you like to make?", 8966);
					packet_set_item_in_interface_model(c, 8941, 200, 6322);
					packet_set_interface_text(c, "Body", 8946);
					packet_set_item_in_interface_model(c, 8942, 200, 6324);
					packet_set_interface_text(c, "Chaps", 8950);
					packet_set_item_in_interface_model(c, 8943, 200, 6330);
					packet_set_interface_text(c, "Vambraces", 8954);
					packet_set_item_in_interface_model(c, 8944, 200, 6326);
					packet_set_interface_text(c, "Bandana", 8958);
					packet_set_item_in_interface_model(c, 8945, 200, 6328);
					packet_set_interface_text(c, "Boots", 8965);
					packet_show_interface_chatbox(c, 8938);
					break;
				/* Add studs to leather bodies.  */
				case 10:
					packet_set_item_in_interface_model(c, 1746, 200, 1133);
					packet_set_interface_text(c, "Studded body", 2799);
					packet_show_interface_chatbox(c, 4429);
					break;
				/* Add studs to leather chaps.  */
				case 11:
					packet_set_item_in_interface_model(c, 1746, 200, 1097);
					packet_set_interface_text(c, "Studded chaps", 2799);
					packet_show_interface_chatbox(c, 4429);
					break;	
			}
			break;	
		/* Making molten glass.  */
		case CRAFTING_ACTION_MOLTEN_GLASS:
			packet_set_item_in_interface_model(c, 1746, 200, 1775);
			packet_set_interface_text(c, "Molten glass", 2799);
			packet_show_interface_chatbox(c, 4429);
			break;
		/* Glass items.  */
		case CRAFTING_ACTION_GLASSBLOWING:
			packet_show_interface_chatbox(c, 11462);
			break;
		/* Cutting gems.  */
		case CRAFTING_ACTION_CUTTING_GEMS:
			switch (item_id)
			{
				/* Opal.  */
				case 1625:
					packet_set_item_in_interface_model(c, 1746, 200, 1609);
					packet_set_interface_text(c, "Opal", 2799);
					c->plr->action_id_type = 130;
					break;
				/* Jade.  */
				case 1627:
					packet_set_item_in_interface_model(c, 1746, 200, 1611);
					packet_set_interface_text(c, "Jade", 2799);
					c->plr->action_id_type = 131;
					break;
				/* Red topaz.  */
				case 1629:
					packet_set_item_in_interface_model(c, 1746, 200, 1613);
					packet_set_interface_text(c, "Red topaz", 2799);
					c->plr->action_id_type = 132;
					break;
				/* Sapphire.  */
				case 1623:
					packet_set_item_in_interface_model(c, 1746, 200, 1607);
					packet_set_interface_text(c, "Sapphire", 2799);
					c->plr->action_id_type = 133;
					break;
				/* Emerald.  */
				case 1621:
					packet_set_item_in_interface_model(c, 1746, 200, 1605);
					packet_set_interface_text(c, "Emerald", 2799);
					c->plr->action_id_type = 134;
					break;
				/* Ruby.  */
				case 1619:
					packet_set_item_in_interface_model(c, 1746, 200, 1603);
					packet_set_interface_text(c, "Ruby", 2799);
					c->plr->action_id_type = 135;
					break;
				/* Diamond.  */
				case 1617:
					packet_set_item_in_interface_model(c, 1746, 200, 1601);
					packet_set_interface_text(c, "Diamond", 2799);
					c->plr->action_id_type = 136;
					break;
				/* Dragonstone.  */
				case 1631:
					packet_set_item_in_interface_model(c, 1746, 200, 1615);
					packet_set_interface_text(c, "Dragonstone", 2799);
					c->plr->action_id_type = 137;
					break;
			}
			packet_show_interface_chatbox(c, 4429);
			break;
		/* Jewelry.  */	
		case CRAFTING_ACTION_MAKING_JEWELRY:
			show_jewelry_menu(c);
			break;	
		case CRAFTING_ACTION_SILVER:
			/* XXX: sickle gets added to this list of things to make
			 * when the player has started the Nature Spirit quest.
			 * FIXME: Add sickle when that quest is made.  */

			packet_set_item_in_interface_model(c, 8883, 200, 1714);
			packet_set_interface_text(c, "Unstrung symbol", 8889);
			packet_set_item_in_interface_model(c, 8884, 200, 1720);
			packet_set_interface_text(c, "Unstrung emblem", 8893);
			packet_set_item_in_interface_model(c, 8885, 200, 5525);
			packet_set_interface_text(c, "Tiara", 8897);
			packet_show_interface_chatbox(c, 8880);
			break;	
	}
}

/* TODO: Change this to a function.  */
/** Holds the interface text id for each slot in the smithing interface.  */
int smithing_menu_level_text_id[5][5] 
	= {{1094, 1085, 1087, 1086, 1088}, 
	   {1091, 1093, 1083, 1092, 8429}, 
	   {1098, 1099, 1100, 1101, 11461}, 
	   {1102, 1103, 1104, 1105, 13358}, 
	   {1107, 1108, 1106, 1096, 1134}};

/** Holds the names for each slot in the smithing interface.  */
char smithing_menu_text[5][5][20]
	= {
	  {{"Dagger"}, {"Sword"}, {"Scimitar"}, {"Long sword"}, {"2 hand sword"}}, 
	  {{"Axe"}, {"Mace"}, {"Warhammer"}, {"Battle axe"}, {"Claws"}}, 
	  {{"Chain body"}, {"Plate legs"}, {"Plate skirt"}, {"Plate body"}, {"Oil lantern frame"}}, 
	  {{"Medium helm"}, {"Full helm"}, {"Square shield"}, {"Kite shield"}, {"Nails"}}, 
	  {{"Dart tips"}, {"Arrowtips"}, {"Throwing knives"}, {"Bronze wire"}, {"Studs"}}
	  };

/**
 * @brief Updates the text in the smithing menu based on metal used, and player
 * smithing level.
 * @param *c Client instance to update the smithing menu.
 * @param metal_bar_id Metal used.
 */
static void smithing_level_text(struct client *c, int metal_bar_id)
{
	char output[100];
	int x, y;
	int smithing_bar_id = get_smithing_bar_id(metal_bar_id);
	int bar_base_level = get_smithing_level_needed(smithing_bar_id);
	int lvl_needed = 0;
	int id_conv = 0;

	c->plr->action_id_type2 = smithing_bar_id;

	/* Show text on the interface for the items that can be smithed.  */
	for (y = 0; y < 5; y++)
	{
		for (x = 0; x < 5; x++)
		{
			/* Hide anything that isn't in the interface for that kind of metal
			 * bar.  */

			/* Bronze.  */
			if (smithing_bar_id == SMITHING_BAR_BRONZE)
			{
				/* Clear the text for lantern, studs, not present with bronze
				 * bars.  */
				if ((y == 2 && x == 4) || (y == 4 && x == 4))
				{
					packet_set_interface_text(c, "", smithing_menu_level_text_id[y][x]);
					continue;
				}
			}
			/* Iron.  */
			if (smithing_bar_id == SMITHING_BAR_IRON)
			{
				/* Clear the text for bronze wire, studs. Not present with iron
				 * bars.  */
				if ((y == 4 && x == 3) || (y == 4 && x == 4))
				{
					packet_set_interface_text(c, "", smithing_menu_level_text_id[y][x]);
					continue;
				}
			}
			/* Steel.  */
			if (smithing_bar_id == SMITHING_BAR_STEEL)
			{
				/* Clear the text for bronze wire, oil lantern frame. Not 
				 * present with steel bars.  */
				if ((y == 2 && x == 4) || (y == 4 && x == 3))
				{
					packet_set_interface_text(c, "", smithing_menu_level_text_id[y][x]);
					continue;
				}
			}
			/* Mithril, adamantite, and rune.  */
			if (smithing_bar_id == SMITHING_BAR_MITHRIL 
				|| smithing_bar_id == SMITHING_BAR_ADAMANT 
				|| smithing_bar_id == SMITHING_BAR_RUNE)
			{
				/* Clear text for wire, oil lantern, and studs. Not present
				 * for these metals.  */
				if ((y == 2 && x == 4) || (y == 4 && x == 3) 
					|| (y == 4 && x == 4))
				{
					packet_set_interface_text(c, "", smithing_menu_level_text_id[y][x]);
					continue;
				}
			}

			/* Display everything else, change color of text to white if level
			 * requirements are met.  */
			id_conv = (100 + (y * 10) + x);
			lvl_needed = get_smithing_level_needed(id_conv) + bar_base_level;

			if (lvl_needed > 99)
			{
				lvl_needed = 99;
			}

			if (c->plr->level[SMITHING_SKILL] >= lvl_needed)
			{
				snprintf(output, 100, "%s%s", 
						 "@whi@", smithing_menu_text[y][x]);
				packet_set_interface_text(c, output, 
								   smithing_menu_level_text_id[y][x]);
			}
			else
			{
				packet_set_interface_text(c, smithing_menu_text[y][x], 
								   smithing_menu_level_text_id[y][x]);
			}
		}
	}
}

/**
 * @brief Checks for metal bar requirements in the smithing menu, shows
 * green text if met, red if not.
 * @param *c Client instance.
 * @param metal_bar_id Metal bar used.
 * @param number_of_bars Number of bars the player has.
 */
static void smithing_menu_bars(struct client *c, int metal_bar_id, 
							   int number_of_bars)
{
	/* Check if the bar requirements are met.  */
	if (number_of_bars >= 5)
	{
		packet_set_interface_text(c, "@gre@5bars", 1112);
	}
	else
	{
		packet_set_interface_text(c, "@red@5bars", 1112);
	}

	if (number_of_bars >= 3)
	{
		packet_set_interface_text(c, "@gre@3bars", 1109);
		packet_set_interface_text(c, "@gre@3bars", 1110);
		packet_set_interface_text(c, "@gre@3bars", 1118);
		packet_set_interface_text(c, "@gre@3bars", 1111);
		packet_set_interface_text(c, "@gre@3bars", 1095);
		packet_set_interface_text(c, "@gre@3bars", 1115);
		packet_set_interface_text(c, "@gre@3bars", 1090);
	}
	else
	{
		packet_set_interface_text(c, "@red@3bars", 1109);
		packet_set_interface_text(c, "@red@3bars", 1110);
		packet_set_interface_text(c, "@red@3bars", 1118);
		packet_set_interface_text(c, "@red@3bars", 1111);
		packet_set_interface_text(c, "@red@3bars", 1095);
		packet_set_interface_text(c, "@red@3bars", 1115);
		packet_set_interface_text(c, "@red@3bars", 1090);
	}

	if (number_of_bars >= 2)
	{
		packet_set_interface_text(c, "@gre@2bars", 1113);
		packet_set_interface_text(c, "@gre@2bars", 1116);
		packet_set_interface_text(c, "@gre@2bars", 1114);
		packet_set_interface_text(c, "@gre@2bars", 1089);
		packet_set_interface_text(c, "@gre@2bars", 8428);
	}
	else
	{
		packet_set_interface_text(c, "@red@2bars", 1113);
		packet_set_interface_text(c, "@red@2bars", 1116);
		packet_set_interface_text(c, "@red@2bars", 1114);
		packet_set_interface_text(c, "@red@2bars", 1089);
		packet_set_interface_text(c, "@red@2bars", 8428);
	}
	if (number_of_bars >= 1)
	{
		packet_set_interface_text(c, "@gre@1bar", 1125);
		packet_set_interface_text(c, "@gre@1bar", 1126);
		packet_set_interface_text(c, "@gre@1bar", 1127);
		packet_set_interface_text(c, "@gre@1bar", 1128);
		packet_set_interface_text(c, "@gre@1bar", 1129);
		packet_set_interface_text(c, "@gre@1bar", 1124);
		packet_set_interface_text(c, "@gre@1bar", 1130);
		packet_set_interface_text(c, "@gre@1bar", 1131);
		packet_set_interface_text(c, "@gre@1bar", 1132);
		packet_set_interface_text(c, "@gre@1bar", 13357);

		/* Oil lantern frame, only with steel bars.  */
		if (metal_bar_id == 2351)
		{
			packet_set_interface_text(c, "@gre@1bar", 11459);
		}
		else
		{
			packet_set_interface_text(c, "", 11459);
		}

		/* Bronze wire, only with bronze bars.  */
		if (metal_bar_id == 2349)
		{
			packet_set_interface_text(c, "@gre@1bar", 1132);
		}
		else
		{
			packet_set_interface_text(c, "", 1132);
		}	

		/* Studs, only with steel bars.  */
		if (metal_bar_id == 2353)
		{
			packet_set_interface_text(c, "@gre@1bar", 1135);
		}
		else
		{
			packet_set_interface_text(c, "", 1135);
		}
	}
	else
	{
		packet_set_interface_text(c, "@red@1bar", 1125);
		packet_set_interface_text(c, "@red@1bar", 1126);
		packet_set_interface_text(c, "@red@1bar", 1127);
		packet_set_interface_text(c, "@red@1bar", 1128);
		packet_set_interface_text(c, "@red@1bar", 1129);
		packet_set_interface_text(c, "@red@1bar", 1124);
		packet_set_interface_text(c, "@red@1bar", 1130);
		packet_set_interface_text(c, "@red@1bar", 1131);
		packet_set_interface_text(c, "@red@1bar", 13357);

		/* Oil lantern frame.  */
		if (metal_bar_id == 2351)
		{
			packet_set_interface_text(c, "@red@1bar", 11459);
		}
		else
		{
			packet_set_interface_text(c, "", 11459);
		}

		/* Bronze wire.  */
		if (metal_bar_id == 2349)
		{
			packet_set_interface_text(c, "@red@1bar", 1132);
		}
		else
		{
			packet_set_interface_text(c, "", 1132);
		}	

		/* Studs.  */
		if (metal_bar_id == 2353)
		{
			packet_set_interface_text(c, "@red@1bar", 1135);
		}
		else
		{
			packet_set_interface_text(c, "", 1135);
		}
	}

}

/**
 * Shows the metal bar smelting menu.
 * @param *c client instance.
 */
void send_smelting_choice_menu(struct client *c)
{
	int smithing_level = c->plr->level[SMITHING_SKILL];
	/* Send the metal bar icons.  */
	packet_set_item_in_interface_model(c, 2405, 150, 2349);
	packet_set_item_in_interface_model(c, 2406, 150, 2351);
	packet_set_item_in_interface_model(c, 2407, 150, 2355);
	packet_set_item_in_interface_model(c, 2409, 150, 2353);
	packet_set_item_in_interface_model(c, 2410, 150, 2357);
	packet_set_item_in_interface_model(c, 2411, 150, 2359);
	packet_set_item_in_interface_model(c, 2412, 150, 2361);
	packet_set_item_in_interface_model(c, 2413, 150, 2363);
	/* There is a stray bit of text on the interface by default for some
	 * reason, clean it up.  */
	packet_set_interface_text(c, "", 4158);
	/* Send the text below the bars. The text should be dark red if they are
	 * not a high enough level to make the bars, black if they can.  */
	/* Bronze is level 1, player can always forge this.  */
	packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Bronze", 3987);
	
	/* Iron bar.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_IRON))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Iron", 3991);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Iron", 3991);
	}
		
	/* Silver.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_SILVER))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Silver", 3995);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Silver", 3995);
	}

	/* Steel.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_STEEL))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Steel", 3999);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Steel", 3999);
	}

	/* Gold.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_GOLD))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Gold", 4003);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Gold", 4003);
	}

	/* Mithril.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_MITHRIL))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Mithril", 7441);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Mithril", 7441);
	}

	/* Adamant.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_ADAMANT))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Adamant", 7446);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Adamant", 7446);
	}

	/* Rune.  */
	if (smithing_level >= get_smithing_level_needed(SMITHING_BAR_RUNE))
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@bla@Rune", 7450);
	}
	else
	{
		packet_set_interface_text(c, "\\n\\n\\n\\n@dre@Rune", 7450);
	}

	packet_show_interface_chatbox(c, 2400);
}

/**
 * Shows the smithing menu for what to make out of metal bars.
 * @param *c client instance.
 * @param metal_bar_id What metal bar they are using.
 */
void send_smithing_choice_menu(struct client *c, int metal_bar_id)
{
	int number_of_bars = player_has_num_items_non_stack(c, metal_bar_id);

	/* Set up the number of bars needed text.  */
	smithing_menu_bars(c, metal_bar_id, number_of_bars);
	smithing_level_text(c, metal_bar_id);

	/* Send all the items for the interface, based on metal bar used.  */
	switch (metal_bar_id)
	{
		/* Bronze bar.  */
		case 2349:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1205, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1277, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1321, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1291, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1307, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1351, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1422, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1337, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1375, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3095, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1103, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1075, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1087, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1117, 1);
			/* Not here for bronze.  */
			packet_set_item_in_interface(c, 1121, 4, -1, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1139, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1155, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1173, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1189, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 4819, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 819, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 39, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 864, 5);
			/* Bronze wire.  */
			packet_set_item_in_interface(c, 1123, 3, 5602, 1);
			/* Not here for bronze.  */
			packet_set_item_in_interface(c, 1123, 4, -1, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		/* Iron bar.  */
		case 2351:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1203, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1279, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1323, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1293, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1309, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1349, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1420, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1335, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1363, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3096, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1101, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1067, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1081, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1115, 1);
			/* Oil lantern frame.  */
			packet_set_item_in_interface(c, 1121, 4, 4540, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1137, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1153, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1175, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1191, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 4820, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 820, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 40, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 863, 5);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 3, -1, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 4, -1, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		/* Steel bar.  */
		case 2353:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1207, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1281, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1325, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1295, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1311, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1353, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1424, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1339, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1365, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3097, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1105, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1069, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1083, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1119, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1121, 4, -1, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1141, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1157, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1177, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1193, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 1539, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 821, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 41, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 865, 5);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 3, -1, 1);
			/* Studs.  */
			packet_set_item_in_interface(c, 1123, 4, 2370, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		/* Mithril bar.  */
		case 2359:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1209, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1285, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1329, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1299, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1315, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1355, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1428, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1343, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1369, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3099, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1109, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1071, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1085, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1121, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1121, 4, -1, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1143, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1159, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1181, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1197, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 4822, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 822, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 42, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 866, 5);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 3, -1, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 4, -1, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		/* Adamantite bar.  */
		case 2361:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1211, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1287, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1331, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1301, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1317, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1357, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1430, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1345, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1371, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3100, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1111, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1073, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1091, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1123, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1121, 4, -1, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1145, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1161, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1183, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1199, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 4823, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 823, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 43, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 867, 5);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 3, -1, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 4, -1, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		/* Runite bar.  */
		case 2363:
		/* First column.  */
			/* Dagger.  */
			packet_set_item_in_interface(c, 1119, 0, 1213, 1);
			/* Sword.  */
			packet_set_item_in_interface(c, 1119, 1, 1289, 1);
			/* Scimitar.  */
			packet_set_item_in_interface(c, 1119, 2, 1333, 1);
			/* Long sword.  */
			packet_set_item_in_interface(c, 1119, 3, 1303, 1);
			/* 2h sword.  */
			packet_set_item_in_interface(c, 1119, 4, 1319, 1);
		/* Second column.  */
			/* Axe.  */
			packet_set_item_in_interface(c, 1120, 0, 1359, 1);
			/* Mace.  */
			packet_set_item_in_interface(c, 1120, 1, 1432, 1);
			/* Warhammer.  */
			packet_set_item_in_interface(c, 1120, 2, 1347, 1);
			/* Battle axe.  */
			packet_set_item_in_interface(c, 1120, 3, 1373, 1);
			/* Claws.  */
			packet_set_item_in_interface(c, 1120, 4, 3101, 1);
		/* Third column.  */
			/* Chain body.  */
			packet_set_item_in_interface(c, 1121, 0, 1113, 1);
			/* Plate legs.  */
			packet_set_item_in_interface(c, 1121, 1, 1079, 1);
			/* Plate skirt.  */
			packet_set_item_in_interface(c, 1121, 2, 1093, 1);
			/* Plate body.  */
			packet_set_item_in_interface(c, 1121, 3, 1127, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1121, 4, -1, 1);
		/* Forth column.  */
			/* Medium helm.  */
			packet_set_item_in_interface(c, 1122, 0, 1147, 1);
			/* Full helm.  */
			packet_set_item_in_interface(c, 1122, 1, 1163, 1);
			/* Square shield.  */
			packet_set_item_in_interface(c, 1122, 2, 1185, 1);
			/* Kite shield.  */
			packet_set_item_in_interface(c, 1122, 3, 1201, 1);
			/* Nails.  */
			packet_set_item_in_interface(c, 1122, 4, 4824, 15);
		/* Fifth column.  */
			/* Dart tips.  */
			packet_set_item_in_interface(c, 1123, 0, 824, 10);
			/* Arrowtips.  */
			packet_set_item_in_interface(c, 1123, 1, 44, 15);
			/* Throwing knives.  */
			packet_set_item_in_interface(c, 1123, 2, 868, 5);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 3, -1, 1);
			/* XXX */
			packet_set_item_in_interface(c, 1123, 4, -1, 1);
			packet_show_interface(c, 994);
			c->plr->interface_open_sub = get_smithing_bar_id(metal_bar_id); 
			break;
		default:
			packet_send_chatbox_message(c, "You don't know how to use those things "
				"together!");
			break;
	}
}

/** Opens the bank interface for the specified client.
 * @param *c Client instance.  */
void send_bank_menu(struct client *c)
{
	packet_reload_items_in_bank_interface(c);
	packet_reload_items_in_bank_sidebar_interface(c);
	packet_show_interface_inventory(c, 5292, 5063);
}

/** Opens the bank deposit box interface for the specified client.
 * @param *c Client instance.  */
void send_bank_deposit_box_menu(struct client *c)
{
	packet_reload_items_in_bank_deposit_box_interface(c);
	packet_show_interface(c, 4465);
}

/** 
 * Opens the shop interface for the specified client, and shows the items for
 * the specific shop.
 * @param *c Client instance.
 * @param shop_id ID of the store.
 */
void send_shop_menu(struct client *c, int shop_id)
{
	int x = 0, found = 0;

	for (x = 0; x < NUM_SHOPS; x++)
	{
		if (shop_id == SHOP_DATA[x]->id)
		{
			/* Found matching shop, skip.  */
			found = 1;
			break;	
		}
	}

	if (found == 0)
	{
		printf("No valid shop of id: %d\n", shop_id);
		return;
	}

	packet_set_interface_text(c, SHOP_DATA[x]->name, 3901);

	packet_reload_items_in_shop_interface(c, x);
	packet_reload_items_in_shop_sidebar_interface(c);
	packet_show_interface_inventory(c, 3824, 13293);
	/* Track what store the player is looking at.  */
	c->plr->interface_open_sub = x;
}

static void send_skill_guide_row(struct client *c, int row_num, int item_id,
								 int level, char *text)
{
	char temp[200];

	if (level == -1)
	{
		packet_set_interface_text(c, "", 8720 + row_num);

		packet_set_interface_text(c, "", 8760 + row_num);

		packet_set_item_in_interface(c, 8847, row_num, -1, 0);
	}
	else
	{
		snprintf(temp, 200, "@dre@%d", level);
		packet_set_interface_text(c, temp, 8720 + row_num);

		snprintf(temp, 200, "@dre@%s", text);
		packet_set_interface_text(c, temp, 8760 + row_num);

		packet_set_item_in_interface(c, 8847, row_num, item_id, 1);
	}
}

static void clear_skill_guide_rows(struct client *c, int start)
{
	/* Clear rows in skill guide.  */
	for (int x = start; x < 40; x++)
	{
		send_skill_guide_row(c, x, -1, -1, "");
	}
}

/**
 * Sends the skill guide for a specific skill. Also shows the specific page,
 * if it exists.
 * @param *c Client instance.
 * @param skill ID of the skill to show the guide for.
 * @param page Page number of the guide, 0 - 9.
 * @todo Most skills need to be implemented, may not be 100% accurate. Need to
 * find more videos of the guide being used.
 */
void send_skill_guide(struct client *c, int skill, int page)
{
	c->plr->interface_open_sub = skill;
	packet_set_interface_text(c, "Level", 8718);
	packet_set_interface_text(c, "Advancement", 8719);
	/* Clear member line.  */
	packet_set_interface_text(c, "", 8849);

	/* Options.  */
	packet_set_interface_text(c, "", 8846);
	packet_set_interface_text(c, "", 8823);
	packet_set_interface_text(c, "", 8824);
	packet_set_interface_text(c, "", 8827);
	packet_set_interface_text(c, "", 8837);
	packet_set_interface_text(c, "", 8840);
	packet_set_interface_text(c, "", 8843);
	packet_set_interface_text(c, "", 8859);
	packet_set_interface_text(c, "", 8862);
	packet_set_interface_text(c, "", 8865);

	switch (skill)
	{
		case ATTACK_SKILL:
			packet_set_interface_text(c, "Attack", 8716);
			packet_set_interface_text(c, "Weapons", 8846);

			switch (page)
			{
				case 0:
					send_skill_guide_row(c, 0, 1205, 1, "Bronze");
					send_skill_guide_row(c, 1, 1203, 1, "Iron");
					send_skill_guide_row(c, 2, 1207, 5, "Steel");
					send_skill_guide_row(c, 3, 1217, 10, "Black");
					send_skill_guide_row(c, 4, 1209, 20, "Mithril");
					send_skill_guide_row(c, 5, 1211, 30, "Adamant");
					send_skill_guide_row(c, 6, 1391, 30, 
										 "Members: Battlestaves "
										 "(with 30 Magic)");
					send_skill_guide_row(c, 7, 1213, 40, "Rune");
					send_skill_guide_row(c, 8, 1405, 40, 
										 "Members: Mystic staves "
										 "(with 40 Magic)");
					send_skill_guide_row(c, 9, 4153, 50, 
										 "Members: Granite maul "
										 "(with 50 Strength)");
					send_skill_guide_row(c, 10, 1215, 60, "Members: Dragon");
					send_skill_guide_row(c, 11, 4151, 70, 
										 "Members: Abyssal whip");
					clear_skill_guide_rows(c, 12);
					break;
			}
			break;
		case DEFENCE_SKILL:
			packet_set_interface_text(c, "Defence", 8716);
			packet_set_interface_text(c, "Armor", 8846);

			switch (page)
			{
				case 0:
					send_skill_guide_row(c, 0, 1155, 1, "Bronze");
					send_skill_guide_row(c, 1, 1153, 1, "Iron");
					send_skill_guide_row(c, 2, 1129, 1, "Leather");
					send_skill_guide_row(c, 3, 1157, 5, "Steel");
					send_skill_guide_row(c, 4, 1165, 10, "Black");
					send_skill_guide_row(c, 5, 1159, 20, "Mithril");
					send_skill_guide_row(c, 6, 1161, 30, "Adamant");
					send_skill_guide_row(c, 7, 1163, 40, "Rune");
					send_skill_guide_row(c, 8, 3122, 50, 
										 "Members: Granite shield "
										 "(with 50 Strength)");
					send_skill_guide_row(c, 9, 1149, 60, "Members: Dragon");
					send_skill_guide_row(c, 10, 4224, 70, 
										 "Members: Crystal shield");
					clear_skill_guide_rows(c, 11);
					break;
			}
			break;
		case STRENGTH_SKILL:
		case HITPOINTS_SKILL:
		case RANGED_SKILL:
		case PRAYER_SKILL:
		case MAGIC_SKILL:
		case COOKING_SKILL:
		case WOODCUTTING_SKILL:
		case FLETCHING_SKILL:
		case FISHING_SKILL:
		case FIREMAKING_SKILL:
		case CRAFTING_SKILL:
		case SMITHING_SKILL:
		case MINING_SKILL:
			packet_set_interface_text(c, "Mining", 8716);
			packet_set_interface_text(c, "Rocks", 8846);

			switch (page)
			{
				case 0:
					send_skill_guide_row(c, 0, 1436, 1, "Rune essence (after Rune Mysteries quest)");
					send_skill_guide_row(c, 1, 434, 1, "Clay");
					send_skill_guide_row(c, 2, 436, 1, "Copper ore");
					send_skill_guide_row(c, 3, 438, 1, "Tin ore");
					send_skill_guide_row(c, 4, 668, 10, "Blurite ore");
					send_skill_guide_row(c, 5, 3211, 10, "Members: Limestone");
					send_skill_guide_row(c, 6, 440, 15, "Iron ore");
					send_skill_guide_row(c, 7, 442, 20, "Silver ore");
					send_skill_guide_row(c, 8, 453, 30, "Coal");
					send_skill_guide_row(c, 9, 444, 40, "Gold ore");
					send_skill_guide_row(c, 10, 1625, 40, "Members: Gemstones");
					send_skill_guide_row(c, 11, 447, 55, "Mithril");
					send_skill_guide_row(c, 12, 449, 70, "Adamant");
					send_skill_guide_row(c, 13, 451, 85, "Rune");
					clear_skill_guide_rows(c, 14);
					break;
			}
			break;
		case HERBLORE_SKILL:
		case AGILITY_SKILL:
		case THIEVING_SKILL:
		case SLAYER_SKILL:
		case FARMING_SKILL:
		case RUNECRAFTING_SKILL:
			break;
	}

	/* Level, description.  */

	packet_show_interface(c, 8714);
}
