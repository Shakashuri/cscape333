# Makefile for CScape333
#
# CScape333
# Copyright (C) 2016-2023 Shakashuri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL = /bin/sh
CC = gcc
CFLAGS = -Wall -Wextra -MMD
EXTRA_CFLAGS = 	-std=gnu99 -Wpedantic -pedantic-errors \
				-Wcast-align -Wcast-qual -Wdisabled-optimization -Wformat=2 \
				-Winit-self -Wlogical-op -Wmissing-include-dirs \
				-Wredundant-decls -Wshadow -Wstrict-overflow=2 -Wundef \
				-Wunused -fdiagnostics-show-option \
				-Wuninitialized -Wmaybe-uninitialized -Wshift-overflow=2 \
				-Wshadow

# Math and zlib.
LIBS = -lm -lz
# Set directory to hold the object files, and dependency files.
OBJDIR = bin
# Automatically find all source files, get object and dependency files from it.
SRC = $(wildcard *.c)
OBJ = $(addprefix $(OBJDIR)/,$(SRC:.c=.o))
DEP = $(addprefix $(OBJDIR)/,$(SRC:.c=.d))
OUT_NAME = cscape333

# Rule for compiling object files.
$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) $(LIBS) -g -c $< -o $@

# Link object files to the executable.
all: $(OBJ)	
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) $(LIBS) $(OBJ) -g -o $(OUT_NAME)

# Delete the bin folder and the executable.
.PHONY: all clean clean_dox
clean:
	$(RM) -r $(OBJDIR) $(OUT_NAME)

# Delete doxygen documentation.
clean_dox:
	$(RM) -r dox

# Create the OBJDIR folder if needed.
$(OBJ): | $(OBJDIR)

$(OBJDIR):
	@mkdir $(OBJDIR)

-include $(DEP)
