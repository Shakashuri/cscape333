#ifndef _SKILL_HERBLORE_H_
#define _SKILL_HERBLORE_H_

int get_herblore_level_needed(int herblore_id);
float get_herblore_xp(int  herblore_id);
int get_herblore_id_from_herb(int herb_id);
int get_herblore_id_from_herb_coconut(int herb_id);
int get_herblore_unidentified_herb_id(int herblore_id);
int get_herblore_identified_herb_id(int herblore_id);
int get_herblore_id_from_vial_ingredient(int vial_id, int ingredient_id);
int get_herblore_vial_id(int herblore_id);

void identify_herb(struct client *c, int herb_id, int herb_slot);
void add_first_ingredient_to_vial(struct client *c, int vial_id, int herb_id);
void add_second_ingredient_to_vial(struct client *c, int vial_id, 
								   int ingredient_id);
#endif
