/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file regions.c
 * @brief Contains functions handling map regions.
 */
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

#include "constants.h"

#include "regions.h"

/** 4D array stores collision data for each square of the map in the game.  */
unsigned int ****MAP_DATA;

/**
 * Takes coordinates and returns the region id that they are located at.
 * @param x, y coordinates to find region id from.
 * @return returns the region id where x and y are located.
 */
int get_region_id(int x, int y)
{
	int region_x, region_y;

	region_x = (x >> 3);
	region_y = (y >> 3);

	return (((region_x / 8) << 8) + (region_y / 8));
}

/**
 * Takes a world_x or world_y coord and returns the region coords.
 * @param coord can be an x or y coordinate.
 * @return returns the region x or y coordinate where coord is.
 */
int get_region_coord(int coord)
{
	return (((coord >> 3) - 6) / 8);
}

/**
 * Searches for a region_list entry that matches the passed id.
 * @param reg_id region id to search for.
 * @return returns the index of the region_list entry matching the passed
 * reg_id variable. returns -1 if not able to be found.
 */
int get_region_index(int reg_id)
{
	int x = 0;

	/* get what region struct in the region_list array contains data
	   for the region we are getting data for.  */
	for (; x < REGION_LIST_NUM; x++)
	{
		if (REGION_LIST[x]->id == reg_id)
		{
			return x;
		}
	}
	
	return -1;
}


/**
 * if a region will have data in it, creates the array. this is done on
 * a case by case basis to save memory usage.
 * @param region_id region id to calloc.
 */
void calloc_map_region(int region_id)
{
	int x, z;

	for (z = 0; z < 4; z++)
	{
		/* Mallocs the z of the 4D array.  */
		if ((MAP_DATA[region_id][z] = calloc(64, sizeof(unsigned int*))) 
			== NULL)
		{
			printf("ERROR! Y\n");
			return;
		}
		
		/* Mallocs the x of the 4D array.  */
		for (x = 0; x < 64; x++)
		{
			if ((MAP_DATA[region_id][z][x] = calloc(64, sizeof(unsigned int))) 
				== NULL)
			{
				printf("ERROR! Z: %d X: %d\n", z, x);
				return;
			}
		}
	}
}

int load_map_level(int height)
{
	unsigned int x = 0, y = 0, mask = 0;
	char read_string[200];
	gzFile fp = NULL;
	
	if (height == 0)
	{
		printf("Reading map data...\n");
		printf("Reading ground level...\n");
		fp = gzopen(MAP_DATA_LIST_0, "r");
	}
	else if (height == 1)
	{
		printf("Reading first level...\n");
		fp = gzopen(MAP_DATA_LIST_1, "r");
	}
	else if (height == 2)
	{
		printf("Reading second level...\n");
		fp = gzopen(MAP_DATA_LIST_2, "r");
	}
	else if (height == 3)
	{
		printf("Reading third level...\n");
		fp = gzopen(MAP_DATA_LIST_3, "r");
	}

	if (fp != NULL)
	{
		while (gzgets(fp, read_string, 200) != NULL)
		{
			sscanf(read_string, "%u\t%u\t%u", &x, &y, &mask);
			add_collision(x, y, height, mask);
		}
		gzclose(fp);
		return 0;
	}
	else
	{
		printf("Failed to load map level %d!\n", height);
		return 1;
	}
}

/**
 * Attempts to load each level of the collision map data.
 * @return 0 on success, anything else on failure.
 * @note Failure on this function is treated as a fatal error, and closes
 * the server with an error message.
 */
int load_map_data(void)
{
	int x;
	int rv = 0;

	/* Callocs the number of regions needed for storage.  */
	if ((MAP_DATA = calloc(PROTOCOL_MAX_REGIONS, sizeof(unsigned int***))) 
		== NULL)
	{
		printf("Error in allocating 4D array pointer for map data!\n");
		return 1;
	}
	
	/* Callocs all the height levels for each region.  */
	for (x = 0; x < PROTOCOL_MAX_REGIONS; x++)
	{
		MAP_DATA[x] = calloc(4, sizeof(unsigned int**));
		if (MAP_DATA[x] == NULL)
		{
			printf("Error in allocating spaces for map data!\n");
			exit(1);
		}
	}
	
	/* Load each level of map data.  */
	for (x = 0; x < 4; x++)
	{
		rv = load_map_level(x);
		if (rv != 0)
		{
			printf("Please check that all map data files are in the data "
				   "folder\nand have not been corrupted.\n");
			exit(1);
		}
	}

	printf("Finished reading map data!\n");
	return 0;
}


/* Free the REGION_LIST, MAP_DATA, and REGION_MAP arrays.  */
void free_map_data(struct region_data **region_list, 
				   struct region_data ***region_map)
{
	int x, y, z;

	for (x = 0; x < REGION_LIST_NUM; x++)
	{
		/* Make sure we free the world objects for each region.  */
		for (y = 0; y < region_list[x]->obj_spawns_size; y++)
		{
			free(region_list[x]->OBJECTS[y]);
		}
		free(region_list[x]->OBJECTS);
		free(region_list[x]);
	}
	free(region_list);

	for (x = 0; x < PROTOCOL_MAX_REGIONS; x++)
	{
		if (MAP_DATA[x][0] != NULL)
		{
			for (z = 0; z < 4; z++)
			{
				for (y = 0; y < 64; y++)
				{
					free(MAP_DATA[x][z][y]);
				}
				free(MAP_DATA[x][z]);
				MAP_DATA[x][z] = NULL;
			}
		}
		if (MAP_DATA[x] != NULL)
		{
			free(MAP_DATA[x]);
		}
	}
	free(MAP_DATA);

	for (x = 0; x < REGION_MAP_SIZE_X; x++)
	{
		free(region_map[x]);
	}
	free(region_map);
}

/**
 * Adds the specified collision mask at a specific point in the map data.
 * @param x, y, height Location of the collision.
 * @param mask Collision value to add.
 */
void add_collision(int x, int y, int height, int mask)
{
	if (x < 0 || y < 0 || height < 0 || height > 3)
	{
		return;
	}

	int region_x, region_y, region_id;

	region_x = (x >> 3);
	region_y = (y >> 3);
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < PROTOCOL_MAX_REGIONS && region_id > 0)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (MAP_DATA[region_id][height] == NULL)
		{
			calloc_map_region(region_id);
		}

		if ((x - region_world_x < 0) || (y - region_world_y < 0))
		{
			return;
		}
		
		MAP_DATA[region_id][height][x - region_world_x][y - region_world_y] 
		|= mask;
	}
}

/** 
 * Returns the collision data for a specific point in the map.
 * @param x, y, height Location to get collision data from.
 * @return Collision data if found, 0 if none exists.
 */
int get_collision(int x, int y, int height)
{
	if (x < 0 || y < 0 || height < 0 || height > 3)
	{
		return 0;
	}

	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < PROTOCOL_MAX_REGIONS && region_id >= 0)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (MAP_DATA[region_id][height] == NULL || (x - region_world_x < 0)
			|| (y - region_world_y < 0))
		{
			return 0;
		}
		
		return MAP_DATA[region_id][height][x - region_world_x][y - region_world_y];
	}
	return 0;
}

/**
 * Changes the collision data for a cell to a specified value.
 * @param x, y, height Location of collision.
 * @param new_value New collision value to place at the location.
 * @return 1 on change was able to be done, 0 if not.
 */
int overwrite_collision(int x, int y, int height, int new_value)
{
	if (x < 0 || y < 0 || height < 0 || height > 3)
	{
		return 0;
	}

	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < PROTOCOL_MAX_REGIONS && region_id >= 0)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (MAP_DATA[region_id][height] == NULL
			|| (x - region_world_x < 0) || (y - region_world_y < 0))
		{
			return 0;
		}
		
		MAP_DATA[region_id][height][x - region_world_x][y - region_world_y]
		= new_value;
		return 1;
	}
	return 0;
}

/**
 * Modifies the collision data for a cell, toggles values given.
 * @param x, y, height Location of collision.
 * @param change What bits to toggle.
 * @return 1 if changed, 0 if unable to.
 */
int mod_collision(int x, int y, int height, int change)
{
	if (x < 0 || y < 0 || height < 0 || height > 3)
	{
		return 0;
	}

	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < PROTOCOL_MAX_REGIONS && region_id >= 0)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;
		
		if (MAP_DATA[region_id][height] == NULL
			|| (x - region_world_x < 0) || (y - region_world_y < 0))
		{
			return 0;
		}
		
		MAP_DATA[region_id][height][x - region_world_x][y - region_world_y]
		^= change;

		return 1;
	}
	return 0;
}

/**
 * Modifies the collision data for a cell, clear values given.
 * @param x, y, height Location of collision.
 * @param change What bits to clear.
 * @return 1 if changed, 0 if unable to.
 */
int clear_collision_bit(int x, int y, int height, int change)
{
	if (x < 0 || y < 0 || height < 0 || height > 3)
	{
		return 0;
	}

	int region_x, region_y, region_id;
	
	region_x = x >> 3;
	region_y = y >> 3;
	region_id = ((region_x / 8) << 8) + (region_y / 8);

	if (region_id < PROTOCOL_MAX_REGIONS && region_id >= 0)
	{
		int region_world_x, region_world_y;
		region_world_x = (region_id >> 8) * 64;
		region_world_y = (region_id & 0xff) * 64;

		if (MAP_DATA[region_id][height] == NULL
			|| (x - region_world_x < 0) || (y - region_world_y < 0))
		{
			return 0;
		}

		MAP_DATA[region_id][height][x - region_world_x][y - region_world_y]
		&= ~(change);

		return 1;
	}
	return 0;
}
