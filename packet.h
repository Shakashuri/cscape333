/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PACKET_H_
#define _PACKET_H_

int get_packet_size(int packet_id);

void packet_show_interface(struct client *c, int interface_id);
void packet_show_interface_inventory(struct client *c, int interface_id, 
							  int inv_interface_id);
void packet_show_interface_walkable(struct client *c, int interface_id);
void packet_show_interface_chatbox(struct client *c, int interface_id);
void packet_show_sidebar_interface(struct client *c, int tab_id, int interface_id);
void packet_send_chatbox_message(struct client *c, char string[]);
void packet_set_interface_npc_head(struct client *c, int npc_id, int interface_id);
void packet_set_interface_player_head(struct client *c, int interface_id);
void packet_set_right_click_menu_options(struct client *c, int command_slot, int top, char string[]);

void set_interface_model_anim(struct client *c, int interface_id, int anim);
void packet_set_interface_text(struct client *c, char string[], int interface_id); 
void packet_set_interface_text_int(struct client *c, int data, int interface_id); 

void packet_close_all_open_interfaces(struct client *c);
void packet_send_choose_amount_chatbox_interface(struct client *c, int close_everything);

void packet_set_chat_options(struct client *c, int pub, int pri, int trade);
void packet_set_client_config_value(struct client *c, int id, int value); 
void packet_set_client_config_value_int(struct client *c, int id, int value); 

void packet_send_run_energy(struct client *c);


void packet_set_item_in_interface_model(struct client *c, int interface_id, int zoom, int item_id);

void player_play_animation(struct client *c, int emote);
void player_play_animation_repeats(struct client *c, int emote, int time);
void player_play_animation_no_overwrite(struct client *c, int emote, int time);
void player_replay_animation(struct client *c); 
void player_reset_animation(struct client *c);

void set_phase_movement(struct client *c, int start_x, int start_y, 
						int end_x, int end_y, int start_time, int end_time,
						int direction);

void set_timed_action(struct client *c, int type, int ticks);
void reset_timed_action(struct client *c);


void packet_draw_item_in_world(struct client *c, int drop_index); 
void packet_erase_item_from_world(struct client *c, int drop_index);
void packet_draw_object_in_world(struct client *c, int obj_index);
void packet_erase_object_from_world(struct client *c, int obj_index);
void packet_draw_object_in_world_custom(struct client *c, int x, int y, 
								   int obj_type, int face, int id);
void packet_erase_object_from_world_custom(struct client *c, int x, int y, 
								   int obj_type, int face);
void packet_reload_items_in_interface(struct client *c, int write_frame); 
void packet_reload_items_in_inventory_interface(struct client *c);
void packet_reload_items_in_equipment_interface(struct client *c);
void packet_reload_items_in_bank_interface(struct client *c);
void packet_reload_items_in_bank_sidebar_interface(struct client *c);
void packet_reload_items_in_bank_deposit_box_interface(struct client *c);
void packet_reload_items_in_shop_interface(struct client *c, int shop_id);
void packet_reload_items_in_shop_sidebar_interface(struct client *c);
void packet_reload_items_in_shop_interface_globally(struct client *c, int shop_idx);
void packet_set_item_in_interface(struct client *c, int interface_id, int slot,
						   int item_id, int stack_size);
void player_reset_autocasting(struct client *c);
int packet_player_drop_item_on_ground(struct client *c, int item_id, int interface_id, int slot);
void packet_send_total_carried_weight(struct client *c);
void packet_send_membership_status(struct client *c, int is_member);

void packet_player_update_viewport_equipment_bonuses(struct client *c);

void packet_player_change_appearance(struct client *c);

void packet_player_walking(struct client *c, int opcode); 
void packet_reset_minimap_flag(struct client *c);
void packet_player_disconnect(struct client *c);
void packet_music_player_disconnect(struct client *c);

void packet_draw_gfx_in_world(struct client *c, int gfx_id, int x, int y, 
						 int draw_height, int time);
void packet_draw_gfx_in_world_globally(struct client *c, int gfx_id, int x, int y, 
								  int draw_height, int time);

void packet_play_song(struct client *c, int song);
void packet_queue_song(struct client *c, int song);
void packet_play_jingle(struct client *c, int jingle);
void packet_play_sfx(struct client *c, int sound, int delay, int volume);

/* Please kill me.  */
void 
packet_create_projectile(struct client *c, int x, int y, int off_x, int off_y,
				  int angle, int speed, int gfx_moving, int start_height,
				  int end_height, int lockon, int time, int slope);

/* Likewise.  */
void 
packet_create_projectile_global(struct client *c, int x, int y, int off_x, int off_y,
						 int angle, int speed, int gfx_moving, 
						 int start_height, int end_height, int lockon, 
						 int time, int slope);
#endif
