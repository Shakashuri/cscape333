/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLIENT_H_
#define _CLIENT_H_

/**
 * @brief Holds all low-level info for each client. Socket, buffers, etc.
 */
struct client
{
	/** Network socket used.  */
	int socket;
	/** Is the client active.  */
	int is_active;
	/** Has the client been setup.  */
	int initialized;
	/** At what part of the login process is this client at?  */
	int login_state;
	/** If a part fails, how long until another attempt.  */
	int login_delay;
	/** Index of client in the user array.  */
	int login_id;
	/** When loging in, holds the size of the packet to get.  */
	int login_packet_size;

	/** 
	 * IP address of the player.
	 *
	 * @note May need to change length for IPv6, maybe add another array to
	 * hold ipv6 address.
	 */
	char ip_addr[20];
	/** Stores number of bytes read.  */
	int bytes_read;
	/** Stores byte buffer coming from the client.  */
	char instream[BUFFER_SIZE];
	/** Offset when reading the instream.  */
	int instream_offset;
	/** Stores byte buffer going out to the client.  */
	char outstream[BUFFER_SIZE];
	/** Offset when reading the outstream.  */
	int outstream_offset;

	/** Buffer holding data when player appearance needs to be updated.  */
	char player_props[PLAYER_PROPS_SIZE];
	/** Offset when reading player_props.  */
	int player_props_offset;
	/** Stores packet length for variable sized packets.  
	 * @todo Possibly rename this to something more intuitive. 
	 */
	int frame_stack[FRAME_STACK_SIZE];
	/** Stores index when reading frame_stack.  */
	int frame_stack_ptr;

	/* Last packets, used for debug data.  */
	/** Current packet id being processed.  */
	int current_packet;
	/** Last packets that were recieved from the client.  */
	int last_packets[LAST_PACKETS_SIZE];

	/* Bit ints.  */
	/** For sending bits.  */
	int bit_instream;
	/** For reading bits.  */
	int bit_outstream;

	/* ISAAC crypt structs.  */
	/** ISAAC packet decryption data.  */
	struct isaac *decrypt;
	/** ISAAC packet encryption data.  */
	struct isaac *encrypt;

	/** Pointer to the client's player struct, holds all game data.  */
	struct player *plr;
	/** Index of the used music client.  */
	int music_idx;
};

/** Music client.  */
struct music_client
{
	/** Player name.  */
	char name[13];
	/** Lowercase name used for savefile.  */
	char file_name[13];
	/** Player password.  */
	char password[21];
	/** Player id.  */
	int id;	
	/** Where the player data is saved.  */
	char save_location[40];

	/** At what part of the login process is this client at?  */
	int login_state;
	/** If a part fails, how long until another attempt.  */
	int login_delay;
	/** Index of client in the user array.  */
	int login_id;
	/** When loging in, holds the size of the packet to get.  */
	int login_packet_size;

	/** Network socket for client.  */
	int socket;
	/** If client instance has been initally setup or not.  */
	int initialized;
	/** IP address of client.  */
	char ip_addr[20];
	/** How many bytes have been read in from the client.  */
	int bytes_read;	
	/** Buffer holding data from the client.  */
	char instream[MUSIC_BUFFER_SIZE];
	/** Buffer holding data to go out to the client.  */
	char outstream[MUSIC_BUFFER_SIZE];
	/** Current position in the instream buffer.  */
	int instream_offset;
	/** Current position in the outstream buffer.  */
	int outstream_offset;
};

struct client *load_connection(int socket_fd);
struct music_client *load_music_connection(int socket_fd);
int get_client_socket(int socket_fd);
int get_ondemand_client_socket(int socket_fd);
int get_music_client_socket(int socket_fd);

void initialize_login_stats(struct client *c);
void initialize_new_login(struct client *c);
int free_player_data(int client_index);
int clean_up_connection(int index);

#endif

