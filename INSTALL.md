Installing
==========

## Requirements:  

	zlib (Server)
	bzlib (Extraction Utilities)
	333 Client Cache (Not included, read below)

The server requires some basic data files before it can be used. Due to
copyright issues, these files cannot be distributed with the server and must
be acquired by the user. These files include collision data, object location
data, and region list data. These can all be extracted from the data from a
RuneScape client cache, version 333. 

**NOTE: The community commonly refers to the client version as 317, but the
data that comes with it is actually a 333 version. It may be easier to find if 
you look for 317 cache instead of 333.**

Tools exist in the utils/ folder made to extract the needed data and convert it
into formats usable by the server.

## Building the Tools:

First, all of the extraction tools must be built to be used. To do this, cd 
into the "utils" folder and do "sh make_extract_tools.sh". This will build all
of the tools and place them in the utils/bin folder. From there, all of the
files in the bin folder need to be placed in the same directory as a client
cache. 

**Example:**

	cd utils
	sh make_extract_tools.sh

For more details about the tools, check their [README](utils/README_UTILS.md).

## Extracting Data:

When you have acquired a 333 client cache to get the data from, copy the 
contents of the utils/bin folder to where it is located. Then, execute 
"sh run_extract_tools.sh". This will run all of the tools in the order they 
need to be run, and get the files needed. After extraction, the following files
need to be copied to the cscape/data/ folder. The server will then be ready to
be used.

**Example:**

	cp utils/bin/* ~/333_Cache_Directory/
	cd ~/333_Cache_Directory
	sh run_extract_tools.sh

Files needed to be copied over:

	intr_data.dat
	map0.gz
	map1.gz
	map2.gz
	map3.gz
	obj_locs.tsv
	region_list.tsv

## Configuration:

Once the data files are copied over, the server can now be configured. Located
in the data folder is the file "server.conf.default". This must be 
copied/renamed to "server.conf" and then edited so the server will run however
is desired. The config file contains comments on what the defaults are, and all
defaults are how RuneScape originally handles it.

After that, the server is fully setup and can be started.
To run the server, go to the directory that the CScape binary is located, 
and execute it.

	./cscape333

## Connection:

Now that the server is setup and running, it can now be connected to. This is
done using a 333 RuneScape client that is pointed to the server's IP address 
and port it is running on. No client can be legally provided with the server,
but can be found online. Accounts are created by entering the desired username
and password in the client and trying to login. If there is no account with
the username that already exists, it is created. Once your account is created,
you may want to enable admin commands on it for testing. To do so, the save
file for your character needs to be edited. The typical layout for a player
save file is the following:

**Save file:**

	1
	Asdf
	test
	0
	0
	...

Order is save file version number, username, password, player rights, and sex.
Player rights are changed by altering the first 0 to a 1 for moderator rights,
or a 2 for admin rights.

**After admin rights enabled:**

	1
	Asdf
	test
	2
	0
	...

Editing the player file like so will allow admin commands to be used. All 
commands are prefixed by two colons, and entered in the chatbox. Note that the
save file **must** be edited when logged out, or the changes will not take 
effect. 

Some example commands:

	::tele x_coordinate y_coordinate [z coordinate]
		Teleport to certain coordinates.
	::item item_id [count] [charges]
		Spawn an item in your inventory.
	::spawn
		Teleport back to the your spawn location.
	::emote animation_id
		Make yourself do a certain animation.
	::pos
		Get current coordinates.

	Arguments in square brackets are optional.

This is not a complete list of commands, and more can be found in the 
packet_handler.c file, at the command_handler function.

Now, everything is all setup and ready to use!
