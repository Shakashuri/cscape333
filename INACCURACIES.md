Inaccuracies
============

This is a list of all *known* inaccuracies in the game. This is different from
the [TODO](TODO.md) list, as features can be implemented, but due to a lack of
solid information on some parts of the mechanics, they may not be entirely 
accurate to how the real game handles it. The game may be 100% playable in 
time, but this will document the things that may differ from the real 
experience so that they can be corrected later.

Do not put features in here that have not been implemented yet. An agility
course not being implemented yet is not an inaccuracy, but something to be
done. If when the agility course is implemented, and the xp bonus for doing
the course in order is not implemented, or doesn't work, then that is an 
inaccuracy.

**Combat**

* When doing a ranged attack if the player is out of range, they will then
	move towards the target until they are in range, stop, then attack.
	This is currently not handled yet.
* Need to re-evaluate that all the formulas are correct. See this as a good
    reference:
    * https://web.archive.org/web/20190905124128/http://webcache.googleusercontent.com/search?q=cache:http://services.runescape.com/m=forum/forums.ws?317,318,712,65587452

**NPCS**

* NPC random walking is not accurate. NPCs can random walk a few spaces at a
	time from what I have seen, and sometimes with less delay.
* NPC maximum wander and aggression distance needs investigation. The values in
	place right now are guesses, and not 100% accurate.
* Currently, NPCs do not move as soon as a ranged attack is shot against them,
	it seems like they do still move until they are hit.
* There are many locations in the game that do not have proper NPCs spawning
	there yet. Many areas are empty, some may have incorrect spawns. Once
	specifics about this is found, it will be added here.
* It appears that NPCs do not just do a simple straight line to the target
	like I thought previously. They first do a diagonal movement to get on
	the same axis as their target, then do a straight line. Needs 
	investigation.

**NPC Stats**

* The following NPC's do not have 100% accurate stats, and had to have been
	adjusted. The wiki did not have exact stats for these, or they didn't
	match up with the combat formula. Format is the name of the npc, then 
	(optionally) a more specific type of npc in perenthesis, and then the ID of
	the npc.
	* Paladin (20)
	* Knights of Ardougne (23 and 26)
	* Cyclops (116)
	* Dwarf (Level 9) (121)
	* Magic Axe (127)
	* Skavid (129)
	* Yeti (130) 
		* Note that this is a unused NPC. 
	* Poison Spider (134)
	* Mounted Terrorbird Gnome (138, 2252)
	* Dark Wizard (172)
	* Dwarf (206)
	* Thief (282)
	* Man (West Arddougne) (351)
	* Paladin (365)
	* River Troll (391 - 396)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.
	* Rock Golem (413 - 418)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.
	* Zombie (419 - 424)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.
	* Shade (425 - 430)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.
	* Tree Spirit (438 - 441)
	* Skeleton (459)
	* Goblin Guard (489)
	* Imp (709)
	* Zombie (751)
	* Rowdy Guard (842)
	* Ogre Shaman (871)
	* Delrith (879)
	* Kolodion (907 - 910)
	* Ungadulu (929)
	* Chicken (951)
	* Zamorak Wizard (1007)
	* Chompy Bird (1015)
	* Monk of Zamorak (1046)
	* Temple Guarding (1047)
	* Dad (1125)
	* Tyras Guards (1203, 1204)
	* Kruk (1441)
	* Skeleton (1471)
	* Snake (1479)
	* Ghost (1549)
	* Turoth (1626, 1631, 1632)
	* Crawling Hands (1650, 1655)
	* Experiments spider and sheep (1677, 1678)
	* The Kendal (1811 - 1813)
	* Arzinian Avatars (1850 - 1858)
	* Mummies (1961 - 1968)
	* Damis (1975)
	* Desert Gods (2007, 08, 09, 10)
	* Mummies (2015 - 2019)
	* Cave Goblin Guard (2073)
	* Thing Under the Bed (2255)
	* Sir Leye (2285)
	* Evil Chicken (2463 - 2468)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.
	* Dr. Hyde (2541 - 2546)
		* 100% accurate stats may be unavailable for him, as he is a random
			event monster and discontinued.

**NPC Drops**
* Possessed priest only drops one of 4 potions in his always list, need to
	update that.
* Chronozon needs the family crest added to his drop, may be the incorrect
	piece.

**Music**

* The message that appears when a new track is unlocked may not be worded
	100% accurately.
* Some regions don't have their songs properly setup yet.
* Region 8779 contains the death alter, which is in this revision but not 
	usable, set to no music.

**Objects**

* Doors cannot get stuck open if a player uses them over and over again.

**Shops**

* Not all shops may be added yet.
* Some general store keepers may not be assigned their region's special
	general store.
* Some shops don't sell the items based on their value, and have a custom 
	price. Add an entry to the item database that will override this.
* Restock timing is most likely not accurate.

**Skills**

* Agility
	* The failure sequence when falling off of the log in the barbarian course
		is not correct.
		* Need to also implement the failure sequence for the swamp bridge
			shortcut, very similar to the barbarian course log failure.
	* All agility obstacle success chances are estimates, and are not 100% 
		accurate.
	* Agility messages may not be 100% accurate.
	* Timing is slightly off on when you can do something else after an 
		obstacle is completed (server issue?).
		* May be resolved, need to double check.
	* Rope swing type obstacles don't show the rope-swing part, no animation
		there yet.
* Cooking
    * Text for milking cows may not be 100%.
* Crafting
	* Sheep always accept being sheared, they do not have a chance to run away
		yet.
* Firemaking
	* The time range for a fire made with firemaking to burn before going out,
		may not be accurate.
* Fishing
    * May not be accurate for the catching chances, needs more investigation.
* Herblore
	* The completion message for making a potion may be incorrect.
* Mining
	* The table for what gem you get from the "you find a gem among the ore"
		random event may not be 100% accurate.
	* Mining success chance for gem rocks may be off, just used the same as
		gold rocks because they share the same level requirement. Should be
		close though.
* Thieving
	* Pickpocket chances are not 100% accurate for most NPCs. The following are
		not based off of official data and are best guesses. Same goes for loot
		chances for most of them.
		* Farmer
		* H.A.M. Members
		* Warror Women/Al-Kharid Warrior
		* Rogue
		* Fremennik Citizen
		* Beareded Pollnivian Bandit
		* Desert Bandit
		* Pollnivian Bandit
		* Yanille Watchmen
		* Menaphite Thug
		* Paladin
		* Gnome
		* Hero
		* Elf
	* All stall thieving loot chances are based off of best guesses. Gem stall
		was based off of 500 drops that someone posted online, so it is 
		probably the most accurate of the tables.
	* The dwarven bakery stall has conflicting information on how much xp it
		gives. Parts of the wiki say two times normal bakery stand xp, but its
		own page says normal xp. I have set it to 2x normal bakery xp, but this
		needs to be confirmed.
	* Lockpicking has almost no information on the chances for it, all will be
		best guesses.
	* Thieving from stalls does not allow using NPCs to block vision yet.
	* Damage taken from trapped chests when failed may not be accurate.
	* Thieving line of sight is based off of best guesses. It may not be 100% 
		accurate to the base game, and no information on it has been released
		by the devs.
* Woodcutting
	* It looks like every 4 ticks there is a chance to get a log, doesn't
		change like it does in mining. Axe quality changes the chance to get a
		log, but not how often a chance appears. Currently this is programmed
		just like mining. Needs research.
* General
	* Skill guides may have incorrect ordering, strings. Hard to find 
		video/pictures of them.
	
**Quests**

* General
	* Quests may not have 100% accurate quest log text, especially the log for
		when the quest was all complete.
* Sheep Shearer
	* Not sure what Farmer Fred says when you talk to him after you've
		completed the quest, just made it so he doesn't have any dialogue.
