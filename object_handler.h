/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OBJECT_HANDLERS_H_
#define _OBJECT_HANDLERS_H_

int get_object_size_x(int object_id);
int get_object_size_y(int object_id);
int use_auto_door_at_pos(struct client *c, int door_id, int pos_x, int pos_y, 
						 int pos_z);
void handle_auto_door_at_pos(struct client *c, int door_x, int door_y, 
							 int door_z);
void close_auto_door(int index);
void handle_objects(struct client *c, int object_index);

#endif
