/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file packet.c
 * @brief Contains functions for sending specific data packets to client.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"

#include "audio.h"
#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "dialogue.h"
#include "dialogue_handler.h"
#include "item.h"
#include "misc.h"
#include "object.h"
#include "pathfinding.h"
#include "player.h"
#include "shops.h"
#include "stream.h"
#include "skills.h"
#include "skill_prayer.h"


/** Returns the size in bytes for a specific packet.
 * @param packet_id ID of the packet to get the size of
 * @returns Packet size in bytes.
 */
int get_packet_size(int packet_id)
{
	switch (packet_id)
	{
	case CLIENT_PKT_EQUIP_ITEM:
		return CLIENT_PKT_SIZE_EQUIP_ITEM;
	case CLIENT_PKT_ITEM_ON_ANOTHER:
		return CLIENT_PKT_SIZE_ITEM_ON_ANOTHER;
	case CLIENT_PKT_REMOVE_ITEM_FROM_INTERFACE:
		return CLIENT_PKT_SIZE_REMOVE_ITEM_FROM_INTERFACE;
	case CLIENT_PKT_USE_GROUND_ITEM:
		return CLIENT_PKT_SIZE_USE_GROUND_ITEM;
	case CLIENT_PKT_SELECT_FIVE_ITEMS:
		return CLIENT_PKT_SIZE_SELECT_FIVE_ITEMS;
	case CLIENT_PKT_SELECT_TEN_ITEMS:
		return CLIENT_PKT_SIZE_SELECT_TEN_ITEMS;
	case CLIENT_PKT_SELECT_ALL_ITEMS:
		return CLIENT_PKT_SIZE_SELECT_ALL_ITEMS;
	case CLIENT_PKT_SELECT_X_ITEMS:
		return CLIENT_PKT_SIZE_SELECT_X_ITEMS;
	case CLIENT_PKT_DROP_ITEM:
		return CLIENT_PKT_SIZE_DROP_ITEM;
	case CLIENT_PKT_MOVE_ITEM:
		return CLIENT_PKT_SIZE_MOVE_ITEM;
	case CLIENT_PKT_PICKUP_ITEM:
		return CLIENT_PKT_SIZE_PICKUP_ITEM;
	case CLIENT_PKT_ITEM_FIRST_ACTION:
		return CLIENT_PKT_SIZE_ITEM_FIRST_ACTION;
	case CLIENT_PKT_ITEM_SECOND_ACTION:
		return CLIENT_PKT_SIZE_ITEM_SECOND_ACTION;
	case CLIENT_PKT_MAGIC_ON_ITEM:
		return CLIENT_PKT_SIZE_MAGIC_ON_ITEM;
	case CLIENT_PKT_OBJECT_FIRST_ACTION:
		return CLIENT_PKT_SIZE_OBJECT_FIRST_ACTION;
	case CLIENT_PKT_OBJECT_SECOND_ACTION:
		return CLIENT_PKT_SIZE_OBJECT_SECOND_ACTION;
	case CLIENT_PKT_OBJECT_THIRD_ACTION:
		return CLIENT_PKT_SIZE_OBJECT_THIRD_ACTION;
	case CLIENT_PKT_ITEM_ON_OBJECT:
		return CLIENT_PKT_SIZE_ITEM_ON_OBJECT;
	case CLIENT_PKT_MAGIC_ON_OBJECT:
		return CLIENT_PKT_SIZE_MAGIC_ON_OBJECT;
	case CLIENT_PKT_NPC_FIRST_ACTION:
		return CLIENT_PKT_SIZE_NPC_FIRST_ACTION;
	case CLIENT_PKT_NPC_SECOND_ACTION:
		return CLIENT_PKT_SIZE_NPC_SECOND_ACTION;
	case CLIENT_PKT_NPC_THIRD_ACTION:
		return CLIENT_PKT_SIZE_NPC_THIRD_ACTION;
	case CLIENT_PKT_ATTACK_NPC:
		return CLIENT_PKT_SIZE_ATTACK_NPC;
	case CLIENT_PKT_ATTACK_TEST:
		return CLIENT_PKT_SIZE_ATTACK_TEST;
	case CLIENT_PKT_MAGIC_ON_NPC:
		return CLIENT_PKT_SIZE_MAGIC_ON_NPC;
	case CLIENT_PKT_ITEM_ON_NPC:
		return CLIENT_PKT_SIZE_ITEM_ON_NPC;
	case CLIENT_PKT_PLR_TRADE:
		return CLIENT_PKT_SIZE_PLR_TRADE;
	case CLIENT_PKT_CHOSE_AMOUNT:
		return CLIENT_PKT_SIZE_CHOSE_AMOUNT;
	case CLIENT_PKT_CLICKED_INTERFACE:
		return CLIENT_PKT_SIZE_CLICKED_INTERFACE;
	case CLIENT_PKT_PLAYER_APPEARANCE:
		return CLIENT_PKT_SIZE_PLAYER_APPEARANCE;
	case CLIENT_PKT_CLICKED_INTERFACE_CLOSE:
		return CLIENT_PKT_SIZE_CLICKED_INTERFACE_CLOSE;
	case CLIENT_PKT_CONTINUE_DIALOGUE:
		return CLIENT_PKT_SIZE_CONTINUE_DIALOGUE;
	case CLIENT_PKT_ENTERED_COMMAND:
		return CLIENT_PKT_SIZE_ENTERED_COMMAND;
	case CLIENT_PKT_MOUSE_CLICKED:
		return CLIENT_PKT_SIZE_MOUSE_CLICKED;
	case CLIENT_PKT_IDLE:
		return CLIENT_PKT_SIZE_IDLE;
	case CLIENT_PKT_WINDOW_FOCUS:
		return CLIENT_PKT_SIZE_WINDOW_FOCUS;
	case CLIENT_PKT_FINISHED_REGION:
		return CLIENT_PKT_SIZE_FINISHED_REGION;
	case CLIENT_PKT_FINISHED_REGION_JUNK:
		return CLIENT_PKT_SIZE_FINISHED_REGION_JUNK;
	case CLIENT_PKT_IDLE_LOGOUT:
		return CLIENT_PKT_SIZE_IDLE_LOGOUT;
	case CLIENT_PKT_CAMERA_MOVED:
		return CLIENT_PKT_SIZE_CAMERA_MOVED;
	case CLIENT_PKT_WALKING_NORMAL:
		return CLIENT_PKT_SIZE_WALKING_NORMAL;
	case CLIENT_PKT_WALKING_MINIMAP:
		return CLIENT_PKT_SIZE_WALKING_MINIMAP;
	case CLIENT_PKT_WALKING_COMMAND:
		return CLIENT_PKT_SIZE_WALKING_COMMAND;
	case CLIENT_PKT_WALKING_ANTICHEAT:
		return CLIENT_PKT_SIZE_WALKING_ANTICHEAT;
	case CLIENT_PKT_CHAT_MESSAGE:
		return CLIENT_PKT_SIZE_CHAT_MESSAGE;
	case CLIENT_PKT_MAGIC_ON_PLAYER:
		return CLIENT_PKT_SIZE_MAGIC_ON_PLAYER;

	default:
		return 0;
	}
}

/**
 * Sets the interface for a specific sidebar tab.
 * @param *c Client instance.
 * @param tab_id Which tab to edit.
 * @param interface_id Interface id to set tab to.
 */
void packet_show_sidebar_interface(struct client *c, int tab_id, int interface_id)
{
	create_frame(c, SERVER_PKT_SET_SIDEBAR_INTERFACE);
	write_word(c, interface_id);
	write_byte(c, tab_id);
}

/**
 * Shows an interface to a client.
 * @param *c Client instance.
 * @param interface_id Interface to send.
 */
void packet_show_interface(struct client *c, int interface_id)
{
	create_frame(c, SERVER_PKT_SHOW_INTERFACE);
	write_word_bigendian(c, interface_id);
	send_out_stream(c);
	c->plr->interface_open = interface_id;
}

/**
 * Show double interfaces, one in the game screen, and one over the sidebar.
 * @param *c Client instance.
 * @param interface_id Interface to show in the main game screen.
 * @param inv_interface_id Interface to show over the sidebar.
 */
void packet_show_interface_inventory(struct client *c, int interface_id, 
							  int inv_interface_id)
{
	create_frame(c, SERVER_PKT_SHOW_INTERFACE_AND_INV);
	write_word(c, interface_id);
	write_word_bigendian(c, inv_interface_id);
	send_out_stream(c);
	c->plr->interface_open = interface_id;
	/** FIXME
	c->plr->sidebar_hidden = 1;
	*/
}

/**
 * Show interface over the chatbox.
 * @param *c Client instance.
 * @param interface_id Interface id to show.
 */
void packet_show_interface_chatbox(struct client *c, int interface_id)
{
	create_frame(c, SERVER_PKT_SHOW_INTERFACE_CHATBOX);
	write_word_a(c, interface_id);
	send_out_stream(c);
	c->plr->interface_open = interface_id;
}

/**
 * Show interface in main game screen, allow walking without closing it.
 * @param *c Client instance.
 * @param interface_id Interface to show.
 */
void packet_show_interface_walkable(struct client *c, int interface_id)
{
	create_frame(c, SERVER_PKT_SHOW_INTERFACE_WALKABLE);
	write_word_bigendian(c, interface_id);
	send_out_stream(c);
	c->plr->interface_open = interface_id;
}

/**
 * Sets a interface id that holds a text string to a specific string.
 * @param *c Client instance.
 * @param string[] Text to set the interface to.
 * @param interface_id ID of the interface.
 */
void packet_set_interface_text(struct client *c, char string[], int interface_id) 
{
	create_frame_vsize_word(c, SERVER_PKT_SET_INTERFACE_TEXT);
	write_string(c, string);
	write_word(c, interface_id);
	end_frame_vsize_word(c);
}

/**
 * Sets a interface id that holds a text string to a text string containing a
 * number.
 * @param *c Client instance.
 * @param data Number to set the text interface to.
 * @param interface_id ID of the interface.
 */
void packet_set_interface_text_int(struct client *c, int data, int interface_id) 
{
	char string[20];

	snprintf(string, 20, "%d", data);
	packet_set_interface_text(c, string, interface_id);
}

/**
 * Sets a npc head to an interface id.
 * @param *c Client instance.
 * @param npc_id NPC ID's head to set in the interface.
 * @param interface_id to set.
 */
void packet_set_interface_npc_head(struct client *c, int npc_id, int interface_id)
{
	create_frame(c, SERVER_PKT_SET_INTERFACE_MOB_HEAD);
	write_word_bigendian(c, npc_id);
	write_word_a(c, interface_id);
}

/**
 * Sets the player's head to an interface id.
 * @param *c Client instance.
 * @param interface_id to set.
 */
void packet_set_interface_player_head(struct client *c, int interface_id)
{
	create_frame(c, SERVER_PKT_SET_INTERFACE_PLAYER_HEAD);
	write_word_bigendian(c, interface_id);
}

/**
 * Sets player's right click menu options.
 * @param *c Client instance.
 * @param command_slot Type of command sent.
 * @param top If command should be set to the top of the list or not.
 * @param string[] Text of the command.
 */
void packet_set_right_click_menu_options(struct client *c, int command_slot, 
		int top, char string[])
{
	create_frame_vsize(c, SERVER_PKT_SET_RCLICK_MENU);
	write_byte_c(c, command_slot); 
	write_string(c, string);
	/* If command should be added to top of menu.  */
	write_byte(c, top); 
	end_frame_vsize(c);
}

/**
 * Closes all open interfaces on the client.
 * @param *c Client instance.
 */
void packet_close_all_open_interfaces(struct client *c)
{
	create_frame(c, SERVER_PKT_CLOSE_ALL_INTERFACES);
	
	/* Reset regular interfaces open.  */
	c->plr->interface_open = -1;
	c->plr->interface_open_sub = -1;

	/* Reset sidebar interfaces.  */
	c->plr->interface_open_sidebar = -1;
	c->plr->interface_open_sidebar_sub = -1;
}

/**
 * Sends a number input textbox in the chatbox interface.
 * @param *c Client instance.
 * @param close_everything Should all interfaces be closed or not.
 */
void packet_send_choose_amount_chatbox_interface(struct client *c, int close_everything)
{
	if (close_everything == 1)
	{
		packet_close_all_open_interfaces(c);
	}
	create_frame(c, SERVER_PKT_SEND_CHOOSE_AMOUNT);
}

/**
 * Sends a text message to a specific client.
 * @param *c Client instance.
 * @param string[] Text to send to the client.
 */
void packet_send_chatbox_message(struct client *c, char string[])
{
	create_frame_vsize(c, SERVER_PKT_SEND_MESSAGE);
	write_string(c, string);
	end_frame_vsize(c);
}

/**
 * Sets chat options for a client.
 * @param *c Client instance.
 * @param pub Public chat setting.
 * @param pri Private chat setting.
 * @param trade Trade chat setting.
 * @todo Document what each setting does, figure out when this function is
 * even really used.
 */
void packet_set_chat_options(struct client *c, int pub, int pri, int trade)
{
	/* Public chat, private chat, trade block.  */
	create_frame(c, SERVER_PKT_SET_CHAT_OPTIONS);
	write_byte(c, pub);
	write_byte(c, pri);
	write_byte(c, trade);
}

static void generic_position_packet(struct client *c, int x_pos, int y_pos)
{
	create_frame(c, SERVER_PKT_GENERIC_POSITION);
	write_byte_s(c, y_pos);
	write_byte(c, x_pos);
}

/**
 * Spawn an item in the game world for a client.
 * @param *c Client instance.
 * @param drop_index Item's index in the ITEM_DROPS array.
 */
void packet_draw_item_in_world(struct client *c, int drop_index)
{
	generic_position_packet(c, 
		(ITEM_DROPS[drop_index]->world_x - 8 * c->plr->world_region_x),
		(ITEM_DROPS[drop_index]->world_y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_CREATE_ITEM_IN_WORLD);
	write_word_bigendian(c, ITEM_DROPS[drop_index]->id);
	write_byte_c(c, 0);
	write_word_bigendian(c, ITEM_DROPS[drop_index]->amount);
}

/**
 * Despawns an item in the game world for a client.
 * @param *c Client instance.
 * @param drop_index Item's index in the ITEM_DROPS array.
 */
void packet_erase_item_from_world(struct client *c, int drop_index)
{
	generic_position_packet(c, 
		(ITEM_DROPS[drop_index]->world_x - 8 * c->plr->world_region_x),
		(ITEM_DROPS[drop_index]->world_y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_REMOVE_ITEM_IN_WORLD);
	write_byte_s(c, 0);
	write_word_bigendian_a(c, ITEM_DROPS[drop_index]->id);
}

/**
 * Sets a configuration value for a client.
 * @param *c Client instance to change the setting for.
 * @param id ID of the setting being changed.
 * @param value Value to set the setting to.
 */
void packet_set_client_config_value(struct client *c, int id, int value) 
{
	create_frame(c, SERVER_PKT_SET_CLIENT_CONFIG);
	write_byte_a(c, value);
	write_word_bigendian_a(c, id);
}

/**
 * Sets a configuration value for a client, uses an int for more data.
 * @param *c Client instance to change the setting for.
 * @param id ID of the setting being changed.
 * @param value Value to set the setting to.
 */
void packet_set_client_config_value_int(struct client *c, int id, int value)
{
	create_frame(c, SERVER_PKT_SET_CLIENT_CONFIG_BIG);
	write_word_bigendian_a(c, id);
	write_dword_v1(c, value);
}

/**
 * Spawns an object in a client's game world.
 * @param *c Client instance.
 * @param obj_index Index of the object in the OBJECT_SPAWNS array.
 */
void packet_draw_object_in_world(struct client *c, int obj_index)
{
	generic_position_packet(c, 
		(OBJECT_SPAWNS[obj_index]->world_x - 8 * c->plr->world_region_x), 
		(OBJECT_SPAWNS[obj_index]->world_y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_REMOVE_OBJECT_IN_WORLD);
	write_byte(c, (OBJECT_SPAWNS[obj_index]->type << 2) 
					+ (OBJECT_SPAWNS[obj_index]->face & 3));
	write_byte_a(c, 0);

	create_frame(c, SERVER_PKT_CREATE_OBJECT_IN_WORLD);
	write_byte(c, (OBJECT_SPAWNS[obj_index]->type << 2) 
				   + (OBJECT_SPAWNS[obj_index]->face & 3));
	write_byte_a(c, 0);
	write_word_bigendian_a(c, OBJECT_SPAWNS[obj_index]->id);
}

/**
 * Despawns an object in a client's game world.
 * @param *c Client instance.
 * @param obj_index Index of the object in the OBJECT_SPAWNS array.
 */
void packet_erase_object_from_world(struct client *c, int obj_index)
{
	generic_position_packet(c, 
		(OBJECT_SPAWNS[obj_index]->world_x - 8 * c->plr->world_region_x), 
		(OBJECT_SPAWNS[obj_index]->world_y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_REMOVE_OBJECT_IN_WORLD);
	write_byte(c, (OBJECT_SPAWNS[obj_index]->type << 2) 
					+ (OBJECT_SPAWNS[obj_index]->face & 3));
	write_byte_a(c, 0);
}

/**
 * Spawns an object in a client's game world, allows for setting the attributes
 * of the object.
 * @param *c Client instance.
 * @param x, y World position of the spawned object.
 * @param obj_type Type of the object, used by the client.
 * @param face Direction the object will face.
 * @param id Object id to spawn.
 */
void packet_draw_object_in_world_custom(struct client *c, int x, int y, 
								   int obj_type, int face, int id)
{
	generic_position_packet(c, 
		(x - 8 * c->plr->world_region_x), 
		(y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_REMOVE_OBJECT_IN_WORLD);
	write_byte(c, (obj_type << 2) + (face & 3));
	write_byte_a(c, 0);

	create_frame(c, SERVER_PKT_CREATE_OBJECT_IN_WORLD);
	write_byte(c, (obj_type << 2) + (face & 3));
	write_byte_a(c, 0);
	write_word_bigendian_a(c, id);
}

/**
 * Despawns an object in a client's game world that was spawned by the custom
 * function.
 * @param *c Client instance.
 * @param x, y World position of the spawned object.
 * @param obj_type Type of the object, used by the client.
 * @param face Direction the object faces.
 */
void packet_erase_object_from_world_custom(struct client *c, int x, int y, 
								   int obj_type, int face)
{
	generic_position_packet(c, 
		(x - 8 * c->plr->world_region_x), 
		(y - 8 * c->plr->world_region_y));

	create_frame(c, SERVER_PKT_REMOVE_OBJECT_IN_WORLD);
	write_byte(c, (obj_type << 2) + (face & 3));
	write_byte_a(c, 0);
}


/**
 * Resets a client's equipment bonus stats.
 * @param *c Client instance.
 */
static void clear_equipment_bonuses(struct client *c) 
{
	int i;

	for (i = 0; i < 12; i++)
	{
		c->plr->player_bonus[i] = 0;
	}

	c->plr->ranged_strength = 0;
}

/**
 * Sends a client's equipment bonus stats to the client to show in the
 * equipment menu.
 * @param *c Client instance.
 */
static void packet_send_player_equipment_bonuses(struct client *c) 
{
	int offset = 0;
	char send[20] = {0};
	int i;

	/* 12 - # of player bonuses.  */
	for (i = 0; i < 12; i++) 
	{
		if (c->plr->player_bonus[i] >= 0) 
		{
			snprintf(send, 20, "%s: +%d", PLAYER_BONUS_NAMES[i],
									 c->plr->player_bonus[i]);
		} 
		else 
		{
			snprintf(send, 20, "%s: %d", PLAYER_BONUS_NAMES[i],
									 c->plr->player_bonus[i]);
		}

		if (i == 10) 
		{
			offset = 1;
		}
		packet_set_interface_text(c, send, (1675 + i + offset));
	}
	calculate_max_hit(c);
}

/**
 * Calculates a client's equipment bonus stats.
 * @param *c Client instance.
 */
void packet_player_update_viewport_equipment_bonuses(struct client *c) 
{
	clear_equipment_bonuses(c);

	int i, x;
	/* Go through each equipment slot.  */
	for (i = 0; i < EQUIP_SIZE; i++) 
	{
		/* Check the bonuses for the item in each slot, add them to the 
		   player's stats.  */
		for (x = 0; x < 12; x++)
		{
			if (c->plr->equipment[i] > -1) 
			{
				c->plr->player_bonus[x] 
					+= ITEMS[c->plr->equipment[i]]->bonuses[x];
			}
		}
		
		/* Add ranged strength for each item in equipment to player.  */
		if (c->plr->equipment[i] > -1)
		{
			c->plr->ranged_strength 
				+= ITEMS[c->plr->equipment[i]]->ranged_strength;
		}
	}

	calculate_prayer_drain_resistance(c);

	packet_send_player_equipment_bonuses(c);
}


/**
 * Sends the amount of run energy the player has left to the client.
 * @param *c Client instance.
 */
void packet_send_run_energy(struct client *c)
{
	create_frame(c, SERVER_PKT_SEND_RUN_ENERGY);
	write_byte(c, c->plr->running_energy);
}	

/**
 * Tells the client to redraw all items in an interface.
 * @param *c Client instance.
 * @param write_frame Interface id that holds items to refresh.
 */
void packet_reload_items_in_interface(struct client *c, int write_frame) 
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, write_frame);
	write_word(c, 28);
	
	for (i = 0; i < INV_SIZE; i++)
	{
		if (c->plr->items_n[i] > 254) 
		{
			write_byte(c, 255);
			write_dword_bigendian(c, c->plr->items_n[i]);
		} 
		else 
		{
			write_byte(c, c->plr->items_n[i]);
		}
		if ((c->plr->items[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->items[i] < -1))
		{
			c->plr->items[i] = PROTOCOL_MAX_ITEMS;
		}
		write_word_bigendian_a(c, (c->plr->items[i] + 1));
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all inventory items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_inventory_interface(struct client *c)
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_INV_PLR_ITEMS);
	write_word(c, 28);
	
	for (i = 0; i < INV_SIZE; i++)
	{
		/* Item ID needs to be one more than it actually is for inventory 
		 * items.  */
		write_word_a(c, (c->plr->items[i] + 1));
		if (c->plr->items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->items_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->items_n[i]);
		}
		if ((c->plr->items[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->items[i] < -1)) 
		{
			c->plr->items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all equipment items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_equipment_interface(struct client *c)
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_EQUIP_PLR_ITEMS);
	write_word(c, EQUIP_SIZE);
	
	for (i = 0; i < EQUIP_SIZE; i++)
	{
		write_word_a(c, (c->plr->equipment[i] + 1));
		if (c->plr->equipment_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->equipment_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->equipment_n[i]);
		}

		if ((c->plr->equipment[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->equipment[i] < -1)) 
		{
			c->plr->equipment[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all bank items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_bank_interface(struct client *c) 
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_BANK_ITEMS);
	write_word(c, PLAYER_BANK_SIZE);
	
	for (i = 0; i < PLAYER_BANK_SIZE; i++)
	{
		write_word_a(c, (c->plr->bank_items[i] + 1));
		if (c->plr->bank_items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->bank_items_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->bank_items_n[i]);
		}
		if ((c->plr->bank_items[i] >= PROTOCOL_MAX_ITEMS) 
			|| (c->plr->bank_items[i] < -1))
		{
			c->plr->bank_items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all bank sidebar items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_bank_sidebar_interface(struct client *c)
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_BANK_PLR_ITEMS);
	write_word(c, 28);
	
	for (i = 0; i < INV_SIZE; i++)
	{
		write_word_a(c, (c->plr->items[i] + 1));
		if (c->plr->items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->items_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->items_n[i]);
		}
		if ((c->plr->items[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->items[i] < -1)) 
		{
			c->plr->items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all bank deposit box items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_bank_deposit_box_interface(struct client *c)
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_BANK_DEPOSIT_BOX_ITEMS);
	write_word(c, 28);

	for (i = 0; i < INV_SIZE; i++)
	{
		write_word_a(c, (c->plr->items[i] + 1));
		if (c->plr->items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->items_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->items_n[i]);
		}
		if ((c->plr->items[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->items[i] < -1)) 
		{
			c->plr->items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all shop items they have.
 * @param *c Client instance.
 * @param shop_idx Index of the shop to show.
 */
void packet_reload_items_in_shop_interface(struct client *c, int shop_idx) 
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_SHOP_ITEMS);
	write_word(c, SHOP_SIZE);
	
	for (i = 0; i < SHOP_SIZE; i++)
	{
		write_word_a(c, (SHOP_DATA[shop_idx]->items[i] + 1));
		if (SHOP_DATA[shop_idx]->items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, SHOP_DATA[shop_idx]->items_n[i]);
		} 
		else 
		{
			/* If a shop has a negative amount of an item, show it without
			 * the count. It will have infinite stock.  */
			if (SHOP_DATA[shop_idx]->items_n[i] == -1) 
			{
				write_byte_c(c, 1);
			}
			else
			{
				write_byte_c(c, SHOP_DATA[shop_idx]->items_n[i]);
			}
		}
		if ((SHOP_DATA[shop_idx]->items[i] >= PROTOCOL_MAX_ITEMS) 
			|| (SHOP_DATA[shop_idx]->items[i] < -1))
		{
			SHOP_DATA[shop_idx]->items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Tells the client to redraw all bank sidebar items they have.
 * @param *c Client instance.
 */
void packet_reload_items_in_shop_sidebar_interface(struct client *c)
{
	int i;
	create_frame_vsize_word(c, SERVER_PKT_REFRESH_INTERFACE_ITEMS);
	write_word(c, INTR_SHOP_PLR_ITEMS);
	write_word(c, 28);
	
	for (i = 0; i < INV_SIZE; i++)
	{
		write_word_a(c, (c->plr->items[i] + 1));
		if (c->plr->items_n[i] > 254) 
		{
			write_byte_c(c, 255);
			write_dword_bigendian(c, c->plr->items_n[i]);
		} 
		else 
		{
			write_byte_c(c, c->plr->items_n[i]);
		}
		if ((c->plr->items[i] >= PROTOCOL_MAX_ITEMS) || (c->plr->items[i] < -1)) 
		{
			c->plr->items[i] = PROTOCOL_MAX_ITEMS;
		}
	}
	end_frame_vsize_word(c);
}

/**
 * Checks every client and sees i they have this particular shop interface
 * open. If so, refresh the interface.
 * @param *c Client instance.
 * @param shop_idx What shop to refresh the interface for.
 */
void packet_reload_items_in_shop_interface_globally(struct client *c, int shop_idx)
{
	int d;

	for (d = 0; d < MAX_LOCAL_PLAYERS; d++)
	{
		/* Only loop through local players because it only contains players
		   nearby, and on the same height level.  */
		if (c->plr->local_players[d] != NULL)
		{
			if (c->plr->local_players[d]->plr->interface_open_sub == shop_idx)
			{
				packet_reload_items_in_shop_interface(c->plr->local_players[d], shop_idx);
			}
		}
	}

	packet_reload_items_in_shop_interface(c, shop_idx);

}

/**
 * Sets a specific item in an interface item slot.
 * @param *c Client instance.
 * @param interface_id Interface that has the item slots.
 * @param slot Which slot to put the item into.
 * @param item_id Item ID to place in the slot.
 * @param stack_size How many of each item should have.
 */
void packet_set_item_in_interface(struct client *c, int interface_id, int slot,
						   int item_id, int stack_size)
{
	create_frame_vsize_word(c, SERVER_PKT_SET_INTERFACE_ITEM);
	write_word(c, interface_id);
	write_smart_b(c, slot);
	write_word(c, (item_id + 1));

	if (stack_size > 254) 
	{
		write_byte(c, 255);
		write_dword(c, stack_size);
	} 
	else 
	{
		write_byte(c, stack_size);
	}
	end_frame_vsize_word(c);
}

/**
 * Resets autocasting, clears it from the client as well.
 * @param *c Client instance.
 */
void player_reset_autocasting(struct client *c)
{
	packet_set_client_config_value(c, COMBAT_AUTOCAST_MODE, 0);
	c->plr->cast_spell_id = 0;
	c->plr->autocast_spell_id = 0;
	c->plr->autocasting = 0;
}

/**
 * Sends the client the total weight of all their items.
 * @param *c Client instance.
 */
void packet_send_total_carried_weight(struct client *c)
{
	float weight;
	weight = get_total_carried_weight(c);
	create_frame(c, SERVER_PKT_SEND_WEIGHT_CARRIED);
	write_word(c, (int) weight);
}

/**
 * Tells the client if the player is a member or not.
 * @param *c Client instance.
 * @param is_member 0 - not member, 1 - is a member.
 */
void packet_send_membership_status(struct client *c, int is_member)
{
	create_frame(c, SERVER_PKT_SET_MEMBERSHIP);
	write_word_a(c, c->plr->id);
	write_byte_s(c, is_member);
}

/**
 * Removes an item from the player's inventory and drops it on the ground.
 * @param *c Client instance.
 * @param item_id ID of the item to drop.
 * @param interface_id Interface the item is being dropped from.
 * @param slot From what slot in the inventory the item is being dropped.
 * @return -1 on failure, anything else is the index of the dropped item in
 * the ITEM_DROPS array.
 */
int packet_player_drop_item_on_ground(struct client *c, int item_id, 
		int interface_id, int slot)
{
	/* We have to get the amount of items before we remove the item
	   from the inventory so that when the item is added to the ground,
	   we still have how many of that item to drop.  */
	int amount, drop_index, charges;

	/* Inventory id.  */
	if (interface_id == INTR_INV_PLR_ITEMS)
	{
		amount = c->plr->items_n[slot];
		charges = c->plr->items_charges[slot];
		delete_item_from_inv_slot(c, slot, c->plr->items_n[slot]);
		drop_index = add_to_item_drops_exclusive(c, item_id, amount, charges, 
												 c->plr->world_x, 
												 c->plr->world_y, 
												 c->plr->world_z, 1);
		packet_send_total_carried_weight(c);

		return drop_index;
	}
	/* Equipment id.
	 * NOTE: Cannot directly drop from equipment in this revision, this is
	 * really only used for dropping equipment on death.  */
	else if (interface_id == INTR_EQUIP_PLR_ITEMS)
	{
		amount = c->plr->equipment_n[slot];
		charges = c->plr->equipment_charges[slot];
		drop_index = add_to_item_drops_exclusive(c, item_id, amount, charges, 
												 c->plr->world_x, 
												 c->plr->world_y, 
												 c->plr->world_z, 1);
		packet_send_total_carried_weight(c);

		return drop_index;
	}

	return -1;
}

/* Checks if the player appearence values are valid. Returns 0 if incorrect,
   1 if there are no issues.  */
static int verify_appearance_look(int sex, int hair_l, int beard_l, 
								  int torso_l, int arms_l, int hands_l, 
								  int legs_l, int feet_l) 
{
	/* If male...  */
	if (sex == 0)
	{
		if (hair_l < 0 || hair_l > 8)
		{
			return 0;
		}
		if (beard_l < 10 || beard_l > 17)
		{
			return 0;
		}
		if (torso_l < 18 || torso_l > 25)
		{
			return 0;
		}
		if (arms_l < 26 || arms_l > 31)
		{
			return 0;
		}
		if (hands_l < 33 || hands_l > 34)
		{
			return 0;
		}
		if (legs_l < 36 || legs_l > 40)
		{
			return 0;
		}
		if (feet_l < 42 || legs_l > 43)
		{
			return 0;
		}
	}
	else
	{
		if (hair_l < 45 || hair_l > 54)
		{
			return 0;
		}
		/* No need to check beard for females.  */
		if (torso_l < 56 || torso_l > 60)
		{
			return 0;
		}
		if (arms_l < 61 || arms_l > 65)
		{
			return 0;
		}
		if (hands_l < 67 || hands_l > 68)
		{
			return 0;
		}
		if (legs_l < 70 || legs_l > 77)
		{
			return 0;
		}
		if (feet_l < 79 || legs_l > 80)
		{
			return 0;
		}
	}
	/* No issue with appearance, return 1.  */
	return 1;
}

static int verify_appearance_color(int sex, int hair_c, int torso_c, 
								   int legs_c, int feet_c, int skin_c) 
{
	/* Both sexes follow the same color rules, may be different in later
	   versions, though.  */
	if (sex == 0 || sex == 1)
	{
		if (hair_c < 0 || hair_c > 11)
		{
			return 0;
		}
		if (torso_c < 0 || torso_c > 15)
		{
			return 0;
		}
		if (legs_c < 0 || legs_c > 15)
		{
			return 0;
		}
		if (feet_c < 0 || feet_c > 5)
		{
			return 0;
		}
		if (skin_c < 0 || skin_c > 7)
		{
			return 0;
		}
	}
	/* No issues with player colors, return 1.  */
	return 1;
}

/**
 * Handles when a client requests to change their player's appearance, ensures
 * that everything is correct and no cheating is taking place.
 * @param *c Client to change appearance of.
 */
void packet_player_change_appearance(struct client *c)
{
	int beard, sex; 
	int hair_l, beard_l, torso_l, arms_l, hands_l, legs_l, feet_l;
	int hair_c, torso_c, legs_c, feet_c, skin_c;
	int good_look = 0, good_color = 0;

	sex = read_unsigned_byte(c);

	/* Bounds checking.  */
	if (sex > 1 || sex < 0)
	{
		sex = 0;
	}

	hair_l = read_unsigned_byte(c);
	
	beard = read_unsigned_byte(c); 

	/* This is to fix when a female character is made, they don't
	   disappear or use a wrong emote.  */
	if (sex == 1)
	{
		beard = 256;
	}
	
	beard_l = beard;
	torso_l = read_unsigned_byte(c);
	arms_l = read_unsigned_byte(c);
	hands_l = read_unsigned_byte(c);
	legs_l = read_unsigned_byte(c);
	feet_l = read_unsigned_byte(c);
	hair_c = read_unsigned_byte(c);
	torso_c = read_unsigned_byte(c);
	legs_c = read_unsigned_byte(c);
	feet_c = read_unsigned_byte(c);
	skin_c = read_unsigned_byte(c);
	
	/*
	printf("h: %d b: %d t: %d a: %d h: %d l: %d f: %d\n", hair_l, beard_l, 
		   torso_l, arms_l, hands_l, legs_l, feet_l);
	printf("hc: %d t: %d l: %d f: %d sc: %d\n", hair_c, torso_c,
		   legs_c, feet_c, skin_c);
	printf("Valid look: %d Valid color: %d\n", good_look, good_color);
	*/

	if (c->plr->can_packet_player_change_appearance == 1)
	{
		good_look = verify_appearance_look(sex, hair_l, beard_l, torso_l, 
										   arms_l, hands_l, legs_l, feet_l);
		good_color = verify_appearance_color(sex, hair_c, torso_c, 
											 legs_c, feet_c, skin_c);
		if (good_look == 1 && good_color == 1)
		{
			c->plr->sex = sex;
			c->plr->player_look[HAIR_LOOK] = hair_l;
			c->plr->player_look[BEARD_LOOK] = beard_l;
			c->plr->player_look[TORSO_LOOK] = torso_l;
			c->plr->player_look[ARMS_LOOK] = arms_l;
			c->plr->player_look[HANDS_LOOK] = hands_l;
			c->plr->player_look[LEGS_LOOK] = legs_l;
			c->plr->player_look[FEET_LOOK] = feet_l;

			c->plr->player_look[HAIR_COLOR] = hair_c;
			c->plr->player_look[TORSO_COLOR] = torso_c;
			c->plr->player_look[LEGS_COLOR] = legs_c;
			c->plr->player_look[FEET_COLOR] = feet_c;
			c->plr->player_look[SKIN_COLOR] = skin_c;
		}
		else
		{
			/* if something is not right with the values given from the client,
			   force the default look.  */
			if (sex == 0)
			{
				default_look_male(c);
			}
			else if (sex == 1)
			{
				default_look_female(c);
			}

			printf("Player %s sent wrong character appearance values!\n", 
				   c->plr->name);
		}

		c->plr->can_packet_player_change_appearance = 0;
		c->plr->update_required = 1;
		c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	}
	else
	{
		printf("Player %s tried to change appearance through hacking!\n", 
			   c->plr->name);
	}
}

/**
 * Makes a player preform a certain animation.
 * @param *c Client instance.
 * @param emote Animation id.
 */
void player_play_animation(struct client *c, int emote)
{
	if (c->plr->animation_play_type == ANIMATION_NO_OVERWRITE)
	{
		return;
	}
	
	/* Track the last actual emote used.  */
	if (emote != -1)
	{
		c->plr->animation_request_last = c->plr->animation_request;
	}

	c->plr->animation_request = emote;
	c->plr->animation_wait_cycles = 0;
	c->plr->animation_play_type = ANIMATION_NORMAL;
	c->plr->update_required = 1;
	c->plr->update_masks[ANIMATION_REQ] = 1;

	c->plr->animation_stored_emote = -1;
	c->plr->animation_stored_ticks = -1;
}

/**
 * Makes a player preform a certain animation, repeating after a certain number
 * of ticks.
 * @param *c Client instance.
 * @param emote Animation id.
 * @param time Ticks to wait until the animation is played again.
 */
void player_play_animation_repeats(struct client *c, int emote, int time)
{
	if (c->plr->animation_play_type == ANIMATION_NO_OVERWRITE)
	{
		return;
	}

	c->plr->animation_request = emote;
	c->plr->animation_wait_cycles = 0;

	/* Set animation repetition time.  */
	c->plr->animation_stored_ticks 
		= c->plr->animation_stored_ticks_start = time;
	c->plr->animation_play_type = ANIMATION_REPEATS;
	c->plr->animation_stored_emote = emote;

	c->plr->update_required = 1;
	c->plr->update_masks[ANIMATION_REQ] = 1;
}

/**
 * Makes a player preform a certain animation, without being able to be
 * interrupted by another animation.
 * @param *c Client instance.
 * @param emote Animation id.
 * @param time Ticks to wait until another animation can be played. 
 */
void player_play_animation_no_overwrite(struct client *c, int emote, int time)
{
	if (c->plr->animation_play_type == ANIMATION_NO_OVERWRITE)
	{
		return;
	}

	c->plr->animation_request = emote;
	c->plr->animation_wait_cycles = 0;

	/* Set animation repetition time.  */
	c->plr->animation_stored_ticks = time;
	c->plr->animation_play_type = ANIMATION_NO_OVERWRITE;
	c->plr->animation_stored_emote = emote;

	c->plr->update_required = 1;
	c->plr->update_masks[ANIMATION_REQ] = 1;
}

void player_reset_animation(struct client *c)
{
	c->plr->animation_request = -1;
	c->plr->animation_wait_cycles = 0;
	c->plr->animation_play_type = ANIMATION_NORMAL;
	c->plr->update_required = 1;
	c->plr->update_masks[ANIMATION_REQ] = 1;

	c->plr->animation_stored_emote = -1;
	c->plr->animation_stored_ticks = -1;
}

/**
 * Sets the information for the phase movment update mask.
 * @param *c Client instance.
 * @param start_x, start_y In-region coordinate where movement starts.
 * @param end_x, end_y In-region coordinate where movement starts.
 * @param start_time Client ticks after the client receives this packet to 
 * start the movement.
 * @param end_time Client ticks after the client receives this packet to 
 * end the movement.
 * @param direction Direction the player faces when the movement is done.
 */
void set_phase_movement(struct client *c, int start_x, int start_y, 
						int end_x, int end_y, int start_time, int end_time,
						int direction)
{
	/* Crash will not occur if the time values are negative, but the client
	 * will be stuck forever if they are. Prevent this.  */
	if (start_time < 0)
	{
		start_time = 0;
	}
	if (end_time < 0)
	{
		end_time = 0;
	}
	/* If both time values are the same, the client will crash. Here we add one
	 * to the end time if they are the same to prevent this.  */
	if (start_time == end_time)
	{
		end_time++;
	}

	printf("sx: %d sy: %d\n", start_x, start_y);
	printf("ex: %d ey: %d\n", end_x, end_y);

	c->plr->phase_movement_start_x = start_x;
	c->plr->phase_movement_start_y = start_y;
	c->plr->phase_movement_end_x = end_x;
	c->plr->phase_movement_end_y = end_y;
	c->plr->phase_movement_start_time = start_time;
	c->plr->phase_movement_end_time = end_time;
	c->plr->phase_movement_direction = direction;

	c->plr->update_required = 1;
	c->plr->update_masks[PHASE_MOVEMENT] = 1;
}

/**
 * Sets an action that is preformed on a player every so often.
 * @param *c Client instance.
 * @param type Type of action to do.
 * @param ticks Number of ticks between each action.
 */
void set_timed_action(struct client *c, int type, int ticks)
{
	c->plr->timed_action_type = type;
	c->plr->timed_action_ticks = 0;
	c->plr->timed_action_ticks_start = ticks;
	c->plr->timed_action_store = 0;
}

/**
 * Resets the timed action running on a player.
 * @param *c Client instance.
 */
void reset_timed_action(struct client *c)
{
	c->plr->timed_action_type = -1;
	c->plr->timed_action_ticks = c->plr->timed_action_ticks_start = -1;
	c->plr->timed_action_store = 0;
}

/**
 * Resets the currently playing animation a player is doing.
 * @param *c Client instance.
 */
void player_replay_animation(struct client *c) 
{
	if (is_player_frozen(c, 0))
	{
		return;
	}

	c->plr->animation_request = -1;
	c->plr->animation_stored_emote = -1;
	c->plr->animation_stored_ticks = -1;
	c->plr->animation_stored_ticks_start = -1;
	c->plr->animation_play_type = -1;

	c->plr->update_required = 1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
}

/**
 * Places an item model in an interface.
 * @param *c Client instance.
 * @param interface_id ID of the interface to place the model in.
 * @param zoom Zoom level of the 3d model.
 * @param item_id ID of the item to place.
 */
void packet_set_item_in_interface_model(struct client *c, int interface_id, int zoom, 
								int item_id)
{
	create_frame(c, SERVER_PKT_SET_INTERFACE_ITEM_MODEL);
	write_word_bigendian_a(c, zoom);
	write_word_a(c, interface_id);
	write_word_bigendian(c, item_id);
}

/**
 * Sets a 3d model, player/npc head to play a certain animation.
 * @param *c Client instance.
 * @param interface_id Interface id.
 * @param anim Animation to make the model play.
 */
void set_interface_model_anim(struct client *c, int interface_id, int anim)
{
	create_frame(c, SERVER_PKT_SET_INTERFACE_MODEL_ANIMATION);
	//printf("Interface id: %d anim: %d\n", interface_id, anim);
	write_word_bigendian_a(c, interface_id);
	write_word_bigendian_a(c, anim);
}

/**
 * Handles all walking related packets.
 * @param *c Client instance.
 * @param opcode Walking packet opcode, determines what needs to be done.
 * @todo Figure out exactly what needs to be called/reset on walking packet,
 * may not need all of this. At least could clean this up.
 */
void packet_player_walking(struct client *c, int opcode) 
{
	int packet_size = read_unsigned_byte(c);
	int i;
	int first_step_x, first_step_y;

	printf("moving\n");

	if (opcode == CLIENT_PKT_WALKING_MINIMAP)
	{
		printf("moving minimap\n");
		/* Ignores junk bytes sent.  */
		packet_size -= 14;
		/* c.resetfollowers(); */
	}
	else if (opcode == CLIENT_PKT_WALKING_NORMAL)
	{
		/* Regular walk.  */
		printf("moving normal\n");
	}
	else if (opcode == CLIENT_PKT_WALKING_COMMAND)
	{
		printf("moving as part of action\n");
	}
	
	packet_close_all_open_interfaces(c);
	
	/* Get where the client wants to go, but handle pathing to there
	   server side.  */
	int steps_total = 0;
	int queue_x[WALK_QUEUE_SIZE] = {0};
	int queue_y[WALK_QUEUE_SIZE] = {0};
	
	steps_total = ((packet_size - 5) / 2);

	if (++(steps_total) > WALK_QUEUE_SIZE) 
	{
		printf("TOO BIG WALK QUEUE! %d\n", steps_total);
		printf("Increase WALK_QUEUE_SIZE!\n");
		return;
	}

	int will_run = read_signed_byte_c(c);

	/* If player is trying to run, make sure they have enough energy for it. */
	if (will_run == 1)
	{
		if (c->plr->running_energy > 0)
		{
			c->plr->is_running_this_path = 1;
		}
	}

	first_step_x = read_signed_word(c);
	
	for (i = 1; i < steps_total; i++) 
	{
		queue_x[i] = (int) read_signed_byte_c(c);
		queue_y[i] = (int) read_signed_byte_c(c);
	}
	
	first_step_y = read_signed_word_a(c);

	for (i = 0; i < steps_total; i++) 
	{
		queue_x[i] += first_step_x;
		queue_y[i] += first_step_y;
	}
	
	steps_total--;

	/* Only move if the player is not frozen or being prevented in some
	 * way.  */
	if (is_player_frozen(c, 1) == 0)
	{
		/* Figure out where to move next.  */
		c->plr->destination_movement_needs_updating = 1;
		c->plr->destination_x = queue_x[steps_total];
		c->plr->destination_y = queue_y[steps_total];
		c->plr->destination_size_x = c->plr->destination_size_y = 1;
	}
	else
	{
		return;
	}
	
	/* End dialogue if speaking.  */
	if (c->plr->chat_type != -1)
	{
		end_dialogue(c);
	}

	/* Resets all player actions.  */
	c->plr->action_mask = 0;

	/* Make sure no skill action is continued.  */
	c->plr->action_skill_id = -1;
	c->plr->action_total_time_acted = 0;

	/* If moving, cannot be attacking at the moment.  */
	reset_player_combat_target(c);
	player_replay_animation(c);

	c->plr->action_store_one = -1;
	c->plr->action_store_two = -1;
	c->plr->action_store_three = -1;
	c->plr->action_store_four = -1;

	/* If a delayed "add item to inv" can be interrupted, do so here.  */
	if (c->plr->item_add_uninterruptable == 0)
	{
		c->plr->item_add_timer = -1;
		c->plr->item_add_id = -1;
		c->plr->item_add_num = -1;
		c->plr->item_add_charges = -1;
		c->plr->item_add_message = 0;
		c->plr->item_add_uninterruptable = 0;
	}

	/* Reset facing at an mobile, if needed.  */
	if (c->plr->face_mob != -1)
	{
		player_stop_facing_npc(c);
	}
}

/**
 * Clears the flag that appears where the player is going on the minimap.
 * @param *c Client instance.
 */
void packet_reset_minimap_flag(struct client *c)
{
	create_frame(c, SERVER_PKT_RESET_MINIMAP_FLAG);
}

/**
 * Disconnects a player from the server.
 * @param *c Client to disconnect.
 */
void packet_player_disconnect(struct client *c)
{
	create_frame(c, SERVER_PKT_DISCONNECT_PLAYER);
}

/**
 * Disconnects a music client from the server.
 * @param *mc Music client to disconnect.
 */
void packet_music_player_disconnect(struct client *c)
{
	if (CONNECTED_MUSIC_CLIENTS[c->music_idx] != NULL)
	{
		write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], 
						 SERVER_PKT_DISCONNECT_MUSIC_CLIENT);
	}
}

/**
 * Tells the client's music client to start playing a specific song.
 * @param *c Music client instance.
 * @param song Song ID to start playing.
 */
void packet_play_song(struct client *c, int song)
{
	if (c->music_idx != -1)
	{
		if (CONNECTED_MUSIC_CLIENTS[c->music_idx] != NULL)
		{
			write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], SERVER_PKT_PLAY_SONG);
			write_music_word(CONNECTED_MUSIC_CLIENTS[c->music_idx], song);
			send_out_music_stream(CONNECTED_MUSIC_CLIENTS[c->music_idx]);
			/* Send the name of the song that is playing to the interface.  */
			packet_set_interface_text(c, get_music_name(song), 4439);
		}
		else
		{
			c->music_idx = -1;
		}
	}
}

/**
 * Tells the client's music client to queue a specific song to play next.
 * @param *c Music client instance.
 * @param song Song ID to queue up.
 */
void packet_queue_song(struct client *c, int song)
{
	if (c->music_idx != -1)
	{
		if (CONNECTED_MUSIC_CLIENTS[c->music_idx] != NULL)
		{
			write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], SERVER_PKT_QUEUE_SONG);
			write_music_word(CONNECTED_MUSIC_CLIENTS[c->music_idx], song);
			send_out_music_stream(CONNECTED_MUSIC_CLIENTS[c->music_idx]);
		}
		else
		{
			c->music_idx = -1;
		}
	}
}

/**
 * Tells the client's music client to play a short jingle song. Once the client
 * finishes playing this jingle, it will then return to the original song it
 * was playing. Used for level up songs and the like.
 * @param *c Music client instance.
 * @param jingle Song ID to queue up.
 */
void packet_play_jingle(struct client *c, int jingle)
{
	if (c->music_idx != -1)
	{
		if (CONNECTED_MUSIC_CLIENTS[c->music_idx] != NULL)
		{
			write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], SERVER_PKT_PLAY_JINGLE);
			write_music_word(CONNECTED_MUSIC_CLIENTS[c->music_idx], jingle);
			send_out_music_stream(CONNECTED_MUSIC_CLIENTS[c->music_idx]);
		}
		else
		{
			c->music_idx = -1;
		}
	}
}

/**
 * Tells the client's music client to start playing a specific sound effect.
 * @param *c Music client instance.
 * @param sound Sound effect ID to start playing.
 * @param delay Delay before sound plays, in milliseconds.
 * @param volume How loud the sfx should be, range is 0 - 128. If -1 is given,
 * will send the sound at default volume, 64.
 */
void packet_play_sfx(struct client *c, int sound, int delay, int volume)
{
	int new_vol = 64;

	//printf("SFX PLAY - ID: %d DELAY: %d VOL: %d\n", sound, delay, volume);

	if (c->music_idx != -1)
	{
		if (CONNECTED_MUSIC_CLIENTS[c->music_idx] != NULL)
		{
			write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], SERVER_PKT_PLAY_SOUND);
			write_music_word(CONNECTED_MUSIC_CLIENTS[c->music_idx], sound);
			write_music_word(CONNECTED_MUSIC_CLIENTS[c->music_idx], delay);

			if (volume < 0 || volume > 128)
			{
				new_vol = 64;
			}
			else
			{
				new_vol = volume;
			}

			write_music_byte(CONNECTED_MUSIC_CLIENTS[c->music_idx], new_vol);

			send_out_music_stream(CONNECTED_MUSIC_CLIENTS[c->music_idx]);
		}
		else
		{
			c->music_idx = -1;
		}
	}
}

/**
 * Plays a specific graphic at a specific point in the game world.
 * @param *c Client to show graphic to.
 * @param gfx_id ID of the graphic to play.
 * @param x, y World coordinates to play it at.
 * @param draw_height How high up off the ground to play the graphic.
 * @param time Delay before playing.
 */
void packet_draw_gfx_in_world(struct client *c, int gfx_id, int x, int y, 
						 int draw_height, int time)
{
	generic_position_packet(c, (x - (c->plr->world_region_x * 8)),
		(y - (c->plr->world_region_y * 8)));

	create_frame(c, SERVER_PKT_CREATE_GFX);
	write_byte(c, 0);
	write_word(c, gfx_id);
	write_byte(c, draw_height);
	write_word(c, time);
}

/**
 * Plays a specific graphic at a specific point in the game world, shows to
 * everyone that is near the specified client.
 * @param *c Client to show graphic to first, and everyone around them.
 * @param gfx_id ID of the graphic to play.
 * @param x, y World coordinates to play it at.
 * @param draw_height How high up off the ground to play the graphic.
 * @param time Delay before playing.
 */
void packet_draw_gfx_in_world_globally(struct client *c, int gfx_id, int x, int y, 
								  int draw_height, int time)
{
	int d;
	
	/* Create graphic for player first, then everyone around him.  */
	packet_draw_gfx_in_world(c, gfx_id, x, y, draw_height, time);
	
	for (d = 0; d < MAX_LOCAL_PLAYERS; d++)
	{
		/* Only loop through local players because it only contains players
		   nearby, and on the same height level.  */
		if (c->plr->local_players[d] != NULL)
		{
			packet_draw_gfx_in_world(c->plr->local_players[d], gfx_id, x, y, 
								draw_height, time);
		}
	}
}

/**
 * @brief Creates a projectile in the game world.
 *
 * @param *c Client instance to show projectile to.
 * @param x start x pos
 * @param y start y pos
 * @param off_x end_x pos
 * @param off_y end y pos
 * @param angle angle the projectile travels at
 * @param speed how fast it travels (lower is faster)
 * @param gfx_moving gfx id
 * @param start_height what height it begins at
 * @param end_height what height it ends at
 * @param lockon npc id that it will follow
 * @param time delay before creation
 * @param slope ?
 */
void 
packet_create_projectile(struct client *c, int x, int y, int off_x, int off_y,
				  int angle, int speed, int gfx_moving, int start_height,
				  int end_height, int lockon, int time, int slope)
{
	/** TODO: rename this to something better.  */
	int offx, offy;

	/* Gets the offset that is sent to the client.  */
	/* TODO: Are the names for these correct? Double check.  */
	offx = (y - off_y) * -1;
	offy = (x - off_x) * -1;
	
	generic_position_packet(c, ((x - (c->plr->world_region_x * 8)) - 3),
		((y - (c->plr->world_region_y * 8)) - 2));

	create_frame(c, SERVER_PKT_CREATE_PROJECTILE);
	write_byte(c, angle);
	write_byte(c, offy);
	write_byte(c, offx);
	/* Have to add 1 to npc id for some reason. This is so that the proper 
	   npc will be hit with the projectile.  */
	/** @todo Check if this messes with pvp.  */
	write_word(c, lockon + 1);
	write_word(c, gfx_moving);
	write_byte(c, start_height);
	write_byte(c, end_height);
	write_word(c, time);
	write_word(c, speed);
	write_byte(c, slope);
	/* XXX: This is known as "initial distance from source", do not know what
	   that quite means.  */
	write_byte(c, 64);
}

/**
 * @brief Creates a projectile in the game world, visible to everyone who is
 * near to the specified player.
 *
 * @param *c Client instance to show projectile to first, and then everyone
 * around them.
 *
 * @param x start x pos
 * @param y start y pos
 * @param off_x end_x pos
 * @param off_y end y pos
 * @param angle angle the projectile travels at
 * @param speed how fast it travels (lower is faster)
 * @param gfx_moving gfx id
 * @param start_height what height it begins at
 * @param end_height what height it ends at
 * @param lockon npc id that it will follow
 * @param time delay before creation
 * @param slope ?
 */
void 
packet_create_projectile_global(struct client *c, int x, int y, int off_x, int off_y,
						 int angle, int speed, int gfx_moving, 
						 int start_height, int end_height, int lockon, 
						 int time, int slope)
{
	int z;

	packet_create_projectile(c, x, y, off_x, off_y, angle, speed, gfx_moving, 
					  start_height, end_height, lockon, time, slope);
	
	for (z = 0; z < c->plr->local_players_num; z++)
	{
		packet_create_projectile(c->plr->local_players[z], x, y, off_x, off_y, angle, 
						  speed, gfx_moving, start_height, end_height, lockon, 
						  time, slope);
	}
}
