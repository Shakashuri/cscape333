/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file npc.c
 * @brief Contains all functions related to updating npc states.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <math.h>

#include "constants.h"

#include "audio.h"
#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "fileio.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "packet.h"
#include "player.h"
#include "regions.h"
#include "rng.h"
#include "skill_magic.h"
#include "stream.h"

/**
 * Sets an NPC's AI to what it should be at spawn.
 * @param *npc NPC used.
 */
void set_initial_npc_ai_state(struct npc_spawn *npc)
{
	if (npc->default_walks == 1)
	{
		npc->ai_state = AI_IDLE;
	}
	else
	{
		npc->ai_state = AI_IDLE;
	}
}

static void leave_npc_ai_state(struct npc_spawn *npc, int state)
{
	switch (state)
	{
		case AI_IDLE:
			break;
		case AI_ATTACK:
			npc->in_combat = 0;
			break;
		case AI_DEAD:
			break;
		case AI_RESPAWN:
			break;
		case AI_TALK:
			break;
	}
}

static void enter_npc_ai_state(struct npc_spawn *npc, int state)
{
	switch (state)
	{
		case AI_IDLE:
			set_initial_npc_ai_state(npc);
			npc->ai_state = state;
			break;
		case AI_ATTACK:
			npc->in_combat = 1;
			npc->ai_state = state;
			break;
		case AI_DEAD:
			npc->ai_state = state;
			break;
		case AI_RESPAWN:
			npc->ai_state = state;
			break;
		case AI_TALK:
			npc->ai_state = state;
			break;
	}
}

/**
 * Preforms the needed operations to change an NPC's AI to a different state.
 * @param *npc NPC to use.
 * @param new_state New AI state to use.
 */
void set_npc_ai_state(struct npc_spawn *npc, int new_state)
{
	leave_npc_ai_state(npc, npc->ai_state);
	enter_npc_ai_state(npc, new_state);
}

/**
 * Checks if an NPC is alive or not.
 * @param *npc NPC to check.
 * @return Returns 1 if dead, 0 if not.
 * @todo Move this to misc.c
 */
int is_npc_dead(struct npc_spawn *npc)
{
	if (npc->ai_state == AI_DEAD || npc->ai_state == AI_RESPAWN)
	{
		return 1;
	}

	return 0;
}

/**
 * Applies default settings to npc_spawn structs.
 * @param *npc Npc struct to apply defaults to.
 */
void apply_npc_spawn_defaults(struct npc_spawn *npc)
{
	int y = 0;
	int orig_id = npc->spawn_id = npc->npc_id;

	/* Ensures the walk queue is zeroed out for each npc.  */
	for (y = 0; y < NPC_WALK_QUEUE_SIZE; y++)
	{
		npc->walk_queue_x[y] = npc->walk_queue_y[y] = -1;
	}
	npc->walk_queue_ptr = 0;
	npc->walking_to_point = 0;
	npc->walking_to_home = 0;
	npc->face_mob = -1;
	npc->target_x = -1;
	npc->target_y = -1;
	npc->pursue_target = 0;

	if (npc->walks == 0)
	{
		npc->last_walked_direction = npc->spawn_facing_direction;
	}

	/* Skip the first one, that is where the max hit goes which will
	   be calculated below.  */
	for (y = 1; y < 6; y++)
	{
		npc->melee_stats[y] = NPCS[orig_id]->melee_stats[y];
		npc->ranged_stats[y] = NPCS[orig_id]->ranged_stats[y];
	}
	for (y = 0; y < 5; y++)
	{
		npc->magic_spells[y] = NPCS[orig_id]->magic_spells[y];
	}
	/* Animations.  */
	npc->blocking_emote = NPCS[orig_id]->blocking_emote;

	npc->num_spells = NPCS[orig_id]->num_spells;
	npc->magic_range = NPCS[orig_id]->magic_range;
	npc->chance_to_use_magic = NPCS[orig_id]->chance_to_use_magic;
	npc->ranged_projectile_id = NPCS[orig_id]->ranged_projectile_id;
	npc->melee_range = NPCS[orig_id]->melee_range;
	npc->max_ranged = NPCS[orig_id]->max_ranged;
	npc->undead = NPCS[orig_id]->undead;
	npc->prefer_ranged = NPCS[orig_id]->prefer_ranged;
	npc->death_emote = NPCS[orig_id]->death_emote;
	npc->death_timer = NPCS[orig_id]->death_timer;
	npc->spawn_x = npc->world_x;
	npc->spawn_y = npc->world_y;
	npc->spawn_z = npc->world_z;
	npc->default_walks = npc->walks;
	npc->walks_stop_ticks = -1;
	npc->health = npc->max_health = NPCS[orig_id]->health;
	npc->level = NPCS[orig_id]->level;
	npc->attackable = NPCS[orig_id]->attackable;
	npc->aggressive = NPCS[orig_id]->aggressive;
	npc->retaliates = NPCS[orig_id]->retaliates;
	npc->respawn = NPCS[orig_id]->respawn_ticks;
	/* NPC stats.  */
	npc->atk_skill = NPCS[orig_id]->atk_skill;
	npc->str_skill = NPCS[orig_id]->str_skill;
	npc->def_skill = NPCS[orig_id]->def_skill;
	npc->range_skill = NPCS[orig_id]->range_skill;
	npc->magic_skill = NPCS[orig_id]->magic_skill;
	npc->atk_bonus = NPCS[orig_id]->atk_bonus;
	npc->str_bonus = NPCS[orig_id]->str_bonus;
	npc->ranged_strength = NPCS[orig_id]->ranged_strength;

	for (y = 0; y < 10; y++)
	{
		npc->npc_bonus[y] = NPCS[orig_id]->npc_bonus[y];
	}

	/* Setup their initial AI state.  */
	set_npc_ai_state(npc, AI_IDLE);
	
	/* Set max hits for melee and ranged.  */
	calculate_npc_max_hit(npc, 0);
	calculate_npc_max_hit(npc, 1);

	/* Any variable that needs to be set specially goes here.  */
	npc->frozen_ticks = 0;
	npc->highest_hit_offset = -1;
	npc->targeted_player = -1;
	npc->targeted_npc = -1;
	npc->poison_damage = -1;
	npc->poison_timer = -1;
	npc->transform_timer = -1;
	npc->teleport_timer = -1;
	npc->hit_gfx_delay = -1;
	npc->will_respawn = 1;

	/* Set collision needed.  */
	add_collision(npc->world_x, npc->world_y, npc->world_z, TAKEN_MASK);
}

static void append_gfx_npc(struct npc_spawn *npc)
{
	write_word_bigendian_a_update(npc->gfx);
	write_dword_bigendian_update(npc->gfx_height);
}

static void append_anim_npc(struct npc_spawn *npc)
{
	write_word_bigendian_a_update(npc->animation_request);
	write_byte_a_update(1);
}

static void append_npc_appearance(struct npc_spawn *npc)
{
	write_word_update(npc->npc_id);
}

/* Sends the client info about how much damage, if any, a npc took.  */
static void append_hit_npc(struct npc_spawn *npc)
{
	write_byte_update(npc->hit_diff);
	if (npc->hit_diff > 0 && npc->need_hp_update == 1) 
	{
		npc->health -= npc->hit_diff;
		npc->need_hp_update = 0;
	}
	if (npc->health <= 0) 
	{
		npc->dead = 1;
		set_npc_ai_state(npc, AI_DEAD);
	}
	
	if (npc->poison_hit != 1)
	{
		if ((npc->hit_diff > 0)) 
		{
			write_byte_c_update(1);
		} 
		else
		{
			write_byte_c_update(0);
		}
	}
	else 
	{
		write_byte_c_update(2);
		npc->poison_hit = 0;
	}

	/* Client can't handle hp values over 100, so we have to do some math here
	   if an npc has more than 100 hp for it to show up normally.  */
	if (npc->max_health > 100)
	{
		int c_hp = (int) ceil(((double) npc->health 
								/ (double) npc->max_health) * 100.0);

		write_byte_update(c_hp);
		write_byte_c_update(100);
	}
	else
	{
		write_byte_update(npc->health);
		write_byte_c_update(npc->max_health);
	}

	/* FIXME Does this need a delay to be added?  */
	if (npc->ai_state != AI_ATTACK && npc->ai_state != AI_DEAD
		&& npc->ai_state != AI_RESPAWN)
	{
		switch (npc->hit_diff_from)
		{
			/* Is npc.  */
			case 0:
				set_npc_combat_target(npc, npc->hit_diff_index, -1);
				break;
			/* Is player.  */
			case 1:
				set_npc_combat_target(npc, -1, npc->hit_diff_index);
				break;
		}
	}
}

/**
 * Sends the client info about how much damage, if any, a npc took.
 */
static void append_hit_npc2(struct npc_spawn *npc)
{
	write_byte_a_update(npc->hit_diff2);
	if (npc->hit_diff2 > 0 && npc->need_hp_update2 == 1) 
	{
		npc->health -= npc->hit_diff2;
		npc->need_hp_update2 = 0;
	}
	if (npc->health <= 0) 
	{
		npc->dead = 1;
		set_npc_ai_state(npc, AI_DEAD);
	}
	
	if (npc->poison_hit2 != 1)
	{
		if ((npc->hit_diff2 > 0)) 
		{
			write_byte_a_update(1);
		} 
		else
		{
			write_byte_a_update(0);
		}
	}
	else 
	{
		write_byte_a_update(2);
		npc->poison_hit2 = 0;
	}
	
	/* Client can't handle hp values over 100, so we have to do some math here
	   if an npc has more than 100 hp for it to show up normally.  */
	if (npc->max_health > 100)
	{
		int c_hp = (int) ceil(((double) npc->health 
								/ (double) npc->max_health) * 100.0);

		write_byte_update(c_hp);
		write_byte_s_update(100);
	}
	else
	{
		write_byte_update(npc->health);
		write_byte_s_update(npc->max_health);
	}

	if (npc->ai_state != AI_ATTACK && npc->ai_state != AI_DEAD
		&& npc->ai_state != AI_RESPAWN)
	{
		switch (npc->hit_diff2_from)
		{
			/* Is npc.  */
			case 0:
				set_npc_combat_target(npc, npc->hit_diff2_index, -1);
				break;
			/* Is player.  */
			case 1:
				set_npc_combat_target(npc, -1, npc->hit_diff2_index);
				break;
		}
	}
}

/* Sends the ID of the mob to face.  */
static void append_face_npc(struct npc_spawn *npc)
{
	write_word_a_update(npc->face_mob);
}

/* Changes direction an npc is facing.  */
static void append_focus_npc(struct npc_spawn *npc)
{
	write_word_bigendian_a_update(npc->focus_point_x);
	write_word_bigendian_update(npc->focus_point_y);
}

static void append_npc_string(struct npc_spawn *npc)
{
	write_string_update(npc->chat_text);
}

/**
 * Updates npc movement and sends it to a client.
 * @param *c Client instance.
 * @param *npc NPC to update.
 */
void update_npc_movement(struct client *c, struct npc_spawn *npc) 
{
	/* Check if NPC is walking or not.  */
	if (convert_server_direction_to_client_format(npc->direction) == -1) 
	{
		if (npc->update_required) 
		{
			write_bits(c, 1, 1);
			write_bits(c, 2, 0);
		} 
		else 
		{
			write_bits(c, 1, 0);
		}
	} 
	else 
	{
		write_bits(c, 1, 1);
		write_bits(c, 2, 1);
		/* Send movement direction.  */
		write_bits(c, 3, 
			convert_server_direction_to_client_format(npc->direction));
		
		if (npc->update_required) 
		{
			write_bits(c, 1, 1);
		} 
		else 
		{
			write_bits(c, 1, 0);
		}
	}
}

static void append_npc_update_blocks(struct npc_spawn *npc) 
{
	int update_mask;
	if (!npc->update_required)
	{
		return;
	}
	
	update_mask = 0;

	if (npc->update_masks[ANIMATION_REQ])
	{
		update_mask |= NPC_ANIMATION_MASK;
	}
	if (npc->update_masks[HIT_UPDATE])
	{
		update_mask |= NPC_HIT_UPDATE_MASK;
	}
	if (npc->update_masks[GFX_REQ])
	{
		update_mask |= NPC_GFX_MASK;
	}
	if (npc->update_masks[FACE_MOB])
	{
		update_mask |= NPC_FACE_MOB_MASK;
	}
	if (npc->update_masks[CHAT_TEXT])
	{
		update_mask |= NPC_CHAT_MASK;
	}
	if (npc->update_masks[HIT_UPDATE2])
	{
		update_mask |= NPC_HIT_UPDATE2_MASK;
	}
	if (npc->focus_point_x != -1)
	{
		update_mask |= NPC_FOCUS_MASK;
	}
	if (npc->update_masks[APPEARANCE_UPDATE])
	{
		update_mask |= NPC_APPEARANCE_MASK;
	}

	/*
	//NPC Flags debug text.
	if (npc->update_masks[GFX_REQ])
		printf("&& gfx rec\n");
	if (npc->update_masks[FACE_MOB])
		printf("&& face npc\n");
	if (npc->update_masks[CHAT_TEXT])
		printf("&& chat text\n");
	if (npc->update_masks[HIT_UPDATE])
		printf("&& hit update\n");
	if (npc->focus_point_x != -1)
		printf("&& focus\n");
	if (npc->update_masks[GFX_REQ])
		printf("&& gfx rec\n");
	if (npc->update_masks[HIT_UPDATE2])
		printf("&& hit update 2\n");
	if (npc->update_masks[ANIMATION_REQ])
		printf("&& animation req\n");
	*/

	write_byte_update(update_mask);
	
	if (npc->update_masks[CHAT_TEXT])
	{
		append_npc_string(npc);
	}
	if (npc->update_masks[GFX_REQ])
	{
		append_gfx_npc(npc);
	}
	if (npc->update_masks[HIT_UPDATE])
	{
		append_hit_npc(npc);
	}
	if (npc->update_masks[FACE_MOB])
	{
		append_face_npc(npc);
	}
	if (npc->update_masks[HIT_UPDATE2])
	{
		append_hit_npc2(npc);
	}
	if (npc->update_masks[ANIMATION_REQ])
	{
		append_anim_npc(npc);
	}
	if (npc->update_masks[APPEARANCE_UPDATE])
	{
		append_npc_appearance(npc);
	}
	if (npc->focus_point_x != -1)
	{
		append_focus_npc(npc);
	}
}

static void add_new_npc(struct client *c, struct npc_spawn *npc)
{
	int id, z;
	char saved_update;
	id = npc->index;

	c->plr->npc_in_list_bitmap[id >> 3] |= 1 << (id & 7);
	c->plr->local_npcs[c->plr->local_npcs_num++] = npc;

	write_bits(c, 14, id);
	write_bits(c, 12, npc->npc_id);
	write_bits(c, 1, 1);
	/* X position. */
	z = npc->world_x - c->plr->world_x;
	if (z < 0)
	{
		z += 32;
	}
	write_bits(c, 5, z);
	
	/* Y position.  */
	z = npc->world_y - c->plr->world_y;

	if (z < 0)
	{
		z += 32;
	}
	write_bits(c, 5, z);

	saved_update = npc->update_required;
	npc->update_required = 1;
	/* Turn the NPC to face the right direction when they are spawned onto the
	 * client's screen.  */
	turn_npc_to_current_direction(npc);
	append_npc_update_blocks(npc);
	npc->update_required = saved_update;
	write_bits(c, 1, 0);
}


/**
 * Handle updating of all NPC related variables relative to a client. Movement,
 * appearance, damage, etc.
 * @param *c Client instance to send the updated data to.
 */
void update_npc(struct client *c)
{
	int size = 0, x = 0;
	
	UPDATE_BLOCK->update_blocks_offset = 0;
	
	create_frame_vsize_word(c, SERVER_PKT_UPDATE_NPCS);
	init_bit_access(c);

	write_bits(c, 8, c->plr->local_npcs_num);
	size = c->plr->local_npcs_num;
	c->plr->local_npcs_num = 0;

	for (x = 0; x < size; x++)
	{
		if ((c->plr->rebuild_npc_list == 0)
			&& is_npc_within_viewpoint_distance(c, c->plr->local_npcs[x]))
		{
			update_npc_movement(c, c->plr->local_npcs[x]);
			append_npc_update_blocks(c->plr->local_npcs[x]);
			c->plr->local_npcs[c->plr->local_npcs_num++] 
			= c->plr->local_npcs[x];
		}
		else
		{
			int id;
			id = c->plr->local_npcs[x]->index;
			c->plr->npc_in_list_bitmap[id >> 3] &= ~(1 << (id & 7));
			write_bits(c, 1, 1);
			write_bits(c, 2, 3);
		}
	}

	for (x = 0; x < NUM_NPC_SPAWNS; x++)
	{
		if (NPC_SPAWNS[x] != NULL)
		{
			int id = NPC_SPAWNS[x]->index;

			if ((c->plr->rebuild_npc_list == 0)
				&& ((c->plr->npc_in_list_bitmap[id >> 3] & (1 << (id & 7))) 
					!= 0))
			{
				continue;
			}
			else if (is_npc_within_viewpoint_distance(c, NPC_SPAWNS[x]) == 0)
			{
				continue;
			}
			else
			{
				add_new_npc(c, NPC_SPAWNS[x]);
			}
		}
	}

	c->plr->rebuild_npc_list = 0;

	if (UPDATE_BLOCK->update_blocks_offset > 0)
	{
		write_bits(c, 14, 16383);
		finish_bit_access(c);
		write_bytes(c, UPDATE_BLOCK->update_blocks, 
					UPDATE_BLOCK->update_blocks_offset, 0);
	}
	else
	{
		finish_bit_access(c);
	}
	end_frame_vsize_word(c);
}

/**
 * Checks if an npc is within view distance of a specified client.
 * @param *c Client instance.
 * @param *npc NPC to check distance from.
 * @return 0 if not close enough, 1 if they are.
 */
int is_npc_within_viewpoint_distance(struct client *c, struct npc_spawn *npc) 
{
	int delta_x, delta_y;
	if (c->plr->world_z != npc->world_z)
	{
		return 0;
	}
	delta_x = npc->world_x - c->plr->world_x; 
	delta_y = npc->world_y - c->plr->world_y;
	return (delta_x <= 15) && (delta_x >= -16) && (delta_y <= 15)
			&& (delta_y >= -16);
}

/**
 * Turns the npc to a specific point.
 * @param *npc NPC to act upon.
 * @param x, y Coordinates to turn npc to face.
 */
void turn_npc_to_coord(struct npc_spawn *npc, int x, int y)
{
	npc->focus_point_x = 2 * x + 1;
	npc->focus_point_y = 2 * y + 1;
	npc->update_required = 1;
}

/**
 * Turns the npc to the direction they're currently facing. Used when an NPC is
 * added to a client's screen.
 * @param *npc NPC to turn.
 */
void turn_npc_to_current_direction(struct npc_spawn *npc)
{
	switch (npc->last_walked_direction)
	{
		case NORTH:
			turn_npc_to_coord(npc, npc->world_x, npc->world_y + 1);
			break;
		case NORTH_WEST:
			turn_npc_to_coord(npc, npc->world_x - 1, npc->world_y + 1);
			break;
		case NORTH_EAST:
			turn_npc_to_coord(npc, npc->world_x + 1, npc->world_y + 1);
			break;
		case SOUTH:
			turn_npc_to_coord(npc, npc->world_x, npc->world_y - 1);
			break;
		case SOUTH_WEST:
			turn_npc_to_coord(npc, npc->world_x - 1, npc->world_y - 1);
			break;
		case SOUTH_EAST:
			turn_npc_to_coord(npc, npc->world_x + 1, npc->world_y - 1);
			break;
		case EAST:
			turn_npc_to_coord(npc, npc->world_x + 1, npc->world_y);
			break;
		case WEST:
			turn_npc_to_coord(npc, npc->world_x - 1, npc->world_y);
			break;
	}
}

/**
 * Turns the npc to face a player.
 * @param *npc NPC to act upon.
 * @param player_idx Index of the player to face.
 */
void npc_face_player(struct npc_spawn *npc, int player_idx)
{
	npc->face_mob = (player_idx + FACE_PLAYER_OFFSET);
	npc->update_masks[FACE_MOB] = 1;
	npc->update_required = 1;
}

/**
 * Turns the npc to face an NPC.
 * @param *npc NPC to act upon.
 * @param npc_idx Index of the NPC to face.
 */
void npc_face_npc(struct npc_spawn *npc, int npc_idx)
{
	npc->face_mob = npc_idx;
	npc->update_masks[FACE_MOB] = 1;
	npc->update_required = 1;
}

/**
 * Clears the NPC facing a mob.
 * @param *npc NPC to reset.
 */
void reset_npc_face_mob(struct npc_spawn *npc)
{
	npc->face_mob = -1;
	npc->update_masks[FACE_MOB] = 1;
	npc->update_required = 1;
}

static int get_player_next_walking_direction_npc(struct npc_spawn *npc)
{
	int dir;
	dir = get_walking_direction(npc->world_x, npc->world_y, 
								npc->walk_to_x, npc->walk_to_y);

	if (dir == STATIONARY)
	{
		return -1;
	}

	return dir;
}

/**
 * Move the NPC to where their walk_to point is. Also handles adding the taken
 * mask to where they are going, and removes it from where they were.
 * @param *npc NPC to move.
 * @note Was previously a part of get_npc_movement_direction, but the function
 * name wasn't clear on that it updated the npc's position. Now it is its own
 * function for better clarity.
 */
void move_npc_to_next_point(struct npc_spawn *npc)
{
	add_collision(npc->walk_to_x, npc->walk_to_y, npc->world_z, TAKEN_MASK);
	clear_collision_bit(npc->world_x, npc->world_y, npc->world_z, TAKEN_MASK);

	npc->world_x = npc->walk_to_x;
	npc->world_y = npc->walk_to_y;
}

/**
 * Get the next movement for a npc in the format that the client is expecting.
 * @param *npc NPC to move.
 */
void get_npc_movement_direction(struct npc_spawn *npc)
{
	npc->direction = STATIONARY;
	
	if (npc->frozen_ticks == 0)
	{
		npc->last_walked_direction = npc->direction 
			= get_player_next_walking_direction_npc(npc);
	}
}

/**
 * Clear NPC update flags.
 * @param *npc NPC to clear flags for.
 */
void clear_npc_update_flags(struct npc_spawn *npc)
{
	int x;
	npc->update_required = 0;
	for (x = 0; x < 11; x++)
	{
		npc->update_masks[x] = 0;
	}
	npc->direction = STATIONARY;
	npc->focus_point_x = -1;
	npc->focus_point_y = -1;
	npc->walk_to_x = -1;
	npc->walk_to_y = -1;
}

/**
 * Make an NPC play a specific animation.
 * @param *npc NPC instance.
 * @param emote Emote id to play.
 */
void set_npc_animation(struct npc_spawn *npc, int emote)
{
	npc->animation_request = emote;
	npc->animation_wait_cycles = 0;
	npc->update_required = 1;
	npc->update_masks[ANIMATION_REQ] = 1;
}

/**
 * Make an NPC stop any currently playing animation.
 * @param *npc NPC instance.
 */
void reset_npc_animation(struct npc_spawn *npc) 
{
	npc->animation_request = -1;
	npc->animation_wait_cycles = -1;
	npc->update_required = 1;
	npc->update_masks[ANIMATION_REQ] = 1;
}

/**
 * Make an NPC play a specific graphic.
 * @param *npc NPC instance.
 * @param gfx Graphic id to play.
 */
void set_npc_gfx(struct npc_spawn *npc, int gfx)
{
	npc->gfx = gfx;
	npc->gfx_height = 6553600;
	npc->update_required = 1;
	npc->update_masks[GFX_REQ] = 1;
}

/**
 * Make an NPC play a specific graphic, at a specific height.
 * @param *npc NPC instance.
 * @param gfx Graphic id to play.
 * @param height Height to play the graphic at.
 */
void set_npc_gfx_height(struct npc_spawn *npc, int gfx, int height)
{
	npc->gfx = gfx;
	npc->gfx_height = (65536 * height);
	npc->update_required = 1;
	npc->update_masks[GFX_REQ] = 1;
}

/**
 * Makes the npc say something above their head, like player chat.
 * @param *npc NPC instance.
 * @param string[] String to show above the npc's head.
 */
void set_npc_overhead_text(struct npc_spawn *npc, char string[])
{
	strncpy(npc->chat_text, string, CHAT_TEXT_SIZE);
	npc->update_required = 1;
	npc->update_masks[CHAT_TEXT] = 1;
}

/**
 * Handle all npc timers.
 * @param *npc NPC instance.
 */
void process_npc_stats(struct npc_spawn *npc)
{
	if (npc->atk_reduced_ticks > 0)
	{
		npc->atk_reduced_ticks--;
	}
	if (npc->str_reduced_ticks > 0)
	{
		npc->str_reduced_ticks--;
	}
	if (npc->def_reduced_ticks > 0)
	{
		npc->def_reduced_ticks--;
	}
	if (npc->frozen_ticks > 0)
	{
		npc->frozen_ticks--;
	}
	if (npc->last_movement > 0)
	{
		npc->last_movement--;
	}
	if (npc->tick_killed > 0)
	{
		npc->tick_killed--;
	}
	if (npc->respawn_ticks > 0)
	{
		npc->respawn_ticks--;
	}
	if (npc->last_attack > 0)
	{
		npc->last_attack--;
	}
	if (npc->hit_gfx_delay > 0)
	{
		npc->hit_gfx_delay--;
	}
	else if (npc->hit_gfx_delay == 0)
	{
		npc->gfx = npc->hit_gfx;
		npc->gfx_height 
			= (65536 * npc->hit_gfx_height);
		npc->hit_gfx_delay = -1;
		npc->update_masks[GFX_REQ] = 1;
		npc->update_required = 1;
	}
	if (npc->projectile_delay > 0)
	{
		npc->projectile_delay--;
	}
	if (npc->poison_timer > 0)
	{
		npc->poison_timer--;
	}
	if (npc->teleport_timer > 0)
	{
		npc->teleport_timer--;
	}
	if (npc->transform_timer > 0)
	{
		npc->transform_timer--;
	}
	if (npc->walks_stop_ticks > 0)
	{
		npc->walks_stop_ticks--;
	}
	else if (npc->walks_stop_ticks == 0)
	{
		npc->walks_stop_ticks = -1;
		npc->walks = npc->default_walks;
	}
}

/**
 * Clears the walking queue and resets the read pointer for it.
 * @param *npc NPC instance.
 */
void clear_npc_walking_queue(struct npc_spawn *npc)
{
	int y;	
	for (y = 0; y < NPC_WALK_QUEUE_SIZE; y++)
	{
		npc->walk_queue_x[y] =
		npc->walk_queue_y[y] = -1;
	}
	npc->walk_queue_ptr = 0;
	npc->walk_queue_last_index = 0;
}

/**
 * Adds a walking position at a certain point in the queue.
 * @param *npc NPC instance.
 * @param x, y Walking position coords, world coordinate format.
 * @return 0 on success, 1 on queue too full to add another.
 */
int add_to_npc_walk_queue(struct npc_spawn *npc, int x, int y)
{
	if (npc->walk_queue_last_index >= NPC_WALK_QUEUE_SIZE
		|| npc->walk_queue_last_index < 0)
	{
		return 1;
	}
	
	npc->walk_queue_x[npc->walk_queue_last_index] = x;
	npc->walk_queue_y[npc->walk_queue_last_index] = y;

	npc->walk_queue_last_index++;
	
	return 0;
}

int is_npc_walking_queue_needing_processing(struct npc_spawn *npc)
{
	if (npc->walk_queue_ptr < npc->walk_queue_last_index)
	{
		return 1;
	}

	return 0;
}

/**
 * Move the npc to the next point in the walking queue.
 * @param *npc NPC instance.
 * @return 0 on success, 1 on failure.
 */
int npc_walk_from_queue(struct npc_spawn *npc)
{
	/* If there is another coord left.  */
	if (is_npc_walking_queue_needing_processing(npc))
	{
		npc->walk_to_x = npc->walk_queue_x[npc->walk_queue_ptr];
		npc->walk_to_y = npc->walk_queue_y[npc->walk_queue_ptr];
		npc->walk_queue_ptr++;

		npc->last_walked_direction = get_walking_direction(npc->world_x,
															npc->world_y,
															npc->walk_to_x,
															npc->walk_to_y);
		if (can_walk_in_direction(npc->world_x, npc->world_y, 
								  npc->world_z, npc->walk_to_x,
								  npc->walk_to_y, 1))
		{
			get_npc_movement_direction(npc);
			move_npc_to_next_point(npc);
		}
		return 0;
	}
	else
	{
		/* If there isn't a next walk coord queued up, clear everything and
		 * exit, returning a 1.  */
		clear_npc_walking_queue(npc);
		return 1;
	}
}

/**
 * Adjusts where the npc is heading based on npc size.
 * @param npc_x, npc_y NPC position coordinates.
 * @param npc_size How big the NPC is.
 * @param *t_x, *t_y Target's position.
 */
void adjust_large_npc_path(int npc_x, int npc_y, int npc_size, 
						   int *t_x, int *t_y)
{
	int internal_size = (npc_size - 1);

	/* If target is right of npc, adjust for npc size.  */
	if (*t_x > npc_x)
	{
		(*t_x) -= internal_size;
	}

	/* If target is north of npc, adjust for npc size.  */
	if (*t_y > npc_y)
	{
		(*t_y) -= internal_size;
	}
}

/**
 * Sets an npc walking path using the line algorithm, not astar.
 * @param *npc NPC instance.
 * @param x0, y0 Starting coordinates.
 * @param x1, y1 Destination coordinates.
 * @param height World height level.
 * @param include_last Should we move up to the last space (0), or on it (1).
 * @return 1 on failure, other on success.
 */
int set_npc_path(struct npc_spawn *npc, int x0, int y0, 
										int x1, int y1, int height,
										int include_last) 
{
	int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
	int err = (dx>dy ? dx : -dy)/2, e2;
	int first = 0;
	int z;

	clear_npc_walking_queue(npc);

	for (z = 0; ; z++)
	{
		/* This is so that the square the npc is currently on is skipped.  */
		if (first != 0)
		{
			if (x0 == x1 && y0 == y1) 
			{
				if (include_last == 1)
				{
					if ((get_collision(x0, y0, height) & OCCUPIED_MASK) != 0
						|| (get_collision(x0, y0, height) & OCCUPIED_MASK) != 0
						|| (get_collision(x0, y0, height) & TAKEN_MASK) != 0)
					{
						return 1;
					}
					add_to_npc_walk_queue(npc, x0, y0);
				}
				break;
			}
			else
			{
				/* If a square in the npc's path is blocked, or closed,
				   end and return 1.  */
				if ((get_collision(x0, y0, height) & OCCUPIED_MASK) != 0
					|| (get_collision(x0, y0, height) & OCCUPIED_MASK) != 0
					|| (get_collision(x0, y0, height) & TAKEN_MASK) != 0)
				{
					return 1;
				}
			}
			add_to_npc_walk_queue(npc, x0, y0);
		}
		e2 = err;

		if (e2 > -dx) 
		{
			err -= dy;
			x0 += sx;
		}

		if (e2 < dy) 
		{
			err += dx;
			y0 += sy;
		}
		first = 1;
	}

	return 0;
}

/**
 * Returns the index of a random spell the NPC can cast.
 * @param *npc NPC instance.
 * @return Returns the spell index of one of the spells that NPC can cast, -1
 * if the NPC has no spells.
 */
int get_npc_random_spell_index(struct npc_spawn *npc)
{
	if (npc->num_spells == 0)
	{
		return -1;
	}

	int result = rand_int(0, (npc->num_spells - 1));

	return get_spell_index(npc->magic_spells[result]);
}

/**
 * Get NPC's attack range based on their attack style.
 * @param *npc NPC instance.
 * @return Returns number of spaces of reach the NPC has to attack.
 */
int get_npc_atk_range(struct npc_spawn *npc)
{
	switch (npc->attack_style)
	{
		/* Melee.  */
		case NPC_USE_MELEE:
			return npc->melee_range;
		/* Ranged.  */
		case NPC_USE_RANGE:
			return npc->max_ranged;
		/* Magic.  */
		case NPC_USE_MAGIC:
			return 10;
	}
	return -1;
}

/**
 * Checks if NPC can hit a player from ranged.
 * @param *npc NPC instance.
 * @param target Client index.
 * @return 1 on success, 0 on failure.
 * @todo Change the "target" variable to take just the player struct like other
 * functions, rename this to show it is NPC vs Player specific.
 */
int is_npc_able_to_use_ranged_attack(struct npc_spawn *npc, int target)
{
	if (npc->attack_style == NPC_USE_RANGE)
	{
		if (get_distance_from_point(npc->world_x, npc->world_y, 
										   CONNECTED_CLIENTS[target]->plr->world_x,
										   CONNECTED_CLIENTS[target]->plr->world_y)
										   <= npc->max_ranged
			&& can_range_in_direction(npc->world_x, npc->world_y,
								 CONNECTED_CLIENTS[target]->plr->world_x,
								 CONNECTED_CLIENTS[target]->plr->world_y,
								 npc->world_z) == 1)
		{
			return 1;
		}
	}
	if (npc->attack_style == NPC_USE_MAGIC)
	{
		if (get_distance_from_point(npc->world_x, npc->world_y, 
										   CONNECTED_CLIENTS[target]->plr->world_x,
										   CONNECTED_CLIENTS[target]->plr->world_y)
										   <= npc->magic_range
			&& can_range_in_direction(npc->world_x, npc->world_y,
								 CONNECTED_CLIENTS[target]->plr->world_x,
								 CONNECTED_CLIENTS[target]->plr->world_y,
								 npc->world_z) == 1)
		{
			return 1;
		}
	}
	return 0;
}

/* Goes through all hits and sets the highest_hit_offset to what remains.  */
static void reset_npc_hit_offset(struct npc_spawn *npc)
{
	int x;

	for (x = npc->highest_hit_offset; x >= -1; x--)
	{
		if (x == -1)
		{
			npc->highest_hit_offset = x;
			break;
		}
		if (npc->hit_delay[x] != -1)
		{
			break;
		}
		npc->highest_hit_offset = x;
	}
}

/**
 * Handles processing of all queued NPC damage, blocking, etc.
 * @param *npc NPC instance.
 */
void process_npc_damage(struct npc_spawn *npc)
{
	int x;

	for (x = 0; x <= npc->highest_hit_offset; x++)
	{
		if (npc->hit_delay[x] > 0)
		{
			npc->hit_delay[x] -= 1;
		}
		else if (npc->hit_delay[x] == 0)
		{
			/* If npc is set to take a hit, try to put hit in hit_diff2.  */
			if (npc->update_masks[HIT_UPDATE] == 1)
			{
				if (npc->update_masks[HIT_UPDATE2] == 1)
				{
					/* If both slots are taken, just continue, will try again
					   next tick.  */
					continue;
				}
				else
				{
					npc->hit_diff2 = npc->hit_queued[x];
					npc->hit_diff2_effect = npc->hit_effect[x];
					npc->hit_diff2_index = npc->hit_index[x];
					npc->hit_diff2_from = npc->hit_from[x];

					npc->update_masks[HIT_UPDATE2] = 1;
					npc->update_required = 1;
					npc->hit_delay[x] = -1;

					if (npc->highest_hit_offset == x)
					{
						npc->highest_hit_offset--;
						reset_npc_hit_offset(npc);
					}
				}
			}
			else
			{
				npc->hit_diff = npc->hit_queued[x];
				npc->hit_diff_effect = npc->hit_effect[x];
				npc->hit_diff_index = npc->hit_index[x];
				npc->hit_diff_from = npc->hit_from[x];

				npc->update_masks[HIT_UPDATE] = 1;
				npc->update_required = 1;
				npc->hit_delay[x] = -1;

				if (npc->highest_hit_offset == x)
				{
					npc->highest_hit_offset--;
					reset_npc_hit_offset(npc);
				}
			}
		}
	}

	/* TODO: May need to tweak this so that npc will still block when stuck
	   on a fence or other thing when trying to get to a player to attack.  */
	if (npc->last_attack >= 2 && npc->update_masks[HIT_UPDATE] == 1)
	{
		/* Only play a defence animation if there is one.  */
		if (npc->blocking_emote > 0)
		{
			set_npc_animation(npc, npc->blocking_emote);
			//packet_play_sfx(c, get_npc_block_sound(npc->npc_id), 50);
		}
	}
}

/**
 * Handles npc poison damage, and fading away of poison state.
 * @param *npc NPC instance.
 */
void player_process_poison_damage_npc(struct npc_spawn *npc)
{
	/* We do not need to process at 400.  */
	if (npc->poison_timer == 400 || npc->poison_damage == -1)
	{
		return;
	}
	if ((npc->poison_timer % 100) == 0)
	{
		/* If npc is already going to take damage from a melee hit, use
		   HIT_UPDATE2.  */
		if (npc->update_masks[HIT_UPDATE] == 1)
		{
			npc->update_masks[HIT_UPDATE2] = 1;
			npc->hit_diff2 = npc->poison_damage;
			npc->need_hp_update2 = 1;
			npc->poison_hit2 = 1;
			npc->update_required = 1;
		}
		else
		{
			npc->update_masks[HIT_UPDATE] = 1;
			npc->hit_diff = npc->poison_damage;
			npc->need_hp_update = 1;
			npc->poison_hit = 1;
			npc->update_required = 1;
		}
		
		/* Timer needs to be reset, poison damage needs to be lowered.  */
		if (npc->poison_timer == 0)
		{
			if (npc->poison_damage == 1)
			{
				npc->poison_timer = npc->poison_damage = -1;
			}
			else
			{
				npc->poison_timer = 400;
				npc->poison_damage--;
			}
		}
	}
}

int is_npc_wander_enabled(struct npc_spawn *npc)
{
	return npc->walks;
}

static void npc_ai_movement_set_wander_path(struct npc_spawn *npc)
{
	if (is_npc_wander_enabled(npc))
	{
		int wander_distance = NPCS[npc->npc_id]->wander_distance;

		if (rand_int(1, npc_random_walk_chance) == npc_random_walk_chance)
		{
			int to_x = npc->spawn_x + rand_int(-wander_distance, wander_distance);
			int to_y = npc->spawn_y + rand_int(-wander_distance, wander_distance);
			
			/* This ensures that NPCS wont move too often.  */
			npc->last_movement += rand_int(5, 10);

			set_npc_path(npc, npc->world_x, npc->world_y, to_x, 
					to_y, npc->world_z, 1);
		}
	}
}

static void npc_ai_movement_to_point(struct npc_spawn *npc)
{
	npc_walk_from_queue(npc);
	
	/* FIXME: Is this needed?  */
	if (npc->walking_to_home == 1 
		&& (npc->world_x == npc->spawn_x 
			&& npc->world_y == npc->spawn_y))
	{
		npc->walking_to_point = 0;
		npc->walking_to_home = 0;
		npc->walks = npc->default_walks;
	}
}

static void npc_ai_movement_combat(struct npc_spawn *npc)
{
	if (is_npc_combat_target_player_valid(npc))
	{
		int target = npc->targeted_player;

		/* If npc too far from home, return.  */
		if ((get_distance_from_point(npc->world_x,
			 npc->world_y,
			 npc->spawn_x, 
			 npc->spawn_y) 
			 >= (NPCS[npc->npc_id]->wander_distance * 2)))
		{
			printf("NPC RETURNING, TOO FAR FROM HOME.\n");
			printf(" Current X: %d Y: %d", npc->world_x, npc->world_y);
			printf(" Spawn X: %d Y: %d", npc->spawn_x, npc->spawn_y);
			printf(" Wander Dist: %d\n", NPCS[npc->npc_id]->wander_distance);
			//reset_player_combat_target(CONNECTED_CLIENTS[target]);
			reset_npc_combat_move_to_home(npc);
		}
		/* If player has moved out of range, return.  */
		else if ((get_distance_from_point(npc->world_x,
			 npc->world_y,
			 CONNECTED_CLIENTS[target]->plr->world_x,
			 CONNECTED_CLIENTS[target]->plr->world_y)
			 >= NPCS[npc->npc_id]->wander_distance))
		{
			printf("NPC RETURNING. TARGET OUT OF RANGE.\n");
			//reset_player_combat_target(CONNECTED_CLIENTS[target]);
			reset_npc_combat_move_to_home(npc);
		}
		else if (!is_npc_is_within_combat_range_with_player(CONNECTED_CLIENTS[target], npc))
		{
			npc_move_to_player_target(CONNECTED_CLIENTS[target], npc);
		}

		/* Makes it so npcs do not walk right up to a player
		   when trying to get in range, only stops when in range or
		   can't move anymore.  */
		if (npc->attack_style != NPC_USE_MELEE)
		{
			if (is_npc_able_to_use_ranged_attack(npc, target) == 1)
			{
				clear_npc_walking_queue(npc);
			}
		}
	}

	npc_walk_from_queue(npc);
}

/**
 * Handles all AI processes for each NPC.
 * @param *npc NPC to handle.
 */
void process_npc_ai(struct npc_spawn *npc)
{
	if (npc->ai_state == AI_IDLE)
	{
		if (npc->last_movement == 0)
		{
			npc_ai_movement_set_wander_path(npc);	
		}
		else
		{
			npc_walk_from_queue(npc);
		}
	}
	else if (npc->ai_state == AI_ATTACK)
	{
		if (is_npc_combat_target_player_valid(npc))
		{
			int target = npc->targeted_player;

			npc_ai_movement_combat(npc);

			/* If it is time for the npc to attack again, do so.  */
			if (npc->last_attack == 0)
			{
				if (CONNECTED_CLIENTS[target] != NULL)
				{
					npc_attack_player(CONNECTED_CLIENTS[target], npc);
				}
				else
				{
					printf("ERROR WITH COMBAT. Player logged out "
						   "unexpectedly, ending combat.\n");
					reset_npc_combat_move_to_home(npc);
				}
			}
			/* If this npc is being attacked with ranged, drop the ammo that
			 * they were shot with, if the arrows didn't break.
			 * TODO: Probably move this to the player's combat function.  */
			if (npc->ammo_to_drop != 0)
			{
				if (rand_int(1, 100) > combat_arrow_break_chance)
				{
					add_to_item_drops_exclusive(CONNECTED_CLIENTS[target], 
												npc->ammo_to_drop, 1, 
												-1,
												npc->world_x, 
												npc->world_y,
												npc->world_z, 1);
				}

				npc->ammo_to_drop = 0;
			}
		}
	}
	/* Handle NPC death, inital animation and stop of movement.  */
	else if (npc->ai_state == AI_DEAD)
	{
		/* If NPC has not played their death animation yet.  */
		if (npc->played_death_animation == 0)
		{
			int target = npc->targeted_player;

			set_npc_animation(npc, npc->death_emote);
			/* Play NPC death sound.  */
			packet_play_sfx(CONNECTED_CLIENTS[target], 
					 get_npc_death_sound(npc->npc_id), 
					 50, -1);

			/* Clear player combat state when NPC dies.  */
			if (target != -1)
			{
				reset_player_combat_target(CONNECTED_CLIENTS[target]);
			}

			npc->tick_killed = npc->death_timer;	
			npc->poison_timer 
				= npc->poison_damage = -1;
			npc->played_death_animation = 1;
			npc->walks = 0;
		}
		/* Drops the npc's loot right before he disappears.  */
		if (npc->tick_killed == 1)
		{
			int target = npc->targeted_player;

			drop_npc_loot(npc);

			/* Clear player combat state when NPC dies.  */
			if (target != -1)
			{
				npc->targeted_player = -1;
			}

			reset_npc_combat(npc);

		}
		/* After the death animation has been played, 
		   remove the npc.  */
		if (npc->tick_killed == 0)
		{
			/* Moves npc to 0,0 in order to remove him from the 
			   player's screen.  */
			clear_collision_bit(npc->world_x, npc->world_y, npc->world_z,
								TAKEN_MASK);
			npc->world_x = 0;	
			npc->world_y = 0;
			reset_npc_animation(npc);
			npc->update_required = 1;	
			
			npc->respawn_ticks = npc->respawn;	
			
			set_npc_ai_state(npc, AI_RESPAWN);
		}
	}
	else if (npc->ai_state == AI_RESPAWN)
	{
		if (npc->respawn_ticks == 0)
		{
			/* Respawn NPC if needed.  */
			if (npc->will_respawn == 1)
			{
				npc->world_x = npc->spawn_x; 
				npc->world_y = npc->spawn_y; 
				npc->world_z = npc->spawn_z; 
				npc->health = npc->max_health;
				npc->tick_killed = 0;
				npc->walks = npc->default_walks;
				npc->last_movement = 4;
				npc->dead = 0; 
				npc->played_death_animation = 0;
				npc->update_required = 1;
				/* Reset the AI state.  */
				clear_npc_walking_queue(npc);
				set_npc_ai_state(npc, AI_IDLE);
				add_collision(npc->world_x, 
							  npc->world_y, 
							  npc->world_z, TAKEN_MASK);
			}
			/* Otherwise, clear the spawn.  */
			else
			{
				int npc_idx = npc->index;
				printf("Removed non respawning npc at %d\n", npc_idx);	
				printf("DEAD: %d WILL_RESPAWN: %d\n", npc->dead,
						npc->will_respawn);

				/* Free the memory and make sure that the NPC_SPAWN entry is
				 * now null.  */
				free(npc);
				NPC_SPAWNS[npc_idx] = NULL;
			}
		}
	}
}

/**
 * Handles npc teleportation on a delay, and graphics for it.
 * @param *npc NPC instance.
 */
void process_npc_timed_teleport(struct npc_spawn *npc)
{
	if (npc->teleport_timer == 0)
	{
		/* Need to set the spawn to its new position for walking handling.  
		   Normally, npcs can't stray too far from spawn, and won't walk 
		   outside their range.  */
		npc->world_x = npc->spawn_x = npc->stored_tele_x;
		npc->world_y = npc->spawn_y = npc->stored_tele_y;
		npc->world_z = npc->stored_tele_z;
		npc->update_required = 1;

		npc->stored_tele_x = npc->stored_tele_y = npc->stored_tele_z = 0;
		npc->teleport_timer = -1;

		/* Handle different teleport types here.  */
	}
}

/**
 * Transforms an npc to another for a certain number of ticks.
 * @param *npc NPC instance.
 * @param to_id ID to transform the NPC to.
 * @param duration Number of ticks the NPC should be transformed for.
 */
void transform_npc(struct npc_spawn *npc, int to_id, int duration)
{
	npc->npc_id = to_id;
	npc->transform_timer = duration;
	npc->update_masks[APPEARANCE_UPDATE] = 1;
	npc->update_required = 1;
}

/**
 * Handles npc transformation.
 * @param *npc NPC instance.
 */
void process_npc_transformation(struct npc_spawn *npc)
{
	if (npc->transform_timer == 0)
	{
		npc->npc_id = npc->spawn_id;
		npc->update_masks[APPEARANCE_UPDATE] = 1;
		npc->update_required = 1;

		npc->transform_timer = -1;
	}
}

/**
 * Checks for the existance of an NPC at a specific point in the world.
 * @param npc_x, npc_y, npc_z NPC world coordinates.
 * @return -1 on not found, anything else is the found NPC's index in the
 * NPC_SPAWNS array.
 */
int get_index_of_npc_at_world_coord(int npc_x, int npc_y, int npc_z)
{
	int x;

	for (x = 0; x < NUM_NPC_SPAWNS; x++)
	{
		if (NPC_SPAWNS[x]->world_x == npc_x
			&& NPC_SPAWNS[x]->world_y == npc_y
			&& NPC_SPAWNS[x]->world_z == npc_z)
		{
			return x;
		}
	}
	return -1;
}

/**
 * Create a new npc spawn and add them to the world.
 * @param npc_id NPC's base id to use.
 * @param spawn_x, spawn_y, spawn_z Spawn location.
 * @param walks If NPC will randomly wander or not.
 * @param will_respawn If NPC will respawn, or is just a one time NPC.
 * @param timer Delay until NPC is moved to the spawn.
 * @return -1 on failure, anything else is the newly created NPC's index in the
 * NPC_SPAWNS array.
 */
int spawn_new_npc(int npc_id, int spawn_x, int spawn_y, int spawn_z, int walks, 
				  int will_respawn, int timer)
{
	/* Make sure that the timer cannot be less than one. If it is, the client
	 * cannot react fast enough to the new spawn, and will crash.  */
	if (timer < 1)
	{
		timer = 1;
	}

	struct npc_spawn *npc_addr = malloc(sizeof(struct npc_spawn));
	struct npc_spawn npc_zero = {0};

	int y;

	/* Zero out the npc data.  */
	*npc_addr = npc_zero;

	/* Set up npc_spawn variables.  */	
	npc_addr->npc_id = npc_id;
	/* Copy data from the base NPC set.  */
	apply_npc_spawn_defaults(npc_addr);

	/* Set specifics for spawning.  */
	npc_addr->world_x = 0;
	npc_addr->world_y = 0;
	npc_addr->world_z = spawn_z;
	npc_addr->stored_tele_x = spawn_x;
	npc_addr->stored_tele_y = spawn_y;
	npc_addr->stored_tele_z = spawn_z;
	npc_addr->teleport_timer = timer;
	npc_addr->walks = walks;
	npc_addr->will_respawn = will_respawn;

	set_initial_npc_ai_state(npc_addr);
	
	for (y = 0; y < NUM_NPC_SPAWNS; y++)
	{
		if (NPC_SPAWNS[y] == NULL)
		{
			printf("Found empty slot: %d!\n", y);
			npc_addr->index = y;
			NPC_SPAWNS[y] = npc_addr;
			return y;
		}
	}

	printf("No empty slot found, increase NUM_NPC_SPAWNS!\n");
	free(npc_addr);
	return -1;
}

