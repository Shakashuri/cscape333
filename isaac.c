/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file isaac.c
 * @brief ISAAC RNG based off of Bob Jenkins's C code.
 * @see http://burtleburtle.net/bob/rand/isaacafa.html
*/

#include <stdint.h>

#include "isaac.h"

/**@{*/
/** ISAAC configuration data.  */
#define ISAAC_SIZEL 8
#define ISAAC_SIZE (1 << ISAAC_SIZEL)
#define ISAAC_MASK (ISAAC_SIZE - 1) << 2
/**@}*/

/**
 * @brief Run ISAAC, generates 256 random values.
 * @param *data Where needed seed/results data is.
 */
static void isaac_generate_values(struct isaac *data)
{
	register uint32_t i,x,y;

	/* cc gets incremented once every 256 results generated.  */
	data->cc = (data->cc + 1);
	/* Then combine it with bb.  */
	data->bb = (data->bb + data->cc);

	for (i = 0; i < 256; ++i)
	{
		x = data->memory[i];
		
		switch (i % 4)
		{
			case 0: 
				data->aa = data->aa^(data->aa << 13); 
				break;
			case 1: 
				data->aa = data->aa^(data->aa >> 6); 
				break;
			case 2: 
				data->aa = data->aa^(data->aa << 2); 
				break;
			case 3: 
				data->aa = data->aa^(data->aa >> 16); 
				break;
		}
		data->aa = data->memory[(i + 128) % 256] + data->aa;
		data->memory[i] = y  = data->memory[(x>>2) % 256] + data->aa 
							   + data->bb;
		data->rand_results[i] = data->bb = data->memory[(y >> 10) % 256] + x;
	}

	/* Note that bits 2..9 are chosen from x but 10..17 are chosen
	   from y.  The only important thing here is that 2..9 and 10..17
	   don't overlap.  2..9 and 10..17 were then chosen for speed in
	   the optimized version (rand.c) */
	/* See http://burtleburtle.net/bob/rand/isaac.html
		for further explanations and analysis. */
}

/**
 * @brief Macro for mixing numbers around for ISAAC.
 */
#define mix(a,b,c,d,e,f,g,h) \
{ \
   a^=b<<11; d+=a; b+=c; \
   b^=c>>2;  e+=b; c+=d; \
   c^=d<<8;  f+=c; d+=e; \
   d^=e>>16; g+=d; e+=f; \
   e^=f<<10; h+=e; f+=g; \
   f^=g>>4;  a+=f; g+=h; \
   g^=h<<8;  b+=g; h+=a; \
   h^=a>>9;  c+=h; a+=b; \
}

/**
 * @brief Initialize ISAAC.
 *
 * @param *data Where to output results.
 * @param use_seed Wether to use seed in *data or not.
 */
void initialize_isaac_data(struct isaac *data, int use_seed)
{
	int i;
	uint32_t a,b,c,d,e,f,g,h;
	data->aa = data->bb = data->cc = 0;
	/* The golden ratio.  */
	a=b=c=d=e=f=g=h=0x9e3779b9;

	for (i=0; i<4; ++i)
	{
		mix(a,b,c,d,e,f,g,h);
	}

	for (i = 0; i < ISAAC_SIZE; i += 8)
	{
		if (use_seed)
		{
			a += data->rand_results[i  ]; 
			b += data->rand_results[i+1]; 
			c += data->rand_results[i+2]; 
			d += data->rand_results[i+3];
			e += data->rand_results[i+4]; 
			f += data->rand_results[i+5]; 
			g += data->rand_results[i+6]; 
			h += data->rand_results[i+7];
		}
		mix(a,b,c,d,e,f,g,h);
		data->memory[i  ]=a; 
		data->memory[i+1]=b; 
		data->memory[i+2]=c; 
		data->memory[i+3]=d;
		data->memory[i+4]=e; 
		data->memory[i+5]=f; 
		data->memory[i+6]=g; 
		data->memory[i+7]=h;
	}

	if (use_seed)
	{        
	/* do a second pass to make all of the seed affect all of mm */
		for (i = 0; i < ISAAC_SIZE; i += 8)
		{
			a+=data->memory[i  ]; 
			b+=data->memory[i+1]; 
			c+=data->memory[i+2]; 
			d+=data->memory[i+3];
			e+=data->memory[i+4]; 
			f+=data->memory[i+5]; 
			g+=data->memory[i+6]; 
			h+=data->memory[i+7];
			
			mix(a,b,c,d,e,f,g,h);
			
			data->memory[i  ]=a; 
			data->memory[i+1]=b; 
			data->memory[i+2]=c; 
			data->memory[i+3]=d;
			data->memory[i+4]=e; 
			data->memory[i+5]=f; 
			data->memory[i+6]=g; 
			data->memory[i+7]=h;
		}
	}

	/* Generate the first set of results.  */
	isaac_generate_values(data);
	data->rand_cnt = ISAAC_SIZE;
}

/**
 * @brief Get a random value from ISAAC, generate new values if all out.
 *
 * @param *data Where to attempt to read an ISAAC result from.
 *
 * @return Returns int ISAAC result.
 */
int get_isaac_value(struct isaac *data)
{
	/* If we are at the end of the results, generate a new set.  */
	if (data->rand_cnt-- == 0)
	{
		isaac_generate_values(data);
		data->rand_cnt = (ISAAC_SIZE - 1);
	}
	return data->rand_results[data->rand_cnt];
}
