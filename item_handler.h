/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ITEM_HANDLER_H_
#define _ITEM_HANDLER_H_

void handle_use_item_on_item(struct client *c, int item_1_id, int item_1_slot,
				  int item_2_id, int item_2_slot);
void interface_item_handler(struct client *c, int interface_id, int slot, 
							int item_id, int packet_opcode);
void handle_item_on_ground(struct client *c, int item_id, int item_x, int item_y);

void handle_item_first_action(struct client *c, int frame_id, int item_id, 
					   int item_slot);
void handle_item_second_action(struct client *c, int frame_id, int item_id, 
						int item_slot);
#endif
