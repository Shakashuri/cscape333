/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DIALOGUE_H_
#define _DIALOGUE_H_

void send_npc_chat(struct client *c, char string[], char string1[], 
				   char string2[], char string3[], int npc_id, int head_anim);
void send_npc_chat_simple(struct client *c, char string[], int npc_id,
						  int head_anim);
void send_player_chat(struct client *c, char string[], char string1[], 
					  char string2[], char string3[], int head_anim);
void send_chat_choice(struct client *c, char string[], char string1[], 
					  char string2[], char string3[], char string4[]);
void send_chat_statement(struct client *c, char string[], char string1[], 
						 char string2[], char string3[], char string4[]);

void send_level_up_dialogue(struct client *c, int skill_id);
void fletching_action_menus(struct client *c);
void crafting_action_menus(struct client *c);
void cooking_action_menus(struct client *c);
void smithing_action_menus(struct client *c);
#endif
