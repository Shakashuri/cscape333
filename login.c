/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file login.c
 * @brief Login function, handles account creation, password checking, etc.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

#include "constants.h"
#include "client.h"
#include "audio.h"
#include "fileio.h"
#include "isaac.h"
#include "misc.h"
#include "stream.h"
#include "packet.h"
#include "player.h"
#include "rng.h"

/* Makes sure that the player isn't already logged in.  */
static int check_player_online(char name[])
{
	int x;
	
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			/* These are seperated, don't compare the name unless we really
			   need to.  */
			if (CONNECTED_CLIENTS[x]->plr->logged_in == 1)
			{
				if (strcmp(CONNECTED_CLIENTS[x]->plr->name, name) == 0)
				{
					return x;
				}
			}
		}
	}

	return -1;
}

/**
 * Handles game connections, checks if it is a new account, or an existing
 * one.
 * @param *c Client instance.
 * @param client_index Index of the client instance in the global array.
 * @param state At what part of the login process the client is at.
 * @return 0 on success
 * @return 1 on failure
 */
int handle_login(struct client *c, int client_index, int state)
{
	int y = 0;
	char password[21] = {0};
	long sessionkey = 0;
	int64_t client_key = 0;
	int64_t server_key = 0;
	FILE *fp;

	switch (state)
	{
		case 0:
			printf("	New connection from: %s\n", c->ip_addr);

			/* Handled in server.c due to the need to check what connection 
			 * type it is, and handle it.  */

			/* Generate the session key.  */
			sessionkey = (long int) xoroshiro128plus();
			
			/* Send the eight ignored bytes.  */
			for (y = 0; y < 8; y++)
			{
				write_char_to_socket(c, 0);
			}

			write_char_to_socket(c, 0);
			write_qword(c, sessionkey);
			send_out_stream(c);
			c->login_state = 1;
			c->login_delay = 2;
			return 0;
			break;
		
		case 1:
			/* Get connect status and size of login packet.  */
			fill_instream(c, 2);

			/* Login type, unused so far.  */
			read_unsigned_byte(c);
			c->login_packet_size = read_unsigned_byte(c);
			c->login_state = 2;
			c->login_delay = 1;
			break;

		case 2:
			fill_instream(c, c->login_packet_size);
			c->instream_offset = 2;
			/* 255 byte.  */
			read_unsigned_byte(c);
			/* Client version.  */
			read_unsigned_word(c);
			/* Client memory type.  */
			read_unsigned_byte(c);

			/* CRC values (unused as of right now).  */
			for (y = 0; y < 9; y++)
			{
				read_dword(c);
			}

			/* Unknown junk byte, not documented.  */	
			read_unsigned_byte(c);
			
			/* 10 byte.  */
			read_unsigned_byte(c);

			/* Client and server session keys.  */
			client_key = read_qword(c);
			server_key = read_qword(c);

			/* Seed decryption ISAAC routine.  */
			c->decrypt->rand_results[0] 
				= (unsigned long int) (client_key >> 32);
			c->decrypt->rand_results[1] 
				= (unsigned long int) client_key;
			c->decrypt->rand_results[2] 
				= (unsigned long int) (server_key >> 32);
			c->decrypt->rand_results[3] 
				= (unsigned long int) server_key;

			/* Seed encryptor, same as decryption, just add 50.  */
			for (y = 0; y < 4; y++)
			{
				c->encrypt->rand_results[y] 
					= (c->decrypt->rand_results[y] + 50); 
			}

			/* Initialize ISAAC with retrieved seeds.  */
			initialize_isaac_data(c->decrypt, 1);
			initialize_isaac_data(c->encrypt, 1);

			/* User id.  */
			read_dword(c);

			c->plr->id = c->plr->index = client_index;
			c->plr->id++;
			
			read_login_string(c, c->plr->name, 13);
			read_login_string(c, password, 21);

			/* Check if password and name are shorter than 3 characters.  */
			for (y = 0; y < 3; y++)
			{
				if (c->plr->name[y] == '\0' || password[y] == '\0')
				{
					printf("Login failed, pass/user not valid size. IP: %s\n", 
							c->ip_addr);
					printf("NAME: %s | %d %d %d \n", c->plr->name, 
							c->plr->name[0], c->plr->name[1], c->plr->name[2]);
					printf("PASS: %s | %d %d %d \n", password, password[0], 
							password[1], password[2]);
					write_char_to_socket(c, 3);
					write_char_to_socket(c, 0);
					write_char_to_socket(c, 0);
					free(c->encrypt);
					free(c->decrypt);
					free(c->plr);
					free(c);
					return 1;
				}
			}
			
			/* Make sure name has only ASCII chars.  */
			for (y = 0; y < 13; y++)
			{
				if (is_alphanum(c->plr->name[y]))
				{
					continue;
				}
				else
				{
					if (c->plr->name[y] == '\0')
					{
						break;
					}
					printf("Name doesn't have only ASCII chars.\n");
					write_char_to_socket(c, 3);
					write_char_to_socket(c, 0);
					write_char_to_socket(c, 0);
					free(c->encrypt);
					free(c->decrypt);
					free(c->plr);
					free(c);
					return 1;
				}
			}

			/* Password can only have ascii chars too.  */
			for (y = 0; y < 21; y++)
			{
				if (is_alphanum(password[y]))
				{
					continue;
				}
				else
				{
					if (password[y] == '\0')
					{
						break;
					}
					printf("Password doesn't have only ASCII chars.\n");
					write_char_to_socket(c, 3);
					write_char_to_socket(c, 0);
					write_char_to_socket(c, 0);
					free(c->encrypt);
					free(c->decrypt);
					free(c->plr);
					free(c);
					return 1;
				}
			}
					
			/* Ensures that player save files aren't case sensitive.  */
			for (y = 0; y < 13; y++)
			{
				c->plr->file_name[y] = to_lower(c->plr->name[y]);
			}

			/* Capitalizes the first letter of the player's name, for dialogue.  */
			c->plr->name[0] = to_upper(c->plr->name[0]);

			/* Test to see if player exists.  */
			snprintf(c->plr->save_location, 40, "%s%s", SAVE_DIR, 
					 c->plr->file_name);
			
			fp = fopen(c->plr->save_location, "r");
			
			if (fp == NULL)
			{
				printf("Player is new!\n");
				printf("New account created, %s from %s\n", 
					   c->plr->name, c->ip_addr);
				c->plr->new_player = 1;
				strncpy(c->plr->password, password, 21);
			}
			else
			{
				printf("Player %s has returned.\n", c->plr->name);
				fclose(fp);

				if (check_player_online(c->plr->name) != -1)
				{
					printf("Login attempt on online player %s\n", c->plr->name);
					printf("Login failed, already online. IP: %s\n", c->ip_addr);
					write_char_to_socket(c, 5);
					write_char_to_socket(c, 0);
					write_char_to_socket(c, 0);
					free(c->encrypt);
					free(c->decrypt);
					free(c->plr);
					free(c);
					return 1;
				}
				load_player_data(c);
				if (strcmp(password, c->plr->password) != 0)
				{
					printf("Wrong pass entered for player %s\n",
						   c->plr->name);
					printf("Login failed, wrong pass. IP: %s\n",
						   c->ip_addr);
					write_char_to_socket(c, 3);
					write_char_to_socket(c, 0);
					write_char_to_socket(c, 0);
					free(c->encrypt);
					free(c->decrypt);
					free(c->plr);
					free(c);
					return 1;
				}
			}
			c->login_state = 3;
			c->login_delay = 0;
			break;
	
		case 3:
			/* Response code.  */
			write_char_to_socket(c, 2);
			/* Player permissions.  */
			write_char_to_socket(c, c->plr->player_rights);
			/* Player flagged.  */
			write_char_to_socket(c, 0);
			
			send_out_stream(c);
			CURRENT_PLAYERS++;
			c->login_state = -1;
			c->login_delay = -1;
			break;
	}

	return 0;
}

/**
 * Handles game connections, checks if it is a new account, or an existing
 * one.
 * @param *mc Music client instance.
 * @param idx Index of the client in the CONNECTED_MUSIC_CLIENTS array.
 * @param state At what part of the login process the client is at.
 * @return 0 on success
 * @return 1 on failure
 */
int handle_music_login(struct music_client *mc, int idx, int state)
{
	int y = 0;
	FILE *fp;

	switch (state)
	{
		case 0:
			printf("	New music connection from: %s\n", mc->ip_addr);
			fill_instream_music(mc, 1);

			mc->login_packet_size = read_unsigned_music_byte(mc);
			mc->login_state = 1;
			mc->login_delay = 1;
			break;

		case 1:
			fill_instream_music(mc, mc->login_packet_size);
			read_login_music_string(mc, mc->name, 13);
			//read_login_music_string(mc, mc->password, 21);

			//printf("Name: %s\n", mc->name);
			//printf("Password: %s\n", mc->password);

			/* Make sure name has only ASCII chars.  */
			for (y = 0; y < 13; y++)
			{
				if (is_alphanum(mc->name[y]))
				{
					continue;
				}
				else
				{
					if (mc->name[y] == '\0')
					{
						break;
					}
					printf("Name doesn't have only ASCII chars.\n");
					write_char_to_music_socket(mc, 3);
					write_char_to_music_socket(mc, 0);
					write_char_to_music_socket(mc, 0);
					return 1;
				}
			}
					
			/* Ensures that player save files aren't case sensitive.  */
			for (y = 0; y < 13; y++)
			{
				mc->file_name[y] = to_lower(mc->name[y]);
			}

			/* Capitalizes the first letter of the player's name, for dialogue.  */
			mc->name[0] = to_upper(mc->name[0]);

			/* Test to see if player exists.  */
			snprintf(mc->save_location, 40, "%s%s", SAVE_DIR, mc->file_name);
			
			fp = fopen(mc->save_location, "r");
			
			if (fp == NULL)
			{
				printf("Player does not exist, kick music client.\n");
				write_char_to_music_socket(mc, 3);
				write_char_to_music_socket(mc, 0);
				write_char_to_music_socket(mc, 0);
				return 1;
			}
			else
			{
				int player_idx = check_player_online(mc->name);
				fclose(fp);
				if (player_idx != -1)
				{
					printf("Player %s music has returned.\n", mc->name);
					mc->id = player_idx;
					CONNECTED_CLIENTS[player_idx]->music_idx = idx;
					packet_play_song(CONNECTED_CLIENTS[player_idx],
							  get_region_music_id(CONNECTED_CLIENTS[player_idx]
							  					  ->plr->region_id));
				}
				else
				{
					write_char_to_music_socket(mc, 3);
					write_char_to_music_socket(mc, 0);
					write_char_to_music_socket(mc, 0);
					return 1;
				}
			}
			mc->login_state = -1;
			mc->login_delay = -1;
			break;
	
		case 3:
			break;
	}

	return 0;
}
