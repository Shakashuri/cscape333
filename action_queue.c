#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "constants.h"
#include "action_queue.h"
#include "client.h"
#include "combat.h"
#include "item.h"
#include "object_handler.h"
#include "packet.h"
#include "player.h"

void action_queue_clear(struct client *c)
{
	int x = 0;

	for (x = 0; x < ACTION_QUEUE_SIZE; x++)
	{
		c->plr->action_queue[x].ticks = 0;
		c->plr->action_queue[x].action_type = 0;
	}

	c->plr->action_queue_last_index = 0;
	c->plr->action_queue_current_index = 0;
	c->plr->action_queue_ticks = 0;
	c->plr->action_queue_next_action_ticks = 0;
}

/**
 * @todo Use variable number of args?
 */
int action_queue_add_action(struct client *c, int type, int ticks, 
							int action_data_1, int action_data_2, 
							int action_data_3, int action_data_4,
							int action_data_5, int action_data_6)
{
	if (c->plr->action_queue_last_index >= ACTION_QUEUE_SIZE)
	{
		printf("action queue full, can't add new event.\n");
		return -1;
	}

	int *index = &c->plr->action_queue_last_index;

	c->plr->action_queue[*index].ticks = ticks;
	c->plr->action_queue[*index].action_type = type;
	c->plr->action_queue[*index].action_data_1 = action_data_1;
	c->plr->action_queue[*index].action_data_2 = action_data_2;
	c->plr->action_queue[*index].action_data_3 = action_data_3;
	c->plr->action_queue[*index].action_data_4 = action_data_4;
	c->plr->action_queue[*index].action_data_5 = action_data_5;
	c->plr->action_queue[*index].action_data_6 = action_data_6;

	return (*index)++;
}

void action_queue_start(struct client *c)
{
	set_player_state(c, STATE_ACTION_QUEUE_HANDLING);
}

void action_queue_end(struct client *c)
{
	c->plr->frozen_ticks = 0;
	apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
}

void process_action_queue(struct client *c)
{
	/* Check if there is anything to process.  */
	if (c->plr->action_queue_last_index == 0)
	{
		return;
	}

	/* Initialize handling for a new action queue.  */
	if (c->plr->action_queue_last_index != 0
		&& c->plr->action_queue_ticks == 0)
	{
		printf("starting new queue!\n");
		c->plr->action_queue_next_action_ticks 
			= c->plr->action_queue[c->plr->action_queue_current_index].ticks;
		freeze_player(c, -1, 200);
	}


	int x = 0;

	for (x = c->plr->action_queue_current_index;
		 x < c->plr->action_queue_last_index; 
		 x++)
	{
		/* If not time for the next queue item, return.  */
		if (c->plr->action_queue_ticks 
			!= c->plr->action_queue_next_action_ticks)
		{
			break;
		}

		switch (c->plr->action_queue[x].action_type)
		{
			case AQ_PLAY_EMOTE:
				player_play_animation(c, c->plr->action_queue[x].action_data_1);
				break;
			case AQ_PLAY_GFX:
				player_draw_gfx_at_height(c, c->plr->action_queue[x].action_data_1,
								c->plr->action_queue[x].action_data_2);
				break;
			case AQ_MOVE_TO_POSITION:
				player_add_straight_line_path_to_walking_queue(c, 
						c->plr->action_queue[x].action_data_1,
						c->plr->action_queue[x].action_data_2,
						1,
						1);
				break;
			case AQ_TURN_TO_DIRECTION:
				player_turn_to_world_coord(c, c->plr->action_queue[x].action_data_1,
									 c->plr->action_queue[x].action_data_2);
				break;
			case AQ_OPERATE_AUTO_DOORS:
				handle_auto_door_at_pos(c, 
						c->plr->action_queue[x].action_data_1,
						c->plr->action_queue[x].action_data_2,
						c->plr->action_queue[x].action_data_3);
				break;
			case AQ_SET_STANDING_EMOTE:
				c->plr->standing_emote = c->plr->action_queue[x].action_data_1;
				c->plr->update_masks[ANIMATION_REQ] = 1;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				break;
			case AQ_SET_WALKING_EMOTE:
				c->plr->walking_emote = c->plr->action_queue[x].action_data_1;
				c->plr->update_masks[ANIMATION_REQ] = 1;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				break;
			case AQ_SET_RUNNING_EMOTE:
				c->plr->running_emote = c->plr->action_queue[x].action_data_1;
				c->plr->update_masks[ANIMATION_REQ] = 1;
				c->plr->update_masks[APPEARANCE_UPDATE] = 1;
				c->plr->update_required = 1;
				break;
			case AQ_PLAY_PHASE_MOVEMENT:
				set_phase_movement(c, c->plr->world_x, c->plr->world_y, 
					c->plr->world_x + c->plr->action_queue[x].action_data_1,
					c->plr->world_y + c->plr->action_queue[x].action_data_2,
					c->plr->action_queue[x].action_data_3,
					c->plr->action_queue[x].action_data_4,
					c->plr->action_queue[x].action_data_5);
				break;
			case AQ_SET_TIMED_TELEPORT:
				c->plr->stored_tele_x = c->plr->action_queue[x].action_data_1;
				c->plr->stored_tele_y = c->plr->action_queue[x].action_data_2;
				c->plr->stored_tele_z = c->plr->action_queue[x].action_data_3;
				c->plr->teleport_timer = c->plr->action_queue[x].action_data_4;
				c->plr->teleport_type = -c->plr->action_queue[x].action_data_5;
				break;
			case AQ_IDLE:
				break;
			default:
				printf("AQ TYPE: %d NOT HANDLED!\n", 
						c->plr->action_queue[x].action_type);
				break;
		}
		
		/* Don't shoot past the limits.  */
		if (x + 1 < ACTION_QUEUE_SIZE)
		{
			c->plr->action_queue_next_action_ticks 
				+= c->plr->action_queue[x + 1].ticks;
		}

		/* If we're going to go past the last index, stop processing. 
		 * Otherwise, move onto the next index.  */
		if (x + 1 == c->plr->action_queue_last_index)
		{
			printf("done with action queue.\n");
			set_player_state(c, STATE_IDLE);
			return;
		}
		else
		{
			c->plr->action_queue_current_index++;
		}
	}

	c->plr->action_queue_ticks++;
}

