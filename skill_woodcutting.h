#ifndef _SKILL_WOODCUTTING_H_
#define _SKILL_WOODCUTTING_H_

int get_woodcutting_level_needed(int woodcutting_id);
float get_woodcutting_chance(int woodcutting_id, int woodcutting_level);
float get_woodcutting_xp(int woodcutting_id);
int get_woodcutting_log_id(int woodcutting_id);
int get_woodcutting_respawn_ticks(int woodcutting_id);
int get_correct_stump_id(int tree_id);

void init_woodcutting(struct client *c);
void handle_chopping_tree(struct client *c);

#endif
