/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file player.c
 * @brief Contains all functions related to updating player states.
 */

#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "constants.h"
#include "action_queue.h"
#include "audio.h"
#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "debug.h"
#include "fileio.h"
#include "item.h"
#include "misc.h"
#include "npc.h"
#include "object.h"
#include "object_handler.h"
#include "pathfinding.h"
#include "skills.h"
#include "skill_agility.h"
#include "skill_cooking.h"
#include "skill_crafting.h"
#include "skill_firemaking.h"
#include "skill_fishing.h"
#include "skill_fletching.h"
#include "skill_mining.h"
#include "skill_prayer.h"
#include "skill_runecrafting.h"
#include "skill_smithing.h"
#include "skill_thieving.h"
#include "skill_woodcutting.h"
#include "stream.h"
#include "packet.h"
#include "player.h"
#include "regions.h"
#include "rng.h"

static void player_update_viewport_movement(struct client *c, struct client *other);
static void update_this_player_movement(struct client *c);
static int add_to_walking_queue(struct client *c, int x, int y);
static void reset_player_hit_offset(struct client *c);

/**
 * Sets a player's initial state to what it should be at login.
 * @param *c Client to act upon.
 */
void set_player_initial_state(struct client *c)
{
	c->plr->state = STATE_IDLE;
}

/**
 * Returns a printable string respective for the state.
 * @param state State to print info for.
 * @return Returns char string of the state.
 */
char *get_player_state_text(int state)
{
	switch (state)
	{
		case STATE_IDLE:
			return "STATE_IDLE";
		case STATE_ATTACK:
			return "STATE_ATTACK";
		case STATE_DEAD:
			return "STATE_DEAD";
		case STATE_RESPAWN:
			return "STATE_RESPAWN";
		case STATE_ACTION:
			return "STATE_ACTION";
		case STATE_ACTION_QUEUE_HANDLING:
			return "STATE_ACTION_QUEUE_HANDLING";
		
		default:
			return "STATE NOT HANDLED!";
	}
}

static int leave_player_state(struct client *c, int state)
{
	switch (state)
	{
		case STATE_IDLE:
			break;
		case STATE_ATTACK:
			break;
		case STATE_DEAD:
			break;
		case STATE_RESPAWN:
			break;
		case STATE_ACTION:
			break;
		case STATE_ACTION_QUEUE_HANDLING:
			action_queue_end(c);
			break;

		default:
			printf("State leave not handled! %d %s\n", state, 
					get_player_state_text(state));
			break;
	}

	return 0;
}

static int enter_player_state(struct client *c, int state)
{
	switch (state)
	{
		case STATE_IDLE:
			c->plr->state = state;
			break;
		case STATE_ATTACK:
			c->plr->is_attacking = 1;
			c->plr->state = state;
			break;
		case STATE_DEAD:
		case STATE_RESPAWN:
			c->plr->state = state;
			c->plr->frozen_ticks = 5;
			c->plr->frozen_type = 0;
			break;
		case STATE_ACTION:
			c->plr->state = state;
			break;
	
		default:
			printf("State enter not handled! %d %s\n", state, 
					get_player_state_text(state));
			c->plr->state = state;
			break;
	}

	return 0;
}

/**
 * Performs the needed operations to change a player to a different state.
 * @param *c Player to use.
 * @param new_state New state to use.
 */
void set_player_state(struct client *c, int new_state)
{
	if (c->plr->state == new_state)
	{
		return;
	}

	if (leave_player_state(c, c->plr->state) != 0)
	{
		printf("can't leave state %s yet\n", get_player_state_text(c->plr->state));
		return;
	}
	printf("Leaving %s entering %s\n", get_player_state_text(c->plr->state), 
		   get_player_state_text(new_state));
	enter_player_state(c, new_state);
}

void set_player_state_type(struct client *c, int new_state_type)
{
	c->plr->state_type = new_state_type;
}

void set_player_state_subtype(struct client *c, int new_state_subtype)
{
	c->plr->state_subtype = new_state_subtype;
}

/**
 * Checks if a player is alive or not.
 * @param *c Player to check.
 * @return Returns 1 if dead, 0 if not.
 */
int is_player_dead(struct client *c)
{
	if (c->plr->state == STATE_DEAD || c->plr->state == STATE_RESPAWN)
	{
		return 1;
	}

	return 0;
}

int is_player_in_combat(struct client *c)
{
	if (c->plr->last_combat_time > 0)
	{
		return 1;
	}
	return 0;
}

int is_player_targeted(struct client *c)
{
	int x = 0;

	for (x = 0; x < c->plr->local_npcs_num; x++)
	{
		if (NPC_SPAWNS[c->plr->local_npcs[x]->index]->targeted_player 
			== c->plr->index)
		{
			return 1;
		}
	}

	return 0;
}

int is_player_poisoned(struct client *c)
{
	if (c->plr->poison_damage > 0)
	{
		return 1;
	}

	return 0;
}

int is_player_poison_immune(struct client *c)
{
	if (c->plr->poison_immunity_timer != 0)
	{
		return 1;
	}

	return 0;
}

int is_player_poisonable(struct client *c)
{
	if (is_player_poisoned(c))
	{
		return 0;
	}

	if (is_player_poison_immune(c))
	{
		return 0;
	}

	return 1;
}

void set_player_poison_damage(struct client *c, int damage, int timer)
{
	c->plr->poison_damage = damage;
	c->plr->poison_timer = timer;
}

static void append_string_update(struct client *c) 
{
	write_string_update(c->plr->player_string);
}

static void append_animation_request(struct client *c) 
{
	#if 0
	if (c->plr->animation_request != -1)
	{
		c->plr->animation_request_last = c->plr->animation_request;
		//printf("fresh: %d\n", c->plr->animation_request_last);
	}
	#endif

	write_word_a_update((c->plr->animation_request == -1) ? 65535
			: c->plr->animation_request);
	write_byte_c_update(c->plr->animation_wait_cycles);
}

static void append_player_chat_text(struct client *c) 
{
	write_word_a_update((int) (((c->plr->chat_text_color & 0xFF) << 8)
			+ (c->plr->chat_text_effect & 0xFF)));
	write_byte_a_update(c->plr->player_rights);
	write_byte_a_update((int) c->plr->chat_msg_size);
	write_bytes_a_update(c->plr->chat_text, (int) c->plr->chat_msg_size, 
							   0);
}

/* Sends the ID of the mob to face.  */
static void append_face_mob_update(struct client *c)
{
	write_word_bigendian_a_update((c->plr->face_mob == -1) ? 65535 
								  : c->plr->face_mob);
}

/* Turn player to face a point.  */
static void append_set_focus_destination(struct client *c) 
{
	write_word_update(c->plr->focus_point_x);
	write_word_bigendian_a_update(c->plr->focus_point_y);
}

static void redemption_prayer_check(struct client *c)
{
	if (c->plr->level[HITPOINTS_SKILL] 
		< (int) ((double) get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]) * .1))
	{
		if (c->plr->prayers_active[PRAYER_REDEMPTION] == 1)
		{
			c->plr->level[HITPOINTS_SKILL] 
				+= (get_level_for_xp(c->plr->xp[PRAYER_SKILL]) * .25);
			c->plr->level[PRAYER_SKILL] = 0;
			player_draw_gfx(c, 436);
		}
	}
}

/* Can I consolidate this and append_hit_update2?  */
static void append_hit_update(struct client *c) 
{
	/* Ensure player can't take more damage than they have health.  */	
	if ((c->plr->level[HITPOINTS_SKILL] - c->plr->hit_diff) < 0)
	{
		c->plr->hit_diff = c->plr->level[HITPOINTS_SKILL];
	}
	write_byte_s_update(c->plr->hit_diff);
	
	/* If poison doesn't hit this tick, it is normal damage.  */
	if (c->plr->poison_hit != 1)
	{
		/* 0: blue splat (miss)
		   1: red splat (hit)
		   2: green splat (poison)
		   3: orange splat (unk)  */
		if (c->plr->hit_diff > 0) 
		{
			write_byte_update(1); 
		} 
		else 
		{
			write_byte_update(0); 
		}
	}
	else
	{
		write_byte_update(2); 
		c->plr->poison_hit = 0;
	}

	c->plr->level[HITPOINTS_SKILL] -= c->plr->hit_diff;

	/* Check if player has taken enough damage to die.  */
	if (c->plr->level[HITPOINTS_SKILL] <= 0) 
	{
		c->plr->level[HITPOINTS_SKILL] = 0;
	}
	else
	{
		/* Check for redemption prayer effect.  */
		redemption_prayer_check(c);
	}
	write_byte_a_update(c->plr->level[HITPOINTS_SKILL]);
	write_byte_c_update(get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]));
}

static void append_hit_update2(struct client *c) 
{
	/* Ensure that health cannot go below 0.  */
	if ((c->plr->level[HITPOINTS_SKILL] - c->plr->hit_diff2) < 0)
	{
		c->plr->hit_diff2 = c->plr->level[HITPOINTS_SKILL];
	}
	write_byte_c_update(c->plr->hit_diff2);
	/* If poison doesn't hit this tick, it is normal damage.  */
	if (c->plr->poison_hit2 != 1)
	{
		/* 0: blue splat (miss)
		   1: red splat (hit)
		   2: green splat (poison)
		   3: orange splat (unk)  */
		if (c->plr->hit_diff > 0) 
		{
			write_byte_a_update(1); 
		} 
		else 
		{
			write_byte_a_update(0); 
		}
	}
	else
	{
		write_byte_a_update(2); 
		c->plr->poison_hit2 = 0;
	}

	c->plr->level[HITPOINTS_SKILL] -= c->plr->hit_diff2;

	/* Check if player has taken enough damage to die.  */
	if (c->plr->level[HITPOINTS_SKILL] <= 0) 
	{
		c->plr->level[HITPOINTS_SKILL] = 0;
	}
	else
	{
		/* Check for redemption prayer effect.  */
		redemption_prayer_check(c);
	}
	write_byte_update(c->plr->level[HITPOINTS_SKILL]);
	write_byte_s_update(get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]));
}

/* Tells client to play a gfx at the player's location.  */
static void append_gfx_update(struct client *c) 
{
	write_word_update(c->plr->gfx);
	write_dword_bigendian_update(c->plr->gfx_height);
}

static int conv_world_to_in_reg_use_reg_coord(int world_coord, int reg_coord)
{
	return (world_coord - 8 * reg_coord);
}

static int conv_world_to_in_reg(int coord)
{
	int reg = (coord >> 3) - 6;
	int mod_res = (reg % 4);

	if (mod_res != 0)
	{
		printf("reg: %d mod: %d\n", reg, mod_res);
		reg -= mod_res;
	}

	return (coord - 8 * reg);
}

/* Send out the update packet that moves a player smoothly from one coord
 * to another, used in agility.  */
static void append_phase_movement_update(struct client *c, int viewing_plr_idx)
{
	/* Convert the coordinate values to be relative to region, and relative to
	 * the player that is viewing if needed.  */

	int final_start_x = 0, final_start_y = 0;
	int final_end_x = 0, final_end_y = 0;

	/* If we are updating the player who started this action, base it off of
	 * their location.  */
	if (viewing_plr_idx == -1)
	{
		final_start_x 
			= conv_world_to_in_reg_use_reg_coord(
				c->plr->phase_movement_start_x, c->plr->world_region_x);
		final_start_y 
			= conv_world_to_in_reg_use_reg_coord(
				c->plr->phase_movement_start_y, c->plr->world_region_y);
		final_end_x 
			= conv_world_to_in_reg_use_reg_coord(
				c->plr->phase_movement_end_x, c->plr->world_region_x);
		final_end_y 
			= conv_world_to_in_reg_use_reg_coord(
				c->plr->phase_movement_end_y, c->plr->world_region_y);
	}
	else
	{
		/* Make sure that the client didn't suddenly disconnect or something.
		 * If so, just render this relative to the player without any viewing
		 * modifications.  */
		if (CONNECTED_CLIENTS[viewing_plr_idx] == NULL)
		{
			final_start_x 
				= conv_world_to_in_reg_use_reg_coord(
					c->plr->phase_movement_start_x, c->plr->world_region_x);
			final_start_y 
				= conv_world_to_in_reg_use_reg_coord(
					c->plr->phase_movement_start_y, c->plr->world_region_y);
			final_end_x 
				= conv_world_to_in_reg_use_reg_coord(
					c->plr->phase_movement_end_x, c->plr->world_region_x);
			final_end_y 
				= conv_world_to_in_reg_use_reg_coord(
					c->plr->phase_movement_end_y, c->plr->world_region_y);
		}
		else
		{
			/* Find the differences between the viewer's position and the
			 * acting player's coordinates.  */
			int world_x_start_diff 
				= (c->plr->phase_movement_start_x
				   - CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_x);
			int world_y_start_diff 
				= (c->plr->phase_movement_start_y
				   - CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_y);
			int world_x_end_diff 
				= (c->plr->phase_movement_end_x
				   - CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_x);
			int world_y_end_diff 
				= (c->plr->phase_movement_end_y
				   - CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_y);

			/*
			printf("wxs: %d wys: %d\n", world_x_start_diff, world_y_start_diff);
			printf("wxe: %d wye: %d\n", world_x_end_diff, world_y_end_diff);
			*/

			/* Get the viewing player's local coordinates.  */
			final_start_x = final_end_x 
				= conv_world_to_in_reg(
					CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_x);

			final_start_y = final_end_y 
				= conv_world_to_in_reg(
					CONNECTED_CLIENTS[viewing_plr_idx]->plr->world_y);

			/* Apply the offsets we got to the viewing player's coordinate.  */
			final_start_x += world_x_start_diff;
			final_start_y += world_y_start_diff;
			final_end_x += world_x_end_diff;
			final_end_y += world_y_end_diff;

			/*
			printf("fsx: %d fsy: %d\n", final_start_x, final_start_y);
			printf("fex: %d fey: %d\n", final_end_x, final_end_y);
			*/
		}
	}

	/* Send out the update mask.  */
	write_byte_update(final_start_x);
	write_byte_c_update(final_start_y);
	write_byte_a_update(final_end_x);
	write_byte_c_update(final_end_y);
	write_word_a_update(c->plr->phase_movement_start_time);
	write_word_bigendian_a_update(c->plr->phase_movement_end_time);
	write_byte_c_update(c->plr->phase_movement_direction);
}

static void append_player_appearance(struct client *c) 
{
	c->player_props_offset = 0;
	write_byte_pprops(c, c->plr->sex);
	write_byte_pprops(c, c->plr->pk_icon);
	write_byte_pprops(c, c->plr->prayer_icon);

	if (c->plr->player_npc_id == -1)
	{
		/* Hat slot.  */
		if (c->plr->equipment[HAT_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[HAT_SLOT]);
		} 
		else 
		{
			write_byte_pprops(c, 0);
		}
		
		/* Cape slot.  */
		if (c->plr->equipment[CAPE_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[CAPE_SLOT]);
		} 
		else 
		{
			write_byte_pprops(c, 0);
		}
		
		/* Amulet slot.  */
		if (c->plr->equipment[AMULET_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[AMULET_SLOT]);
		} 
		else 
		{
			write_byte_pprops(c, 0);
		}

		/* Weapon slot.  */
		if (c->plr->equipment[WEAPON_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[WEAPON_SLOT]);
		} 
		else 
		{
			write_byte_pprops(c, 0);
		}

		/* Chest slot.  */
		if (c->plr->equipment[CHEST_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[CHEST_SLOT]);
		} 
		else 
		{
			write_word_pprops(c, 0x100 + c->plr->player_look[TORSO_LOOK]);
		}

		/* Shield slot.  */
		if (c->plr->equipment[SHIELD_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[SHIELD_SLOT]);
		} 
		else 
		{
			write_byte_pprops(c, 0);
		}

		/* Chest slot, if platebody.  */
		if (does_item_hide_arms(c->plr->equipment[CHEST_SLOT])
			|| does_item_hide_arms_and_hands(c->plr->equipment[CHEST_SLOT])) 
		{
			write_byte_pprops(c, 0);
		} 
		else 
		{
			write_word_pprops(c, 0x100 + c->plr->player_look[ARMS_LOOK]);
		}

		/* Legs slot.  */
		if (c->plr->equipment[LEGS_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[LEGS_SLOT]);
		} 
		else 
		{
			write_word_pprops(c, 0x100 + c->plr->player_look[LEGS_LOOK]);
		}

		/* Hat slot, if helm.  */
		if (does_item_hide_hair(c->plr->equipment[HAT_SLOT]) 
				|| does_item_hide_hair_and_beard(c->plr->equipment[HAT_SLOT]))
		{
			write_byte_pprops(c, 0);
		} 
		else 
		{
			write_word_pprops(c, 0x100 + c->plr->player_look[HAIR_LOOK]);
		}

		/* Hands slot.  */
		if (c->plr->equipment[HANDS_SLOT] > 1
			&& does_item_hide_hands(c->plr->equipment[CHEST_SLOT] == 0)
			&& does_item_hide_arms_and_hands(c->plr->equipment[CHEST_SLOT] == 0)
			)
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[HANDS_SLOT]);
		}
		else if (does_item_hide_hands(c->plr->equipment[CHEST_SLOT])
				 || does_item_hide_arms_and_hands(c->plr->equipment[CHEST_SLOT]))
		{
			write_byte_pprops(c, 0);
		}
		else 
		{
			write_word_pprops(c, 0x100 +  c->plr->player_look[HANDS_LOOK]);
		}

		/* Feet slot.  */
		if (c->plr->equipment[FEET_SLOT] > 1) 
		{
			write_word_pprops(c, 0x200 + c->plr->equipment[FEET_SLOT]);
		} 
		else 
		{
			write_word_pprops(c, 0x100 +  c->plr->player_look[FEET_LOOK]);
		}

		/* Hat slot, if full mask.  */
		if (does_item_hide_hair_and_beard(c->plr->equipment[HAT_SLOT])
			|| does_item_hide_beard(c->plr->equipment[HAT_SLOT]))
		{
			write_byte_pprops(c, 0);
		}
		else
		{
			write_word_pprops(c, 0x100 + c->plr->player_look[BEARD_LOOK]);
		}
	}
	else
	{
		write_word_pprops(c, -1);
		write_word_pprops(c, c->plr->player_npc_id);
	}
	
	write_byte_pprops(c, c->plr->player_look[HAIR_COLOR]);
	write_byte_pprops(c, c->plr->player_look[TORSO_COLOR]);
	write_byte_pprops(c, c->plr->player_look[LEGS_COLOR]);
	write_byte_pprops(c, c->plr->player_look[FEET_COLOR]);
	write_byte_pprops(c, c->plr->player_look[SKIN_COLOR]);

	write_word_pprops(c, c->plr->standing_emote);
	write_word_pprops(c, c->plr->turn_emote);
	write_word_pprops(c, c->plr->walking_emote);
	write_word_pprops(c, c->plr->turn_about_emote);
	write_word_pprops(c, c->plr->turn_cw_emote);
	write_word_pprops(c, c->plr->turn_ccw_emote);
	write_word_pprops(c, c->plr->running_emote);

	/* Converts player name to a int64, and then sends it.  */
	write_qword_pprops(c, convert_player_name_to_int64(c->plr->name));
	calculate_player_combat_level(c);
	
	write_byte_pprops(c, c->plr->combat_level);
	write_word_pprops(c, 0);
	
	write_byte_c_update(c->player_props_offset);
	write_bytes_reverse_update(c->player_props, c->player_props_offset, 0);
}

static void append_player_update_blocks(struct client *c, int viewing_plr_idx)
{
	if (c->plr->update_required == 0 && c->plr->update_masks[CHAT_TEXT] == 0)
	{
		return;
	}

	int update_mask = 0;
	
	if (c->plr->update_masks[GFX_REQ])
		update_mask |= PLR_GFX_MASK;
	if (c->plr->update_masks[PHASE_MOVEMENT])
		update_mask |= PLR_AGILITY_MASK;
	if (c->plr->update_masks[FACE_MOB])
		update_mask |= PLR_FACE_MOB_MASK;
	if (c->plr->update_masks[CHAT_TEXT])
		update_mask |= PLR_CHAT_MASK;
	if (c->plr->update_masks[PLR_TEXT])
		update_mask |= PLR_TEXT_MASK;
	if (c->plr->update_masks[HIT_UPDATE])
		update_mask |= PLR_HIT_UPDATE_MASK;
	if (c->plr->focus_point_x != -1)
		update_mask |= PLR_FOCUS_MASK;
	if (c->plr->update_masks[APPEARANCE_UPDATE])
		update_mask |= PLR_APPEARANCE_MASK;
	if (c->plr->update_masks[HIT_UPDATE2])
		update_mask |= PLR_HIT_UPDATE2_MASK;
	if (c->plr->update_masks[ANIMATION_REQ] != -1)
		update_mask |= PLR_ANIMATION_MASK;
	
	if (update_mask >= 0x100) 
	{
		/* This line might not be needed. XXX  */
		update_mask |= PLR_MORE_DATA_MASK;

		write_byte_update(update_mask & 0xFF);
		write_byte_update(update_mask >> 8);
	} 
	else
	{
		write_byte_update(update_mask);
	}

	/* Debug print.  */
	/*
	if (c->plr->update_masks[GFX_REQ])
		printf("$$ gfx rec\n");
	if (c->plr->update_masks[PHASE_MOVEMENT])
		printf("$$ 400 rec\n");
	if (c->plr->update_masks[FACE_MOB])
		printf("$$ face npc\n");
	if (c->plr->update_masks[CHAT_TEXT])
		printf("$$ chat text\n");
	if (c->plr->update_masks[PLR_TEXT])
		printf("$$ text\n");
	if (c->plr->update_masks[HIT_UPDATE])
		printf("$$ hit update\n");
	if (c->plr->focus_point_x != -1)
		printf("$$ focus\n");
	if (c->plr->update_masks[APPEARANCE_UPDATE])
		printf("$$ appearance\n");
	if (c->plr->update_masks[GFX_REQ])
		printf("$$ gfx rec\n");
	if (c->plr->update_masks[HIT_UPDATE2])
		printf("$$ hit update 2\n");
	if (c->plr->update_masks[ANIMATION_REQ])
		printf("$$ animation req\n");
	*/

	if (c->plr->update_masks[GFX_REQ])
		append_gfx_update(c);
	if (c->plr->update_masks[PHASE_MOVEMENT])
		append_phase_movement_update(c, viewing_plr_idx);
	if (c->plr->update_masks[FACE_MOB])
		append_face_mob_update(c);
	if (c->plr->update_masks[CHAT_TEXT])
		append_player_chat_text(c);
	if (c->plr->update_masks[PLR_TEXT])
		append_string_update(c);
	if (c->plr->update_masks[HIT_UPDATE])
		append_hit_update(c);
	if (c->plr->focus_point_x != -1)
		append_set_focus_destination(c);
	if (c->plr->update_masks[APPEARANCE_UPDATE])
		append_player_appearance(c);
	if (c->plr->update_masks[HIT_UPDATE2])
		append_hit_update2(c);
	if (c->plr->update_masks[ANIMATION_REQ] != -1)
	{
		append_animation_request(c);
		c->plr->update_masks[ANIMATION_REQ] = -1;
	}
}

/* Checks data from second arg (new player), sends it to old player
   (first arg).  */
static void add_new_player(struct client *c, struct client *other) 
{
	/* Protection for overflow in buffer when large # of players in an 
	   area.  */
	int id, z, saved_flag, saved_update_required;
	if (c->plr->local_players_num >= MAX_LOCAL_PLAYERS)
	{
		return;
	}
	
	id = other->plr->id;
	c->plr->player_in_list_bitmap[id >> 3] |= 1 << (id & 7);
	c->plr->local_players[c->plr->local_players_num++] = other;

	/* Player ID.  */
	write_bits(c, 11, id);
	/* X position.  */
	z = (other->plr->world_x - c->plr->world_x);
	if (z < 0)
	{
		z += 32;
	}
	
	write_bits(c, 5, z);
	/* Observed.  */
	write_bits(c, 1, 1);
	
	saved_flag = other->plr->update_masks[APPEARANCE_UPDATE];
	saved_update_required = other->plr->update_required;
	
	other->plr->update_masks[APPEARANCE_UPDATE] = 1;
	other->plr->update_required = 1;
	append_player_update_blocks(other, c->plr->index);
	
	other->plr->update_masks[APPEARANCE_UPDATE] = saved_flag;	
	other->plr->update_required = saved_update_required;

	/* Player teleported?  */
	write_bits(c, 1, 1);
	/* Y position.  */
	z = (other->plr->world_y - c->plr->world_y);
	
	if (z < 0)
	{
		z += 32;
	}
	
	write_bits(c, 5, z);
}

/**
 * Handles updating everything related to a player. Updates what people that
 * are near them, updates their location, appearance, chat text, etc.
 * @param *c Client instance to update.
 */
void player_update_viewport(struct client *c) 
{
	int i, id, size, save_chat_update;
	UPDATE_BLOCK->update_blocks_offset = 0;

	/*
	if (updateRunning && !updateAnnounced) {
		str.createFrame(114);
		str.writeWordBigEndian(updateSeconds * 50 / 30);
	}
	*/
	update_this_player_movement(c);
	save_chat_update = c->plr->update_masks[CHAT_TEXT];

	c->plr->update_masks[CHAT_TEXT] = 0;
	
	append_player_update_blocks(c, -1);
	c->plr->update_masks[CHAT_TEXT] = save_chat_update;

	write_bits(c, 8, c->plr->local_players_num);
	size = c->plr->local_players_num;
	/* Overflow protection.  */
	if (size >= 79)
	{
		size = 79;
	}
	
	c->plr->local_players_num = 0;
	
	for (i = 0; i < size; i++) 
	{
		if ((c->plr->local_players[i]->plr->teleported == 0)
				&& (c->plr->teleported == 0)
				&& (is_player_within_viewpoint_distance(c, 
					c->plr->local_players[i]) == 1))
		{
			player_update_viewport_movement(c, c->plr->local_players[i]);
			
			append_player_update_blocks(c->plr->local_players[i],
										c->plr->index);
			
			c->plr->local_players[c->plr->local_players_num++] 
				= c->plr->local_players[i];
		} 
		else 
		{
			id = c->plr->local_players[i]->plr->id;
			c->plr->player_in_list_bitmap[id >> 3] &= ~(1 << (id & 7));
			write_bits(c, 1, 1);
			write_bits(c, 2, 3);
		}
	}
	 
	for (i = 0; i < MAX_PLAYERS; i++) 
	{
		if ((CONNECTED_CLIENTS[i] == NULL)
			 || (CONNECTED_CLIENTS[i]->is_active == 0)
			|| (CONNECTED_CLIENTS[i] == c)) 
		{
			continue;
		}
		id = CONNECTED_CLIENTS[i]->plr->id;
		if ((c->plr->player_in_list_bitmap[id >> 3] & (1 << (id & 7))) 
			!= (char) 0)
		{
			continue;
		} 
		if (is_player_within_viewpoint_distance(c, CONNECTED_CLIENTS[i]) == 0) 
		{
			continue;
		}
		add_new_player(c, CONNECTED_CLIENTS[i]);
	}
	
	if (UPDATE_BLOCK->update_blocks_offset > 0)
	{
		write_bits(c, 11, 2047);
		finish_bit_access(c);
		write_bytes(c, UPDATE_BLOCK->update_blocks, 
					UPDATE_BLOCK->update_blocks_offset, 0);
	} 
	else 
	{
		finish_bit_access(c);
	}
	end_frame_vsize_word(c);

	if (c->plr->update_masks[HIT_UPDATE] == 1 
		|| c->plr->update_masks[HIT_UPDATE2] == 1)
	{
		packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
	}
}

static void player_update_viewport_movement(struct client *c, struct client *other)
{
	/* Updates another player's movement.  */
	if (other->plr->walking_dir == -1) 
	{
		if (other->plr->update_required || other->plr->update_masks[CHAT_TEXT]) 
		{
			write_bits(c, 1, 1);
			write_bits(c, 2, 0);
		}
		else
		{
			write_bits(c, 1, 0);
		}
	} 
	else if (other->plr->running_dir == -1) 
	{
		write_bits(c, 1, 1);
		write_bits(c, 2, 1);
		write_bits(c, 3, 
			(int) convert_server_direction_to_client_format(
										other->plr->walking_dir));
		write_bits(c, 1, (other->plr->update_required 
						  || other->plr->update_masks[CHAT_TEXT]) ? 1 : 0);
	} 
	else 
	{
		write_bits(c, 1, 1);
		write_bits(c, 2, 2);
		write_bits(c, 3,
			(int) convert_server_direction_to_client_format(
										other->plr->walking_dir));
		write_bits(c, 3,
			(int) convert_server_direction_to_client_format(
										other->plr->running_dir));
		write_bits(c, 1, (other->plr->update_required 
						  || other->plr->update_masks[CHAT_TEXT]) ? 1 : 0);
	}
	
}

static void update_this_player_movement(struct client *c) 
{
	if (c->plr->map_region_changed)
	{
		create_frame(c, SERVER_PKT_LOAD_MAP_REGION);
		write_word(c, c->plr->world_region_y + 6);
		write_word_a(c, c->plr->world_region_x + 6);

	}
	if (c->plr->teleported)
	{
		create_frame_vsize_word(c, SERVER_PKT_UPDATE_PLAYER);
		init_bit_access(c);
		write_bits(c, 1, 1);
		write_bits(c, 2, 3);
		
		write_bits(c, 1, 1);
		write_bits(c, 2, c->plr->world_z);
		write_bits(c, 7, c->plr->in_region_x);
		/* set to true, if discarding (clientside) walking queue.  */
		write_bits(c, 1, (c->plr->update_required) ? 1 : 0);
		write_bits(c, 7, c->plr->in_region_y);
		return;
	}

	if (c->plr->walking_dir == -1)
	{
		c->plr->moving = 0;
		/* don't have to update the character position, because we're just
		   standing.  */
		create_frame_vsize_word(c, SERVER_PKT_UPDATE_PLAYER);
		init_bit_access(c);
		if (c->plr->update_required) 
		{
			/* tell client there's an update block appended at the end.  */
			write_bits(c, 1, 1);
			write_bits(c, 2, 0);
		} 
		else 
		{
			write_bits(c, 1, 0);
		}
	} 
	else 
	{
		create_frame_vsize_word(c, SERVER_PKT_UPDATE_PLAYER);
		init_bit_access(c);
		write_bits(c, 1, 1);

		if (c->plr->running_dir == -1) 
		{
			c->plr->moving = 1;
			/* Send "walking packet".  */
			write_bits(c, 2, 1);
			/* updateType.  */
			write_bits(c, 3,
				(int) convert_server_direction_to_client_format(
										c->plr->walking_dir));
			/* Tells client there's an update block appended at the end.  */
			if (c->plr->update_required)
			{
				write_bits(c, 1, 1);
			}
			else
			{
				write_bits(c, 1, 0);
			}
		} 
		else 
		{
			c->plr->moving = 1;
			/* Send "running packet".  */
			write_bits(c, 2, 2);
			/* updateType.  */
			write_bits(c, 3,
				(int) convert_server_direction_to_client_format(
										c->plr->walking_dir));
			write_bits(c, 3,
				(int) convert_server_direction_to_client_format(
										c->plr->running_dir));

			/* Tells client there's an update block appended at the end.  */
			if (c->plr->update_required)
			{
				write_bits(c, 1, 1);
			}
			else
			{
				write_bits(c, 1, 0);
			}
		}
	}

}

static int add_to_walking_queue(struct client *c, int x, int y) 
{
	int next = (c->plr->walking_queue_writing_offset + 1);

	if (next >= WALK_QUEUE_SIZE)
	{
		printf("TOO MUCH FOR THE WALK_QUEUE, INCREASE SIZE: %d\n", next);
		return 1;
	}
	
	c->plr->walking_queue_x[c->plr->walking_queue_writing_offset] = x;
	c->plr->walking_queue_y[c->plr->walking_queue_writing_offset] = y;
	c->plr->walking_queue_writing_offset = next;
	return 0;
}

/**
 * Returns number of inventory slots a player has that are empty.
 * @param *c Client instance.
 * @return Returns number of empty inventory slots.
 */
int get_free_inv_slots(struct client *c)
{
	int x, count = 0;
	for (x = 0; x < INV_SIZE; x++)
	{
		if (c->plr->items[x] <= 0)
		{
			count++;
		}
	}
	return count;
}

/**
 * Resets all update flags, used in the player_update function.
 * @param *c Client instance.
 */
void clear_player_update_flags(struct client *c)
{
	int x;

	for (x = 0; x < 11; x++)
	{
		/* If ANIMATION_REQ, FACING_COORDS.  */
		/*
		if (x == 2 || x == 7)
		{
			c->plr->update_masks[x] = -1;
		}
		*/
		c->plr->update_masks[x] = 0;
	}

	c->plr->focus_point_x = -1;
	c->plr->focus_point_y = -1;
	/*
	c->plr->animation_request_last = c->plr->animation_request;
	*/
	c->plr->animation_request = -1;
	c->plr->update_required = 0;
}

/**
 * Checks if two players are within sight distance.
 * @param *c Client instance.
 * @param *other Another client, checks distances between.
 * @return 0 if not close enough, 1 if they are.
 */
int is_player_within_viewpoint_distance(struct client *c, struct client *other) 
{
	int delta_x, delta_y;

	if (c->plr->world_z != other->plr->world_z)
	{
		return 0;
	}

	delta_x = other->plr->world_x - c->plr->world_x; 
	delta_y = other->plr->world_y - c->plr->world_y;

	return (delta_x <= 15) && (delta_x >= -16) && (delta_y <= 15)
			&& (delta_y >= -16);
}

/**
 * Moves the specified player along to their destination.
 * @param *c Client instance.
 */
void player_update_world_location(struct client *c)
{
	c->plr->map_region_changed = 0;
	c->plr->teleported = 0;
	c->plr->walking_dir = -1;
	c->plr->running_dir = -1;

	/* If teleport coords are set, handle teleporting the player.  */
	if ((c->plr->tele_x != -1) && (c->plr->tele_y != -1) 
		&& (c->plr->tele_z != -1))
	{
		c->plr->map_region_changed = 1;

		/* Update the player's region, used for music tracking.  */
		c->plr->region_id = get_region_id(c->plr->tele_x, c->plr->tele_y);
		
		if ((c->plr->world_region_x != -1) && (c->plr->world_region_y != -1)) 
		{
			int relX = c->plr->tele_x - c->plr->world_region_x * 8;
			int relY = c->plr->tele_y - c->plr->world_region_y * 8;
			
			if ((relX < 16) || (relX >= 88) || (relY < 16) || (relY >= 88))
			{
				c->plr->map_region_changed = 1;
			}
			else
			{
				c->plr->map_region_changed = 0;
			}
		}

		if (c->plr->map_region_changed == 1) 
		{
			c->plr->world_region_x = (c->plr->tele_x >> 3) - 6;
			c->plr->world_region_y = (c->plr->tele_y >> 3) - 6;
		}
		
		c->plr->in_region_x = (c->plr->tele_x - 8 * c->plr->world_region_x);
		c->plr->in_region_y = (c->plr->tele_y - 8 * c->plr->world_region_y);
		
		/* Apply collision when teleporting.  */
		clear_collision_bit(c->plr->world_x, c->plr->world_y, 
							c->plr->world_z, TAKEN_MASK);
		add_collision(c->plr->tele_x, c->plr->tele_y, c->plr->tele_z, 
					  TAKEN_MASK);

		c->plr->world_x = c->plr->tele_x;
		c->plr->world_y = c->plr->tele_y;

		if (c->plr->tele_z != -1)
		{
			c->plr->world_z = c->plr->tele_z;
		}

		player_clear_walking_queue(c);
		
		c->plr->tele_x = -1; 
		c->plr->tele_y = -1;
		c->plr->tele_z = -1;
		
		c->plr->teleported = 1;
	} 
	else 
	{
		int delta_x = 0;
		int delta_y = 0;
		int i;

		c->plr->walking_dir = get_player_next_walking_direction(c);

		if (c->plr->walking_dir == -1)
		{
			/* If player is standing still, clear the temp running flag.  */
			c->plr->is_running_this_path = 0;
			return;
		}
		else
		{
			/* Only update last_walked_direction if the value isn't a -1.  */
			c->plr->last_walked_direction = c->plr->walking_dir;
			/* Update the player's region, used for music tracking.  */
			c->plr->region_id = get_region_id(c->plr->world_x, c->plr->world_y);
		}

		/* If running, move them another space.  */
		if (c->plr->is_running || c->plr->is_running_this_path) 
		{
			c->plr->running_dir = get_player_next_walking_direction(c);
		}
		
		if (c->plr->in_region_x < 16) 
		{
			delta_x = 32;
			c->plr->world_region_x -= 4;
			c->plr->map_region_changed = 1;
		} 
		else if (c->plr->in_region_x >= 88) 
		{
			delta_x = -32;
			c->plr->world_region_x += 4;
			c->plr->map_region_changed = 1;
		}

		if (c->plr->in_region_y < 16) 
		{
			delta_y = 32;
			c->plr->world_region_y -= 4;
			c->plr->map_region_changed = 1;
		} 
		else if (c->plr->in_region_y >= 88) 
		{
			delta_y = -32;
			c->plr->world_region_y += 4;
			c->plr->map_region_changed = 1;
		}

		if (c->plr->map_region_changed) 
		{
			c->plr->in_region_x += delta_x;
			c->plr->in_region_y += delta_y;
			
			for (i = 0; i < WALK_QUEUE_SIZE; i++) 
			{
				c->plr->walking_queue_x[i] += delta_x;
				c->plr->walking_queue_y[i] += delta_y;
			}
		}
	}
}

/**
 * Looks in the player's walking queue and returns where they need to go next.
 * @param *c Client instance.
 * @return -1 on failure, anything else is their direction they are going.
 */
int get_player_next_walking_direction(struct client *c)
{
	int dir;

	if (c->plr->walking_queue_reading_offset
		== c->plr->walking_queue_writing_offset)
	{
		c->plr->walking_queue_reading_offset = 0;
		c->plr->walking_queue_writing_offset = 0;
		return -1;
	}
	
	do 
	{
		dir = get_walking_direction(c->plr->in_region_x, c->plr->in_region_y,
				c->plr->walking_queue_x[c->plr->walking_queue_reading_offset], 
				c->plr->walking_queue_y[c->plr->walking_queue_reading_offset]);
		
		if (dir == STATIONARY)
		{
			c->plr->walking_queue_reading_offset 
				= ((c->plr->walking_queue_reading_offset + 1)
				   % WALK_QUEUE_SIZE);
		}
	} 
	while ((dir == STATIONARY) 
		   && (c->plr->walking_queue_reading_offset
		   	   != c->plr->walking_queue_writing_offset));
	
	if (dir == STATIONARY)
	{
		return -1;
	}
	
	/* Delta movement changes.  */
	c->plr->in_region_x 
		+= convert_server_direction_to_client_format_delta_x(dir);
	c->plr->in_region_y 
		+= convert_server_direction_to_client_format_delta_y(dir);

	clear_collision_bit(c->plr->world_x, c->plr->world_y, c->plr->world_z,
						TAKEN_MASK);

	c->plr->world_x
		+= convert_server_direction_to_client_format_delta_x(dir);
	c->plr->world_y
		+= convert_server_direction_to_client_format_delta_y(dir);

	add_collision(c->plr->world_x, c->plr->world_y, c->plr->world_z,
				  TAKEN_MASK);

	/* Clear the walking queue if this is the last step.  */
	if (c->plr->walking_queue_reading_offset + 1
		== c->plr->walking_queue_writing_offset)
	{
		c->plr->walking_queue_reading_offset = 0;
		c->plr->walking_queue_writing_offset = 0;
	}

	return dir;
}

/**
 * Clears a player's walking queue.
 * @param *c Client instance.
 */
void player_clear_walking_queue(struct client *c) 
{
	int i;
	
	c->plr->walking_queue_reading_offset = 0;
	c->plr->walking_queue_writing_offset = 0;
	
	for (i = 0; i < WALK_QUEUE_SIZE; i++) 
	{
		c->plr->walking_queue_x[i] = c->plr->in_region_x;
		c->plr->walking_queue_y[i] = c->plr->in_region_y;
	}
}

/**
 * Forces the player to walk to a point. Used for moving the player just one
 * square typically.
 * @param *c Client instance.
 * @param x, y World coordinates to move the player to.
 */
void player_move_one_square(struct client *c, int x, int y) 
{
	/* Need to convert the absolute coords into the in region coord format
	   that the walking queue uses.  */
	x -= (c->plr->world_region_x * 8);
	y -= (c->plr->world_region_y * 8);
	
	add_to_walking_queue(c, x, y);
}

/**
 * Calculates a straight line path from the player's current position to the
 * destination given. Will ignore collision, force move.
 * @param *c Client instance.
 * @param dest_x Destination X coord.
 * @param dest_y Destination Y coord.
 * @param ignore_collision 1 to ignore collision of squares, 0 to test for it.
 * @param include_last 1 to include the last square in the results, 0 to 
 * exclude it.
 * @bug Add handling to ensure path doesn't go over the allotted 99 squares.
 */
void player_add_straight_line_path_to_walking_queue(struct client *c, 
	int dest_x, int dest_y, int ignore_collision, int include_last)
{
	int results_x[99] = {-1};
	int results_y[99] = {-1};

	int x = 0;
	int num_results = 0;

	num_results = get_straight_line_path_to_point(c->plr->world_x, 
										c->plr->world_y,
										dest_x, dest_y, c->plr->world_z,
										results_x, results_y, 
										99,
										ignore_collision, include_last);
	
	for (x = 0; x < num_results; x++)
	{
		player_move_one_square(c, results_x[x], results_y[x]);
	}
}

/**
 * Turns the player to face a specific coordinate.
 * @param *c Client instance.
 * @param x, y World location to turn player to face.
 */
void player_turn_to_world_coord(struct client *c, int x, int y)
{
	if (is_player_frozen(c, 0))
	{
		return;
	}
	c->plr->focus_point_x = 2 * x + 1;
	c->plr->focus_point_y = 2 * y + 1;
	c->plr->update_required = 1;
}

/**
 * Turn the player to a certain direction.
 * @param *c Client instance.
 * @param direction Direction to turn the player to. @see PLAYER_DIRECTIONS.
 */
void player_turn_to_direction(struct client *c, int direction)
{
	if (is_player_frozen(c, 0))
	{
		return;
	}
	int x = c->plr->world_x;
	int y = c->plr->world_y;
	
	switch (direction)
	{
		case SOUTH_WEST:
			x = 0;
			y = 0;
			break;
		case SOUTH:
			y = 0;
			break;
		case SOUTH_EAST:
			x = 30000;
			y = 0;
			break;
		case WEST:
			x = 0;	
			break;
		case EAST:
			x = 30000;	
			break;
		case NORTH_WEST:
			x = 0;	
			y = 30000;
			break;
		case NORTH:
			y = 30000;
			break;
		case NORTH_EAST:
			x = 30000;
			y = 30000;
			break;
		default:
			break;
	}

	/* Update the facing direction.  */
	c->plr->last_walked_direction = direction;
	c->plr->focus_point_x = 2 * x + 1;
	c->plr->focus_point_y = 2 * y + 1;
	c->plr->update_required = 1;
}

/**
 * Turns the player to face a specific coordinate, adjusts for the size of an
 * object/npc.
 * @param *c Client instance.
 * @param x, y World location to turn player to face.
 * @param size_x, size_y Size of the object being faced.
 */
void player_turn_to_world_coord_big(struct client *c, int x, int y, int size_x, int size_y)
{
	if (size_x > 1)
	{
		x += (size_x / 2);
	}
	if (size_y > 1)
	{
		y += (size_y / 2);
	}
	player_turn_to_world_coord(c, x, y);
}

/**
 * Turns the player to face a specific coordinate, adjusts for the size of an
 * object/npc.
 * @param *c Client instance.
 * @param x, y World location to turn player to face.
 * @param size_x, size_y Size of the object being faced.
 */
void player_turn_to_object(struct client *c, int x, int y, int size_x, int size_y, int face)
{
	int temp = size_x;

	/* If facing an object that is turned, adjust the size so that it is
	 * handled properly.  */
	if (face == OBJECT_FACE_NORTH || face == OBJECT_FACE_SOUTH)
	{
		size_x = size_y;
		size_y = temp;
	}

	if (size_x > 1)
	{
		x += (size_x / 2);
	}
	if (size_y > 1)
	{
		y += (size_y / 2);
	}

	player_turn_to_world_coord(c, x, y);
}

/**
 * Turns the player to face a certain npc, persists even during movement.
 * Player "locks on" to them.
 * @param *c Client instance.
 * @param npc_index NPC index in the spawns to face.
 */
void player_face_npc(struct client *c, int npc_index)
{
	/* Prevent facing npcs when frozen.  */
	if (is_player_frozen(c, 0))
	{
		return;
	}
	c->plr->face_mob = npc_index;
	c->plr->update_masks[FACE_MOB] = 1;
	c->plr->update_required = 1;
}

/**
 * Turns the player to face a certain player, persists even during movement.
 * Player "locks on" to them.
 * @param *c Client instance.
 * @param player_index Player index in the spawns to face.
 */
void player_face_player(struct client *c, int player_index)
{
	/* Prevent facing npcs when frozen.  */
	if (is_player_frozen(c, 0))
	{
		return;
	}
	c->plr->face_mob = (player_index + FACE_PLAYER_OFFSET);
	c->plr->update_masks[FACE_MOB] = 1;
	c->plr->update_required = 1;
}

/**
 * Resets player facing an npc.
 * @param *c Client instance.
 */
void player_stop_facing_npc(struct client *c)
{
	c->plr->face_mob = -1;
	c->plr->update_masks[FACE_MOB] = 1;
	c->plr->update_required = 1;
}

/**
 * Plays a gfx at the typical height of a special attack gfx, 100 height.
 * @param *c Client instance.
 * @param gfx_id Graphic to play.
 * @todo Rename this possibly.
 */
void player_draw_gfx(struct client *c, int gfx_id)
{
	c->plr->gfx = gfx_id;
	c->plr->gfx_height = 6553600;
	c->plr->update_masks[GFX_REQ] = 1;
	c->plr->update_required = 1;
}

/**
 * Plays a gfx at a specific height, from 1 to 100.
 * @param *c Client instance.
 * @param gfx_id Graphic to play.
 * @param height Height level in the world to play the graphic at. 1 to 100.
 * @todo Check if heights over 100 are permissible.
 */
void player_draw_gfx_at_height(struct client *c, int gfx_id, int height)
{
	if (height < 1)
	{
		return;
	}

	c->plr->gfx = gfx_id;
	c->plr->gfx_height = (65536 * height);
	c->plr->update_masks[GFX_REQ] = 1;
	c->plr->update_required = 1;
}

/**
 * Goes through the player's queued damage to take, and finds the position
 * of the last hit in the array.
 * @param *c Client instance.
 */
static void reset_player_hit_offset(struct client *c)
{
	int x;

	for (x = c->plr->highest_hit_offset; x >= -1; x--)
	{
		if (x == -1)
		{
			c->plr->highest_hit_offset = x;
			break;
		}
		if (c->plr->hit_delay[x] != -1)
		{
			break;
		}
		c->plr->highest_hit_offset = x;
	}
}

void player_stop_all_movement(struct client *c)
{
	c->plr->tele_x = c->plr->world_x;
	c->plr->tele_y = c->plr->world_y;
	c->plr->tele_z = c->plr->world_z;
	c->plr->destination_movement_needs_updating = 0;
}

void player_check_if_movement_needs_updating(struct client *c)
{
	switch (c->plr->state)
	{
		case STATE_ACTION:
			switch (c->plr->state_type)
			{
				case STATE_NPC_ACTIVATE:
				case STATE_USE_ITEM_ON_NPC:
					if (c->plr->targeted_npc != -1)
					{
						if (c->plr->destination_x 
								!= NPC_SPAWNS[c->plr->targeted_npc]->world_x
							|| c->plr->destination_y 
								!= NPC_SPAWNS[c->plr->targeted_npc]->world_y)
						{
							c->plr->destination_x 
								= NPC_SPAWNS[c->plr->targeted_npc]->world_x;
							c->plr->destination_y 
								= NPC_SPAWNS[c->plr->targeted_npc]->world_y;
							c->plr->destination_movement_needs_updating = 1;
						}
					}
					break;
			}
			break;
		case STATE_ATTACK:
			if (c->plr->targeted_npc != -1)
			{
				if ((c->plr->destination_x 
						!= NPC_SPAWNS[c->plr->targeted_npc]->world_x 
					 || c->plr->destination_y 
					 	!= NPC_SPAWNS[c->plr->targeted_npc]->world_y))
				{
					c->plr->destination_x 
						= NPC_SPAWNS[c->plr->targeted_npc]->world_x;
					c->plr->destination_y 
						= NPC_SPAWNS[c->plr->targeted_npc]->world_y;
					c->plr->destination_movement_needs_updating = 1;
				}
			}
			break;
	}
}

/**
 * Moves the player to their destination. Destination is set from one of the
 * action or movement packets.
 * @todo Need to update this to handle following/attacking players.
 * @todo Need to have this handle door objects, things that take up one edge
 * of their square.
 * @param *c Client instance.
 */
void player_move_to_destination_point(struct client *c)
{
	if (c->plr->destination_movement_needs_updating)
	{
		int is_perpendicular_square_required = 0;
		int final_x = c->plr->world_x;
		int final_y = c->plr->world_y;

		int is_object_action = 0;

		switch (c->plr->state)
		{
			case STATE_ACTION:
				switch (c->plr->state_type)
				{
					case STATE_OBJECT_ACTIVATE:
					case STATE_USE_ITEM_ON_OBJECT:
					case STATE_USE_SPELL_ON_OBJECT:
						if (c->plr->action_id != -1)
						{
							c->plr->destination_size_x 
								= OBJECTS[c->plr->action_id]->size_x;
							c->plr->destination_size_y 
								= OBJECTS[c->plr->action_id]->size_x;
							is_perpendicular_square_required = 1;
							is_object_action = 1;
						}
						break;
					case STATE_NPC_ACTIVATE:
					case STATE_USE_ITEM_ON_NPC:
						if (c->plr->action_id != -1)
						{
							c->plr->destination_size_x 
								= c->plr->destination_size_y 
								= NPCS[c->plr->action_id]->size;
							is_perpendicular_square_required = 1;
						}
						break;
				}
				break;
			case STATE_ATTACK:
				if (c->plr->targeted_npc != -1)
				{
					/* Stop movement if player is using ranged attacks,
					 * and is within combat range.  */
					if (is_player_using_magic_attacks(c) 
							|| is_player_using_ranged_attacks(c))
					{
						if (is_within_combat_range(c->plr->destination_x, 
								c->plr->destination_y, 
								c->plr->destination_size_x,
								c->plr->world_x,
								c->plr->world_y,
								get_player_weapon_reach(c)) == 1)
						{
							player_stop_all_movement(c);
							player_clear_walking_queue(c);
							c->plr->destination_movement_needs_updating = 0;
							return;
						}
					}
					c->plr->destination_size_x = c->plr->destination_size_y 
						= NPCS[NPC_SPAWNS[c->plr->targeted_npc]->npc_id]->size;
					is_perpendicular_square_required = 1;
				}
				break;
		}

		/*
		printf("Orig\n");
		printf(" X: %d Y: %d\n", c->plr->destination_x, c->plr->destination_y);
		printf(" sX: %d sY: %d\n", c->plr->destination_size_x, c->plr->destination_size_y);
		printf(" Get_perpen: %d\n", is_perpendicular_square_required);
		printf(" Player state: %s\n", get_player_state_text(c->plr->state));
		*/

		/* If we aren't attacking an NPC, don't care about being on the same
		 * space as the target. Prevents stepping around to close/open doors
		 * when they technically are on the same square the player is at.  */
		if ((c->plr->world_x == c->plr->destination_x 
				&& c->plr->world_y == c->plr->destination_y)
			&& (c->plr->state != STATE_ATTACK
				|| (c->plr->state == STATE_ACTION &&
					(c->plr->state_type != STATE_NPC_ACTIVATE
					&& c->plr->state_type != STATE_USE_ITEM_ON_NPC)))
			)
		{
			c->plr->destination_movement_needs_updating = 0;
			return;
		}

		if (is_perpendicular_square_required)
		{
			int rv = 0;
			if (is_object_action)
			{
				printf("obj\n");

				get_best_object_usage_square( 
						c->plr->destination_x, 
						c->plr->destination_y, 
						c->plr->destination_size_x, 
						c->plr->destination_size_y, 
						&final_x, 
						&final_y, 
						c->plr->world_z);

			}
			else
			{
				rv = get_closest_perpendicular_square_around_entity( 
						c->plr->destination_x, 
						c->plr->destination_y, 
						c->plr->destination_size_x, 
						c->plr->destination_size_y, 
						&final_x, 
						&final_y, 
						c->plr->world_z);

				printf("rv: %d\n", rv);
			}
		}
		else
		{
			final_x = c->plr->destination_x;
			final_y = c->plr->destination_y;
		}

		/*
		printf("New\n");
		printf(" X: %d Y: %d\n", c->plr->destination_x, c->plr->destination_y);
		printf(" sX: %d sY: %d\n", c->plr->destination_size_x, c->plr->destination_size_y);
		printf(" fX: %d fY: %d\n", final_x, final_y);
		*/

		player_move_to_location(c, c->plr->world_x, c->plr->world_y, 
				final_x, final_y, 
				c->plr->world_z);
		c->plr->destination_movement_needs_updating = 0;

		//c->plr->destination_x = c->plr->destination_y = -1;
	}
}

/**
 * Handles any damage the player takes, displays the hitsplats, plays
 * blocking animation if a player or NPC attacked, and the timing matches.
 * @param *c Client instance.
 */
void player_process_damage(struct client *c)
{
	int x, block = 1;

	/* Loop through the queued hits to the player to see if any need to be
	 * applied. Decrement the timers as well.  */
	for (x = 0; x <= c->plr->highest_hit_offset; x++)
	{
		/* Decrease the timers if needed.  */
		if (c->plr->hit_delay[x] > 0)
		{
			c->plr->hit_delay[x] -= 1;
		}
		else if (c->plr->hit_delay[x] == 0)
		{
			/* If player is already set to take a hit, try to put hit in 
			 * hit_diff2.  */
			if (c->plr->update_masks[HIT_UPDATE] == 1)
			{
				if (c->plr->update_masks[HIT_UPDATE2] == 1)
				{
					/* If both slots are taken, just exit. Will try again
					   next tick.  */
					break;
				}
				else
				{
					c->plr->hit_diff2 = c->plr->hit_queued[x];
					c->plr->hit_diff2_effect = c->plr->hit_effect[x];
					c->plr->update_masks[HIT_UPDATE2] = 1;
					c->plr->update_required = 1;
					c->plr->hit_delay[x] = -1;

					print_player_damage_log(c, x);

					if (c->plr->is_attacking == 0)
					{
						set_player_retaliation_target(c, c->plr->hit_from[x], 
													  c->plr->hit_index[x]);
					}

					if (c->plr->highest_hit_offset == x)
					{
						c->plr->highest_hit_offset--;
						reset_player_hit_offset(c);
					}
				}
			}
			else
			{
				/* No previous hit set, apply it for processing.  */
				c->plr->hit_diff = c->plr->hit_queued[x];
				c->plr->hit_diff_effect = c->plr->hit_effect[x];
				c->plr->update_masks[HIT_UPDATE] = 1;
				c->plr->update_required = 1;
				c->plr->hit_delay[x] = -1;
				
				print_player_damage_log(c, x);

				if (c->plr->is_attacking == 0)
				{
					set_player_retaliation_target(c, c->plr->hit_from[x], 
												  c->plr->hit_index[x]);
				}

				if (c->plr->highest_hit_offset == x)
				{
					c->plr->highest_hit_offset--;
					reset_player_hit_offset(c);
				}
			}

			if (c->plr->hit_index[x] == -1)
			{
				block = 0;
			}

			if (c->plr->hit_from[x] != -1)
			{
				c->plr->last_combat_time = COMBAT_LOGOUT_TICKS;
			}
		}
	}

	/* Play the blocking animation if we need to.  */
	/** TODO: change the 2 for last action to a configurable value.  */
	if (c->plr->last_attack <= (c->plr->attack_speed - 1)
		&& c->plr->update_masks[HIT_UPDATE] == 1
		&& block == 1)
	{
		printf("block last atk: %d\n", c->plr->last_attack);
		player_play_animation(c, c->plr->blocking_emote);
	}
}

static void retribution_prayer_check(struct client *c)
{
	if (c->plr->prayers_active[PRAYER_RETRIBUTION] == 1)
	{
		int max_dmg = (get_level_for_xp(c->plr->xp[PRAYER_SKILL]) * 0.25);
		int x;

		/* Drain prayer.  */
		c->plr->level[PRAYER_SKILL] = 0;
		player_draw_gfx_at_height(c, 437, 20);

		/* If in wilderness, harm players around them.  */
		if (is_player_in_wilderness(c) == 1)
		{
			for (x = 0; x < c->plr->local_players_num; x++)
			{
				if (check_good_distance(c->plr->world_x, c->plr->world_y, 
										c->plr->local_players[x]->plr->world_x,
										c->plr->local_players[x]->plr->world_y,
										1) == 1)
				{
					deal_damage_to_player(c->plr->local_players[x], 
										  rand_int(1, max_dmg), DMG_TYPE_MAGIC,
										  0, EFFECT_RETRIBUTION, 1, 
										  c->plr->index);
				}
			}
		}

		for (x = 0; x < c->plr->local_npcs_num; x++)
		{
			if (check_good_distance(c->plr->world_x, c->plr->world_y, 
									c->plr->local_npcs[x]->world_x,
									c->plr->local_npcs[x]->world_y,
									1) == 1)
			{
				deal_damage_to_npc(c->plr->local_npcs[x], rand_int(1, max_dmg),
								   0, EFFECT_RETRIBUTION, 1, c->plr->index);
			}
		}
	}
}

/**
 * Checks if a player has reached 0 health or is dead, handles any events
 * that need to be, and respawns them.
 * @param *c Client instance.
 */
void player_process_death(struct client *c)
{
	if (c->plr->level[HITPOINTS_SKILL] <= 0 && c->plr->death_stage == 0)
	{
		c->plr->death_stage = 1;
		c->plr->death_items_kept = get_num_items_kept_on_death(c);
		c->plr->frozen_ticks = 4;
		c->plr->frozen_type = EFFECT_FREEZE_NORMAL;
		retribution_prayer_check(c);
		printf("Player %s has died!\n", c->plr->name);
		player_play_animation(c, 836);
		clear_collision_bit(c->plr->world_x, c->plr->world_y, 
							c->plr->world_z, TAKEN_MASK);
		reset_player_combat(c);
		reset_skilling(c);
		packet_close_all_open_interfaces(c);
	}
	else if (c->plr->death_stage > 0)
	{
		if (c->plr->death_stage == 4)
		{
			/* Drop items needed on death.  */
			packet_player_drop_item_on_grounds_on_death(c, c->plr->death_items_kept);
			c->plr->death_items_kept = -1;

			c->plr->tele_x = c->plr->home_x;
			c->plr->tele_y = c->plr->home_y;
			c->plr->tele_z = c->plr->home_z;

			add_collision(c->plr->tele_x, c->plr->tele_y, 
						  c->plr->tele_z, TAKEN_MASK);

			/* Make the player face right, instead of their original,
			   pre-death direction.  */
			player_turn_to_world_coord(c, c->plr->tele_x + 1, c->plr->tele_y);
			packet_send_chatbox_message(c, "Oh dear, you are dead!");
			c->plr->death_stage = 0;
		
			/* Heal player back up to full.  */
			c->plr->level[HITPOINTS_SKILL]
				= c->plr->base_level[HITPOINTS_SKILL];
			packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
			/* TODO: Stats are reset as well.  */
			player_replay_animation(c);
		}
		else
		{
			c->plr->death_stage++;
			/* Is this needed?  */
			player_play_animation(c, 836);
		}
	}
}

/**
 * Handles health regen over time for a player.
 * @param *c Client instance.
 */
void player_process_health_regen(struct client *c)
{
	/* I don't think we need this, it looks like health regens even in 
	   combat.  */
	if (c->plr->health_regen == 0)
	{
		if (c->plr->level[HITPOINTS_SKILL] 
			< get_level_for_xp(c->plr->xp[HITPOINTS_SKILL]))
		{
			c->plr->level[HITPOINTS_SKILL]++;
			c->plr->health_regen = 100;
			packet_send_updated_skill_stats(c, HITPOINTS_SKILL);
		}
	}
}

/**
 * Handles stat drain returning to normal over time.
 * @param *c Client instance.
 */
void player_process_stat_regen(struct client *c)
{
	int x = 0;

	if (c->plr->stat_regen == 0)
	{
		for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
		{
			/* Checks if a stat is below what level it should be, regens it
			 * as long as it isn't prayer.  */
			if (x != PRAYER_SKILL)
			{
				if (c->plr->level[x] < c->plr->base_level[x])
				{
					c->plr->level[x]++;
				}
			}
		}

		/* Process temporary bonuses as well.  */
		for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
		{
			if (c->plr->temp_bonuses[x] < 0)
			{
				c->plr->temp_bonuses[x]++;
			}
			packet_send_updated_skill_stats(c, x);
		}

		c->plr->stat_regen = 100;
	}
}

/**
 * Handles stat buffs returning to normal over time.
 * @param *c Client instance.
 */
void player_process_stat_decay(struct client *c)
{
	int x = 0;

	if (c->plr->stat_decay == 0)
	{
		for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
		{
			/* If a stat is boosted beyond the normal, start to bring it back
			 * down to normal levels.  */
			if (c->plr->level[x] > c->plr->base_level[x])
			{
				c->plr->level[x]--;
			}
		}

		/* Process temporary bonuses as well.  */
		for (x = 0; x < PROTOCOL_NUM_SKILLS; x++)
		{
			if (c->plr->temp_bonuses[x] > 0)
			{
				c->plr->temp_bonuses[x]--;
			}
			packet_send_updated_skill_stats(c, x);
		}

		c->plr->stat_decay = 100;
	}
}

/**
 * Handles prayer points draining when prayers are used.
 * @param *c Client instance.
 */
void player_process_prayer_drain(struct client *c)
{
	int x = 0;

	/* If prayer happens to go down to 0, check if any prayers are active. If
	 * so, deactivate them and refresh prayer level info.  */
	if (c->plr->level[PRAYER_SKILL] == 0)
	{
		for (x = 0; x < PROTOCOL_NUM_PRAYERS; x++)
		{
			if (c->plr->prayers_active[x] == 1)
			{
				disable_prayer(c, x);
				packet_send_updated_skill_stats(c, PRAYER_SKILL);
				packet_send_chatbox_message(c, "You have run out of prayer "
						"points, you must recharge at an altar.");
			}
		}
		return;
	}

	if (c->plr->prayer_drain_counter > c->plr->prayer_drain_resistance)
	{
		while (c->plr->prayer_drain_counter > c->plr->prayer_drain_resistance)
		{
			c->plr->level[PRAYER_SKILL]--;
			c->plr->prayer_drain_counter -= c->plr->prayer_drain_resistance;
		}

		packet_send_updated_skill_stats(c, PRAYER_SKILL);
	}

	if (c->plr->prayer_total_drain > 0)
	{
		c->plr->prayer_drain_counter += c->plr->prayer_total_drain;
	}
}

/**
 * Handles application of poison damage, reduction of poison severity.
 * @param *c Client instance.
 * @todo Roll this under player_process_damage?
 */
void player_process_poison_damage(struct client *c)
{
	/* We do not need to process at 400.  */
	if (c->plr->poison_timer == 400 || c->plr->poison_damage == -1)
	{
		return;
	}
	if ((c->plr->poison_timer % 100) == 0)
	{
		/* If player is already going to take damage from a melee hit, use
		   HIT_UPDATE2.  */
		if (c->plr->update_masks[HIT_UPDATE] == 1)
		{
			c->plr->update_masks[HIT_UPDATE2] = 1;
			c->plr->hit_diff2 = c->plr->poison_damage;
			c->plr->poison_hit2 = 1;
			c->plr->update_required = 1;
		}
		else
		{
			c->plr->update_masks[HIT_UPDATE] = 1;
			c->plr->hit_diff = c->plr->poison_damage;
			c->plr->poison_hit = 1;
			c->plr->update_required = 1;
		}
		c->plr->last_combat_time = COMBAT_LOGOUT_TICKS;
		
		/* Timer needs to be reset, poison damage needs to be lowered.  */
		if (c->plr->poison_timer == 0)
		{
			if (c->plr->poison_damage == 1)
			{
				packet_send_chatbox_message(c, "The poison fades away.");
				c->plr->poison_timer = c->plr->poison_damage = -1;
			}
			else
			{
				c->plr->poison_timer = 400;
				c->plr->poison_damage--;
			}
		}
	}
}

/**
 * Handle skill actions when needed.
 * @param *c Client instance.
 */
void player_process_skills(struct client *c)
{
	if (c->plr->action_ticks == 0)
	{
		/* Ensure that the player didn't remove items that are needed for the
		   skill.  */
		/* FIXME: Maybe just add reset_skilling calls to when the player
		   drops or unequips items?  */
		if (c->plr->skill_tool_equip == 0)
		{
			if (c->plr->skill_tool_id 
				!= c->plr->items[c->plr->skill_tool_slot])
			{
				printf("reset skill, no equip.\n");
				printf("\ttool name: %s\n", ITEMS[c->plr->skill_tool_id]->name);
				printf("\ttool id: %d\n", c->plr->skill_tool_id);
				printf("\tslot id: %d\n", c->plr->skill_tool_slot);
				reset_skilling(c);
				return;
			}
		}
		else if (c->plr->skill_tool_equip == 1)
		{
			if (c->plr->skill_tool_id 
				!= c->plr->equipment[c->plr->skill_tool_slot])
			{
				printf("reset skill, no item.\n");
				reset_skilling(c);
				return;
			}
		}
		if (c->plr->skill_req_one_id != -1)
		{
			if (player_has_item(c, c->plr->skill_req_one_id, 1) == -1)
			{
				printf("req one reset\n");
				reset_skilling(c);
				return;
			}
		}
		if (c->plr->action_skill_id == COOKING_SKILL)
		{
			printf("cooking\n");
			switch (c->plr->action_skill_type)
			{
				/* Normal cooking, basic put on fire.  */
				case 0:
					handle_cooking(c);
					break;
			}
		}
		if (c->plr->action_skill_id == WOODCUTTING_SKILL)
		{
			handle_chopping_tree(c);
		}
		else if (c->plr->action_skill_id == MINING_SKILL)
		{
			handle_mining_ore(c);
		}
		else if (c->plr->action_skill_id == FISHING_SKILL)
		{
			handle_fishing(c);
		}
		else if (c->plr->action_skill_id == FLETCHING_SKILL)
		{
			handle_fletching(c);
		}
		else if (c->plr->action_skill_id == FIREMAKING_SKILL)
		{
			/* If player is burning a funeral pyre instead, handle it.  */
			if (c->plr->action_skill_type == 1)
			{
				light_pyre_remains(c);
			}
			else
			{
				handle_firemaking(c);
			}
		}
		else if (c->plr->action_skill_id == CRAFTING_SKILL)
		{
			handle_crafting(c);
		}
		else if (c->plr->action_skill_id == SMITHING_SKILL)
		{
			if (c->plr->action_skill_type == SMITHING_ACTION_SMITH)
			{
				handle_smithing(c);
			}
			else if (c->plr->action_skill_type == SMITHING_ACTION_SMELT)
			{
				handle_smelting(c);
			}
		}
		else if (c->plr->action_skill_id == AGILITY_SKILL)
		{
			if (c->plr->action_skill_type == AGILITY_ACTION_TURNING
				|| c->plr->action_skill_type == AGILITY_ACTION_GETTING_CLOSER)
			{
				printf("init agility\n");
				init_agility(c);
			}
			else if (c->plr->action_skill_type == AGILITY_ACTION_PROCESSING)
			{
				printf("handling\n");
				handle_agility_obstacle(c);
			}
		}
		else if (c->plr->action_skill_id == THIEVING_SKILL)
		{
			handle_thieving(c);
		}
		else if (c->plr->action_skill_id == RUNECRAFTING_SKILL)
		{
			handle_runecrafting(c);
		}
	}
}

/**
 * Handles delayed teleportation, spell teleports and their animations.
 * @param *c Client instance.
 * @todo Replace teleport_type with an enum, make it more clear.
 */
void player_process_timed_teleport(struct client *c)
{
	if (c->plr->teleport_timer == 0)
	{
		c->plr->tele_x = c->plr->stored_tele_x;
		c->plr->tele_y = c->plr->stored_tele_y;
		c->plr->tele_z = c->plr->stored_tele_z;
		c->plr->stored_tele_z = c->plr->stored_tele_x 
			= c->plr->stored_tele_y = 0;
		c->plr->teleport_timer = -1;

		c->plr->last_tele_z_diff = (c->plr->tele_z - c->plr->world_z);
		
		/* If teleporting from normal spellbook, play the 'warp in' emote on
		   arrival.  */
		if (c->plr->teleport_type == 0)
		{
			player_play_animation(c, 715);
		}
	}
}

/**
 * Handles energy drain when running, restoration of it when not.
 * @param *c Client instance.
 */
void player_process_run_energy(struct client *c)
{
	if ((c->plr->is_running == 1 || c->plr->is_running_this_path == 1)
		&& c->plr->moving != 0)
	{
		if (c->plr->running_energy > 0)
		{
			/* Cap weight that has an effect on running energy to 64, and 0.
			 * Weights under 0 do not affect run energy recharge rate.  */
			c->plr->running_drain += (0.67 
				+ (fmax(fmin(c->plr->weight, 64.0), 0.0) / 100));
			if (c->plr->running_drain > 1.0)
			{
				c->plr->running_energy -= (int) c->plr->running_drain;
				c->plr->running_drain -= 1.0;
				packet_send_run_energy(c);
			}
		}
		else
		{
			c->plr->is_running = 0;
			c->plr->is_running_this_path = 0;
			packet_set_client_config_value(c, WALK_OR_RUN_MODE, 0);
		}
	}
	else
	{
		c->plr->running_drain = 0.0;
		if (c->plr->running_energy != 100)
		{
			c->plr->running_restore 
				+= (8 + (c->plr->level[AGILITY_SKILL] / 6));
			if (c->plr->running_restore >= 100 )
			{
				c->plr->running_energy += 1;
				packet_send_run_energy(c);
				c->plr->running_restore -= 100;
			}
		}
	}
}

/**
 * Handles following a npc or player, updates pathfinding when needed.
 * @param *c Client instance.
 * @todo Investigate if this uses line movement instead of astar. Might be the
 * case.
 */
void player_process_following_target(struct client *c)
{
	if (c->plr->targeted_npc != -1 && c->plr->is_pursuing_target == 1)
	{
		player_move_is_within_combat_range(c, c->plr->world_x, c->plr->world_y,
								 NPC_SPAWNS[c->plr->targeted_npc]->world_x, 
								 NPC_SPAWNS[c->plr->targeted_npc]->world_y, 
								 c->plr->world_z,
								 NPCS[NPC_SPAWNS[c->plr->targeted_npc]->npc_id]->size,
								 get_player_weapon_reach(c));
	}
}

/**
 * Handles autosaving of player data every minute or so.
 * @param *c Client.
 */
void player_process_save(struct client *c)
{
	if (c->plr->save_data_ticks == 0)
	{
		/* If player is not dead, save data. Otherwise, wait about 10 seconds
		 * more to see if they are out of combat or alive by then.  */
		if (is_player_dead(c) == 0 && is_player_in_combat(c) == 0)
		{
			save_player_data(c);
			c->plr->save_data_ticks
			= rand_int(MIN_PLAYER_SAVE, MAX_PLAYER_SAVE);
		}
		else
		{
			c->plr->save_data_ticks = 17;
		}
	}
}

/**
 * Handle the player unlocking new music when they change regions, and play
 * music when it has been unlocked.
 * @param *c Client instance.
 * @todo Need to handle the different music client settings.
 */
void player_process_music(struct client *c)
{
	if (c->plr->region_id != c->plr->old_region_id)
	//if (c->plr->map_region_changed)
	{
		/*
		printf("Region has changed!\n");
		printf("Old: %d New: %d\n", c->plr->old_region_id, c->plr->region_id);
		*/
		c->plr->old_region_id = c->plr->region_id;

		int music_id = get_region_music_id(c->plr->region_id);
		int music_idx = get_music_index(music_id);

		if (music_idx != -1)
		{
			if (c->plr->unlocked_music[music_idx] == 0)
			{
				c->plr->unlocked_music[music_idx] = 1;

				if (music_show_song_unlock_message == 1)
				{
					char return_msg[400];
					snprintf(return_msg, 400, "You've unlocked the "
							 "music track \"%s\"!",
							 MUSIC_DATA[music_idx]->name);
					packet_send_chatbox_message(c, return_msg);
				}

				update_unlocked_music_list(c);

				packet_play_song(c, music_id);
			}
			else
			{
				packet_queue_song(c, music_id);
			}
		}
		else
		{
			printf("Music ID: %d Name: null\n", music_id);
		}
	}
}

/**
 * Processes timers, counters on a player.
 * @param *c Client.
 */
void player_process_stats(struct client *c)
{
	/* Combat related.  */
	if (c->plr->atk_reduced_ticks > 0)
	{
		c->plr->atk_reduced_ticks--;
	}
	if (c->plr->str_reduced_ticks > 0)
	{
		c->plr->str_reduced_ticks--;
	}
	if (c->plr->def_reduced_ticks > 0)
	{
		c->plr->def_reduced_ticks--;
	}
	if (c->plr->frozen_ticks > 0)
	{
		c->plr->frozen_ticks--;
		
		/* TODO: Make sure this is taken care of if the player logs out while
		 * moving.  */
		if (c->plr->frozen_type == EFFECT_FREEZE_AUTODOOR)
		{
			if (c->plr->moving == 1)
			{
				printf("moving!\n");
			}
			else
			{
				printf("not!\n");
				c->plr->frozen_ticks = 0;
				c->plr->frozen_type = EFFECT_CLEAR;
				close_auto_door(c->plr->action_store_one);
			}
		}
	}
	else if (c->plr->frozen_ticks == 0 && c->plr->frozen_type != EFFECT_CLEAR)
	{
		if (c->plr->frozen_type == EFFECT_FREEZE_STUN)
		{
			player_draw_gfx(c, -1);	
		}
		c->plr->frozen_type = EFFECT_CLEAR;
		c->plr->frozen_ticks = 0;
	}
	if (c->plr->last_combat_time > 0)
	{
		c->plr->last_combat_time--;
	}
	if (c->plr->last_attack > 0)
	{
		c->plr->last_attack--;
	}
	if (c->plr->last_action > 0)
	{
		c->plr->last_action--;
	}
	if (c->plr->is_casting_ticks > 0)
	{
		c->plr->is_casting_ticks--;
	}
	if (c->plr->projectile_delay > 0)
	{
		c->plr->projectile_delay--;
	}
	else if (c->plr->projectile_delay == 0)
	{
		/* FIXME: Add pvp projectile creation later.  */
		if (c->plr->targeted_npc != -1)
		{
			/*
			int distance = get_distance_from_point(c->plr->world_x,
						   c->plr->world_y,
						   NPC_SPAWNS[c->plr->targeted_npc]->world_x,
						   NPC_SPAWNS[c->plr->targeted_npc]->world_y);
			int size = NPCS[NPC_SPAWNS[c->plr->targeted_npc]->npc_id]->size;
			*/
			
			/* This will create the proper projectile for the weapon used.  */
			//printf("shoot npc!\n");
			//player_shoot_npc(c, distance, size);
		}

		c->plr->projectile_delay = -1;
	}
	if (c->plr->hit_gfx_delay > 0)
	{
		c->plr->hit_gfx_delay--;
	}
	else if (c->plr->hit_gfx_delay == 0)
	{
		c->plr->gfx = c->plr->hit_gfx;
		c->plr->gfx_height = (65536 * c->plr->hit_gfx_height);
		c->plr->hit_gfx_delay = -1;
		c->plr->update_masks[GFX_REQ] = 1;
		c->plr->update_required = 1;
	}
	if (c->plr->poison_timer > 0)
	{
		c->plr->poison_timer--;
	}
	if (c->plr->poison_immunity_timer > 0)
	{
		c->plr->poison_immunity_timer--;

		if (c->plr->poison_immunity_timer == 0)
		{
			packet_send_chatbox_message(c, "Your poison immunity wears off.");
		}
	}
	if (c->plr->retaliation_delay > 0)
	{
		c->plr->retaliation_delay--;
	}
	else if (c->plr->retaliation_delay == 0)
	{
		if (c->plr->retaliation_is_npc == 1)
		{
			set_player_combat_target(c, c->plr->retaliation_index, -1);
		}
		else
		{
			set_player_combat_target(c, -1, c->plr->retaliation_index);
		}
		c->plr->retaliation_delay = -1;
		c->plr->retaliation_index = -1;
	}
	/* Skill related.  */
	if (c->plr->action_ticks > 0)
	{
		c->plr->action_ticks--;
	}
	if (c->plr->is_casting_ticks > 0)
	{
		c->plr->is_casting_ticks--;
	}
	/* Misc.  */
	if (c->plr->health_regen > 0)
	{
		if (c->plr->prayers_active[PRAYER_RAPID_HEAL] == 1)
		{
			c->plr->health_regen -= 2;
		}
		else
		{
			c->plr->health_regen--;
		}
	}
	if (c->plr->stat_regen > 0)
	{
		if (c->plr->prayers_active[PRAYER_RAPID_RESTORE] == 1)
		{
			c->plr->stat_regen -= 2;
		}
		else
		{
			c->plr->stat_regen--;
		}
	}
	if (c->plr->stat_decay > 0)
	{
		c->plr->stat_decay--;
	}
	if (c->plr->teleport_timer > 0)
	{
		c->plr->teleport_timer--;
	}
	/* Saves player data occasionally.  */
	if (c->plr->save_data_ticks > 0)
	{
		c->plr->save_data_ticks--;
	}
	/* Special animation handling.  */
	if (c->plr->animation_stored_ticks > 0)
	{
		c->plr->animation_stored_ticks--;
		//player_play_animation(c, c->plr->animation_stored_emote);
	}
	/* Handle the animation processing.  */
	else if (c->plr->animation_stored_ticks == 0)
	{
		switch (c->plr->animation_play_type)
		{
			case ANIMATION_REPEATS:
				player_play_animation_repeats(c, c->plr->animation_stored_emote, 
									  c->plr->animation_stored_ticks_start);
				break;

			case ANIMATION_NO_OVERWRITE:
			/** Falls though.  */
			default:
				player_replay_animation(c);
				break;
		}
	}
}

/**
 * Processes timed events on a player.
 * @param *c Client.
 */
void player_process_timed_actions(struct client *c)
{
	/* Timed item add handler.  */
	if (c->plr->item_add_timer > 0)
	{
		c->plr->item_add_timer--;
	}
	/* TODO: Move this to its own function.  */
	else if (c->plr->item_add_timer == 0)
	{
		char msg[100];
		int rv = add_item_to_inv(c, c->plr->item_add_id, c->plr->item_add_num, 
						c->plr->item_add_charges);

		/* Only if the action succeeded, print the acquire message.  */
		if (rv == 1)
		{
			if (c->plr->item_add_message == 1)
			{
				snprintf(msg, 100, "You get some %s.", 
						 ITEMS[c->plr->item_add_id]->name);	
				packet_send_chatbox_message(c, msg);
				c->plr->item_add_message = -1;
			}

			/* If shearing a sheep, transform the sheep to the sheared 
			 * varient.  */
			if (c->plr->item_add_type == SHEEP_SHEARING)
			{
				if (c->plr->item_add_idx != -1)
				{
					/* Sheep sheared version is one id less than the normal
					 * one.  */
					transform_npc(NPC_SPAWNS[c->plr->item_add_idx], 
								  NPC_SPAWNS[c->plr->item_add_idx]->npc_id - 1,
								  15);	
				}
			}
		}

		c->plr->is_pursuing_target = 0;
		player_stop_facing_npc(c);
		printf("set last: %d\n", c->plr->animation_request_last);
		player_play_animation(c, c->plr->animation_request_last);
		printf("post\n");

		c->plr->item_add_timer = -1;
		c->plr->item_add_num = -1;
		c->plr->item_add_id = -1;
		c->plr->item_add_charges = -1;
		c->plr->item_add_type = -1;
		c->plr->item_add_idx = -1;
	}
	/* Timed action handling.  */
	if (c->plr->timed_action_ticks > 0)
	{
		c->plr->timed_action_ticks--;
	}
	/* Handle each timed action.  */
	else if (c->plr->timed_action_ticks == 0)
	{
		switch (c->plr->timed_action_type)
		{
			case TA_FILL_WITH_WATER:
				if (fill_water_containers(c, &c->plr->timed_action_store) == 0)
				{
					reset_timed_action(c);
					player_replay_animation(c);
				}
				else
				{
					packet_send_chatbox_message(c, "You fill the container with water.");
					c->plr->timed_action_ticks 
						= c->plr->timed_action_ticks_start;
					player_play_animation_no_overwrite(c, 832, 3);
				}
				break;

			default:
				printf("Timed action, unhandled type set.\n");
				printf("Type: %d Ticks: %d\n", c->plr->timed_action_type, 
					   c->plr->timed_action_ticks_start);
				reset_timed_action(c);
				break;
		}
	}
}
