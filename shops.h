/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SHOPS_H_
#define _SHOPS_H_

/**
 * Struct containing all information for a shop.
 */
struct shop_info
{
	/** Shop ID.  */
	int id;
	/** Name of the shop.  */
	char name[200];
	/** If buying is enabled for this shop. May not be needed.  */
	int can_buy;
	/** If buying is enabled for this shop. May not be needed.  */
	int can_sell;
	/** If store is a specialty shop, do not allow it to buy items from players
	 * that aren't in the shop's item list.  */
	int is_specialty;
	/** No used, will be replaced with something to track what stores give a
	 * higher than average price when selling items.  */
	float max_sell_value;
	/** Item IDs that the shop sells.  */
	int items[SHOP_SIZE];
	/** Count of items that the shop sells.  */
	int items_n[SHOP_SIZE];

	/** Track the original items the shop sold.  */
	int orig_items[SHOP_SIZE];
	/** Track the original stock of each item in the shop.  */
	int orig_items_n[SHOP_SIZE];
};

int get_shop_index(int shop_id);
int get_shop_selling_price(int shop_idx, int item_id, int stock_override);
int get_shop_buying_price(int shop_idx, int item_id, int stock_override);
int buy_item_from_shop(struct client *c, int shop_idx, int item_id, int amount);
int sell_item_to_shop(struct client *c, int shop_idx, int item_id);

#endif
