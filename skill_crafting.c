/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
/* CRAFTING - ID 12.  */

/* Returns level required.  */
int get_crafting_level_needed(int crafting_id)
{
	switch (crafting_id)
	{
	/* Weaving.  */
		case CRAFTING_BALL_OF_WOOL:
			return 1;
		case CRAFTING_BOW_STRING:
			return 10;
		case CRAFTING_MAGIC_STRING:
			return 19;
		case CRAFTING_CLOTH_STRIP:
			return 10;
		case CRAFTING_EMPTY_SACK:
			return 21;
		case CRAFTING_BASKET:
			return 36;
	/* Pottery.  */
		case CRAFTING_SHAPING_POT:
		case CRAFTING_FIRING_POT:
			return 1;
		case CRAFTING_SHAPING_PIE_DISH:
		case CRAFTING_FIRING_PIE_DISH:
			return 7;
		case CRAFTING_SHAPING_BOWL:
		case CRAFTING_FIRING_BOWL:
			return 8;
		case CRAFTING_SHAPING_PLANT_POT:
		case CRAFTING_FIRING_PLANT_POT:
			return 19;
		case CRAFTING_SHAPING_POT_LID:
		case CRAFTING_FIRING_POT_LID:
			return 25;
	/* Tanning leather.  */
		case CRAFTING_TAN_SOFT_LEATHER:
		case CRAFTING_TAN_HARD_LEATHER:
		case CRAFTING_TAN_SNAKESKIN:
		case CRAFTING_TAN_GREEN_DHIDE:
		case CRAFTING_TAN_BLUE_DHIDE:
		case CRAFTING_TAN_RED_DHIDE:
		case CRAFTING_TAN_BLACK_DHIDE:
			return 1;
	/* Leather crafts.  */
		case CRAFTING_LEATHER_BODY:
			return 14;
		case CRAFTING_LEATHER_GLOVES:
			return 1;
		case CRAFTING_LEATHER_BOOTS:
			return 7;
		case CRAFTING_LEATHER_VAMBRACES:
			return 11;
		case CRAFTING_LEATHER_CHAPS:
			return 18;
		case CRAFTING_HARD_LEATHER_BODY:
			return 28;
		case CRAFTING_LEATHER_COIF:
			return 38;
		case CRAFTING_LEATHER_COWL:
			return 9;
		case CRAFTING_ADD_STUDS_LEATHER_BODY:
			return 41;
		case CRAFTING_ADD_STUDS_LEATHER_CHAPS:
			return 44;
		case CRAFTING_GREEN_DLEATHER_BODY:
			return 63;
		case CRAFTING_GREEN_DLEATHER_VAMBRACES:
			return 57;
		case CRAFTING_GREEN_DLEATHER_CHAPS:
			return 60;
		case CRAFTING_BLUE_DLEATHER_BODY:
			return 71;
		case CRAFTING_BLUE_DLEATHER_VAMBRACES:
			return 66;
		case CRAFTING_BLUE_DLEATHER_CHAPS:
			return 68;
		case CRAFTING_RED_DLEATHER_BODY:
			return 77;
		case CRAFTING_RED_DLEATHER_VAMBRACES:
			return 73;
		case CRAFTING_RED_DLEATHER_CHAPS:
			return 75;
		case CRAFTING_BLACK_DLEATHER_BODY:
			return 84;
		case CRAFTING_BLACK_DLEATHER_VAMBRACES:
			return 82;
		case CRAFTING_BLACK_DLEATHER_CHAPS:
			return 79;
		case CRAFTING_SNAKESKIN_BODY:
			return 45;
		case CRAFTING_SNAKESKIN_CHAPS:
			return 51;
		case CRAFTING_SNAKESKIN_VAMBRACES:
			return 47;
		case CRAFTING_SNAKESKIN_BANDANA:
			return 48;
		case CRAFTING_SNAKESKIN_BOOTS:
			return 45;
		case CRAFTING_SNELM:
			return 15;
	/* Glassblowing.  */
		case CRAFTING_MOLTEN_GLASS:
			return 1;
		case CRAFTING_VIAL:
			return 33;
		case CRAFTING_UNPOWERED_ORB:
			return 46;
		case CRAFTING_BEER_GLASS:
			return 1;
		case CRAFTING_CANDLE_LANTERN:
			return 4;
		case CRAFTING_OIL_LAMP:
			return 12;
		case CRAFTING_LANTERN_LENS:
			return 49;
	/* Cutting gems.  */
		case CRAFTING_CUT_OPAL:
			return 1;
		case CRAFTING_CUT_JADE:
			return 13;
		case CRAFTING_CUT_RED_TOPAZ:
			return 16;
		case CRAFTING_CUT_SAPPHIRE:
			return 20;
		case CRAFTING_CUT_EMERALD:
			return 27;
		case CRAFTING_CUT_RUBY:
			return 34;
		case CRAFTING_CUT_DIAMOND:
			return 43;
		case CRAFTING_CUT_DRAGONSTONE:
			return 55;
	/* Rings.  */
		case CRAFTING_GOLD_RING:
			return 5;
		case CRAFTING_SAPPHIRE_RING:
			return 20;
		case CRAFTING_EMERALD_RING:
			return 27;
		case CRAFTING_RUBY_RING:
			return 34;
		case CRAFTING_DIAMOND_RING:
			return 43;
		case CRAFTING_DRAGONSTONE_RING:
			return 55;
	/* Necklaces.  */
		case CRAFTING_GOLD_NECKLACE:
			return 6;
		case CRAFTING_SAPPHIRE_NECKLACE:
			return 22;
		case CRAFTING_EMERALD_NECKLACE:
			return 29;
		case CRAFTING_RUBY_NECKLACE:
			return 40;
		case CRAFTING_DIAMOND_NECKLACE:
			return 56;
		case CRAFTING_DRAGONSTONE_NECKLACE:
			return 72;
	/* Amulets.  */
		case CRAFTING_GOLD_AMULET:
			return 8;
		case CRAFTING_SAPPHIRE_AMULET:
			return 24;
		case CRAFTING_EMERALD_AMULET:
			return 31;
		case CRAFTING_RUBY_AMULET:
			return 50;
		case CRAFTING_DIAMOND_AMULET:
			return 70;
		case CRAFTING_DRAGONSTONE_AMULET:
			return 80;
	/* Battlestaffs.  */
		case CRAFTING_WATER_BATTLESTAFF:
			return 54;
		case CRAFTING_EARTH_BATTLESTAFF:
			return 58;
		case CRAFTING_FIRE_BATTLESTAFF:
			return 62;
		case CRAFTING_AIR_BATTLESTAFF:
			return 66;
	/* Silver.  */
		case CRAFTING_UNSTRUNG_SYMBOL:
			return 16;
		case CRAFTING_UNSTRUNG_EMBLEM:
			return 17;
		case CRAFTING_TIARA:
			return 23;
		case CRAFTING_SICKLE:
			return 18;
	
		
		default:
			printf("ID: %d not handled! Crafting Level unknown!\n", crafting_id);
			return -1;
	}
}
/* Returns xp.  */
float get_crafting_xp(int crafting_id)
{
	switch (crafting_id)
	{
	/* Weaving.  */
		case CRAFTING_BALL_OF_WOOL:
			return 2.5;
		case CRAFTING_BOW_STRING:
			return 15.0;
		case CRAFTING_MAGIC_STRING:
			return 30.0;
		case CRAFTING_CLOTH_STRIP:
			return 12.0;
		case CRAFTING_EMPTY_SACK:
			return 38.0;
		case CRAFTING_BASKET:
			return 56.0;
	/* Pottery.  */
		case CRAFTING_SHAPING_POT:
		case CRAFTING_FIRING_POT:
			return 6.3;
		case CRAFTING_SHAPING_PIE_DISH:
			return 15.0;
		case CRAFTING_FIRING_PIE_DISH:
			return 10.0;
		case CRAFTING_SHAPING_BOWL:
			return 18.0;
		case CRAFTING_FIRING_BOWL:
			return 15.0;
		case CRAFTING_SHAPING_PLANT_POT:
			return 20.0;
		case CRAFTING_FIRING_PLANT_POT:
			return 17.5;
		case CRAFTING_SHAPING_POT_LID:
			return 20.0;
		case CRAFTING_FIRING_POT_LID:
			return 20.0;
	/* Tanning leather.  */
		case CRAFTING_TAN_SOFT_LEATHER:
		case CRAFTING_TAN_HARD_LEATHER:
		case CRAFTING_TAN_SNAKESKIN:
		case CRAFTING_TAN_GREEN_DHIDE:
		case CRAFTING_TAN_BLUE_DHIDE:
		case CRAFTING_TAN_RED_DHIDE:
		case CRAFTING_TAN_BLACK_DHIDE:
			return 0.0;
	/* Leather crafts.  */
		case CRAFTING_LEATHER_BODY:
			return 25.0;
		case CRAFTING_LEATHER_GLOVES:
			return 13.8;
		case CRAFTING_LEATHER_BOOTS:
			return 16.25;
		case CRAFTING_LEATHER_VAMBRACES:
			return 22.0;
		case CRAFTING_LEATHER_CHAPS:
			return 27.0;
		case CRAFTING_HARD_LEATHER_BODY:
			return 35.0;
		case CRAFTING_LEATHER_COIF:
			return 37.0;
		case CRAFTING_LEATHER_COWL:
			return 18.5;
		case CRAFTING_ADD_STUDS_LEATHER_BODY:
			return 40.0;
		case CRAFTING_ADD_STUDS_LEATHER_CHAPS:
			return 42.0;
		case CRAFTING_GREEN_DLEATHER_BODY:
			return 186.0;
		case CRAFTING_GREEN_DLEATHER_VAMBRACES:
			return 62.0;
		case CRAFTING_GREEN_DLEATHER_CHAPS:
			return 124.0;
		case CRAFTING_BLUE_DLEATHER_BODY:
			return 210.0;
		case CRAFTING_BLUE_DLEATHER_VAMBRACES:
			return 70.0;
		case CRAFTING_BLUE_DLEATHER_CHAPS:
			return 140.0;
		case CRAFTING_RED_DLEATHER_BODY:
			return 234.0;
		case CRAFTING_RED_DLEATHER_VAMBRACES:
			return 78.0;
		case CRAFTING_RED_DLEATHER_CHAPS:
			return 156.0;
		case CRAFTING_BLACK_DLEATHER_BODY:
			return 258.0;
		case CRAFTING_BLACK_DLEATHER_VAMBRACES:
			return 86.0;
		case CRAFTING_BLACK_DLEATHER_CHAPS:
			return 172.0;
		case CRAFTING_SNAKESKIN_BODY:
			return 55.0;
		case CRAFTING_SNAKESKIN_CHAPS:
			return 50.0;
		case CRAFTING_SNAKESKIN_VAMBRACES:
			return 35.0;
		case CRAFTING_SNAKESKIN_BANDANA:
			return 45.0;
		case CRAFTING_SNAKESKIN_BOOTS:
			return 30.0;
		case CRAFTING_SNELM:
			return 32.5;
	/* Glassblowing.  */
		case CRAFTING_MOLTEN_GLASS:
			return 20.0;
		case CRAFTING_VIAL:
			return 35.0;
		case CRAFTING_UNPOWERED_ORB:
			return 52.5;
		case CRAFTING_BEER_GLASS:
			return 17.5;
		case CRAFTING_CANDLE_LANTERN:
			return 19.0;
		case CRAFTING_OIL_LAMP:
			return 25.0;
		case CRAFTING_LANTERN_LENS:
			return 55.0;
	/* Cutting gems.  */
		case CRAFTING_CUT_OPAL:
			return 15.0;
		case CRAFTING_CUT_JADE:
			return 20.0;
		case CRAFTING_CUT_RED_TOPAZ:
			return 25.0;
		case CRAFTING_CUT_SAPPHIRE:
			return 50.0;
		case CRAFTING_CUT_EMERALD:
			return 67.5;
		case CRAFTING_CUT_RUBY:
			return 85.0;
		case CRAFTING_CUT_DIAMOND:
			return 107.5;
		case CRAFTING_CUT_DRAGONSTONE:
			return 137.5;
	/* Rings.  */
		case CRAFTING_GOLD_RING:
			return 15.0;
		case CRAFTING_SAPPHIRE_RING:
			return 40.0;
		case CRAFTING_EMERALD_RING:
			return 55.0;
		case CRAFTING_RUBY_RING:
			return 70.0;
		case CRAFTING_DIAMOND_RING:
			return 85.0;
		case CRAFTING_DRAGONSTONE_RING:
			return 100.0;
	/* Necklaces.  */
		case CRAFTING_GOLD_NECKLACE:
			return 20.0;
		case CRAFTING_SAPPHIRE_NECKLACE:
			return 55.0;
		case CRAFTING_EMERALD_NECKLACE:
			return 60.0;
		case CRAFTING_RUBY_NECKLACE:
			return 75.0;
		case CRAFTING_DIAMOND_NECKLACE:
			return 90.0;
		case CRAFTING_DRAGONSTONE_NECKLACE:
			return 105.0;
	/* Amulets.  */
		case CRAFTING_GOLD_AMULET:
			return 30.0;
		case CRAFTING_SAPPHIRE_AMULET:
			return 65.0;
		case CRAFTING_EMERALD_AMULET:
			return 70.0;
		case CRAFTING_RUBY_AMULET:
			return 85.0;
		case CRAFTING_DIAMOND_AMULET:
			return 100.0;
		case CRAFTING_DRAGONSTONE_AMULET:
			return 150.0;
	/* Battlestaffs.  */
		case CRAFTING_WATER_BATTLESTAFF:
			return 100.0;
		case CRAFTING_EARTH_BATTLESTAFF:
			return 112.5;
		case CRAFTING_FIRE_BATTLESTAFF:
			return 125.0;
		case CRAFTING_AIR_BATTLESTAFF:
			return 137.5;
	/* Silver.  */
		case CRAFTING_UNSTRUNG_SYMBOL:
			return 50.0;
		case CRAFTING_UNSTRUNG_EMBLEM:
			return 50.0;
		case CRAFTING_TIARA:
			return 52.5;
		case CRAFTING_SICKLE:
			return 50.0;

		default:
			printf("ID: %d not handled! Crafting XP unknown!\n", crafting_id);
			return -1;
	}
}

int get_gem_cut_id(int crafting_id)
{
	switch (crafting_id)
	{
		/* Opal.  */
		case 1625:
			return 1609;
		/* Jade.  */
		case 1627:
			return 1611;
		/* Red topaz.  */
		case 1629:
			return 1613;
		/* Sapphire.  */
		case 1623:
			return 1607;
		/* Emerald.  */
		case 1621:
			return 1605;
		/* Ruby.  */
		case 1619:
			return 1603;
		/* Diamond.  */
		case 1617:
			return 1601;
		/* Dragonstone.  */
		case 1631:
			return 1615;
		default:
			printf("Gem id: %d not found!\n", crafting_id);
			return 0;	
	}
}

/**
 * Returns the chance to cut a gem successfully, used in rand from 0.0 to 1.0.
 * If the rand result is higher than this, gem is broken.
 * @param gem_id Id of the gem being tested.
 * @param crafting_level Crafting level of the person attempting the cut.
 * @return Change to break the gem.
 */
double get_gem_cut_chance(int gem_id, int crafting_level)
{
	switch (gem_id)
	{
		/* Opal.  */
		case 1625:
			return ((0.005 * crafting_level) + 0.5);
		/* Jade.  */
		case 1627:
			return ((0.00485 * crafting_level) + 0.5);
		/* Red topaz.  */
		case 1629:
			return ((0.00455 * crafting_level) + 0.5);
		default:
			printf("Gem id: %d not found!\n", gem_id);
			return 0.0;
	}
}

int get_gem_cut_emote(int crafting_id)
{
	switch (crafting_id)
	{
		/* Opal.  */
		case 1625:
			return 890;
		/* Jade.  */
		case 1627:
			return 891;
		/* Red topaz.  */
		case 1629:
			return 892;
		/* Sapphire.  */
		case 1623:
			return 888;
		/* Emerald.  */
		case 1621:
			return 889;
		/* Ruby.  */
		case 1619:
			return 887;
		/* Diamond.  */
		case 1617:
			return 886;
		/* Dragonstone.  */
		case 1631:
			return 885;
		default:
			printf("Gem id: %d emote not found!\n", crafting_id);
			return 0;	
	}
}


int get_leather_tanning_cost(int crafting_id)
{
	switch (crafting_id)
	{
		case CRAFTING_TAN_SOFT_LEATHER:
			return 1;
		case CRAFTING_TAN_HARD_LEATHER:
			return 3;
		case CRAFTING_TAN_SNAKESKIN:
			return 15;
		/* Snakeskin (swamp), not in 333.
		case 33:
			return 20;
		*/
		case CRAFTING_TAN_GREEN_DHIDE:
		case CRAFTING_TAN_BLUE_DHIDE:
		case CRAFTING_TAN_RED_DHIDE:
		case CRAFTING_TAN_BLACK_DHIDE:
			return 20;
		default:
			printf("Leather ID: %d not found! No price!\n", crafting_id);
			return 0;
	}
}

int get_leather_tanned_id(int crafting_id)
{
	switch (crafting_id)
	{
		case CRAFTING_TAN_SOFT_LEATHER:
			return 1741;
		case CRAFTING_TAN_HARD_LEATHER:
			return 1743;
		case CRAFTING_TAN_SNAKESKIN:
			return 6289;
		/* Snakesking (swamp), not in 333.
		case 33:
			return 0;
		*/
		case CRAFTING_TAN_GREEN_DHIDE:
			return 1745;
		case CRAFTING_TAN_BLUE_DHIDE:
			return 2505;
		case CRAFTING_TAN_RED_DHIDE:
			return 2507;
		case CRAFTING_TAN_BLACK_DHIDE:
			return 2509;
		default:
			printf("Leather ID: %d not found! No price!\n", crafting_id);
			return 0;
	}
}

int get_crafting_strung_id(int crafting_id)
{
	switch (crafting_id)
	{
		case 1714:
			return 1716;
		case 1720:
			return 1722;
		case 1673:
			return 1692;
		case 1675:
			return 1694;
		case 1677:
			return 1696;
		case 1679:
			return 1698;
		case 1681:
			return 1700;
		case 1683:
			return 1702;
		default:
			printf("Strung ID: %d not found!\n", crafting_id);
			return -1;	
	}
}

int get_crafting_furnace_used(int id)
{
	switch (id)
	{
		case CRAFTING_FIRING_POT:
		case CRAFTING_FIRING_PIE_DISH:
		case CRAFTING_FIRING_BOWL:
		case CRAFTING_FIRING_PLANT_POT:
		case CRAFTING_FIRING_POT_LID:
		case CRAFTING_MOLTEN_GLASS:
		case CRAFTING_GOLD_RING:
		case CRAFTING_SAPPHIRE_RING:
		case CRAFTING_EMERALD_RING:
		case CRAFTING_RUBY_RING:
		case CRAFTING_DIAMOND_RING:
		case CRAFTING_DRAGONSTONE_RING:
		case CRAFTING_GOLD_NECKLACE:
		case CRAFTING_SAPPHIRE_NECKLACE:
		case CRAFTING_EMERALD_NECKLACE:
		case CRAFTING_RUBY_NECKLACE:
		case CRAFTING_DIAMOND_NECKLACE:
		case CRAFTING_DRAGONSTONE_NECKLACE:
		case CRAFTING_GOLD_AMULET:
		case CRAFTING_SAPPHIRE_AMULET:
		case CRAFTING_EMERALD_AMULET:
		case CRAFTING_RUBY_AMULET:
		case CRAFTING_DIAMOND_AMULET:
		case CRAFTING_DRAGONSTONE_AMULET:
		case CRAFTING_UNSTRUNG_SYMBOL:
		case CRAFTING_UNSTRUNG_EMBLEM:
		case CRAFTING_TIARA:
		case CRAFTING_SICKLE:
			return 1;

		default:
			return 0;
	}
}


/**
 * Change uncooked items to their cooked varient, and add xp.
 * @param *c Client instance.
 * @param xp_gain XP received from the task.
 * @return 1 on valid choice, 0 on invalid.
 */
int get_crafting_furnace_item(struct client *c, float xp_gain)
{
	if (c->plr->action_skill_waiting_for_resource == 1)
	{
		switch (c->plr->action_id_type)
		{
			case CRAFTING_FIRING_POT:
				packet_send_chatbox_message(c, "You remove a Pot from the oven.");
				transform_item(c, c->plr->action_item_slot, 1931);
				break;
			case CRAFTING_FIRING_PIE_DISH:
				packet_send_chatbox_message(c, "You remove a Pie dish from the oven.");
				transform_item(c, c->plr->action_item_slot, 2313);
				break;
			case CRAFTING_FIRING_BOWL:
				packet_send_chatbox_message(c, "You remove a Bowl from the oven.");
				transform_item(c, c->plr->action_item_slot, 1923);
				break;
			case CRAFTING_FIRING_PLANT_POT:
				packet_send_chatbox_message(c, "You remove a Plant pot from the oven.");
				transform_item(c, c->plr->action_item_slot, 5350);
				break;
			case CRAFTING_FIRING_POT_LID:
				packet_send_chatbox_message(c, "You remove a Pot lid from the oven.");
				transform_item(c, c->plr->action_item_slot, 4440);
				break;
			case CRAFTING_MOLTEN_GLASS:
				packet_send_chatbox_message(c, "You remove the molten glass "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1775, 1, -1);
				transform_item(c, c->plr->action_item_slot_2, 1925);
				break;
			case CRAFTING_GOLD_RING:
				packet_send_chatbox_message(c, "You remove the gold ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1635, 1, -1);
				break;
			case CRAFTING_SAPPHIRE_RING:
				packet_send_chatbox_message(c, "You remove the sapphire ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1637, 1, -1);
				break;
			case CRAFTING_EMERALD_RING:
				packet_send_chatbox_message(c, "You remove the emerald ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1639, 1, -1);
				break;
			case CRAFTING_RUBY_RING:
				packet_send_chatbox_message(c, "You remove the ruby ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1641, 1, -1);
				break;
			case CRAFTING_DIAMOND_RING:
				packet_send_chatbox_message(c, "You remove the diamond ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1643, 1, -1);
				break;
			case CRAFTING_DRAGONSTONE_RING:
				packet_send_chatbox_message(c, "You remove the dragonstone ring "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1645, 1, -1);
				break;
			case CRAFTING_GOLD_NECKLACE:
				packet_send_chatbox_message(c, "You remove the gold necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1654, 1, -1);
				break;
			case CRAFTING_SAPPHIRE_NECKLACE:
				packet_send_chatbox_message(c, "You remove the sapphire necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1656, 1, -1);
				break;
			case CRAFTING_EMERALD_NECKLACE:
				packet_send_chatbox_message(c, "You remove the emerald necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1658, 1, -1);
				break;
			case CRAFTING_RUBY_NECKLACE:
				packet_send_chatbox_message(c, "You remove the ruby necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1660, 1, -1);
				break;
			case CRAFTING_DIAMOND_NECKLACE:
				packet_send_chatbox_message(c, "You remove the diamond necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1662, 1, -1);
				break;
			case CRAFTING_DRAGONSTONE_NECKLACE:
				packet_send_chatbox_message(c, "You remove the dragonstone necklace "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1664, 1, -1);
				break;
			case CRAFTING_GOLD_AMULET:
				packet_send_chatbox_message(c, "You remove the gold amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1673, 1, -1);
				break;
			case CRAFTING_SAPPHIRE_AMULET:
				packet_send_chatbox_message(c, "You remove the sapphire amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1675, 1, -1);
				break;
			case CRAFTING_EMERALD_AMULET:
				packet_send_chatbox_message(c, "You remove the emerald amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1677, 1, -1);
				break;
			case CRAFTING_RUBY_AMULET:
				packet_send_chatbox_message(c, "You remove the ruby amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1679, 1, -1);
				break;
			case CRAFTING_DIAMOND_AMULET:
				packet_send_chatbox_message(c, "You remove the diamond amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1681, 1, -1);
				break;
			case CRAFTING_DRAGONSTONE_AMULET:
				packet_send_chatbox_message(c, "You remove the dragonstone amulet "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				delete_item_from_inv_slot(c, c->plr->action_item_slot_2, 1);
				add_item_to_inv(c, 1683, 1, -1);
				break;
			case CRAFTING_UNSTRUNG_SYMBOL:
				packet_send_chatbox_message(c, "You remove the unstrung symbol "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1714, 1, -1);
				break;
			case CRAFTING_UNSTRUNG_EMBLEM:
				packet_send_chatbox_message(c, "You remove the unstrung emblem "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 1720, 1, -1);
				break;
			case CRAFTING_TIARA:
				packet_send_chatbox_message(c, "You remove the tiara "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 5525, 1, -1);
				break;
			case CRAFTING_SICKLE:
				packet_send_chatbox_message(c, "You remove the sickle "
								"from the furnace.");
				delete_item_from_inv_slot(c, c->plr->action_item_slot, 1);
				add_item_to_inv(c, 2961, 1, -1);
				break;
		}
		add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
		c->plr->action_ticks = 2;
		c->plr->action_total_time_acted++;
		c->plr->action_skill_waiting_for_resource = -1;
		return 1;
	}
	return 0;
}

/**
 * Process tanning of leather, removal of coin, etc.
 * @param *c Client instance.
 * @param leather_id ID of the leather being tanned.
 */
void tan_leather(struct client *c, int leather_id)
{
	int to_tan = 0, have = 0, gold_needed = 0, x = 0, store = 0;
	char msg[150];

	/* Get untanned leather id.  */
	switch (leather_id)
	{
		case CRAFTING_TAN_SOFT_LEATHER:
		case CRAFTING_TAN_HARD_LEATHER:
			to_tan = 1739;
			break;
		case CRAFTING_TAN_SNAKESKIN:
			to_tan = 6287;
			break;
		case CRAFTING_TAN_GREEN_DHIDE:
			to_tan = 1753;
			break;
		case CRAFTING_TAN_BLUE_DHIDE:
			to_tan = 1751;
			break;
		case CRAFTING_TAN_RED_DHIDE:
			to_tan = 1749;
			break;
		case CRAFTING_TAN_BLACK_DHIDE:
			to_tan = 1747;
			break;
		default:
			printf("Unknown leather id %d!\n", leather_id);
			return;
	}
	
	have = player_has_num_items_non_stack(c, to_tan);

	if (have == 0)
	{
		packet_send_chatbox_message(c, "You don't have anything to tan!");
		return;
	}

	/* Make sure we don't tan more than the player has.  */
	if (have < c->plr->action_skill_remaining_actions)
	{
		c->plr->action_skill_remaining_actions = have;
	}

	/* Check the cost for tanning, if the player has enough.  */
	gold_needed = (c->plr->action_skill_remaining_actions 
				   * get_leather_tanning_cost(leather_id));
	
	if (player_has_item(c, 995, gold_needed) == -1)
	{
		packet_send_chatbox_message(c, "You don't have enough gold!");
		return;
	}

	/* Remove the gold, then loop and replace all the leather with 
	   tanned varients.  */
	delete_item_from_inv(c, 995, gold_needed);

	for (x = 0; x < c->plr->action_skill_remaining_actions; x++)
	{
		store = player_has_item(c, to_tan, 1);

		if (store != -1)
		{
			transform_item_no_refresh(c, store, 
									  get_leather_tanned_id(leather_id));
		}
		else
		{
			printf("Error! tan_leather for loop, %d!\n", x);
			packet_reload_items_in_inventory_interface(c);
			return;
		}
		packet_reload_items_in_inventory_interface(c);
	}

	switch (to_tan)
	{
		case 1739:
			snprintf(msg, 150, "The tanner tans %d cowhide for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
		case 6287:
			snprintf(msg, 150, "The tanner tans %d snakeskin for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
		case 1753:
			snprintf(msg, 150, "The tanner tans %d green d'hide for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
		case 1751:
			snprintf(msg, 150, "The tanner tans %d blue d'hide for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
		case 1749:
			snprintf(msg, 150, "The tanner tans %d red d'hide for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
		case 1747:
			snprintf(msg, 150, "The tanner tans %d black d'hide for you.", 
					 c->plr->action_skill_remaining_actions);
			break;
	}
	packet_send_chatbox_message(c, msg);
}

/**
 * Handle shaping snail shells into snelms.
 * @param *c Client instance.
 * @param shell_id ID of the shell being shaped.
 * @param snelm_id ID of the helmet being made.
 * @param crafting_id ID representing the crafting task.
 */
void craft_snelm(struct client *c, int shell_id, int snelm_id, int crafting_id)
{
	int shell_slot = 0, chisel_slot = 0;
	int required_level = get_crafting_level_needed(crafting_id);
	int crafting_level = get_player_skill_level(c, CRAFTING_SKILL);

	/* Check if level requirements are met. If not, exit.  */
	if (crafting_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "crafting to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	shell_slot = player_has_item(c, shell_id, 1);
	chisel_slot = player_has_item(c, 1755, 1);

	if (shell_slot != -1 && chisel_slot != -1)
	{
		delete_item_from_inv_slot(c, shell_slot, 1);
		add_item_to_inv(c, snelm_id, 1, -1);
		add_skill_xp(c, CRAFTING_SKILL, get_crafting_xp(crafting_id)); 
		packet_send_chatbox_message(c, "You shape the shell into a helmet.");
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Handle adding powered orbs to battlestaves.
 * @param *c Client instance.
 * @param staff_id ID of the staff used.
 * @param staff_slot Where the staff is in the inventory.
 * @param orb_id ID of the orb used.
 * @param orb_slot Where the orb is in the inventory.
 * @param crafting_id ID representing the crafting task.
 */
void craft_battlestaff(struct client *c, int staff_id, int staff_slot, 
					   int orb_id, int orb_slot, int crafting_id)
{
	int required_level = get_crafting_level_needed(crafting_id);
	int crafting_level = get_player_skill_level(c, CRAFTING_SKILL);
	int finished_staff_id = 0;

	/* Check if level requirements are met. If not, exit.  */
	if (crafting_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "crafting to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	switch (crafting_id)
	{
		case CRAFTING_WATER_BATTLESTAFF:
			finished_staff_id = 1395;
			break;
		case CRAFTING_EARTH_BATTLESTAFF:
			finished_staff_id = 1399;
			break;
		case CRAFTING_FIRE_BATTLESTAFF:
			finished_staff_id = 1393;
			break;
		case CRAFTING_AIR_BATTLESTAFF:
			finished_staff_id = 1397;
			break;
		default:
			printf("Invalid staff crafting id!\n");
			set_player_state(c, STATE_IDLE);
			return;
	}


	/* Check if the slots match up with what we were told.  */	
	if ((staff_id == get_item_in_inventory_slot(c, staff_slot))
		&& orb_id == get_item_in_inventory_slot(c, orb_slot))
	{
		delete_item_from_inv_slot(c, orb_slot, 1);
		transform_item(c, staff_slot, finished_staff_id);
		add_skill_xp(c, CRAFTING_SKILL, get_crafting_xp(crafting_id)); 
		packet_send_chatbox_message(c, "You put the orb on the battlestaff.");
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}


/**
 * Process cutting of gems, handle chance to crush if the gem can be crushed.
 * @param *c Client instance.
 * @param gem_id ID of the gem being cut.
 * @param crafting_id ID representing the crafting task.
 */
void cut_gem(struct client *c, int gem_id, int crafting_id)
{
	int gem_slot = 0, chisel_slot = 0, crushable = 0;
	int cut_gem_id = get_gem_cut_id(gem_id);
	int required_level = get_crafting_level_needed(crafting_id);
	float crush_xp = 0.0;
	double rand = rand_double(0.0, 1.0);
	double crush_chance = get_gem_cut_chance(gem_id, 
										  c->plr->level[CRAFTING_SKILL]);
	int crafting_level = get_player_skill_level(c, CRAFTING_SKILL);
	char msg[50];
	
	if (gem_id == 1625 || gem_id == 1627 || gem_id == 1629)
	{
		/* Opal crush xp.  */
		if (gem_id == 1625)
		{
			crush_xp = 3.8;
		}
		/* Jade crush xp.  */
		if (gem_id == 1627)
		{
			crush_xp = 5.0;
		}
		/* Red topaz crush xp.  */
		if (gem_id == 1629)
		{
			crush_xp = 6.3;
		}
		crushable = 1;
	}

	/* Check if level requirements are met. If not, exit.  */
	if (crafting_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "crafting to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	gem_slot = player_has_item(c, gem_id, 1);
	chisel_slot = player_has_item(c, 1755, 1);

	if (gem_slot != -1 && chisel_slot != -1)
	{
		player_play_animation(c, get_gem_cut_emote(gem_id));
		delete_item_from_inv_slot(c, gem_slot, 1);

		if (crushable == 1)
		{
			if (rand < crush_chance)
			{
				add_item_to_inv(c, cut_gem_id, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, get_crafting_xp(crafting_id)); 
				snprintf(msg, 50, "You cut the %s.", 
						 ITEMS[cut_gem_id + 1]->name);
			}
			else
			{
				add_item_to_inv(c, 1633, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, crush_xp); 
				snprintf(msg, 50, "You crush the %s!", 
						 ITEMS[cut_gem_id + 1]->name);
			}
		}
		else
		{
			add_item_to_inv(c, cut_gem_id, 1, -1);
			add_skill_xp(c, CRAFTING_SKILL, get_crafting_xp(crafting_id)); 
			snprintf(msg, 50, "You cut the %s.", ITEMS[cut_gem_id + 1]->name);
		}
		packet_send_chatbox_message(c, msg);
		c->plr->action_ticks = 2;
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Add a string to an amulet, necklace type item.
 * @param *c Client instance.
 * @param item_id Item the string is being added to.
 * @param item_slot Slot where the item is in the inventory.
 * @param string_id ID of the string being used.
 * @param string_slot Slot where the string is in the inventory.
 * @todo Need to add a less generic message when stringing items.
 * @todo Add support for nature amulets, they use special string I believe.
 * They may or may not be in this revision.
 */
void crafting_string_item(struct client *c, int item_id, int item_slot,
						  int string_id, int string_slot)
{
	int strung_id = get_crafting_strung_id(item_id);
	
	/* Check if the slots match up with what we were told.  */	
	if ((item_id == get_item_in_inventory_slot(c, item_slot))
		&& string_id == get_item_in_inventory_slot(c, string_slot))
	{
		delete_item_from_inv_slot(c, string_slot, 1);
		transform_item(c, item_slot, strung_id);
		add_skill_xp(c, CRAFTING_SKILL, 4.0); 
		/* TODO: Make this spit out lines specific to the item.  */
		packet_send_chatbox_message(c, "You add a string to the item.");
	}
	else
	{
		set_player_state(c, STATE_IDLE);
	}
}

/**
 * Handle putting of jewelry in the furnace, using gems and gold/silver bars.
 * @param *c Client instance.
 * @param crafting_id ID that represents the item being made.
 */
void put_jewelry_in_furnace(struct client *c, int crafting_id)
{
	/* Mould ID's needed. TODO: change these to enums later.  */
	int ring_mould = 1592, necklace_mould = 1597, amulet_mould = 1595;
	int holy_mould = 1599, unholy_mould = 1594, tiara_mould = 5523;
	int sickle_mould = 2976;
	int gem_id = 0, bar_id, no_gem = 0, has_mould = 0;
	
	/* Rings.  */
	if (crafting_id >= CRAFTING_GOLD_RING 
		&& crafting_id <= CRAFTING_DRAGONSTONE_RING)
	{
		has_mould = player_has_item(c, ring_mould, 1);
	}
	/* Necklaces.  */
	else if (crafting_id >= CRAFTING_GOLD_NECKLACE
			 && crafting_id <= CRAFTING_DRAGONSTONE_NECKLACE)
	{
		has_mould = player_has_item(c, necklace_mould, 1);
	}
	/* Amulets.  */
	else if (crafting_id >= CRAFTING_GOLD_AMULET
			 && crafting_id <= CRAFTING_DRAGONSTONE_AMULET)
	{
		has_mould = player_has_item(c, amulet_mould, 1);
	}
	/* Silver.  */
	else if (crafting_id >= CRAFTING_UNSTRUNG_SYMBOL 
			 && crafting_id <= CRAFTING_SICKLE)
	{
		switch (crafting_id)
		{
			case CRAFTING_UNSTRUNG_SYMBOL:
				has_mould = player_has_item(c, holy_mould, 1);
				break;	
			case CRAFTING_UNSTRUNG_EMBLEM:
				has_mould = player_has_item(c, unholy_mould, 1);
				break;	
			case CRAFTING_TIARA:
				has_mould = player_has_item(c, tiara_mould, 1);
				break;	
			case CRAFTING_SICKLE:
				has_mould = player_has_item(c, sickle_mould, 1);
				break;	
		}
	}

	switch (crafting_id)
	{
		case CRAFTING_GOLD_RING:
		case CRAFTING_GOLD_NECKLACE:
		case CRAFTING_GOLD_AMULET:
			no_gem = 1;
			bar_id = 2357;
			break;
		case CRAFTING_SAPPHIRE_RING:
		case CRAFTING_SAPPHIRE_NECKLACE:
		case CRAFTING_SAPPHIRE_AMULET:
			gem_id = 1607;
			bar_id = 2357;
			break;
		case CRAFTING_EMERALD_RING:
		case CRAFTING_EMERALD_NECKLACE:
		case CRAFTING_EMERALD_AMULET:
			gem_id = 1605;
			bar_id = 2357;
			break;
		case CRAFTING_RUBY_RING:
		case CRAFTING_RUBY_NECKLACE:
		case CRAFTING_RUBY_AMULET:
			gem_id = 1603;
			bar_id = 2357;
			break;
		case CRAFTING_DIAMOND_RING:
		case CRAFTING_DIAMOND_NECKLACE:
		case CRAFTING_DIAMOND_AMULET:
			gem_id = 1601;
			bar_id = 2357;
			break;
		case CRAFTING_DRAGONSTONE_RING:
		case CRAFTING_DRAGONSTONE_NECKLACE:
		case CRAFTING_DRAGONSTONE_AMULET:
			gem_id = 1615;
			bar_id = 2357;
			break;
		case CRAFTING_UNSTRUNG_SYMBOL:
		case CRAFTING_UNSTRUNG_EMBLEM:
		case CRAFTING_TIARA:
		case CRAFTING_SICKLE:
			bar_id = 2355;
			no_gem = 1;
			break;
		default:
			bar_id = -1;
			gem_id = -1;
			no_gem = -1;
			printf("Invalid crafting id: %d\n", crafting_id);
			set_player_state(c, STATE_IDLE);
			break;
	}

	c->plr->action_item_slot = player_has_item(c, bar_id, 1);	
	c->plr->action_item_slot_2 = player_has_item(c, gem_id, 1);	

	if (no_gem == 1)
	{
		if (c->plr->action_item_slot != -1 && has_mould != -1)
		{
			player_play_animation(c, 899);
			packet_send_chatbox_message(c, "You put the mould and materials in the furnace.");
			c->plr->action_ticks = 4;
			c->plr->action_skill_waiting_for_resource = 1;
		}
		else
		{
			set_player_state(c, STATE_IDLE);
		}
	}
	else	
	{
		if (c->plr->action_item_slot != -1 
			&& c->plr->action_item_slot_2 != -1 && has_mould != -1)
		{
			player_play_animation(c, 899);
			packet_send_chatbox_message(c, "You put the mould and materials in the furnace.");
			c->plr->action_ticks = 4;
			c->plr->action_skill_waiting_for_resource = 1;
		}
		else
		{
			set_player_state(c, STATE_IDLE);
		}
	}
}

/**
 * Make sure all requirements are met before starting the crafting process.
 * @param *c Client instance.
 */
void init_crafting(struct client *c)
{
	int required_level = get_crafting_level_needed(c->plr->action_id_type);
	int crafting_level = get_player_skill_level(c, CRAFTING_SKILL);

	/* Set action related ids.  */
	c->plr->action_skill_id = CRAFTING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_CRAFTING);

	if (crafting_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "crafting to make that!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* Sewing together armor, check for needle and thread.  */
	if (c->plr->action_id_type >= 40
		&& c->plr->action_id_type <= 100)
	{
		/* No needle.  */	
		if (player_has_item(c, 1733, 1) == -1)
		{
			packet_send_chatbox_message(c, "You don't have a needle!");
		}
		/* No thread.  */
		if (player_has_item(c, 1734, 1) == -1)
		{
			packet_send_chatbox_message(c, "You don't have any thread!");
		}
	}
	printf("Crafting setup. No errors.\n");
}

/**
 * Handle the crafting process for each item.
 * @param *c Client instance.
 */
void handle_crafting(struct client *c)
{
	int num_made = 0;
	int item_1_slot = 0, item_2_slot = 0;
	int x = 0;
	float xp_gain = get_crafting_xp(c->plr->action_id_type);

	/* This function handles giving xp and the finished product when the 
	   "getting the item back" part of the animation is done. If it returns
	   a 1 when called, return here, too.  */
	if (get_crafting_furnace_item(c, xp_gain) == 1)
	{
		return;
	}

	/* Check if there are actions left to do, if not, end skilling.  */
	if (c->plr->action_skill_remaining_actions == 0)
	{
		set_player_state(c, STATE_IDLE);
	}

	switch (c->plr->action_id_type)
	{
		case CRAFTING_BALL_OF_WOOL:
			item_1_slot = player_has_item(c, 1737, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 1759, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You spin the wool into a ball.");
				c->plr->action_ticks = 3;
			}
			else
			{
				packet_send_chatbox_message(c, "You don't have any wool to spin!");
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Spinning flax into a bow string.  */
		case CRAFTING_BOW_STRING:
			item_1_slot = player_has_item(c, 1779, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 1777, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You spin the flax into a bow string.");
				c->plr->action_ticks = 3;
			}
			else
			{
				packet_send_chatbox_message(c, "You don't have any flax to spin!");
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Spinning magic roots into a bow string.  */
		case CRAFTING_MAGIC_STRING:
			item_1_slot = player_has_item(c, 6051, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 6038, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You spin the magic roots into a magic string.");
				c->plr->action_ticks = 3;
			}
			else
			{
				packet_send_chatbox_message(c, "You don't have any roots to spin!");
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Weaving balls of wool into a strip of cloth.  */
		case CRAFTING_CLOTH_STRIP:
			item_1_slot = player_has_item_non_stack(c, 1759, 4);
			
			if (item_1_slot != -1)
			{
				for (x = 0; x < 4; x++)
				{
					delete_item_from_inv(c, 1759, 1);
				}
				add_item_to_inv(c, 3224, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You weave the wool into a strip of cloth.");
				c->plr->action_ticks = 3;
			}
			else
			{
				packet_send_chatbox_message(c, "You don't have any wool to spin!");
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Weaving jute fibers into a empty sack.  */
		case CRAFTING_EMPTY_SACK:
			item_1_slot = player_has_item_non_stack(c, 5931, 4);
			
			if (item_1_slot != -1)
			{
				for (x = 0; x < 4; x++)
				{
					delete_item_from_inv(c, 5931, 1);
				}
				add_item_to_inv(c, 5418, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You weave the jute fibers into a empty sack.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Weaving willow branches into a basket.  */
		case CRAFTING_BASKET:
			item_1_slot = player_has_item_non_stack(c, 5933, 6);
			
			if (item_1_slot != -1)
			{
				for (x = 0; x < 6; x++)
				{
					delete_item_from_inv(c, 5933, 1);
				}
				add_item_to_inv(c, 5376, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 894);
				packet_send_chatbox_message(c, "You weave the wool into a strip of cloth.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Shaping clay into a pot.  */
		case CRAFTING_SHAPING_POT:
			item_1_slot = player_has_item(c, 1761, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 1787, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 896);
				packet_send_chatbox_message(c, "You shape the soft clay into a pot.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Shaping clay into a pie dish.  */
		case CRAFTING_SHAPING_PIE_DISH:
			item_1_slot = player_has_item(c, 1761, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 1789, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 896);
				packet_send_chatbox_message(c, "You shape the soft clay into a pie dish.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Shaping clay into a bowl.  */
		case CRAFTING_SHAPING_BOWL:
			item_1_slot = player_has_item(c, 1761, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 1791, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 896);
				packet_send_chatbox_message(c, "You shape the soft clay into a bowl.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Shaping clay into a plant pot.  */
		case CRAFTING_SHAPING_PLANT_POT:
			item_1_slot = player_has_item(c, 1761, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 5352, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 896);
				packet_send_chatbox_message(c, "You shape the soft clay into a plant pot.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Shaping clay into a pot lid.  */
		case CRAFTING_SHAPING_POT_LID:
			item_1_slot = player_has_item(c, 1761, 1);
			
			if (item_1_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				add_item_to_inv(c, 4438, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 896);
				packet_send_chatbox_message(c, "You shape the soft clay into a pot lid.");
				c->plr->action_ticks = 3;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Firing a clay pot.  */
		case CRAFTING_FIRING_POT:
			c->plr->action_item_slot = player_has_item(c, 1787, 1);
			
			if (c->plr->action_item_slot != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put a Pot in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Firing a pie dish.  */
		case CRAFTING_FIRING_PIE_DISH:
			c->plr->action_item_slot = player_has_item(c, 1789, 1);
			
			if (c->plr->action_item_slot != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put a Pie dish in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Firing a bowl.  */
		case CRAFTING_FIRING_BOWL:
			c->plr->action_item_slot = player_has_item(c, 1791, 1);
			
			if (c->plr->action_item_slot != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put a Bowl in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Firing a Plant pot.  */
		case CRAFTING_FIRING_PLANT_POT:
			c->plr->action_item_slot = player_has_item(c, 5352, 1);
			
			if (c->plr->action_item_slot != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put a Plant pot in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Firing a Pot lid.  */
		case CRAFTING_FIRING_POT_LID:
			c->plr->action_item_slot = player_has_item(c, 4438, 1);
			
			if (c->plr->action_item_slot != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put a Pot lid in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
	/* Tanning leather.  */
		case CRAFTING_TAN_SOFT_LEATHER:
		case CRAFTING_TAN_HARD_LEATHER:
		case CRAFTING_TAN_SNAKESKIN:
		case CRAFTING_TAN_GREEN_DHIDE:
		case CRAFTING_TAN_BLUE_DHIDE:
		case CRAFTING_TAN_RED_DHIDE:
		case CRAFTING_TAN_BLACK_DHIDE:
			tan_leather(c, c->plr->action_id_type);
			set_player_state(c, STATE_IDLE);
			break;
		
		/* Leather body.  */
		case CRAFTING_LEATHER_BODY:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1129, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a leather body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather gloves.  */
		case CRAFTING_LEATHER_GLOVES:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1059, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of leather gloves.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather boots.  */
		case CRAFTING_LEATHER_BOOTS:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1061, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of leather boots.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather vambraces.  */
		case CRAFTING_LEATHER_VAMBRACES:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1063, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of leather vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather chaps.  */
		case CRAFTING_LEATHER_CHAPS:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1095, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of leather chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather coif.  */
		case CRAFTING_LEATHER_COIF:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1169, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a leather coif.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Leather cowl.  */
		case CRAFTING_LEATHER_COWL:
			item_1_slot = player_has_item(c, 1741, 1);
			item_2_slot = player_has_item(c, 1734, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv_slot(c, item_1_slot, 1);
				remove_inventory_item_charges(c, item_2_slot, 1);	
				add_item_to_inv(c, 1167, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a leather body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Adding studs to leather body.  */
		case CRAFTING_ADD_STUDS_LEATHER_BODY:
			num_made = combine_items(c, 1129, 1, 2370, 1, 1133, 1);
			
			if (num_made != -1)
			{
				packet_send_chatbox_message(c, "You add steel studs to the leather body.");
				add_skill_xp(c, CRAFTING_SKILL, xp_gain);
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Adding studs to leather chaps.  */
		case CRAFTING_ADD_STUDS_LEATHER_CHAPS:
			num_made = combine_items(c, 1095, 1, 2370, 1, 1097, 1);
			
			if (num_made != -1)
			{
				packet_send_chatbox_message(c, "You add steel studs to the leather chaps.");
				add_skill_xp(c, CRAFTING_SKILL, xp_gain);
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Hard leather body.  */
		case CRAFTING_HARD_LEATHER_BODY:
			x = player_has_item(c, 1743, 1);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				delete_item_from_inv(c, 1743, 1);
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 1131, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a hardleather body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Green d'hide body.  */
		case CRAFTING_GREEN_DLEATHER_BODY:
			x = player_has_item_non_stack(c, 1745, 3);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 3; x++)
				{
					delete_item_from_inv(c, 1745, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 1135, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a green dragonhide body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Green d'hide vambraces.  */
		case CRAFTING_GREEN_DLEATHER_VAMBRACES:
			x = player_has_item(c, 1745, 1);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				delete_item_from_inv(c, 1745, 1);
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 1065, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of green dragonhide "
								"vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Green d'hide chaps.  */
		case CRAFTING_GREEN_DLEATHER_CHAPS:
			x = player_has_item_non_stack(c, 1745, 2);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 2; x++)
				{
					delete_item_from_inv(c, 1745, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 1099, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of green dragonhide chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Blue d'hide body.  */
		case CRAFTING_BLUE_DLEATHER_BODY:
			x = player_has_item_non_stack(c, 2505, 3);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 3; x++)
				{
					delete_item_from_inv(c, 2505, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2499, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a blue dragonhide body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Blue d'hide vambraces.  */
		case CRAFTING_BLUE_DLEATHER_VAMBRACES:
			x = player_has_item(c, 2505, 1);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				delete_item_from_inv(c, 2505, 1);
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2487, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of blue dragonhide "
								"vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Blue d'hide chaps.  */
		case CRAFTING_BLUE_DLEATHER_CHAPS:
			x = player_has_item_non_stack(c, 2505, 2);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 2; x++)
				{
					delete_item_from_inv(c, 2505, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2493, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of blue dragonhide chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Red d'hide body.  */
		case CRAFTING_RED_DLEATHER_BODY:
			x = player_has_item_non_stack(c, 2507, 3);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 3; x++)
				{
					delete_item_from_inv(c, 2507, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2501, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a red dragonhide body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Red d'hide vambraces.  */
		case CRAFTING_RED_DLEATHER_VAMBRACES:
			x = player_has_item(c, 2507, 1);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				delete_item_from_inv(c, 2507, 1);
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2489, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of red dragonhide "
								"vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Red d'hide chaps.  */
		case CRAFTING_RED_DLEATHER_CHAPS:
			x = player_has_item_non_stack(c, 2507, 2);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 2; x++)
				{
					delete_item_from_inv(c, 2507, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2495, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of red dragonhide chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Black d'hide body.  */
		case CRAFTING_BLACK_DLEATHER_BODY:
			x = player_has_item_non_stack(c, 2509, 3);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 3; x++)
				{
					delete_item_from_inv(c, 2509, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2503, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a black dragonhide body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Black d'hide vambraces.  */
		case CRAFTING_BLACK_DLEATHER_VAMBRACES:
			x = player_has_item(c, 2509, 1);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				delete_item_from_inv(c, 2509, 1);
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2491, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of black dragonhide "
								"vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Black d'hide chaps.  */
		case CRAFTING_BLACK_DLEATHER_CHAPS:
			x = player_has_item_non_stack(c, 2509, 2);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 2; x++)
				{
					delete_item_from_inv(c, 2509, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 2497, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of black dragonhide chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Snakeskin body.  */
		case CRAFTING_SNAKESKIN_BODY:
			x = player_has_item_non_stack(c, 6289, 15);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 15; x++)
				{
					delete_item_from_inv(c, 6289, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 6322, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a snakeskin body.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Snakeskin chaps.  */
		case CRAFTING_SNAKESKIN_CHAPS:
			x = player_has_item_non_stack(c, 6289, 12);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 12; x++)
				{
					delete_item_from_inv(c, 6289, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 6324, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of snakeskin chaps.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Snakeskin vambraces.  */
		case CRAFTING_SNAKESKIN_VAMBRACES:
			x = player_has_item_non_stack(c, 6289, 8);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 8; x++)
				{
					delete_item_from_inv(c, 6289, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 6330, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of snakeskin vambraces.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Snakeskin bandana.  */
		case CRAFTING_SNAKESKIN_BANDANA:
			x = player_has_item_non_stack(c, 6289, 5);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 5; x++)
				{
					delete_item_from_inv(c, 6289, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 6326, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a snakeskin bandana.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Snakeskin boots.  */
		case CRAFTING_SNAKESKIN_BOOTS:
			x = player_has_item_non_stack(c, 6289, 6);
			item_1_slot = player_has_item(c, 1734, 1);

			if (x != -1 && item_1_slot != -1)
			{
				for (x = 0; x < 6; x++)
				{
					delete_item_from_inv(c, 6289, 1);
				}
				remove_inventory_item_charges(c, item_1_slot, 1);	
				add_item_to_inv(c, 6328, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 1249);
				packet_send_chatbox_message(c, "You make a pair of snakeskin boots.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Making molten glass.  */
		case CRAFTING_MOLTEN_GLASS:
			c->plr->action_item_slot = player_has_item(c, 1781, 1);
			c->plr->action_item_slot_2 = player_has_item(c, 1783, 1);
			
			if (c->plr->action_item_slot != -1 
				&& c->plr->action_item_slot_2 != -1)
			{
				player_play_animation(c, 899);
				packet_send_chatbox_message(c, "You put the sand and soda ash in the oven.");
				c->plr->action_ticks = 4;
				c->plr->action_skill_waiting_for_resource = 1;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Glass vial.  */
		case CRAFTING_VIAL:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 229, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into a vial.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Unpowered orb.  */
		case CRAFTING_UNPOWERED_ORB:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 567, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into an unpowered orb.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Beer glass.  */
		case CRAFTING_BEER_GLASS:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 1919, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into a beer glass.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Candle lantern.  */
		case CRAFTING_CANDLE_LANTERN:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 4527, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into a candle lantern.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Oil lamp.  */
		case CRAFTING_OIL_LAMP:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 4535, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into an oil lantern.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
		/* Lantern lens.  */
		case CRAFTING_LANTERN_LENS:
			item_1_slot = player_has_item(c, 1775, 1);
			item_2_slot = player_has_item(c, 1785, 1);

			if (item_1_slot != -1 && item_2_slot != -1)
			{
				delete_item_from_inv(c, 1775, 1);
				add_item_to_inv(c, 4542, 1, -1);
				add_skill_xp(c, CRAFTING_SKILL, xp_gain); 
				player_play_animation(c, 884);
				packet_send_chatbox_message(c, "You blow the glass into a lantern lens.");
				c->plr->action_ticks = 4;
			}
			else
			{
				set_player_state(c, STATE_IDLE);
			}
			break;
	/* Cutting gems.  */
		case CRAFTING_CUT_OPAL:
			cut_gem(c, 1625, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_JADE:
			cut_gem(c, 1627, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_RED_TOPAZ:
			cut_gem(c, 1629, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_SAPPHIRE:
			cut_gem(c, 1623, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_EMERALD:
			cut_gem(c, 1621, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_RUBY:
			cut_gem(c, 1619, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_DIAMOND:
			cut_gem(c, 1617, c->plr->action_id_type);
			break;
		case CRAFTING_CUT_DRAGONSTONE:
			cut_gem(c, 1631, c->plr->action_id_type);
			break;
	/* Making jewelry.  */	
		case CRAFTING_GOLD_RING:
		case CRAFTING_SAPPHIRE_RING:
		case CRAFTING_EMERALD_RING:
		case CRAFTING_RUBY_RING:
		case CRAFTING_DIAMOND_RING:
		case CRAFTING_DRAGONSTONE_RING:
		case CRAFTING_GOLD_NECKLACE:
		case CRAFTING_SAPPHIRE_NECKLACE:
		case CRAFTING_EMERALD_NECKLACE:
		case CRAFTING_RUBY_NECKLACE:
		case CRAFTING_DIAMOND_NECKLACE:
		case CRAFTING_DRAGONSTONE_NECKLACE:
		case CRAFTING_GOLD_AMULET:
		case CRAFTING_SAPPHIRE_AMULET:
		case CRAFTING_EMERALD_AMULET:
		case CRAFTING_RUBY_AMULET:
		case CRAFTING_DIAMOND_AMULET:
		case CRAFTING_DRAGONSTONE_AMULET:
		case CRAFTING_UNSTRUNG_SYMBOL:
		case CRAFTING_UNSTRUNG_EMBLEM:
		case CRAFTING_TIARA:
		case CRAFTING_SICKLE:
			put_jewelry_in_furnace(c, c->plr->action_id_type);
			break;	
	}

	c->plr->action_total_time_acted++;
	c->plr->action_skill_remaining_actions--;
}

