#ifndef _SKILL_FISHING_H
#define _SKILL_FISHING_H

int get_fishing_level_needed(int fishing_id);
float get_fishing_chance(int fishing_id, int fishing_level);
float get_fishing_xp(int fish_id);
int get_fishing_loot(int fishing_level, int fishing_id);

void init_fishing(struct client *c);
void handle_fishing(struct client *c);

#endif
