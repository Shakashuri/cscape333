/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "combat.h"
#include "item.h"
#include "npc.h"
#include "object.h"
#include "packet.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "misc.h"

int get_fishing_level_needed(int fishing_id)
{
	switch (fishing_id)
	{
		case FISHING_SMALL_NET:
			return 1;
		case FISHING_LARGE_NET:
			return 16;
		case FISHING_BAIT:
			return 5;
		case FISHING_RIVER_BAIT:
			return 25;
		case FISHING_LURE:
			return 20;
		case FISHING_HARPOON:
			return 35;
		case FISHING_LOBSTER_POT:
			return 40;
		case FISHING_KARAMBWANJI:
			return 65;
		default:
			printf("Error! Unknown id: %d, fishing lvl!\n", fishing_id);
			return -1;
	}
}

float get_fishing_chance(int fishing_id, int fishing_level)
{
	switch (fishing_id)
	{
		case FISHING_SMALL_NET:
			return (fishing_level * 0.0085) + 0.45;
		case FISHING_LARGE_NET:
			return fishing_level * 0.0085;
		case FISHING_BAIT:
			return (fishing_level * 0.0085) + 0.2;
		case FISHING_RIVER_BAIT:
			return fishing_level * 0.004;
		case FISHING_LURE:
			return fishing_level * 0.004;
		case FISHING_HARPOON:
			return fishing_level * 0.003;
		case FISHING_LOBSTER_POT:
			return fishing_level * 0.002;
		case FISHING_KARAMBWANJI:
			return fishing_level * 0.001;
		default:
			printf("Error! Unknown id: %d, fishing lvl!\n", fishing_id);
			return -1;
	}
}

float get_fishing_xp(int fish_id)
{
	switch (fish_id)
	{
		/* Shrimp.  */
		case 317:
			return 10.0;
		/* Anchovy.  */
		case 321:
			return 40.0;
		/* Sardine.  */
		case 327:
			return 20.0;
		/* Salmon.  */
		case 331:
			return 70.0;
		/* Trout.  */
		case 335:
			return 50.0;
		/* Cod.  */
		case 341:
			return 45.0;
		/* Herring.  */
		case 345:
			return 30.0;
		/* Pike.  */
		case 349:
			return 60.0;
		/* Mackeral.  */
		case 353:
			return 20.0;
		/* Tuna.  */
		case 359:
			return 80.0;
		/* Bass.  */
		case 363:
			return 100.0;
		/* Swordfish.  */
		case 371:
			return 100.0;
		/* Lobster.  */
		case 377:
			return 90.0;
		/* Shark.  */
		case 383:
			return 110.0;
		/* Manta ray.  */
		case 389:
			return 46.0;
		/* Sea turtle.  */
		case 395:
			return 38.0;
		/* Seaweed.  */
		case 401:
			return 1.0;
		/* Casket.  */
		case 405:
			return 10.0;
		/* Oyster.  */
		case 407:
			return 10.0;
		/* Lava eel.  */
		case 2148:
			return 30.0;
		/* Karambwan.  */
		case 3142:
			return 105.0;
		/* Karambwanji.  */
		case 3151:
			return 5.0;
		/* Slimey eel.  */
		case 3379:
			return 65.0;
		/* Cave eel.  */
		case 5001:
			return 80.0;
		/* Frog spawn.  */
		case 5004:
			return 75.0;
		default:
			printf("Error! Unknown id: %d, fishing xp!\n", fish_id);
			return 0.0;
	}
}

int get_fishing_loot(int fishing_level, int fishing_id)
{
	int range = 0;

	/* Return loot based on type of fishing being done.  */
	switch (fishing_id)
	{
		case FISHING_SMALL_NET:
			if (fishing_level >= 15)
			{
				range = 1;
			}

			switch (rand_int(0, range))
			{
				case 0:
					return 317;
				case 1:
					return 321;
				default:
					printf("Fish loot error! Id: %d Range: %d\n", fishing_id, 
							range);
					return -1;
			}
		case FISHING_LARGE_NET:
			/* Minimum range for this.  */
			range = 3;

			if (fishing_level >= 20)
			{
				range++;

				if (fishing_level >= 25)
				{
					range++;

					if (fishing_level >= 46)
					{
						range++;
					}
				}
			}

			switch (rand_int(0, range))
			{
				case 0:
					return 321;
				case 1:
					return 353;
				case 2:
					return 407;
				case 3:
					return 405;
				case 4:
					return 401;
				case 5:
					return 341;
				case 6:
					return 363;
				default:
					printf("Fish loot error! Id: %d Range: %d\n", fishing_id, 
							range);
					return -1;
			}
		case FISHING_BAIT:
			if (fishing_level >= 10)
			{
				range++;
			}
			
			switch (rand_int(0, range))
			{
				case 0:
					return 327;
				case 1:
					return 345;
				default:
					printf("Fish loot error! Id: %d Range: %d\n", fishing_id, 
							range);
					return -1;
			}
		case FISHING_RIVER_BAIT:
			return 351;
		case FISHING_LURE:
			if (fishing_level >= 30)
			{
				range++;
			}
			
			switch (rand_int(0, range))
			{
				case 0:
					return 335;
				case 1:
					return 331;
				default:
					printf("Fish loot error! Id: %d Range: %d\n", fishing_id, 
							range);
					return -1;
			}

		case FISHING_HARPOON:
			if (fishing_level >= 50)
			{
				range++;
				if (fishing_level >= 76)
				{
					range++;
				}
			}
			switch (rand_int(0, range))
			{
				case 0:
					return 359;
				case 1:
					return 371;
				case 2:
					return 383;
				default:
					printf("Fish loot error! Id: %d Range: %d\n", fishing_id, 
							range);
					return -1;
			}
		case FISHING_LOBSTER_POT:
			return 377;
		case FISHING_KARAMBWANJI:
			return 3151;

		default:
			printf("Error! Unknown id: %d, fishing loot id!\n", fishing_id);
			return -1;
	}
}

/**
 * Sets up all the needed variables for fishing, checking skill requirements, 
 * setting animation.
 * @param *c Client instance.
 */
void init_fishing(struct client *c)
{
	int required_level = get_fishing_level_needed(c->plr->action_id_type);
	int fishing_level = get_player_skill_level(c, FISHING_SKILL);

	/* Set action related ids.  */
	c->plr->action_skill_id = FISHING_SKILL;
	c->plr->action_skill_type = 0;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_FISHING);

	if (get_free_inv_slots(c) == 0)
	{
		packet_send_chatbox_message(c, "You don't have room for any more fish!");
		set_player_state(c, STATE_IDLE);
		return;
	}

	if (fishing_level < required_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "fishing to fish here!\n", required_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}

	/* TODO: In this version, there are no equipable fishing tools. In later
	   versions there are, and this needs to be changed for that.  */
	c->plr->skill_tool_equip = 0;

	/* Check for equipment.  */
	switch (c->plr->action_id_type)
	{
		/* Net fishing.  */
		case FISHING_SMALL_NET:
			c->plr->skill_tool_id = 303;
			c->plr->skill_tool_slot = player_has_item(c, 303, 1);
			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a net for fishing!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			c->plr->skill_tool_emote = 621;
			c->plr->skill_tool_time = 5;
			break;
		
		/* Large net fishing.  */
		case FISHING_LARGE_NET:
			c->plr->skill_tool_id = 305;
			c->plr->skill_tool_slot = player_has_item(c, 305, 1);

			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a large net for fishing!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			c->plr->skill_tool_emote = 620;
			c->plr->skill_tool_time = 5;
			break;
	
		/* Bait fishing.  */
		case FISHING_BAIT:
		case FISHING_RIVER_BAIT:
			c->plr->skill_tool_id = 307;
			c->plr->skill_tool_slot = player_has_item(c, 307, 1);

			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a bait fishing rod!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			else
			{
				c->plr->skill_req_one_id = 313;
				if (player_has_item(c, 313, 1) == -1) 
				{
					packet_send_chatbox_message(c, "You don't have any bait!");
					set_player_state(c, STATE_IDLE);
					return;
				}
			}
			c->plr->skill_tool_emote = 622;
			c->plr->skill_tool_time = 5;
			break;
		
		/* Lure fishing.  */
		case FISHING_LURE:
			c->plr->skill_tool_id = 309;
			c->plr->skill_tool_slot = player_has_item(c, 309, 1);
			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a fly fishing rod!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			else
			{
				c->plr->skill_req_one_id = 314;
				if (player_has_item(c, 314, 1) == -1) 
				{
					packet_send_chatbox_message(c, "You don't have any feathers!");
					set_player_state(c, STATE_IDLE);
					return;
				}
			}
			c->plr->skill_tool_emote = 622;
			c->plr->skill_tool_time = 5;
			break;
	
		/* Cage fishing.  */
		case FISHING_LOBSTER_POT:
			c->plr->skill_tool_id = 301;
			c->plr->skill_tool_slot = player_has_item(c, 301, 1);
			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a lobster pot!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			c->plr->skill_tool_emote = 619;
			c->plr->skill_tool_time = 5;
			break;

		/* Harpoon fishing.  */
		case FISHING_HARPOON:
			c->plr->skill_tool_id = 311;
			c->plr->skill_tool_slot = player_has_item(c, 311, 1);
			if (c->plr->skill_tool_slot == -1)
			{
				packet_send_chatbox_message(c, "You don't have a harpoon!");
				set_player_state(c, STATE_IDLE);
				return;
			}
			c->plr->skill_tool_emote = 618;
			c->plr->skill_tool_time = 8;
			break;
	}
	/* If we got here, everything is alright.  */
	player_play_animation(c, c->plr->skill_tool_emote);

	/* If emote is the initial 'casting' emote, change it to the regular one
	   for later calls.  */
	if (c->plr->skill_tool_emote == 622)
	{
		c->plr->skill_tool_emote++;
	}

	c->plr->action_ticks = c->plr->skill_tool_time;
}

/**
 * Handles chance for fishing, removing fishing spots, item handling, etc.
 * @param *c Client instance.
 */
void handle_fishing(struct client *c)
{
	int effective_fishing_level = get_player_skill_level(c, FISHING_SKILL); 
	
	double roll = rand_double(0.0, 1.0);
	double fish_chance = get_fishing_chance(c->plr->action_id_type, 
											effective_fishing_level);

	if (DEBUG == 1)
	{
		printf("roll: %f chance: %f %d\n", roll, fish_chance, 
			   (roll < fish_chance) ? 1 : 0);
	}

	/* If fishing has succeeded.  */
	if (roll < fish_chance)
	{
		int npc_index, fish_caught;

		fish_caught 
		= get_fishing_loot(effective_fishing_level, c->plr->action_id_type);

		if (add_item_to_inv(c, fish_caught, 1, -1) == 1)
		{
			packet_send_chatbox_message(c, "You caught a fish.");
			
			/* Gives the player xp proper to the fish caught.  */
			add_skill_xp(c, FISHING_SKILL, get_fishing_xp(fish_caught));

			/* Remove a piece of bait or feather if used in fishing.  */
			if (c->plr->skill_req_one_id != -1)
			{
				delete_item_from_inv(c, c->plr->skill_req_one_id, 1);
			}
		}
		else
		{
			set_player_state(c, STATE_IDLE);
			return;
		}

		if (rand_int(1, 8) == 8)
		{
			npc_index = get_index_of_npc_at_world_coord(c->plr->action_x, 
						c->plr->action_y, c->plr->world_z);
			
			/* Sets fishing spot as 'dead', and moves it out of the world until
			   respawn.  */
			/* This says combat targets, but is used here for making sure
			   the player doesn't try to move to the fishing spot when it moves
			   because they are treated as NPCs.  */
			reset_player_combat_target(c);

			/* TODO: This may not work properly, change this to a function
			 * call instead as well.  */
			NPC_SPAWNS[npc_index]->dead = (char) 2;
			NPC_SPAWNS[npc_index]->world_x = NPC_SPAWNS[npc_index]->world_y = 0;
			NPC_SPAWNS[npc_index]->update_required = 1;
			NPC_SPAWNS[npc_index]->tick_killed = 0;
			NPC_SPAWNS[npc_index]->respawn_ticks = 10;

			reset_others_skilling(c, c->plr->action_index);
			set_player_state(c, STATE_IDLE);
		}
	}

	player_play_animation(c, c->plr->skill_tool_emote);
	c->plr->action_ticks = c->plr->skill_tool_time;
	c->plr->action_total_time_acted++;
}
