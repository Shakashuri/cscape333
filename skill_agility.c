/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file skill_agility.c
 * @brief Specific agility skill related functions.
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "constants.h"
#include "client.h"
#include "combat.h"
#include "combat_calc.h"
#include "item.h"
#include "item_data.h"
#include "misc.h"
#include "object.h"
#include "packet.h"
#include "pathfinding.h"
#include "player.h"
#include "rng.h"
#include "skills.h"
#include "skill_agility.h"

int get_agility_obstacle_index(int id, int pos_x, int pos_y, int pos_z)
{
	int x;

	for (x = 0; x < NUM_AGILITY_OBSTACLES; x++)
	{
		if (AGILITY_DATA[x]->id == id 
			&& AGILITY_DATA[x]->world_x == pos_x
			&& AGILITY_DATA[x]->world_y == pos_y
			&& AGILITY_DATA[x]->world_z == pos_z)
		{
			return x;
		}
	}

	printf("Invalid agility obstacle!\n");
	printf("ID: %d X: %d Y: %d Z: %d\n", id, pos_x, pos_y, pos_z);
	return -1;
}

int get_agility_course_obstacle_count(int index)
{
	if (index < 0 || index >= NUM_AGILITY_OBSTACLES)
	{
		printf("Invalid agility index %d!\n", index);
		return -1;
	}

	return AGILITY_DATA[AGILITY_DATA[index]->course_index]->course_obstacle_id;
}

int get_agility_level_needed(int index)
{
	if (index < 0 || index >= NUM_AGILITY_OBSTACLES)
	{
		printf("Invalid agility index %d!\n", index);
		return -1;
	}

	return AGILITY_DATA[index]->level_needed;
}

float get_agility_xp(int index)
{
	if (index < 0 || index >= NUM_AGILITY_OBSTACLES)
	{
		printf("Invalid agility index %d!\n", index);
		return -1;
	}

	return AGILITY_DATA[index]->xp;
}

float get_agility_course_xp(int index)
{
	if (index < 0 || index >= NUM_AGILITY_OBSTACLES)
	{
		printf("Invalid agility index %d!\n", index);
		return -1;
	}

	if (AGILITY_DATA[index]->course_index != -1)
	{
		return AGILITY_DATA[AGILITY_DATA[index]->course_index]->xp;
	}
	else
	{
		return 0.0;
	}
}

int is_player_at_agility_position(struct client *c)
{
	int index = c->plr->action_store_one;

	if (index < 0 || index >= NUM_AGILITY_OBSTACLES)
	{
		printf("Invalid agility index %d!\n", index);
		return -1;
	}

	if (c->plr->world_x == AGILITY_DATA[index]->use_x 
		&& c->plr->world_y == AGILITY_DATA[index]->use_y)
	{
		return 1;
	}
	else if (c->plr->world_x == AGILITY_DATA[index]->use_x_two 
			 && c->plr->world_y == AGILITY_DATA[index]->use_y_two)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}


/**
 * Clear the agility progress of a course.
 * @param *c Client to clear progress for.
 */
void reset_agility_course_progress(struct client *c)
{
	int x = 0;

	for (x = 0; x < AGILITY_MAX_OBSTACLES; x++)
	{
		c->plr->agility_course_progress[x] = 0;
	}
}

/**
 * Check if the player's current agility course has been fully completed.
 * @param *c Client to check.
 * @return Returns 1 if complete, 0 if not.
 */
int is_agility_course_complete(struct client *c)
{
	int x = 0, not_done = 0;

	printf("check course progress!\n");
	printf(" num : %d\n", c->plr->agility_obstacles_number);

	for (x = 0; x < c->plr->agility_obstacles_number; x++)
	{
		if (c->plr->agility_course_progress[x] == 0)
		{
			not_done = 1;
		}

		printf("%d: %d\n", x, c->plr->agility_course_progress[x]);
	}

	if (not_done == 0)
	{
		if (c->plr->agility_course_idx != -1 
			&& AGILITY_DATA[c->plr->action_store_one]->is_final_obstacle == 1)
		{
			printf("Final obstacle, and have done every other one\n");
			printf("Giving bonus xp\n");
			return 1;
		}
	}

	return 0;
}

static void end_agility(struct client *c, int failed)
{
	float xp, course_xp;

	/* If they failed the agility course, handle everything.  */
	if (failed == 1)
	{
		/* Send the agility failure message.  */
		if (strcmp(AGILITY_DATA[c->plr->action_store_one]->lose_message,
				   "null") != 0)
		{
			packet_send_chatbox_message(c, 
				AGILITY_DATA[c->plr->action_store_one]->lose_message);
		}

		/* Reset all animations, clear agility stuff.  */
		apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
		/* TODO: Do we really need an individual failure emote for each stage?
		 * Change this later.  */
		player_play_animation(c, AGILITY_DATA[c->plr->action_store_one]->stages[c->plr->action_store_two].lose_emote);

		/* Force animation update.  */
		c->plr->update_masks[ANIMATION_REQ] = 1;
		c->plr->update_masks[APPEARANCE_UPDATE] = 1;
		c->plr->update_required = 1;

		/* Teleport to the failure area after the delay stated in the agility
		 * data.  */
		c->plr->stored_tele_x 
			= AGILITY_DATA[c->plr->action_store_one]->fail_x;
		c->plr->stored_tele_y 
			= AGILITY_DATA[c->plr->action_store_one]->fail_y;
		c->plr->stored_tele_z 
			= AGILITY_DATA[c->plr->action_store_one]->fail_z;
		c->plr->teleport_timer 
			= AGILITY_DATA[c->plr->action_store_one]->fail_ticks;
		c->plr->teleport_type = 1;

		/* Add xp given on failure of an obstacle, mainly used for shortcuts. 
		 */
		add_skill_xp(c, AGILITY_SKILL, 
			AGILITY_DATA[c->plr->action_store_one]->xp_on_failure);

		/* Freeze movement while failing.  */
		freeze_player(c, EFFECT_FREEZE, 
			AGILITY_DATA[c->plr->action_store_one]->fail_ticks);

		/* If damage is dealt to the player during their failure, do it here.*/
		float max_dmg = AGILITY_DATA[c->plr->action_store_one]->dmg;

		/* Deal damage based on either percentage, or a flat number. Eitherway,
		 * when dealing damage, roll from 1 to the max.  */
		if (max_dmg > 0.0 && max_dmg < 1.0)
		{
			/* If damage is less than 1, it must be percentage based.  */
			int damage_to_deal = floorf(c->plr->level[HITPOINTS_SKILL] 
										* max_dmg);

			deal_damage_to_player(c, rand_int(1, damage_to_deal), -1, 2, -1, 
				-1, -1);
		}
		else if (max_dmg >= 1.0)
		{
			deal_damage_to_player(c, rand_int(1, max_dmg), -1, 2, -1, 
				-1, -1);
		}

		set_player_state(c, STATE_IDLE);
		return;
	}
	else
	{
		/* Send the initial agility message.  */
		if (strcmp(AGILITY_DATA[c->plr->action_store_one]->win_message, 
				   "null") != 0)
		{
			packet_send_chatbox_message(c, 
				AGILITY_DATA[c->plr->action_store_one]->win_message);
		}
	}

	/* Set this obstacle as completed.  */
	if (c->plr->agility_course_idx != -1)
	{
		int obstacle_number 
			= AGILITY_DATA[c->plr->action_store_one]->course_obstacle_id;

		if (obstacle_number >= 0 && obstacle_number < AGILITY_MAX_OBSTACLES)
		{
			c->plr->agility_course_progress[obstacle_number] = 1;
		}
	}

	xp = get_agility_xp(c->plr->action_store_one); 
	course_xp = get_agility_course_xp(c->plr->action_store_one);

	if (is_agility_course_complete(c) == 1)
	{
		printf("Course completed, add extra XP.\n");
		add_skill_xp(c, AGILITY_SKILL, (xp + course_xp));
		reset_agility_course_progress(c);
	}
	else
	{
		add_skill_xp(c, AGILITY_SKILL, xp);
	}

	/* Reset all animations, clear agility stuff.  */
	apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);

	if (AGILITY_DATA[c->plr->action_store_one]->obstacle_type == AGILITY_WALK)
	{
		c->plr->update_masks[APPEARANCE_UPDATE] = 1;
		c->plr->update_required = 1;
	}

	reset_skilling_no_animation_reset(c);
	set_player_state(c, STATE_IDLE);
}

static void handle_agility_stage(struct client *c)
{
	int ob_idx = c->plr->action_store_one;
	int ob_stage = c->plr->action_store_two;
	int store_x, store_y;
	int direction = 0;

	if (AGILITY_DATA[ob_idx]->stages[ob_stage].failable == 1)
	{
		/* Check if the player fails the obstacle.  */
		if (is_skill_check_success(c, AGILITY_SKILL, 
			AGILITY_DATA[ob_idx]->win_base_chance, 
			AGILITY_DATA[ob_idx]->win_max_chance, 1.0) == 1)
		{
			/* won obstacle, continue down the rest of this function for
			 * victory handling.  */
		}
		else
		{
			/* End agility with a failure.  */
			end_agility(c, 1);
			return;	
		}
	}

	switch (AGILITY_DATA[c->plr->action_store_one]->obstacle_type)
	{
		/* If stage is a walker, setup emotes.  */
		case AGILITY_WALK:
			/* Walk type 1, force move while doing an animation.  */
			if (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 1)
			{
				if (AGILITY_DATA[ob_idx]->stages[ob_stage].walk_emote == -1)
				{
					apply_weapon_info(c, c->plr->equipment[WEAPON_SLOT]);
				}
				else
				{
					c->plr->walking_emote 
						= AGILITY_DATA[ob_idx]->stages[ob_stage].walk_emote;
					c->plr->standing_emote 
						= AGILITY_DATA[ob_idx]->stages[ob_stage].stand_emote;
				}

				if (AGILITY_DATA[ob_idx]->stages[ob_stage].ticks != 0)
				{
					c->plr->action_ticks 
						= AGILITY_DATA[ob_idx]->stages[ob_stage].ticks;

					freeze_player(c, EFFECT_FREEZE, 
						AGILITY_DATA[ob_idx]->stages[ob_stage].ticks);

				}
				else
				{
					/* FIXME, doesn't need to be this long of a freeze.  */
					freeze_player(c, EFFECT_FREEZE, 90);

					/* We will check every tick to see if the player is still
					 * moving. If so, we'll check again until they stop.  */
					c->plr->action_ticks = 0;
				}

				/* Handle movement based on which valid square the player is on
				 * at the time.  */
				switch (c->plr->action_store_four)
				{
					case 1:
						player_add_straight_line_path_to_walking_queue(c,
							(c->plr->world_x 
								+ AGILITY_DATA[ob_idx]->stages[ob_stage].x),
							(c->plr->world_y
								+ AGILITY_DATA[ob_idx]->stages[ob_stage].y),
							1, 1);	
						break;
					case 2:
						player_add_straight_line_path_to_walking_queue(c,
							(c->plr->world_x 
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].x),
							(c->plr->world_y
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].y),
							1, 1);	
						break;
					
					default:
						printf("no good!\n");
						return;
				}

			}
			/* Type 2, phased movement, single time.
			 * Type 3, phased movement, but used for when phase is needed
			 * over and over again, like the stepping stones in the 
			 * wilderness.  */
			else if (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 2
					 || AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 3)
			{
				if (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 2
					|| (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 3 
						&& c->plr->stored_tele_x == 0))
				{
					switch (c->plr->action_store_four)
					{
						case 1:
							store_x = c->plr->world_x 
									+ AGILITY_DATA[ob_idx]->stages[ob_stage].x;
							store_y = c->plr->world_y
									+ AGILITY_DATA[ob_idx]->stages[ob_stage].y;
							break;
						case 2:
							store_x = c->plr->world_x 
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].x;
							store_y = c->plr->world_y
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].y;
							break;
						
						default:
							printf("no good!\n");
							return;
					}
				}
				else
				{
					/* If this is incremental phased movement, use the stored
					 * teleport variable to keep track of where we need to
					 * do the phase movement from.  */
					switch (c->plr->action_store_four)
					{
						case 1:
							store_x = c->plr->stored_tele_x 
									+ AGILITY_DATA[ob_idx]->stages[ob_stage].x;
							store_y = c->plr->stored_tele_y
									+ AGILITY_DATA[ob_idx]->stages[ob_stage].y;
							break;
						case 2:
							store_x = c->plr->stored_tele_x 
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].x;
							store_y = c->plr->stored_tele_y
								+ -AGILITY_DATA[ob_idx]->stages[ob_stage].y;
							break;
						
						default:
							printf("no good!\n");
							return;
					}
				}

				player_play_animation(c, 
					AGILITY_DATA[ob_idx]->stages[ob_stage].walk_emote);

				/* Get direction for when we do the phase movement.  */
				direction = get_walking_direction(c->plr->world_x,
										c->plr->world_y, store_x, store_y);

				/* Check if we're using regular phased, or incremental.  */
				if (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 2
					|| (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 3 
						&& c->plr->stored_tele_x == 0))
				{
					set_phase_movement(c, c->plr->world_x, c->plr->world_y, 
									   store_x,
									   store_y,
									   AGILITY_DATA[ob_idx]->
											stages[ob_stage].phase_delay,
									   AGILITY_DATA[ob_idx]->
											stages[ob_stage].phase_time,
									   direction);
				}
				else
				{
					set_phase_movement(c, c->plr->stored_tele_x, 
									   c->plr->stored_tele_y, 
									   store_x,
									   store_y,
									   AGILITY_DATA[ob_idx]->
											stages[ob_stage].phase_delay,
									   AGILITY_DATA[ob_idx]->
											stages[ob_stage].phase_time,
									   direction);
				}

				if (AGILITY_DATA[ob_idx]->stages[ob_stage].ticks != 0)
				{
					c->plr->action_ticks 
						= AGILITY_DATA[ob_idx]->stages[ob_stage].ticks;

					freeze_player(c, EFFECT_FREEZE, 
						AGILITY_DATA[ob_idx]->stages[ob_stage].ticks);
				}
				else
				{
					printf("ERROR! Agility item %d doesn't have phase time!\n",
						ob_idx);
					freeze_player(c, EFFECT_FREEZE, 6);

					c->plr->action_ticks = 0;
				}
				
				/* Move player to position.  */
				c->plr->stored_tele_x = store_x;
				c->plr->stored_tele_y = store_y;
				c->plr->stored_tele_z = c->plr->world_z;
				/* TODO: Make this configurable?  */
				c->plr->teleport_timer = 2;
				c->plr->teleport_type = -1;
			}
			/* Type 4, teleport.  */
			else if (AGILITY_DATA[ob_idx]->stages[ob_stage].is_walk == 4)
			{
				printf("teleport!\n");
				if (AGILITY_DATA[ob_idx]->stages[ob_stage].ticks != 0)
				{
					c->plr->action_ticks 
						= AGILITY_DATA[ob_idx]->stages[ob_stage].ticks;

					freeze_player(c, EFFECT_FREEZE, 
						AGILITY_DATA[ob_idx]->stages[ob_stage].ticks);
				}
				else
				{
					c->plr->action_ticks = 0;

					freeze_player(c, EFFECT_FREEZE_AGILITY, 90);
				}

				c->plr->tele_x = AGILITY_DATA[ob_idx]->stages[ob_stage].x;
				c->plr->tele_y = AGILITY_DATA[ob_idx]->stages[ob_stage].y;
				c->plr->tele_z = c->plr->world_z;
			}
 
			c->plr->update_masks[ANIMATION_REQ] = 1;
			c->plr->update_masks[APPEARANCE_UPDATE] = 1;
			c->plr->update_required = 1;
			break;
		case AGILITY_LADDER:
			player_play_animation(c, AGILITY_DATA[ob_idx]->stages[ob_stage].win_emote);
			c->plr->teleport_timer = 1;
			c->plr->teleport_type = -1;
			c->plr->stored_tele_x = AGILITY_DATA[ob_idx]->stages[ob_stage].x;
			c->plr->stored_tele_y = AGILITY_DATA[ob_idx]->stages[ob_stage].y;
			c->plr->stored_tele_z = (AGILITY_DATA[ob_idx]->stages[ob_stage].z);
			end_agility(c, 0);
			break;
		default:
			printf("Phase movement not handled yet!\n");
			break;
	}
}

/**
 * Handle moving the player across an obstacle.
 * @param *c Client instance.
 * @todo Add messages, fill out the table for completing the course.
 */
void handle_agility_obstacle(struct client *c)
{
	printf("store: %d remain: %d\n", 
		c->plr->action_store_two, c->plr->action_skill_remaining_actions);

	/* If there are still stages, handle them.  */
	if (c->plr->action_store_two 
		<= c->plr->action_skill_remaining_actions)
	{
		printf("stage: %d\n", c->plr->action_store_two);
		handle_agility_stage(c);
		c->plr->action_store_two++;
	}
	else
	{
		printf("end obstacle!\n");
		end_agility(c, 0);
	}
}

/**
 * Make sure that everything is ready for an agility task to start.
 * @param *c Client instance.
 */
void init_agility(struct client *c)
{
	printf("#########init agility\n");
	int at_pos = 0;
	int ob_idx 
		= c->plr->action_store_one 
		= get_agility_obstacle_index(c->plr->action_id, c->plr->action_x,
									 c->plr->action_y, c->plr->world_z);
	int rv = 0;
	int direction_to_turn_to = -1;
	c->plr->action_store_two = 0;

	if (ob_idx == -1)
	{
		set_player_state(c, STATE_IDLE);
		return;
	}

	int agility_level = get_player_skill_level(c, AGILITY_SKILL);
	int req_level = get_agility_level_needed(ob_idx);

	if (agility_level < req_level)
	{
		char error_msg[150];
		snprintf(error_msg, 150, "You must be at least level %d in "
				 "agility to do that!\n", req_level);
		packet_send_chatbox_message(c, error_msg);
		set_player_state(c, STATE_IDLE);
		return;
	}
	
	c->plr->action_skill_id = AGILITY_SKILL;
	c->plr->action_ticks = 0;
	c->plr->action_total_time_acted = 0;

	set_player_state(c, STATE_ACTION);
	set_player_state_type(c, STATE_ACTION_SKILL_AGILITY);

	at_pos = is_player_at_agility_position(c);

	/* Check if the player is at the right position to do the agility obstacle.
	 * If they are not, move them to the closest valid position.  */
	if (at_pos >= 1)
	{
		printf("at good position, continue.\n");
		
		/* If we're at the secondary position, force the secondary turn.  */
		if (at_pos == 2)
		{
			/* Make sure that the turn_two position actually has data, if not,
			 * use the turn position.  */
			if (AGILITY_DATA[ob_idx]->turn_two == -1 
				&& AGILITY_DATA[ob_idx]->turn != -1)
			{
				direction_to_turn_to = AGILITY_DATA[ob_idx]->turn;
			}
			else
			{
				direction_to_turn_to = AGILITY_DATA[ob_idx]->turn_two;
			}
		}
		else
		{
			direction_to_turn_to = AGILITY_DATA[ob_idx]->turn;
		}
	}
	else
	{
		int first_dist = get_distance_from_point(c->plr->world_x, 
								 c->plr->world_y, 
								 AGILITY_DATA[ob_idx]->use_x, 
								 AGILITY_DATA[ob_idx]->use_y);
		int second_dist = get_distance_from_point(c->plr->world_x, 
								 c->plr->world_y, 
								 AGILITY_DATA[ob_idx]->use_x_two, 
								 AGILITY_DATA[ob_idx]->use_y_two);

		if (first_dist <= second_dist)
		{
			rv = player_move_to_location(c, c->plr->world_x, c->plr->world_y, 
						 AGILITY_DATA[ob_idx]->use_x, 
						 AGILITY_DATA[ob_idx]->use_y, c->plr->world_z);
		}
		else
		{
			rv = player_move_to_location(c, c->plr->world_x, c->plr->world_y, 
						 AGILITY_DATA[ob_idx]->use_x_two, 
						 AGILITY_DATA[ob_idx]->use_y_two, 
						 c->plr->world_z);
		}
		printf("no good, moving!\n");
		/*
		set_player_state(c, STATE_IDLE);
		c->plr->action_store_three = 0;
		c->plr->action_skill_id = -1;
		*/

		if (rv == 0)
		{
			printf("Set to 2\n");
			c->plr->action_mask = WILL_OPERATE;
			c->plr->action_skill_id = AGILITY_SKILL;
			c->plr->action_skill_type = AGILITY_ACTION_GETTING_CLOSER;
		}
		else
		{
			printf("Set to 0\n");
			c->plr->action_mask = WILL_OPERATE;
			set_player_state(c, STATE_IDLE);
		}
		return;
	}

	/* Store what position the player started at for later processing.  */
	c->plr->action_store_four = at_pos;

	/* Turn the player to the right direction if they aren't already facing
	 * the direction.  */
	if ((c->plr->action_store_three != 1 && direction_to_turn_to != -1)
		&& c->plr->last_walked_direction != direction_to_turn_to)
	{
		printf("not facing! Turning player\n");
		c->plr->action_skill_id = AGILITY_SKILL;
		c->plr->action_skill_type = AGILITY_ACTION_TURNING;
		c->plr->action_ticks = 1;
		c->plr->action_store_three = 1;
		player_turn_to_direction(c, direction_to_turn_to);
		return;
	}
	else
	{
		printf("now turned, continue!\n");
		c->plr->action_store_three = 0;
		c->plr->action_skill_type = AGILITY_ACTION_PROCESSING;
	}

	/* Send the initial agility message.  */
	if (strcmp(AGILITY_DATA[ob_idx]->start_message, "null") != 0)
	{
		packet_send_chatbox_message(c, AGILITY_DATA[ob_idx]->start_message);
	}

	/* Setup agility course tracking.  */

	/* If we're on a different course than one before, reset the progress.  */
	if (c->plr->agility_course_idx != AGILITY_DATA[ob_idx]->course_index)
	{
		reset_agility_course_progress(c);
	}

	c->plr->agility_course_idx 
		= AGILITY_DATA[ob_idx]->course_index;

	if (c->plr->agility_course_idx != -1)
	{
		c->plr->agility_obstacles_number
			= AGILITY_DATA[AGILITY_DATA[ob_idx]->
				course_index]->course_obstacle_id;
	}

	/* Set total number of stages, and subtract one to make it easier for the
	 * array handling.  */
	c->plr->action_skill_remaining_actions 
		= (AGILITY_DATA[ob_idx]->num_stages - 1);

	/* Make sure player doesn't run, and cannot send movement commands when
	 * they are doing the obstacle.  */
	if (AGILITY_DATA[ob_idx]->obstacle_type == AGILITY_WALK)
	{
		freeze_player(c, EFFECT_FREEZE, 90);

		c->plr->action_ticks = 0;
	}
	else
	{
		/* TODO: Freeze player, but move duration to file.  */
		freeze_player(c, EFFECT_FREEZE, 1);

		c->plr->action_ticks = 1;
	}

	c->plr->is_running = 0;
	c->plr->focus_point_x = c->plr->focus_point_y = -1;
	c->plr->update_masks[APPEARANCE_UPDATE] = 1;
	c->plr->update_required = 1;

	/* Play a start emote, if there is one.  */
	if (AGILITY_DATA[ob_idx]->start_emote != -1)
	{
		player_play_animation(c, AGILITY_DATA[ob_idx]->start_emote);
	}
	/* Handle the first stage of the agility obstacle.  */
	handle_agility_obstacle(c);
}


