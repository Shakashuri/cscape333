#ifndef _REGION_H_
#define _REGION_H_

/**
 * @brief Holds information for each region in the world map.
 *
 * Holds data for each region of the map, any variables specific for the region
 * that need to be kept track of.
 */
struct region_data
{
	/** Id of the region.  */
	int id;
	/** World X of the region.  */
	int world_x;
	/** World Y of the region.  */
	int world_y;
	/** X coord of the region.  */
	int region_x;
	/** Y coord of the region.  */
	int region_y;

	/** If the region has music or not. Used to mark if a region is one that a
	 * player can move to or not.  */
	int has_music;
	/** Music id of this region.  */
	int music_id;

	/** Malloc'd size of the arrays.  */
	unsigned short int obj_spawns_size;

	/** List of objects in this region by default.  */
	struct obj_spawn_org **OBJECTS;
};

int get_region_id(int x, int y);
int get_region_coord(int coord);
int get_region_index(int reg_id);
void calloc_map_region(int region_id);
void free_map_data(struct region_data **region_list, 
				   struct region_data ***region_map);

void add_collision(int x, int y, int height, int mask);
int get_collision(int x, int y, int height);
int overwrite_collision(int x, int y, int height, int new_value);
int mod_collision(int x, int y, int height, int change);
int clear_collision_bit(int x, int y, int height, int change);
int load_map_level(int height);
int load_map_data(void);

#endif
