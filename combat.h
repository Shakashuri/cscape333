/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMBAT_H_
#define _COMBAT_H_

void reset_player_combat(struct client *c);
void reset_player_combat_target(struct client *c);
void reset_npc_combat(struct npc_spawn *npc);
void reset_npc_combat_move_to_home(struct npc_spawn *npc);
void set_player_combat_target(struct client *c, int npc_index, 
							  int player_index);
void set_npc_combat_target(struct npc_spawn *n, int npc_index, 
						   int player_index);
int is_npc_combat_target_player_valid(struct npc_spawn *npc);
void set_player_retaliation_target(struct client *c, int is_player, int index);
void process_aggressive_npcs(struct client *c);

int player_init_combat(struct client *c, int npc_index, int player_index, 
					   int spell_id);

void poison_player(struct client *c, int damage);
void poison_npc(struct npc_spawn *npc, int damage);
void cure_poison_player(struct client *c, int immunity_time);
void freeze_player(struct client *c, int type, int duration);
int deal_damage_to_player(struct client *c, int amount, int damage_type,
						  int delay, int effect, int is_player, int index);
int deal_damage_to_npc(struct npc_spawn *npc, int amount, int delay, 
					   int effect, int is_player, int index);
void heal_player(struct client *c, int heal_amount);
void restore_run_energy(struct client *c, int restore_amount);
void restore_skill_points(struct client *c, int skill_id, int restore_amount);

void send_combat_xp(struct client *c, int damage_done);
void send_magic_xp(struct client *c, int damage_done, int spell_id);

int is_player_using_magic_attacks(struct client *c);
int is_player_using_ranged_attacks(struct client *c);
int is_player_using_melee_attacks(struct client *c);
void draw_ranged_weapon(struct client *c);

void player_attack_npc(struct client *c, struct npc_spawn *npc);
int is_npc_is_within_combat_range_with_player(struct client *c, 
										   struct npc_spawn *npc);
void npc_move_to_player_target(struct client *c, struct npc_spawn *npc);
void npc_attack_player(struct client *c, struct npc_spawn *npc);
#endif
