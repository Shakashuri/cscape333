#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "misc.h"

int get_thieving_level_needed(int thieving_id);
float get_thieving_xp(int thieving_id);
int get_thieving_pickpocket_chance_base(int thieving_id);
int get_thieving_pickpocket_chance_max(int thieving_id);
int get_thieving_pickpocket_damage(int thieving_id);
int get_thieving_pickpocket_stun_duration(int thieving_id);
void get_thieving_fail_text(int thieving_id, char *name, char *return_msg, 
							int size);
int get_thieving_stall_respawn_ticks(int thieving_id);
int get_thieving_stall_empty_id(int stall_id);
float get_thieving_chest_trap_max_damage(int thieving_id);
int get_thieving_chest_respawn_ticks(int thieving_id);
int get_thieving_lockpick_needed(int thieving_id);
int get_thieving_lockpick_chance_base(int thieving_id);
int get_thieving_lockpick_chance_max(int thieving_id);


void activate_chest_trap(struct client *c, int chest_type);
void init_thieving(struct client *c);
void handle_thieving(struct client *c);

