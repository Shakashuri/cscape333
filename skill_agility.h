#ifndef _SKILL_AGILITY_H_
#define _SKILL_AGILITY_H_

/**
 * Contains the agility info for an individual stage in an agility obstacle.
 */
struct agility_obstacle_stage
{
	/** X offset used for stage.  */
	int x;
	/** Y offset used for stage.  */
	int y;
	/** Z offset used for stage.  */
	int z;
	/** If stage is failable or not.  */
	int failable;
	/** Emote played when the stage is won. Not really used expect for ladder
	 * obstacles. @todo Remove this, make this a value in the 
	 * agility_obstacle_data struct instead.  */
	int win_emote;
	/** Emote played when the stage is lost. 
	 * @todo Remove this, make this a value in the 
	 * agility_obstacle_data struct instead. Don't need multiple of this as
	 * far as I know.  */
	int lose_emote;
	/** If this is a walking type stage, what type is it.
	 * 1 - Normal movement with animation.
	 * 2 - Phase movement
	 * 3 - Phase movement incremental
	 * 4 - Teleport
	 */
	int is_walk;
	/** Emote to replace the normal walking animation with.  */
	int walk_emote;
	/** Emote to replace the normal standing animation with.  */
	int stand_emote;
	/** If this stage uses phase movement, what is the delay before it starts.
	 * Uses client timing, not in server ticks.  */
	int phase_delay;
	/** If this stage uses phase movement, at what time should it end.
	 * Uses client timing, not in server ticks.  */
	int phase_time;
	/** Number of ticks that this stage lasts for.  */
	int ticks;
};

/**
 * Stores data for an agility obstacle.
 */
struct agility_obstacle_data
{
	/** If this is storing data for an obstacle, or a course.  */
	int is_course;
	/** Object ID of the obstacle used.  */
	int id;
	/**@{*/
	/** World coordinates of the object.  */
	int world_x;
	int world_y;
	int world_z;
	/**@}*/
	/** XP gained on completion. If this is a course, it is the bonus XP given
	 * when all obstacles have been done successfully.  */
	float xp;
	/** XP given if the obstacle was failed.  */
	float xp_on_failure;
	/** Agility level needed to use the obstacle.  */
	int level_needed;
	/** Type of the obstacle. AGILITY_WALK or AGILITY_LADDER.  */
	int obstacle_type;
	/** What direction to turn the player before doing the obstacle.  */
	int turn;
	/** Used with the use_x_two coordinates, alternate turning direction to
	 * use if the player is at the second usable zone. Used for obstacles that
	 * can be activated from each side of it, such as stiles.  */
	int turn_two;
	/**@{*/
	/** World coordinates the player has to be at to use the obstacle.  */
	int use_x;
	int use_y;
	int use_x_two;
	int use_y_two;
	/**@}*/
	/** Emote to play before processing.  */
	int start_emote;
	/** Number of stages this obstacle has.  */
	int num_stages;

	/** Stores the data for each stage of the obstacle.  */
	struct agility_obstacle_stage stages[AGILITY_MAX_STAGES];

	/** ID of the course this obstacle is a part of.  */
	int course_id;
	/** Index of the course this obstacle is part of.  */
	int course_index;
	/** ID of the obstacle in the course. Used for tracking which obstacles in
	 * the course have and have not been done yet.  */
	int course_obstacle_id;
	/** If this obstacle is a duplicate of another one in the course.  */
	int course_is_duplicate_obstacle;
	/** If this obstacle is the last one in the course.  */
	int is_final_obstacle;

	/** Message sent to the player when starting the obstacle.  */
	char start_message[200];
	/** Message sent to the player when winning the obstacle.  */
	char win_message[200];
	/** Message sent to the player when losing the obstacle.  */
	char lose_message[200];
	
	/** Type of failure, not currently used.  */
	int fail_type;
	/** Ticks it takes for failure to be handled.  */
	int fail_ticks;
	/**@{*/
	/** Coordinates to teleport the player to on failure.  */
	int fail_x;
	int fail_y;
	int fail_z;
	/**@}*/
	/** Chance out of 255 to succeed at level 1 agility, uchar method.  */
	int win_base_chance;
	/** Chance out of 255 to succeed at level 99 agility, uchar method.  */
	int win_max_chance;
	/** Damage dealt to the player if they fail. If less than 1, it is a 
	 * percentage. If 1 or greater, it is a random number from 1 to this
	 * number.  */
	float dmg;
};

int get_agility_obstacle_index(int id, int pos_x, int pos_y, int pos_z);
int get_agility_level_needed(int index);
int get_agility_course_obstacle_count(int index);
float get_agility_xp(int index);
float get_agility_course_xp(int index);
int is_player_at_agility_position(struct client *c);

void reset_agility_course_progress(struct client *c);
int is_agility_course_complete(struct client *c);
void init_agility(struct client *c);
void handle_agility_obstacle(struct client *c);

#endif
