/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file server.c
 * @brief Contains int main, start of the server.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>

#include <netdb.h>

#include "constants.h"

#include "action_queue.h"
#include "client.h"
#include "combat.h"
#include "debug.h"
#include "fileio.h"
#include "item.h"
#include "item_handler.h"
#include "login.h"
#include "misc.h"
#include "npc.h"
#include "npc_handler.h"
#include "object.h"
#include "object_handler.h"
#include "packet.h"
#include "packet_handler.h"
#include "player.h"
#include "regions.h"
#include "rng.h"
#include "server.h"
#include "shops.h"
#include "skills.h"
#include "stream.h"

/**@{*/
/** Server configuration data, sane defaults but to be changed when config
 * is loaded.  */
/* Latest version of the server config file.  */
int config_version = 0;
char server_name[CFG_SERVER_NAME_SIZE] = {"CScape"};
char server_port[CFG_SERVER_PORT_SIZE] = {"43594"};
float server_xp_multiplier = 1.0;
/* Player behavior configuration.  */
int player_unlimited_stamina = 0;
/* NPC behavior configuration.  */
int npc_random_walk_chance = 6;
/* Combat configuration.  */
int combat_arrow_break_chance = 25;
int combat_pvp_everywhere = 0;
/* Skills configuration.  */
int skills_cooking_base_burn_chance = 55;
int skills_cooking_range_burn_mod = 3;
int skills_cooking_lumbridge_range_burn_mod = 5;
int skills_cooking_enable_time_increase = 1;
int skills_cooking_max_flour_in_bin = 30;
int skills_mining_ore_depletion_method = 0;
int skills_mining_rune_essence_method = 0;
/* Shops configuration.  */
int shops_restock_ticks = 100;
/* Music config.  */
int music_show_song_unlock_message = 0;
/**@}*/

/* Statistic tracking.  */
int CURRENT_PLAYERS = 0;
int CURRENT_ONDEMAND_CONNECTIONS = 0;
int CURRENT_MUSIC_CONNECTIONS = 0;
int CURRENT_TICK = 0;
/* Number of interfaces.  */
int INTERFACES_SIZE = 0;
struct parent_interface *INTERFACES = NULL;
/* Number of regions in the cache.  */
struct region_data **REGION_LIST = NULL;
int REGION_LIST_NUM = 0;

struct region_data ***REGION_MAP = NULL;
/**@{*/
/** X and Y sizes of the REGION_MAP array.  */
int REGION_MAP_SIZE_X = 0;
int REGION_MAP_SIZE_Y = 0;
/**@}*/


/**@{*/
/** Flour bin locations, data.
 * @todo Move this to configuration.
 */
int flour_bin_x[] = {3166, 2405, 2632, 3139};
int flour_bin_y[] = {3306, 4457, 3385, 3450};
int flour_bin_face[] = {0, 0, 0, 2};
int flour_bin_num = PROTOCOL_MAX_FLOUR_BINS;
/**@}*/

/**@{*/
/** How fast the projectile travels based on distance from target.
 * The higher the number, the slower the projectile moves.
 * @todo These need modification, speeds need tweaking after the first squares
 * of distance.
 */
int ranged_arrow_speed[11] ={70, 70, 70, 75, 75, 75, 80, 80, 80, 85, 85};
												/* Fix these ones on.  */
												/* .......			.	*/
int magic_attack_speed[11] = {70, 70, 70, 75, 75, 75, 80, 80, 80, 85, 85};
/**@}*/

/* Holds calculated xp values, see init_xp_table().  */
int XP_FOR_LEVEL[100] = {0};
int BIT_MASKS[32] = {0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095,
					 8191, 16383, 32767, 65535, 131071, 262143, 524287,
					 1048575, 2097151, 4194303, 8388607, 16777215, 33554431,
					 67108863, 134217727, 268435455, 536870911, 1073741823, 
					 -1};

char PLAYER_BONUS_NAMES[12][7] = {"Stab", "Slash", "Crush", "Magic", "Range",
								  "Stab", "Slash", "Crush", "Magic", "Range",
								  "Str", "Prayer"};

/* Conversion from spell data tables to what rune id it needs.  */
int RUNE_IDS[PROTOCOL_NUM_RUNES] = {556, 557, 555, 554, 558, 559, 564, 562, 
									561, 563, 560, 0, 565, 566};

char SKILL_NAMES[PROTOCOL_NUM_SKILLS][20] = {"Attack", "Defence", "Strength",
									"Hitpoints", "Ranged", "Prayer",
									"Magic", "Cooking", "Woodcutting",
									"Fletching", "Fishing", "Firemaking",
									"Crafting", "Smithing", "Mining",
									"Herblore", "Agility", "Thieving",
									"Slayer", "Farming", "Runecrafting"};

struct update_stream *UPDATE_BLOCK;
struct client *CONNECTED_CLIENTS[MAX_PLAYERS] = {NULL};
struct ondemand_client *CONNECTED_ONDEMAND_CLIENTS[MAX_PLAYERS] = {NULL};
struct music_client *CONNECTED_MUSIC_CLIENTS[MAX_PLAYERS] = {NULL};
struct item *ITEMS[PROTOCOL_MAX_ITEMS] = {NULL};
struct object *OBJECTS[PROTOCOL_MAX_OBJECTS] = {NULL};
struct npc *NPCS[PROTOCOL_MAX_NPCS] = {NULL};
struct spell **SPELLS = NULL;
int NUM_SPELLS = 0;
struct loot_table **LOOT_TABLES = 0;
int NUM_LOOT_TABLES = 0;
int RDT_GEM_WEALTH_IDX = 0;
int RDT_MEGA_WEALTH_IDX = 0;
int MINING_PRECIOUS_GEM_TABLE = 0;
int MINING_GEM_ROCK_TABLE = 0;

/** Keeps track of ticks since the last restocking of shops.  */
int SHOP_RESTOCK_TIMER = 0;

struct shop_info **SHOP_DATA = NULL;
int NUM_SHOPS = 0;
int NUM_AGILITY_OBSTACLES = 0;
struct agility_obstacle_data **AGILITY_DATA = NULL;
struct quest_info **QUEST_DATA = NULL;
struct music_info **MUSIC_DATA = NULL;

/* TODO: Change these to be 2D arrays, [region_id][drop_num].  */
struct item_drop *ITEM_DROPS[MAX_ITEM_DROPS] = {NULL};
struct obj_spawn *OBJECT_SPAWNS[MAX_OBJECT_SPAWNS] = {NULL};
struct npc_spawn **NPC_SPAWNS = NULL;
int NUM_NPC_SPAWNS = 0;

/** Socket id of the main "listener" socket.  */
int LISTENER;
/** Highest socket id being used.  */
int FDMAX;
/** Socket id of the one being used right now.  */
int NEWFD;
/** Master file descriptor list.  */
fd_set MASTER;
/** Temp file descriptor list for select().  */
fd_set READ_FDS;

/**
 * Checks all the sockets for new connections, and loads them into the game.
 *
 * @returns 0 on success, 1 on failure.
 */
int handle_new_connections(void)
{
	struct sockaddr_storage remoteaddr;
	printf("new conn\n");
	/* Handle new connections.  */
	socklen_t addrlen = sizeof(remoteaddr);
	NEWFD = accept(LISTENER, (struct sockaddr *) &remoteaddr,
		&addrlen);

	if (NEWFD == -1) 
	{
		perror("accept");
	} 
	else 
	{
		int z;
		int bytes_recv;
		char connect_type[5];

		FD_SET(NEWFD, &MASTER); /* Add to master set.  */
		
		if (NEWFD > FDMAX) 
		{ 
			/* Keep track of the max.  */
			FDMAX = NEWFD;
		}
		
		/* Check what the connection type is.  */	
		bytes_recv = recv(NEWFD, connect_type, 2, 0);
		
		/* Just drop Ondemand clients for now. 
		   TODO: Figure out how to stop the client from
		   creating unlimited amounts.  */
		if (bytes_recv < 1 || connect_type[0] == 15)
		{
			close(NEWFD);
			FD_CLR(NEWFD, &MASTER);
			return 1;
		}

		/* Normal player login.  */
		if (connect_type[0] == 14)
		{
			/* See which player_clients are empty.  */
			/* Handle connected clients and put them in
			   the structs for later use.  */
			for (z = 0; z < MAX_PLAYERS; z++)
			{
				if (CONNECTED_CLIENTS[z] == NULL)
				{
					CONNECTED_CLIENTS[z] 
						= load_connection(NEWFD);

					if (CONNECTED_CLIENTS[z] != NULL)
					{
						if (handle_login(CONNECTED_CLIENTS[z], z, 0) == 0)
						{
							CONNECTED_CLIENTS[z]->login_id = z;
						}
						else
						{
							CONNECTED_CLIENTS[z] = NULL;
						}
					}
					else
					{
						printf("Client instance failed to be setup properly!\n");
					}
					break;
				}
			}
		}
		else if (connect_type[0] == 'a')
		{
			for (z = 0; z < MAX_PLAYERS; z++)
			{
				if (CONNECTED_MUSIC_CLIENTS[z] == NULL)
				{
					CONNECTED_MUSIC_CLIENTS[z] = load_music_connection(NEWFD);

					if (CONNECTED_MUSIC_CLIENTS[z] != NULL)
					{

						if (handle_music_login(CONNECTED_MUSIC_CLIENTS[z], z, 0) == 0)
						{
							//CURRENT_MUSIC_CONNECTIONS++;
							printf("Total music connections: %d\n",
									CURRENT_MUSIC_CONNECTIONS);
						}
						else
						{
							printf("Totally fuggered m8\n");
							CONNECTED_MUSIC_CLIENTS[z] = NULL;
						}
					}
					else
					{
						printf("Music client instance failed to be setup properly!\n");
					}
					break;
				}
			}
		}
	}

	return 0;
}

/**
 * Goes through all existing connections and handles any new data they
 * may have sent.
 *
 * @param client_connection_socket Socket ID of the client to process data
 * from.
 */
void handle_client_data(int client_connection_socket)
{
	/* Handle data from a client.  */
	int client_used = get_client_socket(client_connection_socket); 
	int music_client_used = get_music_client_socket(client_connection_socket);

	int nbytes = 0;

	if (client_used != -1)
	{
		/* TODO: Rewrite this to make sure data for all the packets is recv'd.
		 * This may end up not getting complete packets as it is. This isn't
		 * quite a problem at the momemnt, but isnt' good.  */
		if ((nbytes = recv(CONNECTED_CLIENTS[client_used]->socket, 
			&CONNECTED_CLIENTS[client_used]->instream, 
			BUFFER_SIZE -1, 0)) 
				<= 0) 
		{
			/* Got error or connection closed by client.  */
			if (nbytes <= 0) 
			{
				CONNECTED_CLIENTS[client_used]->bytes_read = 0;

				if (CONNECTED_CLIENTS[client_used]->plr->logged_out != 1)
				{
					CONNECTED_CLIENTS[client_used]->plr->unclean_logout = 1;
				}
			}
			
			close(CONNECTED_CLIENTS[client_used]->socket);
			FD_CLR(CONNECTED_CLIENTS[client_used]->socket, 
				   &MASTER);
			
			clean_up_connection(client_used);
		} 
		else 
		{
			CONNECTED_CLIENTS[client_used]->bytes_read = nbytes;
		}
	}
	else if (music_client_used != -1)
	{
		if ((nbytes = recv(CONNECTED_MUSIC_CLIENTS[music_client_used]->socket, 
			&CONNECTED_MUSIC_CLIENTS[music_client_used]->instream, 
			MUSIC_BUFFER_SIZE -1, 0)) 
				<= 0) 
		{
			close(CONNECTED_MUSIC_CLIENTS[music_client_used]->socket);
			FD_CLR(CONNECTED_MUSIC_CLIENTS[music_client_used]->socket, 
				   &MASTER);
			CURRENT_MUSIC_CONNECTIONS--;

			if (CONNECTED_MUSIC_CLIENTS[music_client_used] != NULL)
			{
				free(CONNECTED_MUSIC_CLIENTS[music_client_used]);
				CONNECTED_MUSIC_CLIENTS[music_client_used] = NULL;
			}
		}
		else
		{
			CONNECTED_MUSIC_CLIENTS[music_client_used]
			->bytes_read = nbytes;
			for (int z = 0; z < 20; z++)
			{
				CONNECTED_MUSIC_CLIENTS[music_client_used]->instream_offset = 0;
			}
		}
	}
	else
	{
		printf("No socket client corresponds to %d\n", client_used);
		close(client_connection_socket);
		FD_CLR(client_connection_socket, &MASTER);
	}

}
/**
 * You should know what main is. The start of the whole program.
 * @return Returns 0 when closed normally, anything else on error.
 */
int main(void)
{
	int i = 0, x = 0, rv = 0;
	/* Listening socket descriptor.  */
	/* Struct used by select so that there isn't any delay.  */
	struct timeval tv = {0, 0};
	struct addrinfo hints, *ai, *p;
	
	initialize_rng(time(NULL));

	printf("Starting up CScape: 333 protocol!\n");
	
	/* Need to load configuration before network setup.  */
	if (load_config_data() == 1)
	{
		printf("Make sure that you copy the file data/server.conf.default\n"
			   "to data/server.conf!\n");
		printf("Falling back to default settings.\n");
	}

	FD_ZERO(&MASTER);/* clear the master and temp sets.  */
	FD_ZERO(&READ_FDS);

	/* Get us a socket and bind it.  */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	if ((rv = getaddrinfo(NULL, server_port, &hints, &ai)) != 0) 
	{
		fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
		exit(1);
	}
	
	for(p = ai; p != NULL; p = p->ai_next) 
	{

		LISTENER = socket(p->ai_family, p->ai_socktype, p->ai_protocol);

		if (LISTENER < 0) 
		{ 
			continue;
		}
		
		/* Lose the pesky "address already in use" error message.  */
		/*
		if (setsockopt(LISTENER, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		{
			perror("setsockopt");
			exit(3);
		}
		*/

		if (bind(LISTENER, p->ai_addr, p->ai_addrlen) < 0) 
		{
			close(LISTENER);
			continue;
		}

		break;
	}

	/* If we got here, it means we didn't get bound.  */
	if (p == NULL) 
	{
		fprintf(stderr, "selectserver: failed to bind\n");
		fprintf(stderr, "Make sure you don't have any other instances of\n"
				"CScape open, or try restarting the game client.\n");
		exit(2);
	}

	freeaddrinfo(ai); /* All done with this.  */

	/* Listen.  */
	if (listen(LISTENER, 10) == -1) 
	{
		perror("listen");
		exit(3);
	}

	/* Add the LISTENER to the master set.  */
	FD_SET(LISTENER, &MASTER);

	/* Keep track of the biggest file descriptor.  */
	FDMAX = LISTENER; /* So far, it's this one.  */
	
	/* Setup ctrl-c handler.  */
	signal(SIGINT, shutdown_server);
	/* Ignore sigpipe, needs further testing on handling.  */
	signal(SIGPIPE, SIG_IGN);

	/* Check if needed directories exist.  */
	if (is_directory_present("./data/") == 0)
	{
		printf("Creating ./data/ folder, did not exist.\n");
		create_dir("./data/");
		printf("Data directory did not exist on load, expect file load"
			   " errors.\n");
		printf("Make sure you have copied over the data directory to the"
			   " server folder, it has data the server requires!\n");
		return 1;
	}

	if (is_directory_present("./data/players/") == 0)
	{
		printf("Creating ./data/players/ folder, did not exist.\n");
		create_dir("./data/players/");
	}
	
	/* Initialize data needed.  */
	init_xp_table();
	load_interfaces();
	load_region_list();
	
	if (REGION_LIST_NUM == 0)
	{
		printf("REGION_LIST_NUM did not get set!\n");
		return 1;
	}
	if (REGION_LIST == NULL)
	{
		printf("REGION_LIST did not get calloc'd correctly!\n");
		return 1;
	}

	/* Malloc data arrays.  */
	UPDATE_BLOCK = malloc(sizeof(struct update_stream));

	/* Load map data first, as some later stuff relies on it.  */
	load_map_data();

	/* Loads data from files that are needed for the server.  */
	load_item_definitions();
	load_item_spawns();
	load_spell_data();
	load_loot_table_definitions();
	load_npc_definitions();
	load_npc_spawns();
	load_object_definitions();
	load_object_spawns();
	load_shop_data();
	load_quest_data();
	load_music_data();
	load_agility_data();

	/* Test regions for music.  */
	/* FIXME: Move this to its own function.  */
	for (x = 0; x < REGION_LIST_NUM; x++)
	{
		if (REGION_LIST[x]->music_id == -1 && REGION_LIST[x]->has_music == 1)
		{
			printf("ERROR: Region %5d doesn't have any music X: %d Y: %d\n",
				   REGION_LIST[x]->id, REGION_LIST[x]->world_x, 
				   REGION_LIST[x]->world_y);
		}
	}

	printf("%s now online on port %s.\n\n", server_name, server_port);

	/* Main loop.  */
	for(;;) 
	{
		/* May need to change this so that it compensates for varying
		   time taken to execute the main loop.  */
		
		/* TODO: Remove the need for the sleep call here, rewrite this so it
		   isn't needed, better method.  */
		struct timeval sleep_time = {0, (TICKRATE * MICRO_CONV)};
		select(0, NULL, NULL, NULL, &sleep_time);
		CURRENT_TICK++;

		READ_FDS = MASTER; /* Copy it.  */

		if (select(FDMAX + 1, &READ_FDS, NULL, NULL, &tv) < 0) 
		{
			if (errno == EINTR)
			{
				/* printf("Select system call, continue.\n"); */	
				continue;
			}
			else
			{
				perror("select");
				exit(4);
			}
		}

		/* Run through the existing connections looking for data to read.  */
		for (i = 0; i <= FDMAX; i++) 
		{
			if (FD_ISSET(i, &READ_FDS))
			{ 
				/* New client connection.  */
				if (i == LISTENER) 
				{
					if (!handle_new_connections())
					{
						continue;
					}
				} 
				else 
				{
					handle_client_data(i);
				}
			}
			else
			{
				continue;
			}
		}

		process_all_shops();
		process_all_items();
		process_all_objects();

		process_all_npcs();
		process_all_players();
		process_all_music_clients();
	}
	
	shutdown_server(0);
	return 0;
}

/**
 * Handle restocking all shops when they fall under their original amount
 * of an item.
 *
 * Will restock one at a time, or 1% of the original stock, whatever is higher.
 */
void process_all_shops(void)
{
	if (SHOP_RESTOCK_TIMER < shops_restock_ticks)
	{
		SHOP_RESTOCK_TIMER++;
		return;
	}

	SHOP_RESTOCK_TIMER = 0;
	int x = 0, y = 0, d = 0, changed_stock = 0;

	for (x = 0; x < NUM_SHOPS; x++)
	{
		for (y = 0; y < SHOP_SIZE; y++)
		{
			int orig_stock = SHOP_DATA[x]->orig_items_n[y];
			int cur_stock = SHOP_DATA[x]->items_n[y];
			int diff_stock = (orig_stock - cur_stock);
			int to_add = (orig_stock * 0.01);

			/* Restock the item if it is lower than it should be.  */
			if (cur_stock < orig_stock)
			{
				if (1 > to_add)
				{
					SHOP_DATA[x]->items_n[y]++;
				}
				else
				{
					if (to_add > diff_stock)
					{
						to_add = diff_stock;
					}

					SHOP_DATA[x]->items_n[y] += to_add;
				}

				/* If the store doesn't have any of them, bring it back.  */
				if (SHOP_DATA[x]->items[y] != SHOP_DATA[x]->orig_items[y])
				{
					SHOP_DATA[x]->items[y] = SHOP_DATA[x]->orig_items[y];
				}

				changed_stock = 1;
			}
		}

		/* Refresh the shop window of all players with the shop currently open.
		 */
		if (changed_stock == 1)
		{
			for (d = 0; d < MAX_PLAYERS; d++)
			{
				if (CONNECTED_CLIENTS[d] != NULL)
				{
					if (CONNECTED_CLIENTS[d]->login_state == -1 
						&& CONNECTED_CLIENTS[d]->plr->unclean_logout != 1)
					{
						if (CONNECTED_CLIENTS[d]->plr->interface_open_sub == x)
						{
							packet_reload_items_in_shop_interface(CONNECTED_CLIENTS[d], x);
						}
					}
				}
			}
			changed_stock = 0;
		}
	}
}

/**
 * @brief Loops through all item drops in the server, preforms updates on them
 * if needed.
 */
void process_all_items(void)
{
	int x = 0;

	for (x = 0; x < MAX_ITEM_DROPS; x++)
	{
		/* If an object actually exists.  */
		if (ITEM_DROPS[x]->world_x != 0)
		{
			/* Handle items that respawn after being picked up.  */
			if (ITEM_DROPS[x]->tick_picked_up > 0)
			{
				ITEM_DROPS[x]->tick_picked_up--;
			}
			else if (ITEM_DROPS[x]->tick_picked_up == 0)
			{
				spawn_item_globally(x);
			}
			/* Item despawn handling (dropped items).  */	
			if (ITEM_DROPS[x]->tick_dropped > 0)
			{
				ITEM_DROPS[x]->tick_dropped--;

				if (ITEM_DROPS[x]->tick_dropped 
					== (LOOT_DESPAWN_TICKS - LOOT_SHOW_OTHERS))
				{
					/* If a player has 'ownership' of an item, spawn it for 
					   everyone except him to ensure no double item 
					   spawning, but only if the item can be traded.  */
					if (ITEMS[ITEM_DROPS[x]->id]->tradable == 1)
					{
						spawn_item_globally_except_owner(x);
					}
				}
			}
			else if (ITEM_DROPS[x]->tick_dropped == 0)
			{
				despawn_item_globally(x);
			}
			/* Process items that will only show up to others after a
			 * set delay.  */
			if (ITEM_DROPS[x]->tick_spawn_delay > 0)
			{
				ITEM_DROPS[x]->tick_spawn_delay--;
			}
			else if (ITEM_DROPS[x]->tick_spawn_delay == 0)
			{
				int player_index = ITEM_DROPS[x]->player_index;

				if (player_index != -1)
				{
					spawn_item_on_ground(CONNECTED_CLIENTS[player_index], x);
				}
				else
				{
					spawn_item_globally(x);
				}
				ITEM_DROPS[x]->tick_spawn_delay = -1;
			}
		}
	}
}

/**
 * @brief Loops through all object spawns in the server, preforms updates on
 * them if needed.
 */
void process_all_objects(void)
{
	int x;
	for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
	{
		/* If the object exists.  */
		if (OBJECT_SPAWNS[x]->world_x != 0)
		{
			if (OBJECT_SPAWNS[x]->transform_ticks > 0)
			{
				OBJECT_SPAWNS[x]->transform_ticks--;
			}
			else if (OBJECT_SPAWNS[x]->transform_ticks == 0)
			{
				OBJECT_SPAWNS[x]->id = OBJECT_SPAWNS[x]->spawn_id;
				OBJECT_SPAWNS[x]->transform_ticks = -1;
				reset_object_globally(x);
				spawn_object_globally(x);
			}
			if (OBJECT_SPAWNS[x]->spawn_ticks > 0)
			{
				OBJECT_SPAWNS[x]->spawn_ticks--;
			}
			else if (OBJECT_SPAWNS[x]->spawn_ticks == 0)
			{
				spawn_object_globally(x);
				OBJECT_SPAWNS[x]->spawn_ticks = -1;
			}
			if (OBJECT_SPAWNS[x]->cooldown_ticks > 0)
			{
				OBJECT_SPAWNS[x]->cooldown_ticks--;
			}
			if (OBJECT_SPAWNS[x]->cooldown_ticks == 0)
			{
				OBJECT_SPAWNS[x]->cooldown_ticks = -1;
			}
			if (OBJECT_SPAWNS[x]->despawn_ticks > 0)
			{
				OBJECT_SPAWNS[x]->despawn_ticks--;
			}
			else if (OBJECT_SPAWNS[x]->despawn_ticks == 0)
			{
				delete_from_object_list(x, OBJECT_SPAWNS[x]->despawn_globally);
			}
		}
	}
}

/**
 * @brief Loops through all instanced object spawns for a certain player, and
 * updates them if needed.
 * @param *c Client instance.
 */
void process_all_inst_objects(struct client *c)
{
	int x;
	if (c->plr->inst_objects != NULL)
	{
		for (x = 0; x < c->plr->inst_objects_size; x++)
		{
			if (c->plr->inst_objects[x] == NULL)
			{
				continue;
			}
			/* If the object exists.  */
			if (c->plr->inst_objects[x]->world_x != 0)
			{
				if (c->plr->inst_objects[x]->transform_ticks > 0)
				{
					c->plr->inst_objects[x]->transform_ticks--;
				}
				else if (c->plr->inst_objects[x]->transform_ticks == 0)
				{
					c->plr->inst_objects[x]->id 
						= c->plr->inst_objects[x]->spawn_id;
					c->plr->inst_objects[x]->transform_ticks = -1;
					c->plr->inst_objects_on_ground[x] = 0;
					spawn_inst_object_on_ground(c, x);
				}
				if (c->plr->inst_objects[x]->spawn_ticks > 0)
				{
					c->plr->inst_objects[x]->spawn_ticks--;
				}
				else if (c->plr->inst_objects[x]->spawn_ticks == 0)
				{
					spawn_inst_object_on_ground(c, x);
					c->plr->inst_objects[x]->spawn_ticks = -1;
				}
				if (c->plr->inst_objects[x]->cooldown_ticks > 0)
				{
					c->plr->inst_objects[x]->cooldown_ticks--;
				}
				if (c->plr->inst_objects[x]->cooldown_ticks == 0)
				{
					c->plr->inst_objects[x]->cooldown_ticks = -1;
				}
				if (c->plr->inst_objects[x]->despawn_ticks > 0)
				{
					c->plr->inst_objects[x]->despawn_ticks--;
				}
				else if (c->plr->inst_objects[x]->despawn_ticks == 0)
				{
					remove_inst_object(c, x, 
						c->plr->inst_objects[x]->despawn_globally);
				}
			}
		}
	}

	return;
}

/**
 * Handle processing of what state a player is in.
 * @param *c Client instance.
 */
void process_player_states(struct client *c)
{
	if (c->plr->state == STATE_ACTION)
	{
		if (c->plr->state_type == STATE_PICKUP_ITEM)
		{
			scan_for_pickup(c);
		}
		else if (c->plr->state_type == STATE_OBJECT_ACTIVATE
				 || c->plr->state_type == STATE_USE_ITEM_ON_OBJECT)
		{
			handle_objects(c, c->plr->action_id);
		}
		else if (c->plr->state_type == STATE_NPC_ACTIVATE)
		{
			handle_npc_interactions(c); 
		}
		else if (c->plr->state_type == STATE_USE_ITEM_ON_NPC)
		{
			handle_item_on_npc(c);
		}
		else if (c->plr->state_type == STATE_GROUND_ITEM_ACTIVATE)
		{
			handle_item_on_ground(c, c->plr->action_item_id, 
								  c->plr->action_x, c->plr->action_y);
		}
		else if (is_player_state_type_skilling(c))
		{
			player_process_skills(c);
		}
	}
	else if (c->plr->state == STATE_ACTION_QUEUE_HANDLING)
	{
		process_action_queue(c);
	}
	else if (c->plr->state == STATE_ATTACK)
	{
		if (c->plr->targeted_npc != -1)
		{
			/* Needs a LOT of work still.  */
			player_attack_npc(c, NPC_SPAWNS[c->plr->targeted_npc]);
		}
		else if (c->plr->targeted_player != -1)
		{
			packet_send_chatbox_message(c, "You can't attack that!\n");
		}
	}

	/* Check if the player's target has moved, and if so, start moving the 
	 * player to their new location.  */
	player_check_if_movement_needs_updating(c);
	player_move_to_destination_point(c);
}

/**
 * @brief Loops through all players in the server, updates their data as
 * needed.
 */
void process_all_players(void)
{
	int d, x;
	/* Main loop that checks, processes all client data.  */
	for (d = 0; d < MAX_PLAYERS; d++)
	{
		if (CONNECTED_CLIENTS[d] != NULL)
		{
			/* If the player is loging in, handle them at each stage.  */
			if (CONNECTED_CLIENTS[d]->login_state != -1)
			{
				/* If this returned a one, then something went wrong and we
				 * need to clean up.  */
				if (CONNECTED_CLIENTS[d]->login_delay-- <= 0)
				{
					int sock = CONNECTED_CLIENTS[d]->socket;
					if (handle_login(CONNECTED_CLIENTS[d], 
								 CONNECTED_CLIENTS[d]->login_id, 
								 CONNECTED_CLIENTS[d]->login_state) == 1)
					{
						FD_CLR(sock, &MASTER);
						close(sock);
						CONNECTED_CLIENTS[d] = NULL;
						continue;
					}
					/* If login state was just changed to -1, finish 
					 * initialization.  */
					if (CONNECTED_CLIENTS[d]->login_state == -1)
					{
						initialize_login_stats(CONNECTED_CLIENTS[d]);
						initialize_new_login(CONNECTED_CLIENTS[d]);
						printf("Total players online: %d\n", 
							   CURRENT_PLAYERS);
						/* Prevent incorrect reading of packets right
						 * when login is complete.  */
						CONNECTED_CLIENTS[d]->bytes_read = 0;
					}
				}
				continue;
			}
			/* Handle player that disconnected adruptly.  */
			if (CONNECTED_CLIENTS[d]->plr->unclean_logout == 1)
			{
				clean_up_connection(d);
				continue;
			}
			
			handle_packets(CONNECTED_CLIENTS[d]);
			player_move_to_destination_point(CONNECTED_CLIENTS[d]);

			/* Handle all player stats that need to be decremented.  */
			player_process_stats(CONNECTED_CLIENTS[d]);

			/* Spawn any new items or objects needed when entering a new map
			   region.  */
			if (CONNECTED_CLIENTS[d]->plr->map_region_changed == 1
				|| CONNECTED_CLIENTS[d]->plr->teleported == 1)
			{
				/* Update items.  */
				for (x = 0; x < MAX_ITEM_DROPS; x++)
				{
					if (ITEM_DROPS[x]->world_x != 0 
						&& CONNECTED_CLIENTS[d]->initialized != 0)
					{
						despawn_item_on_ground(CONNECTED_CLIENTS[d], x);
						spawn_item_on_ground(CONNECTED_CLIENTS[d], x); 
					}
				}
				/* Update objects.  */
				for (x = 0; x < MAX_OBJECT_SPAWNS; x++)
				{
					if (OBJECT_SPAWNS[x]->world_x != 0 
						&& CONNECTED_CLIENTS[d]->initialized != 0)
					{
						CONNECTED_CLIENTS[d]->plr->objects_on_ground[x] = 0;
						spawn_object_on_ground(CONNECTED_CLIENTS[d], x);
					}
				}

				/* Update instanced objects.  */
				if (CONNECTED_CLIENTS[d]->plr->inst_objects != NULL)
				{
					for (x = 0; 
						 x < CONNECTED_CLIENTS[d]->plr->inst_objects_size; 
						 x++)
					{
						if (CONNECTED_CLIENTS[d]->plr->inst_objects[x] != NULL)
						{
							CONNECTED_CLIENTS[d]->plr->inst_objects_on_ground[x]
								= 0;
							spawn_inst_object_on_ground(CONNECTED_CLIENTS[d], 
														x);
						}
					}
				}
			}

			process_player_states(CONNECTED_CLIENTS[d]);

			/* Various player timer handling.  */
			player_process_health_regen(CONNECTED_CLIENTS[d]);
			player_process_stat_regen(CONNECTED_CLIENTS[d]);
			player_process_stat_decay(CONNECTED_CLIENTS[d]);
			player_process_prayer_drain(CONNECTED_CLIENTS[d]);
			player_process_poison_damage(CONNECTED_CLIENTS[d]);

			/* Check if server has unlimited run energy enabled, if not, handle
			   running as usual.  */
			if (player_unlimited_stamina == 0)
			{
				player_process_run_energy(CONNECTED_CLIENTS[d]);
			}

			player_process_timed_actions(CONNECTED_CLIENTS[d]);
			player_process_timed_teleport(CONNECTED_CLIENTS[d]);
			player_process_damage(CONNECTED_CLIENTS[d]);
			player_process_death(CONNECTED_CLIENTS[d]);
			player_process_save(CONNECTED_CLIENTS[d]);
			player_process_music(CONNECTED_CLIENTS[d]);

			process_all_inst_objects(CONNECTED_CLIENTS[d]);
			process_aggressive_npcs(CONNECTED_CLIENTS[d]);		
			
			player_update_world_location(CONNECTED_CLIENTS[d]);
		}
	}
	/* This handles any player updating that needs to be done.  */
	for (d = 0; d < MAX_PLAYERS; d++)
	{
		if (CONNECTED_CLIENTS[d] == NULL)
		{
			continue;
		}
		else if (CONNECTED_CLIENTS[d]->is_active == 0)
		{
			continue;
		}
		else
		{
			player_update_viewport(CONNECTED_CLIENTS[d]);
			update_npc(CONNECTED_CLIENTS[d]);
			/* Do a quick check to make sure that the buffers haven't been
			 * written past their bounds.  */
			check_outstream_buffer_limit(CONNECTED_CLIENTS[d]);
			check_pprops_buffer_limit(CONNECTED_CLIENTS[d]);
			check_update_block_buffer_limit();

			send_out_stream(CONNECTED_CLIENTS[d]);	
		}
	}
	for (d = 0; d < MAX_PLAYERS; d++)
	{
		if (CONNECTED_CLIENTS[d] == NULL)
		{
			continue;
		}
		else if (CONNECTED_CLIENTS[d]->is_active == 0)
		{
			continue;
		}
		else
		{
			clear_player_update_flags(CONNECTED_CLIENTS[d]);
		}
	}
}

/**
 * Loops through all the connected music clients in the server, handles any
 * connection maintenance that needs to be done.
 */
void process_all_music_clients(void)
{
	int d;
	/* Main loop that checks, processes all client data.  */
	for (d = 0; d < MAX_PLAYERS; d++)
	{
		if (CONNECTED_MUSIC_CLIENTS[d] != NULL)
		{
			/* If the player is loging in, handle them at each stage.  */
			if (CONNECTED_MUSIC_CLIENTS[d]->login_state != -1)
			{
				/* If this returned a one, then something went wrong and we
				 * need to clean up.  */
				if (CONNECTED_MUSIC_CLIENTS[d]->login_delay-- <= 0)
				{
					int sock = CONNECTED_MUSIC_CLIENTS[d]->socket;
					if (handle_music_login(CONNECTED_MUSIC_CLIENTS[d], d, 
								 CONNECTED_MUSIC_CLIENTS[d]->login_state) == 1)
					{
						printf("close login socket music\n");
						FD_CLR(sock, &MASTER);
						close(sock);
						CONNECTED_MUSIC_CLIENTS[d] = NULL;
						continue;
					}
					/* If login state was just changed to -1, finish 
					 * initialization.  */
					if (CONNECTED_MUSIC_CLIENTS[d]->login_state == -1)
					{
						/*
						initialize_login_stats(CONNECTED_MUSIC_CLIENTS[d]);
						initialize_new_login(CONNECTED_MUSIC_CLIENTS[d]);
						*/
						printf("Total players online: %d\n", 
							   CURRENT_PLAYERS);
						/* Prevent incorrect reading of packets right
						 * when login is complete.  */
						CONNECTED_MUSIC_CLIENTS[d]->bytes_read = 0;
					}
				}
				continue;
			}
		}
	}
}

/**
 * @brief Loops through all NPC spawns in the server, updates their data if
 * needed.
 */
void process_all_npcs(void)
{
	int x;

	for (x = 0; x < NUM_NPC_SPAWNS; x++)
	{
		if (NPC_SPAWNS[x] != NULL)
		{
			/* These all process timers or things that are not affected by the
			 * NPC's AI state.  */
			process_npc_stats(NPC_SPAWNS[x]);
			clear_npc_update_flags(NPC_SPAWNS[x]);
			process_npc_damage(NPC_SPAWNS[x]);
			player_process_poison_damage_npc(NPC_SPAWNS[x]);
			process_npc_timed_teleport(NPC_SPAWNS[x]);	
			process_npc_transformation(NPC_SPAWNS[x]);	

			/* Process NPC AI.  */
			process_npc_ai(NPC_SPAWNS[x]);
		}
	}
}

/**
 * @brief When a player attacks an NPC, this processes the combat.
 * @param *c Client fighting the NPC.
 */
void process_combat_with_npc(struct client *c)
{
	if (c->plr->is_attacking == 1 && c->plr->targeted_npc != -1
		&& c->plr->death_stage == 0)
	{
		if (c->plr->last_attack == 0)
		{
			/* Needs a LOT of work still.  */
			player_attack_npc(c, NPC_SPAWNS[c->plr->targeted_npc]);

			/* Return if some error occured to prevent issues with next
			   functions.  
			   NOTE: Is this needed anymore?  */
			if (c->plr->targeted_npc == -1)
			{
				return;
			}
		}
		
		/* If the NPC runs out of health during battle.  */
		if (NPC_SPAWNS[c->plr->targeted_npc]->dead > 0)
		{
			reset_player_combat(c);
		}
	}
}

/**
 * @brief Shuts the server down, ensures all connections are closed and data
 * is free'd.
 * @param signum Signal number received, used for signal handling (ex: ^C).
 */
void shutdown_server(int signum)
{
	int x;
	/* Ensures that the ^C does not interfere with text readability.  */
	printf("\n");
	printf("Signal num %d received!\n", signum);

	/* Disconnect all current clients, then wait a second before actually
	 * closing the sockets. This prevents not being able to restart the server
	 * after forcibly closing it with multiple clients connected.  */
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			packet_player_disconnect(CONNECTED_CLIENTS[x]);
			send_out_stream(CONNECTED_CLIENTS[x]);

			if (CONNECTED_MUSIC_CLIENTS[CONNECTED_CLIENTS[x]->music_idx] != NULL)
			{
				packet_music_player_disconnect(CONNECTED_CLIENTS[x]);
				send_out_music_stream(CONNECTED_MUSIC_CLIENTS[CONNECTED_CLIENTS[x]->music_idx]);
			}
		}
	}

	sleep(2);

	/* Disonnect all music clients.  */
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_MUSIC_CLIENTS[x] != NULL)
		{
			printf("Disconnecting music client at %s.\n", 
						CONNECTED_MUSIC_CLIENTS[x]->ip_addr);
			close(CONNECTED_MUSIC_CLIENTS[x]->socket);
			free(CONNECTED_MUSIC_CLIENTS[x]);
			CONNECTED_MUSIC_CLIENTS[x] = NULL;
		}
	}

	/* Disconnect all players.  */
	for (x = 0; x < MAX_PLAYERS; x++)
	{
		if (CONNECTED_CLIENTS[x] != NULL)
		{
			printf("Disconnecting player %s.\n", 
					CONNECTED_CLIENTS[x]->plr->name);
			close(CONNECTED_CLIENTS[x]->socket);
			save_player_data(CONNECTED_CLIENTS[x]);
			free_player_data(x);
			CONNECTED_CLIENTS[x] = NULL;
		}
	}

	printf("Closing listener socket...\n");
	close(LISTENER);
	free_memory();
	exit(0);
}
