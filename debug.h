/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DEBUG_H_
#define _DEBUG_H_

void print_outstream(struct client *c, int limit);
void print_instream(struct client *c, int limit);
void print_update_blocks(int limit);
void print_player_props(struct client *c, int limit);
void print_player_last_packets(struct client *c);
void print_player_last_packets_unsigned(struct client *c);
void print_player_damage_log(struct client *c, int idx);
void print_npc_index_info(int idx);
void print_npc_ai_state(int idx);

void empty_instream(struct client *c);
void empty_outstream(struct client *c);
void empty_update(void);

void check_outstream_buffer_limit(struct client *c);
void check_pprops_buffer_limit(struct client *c);
void check_update_block_buffer_limit(void);

void check_object_data_for_errors(struct object *o, int count);
void check_item_data_for_errors(struct item *i, int count);
void check_item_spawn_data_for_errors(struct item_drop *i, int count);
void check_npc_data_for_errors(struct npc *n, int count);
void check_npc_spawn_data_for_errors(struct npc_spawn *n, int count);
void check_shop_data_for_errors(struct shop_info *s, int count);
void check_agility_data_for_errors(struct agility_obstacle_data *a, int count);
#endif

