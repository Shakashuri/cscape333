/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ISAAC_H_
#define _ISAAC_H_

/**
 * @brief ISAAC encryption state, holds seed, results, etc.
 */
struct isaac
{
	/** Number of ready random numbers left.  */
	uint32_t rand_cnt;
	/** Buffer holding random numbers generated.  */
	uint32_t rand_results[256];
	/** Seed buffer.  */
	uint32_t memory[256];
	/** Contains how many times a generation has happened.  */
	uint32_t aa;
	/** Combined with cc.  */ 
	uint32_t bb;
	/** Contains how many times a generation has happened.  */
	uint32_t cc;
};


void initialize_isaac_data(struct isaac *data, int use_seed);
int get_isaac_value(struct isaac *data);

#endif
