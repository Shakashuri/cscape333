/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file item_handler.c
 * @brief Functions relating to handling the usage of items. 
 */

#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "client.h"
#include "dialogue_handler.h"
#include "item.h"
#include "misc.h"
#include "packet.h"
#include "player.h"
#include "shops.h"
#include "skills.h"
#include "skill_cooking.h"
#include "skill_crafting.h"
#include "skill_firemaking.h"
#include "skill_fletching.h"
#include "skill_herblore.h"
#include "skill_prayer.h"

/**
 * Handles when an item is used on another one.
 * @param *c Client instance.
 * @param item_1_id ID of the first item.
 * @param item_1_slot Inventory slot of the first item.
 * @param item_2_id ID of the second item.
 * @param item_2_slot Inventory slot of the second item.
 */
void handle_use_item_on_item(struct client *c, int item_1_id, int item_1_slot,
							 int item_2_id, int item_2_slot)
{
	/* Used later to reverse the item_1_id and item_2_id to ensure combinations
	   will be read, no matter what is used on what.  */
	int reversed = 0, break_loop = 0;
	int store;

	/* Reset these variables in case they are used.  */
	c->plr->next_chat_id = 0;
	c->plr->chat_type = -1;
	c->plr->action_menu_id = -1;
	c->plr->action_menu_skill_id = -1;

	/* Item id's are one less than what they are in the inventory.  */
	if ((player_has_item_slot(c, item_1_id, 1, item_1_slot) == -1)
		|| (player_has_item_slot(c, item_2_id, 1, item_2_slot) == -1))
	{
		printf("Player %s tried to use nonexistant items together!\n", 
			   c->plr->name);
		return;
	}
	player_clear_walking_queue(c);

	do
	{
		/* If item id is reversed, set break loop to ensure this is the last
		   time.  */
		if (reversed == 1)
		{
			break_loop = 1;
		}
		switch (item_1_id)
		{
			/* Headless arrow.  */
			case 53:
				switch (item_2_id)
				{
					/* Arrowheads.  */
					case 39:
					case 40:
					case 41:
					case 42:
					case 43:
					case 44:
						c->plr->action_id_type = item_2_id;
						c->plr->action_menu_id 
							= FLETCHING_ACTION_ADD_ARROW_HEADS;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->action_menu_skill_id = FLETCHING_SKILL;
						handle_dialogue(c);
						break;
				}
				break;

			/* Unfinished potions.  */
			case 91:
			case 93:
			case 95:
			case 97:
			case 99:
			case 3002:
			case 101:
			case 103:
			case 105:
			case 3004:
			case 107:
			case 109:
			case 111:
			case 5936:
			case 5939:
			case 5942:
			case 5951:
				switch (item_2_id)
				{
					/* All possible ingredients that can be used.  */
					case 221:
					case 223:
					case 225:
					case 231:
					case 235:
					case 239:
					case 241:
					case 245:
					case 247:
					case 592:
					case 1581:
					case 1975:
					case 2398:
					case 2970:
					case 6049:
					case 6051:
						add_second_ingredient_to_vial(c, item_1_id, item_2_id);
						break;
				}
				break;
			/* Feather.  */
			case 314:
				switch (item_2_id)
				{
					/* Arrow shaft.  */
					case 52:
						c->plr->action_menu_id = FLETCHING_ACTION_ADD_FEATHERS;
						c->plr->action_menu_skill_id = FLETCHING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						handle_dialogue(c);
						break;

					/* Dart heads.  */
					case 819:
					case 820:
					case 821:
					case 822:
					case 823:
					case 824:
						c->plr->action_id_type = item_2_id;
						c->plr->action_menu_id 
							= FLETCHING_ACTION_ADD_DART_HEADS;
						c->plr->action_menu_skill_id = FLETCHING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						handle_dialogue(c);
						break;

				}
				break;

			/* Water Sources.  */
			/* Vial of water.  */
			case 227:
				switch (item_2_id)
				{
					/* Herbs.  */
					case 249:
					case 251:
					case 253:
					case 255:
					case 257:
					case 259:
					case 261: 
					case 263:
					case 265:
					case 267:
					case 269:
					/* Note: Quest requirement for these, Jungle Potion.  */
					case 1526:
					case 1528:
					case 1530:
					case 1532:
					case 1534:
						add_first_ingredient_to_vial(c, item_1_id, item_2_id);
						break;
				}
				/* Falls through */
			case 1921:
			case 1929:
			case 1937:
			case 4458:
				switch (item_2_id)
				{
					/* Dry clay.  */
					case 434:
						transform_item(c, item_1_slot, 
									   ITEMS[item_1_id]->item_consume_id);
						transform_item(c, item_2_slot, 1761);
						packet_send_chatbox_message(c, "You mix the clay and water.");
						break;

					/* Pot of flour.  */
					case 1933:
						c->plr->action_menu_id = COOKING_ACTION_DOUGH;
						c->plr->action_menu_skill_id = COOKING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->action_skill_type = 1;
						c->plr->next_chat_id = 0;
						handle_dialogue(c);
						break;
				}
				break;

			/* Tinderbox.  */
			case 590:
				switch (item_2_id)
				{
					/* Normal logs.  */
					case 1511:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 0;
						init_firemaking(c);
						break;
					/* Achey logs.  */
					case 2862:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 1;
						init_firemaking(c);
						break;
					/* Oak logs.  */
					case 1521:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 2;
						init_firemaking(c);
						break;
					/* Willow logs.  */
					case 1519:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 3;
						init_firemaking(c);
						break;
					/* Teak logs.  */
					case 6333:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 4;
						init_firemaking(c);
						break;
					/* Maple logs.  */
					case 1517:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 5;
						init_firemaking(c);
						break;
					/* Teak logs.  */
					case 6332:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 6;
						init_firemaking(c);
						break;
					/* Yew logs.  */
					case 1515:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 7;
						init_firemaking(c);
						break;
					/* Magic logs.  */
					case 1513:
						c->plr->action_item_id = item_2_id;
						c->plr->action_item_slot = item_2_slot;
						c->plr->action_id_type = 8;
						init_firemaking(c);
						break;
				}
				break;

			/* Bolts.  */
			case 877:
				switch (item_2_id)
				{
				/* Gem bolt tips.  */
					/* Opal bolt tips.  */
					case 45:
						add_tips_to_bolts(c, item_2_id, 879, 
										  FLETCHING_OPAL_BOLTS);
						break;
					/* Pearl bolt tips.  */
					case 46:
						add_tips_to_bolts(c, item_2_id, 880, 
										  FLETCHING_PEARL_BOLTS);
						break;
					/* Barbed bolt tips.  */
					case 47:
						add_tips_to_bolts(c, item_2_id, 881, 
										  FLETCHING_BARBED_BOLTS);
						break;
				}
				break;

			/* Knife.  */
			case 946:
				switch (item_2_id)
				{
					/* Normal logs.  */
					case 1511:
					/* Oak logs.  */
					case 1521:
					/* Willow logs.  */
					case 1519:
					/* Maple logs.  */
					case 1517:
					/* Yew logs.  */
					case 1515:
					/* Magic logs.  */
					case 1513:
					/* Achey logs.  */
					case 2862:
						c->plr->action_menu_id = FLETCHING_ACTION_USE_LOGS;
						c->plr->action_menu_skill_id = FLETCHING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->next_chat_id = 0;
						c->plr->action_id_type = item_2_id;
						handle_dialogue(c);
						break;
					/* Chocolate bar.  */
					case 1973:
						transform_item(c, item_2_slot, 1975);
						player_play_animation(c, 1989);
						packet_send_chatbox_message(c, "You break the chocolate bar down to "
									 "dust.");
						break;
				}
				break;
			
			/* Battlestaff.  */
			case 1391:
				switch (item_2_id)
				{
					case 569:
						craft_battlestaff(c, item_1_id, item_1_slot, item_2_id, 
										  item_2_slot, 172);	
						break;
					case 571:
						craft_battlestaff(c, item_1_id, item_1_slot, item_2_id, 
										  item_2_slot, 170);	
						break;
					case 573:
						craft_battlestaff(c, item_1_id, item_1_slot, item_2_id, 
										  item_2_slot, 173);	
						break;	
					case 575:
						craft_battlestaff(c, item_1_id, item_1_slot, item_2_id, 
										  item_2_slot, 171);	
						break;
				}
				break;

			/* Needle.  */
			case 1733:
				switch (item_2_id)
				{
					/* Soft leather.  */
					case 1741:
					case 1743:
					case 1745:
					case 2505:
					case 2507:
					case 2509:
					case 6289:
						c->plr->action_menu_id = CRAFTING_ACTION_LEATHER;
						c->plr->action_menu_skill_id = CRAFTING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->action_id_type = item_2_id;
						c->plr->next_chat_id = 0;
						handle_dialogue(c);
						break;
				}
				break;

			/* Chisel.  */
			case 1755:
				switch (item_2_id)
				{
				/* Myre snelms.  */
					/* Rounded.  */
					case 3345:
						craft_snelm(c, item_2_id, 3327, CRAFTING_SNELM);
						break;
					/* Pointed.  */
					case 3355:
						craft_snelm(c, item_2_id, 3337, CRAFTING_SNELM);
						break;
				/* Blood'n'tar snelms.  */
					/* Rounded.  */
					case 3347:
						craft_snelm(c, item_2_id, 3329, CRAFTING_SNELM);
						break;
					/* Pointed.  */
					case 3357:
						craft_snelm(c, item_2_id, 3339, CRAFTING_SNELM);
						break;
				/* Ochre snelms.  */
					/* Rounded.  */
					case 3349:
						craft_snelm(c, item_2_id, 3331, CRAFTING_SNELM);
						break;
					/* Pointed.  */
					case 3359:
						craft_snelm(c, item_2_id, 3341, CRAFTING_SNELM);
						break;
				/* Bruise blue snelms.  */
					/* Rounded.  */
					case 3351:
						craft_snelm(c, item_2_id, 3333, CRAFTING_SNELM);
						break;
					/* Pointed.  */
					case 3361:
						craft_snelm(c, item_2_id, 3343, CRAFTING_SNELM);
						break;
				/* Broken bark snelms.  */
					/* Rounded.  */
					case 3353:
						craft_snelm(c, item_2_id, 3335, CRAFTING_SNELM);
						break;
					/* Broken bark only has a rounded variant.  */
				
				/* Gems.  */
					case 1617:
					case 1619:
					case 1621:
					case 1623:
					case 1625:
					case 1627:
					case 1629:
					case 1631:
						c->plr->action_menu_id = CRAFTING_ACTION_CUTTING_GEMS;
						c->plr->action_menu_skill_id = CRAFTING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->action_id_type = item_2_id;
						c->plr->next_chat_id = 0;
						handle_dialogue(c);
						break;
				}
				break;

			/* Ball of wool.  */
			case 1759:
				switch (item_2_id)
				{
					case 1673:
					case 1675:
					case 1677:
					case 1679:
					case 1681:
					case 1683:
					case 1714:
					case 1720:
						crafting_string_item(c, item_2_id, item_2_slot,
											 item_1_id, item_1_slot);
						break;
				}
				break;

			/* Molten glass.  */
			case 1775:
				switch (item_2_id)
				{
					/* Glassblowing pipe.  */
					case 1785:
						c->plr->action_menu_id = 134;
						c->plr->action_menu_skill_id = CRAFTING_SKILL;
						c->plr->next_chat_id = 0;
						handle_dialogue(c);
						break;
				}
				break;

			/* Bow string.  */
			case 1777:
				switch (item_2_id)
				{
				/* String bows.  */
					/* Normal bows.  */
					case 50:
						string_bow(c, item_2_id, 841, FLETCHING_STRING_SBOW);
						break;
					case 48:
						string_bow(c, item_2_id, 839, FLETCHING_STRING_BOW);
						break;
					/* Oak bows.  */
					case 54:
						string_bow(c, item_2_id, 843,
								   FLETCHING_STRING_OAK_SBOW);
						break;
					case 56:
						string_bow(c, item_2_id, 845,
								   FLETCHING_STRING_OAK_BOW);
						break;
					/* Willow bows.  */
					case 60:
						string_bow(c, item_2_id, 849,
								   FLETCHING_STRING_WILLOW_SBOW);
						break;
					case 58:
						string_bow(c, item_2_id, 847,
								   FLETCHING_STRING_WILLOW_BOW);
						break;
					/* Maple bows.  */
					case 64:
						string_bow(c, item_2_id, 853,
								   FLETCHING_STRING_MAPLE_SBOW);
						break;
					case 62:
						string_bow(c, item_2_id, 851,
								   FLETCHING_STRING_MAPLE_BOW);
						break;
					/* Yew bows.  */
					case 68:
						string_bow(c, item_2_id, 857,
								   FLETCHING_STRING_YEW_SBOW);
						break;
					case 66:
						string_bow(c, item_2_id, 855,
								   FLETCHING_STRING_YEW_BOW);
						break;
					/* Magic bows.  */
					case 72:
						string_bow(c, item_2_id, 861,
								   FLETCHING_STRING_MAGIC_SBOW);
						break;
					case 70:
						string_bow(c, item_2_id, 859,
								   FLETCHING_STRING_MAGIC_BOW);
						break;
				}
				break;

			/* Chocolate bar.  */
			case 1973:
			/* Chocolate dust.  */
			case 1975:
				switch (item_2_id)
				{
					/* Cake.  */
					case 1891:
						if (c->plr->level[COOKING_SKILL] >= 50)
						{
							transform_item(c, item_2_slot, 1897);
							delete_item_from_inv_slot(c, item_1_slot, 1);
							packet_send_chatbox_message(c, "You turn the cake into a "
										 "chocolate cake.");
							add_skill_xp(c, COOKING_SKILL, 
								get_cooking_xp(COOKING_CHOCOLATE_CAKE));
						}
						else
						{
							packet_send_chatbox_message(c, "You need to be level 50 in "
										 "cooking to make that!");
						}
						break;
				}
				break;

			/* Plain pizza.  */
			case 2289:
				switch (item_2_id)
				{
					/* Meat for meat pizza.  */
					case 2142:
						if (c->plr->level[COOKING_SKILL] >= 45)
						{
							transform_item(c, item_1_slot, 2293);
							delete_item_from_inv_slot(c, item_2_slot, 1);
							packet_send_chatbox_message(c, "You add meat to the pizza.");
							add_skill_xp(c, COOKING_SKILL, 
								get_cooking_xp(COOKING_MEAT_PIZZA));
						}
						else
						{
							packet_send_chatbox_message(c, "You need to be level 45 in "
										 "cooking to make that!");
						}
						break;
					/* Anchovies for anchovy pizza.  */
					case 319:
						if (c->plr->level[COOKING_SKILL] >= 55)
						{
							transform_item(c, item_1_slot, 2297);
							delete_item_from_inv_slot(c, item_2_slot, 1);
							packet_send_chatbox_message(c, "You add anchovies to the pizza.");
							add_skill_xp(c, COOKING_SKILL, 
								get_cooking_xp(COOKING_ANCHOVY_PIZZA));
						}
						else
						{
							packet_send_chatbox_message(c, "You need to be level 55 in "
										 "cooking to make that!");
						}
						break;
					/* Pineapple for pineapple pizza.  */
					case 2116:
					case 2118:
						if (c->plr->level[COOKING_SKILL] >= 65)
						{
							transform_item(c, item_1_slot, 2301);
							delete_item_from_inv_slot(c, item_2_slot, 1);
							packet_send_chatbox_message(c, "You add pineapple to the pizza.");
							add_skill_xp(c, COOKING_SKILL,
								get_cooking_xp(COOKING_PINEAPPLE_PIZZA));
						}
						else
						{
							packet_send_chatbox_message(c, "You need to be level 65 in "
										 "cooking to make that!");
						}
						break;
				}
				break;

			/* Steel studs.  */
			case 2370:
				switch (item_2_id)
				{
				/* Add studs to leather armor.  */
					/* Leather chaps.  */
					case 1095:
					case 1129:
						c->plr->action_menu_id = CRAFTING_ACTION_LEATHER;
						c->plr->action_menu_skill_id = CRAFTING_SKILL;
						c->plr->chat_type = SKILL_MENU_CHAT;
						c->plr->action_id_type = item_2_id;
						c->plr->next_chat_id = 0;
						handle_dialogue(c);
						break;
				}
				break;

			/* Sacred oils.  */
			case 3430:
			case 3432:
			case 3434:
			case 3436:
				switch (item_2_id)
				{
					/* Normal logs.  */
					case 1511:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 20);
						break;
					/* Oak logs.  */
					case 1521:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 21);
						break;
					/* Willow logs.  */
					case 1519:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 22);
						break;
					/* Teak logs.  */
					case 6333:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 23);
						break;
					/* Maple logs.  */
					case 1517:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 24);
						break;
					/* Mahogany logs.  */
					case 6332:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 25);
						break;
					/* Yew logs.  */
					case 1515:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 26);
						break;
					/* Magic logs.  */
					case 1513:
						anoint_logs(c, item_1_id, item_1_slot, item_2_id, 
									 item_2_slot, 27);
						break;
				}
				/* Falls through.  */
				case 2428:
				case 121:
				case 123:
				case 125:
				case 2446:
				case 175:
				case 177:
				case 179:
				case 113:
				case 115:
				case 117:
				case 119:
				case 3408:
				case 3410:
				case 3412:
				case 3414:
				case 2430:
				case 127:
				case 129:
				case 131:
				case 3008:
				case 3010: 
				case 3012:
				case 3014:
				case 2432:
				case 133:
				case 135:
				case 137:
				case 2434:
				case 139:
				case 141:
				case 143:
				case 3032:
				case 3034:
				case 3036:
				case 3038:
				case 2436:
				case 145:
				case 147:
				case 149:
				case 2448:
				case 181:
				case 183:
				case 185:
				case 2438:
				case 151:
				case 153:
				case 155:
				case 3016:
				case 3018:
				case 3020:
				case 3022:
				case 2440:
				case 157:
				case 159:
				case 161:
				case 3024:
				case 3026:
				case 3028:
				case 3030:
				case 2442:
				case 163:
				case 165:
				case 167:
				case 5943:
				case 5945:
				case 5947:
				case 5949:
				case 2444:
				case 169:
				case 171:
				case 173:
				case 2450:
				case 189:
				case 191:
				case 193:
				case 5952:
				case 5954:
				case 5956:
				case 5958:
					decant_potions(c, item_2_id, item_2_slot, 
								   item_1_id, item_1_slot);
					break;
			
			default:
				/* Swap ids for another pass.  */
				if (reversed != 1)
				{
					store = item_1_id;
					item_1_id = item_2_id;
					item_2_id = store;
					
					store = item_1_slot;
					item_1_slot = item_2_slot;
					item_2_slot = store;
					
					reversed = 1;
				}
				else
				{
					packet_send_chatbox_message(c, "Nothing interesting happens.");
				}
				break;
		}
	}
	while (reversed == 1 && break_loop != 1);
}

/**
 * Handles packets dealing with specific interfaces.
 * @param *c Client instance.
 * @param interface_id Interface where the item was used.
 * @param slot Slot where the item is in the interface.
 * @param item_id ID of the item used.
 * @param packet_opcode Specific packet id representing the item related
 * action done.
 */
void interface_item_handler(struct client *c, int interface_id, int slot, 
							int item_id, int packet_opcode)
{
	int number = 0;
	char return_msg[200];
	int temp_var = 0;

	/* Check if an equipment item was clicked, handle uneqipping.  */	
	if (c->plr->sidebar_hidden == 0)
	{
		if (interface_id == INTR_EQUIP_PLR_ITEMS 
			&& c->plr->equipment[slot] > 0)
		{
			unequip_item(c, item_id, slot);
			return;
		}
	}
	
	if (interface_id == INTR_BANK_ITEMS)
	{
		/* Withdraw.  */
		c->plr->action_menu_id = 0;
	}
	else if (interface_id == INTR_BANK_PLR_ITEMS)
	{
		/* Deposit.  */
		c->plr->action_menu_id = 1;
	}

	/* Bank 5.  */
	if (packet_opcode == CLIENT_PKT_SELECT_FIVE_ITEMS)
	{
		if (interface_id == INTR_SHOP_ITEMS
			|| interface_id == INTR_SHOP_PLR_ITEMS)
		{
			number = 1;
		}
		else
		{
			number = 5;
		}
	}
	/* Bank 10.  */
	else if (packet_opcode == CLIENT_PKT_SELECT_TEN_ITEMS)
	{
		if (interface_id == INTR_SHOP_ITEMS)
		{
			number = 5;
		}
		else
		{
			number = 10;
		}
	}
	/* Bank all.  */
	else if (packet_opcode == CLIENT_PKT_SELECT_ALL_ITEMS)
	{
		if (interface_id == INTR_SHOP_ITEMS)
		{
			number = 10;
		}
		else
		{
			number = MAX_ITEM_STACK;
		}
	}
	/* Bank X.  */
	else if (packet_opcode == CLIENT_PKT_SELECT_X_ITEMS)
	{
		number = c->plr->action_skill_remaining_actions;
		/*reset_skilling(c); */
		
		if (number <= 0)
		{
			return;
		}
	}
	else
	{
		number = 1;
	}
	
	printf("\tNUMBER: %d\n", number);

	switch (interface_id)
	{
		/* Crafting rings.  */
		case 4233:
			c->plr->action_menu_id = 136;
			c->plr->action_menu_skill_id = CRAFTING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (140 + slot);
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		/* Crafting necklaces.  */
		case 4239:
			c->plr->action_menu_id = 136;
			c->plr->action_menu_skill_id = CRAFTING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (150 + slot);
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		/* Crafting amulets.  */
		case 4245:
			c->plr->action_menu_id = 136;
			c->plr->action_menu_skill_id = CRAFTING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (160 + slot);
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		/* Smithing interface.  */
		case 1119:
			c->plr->action_menu_id = SMITHING_ACTION_SMITH;
			c->plr->action_menu_skill_id = SMITHING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (100 + slot);
			c->plr->action_skill_result_item = item_id;
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		case 1120:
			c->plr->action_menu_id = SMITHING_ACTION_SMITH;
			c->plr->action_menu_skill_id = SMITHING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (110 + slot);
			c->plr->action_skill_result_item = item_id;
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		case 1121:
			c->plr->action_menu_id = SMITHING_ACTION_SMITH;
			c->plr->action_menu_skill_id = SMITHING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (120 + slot);
			c->plr->action_skill_result_item = item_id;
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		case 1122:
			c->plr->action_menu_id = SMITHING_ACTION_SMITH;
			c->plr->action_menu_skill_id = SMITHING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (130 + slot);
			c->plr->action_skill_result_item = item_id;
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		case 1123:
			c->plr->action_menu_id = SMITHING_ACTION_SMITH;
			c->plr->action_menu_skill_id = SMITHING_SKILL;
			c->plr->chat_type = SKILL_MENU_CHAT;
			c->plr->action_id_type = (140 + slot);
			c->plr->action_skill_result_item = item_id;
			c->plr->action_skill_remaining_actions = number;
			handle_dialogue(c);
			break;
		/* Deposit, bank deposit box.  */
		case INTR_BANK_DEPOSIT_BOX_ITEMS:
			printf("BANKING FROM INVENTORY TO BANK!\n");
			printf("ID: %d SLOT: %d NUM: %d\n", item_id, slot, number);
			deposit_into_bank(c, item_id, slot, number);
			packet_reload_items_in_bank_deposit_box_interface(c);
			packet_reload_items_in_inventory_interface(c);
			break;
		/* Player inventory when banking, deposit.  */
		case INTR_BANK_PLR_ITEMS:
			printf("BANKING FROM INVENTORY TO BANK!\n");
			printf("ID: %d SLOT: %d NUM: %d\n", item_id, slot, number);
			deposit_into_bank(c, item_id, slot, number);
			break;
		/* Bank inventory when banking, withdraw.  */
		case INTR_BANK_ITEMS:
			printf("BANKING FROM BANK TO INVENTORY!\n");
			printf("ID: %d SLOT: %d NUM: %d\n", item_id, slot, number);
			withdraw_from_bank(c, item_id, slot, number);
			break;
		case INTR_SHOP_ITEMS:
			if (packet_opcode == CLIENT_PKT_REMOVE_ITEM_FROM_INTERFACE)
			{
				temp_var = get_shop_selling_price(c->plr->interface_open_sub,
												 item_id, -1);

				if (temp_var == 1)
				{
					snprintf(return_msg, 200, "%s: currently costs %d coin.",
							 ITEMS[item_id]->name, temp_var);
				}
				else
				{
					snprintf(return_msg, 200, "%s: currently costs %d coins.",
							 ITEMS[item_id]->name, temp_var);
				}

				packet_send_chatbox_message(c, return_msg);

			}
			else
			{
				buy_item_from_shop(c, c->plr->interface_open_sub, item_id, 
								   number);
			}
			break;
		case INTR_SHOP_PLR_ITEMS:
			if (packet_opcode == CLIENT_PKT_REMOVE_ITEM_FROM_INTERFACE)
			{
				temp_var = get_shop_buying_price(c->plr->interface_open_sub,
												 item_id, -1);

				if (temp_var == 1)
				{
					snprintf(return_msg, 200, "%s: shop will buy for %d coin.",
							 ITEMS[item_id]->name, temp_var);
				}
				else
				{
					snprintf(return_msg, 200, "%s: shop will buy for %d coins.",
							 ITEMS[item_id]->name, temp_var);
				}

				packet_send_chatbox_message(c, return_msg);
			}
			else
			{
				sell_item_to_shop(c, c->plr->interface_open_sub, item_id);
			}
			break;
		default:
			printf("ID: %d\n", interface_id);
			break;
	}
}

/**
 * For when an item on the ground has a option, and it was clicked. Mostly for
 * lighting logs.
 * @param *c Client instance.
 * @param item_id ID of the item used.
 * @param item_x X position in the world of the item.
 * @param item_y Y position in the world of the item.
 */
void handle_item_on_ground(struct client *c, int item_id, int item_x, 
						   int item_y)
{
	if (is_item_drop_in_world(item_id, 
								  item_x, 
								  item_y,
								  c->plr->world_z) == -1)
	{
		printf("Player %s tried to use a nonexistant item on the ground!\n", 
			   c->plr->name);
		c->plr->action_mask = 0;
		return;
	}

	if (check_good_distance(c->plr->world_x, c->plr->world_y, 
		c->plr->action_x, c->plr->action_y, 1)
		&& (c->plr->walking_dir == -1 
			&& c->plr->running_dir == -1))
	{
		switch (c->plr->action_item_id)
		{
			/* Normal logs.  */
			case 1511:
				c->plr->action_id_type = 0;
				init_firemaking(c);
				break;
			/* Achey logs.  */
			case 2862:
				c->plr->action_id_type = 1;
				init_firemaking(c);
				break;
			/* Oak logs.  */
			case 1521:
				c->plr->action_id_type = 2;
				init_firemaking(c);
				break;
			/* Willow logs.  */
			case 1519:
				c->plr->action_id_type = 3;
				init_firemaking(c);
				break;
			/* Teak logs.  */
			case 6333:
				c->plr->action_id_type = 4;
				init_firemaking(c);
				break;
			/* Maple logs.  */
			case 1517:
				c->plr->action_id_type = 5;
				init_firemaking(c);
				break;
			/* Teak logs.  */
			case 6332:
				c->plr->action_id_type = 6;
				init_firemaking(c);
				break;
			/* Yew logs.  */
			case 1515:
				c->plr->action_id_type = 7;
				init_firemaking(c);
				break;
			/* Magic logs.  */
			case 1513:
				c->plr->action_id_type = 8;
				init_firemaking(c);
				break;
			default:
				printf("Not handled item option! Exit!\n");
				reset_skilling(c);
				break;
		}
		c->plr->action_mask = 0;
	}
}

/**
 * Handles an item's first click action. This is the first action in the right
 * click list.
 * @param *c Client instance.
 * @param frame_id Interface where the item was used.
 * @param item_id Item id of the item that was used.
 * @param item_slot Which slot the item was located in the interface.
 */
void handle_item_first_action(struct client *c, int frame_id, int item_id, 
							  int item_slot)
{
	if (frame_id == INTR_INV_PLR_ITEMS)
	{
		if (get_item_in_inventory_slot(c, item_slot) != item_id)
		{
			printf("Error, player does not actually have item for first "
				   "action\n");
			printf("\tFRAME: %d ITEM ID: %d SLOT: %d\n", frame_id, item_id, 
				   item_slot);
			return;
		}

		switch (item_id)
		{
			/* Bury bones action.  */
			case 526:
			case 528:
			case 2859:
			case 3179:
			case 3180:
			case 3183:
			case 3185:
			case 3186:
			case 530:
			case 532:
			case 3125:
			case 3181:
			case 3182:
			case 534:
			case 536:
			case 4812:
			case 4830:
			case 4832:
			case 4834:
				bury_bones(c, item_id, item_slot);
				break;
			/* Foods.  */
			case 113:
			case 115:
			case 117:
			case 119:
			case 121:
			case 123:
			case 125:
			case 127:
			case 129:
			case 131:
			case 133:
			case 135:
			case 137:
			case 139:
			case 141:
			case 143:
			case 145:
			case 147:
			case 149:
			case 151:
			case 153:
			case 155:
			case 157:
			case 159:
			case 161:
			case 163:
			case 165:
			case 167:
			case 169:
			case 171:
			case 173:
			case 175:
			case 177:
			case 179:
			case 181:
			case 183:
			case 185:
			case 189:
			case 191:
			case 193:
			case 197:
			case 247:
			case 315:
			case 319:
			case 325:
			case 329:
			case 333:
			case 337:
			case 339:
			case 347:
			case 351:
			case 355:
			case 361:
			case 365:
			case 373:
			case 379:
			case 385:
			case 391:
			case 397:
			case 403:
			case 464:
			case 712:
			case 739:
			case 1582:
			case 1861:
			case 1883:
			case 1885:
			case 1891:
			case 1893:
			case 1895:
			case 1897:
			case 1899:
			case 1901:
			case 1905:
			case 1907:
			case 1909:
			case 1911:
			case 1913:
			case 1915:
			case 1917:
			case 1942:
			case 1957:
			case 1959:
			case 1961:
			case 1963:
			case 1965:
			case 1967:
			case 1969:
			case 1971:
			case 1973:
			case 1977:
			case 1978:
			case 1982:
			case 1985:
			case 1989:
			case 1991:
			case 1993:
			case 2003:
			case 2011:
			case 2015:
			case 2017:
			case 2019:
			case 2021:
			case 2028:
			case 2030:
			case 2032:
			case 2034:
			case 2036:
			case 2038:
			case 2040:
			case 2042:
			case 2044:
			case 2046:
			case 2048:
			case 2050:
			case 2052:
			case 2054:
			case 2056:
			case 2058:
			case 2060:
			case 2062:
			case 2064:
			case 2066:
			case 2068:
			case 2070:
			case 2072:
			case 2074:
			case 2076:
			case 2078:
			case 2080:
			case 2082:
			case 2084:
			case 2086:
			case 2088:
			case 2090:
			case 2092:
			case 2102:
			case 2104:
			case 2106:
			case 2108:
			case 2110:
			case 2112:
			case 2114:
			case 2116:
			case 2118:
			case 2120:
			case 2122:
			case 2124:
			case 2126:
			case 2128:
			case 2130:
			case 2140:
			case 2142:
			case 2149:
			case 2152:
			case 2154:
			case 2156:
			case 2158:
			case 2160:
			case 2162:
			case 2173:
			case 2179:
			case 2181:
			case 2183:
			case 2185:
			case 2187:
			case 2189:
			case 2191:
			case 2193:
			case 2195:
			case 2197:
			case 2203:
			case 2205:
			case 2207:
			case 2209:
			case 2211:
			case 2213:
			case 2215:
			case 2217:
			case 2219:
			case 2221:
			case 2223:
			case 2225:
			case 2227:
			case 2229:
			case 2231:
			case 2233:
			case 2235:
			case 2237:
			case 2239:
			case 2241:
			case 2243:
			case 2245:
			case 2251:
			case 2253:
			case 2255:
			case 2257:
			case 2259:
			case 2261:
			case 2263:
			case 2265:
			case 2267:
			case 2269:
			case 2271:
			case 2273:
			case 2275:
			case 2277:
			case 2279:
			case 2281:
			case 2289:
			case 2291:
			case 2293:
			case 2295:
			case 2297:
			case 2299:
			case 2301:
			case 2303:
			case 2309:
			case 2323:
			case 2325:
			case 2327:
			case 2331:
			case 2333:
			case 2335:
			case 2343:
			case 2428:
			case 2430:
			case 2432:
			case 2434:
			case 2436:
			case 2438:
			case 2440:
			case 2442:
			case 2444:
			case 2446:
			case 2448:
			case 2450:
			case 2452:
			case 2454:
			case 2456:
			case 2458:
			case 2878:
			case 2955:
			case 3008:
			case 3010:
			case 3012:
			case 3014:
			case 3016:
			case 3018:
			case 3020:
			case 3022:
			case 3024:
			case 3026:
			case 3028:
			case 3030:
			case 3032:
			case 3034:
			case 3036:
			case 3038:
			case 3040:
			case 3042:
			case 3044:
			case 3046:
			case 3144:
			case 3146:
			case 3151:
			case 3162:
			case 3168:
			case 3177:
			case 3228:
			case 3369:
			case 3371:
			case 3373:
			case 3381:
			case 3801:
			case 3803:
			case 4012:
			case 4014:
			case 4016:
			case 4291:
			case 4293:
			case 4417:
			case 4419:
			case 4421:
			case 4423:
			case 4517:
			case 4558:
			case 4559:
			case 4560:
			case 4561:
			case 4562:
			case 4563:
			case 4564:
			case 4608:
			case 4620:
			case 4627:
			case 4842:
			case 4844:
			case 4846:
			case 4848:
			case 5003:
			case 5004:
			case 5504:
			case 5739:
			case 5741:
			case 5743:
			case 5745:
			case 5747:
			case 5749:
			case 5751:
			case 5753:
			case 5755:
			case 5757:
			case 5759:
			case 5761:
			case 5763:
			case 5765:
			case 5771:
			case 5773:
			case 5775:
			case 5777:
			case 5779:
			case 5781:
			case 5783:
			case 5785:
			case 5787:
			case 5789:
			case 5791:
			case 5793:
			case 5795:
			case 5797:
			case 5799:
			case 5801:
			case 5803:
			case 5805:
			case 5807:
			case 5809:
			case 5811:
			case 5813:
			case 5815:
			case 5817:
			case 5819:
			case 5821:
			case 5823:
			case 5825:
			case 5827:
			case 5829:
			case 5831:
			case 5833:
			case 5835:
			case 5837:
			case 5839:
			case 5841:
			case 5843:
			case 5845:
			case 5847:
			case 5849:
			case 5851:
			case 5853:
			case 5855:
			case 5857:
			case 5859:
			case 5861:
			case 5863:
			case 5865:
			case 5867:
			case 5869:
			case 5871:
			case 5873:
			case 5875:
			case 5877:
			case 5879:
			case 5881:
			case 5883:
			case 5885:
			case 5887:
			case 5889:
			case 5891:
			case 5893:
			case 5895:
			case 5897:
			case 5899:
			case 5901:
			case 5903:
			case 5905:
			case 5907:
			case 5909:
			case 5911:
			case 5913:
			case 5915:
			case 5917:
			case 5919:
			case 5921:
			case 5923:
			case 5925:
			case 5927:
			case 5929:
			case 5943:
			case 5945:
			case 5947:
			case 5949:
			case 5952:
			case 5954:
			case 5956:
			case 5958:
			case 5972:
			case 5982:
			case 5984:
			case 5988:
			case 6118:
			case 6202:
			case 6206:
			case 6297:
			case 6299:
			case 6311:
			case 6469:
				eat_food(c, item_id, item_slot);
				break;
			/* Burnt stew.  */
			case 2005:
				transform_item(c, item_slot, ITEMS[item_id]->item_consume_id);
				packet_send_chatbox_message(c, "You remove the burnt stew from the bowl.");
				break;
			/* Burnt curry.  */
			case 2013:
				transform_item(c, item_slot, ITEMS[item_id]->item_consume_id);
				packet_send_chatbox_message(c, "You remove the burnt curry from the bowl.");
				break;
			/* Burnt pie.  */
			case 2329:
				transform_item(c, item_slot, ITEMS[item_id]->item_consume_id);
				packet_send_chatbox_message(c, "You remove the burnt pie from the dish.");
				break;

		/* Identifying herbs.  */
			case 199:
			case 201:
			case 203:
			case 205:
			case 207:
			case 209:
			case 211:
			case 213:
			case 215:
			case 217:
			case 219:
			case 1527:
			case 1529:
			case 1531:
			case 1533:
			case 2485:
			case 3049:
			case 3051:
				identify_herb(c, item_id, item_slot);
				break;

			/* Nothing interesting for this items.  */
			case 1984:
				packet_send_chatbox_message(c, "Nothing interesting happens.");
				break;
			default:
				printf("Item id %d not handled for first action, "
					   "inventory.\n", item_id);
				packet_send_chatbox_message(c, "Nothing interesting happens.");
				break;
		}
	}
	else
	{
		printf("ITEM FIRST ACTION NOT HANDLED:\n");
		printf("\tFRAME: %d ITEM ID: %d SLOT: %d\n", frame_id, item_id, 
			   item_slot);
		return;
	}
}

/**
 * Handles an item's second click action. This is the second action in the 
 * right click list.
 * @param *c Client instance.
 * @param frame_id Interface where the item was used.
 * @param item_id Item id of the item that was used.
 * @param item_slot Which slot the item was located in the interface.
 */
void handle_item_second_action(struct client *c, int frame_id, int item_id, 
							   int item_slot)
{
	if (frame_id == INTR_INV_PLR_ITEMS)
	{
		if (get_item_in_inventory_slot(c, item_slot) != item_id)
		{
			printf("Error, player does not actually have item for first "
				   "action\n");
			printf("\tFRAME: %d ITEM ID: %d SLOT: %d\n", frame_id, item_id, 
				   item_slot);
			return;
		}

		switch (item_id)
		{
		/* Potions, empty contents onto ground.  */
			case 91:
			case 93:
			case 95:
			case 97:
			case 99:
			case 101:
			case 103:
			case 105:
			case 107:
			case 109:
			case 111:
			case 113:
			case 115:
			case 117:
			case 119:
			case 121:
			case 123:
			case 125:
			case 127:
			case 129:
			case 131:
			case 133:
			case 135:
			case 137:
			case 139:
			case 141:
			case 143:
			case 151:
			case 153:
			case 155:
			case 169:
			case 171:
			case 173:
			case 189:
			case 191:
			case 193:
			case 2428:
			case 2430:
			case 2432:
			case 2434:
			case 2438:
			case 2444:
			case 2450:
			case 2452:
			case 2454:
			case 2456:
			case 2458:
			case 2483:
			case 3002:
			case 3004:
			case 3008:
			case 3010:
			case 3012:
			case 3014:
			case 3032:
			case 3034:
			case 3036:
			case 3038:
			case 3040:
			case 3042:
			case 3044:
			case 3046:
			case 4840:
			case 5936:
			case 5939:
			case 5942:
			case 5951:
			case 6470:
			case 6472:
			case 6474:
			case 6476:
			case 145:
			case 147:
			case 149:
			case 157:
			case 159:
			case 161:
			case 163:
			case 165:
			case 167:
			case 175:
			case 177:
			case 179:
			case 181:
			case 183:
			case 185:
			case 187:
			case 2436:
			case 2440:
			case 2442:
			case 2446:
			case 2448:
			case 3016:
			case 3018:
			case 3020:
			case 3022:
			case 3024:
			case 3026:
			case 3028:
			case 3030:
			case 3408:
			case 3410:
			case 3412:
			case 3414:
			case 3416:
			case 3417:
			case 3418:
			case 3419:
			case 3430:
			case 3432:
			case 3434:
			case 3436:
			case 4842:
			case 4844:
			case 4846:
			case 4848:
			case 5937:
			case 5940:
			case 5943:
			case 5945:
			case 5947:
			case 5949:
			case 5952:
			case 5954:
			case 5956:
			case 5958:

				if (ITEMS[item_id]->item_container_id != -1)
				{
					transform_item(c, item_slot, 
								   ITEMS[item_id]->item_container_id);
					packet_send_chatbox_message(c, "You empty the potion on the ground.");
				}
				break;

			/* Other items with an empty action.  */
			case 227:
			case 723:
			case 724:
			case 725:
			case 726:
			case 1783:
			case 1921:
			case 1927:
			case 1929:
			case 1933:
			case 1937:
			case 3006:
			case 3422:
			case 3424:
			case 3426:
			case 3428:
			case 4417:
			case 4419:
			case 4421:
			case 4423:
			case 4456:
			case 4458:
			case 4460:
			case 4462:
			case 4464:
			case 4466:
			case 4468:
			case 4470:
			case 4472:
			case 4474:
			case 4476:
			case 4478:
			case 4480:
			case 4482:
			case 4622:
			case 4687:
			case 4693:
			case 5935:
			case 6032:
			case 6034:

				if (ITEMS[item_id]->item_container_id != -1)
				{
					transform_item(c, item_slot, 
								   ITEMS[item_id]->item_container_id);
					packet_send_chatbox_message(c, 
							"You empty the contents onto the ground.");
				}
				else
				{
					printf("Item %d doesn't have a container id!\n", item_id);
				}
				break;

			default:
				printf("Item id %d not handled for second action, "
					   "inventory.\n", item_id);
				packet_send_chatbox_message(c, "Nothing interesting happens.");
				break;
		}
	}
	else
	{
		printf("ITEM SECOND ACTION NOT HANDLED:\n");
		printf("\tFRAME: %d ITEM ID: %d SLOT: %d\n", frame_id, item_id, 
			   item_slot);
		return;
	}

}
