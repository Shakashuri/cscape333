/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MISC_H_
#define _MISC_H_

int convert_server_direction_to_client_format(int direction);
int convert_server_direction_to_client_format_delta_x(int direction);
int convert_server_direction_to_client_format_delta_y(int direction);

void default_look_male(struct client *c);
void default_look_female(struct client *c);

int get_straight_line_path_to_point(int start_x, int start_y,
									int end_x, int end_y, int height,
									int results_x[], int results_y[], 
									int results_size,
									int ignore_collision, int include_last);

int check_ranged_line(int x0, int y0, int x1, int y1, int height); 
int check_line_of_sight(struct client *c, struct npc_spawn *npc, int distance);

void print_npc_spawn_data(int npc_index);
void print_npcs_in_range(int npc_id, int range);

int get_walking_direction(int orig_x, int orig_y, int dest_x, int dest_y);
int can_walk_in_direction(int orig_x, int orig_y, int orig_z,
						  int dest_x, int dest_y, int is_npc);
int can_walk_in_direction_big(int orig_x, int orig_y, int orig_z, 
							  int dest_x, int dest_y, int is_npc, int size);
int can_range_in_direction(int orig_x, int orig_y,
						   int dest_x, int dest_y, int orig_z);
int get_uptime_seconds(void);

int is_alphanum(char letter);
char to_lower(char letter);
char to_upper(char letter);
char *string_to_lower(char *string);
char *return_lowercase_string(char *string);
char *string_to_lower_num(char *string, int limit);
char *get_direction_to_string(int direction);
void get_a_or_an(char *to_check, char *article, int size);

int is_double_about_equal(double v1, double v2, double epsilon);

int is_within_combat_range(int t_y, int t_x, int t_size, int a_y, int a_x,
						int a_distance);
int get_distance_from_point(int start_x, int start_y, int end_x, int end_y);
int check_good_distance(int start_x, int start_y, int end_x, int end_y, 
						int distance);
int check_within_object_range(int player_x, int player_y, int obj_x, int obj_y,
							  int obj_size_x, int obj_size_y, int index, 
							  int face);
double f_max(double one, double two);
int get_higher(int x, int y);
int get_smaller(int x, int y);

int is_player_in_wilderness(struct client *c);

void free_agility_data(void);
void free_memory(void);

int is_npc_id_invalid(int npc_id);
int is_npc_index_invalid(int npc_index);
int is_object_id_invalid(int object_id);
int is_item_id_invalid(int item_id);
int is_spell_index_invalid(int spell_index);
int is_slot_num_invalid(int slot, int interface_id);
int is_player_frozen(struct client *c, int print_stun_msg);
int is_player_casting_a_spell(struct client *c);
int is_player_dead_or_frozen(struct client *c, int print_stun_msg);
int is_player_too_far(struct client *c, int entity_x, int entity_y, int dist);
#endif
