/*  CScape333
 *  Copyright (C) 2016-2024 Shakashuri
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SKILLS_H_
#define _SKILLS_H_

void add_skill_xp(struct client *c, int skill_id, float xp);
int get_next_level_up_to_handle(struct client *c);
void packet_send_player_skill_info(struct client *c, int skill_id, 
		int current_level, float xp);
int get_total_levels(struct client *c);
void init_xp_table(void);
int get_level_for_xp(float exp);
int get_xp_needed_for_level(int level);
void packet_send_updated_skill_stats(struct client *c, int skill_id); 
void add_temp_skill_modifier(struct client *c, int skill_id, int amount);
void add_temp_skill_modifier_stacking(struct client *c, int skill_id, 
									  int amount);
int get_player_skill_level(struct client *c, int skill_id);

void reset_skilling(struct client *c);
void reset_skilling_no_animation_reset(struct client *c);
void reset_others_skilling(struct client *c, int index);
int is_player_state_type_skilling(struct client *c);

int is_skill_check_success(struct client *c, int skill_id, int base, 
									 int max, double bonus_amount);

#endif
